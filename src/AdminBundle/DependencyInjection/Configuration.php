<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 10.02.17
 * Time: 17:18
 */

namespace AdminBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('whadmin');

        $rootNode
            ->children()
                ->scalarNode('session_ttl')
                ->defaultValue(15)
                ->end()
                ->scalarNode('password_ttl')
                ->defaultValue(30)
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}