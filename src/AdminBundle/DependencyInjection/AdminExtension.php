<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 10.02.17
 * Time: 17:18
 */

namespace AdminBundle\DependencyInjection;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Yaml\Yaml;

class AdminExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $roles = Yaml::parse(file_get_contents(__DIR__ . '/../Resources/config/security/admin_roles_permissions.yml'));

        $container->setParameter('admin.roles', $roles['admin_roles']);
        $container->setParameter('admin.permissions', $roles['admin_permissions']);

        $container->setParameter('admin.session_ttl', $config['session_ttl']);
        $container->setParameter('admin.password_ttl_seconds', $config['password_ttl'] * 24 * 60 * 60);
    }
}