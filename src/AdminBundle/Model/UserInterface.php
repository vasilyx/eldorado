<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 08.02.17
 * Time: 9:17
 */

namespace AdminBundle\Model;


interface UserInterface extends \Symfony\Component\Security\Core\User\UserInterface
{
    /**
     * This role will never be saved into database, it will be added dynamically in the getRoles() method
     */
    const ROLE_DEFAULT = 'ROLE_ADMIN';

    /**
     * Sets the timestamp that the user requested a password reset.
     *
     * @param null|\DateTime $date
     *
     * @return self
     */
    public function setPasswordRequestedAt(\DateTime $date = null);

    /**
     * Checks whether the password reset request has expired.
     *
     * @param int $ttl Requests older than this many seconds will be considered expired
     *
     * @return boolean
     */
    public function isPasswordRequestNonExpired($ttl);
}