<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 08.02.17
 * Time: 15:53
 */

namespace AdminBundle\Entity;


use AppBundle\Interfaces\SoftDeletableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * Class Session
 * @package AdminBundle\Entity
 */
class Session
{
    use Timestampable;

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    protected $token;
    /**
     * @var User
     */
    protected $user;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return Session
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Session
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }
}