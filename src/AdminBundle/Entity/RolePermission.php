<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 14.02.17
 * Time: 11:52
 */

namespace AdminBundle\Entity;


use AdminBundle\Model\RolePermissionInterface;
use AppBundle\Interfaces\SoftDeletableInterface;
use Knp\DoctrineBehaviors\Model\SoftDeletable\SoftDeletable;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * Class RolePermission
 * @package AdminBundle\Entity
 */
class RolePermission implements RolePermissionInterface, SoftDeletableInterface
{
    use Timestampable;
    use SoftDeletable;

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $role;
    /**
     * @var string
     */
    private $permission;
    /**
     * @var int
     */
    private $mask;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return RolePermission
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return string
     */
    public function getPermission()
    {
        return strtoupper($this->permission);
    }

    /**
     * @param string $permission
     * @return RolePermission
     */
    public function setPermission($permission)
    {
        $this->permission = strtoupper($permission);
        return $this;
    }

    /**
     * @return int
     */
    public function getMask()
    {
        return $this->mask;
    }

    /**
     * @param int $mask
     * @return RolePermission
     */
    public function setMask($mask)
    {
        $this->mask = $mask;
        return $this;
    }
}