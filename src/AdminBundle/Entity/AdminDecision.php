<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 23.05.2018
 * Time: 10:42
 */

namespace AdminBundle\Entity;


use AppBundle\Entity\CallCreditAPICalls;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

class AdminDecision
{
    const APPROVE_DECISION = 100;
    const DECLINE_DECISION = 200;

    use Timestampable;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var CallCreditAPICalls
     */
    private $apiCall;

    /**
     * @var integer
     */
    private $decision;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var User
     */
    private $admin;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return CallCreditAPICalls
     */
    public function getApiCall()
    {
        return $this->apiCall;
    }

    /**
     * @param CallCreditAPICalls $apiCall
     * @return $this
     */
    public function setApiCall($apiCall)
    {
        $this->apiCall = $apiCall;

        return $this;
    }

    /**
     * @return int
     */
    public function getDecision()
    {
        return $this->decision;
    }

    /**
     * @param int $decision
     * @return $this
     */
    public function setDecision($decision)
    {
        $this->decision = $decision;

        return $this;
    }

    /**
     * @return User
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param User $admin
     * @return $this
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }
}