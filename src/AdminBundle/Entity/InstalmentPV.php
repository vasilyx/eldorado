<?php
namespace AdminBundle\Entity;


class InstalmentPV
{
    protected $instalment;

    /**
     * @return mixed
     */
    public function getInstalment()
    {
        return $this->instalment;
    }

    /**
     * @param mixed $instalment
     */
    public function setInstalment($instalment)
    {
        $this->instalment = $instalment;
    }

    /**
     * @return mixed
     */
    public function getInterest()
    {
        return $this->interest;
    }

    /**
     * @param mixed $interest
     */
    public function setInterest($interest)
    {
        $this->interest = $interest;
    }

    /**
     * @return mixed
     */
    public function getBonusInterest()
    {
        return $this->bonusInterest;
    }

    /**
     * @param mixed $bonusInterest
     */
    public function setBonusInterest($bonusInterest)
    {
        $this->bonusInterest = $bonusInterest;
    }

    /**
     * @return mixed
     */
    public function getBrokerInterest()
    {
        return $this->brokerInterest;
    }

    /**
     * @param mixed $brokerInterest
     */
    public function setBrokerInterest($brokerInterest)
    {
        $this->brokerInterest = $brokerInterest;
    }

    /**
     * @return mixed
     */
    public function getInvestorInterest()
    {
        return $this->investorInterest;
    }

    /**
     * @param mixed $investorInterest
     */
    public function setInvestorInterest($investorInterest)
    {
        $this->investorInterest = $investorInterest;
    }

    /**
     * @return mixed
     */
    public function getInvestorPrinciple()
    {
        return $this->investorPrinciple;
    }

    /**
     * @param mixed $investorPrinciple
     */
    public function setInvestorPrinciple($investorPrinciple)
    {
        $this->investorPrinciple = $investorPrinciple;
    }
    protected $interest;
    protected $bonusInterest;
    protected $brokerInterest;
    protected $investorInterest;
    protected $investorPrinciple;
}