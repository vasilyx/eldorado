<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 14.02.17
 * Time: 15:16
 */

namespace AdminBundle\Exception;


class BadPermissionException extends \Exception
{
    protected $message = 'Bad admin permission';

    public function __construct()
    {
        parent::__construct($this->message);
    }
}