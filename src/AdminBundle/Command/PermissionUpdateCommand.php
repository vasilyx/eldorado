<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 14.02.17
 * Time: 17:30
 */

namespace AdminBundle\Command;


use AdminBundle\Entity\RolePermission;
use AdminBundle\Security\Permission\MaskBuilder;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Updates role-permission table in according with an admin_roles_permissions.yml file content
 *
 * Class PermissionUpdateCommand
 * @package AdminBundle\Command
 */
class PermissionUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('admin:permissions:update')
            ->setDescription('This command updates database permission scheme from config files')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $roles = $this->getContainer()->getParameter('admin.roles');
        /**
         * We should set permissions at least for the ROLE_SUPER_ADMIN
         */
        $roles = array_unique(array_merge($roles, ['ROLE_SUPER_ADMIN']));

        $permissions = $this->getContainer()->get('whadmin.role_permission_manager')->getPermissionNames();

        /**
         * We should set permissions USER and PERMISSION at least
         */
        $permissions = array_unique(array_merge($permissions, ['USER', 'PERMISSION']));
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        foreach ($roles as $role) {
            foreach ($permissions as $permission) {
                if (!$this->getContainer()->get('whadmin.role_permission_manager')->isExist($role, strtoupper($permission))) {
                    $rolePermission = new RolePermission();
                    $mask = MaskBuilder::create();
                    if ("ROLE_SUPER_ADMIN" === $role) {
                        $mask
                            ->addCreate()
                            ->addView()
                            ->addUpdate()
                            ->addDelete();
                    }
                    $rolePermission
                        ->setRole($role)
                        ->setPermission(strtoupper($permission))
                        ->setMask($mask->getMask())
                    ;

                    $entityManager->persist($rolePermission);
                }
            }
        }

        $entityManager->flush();
    }
}