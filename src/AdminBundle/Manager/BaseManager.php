<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 08.02.17
 * Time: 16:22
 */

namespace AdminBundle\Manager;


use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class BaseManager
{
    use ContainerAwareTrait;

    public function getEntityManager()
    {
        return $this->container->get('doctrine.orm.entity_manager');
    }
}