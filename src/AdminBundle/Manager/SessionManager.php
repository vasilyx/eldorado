<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 08.02.17
 * Time: 16:22
 */

namespace AdminBundle\Manager;


use AdminBundle\Entity\Session;
use AdminBundle\Entity\User;
use Doctrine\Common\Collections\Criteria;

/**
 * Class SessionManager
 * @package AdminBundle\Manager
 */
class SessionManager extends BaseManager
{

    const TTL = 'PT15M';

    /**
     * Generates token for session
     *
     * @return string
     */
    public function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }

    /**
     * Creates a session for a user
     *
     * @param User $user
     * @return Session
     */
    public function createSession(User $user)
    {
        $session = new Session();

        $session
            ->setToken($this->generateToken())
            ->setUser($user)
            ;

        $em = $this->container->get('doctrine.orm.entity_manager');

        $em->persist($session);
        $em->flush();

        return $session;
    }

    /**
     * Finds session with valid createdAt
     *
     * @param $token
     * @return Session
     */
    public function findOneByToken($token)
    {
        $ttl = new \DateTime();

        /**
         * Subtracts ttl from now
         */
        $ttl->sub(new \DateInterval($this->getTtl()));

        /**
         * CreatedAt greater than or equal to "now minus ttl"
         */
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->gte('session.updatedAt', $ttl))
        ;

        $session = $this->getEntityManager()->getRepository(Session::class)->findOneByToken($token, $criteria);

        return $session;
    }

    /**
     * Time to live for session
     *
     * @return string
     */
    protected function getTtl()
    {
        return sprintf('PT%dM', $this->container->getParameter('admin.session_ttl'));
    }
}