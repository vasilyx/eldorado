<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 08.02.17
 * Time: 11:29
 */

namespace AdminBundle\Manager;


use AdminBundle\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Class UserManager
 * @package AdminBundle\Manager
 */
class UserManager extends BaseManager
{
    /**
     * Creates empty user instance
     *
     * @return User
     */
    public function createUser()
    {
        $user = new User();

        return $user;
    }

    /**
     * Updates user to set encoded password. Persists and flushes (if andFlush is true) user entity.
     *
     * @param User $user
     * @param bool $andFlush
     *
     * @return ConstraintViolationListInterface A list of constraint violations
     *                                          If the list is empty, validation
     *                                          succeeded
     */
    public function updateUser(User $user, $andFlush = true)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $errors = $this->container->get('validator')->validate($user);
        if (0 === count($errors)) {
            $this->hashPassword($user);

            $em->persist($user);
            if ($andFlush) {
                $em->flush();
            }
        }

        return $errors;
    }

    /**
     * Hashes password with generated salt
     *
     * @param User $user
     */
    public function hashPassword(User $user)
    {
        $plainPassword = $user->getPlainPassword();

        if (0 === strlen($plainPassword)) {
            return;
        }

        $user->setSalt($this->generateSalt());

        $hashedPassword = $this->encodePassword($user, $plainPassword);
        $user->setPassword($hashedPassword);
        $user->eraseCredentials();
    }

    /**
     * Encodes password
     *
     * @param UserInterface $user
     * @param $plainPassword
     * @return string
     */
    public function encodePassword(UserInterface $user, $plainPassword)
    {
        $encoder = $this->container->get('security.password_encoder');

        $hashedPassword = $encoder->encodePassword($user, $plainPassword);

        return $hashedPassword;
    }

    /**
     * @return string
     */
    public function generateSalt()
    {
        return rtrim(str_replace('+', '.', base64_encode(random_bytes(32))), '=');
    }

    /**
     * @param $username
     * @return User|null|object
     */
    public function findOneByUsername($username)
    {
        $user = $this->getEntityManager()->getRepository(User::class)->findOneBy([
            'username' => $username
        ]);

        return $user;
    }

    /**
     * @param $token
     * @return User|null|object
     */
    public function findOneByForgottenPasswordToken($token)
    {
        $user = $this->getEntityManager()->getRepository(User::class)->findOneBy(['forgottenPasswordToken' => $token]);
        return $user;
    }

    /**
     * @param User $user
     * @param $plainPassword
     * @return bool
     */
    public function isPlainPasswordValid(User $user, $plainPassword)
    {
        return $user->getPassword() === $this->encodePassword($user, $plainPassword);
    }

    public function isPasswordRequestNonExpired(User $user)
    {
        return $user->isPasswordRequestNonExpired($this->container->getParameter('admin.password_ttl_seconds'));
    }

    /**
     * @return \AdminBundle\Entity\User[]|array
     */
    public function findAllNonDeleted()
    {
        $users = $this->getEntityManager()->getRepository(User::class)->findAll();

        return $users;
    }

    /**
     * Returns plain array with roles and permissions
     *
     * @param User $user
     * @return array
     */
    public function getPermissions(User $user)
    {

        $result = $this->container->get('whadmin.role_permission_manager')->getPermissionsForUser($user);

        return $result;
    }

    /**
     * Method returns activity logs for admin with page number and limit (amount of logs on the one page)
     *
     * @param User $admin
     * @param $page
     * @param $limit
     * @return mixed
     */
    public function getLogsForUser(User $admin, $page, $limit)
    {
        $qb = $this->getEntityManager()->getRepository('LogBundle:AdminActivityLog')->createQueryBuilder('log');
        $qb
            ->addSelect('user')
            ->addSelect('details')
            ->leftJoin('log.user', 'user')
            ->leftJoin('user.details', 'details')
            ->where('log.admin = :admin')
            ->orderBy('log.id', 'DESC')
            ->setParameter('admin', $admin);

        $query = $qb->getQuery();

        $paginator = $this->container->get('knp_paginator');
        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($query, $page, $limit);

        return array(
            'logs'  => $pagination->getItems(),
            'count' => $pagination->getTotalItemCount()
        );
    }


}