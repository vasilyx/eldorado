<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 23.05.2018
 * Time: 11:11
 */

namespace AdminBundle\Manager;


use AdminBundle\Entity\AdminDecision;
use AdminBundle\Entity\User;
use AppBundle\Entity\CallCreditAPICalls;
use Doctrine\ORM\EntityManager;

class AdminDecisionManager extends BaseManager
{
    /**
     * Create admin decision method
     *
     * @param User $admin
     * @param $apiCallId
     * @param $decision
     * @param $comment
     * @return AdminDecision|null
     */
    public function createAdminDecision(User $admin, $apiCallId, $decision, $comment)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getEntityManager();

        /** @var CallCreditAPICalls $apiCall */
        $apiCall = $entityManager->getRepository('AppBundle:CallCreditAPICalls')->find($apiCallId);

        if (empty($apiCall)) {
            return null;
        }

        $adminDecision = (new AdminDecision())
                ->setAdmin($admin)
                ->setApiCall($apiCall)
                ->setDecision($decision)
                ->setComment($comment)
        ;

        $entityManager->persist($adminDecision);
        $entityManager->flush();

        return $adminDecision;
    }
}