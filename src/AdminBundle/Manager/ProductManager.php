<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 16.05.2018
 * Time: 10:40
 */

namespace AdminBundle\Manager;


use AppBundle\Entity\Communication;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\Product;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\User;
use AppBundle\Entity\UserProduct;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;

class ProductManager extends BaseManager
{
    /**
     * Get paginated borrowers
     *
     * @param $limit
     * @param $page
     * @param $orderField
     * @param $orderDirection
     *
     * @return array
     *
     */
    public function getPaginatedBorrowers($limit, $page, $orderField, $orderDirection)
    {
        list($borrowers, $count) = $this->getPaginatedProductsByType($limit, $page, $orderField, $orderDirection, Product::TYPE_BORROWER);

        return ['clients' => $borrowers, 'count' => $count];
    }

    /**
     * Get paginated investors
     *
     * @param $limit
     * @param $page
     * @param $orderField
     * @param $orderDirection
     *
     * @return array
     *
     */
    public function getPaginatedInvestors($limit, $page, $orderField, $orderDirection)
    {
        list($investors, $count) = $this->getPaginatedProductsByType($limit, $page, $orderField, $orderDirection, Product::TYPE_INVESTOR);

        return ['clients' => $investors, 'count' => $count];
    }

    /**
     * Get paginated clients query (borrowers / investors lists)
     *
     * @param $limit
     * @param $page
     * @param $orderField
     * @param $orderDirection
     * @param int $type
     * @return array
     */
    private function getPaginatedProductsByType($limit, $page, $orderField, $orderDirection, $type = Product::TYPE_BORROWER)
    {
        $qb = $this->getEntityManager()->getRepository(QuoteApply::class)->createQueryBuilder('quote_apply');
        $qb->addSelect('product')
           ->addSelect('user')
           ->addSelect('details')
           ->addSelect('communications')
           ->addSelect('address');

        $qb->leftJoin('quote_apply.product', 'product')
           ->leftJoin('quote_apply.user', 'user')
           ->leftJoin('user.details', 'details')
           ->leftJoin('user.addresses', 'address')
           ->leftJoin('user.communications', 'communications');

        $qb->where('product.type = ' . $type);

        switch ($orderField) {
            case 'id' :
                $orderFieldInQuery = 'user.id';
                break;
            case 'first_name' :
                $orderFieldInQuery = 'details.firstName';
                break;
            case 'surname' :
                $orderFieldInQuery = 'details.lastName';
                break;
            case 'birth_date' :
                $orderFieldInQuery = 'details.birthdate';
                break;
            case 'age' :
                $orderFieldInQuery = 'details.birthdate';
                break;
            case 'post_code' :
                $orderFieldInQuery = 'address.postalCode';
                break;
            case 'mobile' :
                $orderFieldInQuery = 'communications.value';
                break;
            case 'city' :
                $orderFieldInQuery = 'address.city';
                break;
            case 'email' :
                $orderFieldInQuery = 'user.username';
                break;
            case 'screenname' :
                $orderFieldInQuery = 'details.screenName';
                break;
            case 'quote_status':
                $orderFieldInQuery = 'quote_apply.status';
                break;
            default:
                $orderFieldInQuery = 'user.createdAt';
                $orderDirection = 'DESC';
                break;
        }

        //order direction
        if (!in_array(strtoupper($orderDirection), ['ASC', 'DESC'])) {
            $orderDirection = 'ASC';
        }

        $qb->orderBy($orderFieldInQuery, $orderDirection);
        $query = $qb->getQuery();

        $paginator = $this->container->get('knp_paginator');
        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($query, $page, $limit, ['distinct' => true]);
        /** @var UserProduct[] $users */
        $quotes = $pagination->getItems();

        $quoteItems = [];

        if (!empty($quotes) && is_array($quotes)) {
            /** @var QuoteApply $quote */
            foreach ($quotes as $key => $quote) {
                $user = $quote->getUser();
                $userData = [
                    'id'         => $user->getId(),
                    'first_name' => !empty($user->getDetails()) ? $user->getDetails()->getFirstName() : '-',
                    'surname'    => !empty($user->getDetails()) ? $user->getDetails()->getLastName() : '-',
                    'birth_date' => !empty($user->getDetails()) ? $user->getDetails()->getBirthdate()->format('Y-m-d') : '-',
                    'age'        => !empty($user->getDetails()) ? (new \DateTime())->diff($user->getDetails()->getBirthdate())->format('%y') : '-',
                    'post_code'  => isset($user->getAddresses()[0]) ? $user->getAddresses()[0]->getPostalCode() : '-',
                    'mobile'     => isset($user->getCommunications()[0]) ? $user->getCommunications()[0]->getValue() : '-',
                    'city'       => isset($user->getAddresses()[0]) ? $user->getAddresses()[0]->getCity() : '-',
                    'email'      => $user->getUsername(),
                    'screenname' => !empty($user->getDetails()) ? $user->getDetails()->getScreenName() : '-',
                    'created_at' => $user->getCreatedAt(),
                    'quote_id'   => $quote->getId(),
                    'quote_status' => $quote->getStatus(),
                    'product_name' => $quote->getProduct()->getName()
                ];

                $quoteItems[] = $userData;
            }
        }

        if ($type == Product::TYPE_BORROWER && !empty($quotes) && is_array($quotes)) {
            $loanApplyManager = $this->container->get('whs.manager.loan_apply_manager');
            foreach ($quotes as $key => $quote) {
                $loanApply = $loanApplyManager->getLoanApplyByQuote($quote);
                $quoteItems[$key]['loan_apply_status'] = !empty($loanApply) ? $loanApply->getStatus() : '';
            }
        }

        return [$quoteItems, $pagination->getTotalItemCount()];
    }
}