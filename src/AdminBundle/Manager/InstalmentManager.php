<?php
namespace AdminBundle\Manager;

use AdminBundle\Entity\AdminDecision;
use AppBundle\Entity\Instalment;
use AppBundle\Entity\User;
use AdminBundle\Entity\InstalmentPV;
use AppBundle\Entity\Agreement;
use AppBundle\Entity\AgreementProcess;
use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\Transaction;
use AppBundle\Event\SystemTransactionEvent;
use AppBundle\Event\TransactionEvents;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class InstalmentManager extends BaseManager
{
    protected $dispatcher;

    /**
     * InstalmentManager constructor.
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function buildPaymentValues(Agreement $agreement, $amount)
    {
        if (!isset(InvestOffer::getRatesByTerms()[$agreement->getTerm()])) {
            throw new NotFoundHttpException('Not found offer rate by agreement term');
        }

        $balance = $this->getEntityManager()->getRepository(Transaction::class)->getBalanceByAgreement($agreement->getId());
        $offerRate = InvestOffer::getRatesByTerms()[$agreement->getTerm()];

        $paymentValues = new InstalmentPV();
        $paymentValues->setInstalment($amount);
        $paymentValues->setInterest($balance * $agreement->getApr() / 12);
        $paymentValues->setBonusInterest(
            $paymentValues->getInterest() - ($balance * ($agreement->getApr() - $agreement->getBonusRate())) / 12
        );
        $paymentValues->setBrokerInterest($paymentValues->getInterest() * $offerRate / $agreement->getApr());
        $paymentValues->setBrokerInterest(
            $paymentValues->getInterest() - $paymentValues->getBonusInterest() -  $paymentValues->getInvestorInterest()
        );
        $paymentValues->setInvestorPrinciple($paymentValues->getInstalment() - $paymentValues->getInterest());

        return  $paymentValues;
    }

    /**
     * @param InstalmentPV $instalmentPV
     * @param Agreement $agreement
     */
    public function createConfirmTransactions(InstalmentPV $instalmentPV, Agreement $agreement)
    {
        $agreement_process = new AgreementProcess();
        $agreement_process->setAgreement($agreement);
        $this->getEntityManager()->persist($agreement_process);
        $this->getEntityManager()->flush();

        $systemUser = $this->getEntityManager()->getRepository(User::class)->find(User::MAIN_TRANSACTION_USER_ID);

        $this->dispatcher->dispatch(TransactionEvents::MTHA,
            SystemTransactionEvent::create()
                ->setUser($agreement->getLoanApply()->getUser())
                ->setProcess($agreement_process)
                ->addTransaction(
                    Transaction::create()
                        ->setTransactionAmount($instalmentPV->getInstalment())
                        ->setAgreementId($agreement->getId())
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_BANK_ACCOUNT_BORROWER)
                )
                ->addTransaction(
                    Transaction::create()
                        ->setTransactionAmount(-1 * $instalmentPV->getInvestorPrinciple())
                        ->setAgreementId($agreement->getId())
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_LOAN_ACCOUNT_BORROWER)
                        ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_PRINCIPLE)
                )
                ->addTransaction(
                    Transaction::create()
                        ->setTransactionAmount(-1 * $instalmentPV->getInterest())
                        ->setAgreementId($agreement->getId())
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_LOAN_ACCOUNT_BORROWER)
                        ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_INTEREST)
                )
        );

        $agreement->setBalance($agreement->getBalance() - $instalmentPV->getInvestorPrinciple());
        $this->getEntityManager()->flush();

        $this->dispatcher->dispatch(TransactionEvents::MTHD,
            SystemTransactionEvent::create()
                ->setProcess($agreement_process)
                ->addTransaction(
                    Transaction::create()
                        ->setUser($agreement->getLoanApply()->getUser())
                        ->setTransactionAmount(-1 * $instalmentPV->getInstalment())
                        ->setAgreementId($agreement->getId())
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_BANK_ACCOUNT_BORROWER)
                )
                ->addTransaction(
                    Transaction::create()
                        ->setUser($agreement->getLoanApply()->getUser())
                        ->setTransactionAmount($instalmentPV->getBonusInterest())
                        ->setAgreementId($agreement->getId())
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_BANK_ACCOUNT_BONUS)
                        ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_PENDING)
                )
                ->addTransaction(
                    Transaction::create()
                        ->setUser($agreement->getLoanApply()->getUser())
                        ->setTransactionAmount($instalmentPV->getBrokerInterest())
                        ->setAgreementId($agreement->getId())
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_BANK_ACCOUNT_BROKER)
                )

                ->addTransaction(
                    Transaction::create()
                        ->setUser($systemUser)
                        ->setTransactionAmount(-1 * $instalmentPV->getInvestorInterest())
                        ->setAgreementId($agreement->getId())
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR)
                        ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_REINVEST)
                )
                ->addTransaction(
                    Transaction::create()
                        ->setUser($systemUser)
                        ->setTransactionAmount($instalmentPV->getInvestorInterest())
                        ->setAgreementId($agreement->getId())
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_BANK_ACCOUNT_INVESTOR)
                )
                ->addTransaction(
                    Transaction::create()
                        ->setUser($systemUser)
                        ->setTransactionAmount($instalmentPV->getInvestorInterest())
                        ->setAgreementId($agreement->getId())
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_BROKER_SUSPENSE_ACCOUNT)
                )
        );

        $offerList = $this->getEntityManager()->getRepository('AppBundle:ContractMatching')->getSumOffersByAgreement(
            $agreement
        );

        foreach ($offerList as $offer) {

            $offerPercent = $offer['summ'] / $agreement->getAmount();
            $offerObject = $this->getEntityManager()->getRepository(InvestOffer::class)->find($offer['offer']);

            $this->dispatcher->dispatch(TransactionEvents::MTHD,
                SystemTransactionEvent::create()
                    ->setUser($agreement->getLoanApply()->getUser())
                    ->setProcess($agreement_process)
                    ->addTransaction(
                        Transaction::create()
                            ->setTransactionAmount($instalmentPV->getInvestorPrinciple() * $offerPercent)
                            ->setAgreementId($agreement->getId())
                            ->setInvestmentType($offerObject->getInvestmentType())
                            ->setOfferId($offer['offer'])
                            ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_BANK_ACCOUNT_INVESTOR)
                    )
                    ->addTransaction(
                        Transaction::create()
                            ->setTransactionAmount($instalmentPV->getInvestorPrinciple() * $offerPercent)
                            ->setAgreementId($agreement->getId())
                            ->setOfferId($offer['offer'])
                            ->setInvestmentType($offerObject->getInvestmentType())
                            ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_LOAN_ACCOUNT_INVESTOR)
                            ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_PRINCIPLE)
                    )
                    ->addTransaction(
                        Transaction::create()
                            ->setTransactionAmount(-1 * ($instalmentPV->getInvestorPrinciple() * $offerPercent))
                            ->setAgreementId($agreement->getId())
                            ->setInvestmentType($offerObject->getInvestmentType())
                            ->setOfferId($offer['offer'])
                            ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR)
                            ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_REINVEST)
                    )
            );
        }
    }

    /**
     * @param Agreement $agreement
     */
    public function createInstalmentTransactions(Agreement $agreement)
    {
        $agreement_process = new AgreementProcess();
        $agreement_process->setAgreement($agreement);
        $this->getEntityManager()->persist($agreement_process);
        $this->getEntityManager()->flush();

        $systemUser = $this->getEntityManager()->getRepository(User::class)->find(User::MAIN_TRANSACTION_USER_ID);

        $instalmentPV = $this->buildPaymentValues($agreement, $agreement->getAmount());

        $this->dispatcher->dispatch(TransactionEvents::MTHA,
            SystemTransactionEvent::create()
                ->setUser($agreement->getLoanApply()->getUser())
                ->setProcess($agreement_process)
                ->addTransaction(
                    Transaction::create()
                        ->setUser($agreement->getLoanApply()->getUser())
                        ->setTransactionAmount($instalmentPV->getInterest())
                        ->setAgreementId($agreement->getId())
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_LOAN_ACCOUNT_BORROWER)
                        ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_INTEREST)

                )
                ->addTransaction(
                    Transaction::create()
                        ->setUser($agreement->getLoanApply()->getUser())
                        ->setTransactionAmount(-1 * $instalmentPV->getInterest())
                        ->setAgreementId($agreement->getId())
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_INTEREST_EXPENSE_BORROWER)
                        ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_INTEREST)
                )
                ->addTransaction(
                    Transaction::create()
                        ->setUser($agreement->getLoanApply()->getUser())
                        ->setTransactionAmount($instalmentPV->getInstalment())
                        ->setAgreementId($agreement->getId())
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_INSTALMENT_ACCOUNT_BORROWER)
                        ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_INSTALLMENT)
                )
        );


        $this->dispatcher->dispatch(TransactionEvents::MTHD,
            SystemTransactionEvent::create()
                ->setProcess($agreement_process)
                ->addTransaction(
                    Transaction::create()
                        ->setUser($agreement->getLoanApply()->getUser())
                        ->setTransactionAmount(-1 * $instalmentPV->getBonusInterest())
                        ->setAgreementId($agreement->getId())
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_BANK_ACCOUNT_BORROWER)
                        ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_PENDING)
                )
                ->addTransaction(
                    Transaction::create()
                        ->setUser($agreement->getLoanApply()->getUser())
                        ->setTransactionAmount(-1 * $instalmentPV->getBrokerInterest())
                        ->setAgreementId($agreement->getId())
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_BROKER_FEE)
                )
                ->addTransaction(
                    Transaction::create()
                        ->setUser($systemUser)
                        ->setTransactionAmount(-1 * $instalmentPV->getInvestorInterest())
                        ->setAgreementId($agreement->getId())
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_INSTALMENT_ACCOUNT_INVESTOR)
                )
        );


        $offerList = $this->getEntityManager()->getRepository('AppBundle:ContractMatching')->getSumOffersByAgreement(
            $agreement
        );

        foreach ($offerList as $offer) {

            $offerPercent = $offer['summ'] / $agreement->getAmount();
            $offerObject = $this->getEntityManager()->getRepository(InvestOffer::class)->find($offer['offer']);

            $this->dispatcher->dispatch(TransactionEvents::MTHD,
                SystemTransactionEvent::create()
                    ->setUser($agreement->getLoanApply()->getUser())
                    ->setProcess($agreement_process)
                    ->addTransaction(
                        Transaction::create()
                            ->setTransactionAmount(-1 * $instalmentPV->getInvestorPrinciple() * $offerPercent)
                            ->setAgreementId($agreement->getId())
                            ->setOfferId($offer['offer'])
                            ->setInvestmentType($offerObject->getInvestmentType())
                            ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_INSTALMENT_ACCOUNT_INVESTOR)
                    )
            );
        }

    }

    public function updateInstalmentRecords(Agreement $agreement, $paymentAmount)
    {
        $instalmentRepository = $this->getEntityManager()->getRepository(Instalment::class);

        /**
         * @var Instalment $lastInstalment
         */
        $lastInstalment = $instalmentRepository->getLastInstalmentByLoanApply($agreement->getLoanApply());
        $lastInstalment->setPaymentAmount($lastInstalment->getPaymentAmount() + $paymentAmount);

        $instalmentList = $instalmentRepository->getInstalmentsByLoanApply($agreement->getLoanApply());

        $this->getEntityManager()->flush();

        /**
         * @var Instalment $instalment
         */
        foreach ($instalmentList as $instalment) {

            if ($paymentAmount < $instalment->getOutstanding()) {
                $instalment->setAllocation($paymentAmount);
                $instalment->setOutstanding($instalment->getOutstanding() - $paymentAmount);
                $instalment->setInstallmentStatus(Instalment::INSTALLMENT_STATUS_PART_CLEARED);
                $paymentAmount = 0;
            } elseif ($paymentAmount == $instalment->getOutstanding()) {
                $instalment->setAllocation($instalment->getInstalment());
                $instalment->setOutstanding(0);
                $instalment->setInstallmentStatus(Instalment::INSTALLMENT_STATUS_CLEARED);

                if ($instalment->getPaymentStatus() == Instalment::PAYMENT_STATUS_PENDING) {
                    $instalment->getPaymentStatus(Instalment::PAYMENT_STATUS_PAID);
                }

                $paymentAmount = 0;
            } elseif ($paymentAmount > $instalment->getOutstanding()) {
                $instalment->setAllocation($instalment->getInstalment());
                $instalment->setOutstanding(0);
                $instalment->setInstallmentStatus(Instalment::INSTALLMENT_STATUS_CLEARED);

                if ($instalment->getPaymentStatus() == Instalment::PAYMENT_STATUS_PENDING) {
                    $instalment->getPaymentStatus(Instalment::PAYMENT_STATUS_PAID);
                }

                $paymentAmount = $paymentAmount - $instalment->getOutstanding();
            }

            if ($paymentAmount == 0) break;
        }

        if ($paymentAmount > 0) {
            $lastInstalment->setAllocation($lastInstalment->getAllocation() + $paymentAmount);
            $lastInstalment->setOutstanding($lastInstalment->getOutstanding() - $paymentAmount);
            $lastInstalment->setInstallmentStatus(Instalment::INSTALLMENT_STATUS_OVERPAID);
            $lastInstalment->setPaymentStatus(Instalment::PAYMENT_STATUS_PAID);
            $lastInstalment->setPrinciple($lastInstalment->getPrinciple() + $paymentAmount - $lastInstalment->getInterest());
            $this->getEntityManager()->flush();
        }
    }
}