<?php
namespace AdminBundle\Manager;


use AdminBundle\Response\OfferDetails;
use AppBundle\Entity\Agreement;
use AppBundle\Entity\ContractMatching;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\QuoteApply;
use AppBundle\Repository\ContractMatchingRepository;
use AppBundle\Utils\Payment\PaymentsComputerService;
use Doctrine\ORM\EntityManager;

class AdminBorrowerManager extends BaseManager
{
    const ACCEPTANCE_PENDING = 'Pending';
    const ACCEPTANCE_ACCEPTED = 'Accepted';
    const ACCEPTANCE_DECLINED = 'Declined';

    const OFFER_DETAILS_PENDING = 'Pending';
    const OFFER_DETAILS_ACTIVE = 'Active';
    const OFFER_DETAILS_SETTLED = 'Settled';

    /**
     * @var PaymentsComputerService
     */
    protected $paymentsComputer;

    public function __construct(PaymentsComputerService $paymentsComputer)
    {
        $this->paymentsComputer = $paymentsComputer;
    }


    /**
     * @param Agreement $agreement
     * @return array
     */
    public function getAgreementMatchedDetails(Agreement $agreement)
    {
        $contractMatching = $this->getEntityManager()->getRepository(ContractMatching::class);

        $sum = $contractMatching->getMatchedSumByAgreement($agreement);


        if ($agreement->getAmount() > 0) {
            $percent = $sum / (float)$agreement->getAmount() * 100;
        } else {
            $percent = 0;
        }

        $rows = $contractMatching->getMatchedLenderSumByAgreement($agreement);

        return ['matching' => round($percent, 2) , 'investors' => $rows];
    }

    /**
     * @param Agreement $agreement
     * @return array
     */
    public function getAcceptanceStatus(Agreement $agreement)
    {
        $status = '';

        if ($agreement->getLoanApply()->getStatus() == LoanApply::STATUS_ACCEPTANCE
            && !$agreement->getAcceptanceDate()) {
            $status = self::ACCEPTANCE_PENDING;
        }

        if ($agreement->getLoanApply()->getStatus() != LoanApply::STATUS_ACCEPTANCE
            && $agreement->getAcceptanceDate()) {
            $status = self::ACCEPTANCE_ACCEPTED;
        }

        if ($agreement->getLoanApply()->getStatus() == LoanApply::STATUS_DECLINED) {
            $status = self::ACCEPTANCE_DECLINED;
        }

        return ['status' => $status , 'date' => $agreement->getAcceptanceDate()];
    }

    /**
     * @param QuoteApply $quoteApply
     * @return array
     */
    public function getOfferDetailsList(QuoteApply $quoteApply)
    {
        /**
         * @var $contractMatching ContractMatchingRepository
         */
        $contractMatching = $this->getEntityManager()->getRepository(ContractMatching::class);

        $offerList = $this->getEntityManager()->getRepository(InvestOffer::class)->findBy(['user' => $quoteApply->getUser()]);

        $responseList = [];
        /**
         * @var $offer InvestOffer
         */
        foreach ($offerList as $offer) {

            $offerDetails = new OfferDetails();
            $offerDetails->setOfferId($offer->getId());
            $offerDetails->setName($offer->getInvestmentName());

            $offerDetails->setAmount(
                $offer->getInvestmentAmount()
            );

            $offerDetails->setPending(
                $contractMatching->getSumByOfferStatus( $offer,ContractMatching::STATUS_PENDING)
            );
            $offerDetails->setActive(
                $contractMatching->getSumByOfferStatus( $offer,ContractMatching::STATUS_ACTIVE)
            );
            $offerDetails->setSettled(
                $contractMatching->getSumByOfferStatus( $offer,ContractMatching::STATUS_SETTLED)
            );

            $agreementList = $contractMatching->getMatchedAgreementsByOffer($offer);

            foreach ($agreementList as $agreement) {

                $int_new = $this->getInterestNew(
                    $agreement['sum_amount'],
                    $agreement['term'],
                    InvestOffer::getRatesByTerms()[$offer->getTerm()]
                );
                $agreement['sum_amount'] = (float)$agreement['sum_amount'];
                $agreement['apr'] = (float)$agreement['apr'];
                $agreement['interest'] = $int_new['interest'];
                $agreement['rate'] = (float)InvestOffer::getRatesByTerms()[$offer->getTerm()];

                $offerDetails->addAgreement($agreement);
            }

            array_push($responseList, $offerDetails);
        }

        return $responseList;
    }

    /**
     * @param $status
     * @return string
     */
    protected function prepareOfferDetailsStatus($status)
    {
        switch ($status){
            case LoanApply::STATUS_MATCHING:
            case LoanApply::STATUS_FULLY_MATCHED:
            case LoanApply::STATUS_SIGNED:
                $result = self::OFFER_DETAILS_PENDING;
                break;
            case LoanApply::STATUS_ACTIVE:
                $result = self::OFFER_DETAILS_ACTIVE;
                break;
            case LoanApply::STATUS_SETTLED:
                $result = self::OFFER_DETAILS_SETTLED;
                break;
            default:
                $result = '?';
        }

        return $result;
    }

    protected function getInterest($loanAmount, $loanTerm, $loanApr)
    {
        $creditPayments = $this->paymentsComputer->getCreditPaymentsTotal($loanApr, $loanTerm, $loanAmount);
        return round($creditPayments['totalPaid'] - $loanAmount, 2);
    }

    /**
     * @param $amount
     * @param $term
     * @param $percent
     * @return array
     */
    public function getInterestNew($amount, $term, $percent)
    {
        $percent = $percent / 100;

        $fullYear = floor($term / 12);
        $remainingMonth = $term - $fullYear * 12;

        $capitalInterest = $amount * pow((1+$percent), $fullYear);
        $interest = $capitalInterest * ($percent/12) * $remainingMonth;

        $capital = $capitalInterest + $interest;
        $int = $capital - $amount;

        return ['capital' => round($capital, 2), 'interest' => round($int, 2)];
    }
}