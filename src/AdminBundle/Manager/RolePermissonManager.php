<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 14.02.17
 * Time: 14:31
 */

namespace AdminBundle\Manager;


use AdminBundle\Entity\RolePermission;
use AdminBundle\Entity\User;
use AdminBundle\Exception\BadPermissionException;
use AdminBundle\Security\Permission\MaskBuilder;

/**
 * Class RolePermissonManager
 * @package AdminBundle\Manager
 */
class RolePermissonManager extends BaseManager
{

    /**
     * Returns permissions for the User by users roles and masks in RolePermission
     *
     * @param User $user
     * @return array
     * @throws BadPermissionException
     */
    public function getPermissionsForUser(User $user)
    {
        $result = [];

        $rolePermissions = $this->getEntityManager()->getRepository(RolePermission::class)->getRolesPermissionsForUser($user);
        if ($rolePermissions) {
            foreach ($rolePermissions as $rolePermission) {
                if (!in_array($rolePermission->getPermission(), $this->getPermissionNames())) {
                    throw new BadPermissionException();
                }

                if (($rolePermission->getMask() & MaskBuilder::MASK_VIEW) == MaskBuilder::MASK_VIEW) {
                    $result[] = 'PERMISSION_' . strtoupper($rolePermission->getPermission()) . '_VIEW';
                }

                if (($rolePermission->getMask() & MaskBuilder::MASK_CREATE) == MaskBuilder::MASK_CREATE) {
                    $result[] = 'PERMISSION_' . strtoupper($rolePermission->getPermission()) . '_CREATE';
                }

                if (($rolePermission->getMask() & MaskBuilder::MASK_UPDATE) == MaskBuilder::MASK_UPDATE) {
                    $result[] = 'PERMISSION_' . strtoupper($rolePermission->getPermission()) . '_UPDATE';
                }

                if (($rolePermission->getMask() & MaskBuilder::MASK_DELETE) == MaskBuilder::MASK_DELETE) {
                    $result[] = 'PERMISSION_' . strtoupper($rolePermission->getPermission()) . '_DELETE';
                }
            }
        }

        return array_unique($result);
    }

    /**
     * Returns permissions list from the admin_role_permission.yml file in array with permissions names as keys
     *
     * @return array
     */
    public function getPermissions()
    {
        $result = [];

        foreach ($this->container->getParameter('admin.permissions') as $item) {
            $result[$item['name']] = $item;
        }

        return $result;
    }

    /**
     * Returns permissions names only
     *
     * @return array
     */
    public function getPermissionNames()
    {
        return array_keys($this->getPermissions());
    }

    /**
     * Returns all RolePermissions for a role. If the role is null method returns all RolePermissions for the all roles.
     *
     * @param string $role
     *
     * @return RolePermission[]
     */
    public function findAllForRole($role = null)
    {
        $repository = $this->container->get('doctrine.orm.entity_manager')->getRepository(RolePermission::class);
        if ($role) {
            $result = $repository->findBy(['role' => $role]);
        } else {
            $result = $repository->findAll();
        }

        return $result;
    }

    /**
     * Check if a RolePermission exists for a role and a permission
     *
     * @param $role
     * @param $permission
     * @return bool
     */
    public function isExist($role, $permission)
    {
        return (boolean) $this->getEntityManager()->getRepository(RolePermission::class)->findOneBy(['role' => $role, 'permission' => $permission]);
    }

    /**
     * Returns a list of permission groups for user permissions which not 0
     *
     * @param User $user
     * @return array
     */
    public function getNotDeniedPermissionsGroups(User $user)
    {
        $result = [];

        $rolePermissions = $this->getEntityManager()->getRepository(RolePermission::class)->getRolesPermissionsForUser($user);
        if ($rolePermissions) {
            foreach ($rolePermissions as $rolePermission) {
                if ($rolePermission->getMask() > 0) {
                    $result = array_merge($result, $this->getPermissions()[$rolePermission->getPermission()]['groups']);
                }
            }
        }

        return array_values(array_unique($result));
    }
}