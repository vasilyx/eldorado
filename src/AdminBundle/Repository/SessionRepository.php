<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 09.02.17
 * Time: 15:12
 */

namespace AdminBundle\Repository;


use AdminBundle\Entity\Session;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class SessionRepository extends EntityRepository
{
    /**
     * @param $token
     * @param Criteria|null $criteria
     * @return Session|null
     */
    public function findOneByToken($token, Criteria $criteria = null)
    {
        $qb = $this->createQueryBuilder('session');

        $qb
            ->where('session.token = :token')
            ->setParameter('token', $token)
            ;

        if ($criteria) {
            $this->addCriteria($qb, $criteria);
        }

        $result = $qb->getQuery()->getOneOrNullResult();

        return $result;
    }

    protected function addCriteria(QueryBuilder $queryBuilder, Criteria $criteria)
    {
        if ($criteria) {
            $queryBuilder->addCriteria($criteria);
        }
    }
}