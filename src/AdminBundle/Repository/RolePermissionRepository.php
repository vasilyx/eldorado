<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 14.02.17
 * Time: 14:38
 */

namespace AdminBundle\Repository;


use AdminBundle\Entity\RolePermission;
use AdminBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class RolePermissionRepository extends EntityRepository
{
    /**
     * @param User $user
     * @return RolePermission[]
     */
    public function getRolesPermissionsForUser(User $user)
    {
        $qb = $this->createQueryBuilder('role_permission_repository');

        $qb
            ->where('role_permission_repository.role IN(:roles)')
            ->setParameter('roles', $user->getRoles())
        ;

        $result = $qb->getQuery()->getResult();

        return $result;
    }

}