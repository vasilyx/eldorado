<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 07.02.17
 * Time: 17:00
 */

namespace AdminBundle\Tests;


use AdminBundle\Entity\Session;
use AdminBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

abstract class AbstractAdminTest extends WebTestCase
{
    /**
     * @var Client
     */
    protected static $client;

    /**
     * @var EntityManager
     */
    protected static $em;

    /**
     * @var string
     */
    protected static $username;

    /**
     * We need token in different tests
     * @var string
     */
    protected static $token;

    /**
     * @var array The server parameters (HTTP headers are referenced with a HTTP_ prefix as PHP does)
     */
    protected $server = [];

    public static function setUpBeforeClass()
    {
        static::$client = static::createClient();

        static::$username = static::generateUserName();

        $user = new User();

        $user
            ->setUsername(static::$username)
            ->setPlainPassword('1234567Q1234567Q&')
            ->addRole('ROLE_SUPER_ADMIN')
            ;

        static::$client->getContainer()->get('whadmin.user_manager')->updateUser($user);
    }

    public function setUp()
    {
        static::$em = static::$client->getContainer()->get('doctrine.orm.entity_manager');
    }

    public function tearDown()
    {
        static::$em->close();
    }

    public static function tearDownAfterClass()
    {
    }

    /**
     * Sends request with preset server headers
     *
     * @param string $method        The request method
     * @param string $uri           The URI to fetch
     * @param array  $parameters    The Request parameters
     * @param array $content        The raw body data as array
     * @param array  $server        The server parameters (HTTP headers are referenced with a HTTP_ prefix as PHP does)
     *
     * @return Crawler
     */
    protected function send($method, $uri, $parameters = [], $content = [], $server = [])
    {
        /**
         * If authorization header is not defined set Bearer token
         */
        if (!array_key_exists('HTTP_AUTHORIZATION', $server)) {
            $server['HTTP_AUTHORIZATION'] = 'Bearer ' . static::$token;
        }

        $server = array_merge($server, $this->server);

        $server['HTTP_CONTENT_TYPE'] = 'application/json';

        return static::$client->request($method, $uri, $parameters, [], $server, json_encode($content));
    }

    /**
     * Returns response as array
     *
     * @return array
     */
    protected function getResponseAsArray()
    {
        $response = json_decode(static::$client->getResponse()->getContent(), true);

        return $response;
    }

    /**
     * Returns response as object
     *
     * @return mixed
     */
    protected function getResponseAsObject()
    {
        $response = json_decode(static::$client->getResponse()->getContent());

        return $response;
    }

    /**
     * This method may be called before login
     *
     * @return User|null|object
     */
    protected function getUserByUsername()
    {
        return static::$em->getRepository(User::class)->findOneBy(['username' => static::$username]);
    }

    /**
     * This method may be called only after login
     *
     * @return User
     */
    protected function getUserByToken()
    {
        $session = static::$em->getRepository(Session::class)->findOneBy(['token' => static::$token]);

        return $session->getUser();
    }

    protected static function generateUserName()
    {
        return rand(1, 1000) . time() . '@wh.money';
    }

    /**
     * Method should be used to set token for user with static::$username
     */
    protected function setToken()
    {
        $this->send('GET', '/whadmin/token', [], [], ['HTTP_AUTHORIZATION' => 'BASIC ' . base64_encode(static::$username . ':' . '1234567Q1234567Q&')]);

        static::$token = $this->getResponseAsObject()->content->token;
    }
}