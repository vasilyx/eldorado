<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 07.02.17
 * Time: 17:00
 */

namespace AdminBundle\Tests\Controller;


use AdminBundle\Entity\Session;
use AdminBundle\Entity\User;
use AdminBundle\Tests\AbstractAdminTest;
use Symfony\Bundle\SwiftmailerBundle\DataCollector\MessageDataCollector;
use Symfony\Component\HttpFoundation\Response;

class UserAdminApiControllerTest extends AbstractAdminTest
{
    public function testGetTokenAction()
    {
        /**
         * Check the error on no credentials request
         */
        $this->send('GET', '/whadmin/token', [], [], []);

        $this->assertEquals(static::$client->getResponse()->getStatusCode(), Response::HTTP_UNAUTHORIZED);
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->code, 'admin.token.badCredentials');

        /**
         * Check the error on null authorization header
         */
        $this->send('GET', '/whadmin/token', [], [], ['HTTP_AUTHORIZATION' => null]);

        $this->assertEquals(static::$client->getResponse()->getStatusCode(), Response::HTTP_UNAUTHORIZED);
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->code, 'admin.token.badCredentials');

        /**
         * Check the error on a nonexistent username
         */
        $this->send('GET', '/whadmin/token', [], [], ['HTTP_AUTHORIZATION' => 'BASIC ' . base64_encode('BadUsername' . ':' . '1234567Q')]);

        $this->assertEquals(static::$client->getResponse()->getStatusCode(), Response::HTTP_UNAUTHORIZED);
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->code, 'admin.token.badCredentials');

        /**
         * Check the error on a bad password
         */
        $this->send('GET', '/whadmin/token', [], [], ['HTTP_AUTHORIZATION' => 'BASIC ' . base64_encode(static::$username . ':' . '1234567')]);

        $this->assertEquals(static::$client->getResponse()->getStatusCode(), Response::HTTP_UNAUTHORIZED);
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->code, 'admin.token.badCredentials');

        /**
         * Check the valid request
         */
        $this->send('GET', '/whadmin/token', [], [], ['HTTP_AUTHORIZATION' => 'BASIC ' . base64_encode(static::$username . ':' . '1234567Q1234567Q&')]);

        $this->assertTrue(static::$client->getResponse()->isOk());
        $this->assertNotNull($this->getResponseAsObject()->content->token);

        /**
         * Sets the token to static property. Now we can get the token in the other tests
         */
        static::$token = $this->getResponseAsObject()->content->token;
    }

    public function testPostUsersAction()
    {
        $username = static::generateUserName();

        /**
         * Sleep to check that updatedAt for session is updated
         */
        sleep(1);

        /**
         * There are two mandatory parameters: username and plain_password
         */
        $this->send('POST', '/whadmin/users', [], [
            'username' => $username,
            'plain_password' => '1234567Q1234567Q&',
            'roles' => ['ROLE_REDACTOR']
        ]);

        $this->assertTrue(static::$client->getResponse()->isOk());
        $this->assertEquals($this->getResponseAsObject()->status, 'success');

        /**
         * Check that updatedAt for session is updated
         */
        $session = static::$kernel->getContainer()->get('whadmin.session_manager')->findOneByToken(static::$token);
        $this->assertGreaterThanOrEqual(1, $session->getUpdatedAt()->getTimestamp() - $session->getCreatedAt()->getTimestamp());

        /**
         * Check that id is present in the response
         */
        $this->assertNotNull($this->getResponseAsObject()->content->id);
        /**
         * Check that username is present in the response
         */
        $this->assertEquals($this->getResponseAsObject()->content->username, $username);

        /**
         * Check that the default role ROLE_ADMIN is present in the response
         */
        $this->assertTrue(in_array('ROLE_ADMIN', $this->getResponseAsObject()->content->roles));

        /**
         * Check that the role from request ROLE_REDACTOR is present in the response
         */
        $this->assertTrue(in_array('ROLE_REDACTOR', $this->getResponseAsObject()->content->roles));

        /**
         * Check that user has been written to the database
         */
        $userFromDatabase = static::$em->getRepository(User::class)->findOneBy(['username' => $username]);
        $this->assertNotNull($userFromDatabase);

        /**
         * Check that there is correct password in the database
         */
        $this->assertEquals($userFromDatabase->getPassword(), static::$client->getContainer()->get('whadmin.user_manager')->encodePassword($userFromDatabase, '1234567Q1234567Q&'));

        /**
         * Check roles
         */
        $this->assertTrue($userFromDatabase->hasRole('ROLE_ADMIN'));
        $this->assertTrue($userFromDatabase->hasRole('ROLE_REDACTOR'));


        /**
         * Username should be unique. This is bad request, because the same username is already used
         */
        $this->send('POST', '/whadmin/users', [], [
            'username' => $username,
            'plain_password' => '1234567Q',
        ]);

        /**
         * Check that error response is correct
         */
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->username->code, 'admin.user.username.notUnique');

        /**
         * Check that non super admin can not create users
         */
        $this->send('GET', '/whadmin/token', [], [], ['HTTP_AUTHORIZATION' => 'BASIC ' . base64_encode($username . ':' . '1234567Q1234567Q&')]);

        $this->assertTrue(static::$client->getResponse()->isOk());
        $token = $this->getResponseAsObject()->content->token;

        /**
         * We should to set authorization token for the user with ROLE_REDACTOR role, non super admin
         */
        $this->send('POST', '/whadmin/users', [], [
            'username' => 'ThisUserWillNotBeCreated',
            'plain_password' => '1234567Q'
        ], ['HTTP_AUTHORIZATION' => 'BEARER ' . $token]);

        $this->assertTrue(static::$client->getResponse()->isForbidden());
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->code, 'admin.users.accessDenied');


        /**
         * Check that the posting of a username which does not contain @wh.money returns error
         */
        $this->send('POST', '/whadmin/users', [], [
            'username' => 'badUserName@nowh.money',
            'plain_password' => '1234567Q',
            'roles' => ['ROLE_REDACTOR']
        ]);

        $this->assertTrue(static::$client->getResponse()->isClientError());
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->username->code, 'admin.user.username.badFormat');

        /**
         * Check that the posting of a password with length < 12 returns error
         */
        $this->send('POST', '/whadmin/users', [], [
            'username' => $username,
            'plain_password' => '1234567Q#',
            'roles' => ['ROLE_REDACTOR']
        ]);

        $this->assertTrue(static::$client->getResponse()->isClientError());
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->plainPassword->code, 'admin.user.plainPassword.badLength');

        /**
         * Check that the posting of a password with no letters returns error
         */
        $this->send('POST', '/whadmin/users', [], [
            'username' => $username,
            'plain_password' => '1234567123456$',
            'roles' => ['ROLE_REDACTOR']
        ]);

        $this->assertTrue(static::$client->getResponse()->isClientError());
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->plainPassword->code, 'admin.user.plainPassword.noLetters');

        /**
         * Check that the posting of a password with no numbers returns error
         */
        $this->send('POST', '/whadmin/users', [], [
            'username' => $username,
            'plain_password' => 'sdfASDFsdfsdfsdfs#',
            'roles' => ['ROLE_REDACTOR']
        ]);

        $this->assertTrue(static::$client->getResponse()->isClientError());
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->plainPassword->code, 'admin.user.plainPassword.noNumbers');

        /**
         * Check that the posting of a password with no special characters returns error
         */
        $this->send('POST', '/whadmin/users', [], [
            'username' => $username,
            'plain_password' => 'sdfASDFsdfsdfsdfs12312',
            'roles' => ['ROLE_REDACTOR']
        ]);

        $this->assertTrue(static::$client->getResponse()->isClientError());
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->plainPassword->code, 'admin.user.plainPassword.noSpecial');
    }

    public function testGetUsersAction()
    {
        /**
         * Get permissions for role SUPER_ADMIN
         */
        $this->send('GET', '/whadmin/permissions', ['role' => 'ROLE_SUPER_ADMIN']);

        $permissions = $this->getResponseAsArray()['content'];

        /**
         * Patch role-permission for the USER permission now we CAN NOT view users
         */
        foreach ($permissions as $permission) {
            if ('USER' === $permission['permission']) {
                $this->send('PATCH', '/whadmin/permission', [], ['id' => $permission['id'], 'mask' => 'n']);
            }
        }
        /**
         * Check the bad request
         */
        $this->send('GET', '/whadmin/users');

        /**
         * We can not view users
         */
        $this->assertTrue(static::$client->getResponse()->isForbidden());
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->code, 'admin.users.accessDenied');

        /**
         * Patch role-permission for the USER permission now we CAN view users
         */
        foreach ($permissions as $permission) {
            if ('USER' === $permission['permission']) {
                $this->send('PATCH', '/whadmin/permission', [], ['id' => $permission['id'], 'mask' => 'crud']);
            }
        }
        /**
         * Check the valid request
         */
        $this->send('GET', '/whadmin/users');

        $this->assertTrue(static::$client->getResponse()->isOk());

        $users = $this->getResponseAsArray()['content'];
        $this->assertNotEmpty($users);

        $currentUser = $this->getUserByToken();

        $this->assertNotNull($users[$currentUser->getId()]['username']);
        $this->assertTrue(in_array('PERMISSION_USER_VIEW', $users[$currentUser->getId()]['permissions']));
        $this->assertTrue($users[$currentUser->getId()]['enabled']);
    }

    public function testPutUserPasswordExpiredAction()
    {
        /**
         * Check valid request
         */
        $this->send('PUT', '/whadmin/user/password', [], ['plain_password' => '7654321Q7654321Q%', 'repeat_password' => '7654321Q7654321Q%'], ['HTTP_AUTHORIZATION' => 'Basic ' . base64_encode(static::$username . ':' . '1234567Q1234567Q&')]);

        $this->assertTrue(static::$client->getResponse()->isOk());

        /**
         * Check bad request in wich password and repeat password are not match
         */
        $this->send('PUT', '/whadmin/user/password', [], ['plain_password' => '123456', 'repeat_password' => '123'], ['HTTP_AUTHORIZATION' => 'Basic ' . base64_encode(static::$username . ':' . '1234567Q1234567Q&')]);

        $this->assertTrue(static::$client->getResponse()->isClientError());
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->code, 'admin.password.notMatch');

        /**
         * Check bac request with blank new plain password
         */

        $this->send('PUT', '/whadmin/user/password', [], [], ['HTTP_AUTHORIZATION' => 'Basic ' . base64_encode(static::$username . ':' . '7654321Q7654321Q%')]);

        $this->assertTrue(static::$client->getResponse()->isClientError());
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->plainPassword->code, 'admin.user.password.isBlank');
    }

    public function testPutUserForgotten_password_tokenAction()
    {
        static::$client->enableProfiler();
        $url = '/some/url';
        $this->send('PUT', '/whadmin/user/forgotten_password_token', [], ['url' => $url], ['HTTP_AUTHORIZATION' => 'Basic ' . base64_encode(static::$username . ':' . '')]);

        $this->assertTrue(static::$client->getResponse()->isOk());

        /**
         * Check that a letter was sent to the user
         */
        /**
         * @var MessageDataCollector $messageDataCollector
         */
        $messageDataCollector = static::$client->getProfile()->getCollector('swiftmailer');
        $this->assertGreaterThan(0, $messageDataCollector->getMessageCount());
        /**
         * @var \Swift_Message $message
         */
        $message = $messageDataCollector->getMessages()[0];
        $user = static::$em->getRepository(User::class)->findOneBy(['username' => static::$username]);
        /**
         * Check that token is generated and saved to user
         */
        $this->assertNotNull($user->getForgottenPasswordToken());

        /**
         * Check message content
         */
        $this->assertEquals($message->getBody(), static::$client->getRequest()->getSchemeAndHttpHost() . $url . '?t=' . $user->getForgottenPasswordToken());
        $this->assertEquals(array_keys($message->getTo())[0], $user->getUsername());
        $this->assertEquals(array_keys($message->getFrom())[0], static::$kernel->getContainer()->getParameter('mailer_support'));

        return $user->getForgottenPasswordToken();
    }

    /**
     * @depends testPutUserForgotten_password_tokenAction
     * @param string $token
     */
    public function testPutUserPasswordResetAction($token)
    {
        /**
         * Check that user has forgottenPasswordToken
         */
        $user = static::$em->getRepository(User::class)->findOneBy(['forgottenPasswordToken' => $token]);
        $this->assertNotNull($user->getForgottenPasswordToken());
        static::$em->clear();

        /**
         * Change password by forgottenPasswordToken
         */
        $this->send('PUT', '/whadmin/user/password', [], ['plain_password' => '7654321Q7654321Q*', 'repeat_password' => '7654321Q7654321Q*'], ['HTTP_AUTHORIZATION' => 'Forgotten ' . $token]);

        $this->assertTrue(static::$client->getResponse()->isOk());
        $user = static::$em->getRepository(User::class)->findOneBy(['username' => $user->getUsername()]);

        /**
         * Check that after password updating the forgottenPasswordToken is null
         */
        $this->assertNull($user->getForgottenPasswordToken());

        /**
         * Check that the password is changed by get token for example. New password is used.
         */

        $this->send('GET', '/whadmin/token', [], [], ['HTTP_AUTHORIZATION' => 'Basic ' . base64_encode($user->getUsername() . ':' . '7654321Q7654321Q*')]);

        $this->assertTrue(static::$client->getResponse()->isOk());

        /**
         * Check error with bad token
         */
        $this->send('PUT', '/whadmin/user/password', [], ['plain_password' => '7654321Q7654321Q*', 'repeat_password' => '7654321Q7654321Q*'], ['HTTP_AUTHORIZATION' => 'Forgotten ' . 'SomethingNotValid']);

        $this->assertTrue(static::$client->getResponse()->isClientError());
        $this->assertEquals(static::$client->getResponse()->getStatusCode(), Response::HTTP_UNAUTHORIZED);
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->code, 'admin.authentication.badCredentials');
    }
}