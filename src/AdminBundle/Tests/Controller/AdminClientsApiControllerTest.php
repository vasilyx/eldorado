<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 17.02.17
 * Time: 17:00
 */

namespace AdminBundle\Tests\Controller;


use AdminBundle\Entity\User;
use AdminBundle\Tests\AbstractAdminTest;
use AppBundle\Entity\Communication;
use AppBundle\Entity\User as Client;
use AppBundle\Entity\UserAddress;
use AppBundle\Entity\UserDependent;
use AppBundle\Entity\UserDetails;
use LogBundle\Entity\AdminActivityLog;

class AdminClientsApiControllerTest extends AbstractAdminTest
{
    /**
     * @return \AppBundle\Entity\User[]
     */
    private function createTestClients()
    {
        //add clients
        $plainPassword = '1Password';
        $clientNames = [
            'Vladimir' . uniqid() . '@example.com',
            'Ivan' . uniqid() . '@example.com',
            'oleg' . uniqid() . '@example.com',
            'konstantin' . uniqid() . '@example.com',
        ];
        $container = static::$client->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');
        $userManager = $container->get('whs.manager.user_manager');
        /** @var Client[] $clients */
        $clients = [];
        foreach ($clientNames as $clientName) {
            $client = new Client();
            $client->setUsername($clientName)->setPassword($plainPassword);
            $userManager->registerUser($client);
            $clients[] = $client;
        }

        //add two addresses to first client
        $movedDate = (new \DateTime())->sub((new \DateInterval('P1Y')));
        $address = (new UserAddress())->setPostalCode('SW15 2NQ')->setCountry('GB')->setCity('London')->setLine1('line1')->setMoved($movedDate);
        $address->setUser($clients[0]);
        $em->persist($address);

        $movedDate = (new \DateTime())->sub((new \DateInterval('P5Y')));
        $address = (new UserAddress())->setPostalCode('SW15 2NQ')->setCountry('GB')->setCity('Manchester')->setLine1('line2')->setMoved($movedDate);
        $address->setUser($clients[0]);
        $em->persist($address);


        //add communications to second client
        $communicationManager = $container->get('whs.manager.user_communication_manager');
        $communicationManager->createUserCommunication($clients[1], Communication::TYPE_SKYPE, 'skype_login');
        $communicationManager->createUserCommunication($clients[1], Communication::TYPE_MOBILE_PHONE, '123123123');
        $communicationManager->createUserCommunication($clients[1], Communication::TYPE_HOME_PHONE, '321321321');
        $communicationManager->createUserCommunication($clients[1], Communication::TYPE_WORK_PHONE, '987987987');
        $communicationManager->createUserCommunication($clients[1], Communication::TYPE_LINKED_IN, 'linkedin_login');
        $communicationManager->createUserCommunication($clients[1], Communication::TYPE_FACEBOOK, 'facebook_login');
        $communicationManager->createUserCommunication($clients[1], Communication::TYPE_EMAIL, 'user' . uniqid() . '@example.com');

        //add details to third user
        $birthDate = (new \DateTime())->sub((new \DateInterval('P20Y')));
        $details = (new UserDetails())
            ->setBirthdate($birthDate)
            ->setFirstName('FirstName')
            ->setGender('m')
            ->setLastName('LastName')
            ->setMaidenName('MaidenName')
            ->setScreenName('ScreenName')
            ->setTitle('Mr')
            ->setUser($clients[2]);
        $em->persist($details);
        //also add dependents to third client
        $depBirthDate = (new \DateTime())->sub((new \DateInterval('P3Y')));
        $dependent = (new UserDependent())->setFirstName('DepFirstName')->setBirthdate($depBirthDate)->setUser($clients[2]);
        $em->persist($dependent);


        $em->flush();

        return $clients;
    }

    public function testClientsSearchAction()
    {
        $this->setToken();
        $this->send('GET', '/whadmin/clients/search');
        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
        $responseArr = $this->getResponseAsArray();
        $this->assertArrayHasKey('content', $responseArr);
        $this->assertArrayHasKey('clients', $responseArr['content']);
        $this->assertArrayHasKey('count', $responseArr['content']);
        $this->assertEquals('success', $this->getResponseAsArray()['status']);
    }

    public function testGetClientPersonalInfoActions()
    {
        $clients = $this->createTestClients();

        $this->setToken();

//      TEST GET client addresses route
        $this->send('GET', '/whadmin/clients/' . $clients[0]->getId() . '/addresses');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $responseArr = $this->getResponseAsArray();
        $this->assertArrayHasKey('content', $responseArr);
        $this->assertEquals('success', $this->getResponseAsArray()['status']);
        $this->assertEquals(2, count($responseArr['content']));

        //test response structure
        $this->assertArrayHasKey('id', $responseArr['content'][0]);
        $this->assertArrayHasKey('postal_code', $responseArr['content'][0]);
        $this->assertArrayHasKey('city', $responseArr['content'][0]);
        $this->assertArrayHasKey('line1', $responseArr['content'][0]);
        $this->assertArrayHasKey('country', $responseArr['content'][0]);
        $this->assertArrayHasKey('moved', $responseArr['content'][0]);

        //test response values
        $this->assertEquals($clients[0]->getAddresses()[1]->getId(), $responseArr['content'][1]['id']);
        $this->assertEquals($clients[0]->getAddresses()[1]->getPostalCode(), $responseArr['content'][1]['postal_code']);
        $this->assertEquals($clients[0]->getAddresses()[1]->getCity(), $responseArr['content'][1]['city']);
        $this->assertEquals($clients[0]->getAddresses()[1]->getLine1(), $responseArr['content'][1]['line1']);
        $this->assertEquals($clients[0]->getAddresses()[1]->getCountry(), $responseArr['content'][1]['country']);
        $this->assertEquals($clients[0]->getAddresses()[1]->getMoved()->format('Ymd'), $responseArr['content'][1]['moved']);

//      TEST GET client communications route
        $this->send('GET', '/whadmin/clients/' . $clients[1]->getId() . '/communications');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $responseArr = $this->getResponseAsArray();
        $communicationManager = static::$client->getContainer()->get('whs.manager.user_communication_manager');
        $communicationsList = $communicationManager->getCommunicationList($clients[1]);
        $this->assertArrayHasKey('content', $responseArr);
        $this->assertEquals('success', $this->getResponseAsArray()['status']);
        $this->assertEquals($communicationsList, $responseArr['content']);
        $this->assertEquals(Communication::TYPE_SKYPE, $responseArr['content'][0]['type']);
        $this->assertEquals('skype_login', $responseArr['content'][0]['value']);

//      TEST GET client details route
        $this->send('GET', '/whadmin/clients/' . $clients[2]->getId() . '/details');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $responseArr = $this->getResponseAsArray();
        $this->assertArrayHasKey('content', $responseArr);
        $this->assertEquals('success', $this->getResponseAsArray()['status']);
        $this->assertEquals($clients[2]->getDetails()->getFirstName(), $responseArr['content']['first_name']);
        $this->assertEquals($clients[2]->getDetails()->getLastName(), $responseArr['content']['last_name']);
        $this->assertEquals($clients[2]->getDetails()->getMaidenName(), $responseArr['content']['maiden_name']);
        $this->assertEquals($clients[2]->getDetails()->getGender(), $responseArr['content']['gender']);
        $this->assertEquals($clients[2]->getDetails()->getTitle(), $responseArr['content']['title']);
        $this->assertEquals($clients[2]->getDependents()[0]->getBirthDate()->format('Ymd'), $responseArr['content']['dependents'][0]['birthdate']);
        $this->assertEquals($clients[2]->getDependents()[0]->getFirstName(), $responseArr['content']['dependents'][0]['first_name']);

    }


    public function testPostClearSecurityAction()
    {
        $username = static::generateUserName();

        /**
         * There are two mandatory parameters: username and plain_password
         */
        $this->send('POST', '/whadmin/users', [], [
            'username'       => $username,
            'plain_password' => '1234567Q#Q765',
            'roles'          => ['ROLE_SUPER_ADMIN']
        ]);

        $this->assertTrue(static::$client->getResponse()->isOk());
        $this->assertEquals($this->getResponseAsObject()->status, 'success');

        /**
         * We should get a token for next requests
         */
        $this->send('GET', '/whadmin/token', [], [], ['HTTP_AUTHORIZATION' => 'BASIC ' . base64_encode($username . ':' . '1234567Q#Q765')]);

        $this->assertTrue(static::$client->getResponse()->isOk());
        $token = $this->getResponseAsObject()->content->token;

        $userId = ($user = static::$em->getRepository(\AppBundle\Entity\User::class)->findAll()[0]) ? $user->getId() : '';

        /**
         * Check admin only type (without metadata).
         */
        $this->send('POST', '/whadmin/clear_security', ['user_id' => $userId, 'type' => 1], [], ['HTTP_AUTHORIZATION' => 'BEARER ' . $token]);
        $this->assertTrue(static::$client->getResponse()->isOk());
        /**
         * Get user and log from DB
         */
        $userFromDatabase = static::$em->getRepository(User::class)->findOneBy(['username' => $username]);
        $log = static::$em->getRepository(AdminActivityLog::class)->findOneBy(['admin' => $userFromDatabase, 'code' => AdminActivityLog::CLEAR_SECURITY_ADMIN_ONLY_ACTION]);
        $this->assertNotNull($log);
        /** This log hasn't metadata */
        $this->assertNull($log->getMetadata());
        /**
         * Check cleared type (without metadata). This is bad request, because without metadata JSON.
         */
        $this->send('POST', '/whadmin/clear_security', ['user_id' => $userId, 'type' => 0], [], ['HTTP_AUTHORIZATION' => 'BEARER ' . $token]);
        $this->assertTrue(static::$client->getResponse()->isClientError());

        /**
         * Check error code, status and message
         */
        $response = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertEquals($response['status'], 'error');
        $this->assertEquals($response['content']['metadata']['code'], 'admin.clearSecurity.incorrectMetadata');
        $this->assertEquals($response['content']['metadata']['message'], 'Metadata must contain at least three fields! You sent 0 fields.');

        /**
         * Try to send incorrect metadata JSON with 2 fields. (Message contain info about sent fields: "You sent 2 field")
         */
        $metadata = array(
            'full_name' => 'Peter Noble',
            'dob'       => '19690423'
        );
        $this->send('POST', '/whadmin/clear_security', ['user_id' => $userId, 'type' => 0, 'metadata' => $metadata],
            [], ['HTTP_AUTHORIZATION' => 'BEARER ' . $token]);
        $this->assertTrue(static::$client->getResponse()->isClientError());
        /**
         * Check message
         */
        $response = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertEquals($response['content']['metadata']['message'], 'Metadata must contain at least three fields! You sent 2 fields.');
        /**
         * Check that log with 'Cleared' type is not in the database
         */
        $log = static::$em->getRepository(AdminActivityLog::class)->findOneBy(['admin' => $userFromDatabase, 'code' => AdminActivityLog::CLEAR_SECURITY_CLEARED_ACTION]);
        $this->assertNull($log);
        /**
         * Check with correct metadata (three or more fields).
         */
        $metadata['screenname'] = 'Nobility';
        $this->send('POST', '/whadmin/clear_security', ['user_id' => $userId, 'type' => 0, 'metadata' => $metadata],
            [], ['HTTP_AUTHORIZATION' => 'BEARER ' . $token]);
        $this->assertTrue(static::$client->getResponse()->isOk());
        /**
         * Check that log in the database
         */
        $log = static::$em->getRepository(AdminActivityLog::class)->findOneBy(['admin' => $userFromDatabase, 'code' => AdminActivityLog::CLEAR_SECURITY_CLEARED_ACTION]);
        $this->assertNotNull($log);
        /** Check that metadata also into DB */
        $this->assertNotNull($log->getMetadata());
    }
}