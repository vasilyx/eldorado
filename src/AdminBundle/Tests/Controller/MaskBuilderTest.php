<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 14.02.17
 * Time: 12:06
 */

namespace AdminBundle\Tests\Controller;


use AdminBundle\Security\Permission\MaskBuilder;

class MaskBuilderTest extends \PHPUnit_Framework_TestCase
{
    public function testAddView()
    {
        // was 0000 became 0001
        $canViewMask = MaskBuilder::create()->addView()->getMask();
        $this->assertEquals(1, $canViewMask);

        // was 0010 became 0011
        $alsoCanView = MaskBuilder::create(2)->addView()->getMask();
        $this->assertEquals(3, $alsoCanView);

        $this->assertTrue(MaskBuilder::create($alsoCanView)->canView());
    }

    public function testRemoveView()
    {
        // was 0011 became 0010
        $noViewMask = MaskBuilder::create(3)->removeView()->getMask();
        $this->assertEquals(2, $noViewMask);

        $this->assertFalse(MaskBuilder::create($noViewMask)->canView());
    }

    public function testAddCreate()
    {
        $canCreateMask = MaskBuilder::create()->addCreate()->getMask();
        $this->assertEquals(2, $canCreateMask);

        // we have 0011 and after add create we should have 0011
        $alsoCanCreateMask = MaskBuilder::create(3)->addCreate()->getMask();
        $this->assertEquals(3, $alsoCanCreateMask);

        $this->assertTrue(MaskBuilder::create($alsoCanCreateMask)->canCreate());
    }

    public function testRemoveCreate()
    {
        $noCreateMask = MaskBuilder::create(3)->removeCreate()->getMask();
        $this->assertEquals(1, $noCreateMask);
        $this->assertFalse(MaskBuilder::create($noCreateMask)->canCreate());
    }

    public function testAddUpdate()
    {
        $canUpdateMask = MaskBuilder::create()->addUpdate()->getMask();
        $this->assertEquals(4, $canUpdateMask);

        // we have 0111 and after add create we should have 0111
        $alsoCanUpdateMask = MaskBuilder::create(7)->addUpdate()->getMask();
        $this->assertEquals(7, $alsoCanUpdateMask);

        $this->assertTrue(MaskBuilder::create($alsoCanUpdateMask)->canUpdate());
    }

    public function testRemoveUpdate()
    {
        $noUpdateMask = MaskBuilder::create(7)->removeUpdate()->getMask();
        $this->assertEquals(3, $noUpdateMask);
        $this->assertFalse(MaskBuilder::create($noUpdateMask)->canUpdate());
    }

    public function testAddDelete()
    {
        $canDeleteMask = MaskBuilder::create()->addDelete()->getMask();
        $this->assertEquals(8, $canDeleteMask);

        // we have 0111 and after add create we should have 0111
        $alsoCanDeleteMask = MaskBuilder::create(7)->addDelete()->getMask();
        $this->assertEquals(15, $alsoCanDeleteMask);

        $this->assertTrue(MaskBuilder::create($alsoCanDeleteMask)->canDelete());
    }

    public function testRemoveDelete()
    {
        $noDeleteMask = MaskBuilder::create(15)->removeDelete()->getMask();
        $this->assertEquals(7, $noDeleteMask);
        $this->assertFalse(MaskBuilder::create($noDeleteMask)->canDelete());
    }
}