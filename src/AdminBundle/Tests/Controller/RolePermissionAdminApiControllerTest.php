<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 14.02.17
 * Time: 15:40
 */

namespace AdminBundle\Tests\Controller;


use AdminBundle\Entity\RolePermission;
use AdminBundle\Security\Permission\MaskBuilder;
use AdminBundle\Tests\AbstractAdminTest;

class RolePermissionAdminApiControllerTest extends AbstractAdminTest
{
    public function testGetPermissions()
    {
        /**
         * Create new session. We can use the token during all tests from this class
         */
        $this->setToken();

        /**
         * Count of result array elements for all roles and all permissions should be equal to roles_count * permissions_count
         */
        $this->send('GET', '/whadmin/permissions');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $permissions = $this->getResponseAsArray()['content'];
        /**
         * Check a response structure
         */
        $this->assertEquals($this->getResponseAsObject()->status, 'success');
        $this->assertNotEmpty($this->getResponseAsArray());
        $this->assertEquals(count(static::$kernel->getContainer()->getParameter('admin.permissions')) * count(static::$kernel->getContainer()->getParameter('admin.roles')), count($this->getResponseAsObject()->content));

        /**
         * Count of result array elements should be equal to roles_count * permissions_count
         */
        $this->send('GET', '/whadmin/permissions', ['role' => 'ROLE_SUPER_ADMIN'], []);
        $this->assertTrue(static::$client->getResponse()->isOk());
        $this->assertEquals($this->getResponseAsObject()->status, 'success');
        $this->assertNotEmpty($this->getResponseAsArray());
        $this->assertEquals(count(static::$kernel->getContainer()->getParameter('admin.permissions')), count($this->getResponseAsObject()->content));

        /**
         * Patch role-permission for the PERMISSIONS permission now we CAN NOT view and update permissions
         */
        foreach ($permissions as $permission) {
            if ('PERMISSION' === $permission['permission']) {
                $this->send('PATCH', '/whadmin/permission', [], ['id' => $permission['id'], 'mask' => 'n']);
            }
        }

        $this->send('GET', '/whadmin/permissions');
        $this->assertTrue(static::$client->getResponse()->isForbidden());
        $this->assertEquals($this->getResponseAsObject()->status, 'error');

        foreach ($permissions as $permission) {
            if ('PERMISSION' === $permission['permission']) {
                $this->send('PATCH', '/whadmin/permission', [], ['id' => $permission['id'], 'mask' => 'crud']);
            }
        }
        $this->assertTrue(static::$client->getResponse()->isForbidden());
        $this->assertEquals($this->getResponseAsObject()->status, 'error');


        /**
         * We should return crud mask to SUPER_ADMIN for permissions
         */
        static::$em->clear();
        $permission = static::$em->getRepository(RolePermission::class)->findOneBy(['role' => 'ROLE_SUPER_ADMIN', 'permission' => 'PERMISSION']);
        $permission->setMask(15);
        static::$em->flush();

    }

    public function testPatchPermission()
    {
        /**
         * Get permissions for role REDACTOR
         */
        $this->send('GET', '/whadmin/permissions', ['role' => 'ROLE_REDACTOR']);

        $permissions = $this->getResponseAsArray()['content'];

        /**
         * Patch role-permission for the USER permission
         */
        foreach ($permissions as $permission) {
            if ('USER' === $permission['permission']) {
                $this->send('PATCH', '/whadmin/permission', [], ['id' => $permission['id'], 'mask' => 'rud']);
            }
        }

        $this->assertTrue(static::$client->getResponse()->isOk());
        $this->assertEquals($this->getResponseAsObject()->status, 'success');
        /**
         * Check response mask
         */
        $this->assertEquals($this->getResponseAsObject()->content->mask, 13);

        $permission = static::$em->getRepository(RolePermission::class)->findOneBy(['role' => 'ROLE_REDACTOR', 'permission' => 'USER']);

        $this->assertEquals(13, $permission->getMask());

        static::$em->clear();

        /**
         * Check that null request mask does not change the permission mask to 0000
         */
        foreach ($permissions as $permission) {
            if ('USER' === $permission['permission']) {
                $this->send('PATCH', '/whadmin/permission', [], ['id' => $permission['id']]);
            }
        }

        $this->assertTrue(static::$client->getResponse()->isOk());
        $this->assertEquals($this->getResponseAsObject()->status, 'success');
        $this->assertEquals($this->getResponseAsObject()->content->mask, 13);

        $permission = static::$em->getRepository(RolePermission::class)->findOneBy(['role' => 'ROLE_REDACTOR', 'permission' => 'USER']);

        $this->assertEquals(13, $permission->getMask());

        static::$em->clear();

        /**
         * Check that 'n' request mask changes the permission mask to 0000
         */
        foreach ($permissions as $permission) {
            if ('USER' === $permission['permission']) {
                $this->send('PATCH', '/whadmin/permission', [], ['id' => $permission['id'], 'mask' => 'n']);
            }
        }

        $this->assertTrue(static::$client->getResponse()->isOk());
        $this->assertEquals($this->getResponseAsObject()->status, 'success');
        $this->assertEquals($this->getResponseAsObject()->content->mask, 0);

        $permission = static::$em->getRepository(RolePermission::class)->findOneBy(['role' => 'ROLE_REDACTOR', 'permission' => 'USER']);

        $this->assertEquals(0, $permission->getMask());

        /**
         * Check patch permission with null id
         */
        foreach ($permissions as $permission) {
            if ('USER' === $permission['permission']) {
                $this->send('PATCH', '/whadmin/permission', [], []);
            }
        }

        $this->assertEquals(static::$client->getResponse()->getStatusCode(), 400);
        $this->assertEquals($this->getResponseAsObject()->status, 'error');
        $this->assertEquals($this->getResponseAsObject()->content->code, 'admin.permissions.id.isBlank');
    }
}