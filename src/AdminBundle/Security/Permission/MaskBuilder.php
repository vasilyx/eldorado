<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 14.02.17
 * Time: 11:54
 */

namespace AdminBundle\Security\Permission;


/**
 * Class MaskBuilder
 * @package AdminBundle\Security\Permission
 */
class MaskBuilder
{
    /**
     * No action
     */
    const MASK_NO_ACTION    = 0;    // 0000
    /**
     * View
     */
    const MASK_VIEW             = 1;    // 0001
    /**
     * Create
     */
    const MASK_CREATE           = 2;    // 0010
    /**
     * Update
     */
    const MASK_UPDATE           = 4;    // 0100
    /**
     * Delete
     */
    const MASK_DELETE           = 8;    // 1000

    /**
     * @var int
     */
    private $mask = 0;

    /**
     * @return int
     */
    public function getMask()
    {
        return $this->mask;
    }

    /**
     * @param $mask
     * @return $this
     */
    public function setMask($mask)
    {
        $this->mask = (int) $mask;

        return $this;
    }

    /**
     * @param $mask
     * @return MaskBuilder
     */
    public static function create($mask = 0)
    {
        $class = __CLASS__;
        /**
         * @var MaskBuilder $maskBuilder
         */
        $maskBuilder = new $class();
        return $maskBuilder->setMask($mask);
    }

    /**
     * @return $this
     */
    public function addView()
    {
        $this->mask = $this->mask | self::MASK_VIEW;

        return $this;
    }

    /**
     * @return $this
     */
    public function removeView()
    {
        $this->mask = $this->mask ^ self::MASK_VIEW;

        return $this;
    }

    /**
     * @return bool
     */
    public function canView()
    {
        return ($this->mask & self::MASK_VIEW) == self::MASK_VIEW;
    }

    /**
     * @return $this
     */
    public function addCreate()
    {
        $this->mask = $this->mask | self::MASK_CREATE;
        return $this;
    }

    /**
     * @return $this
     */
    public function removeCreate()
    {
        $this->mask = $this->mask ^ self::MASK_CREATE;
        
        return $this;
    }

    /**
     * @return bool
     */
    public function canCreate()
    {
        return ($this->mask & self::MASK_CREATE) == self::MASK_CREATE;
    }

    /**
     * @return $this
     */
    public function addUpdate()
    {
        $this->mask = $this->mask | self::MASK_UPDATE;
        return $this;
    }

    /**
     * @return $this
     */
    public function removeUpdate()
    {
        $this->mask = $this->mask ^ self::MASK_UPDATE;
        
        return $this;
    }

    /**
     * @return bool
     */
    public function canUpdate()
    {
        return ($this->mask & self::MASK_UPDATE) == self::MASK_UPDATE;
    }

    /**
     * @return $this
     */
    public function addDelete()
    {
        $this->mask = $this->mask | self::MASK_DELETE;
        
        return $this;
    }

    /**
     * @return $this
     */
    public function removeDelete()
    {
        $this->mask = $this->mask ^ self::MASK_DELETE;
        
        return $this;
    }

    /**
     * @return bool
     */
    public function canDelete()
    {
        return ($this->mask & self::MASK_DELETE) == self::MASK_DELETE;
    }
}