<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 13.02.17
 * Time: 9:34
 */

namespace AdminBundle\Security\Voter;


use AdminBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Voter decides if the user has permissions from the admin&#95;role&#95;permissions.yml file
 *
 * Class GroupPermissionsVoter
 * @package AdminBundle\Security\Voter
 */
class GroupPermissionsVoter extends Voter
{
    use ContainerAwareTrait;

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (false === stripos($attribute, 'PERMISSION_')) {
            return false;
        }
        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        if (in_array($attribute, $this->container->get('whadmin.user_manager')->getPermissions($user), true)) {
            return true;
        }

        return false;
    }
}