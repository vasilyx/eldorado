<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 09.02.17
 * Time: 12:08
 */

namespace AdminBundle\Security\Token;


use AdminBundle\Entity\Session;
use AdminBundle\Entity\User;
use AdminBundle\Manager\SessionManager;
use AdminBundle\Manager\UserManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * To learn more see http://symfony.com/doc/2.8/security/api_key_authentication.html
 *
 * Class TokenUserProvider
 * @package AdminBundle\Security\Token
 */
class TokenUserProvider implements UserProviderInterface
{
    /**
     * @var SessionManager
     */
    private $sm;

    /**
     * @var UserManager
     */
    private $um;

    public function __construct(SessionManager $sessionManager, UserManager $userManager)
    {
        $this->sm = $sessionManager;
        $this->um = $userManager;
    }

    public function getUsernameForApikey($apiKey)
    {
        $session = $this->sm->findOneByToken($apiKey);
        if ($session) {
            $username = $session->getUser()->getUsername();
            $session->updateTimestamps();

            $this->sm->getEntityManager()->flush();

            return $username;
        }

        return null;
    }

    public function loadUserByUsername($username)
    {
        $user = $this->um->findOneByUsername($username);

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }
}