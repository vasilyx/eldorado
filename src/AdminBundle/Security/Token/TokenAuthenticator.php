<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 09.02.17
 * Time: 12:08
 */

namespace AdminBundle\Security\Token;


use AdminBundle\Controller\AbstractAdminApiController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;

/**
 * To learn more see http://symfony.com/doc/2.8/security/api_key_authentication.html
 *
 * Class TokenAuthenticator
 * @package AdminBundle\Security\Token
 */
class TokenAuthenticator implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface
{
    public function createToken(Request $request, $providerKey)
    {
        // get token from header
        $apiKey = $this->getTokenFromHeader($request->headers->get('authorization'));

        if (!$apiKey) {
            throw new BadCredentialsException();
        }

        return new PreAuthenticatedToken(
            'anon.',
            $apiKey,
            $providerKey
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    /**
     * Uses AdminBundle\Security\Token\TokenUserProvider, see security.yml section _providers_
     *
     * @param TokenInterface $token
     * @param UserProviderInterface $userProvider
     * @param $providerKey
     * @return PreAuthenticatedToken
     */
    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        if (!$userProvider instanceof TokenUserProvider) {
            throw new \InvalidArgumentException(
                sprintf(
                    'The user provider must be an instance of TokenUserProvider (%s was given).',
                    get_class($userProvider)
                )
            );
        }

        $apiKey = $token->getCredentials();

        // uses AdminBundle\Security\Token\TokenUserProvider, see security.yml section _providers_
        $username = $userProvider->getUsernameForApikey($apiKey);

        if (!$username) {
            // CAUTION: this message will be returned to the client
            // (so don't put any un-trusted messages / error strings here)
            throw new CustomUserMessageAuthenticationException(
                sprintf('API Key "%s" does not exist.', $apiKey)
            );
        }

        $user = $userProvider->loadUserByUsername($username);

        return new PreAuthenticatedToken(
            $user,
            $apiKey,
            $providerKey,
            $user->getRoles()
        );
    }

    /**
     * Returns string token from the Authorization http header. We use so called BEARER authorization scheme from OAuth, but we don't realize OAuth.
     * Authorization: Bearer stringTokenFromMethodGetToken
     *
     * @param $header
     * @return string|null
     */
    private function getTokenFromHeader($header)
    {

        if (!isset($header)) {
            return null;
        }

        $credentialsAsArray = explode(' ', $header);
        $authorizationScheme = $credentialsAsArray[0];
        if ('BEARER' === strtoupper($authorizationScheme)) {
            $token = $credentialsAsArray[1];
            return $token;
        }

        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $message = strtr($exception->getMessageKey(), $exception->getMessageData());
        $response = new JsonResponse(
            [
                'status' => AbstractAdminApiController::STATUS_ERROR,
                'content' =>
                    [
                        'code'    => Response::HTTP_UNAUTHORIZED,
                        'message' => $message,
                    ]
            ],
            Response::HTTP_UNAUTHORIZED
        );

        return $response;
    }
}