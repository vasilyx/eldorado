<?php

namespace AdminBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 16.02.2017
 * Time: 17:26
 */
class ClearedMetadata extends Constraint
{
    public $message = 'Metadata must contain at least three fields! You sent %string% fields.';
    public $code = 'admin.clearSecurity.incorrectMetadata';

    public function getTargets()
    {
        return self::PROPERTY_CONSTRAINT;
    }
}