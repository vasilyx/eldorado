<?php

namespace AdminBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 16.02.2017
 * Time: 17:21
 */
class ClearedMetadataValidator extends ConstraintValidator
{

    /**
     * Checks if metadata array is correct and has at least three items.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (($count = count($value)) < 3) {
            $constraint->payload = array('code' => $constraint->code);
            $this->context->buildViolation($constraint->message)
                ->atPath('metadata')
                ->setParameter('%string%', $count)
                ->addViolation()
            ;
        }
    }
}