<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 07.02.17
 * Time: 16:23
 */

namespace AdminBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

abstract class AbstractAdminApiController extends FOSRestController
{
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    /**
     * Common AdminApi response with content, status and code
     *
     * @param $content
     * @param string $status
     * @param $code
     * @return View
     */
    public function getResponse($content, $status = self::STATUS_SUCCESS, $code = Response::HTTP_OK)
    {
        $view = View::create(['status'  => $status, 'content' => $content], $code);
        $view->setSerializationContext(SerializationContext::create()->enableMaxDepthChecks());
        return $view;
    }

    public function getSerialiser()
    {
        return $this->get('serializer');
    }

    /**
     * Get array of constraint violations messages. Prepares validation errors for api response.
     *
     * @param  ConstraintViolationListInterface $violations
     * @return array
     */
    protected function getConstraintViolations(ConstraintViolationListInterface $violations)
    {
        $errors = [];

        /* @var ConstraintViolation $violation */
        foreach ($violations as $violation) {
            $constraint = $violation->getConstraint();

            /*
             * We try to use payload property path before take it from getPropertyPath
             */
            $propertyPath = isset($violation->getConstraint()->payload['propertyPath']) ? $violation->getConstraint()->payload['propertyPath'] : $violation->getPropertyPath();

            $errors[$propertyPath] = [
                'code'    => isset($constraint->payload['code']) ? $constraint->payload['code'] : $violation->getMessage(),
                'message' => $violation->getMessage()
            ];
        }

        return $errors;
    }

    /**
     * Response view for request data validation violations
     *
     * @param  ConstraintViolationListInterface $violations
     * @return View
     */
    protected function getValidationErrorResponse(ConstraintViolationListInterface $violations)
    {
        return $this->getResponse($this->getConstraintViolations($violations), self::STATUS_ERROR, Response::HTTP_BAD_REQUEST);
    }
}