<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 22.05.2018
 * Time: 14:58
 */

namespace AdminBundle\Controller;


use AdminBundle\Entity\AdminDecision;
use AdminBundle\Manager\AdminDecisionManager;
use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\QuoteApply;
use AppBundle\Manager\ApiCallsManager;
use AppBundle\Manager\LoanApplyManager;
use AppBundle\Repository\CallCreditAPICallsRepository;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiCallsAdminController extends AbstractAdminApiController
{
    /**
     *
     * This method returns **HTML with response** which was gotten at Quotation. Created by Shatilenya Vladislav.
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="ApiCalls",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BASIC base64EncodedUsernamePassword', where base64EncodedUsernamePassword is string 'username:password' encoded with base64 algorithm",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {
     *              "name" = "id",
     *              "dataType" = "integer",
     *              "required" = true,
     *              "description" = "QuoteApply id"
     *          },
     *          {
     *              "name" = "type",
     *              "dataType" = "integer",
     *              "required" = true,
     *              "description" = "Type of api call, CallValidate - 1, CreditHistory - 2, Affordability - 3"
     *          }
     *     }
     * )
     *
     * @param Request $request
     * @return View
     *
     * @Get("/get_api_call_response")
     */
    public function getApiCallResponseAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var QuoteApply $quoteApply */
        $quoteApply = $em->getRepository(QuoteApply::class)->find($request->get('id'));

        if (!$quoteApply) {
            return $this->getResponse('QuoteApply not found', self::STATUS_ERROR);
        }

        /** @var CallCreditAPICallsRepository $callCreditRepository */
        $callCreditRepository = $this->getDoctrine()->getRepository(CallCreditAPICalls::class);
        /** @var CallCreditAPICalls $apiCall */
        $apiCall = $callCreditRepository->getApiCallByQuoteAndType($quoteApply, $request->get('type'));

        if (!$apiCall) {
            return $this->getResponse('Response HTML not found', self::STATUS_ERROR);
        }

        $apiCallHTML = $this->get('whs.manager.api_calls_manager')->xmlToHtmlRender($apiCall->getResponseXML(), $request->get('type'));

        return $this->getResponse(['html' => $apiCallHTML, 'id' => $apiCall->getId()]);
    }
}