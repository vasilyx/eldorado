<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\User;
use AppBundle\Entity\Transaction;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\View\View;
use LogBundle\Entity\AdminActivityLog;
use LogBundle\Handler\AdminActivityHandler;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminApiController extends AbstractAdminApiController
{

    /**
     * Get all transactions
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *      {
     *          "name"        =   "limit",
     *          "dataType"    =   "string",
     *          "required"    =    false,
     *          "description" =   "items per page"
     *      },
     *      {
     *          "name"        =   "page",
     *          "dataType"    =   "string",
     *          "required"    =    false,
     *          "description" =   "page number"
     *      }
     *     },
     *     section="Transactions"
     * )
     *
     * @Get("/transactions")
     * @param Request $request
     * @return View
     */
    public function getTransactionsAction(Request $request)
    {
        $data = $this->get('whs.manager.transaction_manager')->getPaginated(
            $request->get('page', 1),
            $request->get('limit', 10)
        );

        return $this->getResponse($data);
    }
}