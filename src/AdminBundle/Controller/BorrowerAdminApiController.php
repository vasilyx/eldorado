<?php
namespace AdminBundle\Controller;
use AdminBundle\Entity\AdminDecision;
use AdminBundle\Entity\InstalmentPV;
use AdminBundle\Manager\AdminDecisionManager;
use AdminBundle\Response\CreditLineInPending;
use AppBundle\Entity\Agreement;
use AppBundle\Entity\AgreementProcess;
use AppBundle\Entity\BankDetails;
use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\ContractMatching;
use AppBundle\Entity\Declutter;
use AppBundle\Entity\Instalment;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\InvestOfferProcess;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\QuoteTerm;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\UnderwritingResult;
use AppBundle\Entity\User;
use AppBundle\Entity\UserEmployment;
use AppBundle\Entity\UserHousehold;
use AppBundle\Event\SystemTransactionEvent;
use AppBundle\Event\TransactionEvents;
use AppBundle\Manager\AgreementManager;
use AppBundle\Manager\LoanApplyManager;
use AppBundle\Manager\UserManager;
use AppBundle\Service\Mailer;
use AppBundle\Utils\Payment\InterestRatesProviderInterface;
use AppBundle\Utils\Payment\PaymentsComputerService;
use Beta\A;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @package AdminBundle\Controller
 */
class BorrowerAdminApiController extends AbstractAdminApiController
{
    /**
     * Returns borrower details by specified quote id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     description="The method returns borrower details",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/borrowers/{quote_id}/details")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getBorrowerDetailsAction(QuoteApply $quoteApply)
    {
        if (!$this->isGranted('PERMISSION_BORROWER_VIEW')) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.accessDenied',
                    'message' => 'Access denied to view borrowers'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }

        /** @var UserManager $userManager */
        $userManager = $this->get('whs.manager.user_manager');
        $userDetails = $userManager->getUserPersonalInfo($quoteApply->getUser());

        return $this->getResponse($userDetails);
    }

    /**
     * Returns addresses for borrower with specified quote id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     description="The method returns borrower addresses",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/borrowers/{quote_id}/addresses")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getBorrowerAddressesAction(QuoteApply $quoteApply)
    {
        if (!$this->isGranted('PERMISSION_BORROWER_VIEW')) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.accessDenied',
                    'message' => 'Access denied to view borrowers'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }

        $user = $quoteApply->getUser();

        if (empty($user)) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.userNotFound',
                    'message' => 'User for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $userAddressManager = $this->get('whs.manager.user_address_manager');
        $userAddresses = $userAddressManager->getAddressList($user);

        return $this->getResponse($userAddresses);
    }

    /**
     * Returns borrower finances by specified quote id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     description="The method returns borrower finances",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/borrowers/{quote_id}/finances")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getBorrowerFinancesAction(QuoteApply $quoteApply)
    {
        if (!$this->isGranted('PERMISSION_BORROWER_VIEW')) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.accessDenied',
                    'message' => 'Access denied to view borrowers'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }

        $user = $quoteApply->getUser();

        if (empty($user)) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.userNotFound',
                    'message' => 'User for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $quoteIncome = $this->get('whs.manager.quote_income_manager')->getQuoteIncome($user);

        return $this->getResponse($quoteIncome);
    }

    /**
     * Get borrower quote terms by specified quote id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     description="The method returns borrower quote terms",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/borrowers/{quote_id}/terms")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getBorrowerTermsAction(QuoteApply $quoteApply)
    {
        if (!$this->isGranted('PERMISSION_BORROWER_VIEW')) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.accessDenied',
                    'message' => 'Access denied to view borrowers'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }

        $underwritingManager = $this->get('whs.manager.underwriting_result_manager');
        $underwritingResult = $underwritingManager->getUnderwritingResultForQuoteApply($quoteApply);

        if (!$underwritingResult) {
            return $this->getResponse(
                [
                    'code' => 'admin.quoteTerms.notFound',
                    'message' => 'Quote terms not found for this quote'
                ], self::STATUS_ERROR, Response::HTTP_NOT_FOUND
            );
        }

        return $this->getResponse($underwritingResult);
    }

    /**
     * Returns ID Check for borrower with specified quote id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     description="The method returns borrower ID check",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/borrowers/{quote_id}/id_check")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getBorrowerIDCheckAction(QuoteApply $quoteApply)
    {
        if (!$this->isGranted('PERMISSION_INVESTOR_VIEW')) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.accessDenied',
                    'message' => 'Access denied to view borrowers'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }

        $user = $quoteApply->getUser();

        if (empty($user)) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.userNotFound',
                    'message' => 'User for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $callvalidateResponse = $this->getDoctrine()->getRepository(CallCreditAPICalls::class)->getLatestCallValidateForInvestor($user);
        unset($callvalidateResponse['xml']);

        return $this->getResponse($callvalidateResponse);
    }

    /**
     *
     * This method set admin decision on API CALL (CV, CR, AR). Created by Shatilenya Vladislav.
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BASIC base64EncodedUsernamePassword', where base64EncodedUsernamePassword is string 'username:password' encoded with base64 algorithm",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {
     *              "name" = "api_call_id",
     *              "dataType" = "integer",
     *              "required" = true,
     *              "description" = "API Call identifier"
     *          },
     *          {
     *              "name" = "decision",
     *              "dataType" = "integer",
     *              "required" = true,
     *              "description" = "Admin decision 100 - Approve, 200 - Decline"
     *          },
     *          {
     *              "name" = "comment",
     *              "dataType" = "textarea",
     *              "required" = true,
     *              "description" = "Comment"
     *          }
     *     }
     * )
     *
     * @param Request $request
     * @return View
     *
     * @Post("/borrower/decision")
     */
    public function postBorrowerAdminDecisionAction(Request $request)
    {
        /** @var AdminDecisionManager $adminDecisionManager */
        $adminDecisionManager = $this->get('whadmin.manager.admin_decision');
        $adminDecision = $adminDecisionManager->createAdminDecision($this->getUser(), $request->request->get('api_call_id'), $request->request->get('decision'), $request->request->get('comment'));

        if (empty($adminDecision)) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.Failed',
                    'message' => 'API Call not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $loanApply = null;
        /** @var LoanApplyManager $loanApplyManager */
        $loanApplyManager = $this->get('whs.manager.loan_apply_manager');

        $decisionValue = $request->request->get('decision');

        if ($decisionValue == AdminDecision::APPROVE_DECISION) {
            switch ($adminDecision->getApiCall()->getType()) {
                case CallCreditAPICalls::TYPE_CALLVALIDATE:
                    $loanApply = $loanApplyManager->setLoanApplyStatusByQuote($adminDecision->getApiCall()->getQuoteApply(), LoanApply::STATUS_AFFORDABILITY_CHECK);
                    break;
                case CallCreditAPICalls::TYPE_AFFORDABILITY:
                    $loanApply = $loanApplyManager->setLoanApplyStatusByQuote($adminDecision->getApiCall()->getQuoteApply(), LoanApply::STATUS_CREDIT_HISTORY_CHECK);
                    break;
                case CallCreditAPICalls::TYPE_CALLREPORT:
                    $loanApply = $loanApplyManager->setLoanApplyStatusByQuote($adminDecision->getApiCall()->getQuoteApply(), LoanApply::STATUS_CREDIT_DECISION);
                    break;
                default:
                    break;
            }
        } elseif ($decisionValue == AdminDecision::DECLINE_DECISION) {
            $quoteApply = $adminDecision->getApiCall()->getQuoteApply();
            $loanApply = $loanApplyManager->setLoanApplyStatusByQuote($quoteApply, LoanApply::STATUS_DECLINED);

            $em = $this->getDoctrine()->getManager();

            $quoteApply->setStatus(QuoteApply::STATUS_REJECTED);

            /** @var Mailer $mailer */
            $mailer = $this->container->get('whs_mailer');
            $user = $quoteApply->getUser();
            $mailer->sendLoanDecline($user->getUsername(), ['name' => $user->getDetails()->getFirstName()]);


            $em->persist($quoteApply);
            $em->flush();
        }

        return $this->getResponse(['loan_apply_status' => !empty($loanApply) ? $loanApply->getStatus() : ''], self::STATUS_SUCCESS, Response::HTTP_CREATED);
    }

    /**
     * Returns loan apply details for borrower with specified quote id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     description="The method returns borrower loan apply details",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/borrowers/{quote_id}/loan_apply_details")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getBorrowerLoanApplyDetailsAction(QuoteApply $quoteApply)
    {
        /** @var LoanApply $loanApply */
        $loanApply = $this->get('whs.manager.loan_apply_manager')->getLoanApplyByQuote($quoteApply);

        if (empty($loanApply)) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.LoanApplyNotFound',
                    'message' => 'Loan apply not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        return $this->getResponse(['id' => $loanApply->getId(), 'status' => (int)$loanApply->getStatus()]);
    }

    /**
     * Returns quotation details for borrower with specified quote id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     description="The method returns borrower quotation details",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/borrowers/{quote_id}/quotation_details")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getBorrowerQuoteDetailsAction(QuoteApply $quoteApply)
    {
        return $this->getResponse($quoteApply);
    }

    /**
     * Returns quotation details for borrower with specified quote id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     description="The method returns borrower quotation details",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/borrowers/{quote_id}/quotation")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getBorrowerQuotationAction(QuoteApply $quoteApply)
    {
        /** @var LoanApply $loanApply */
        $loanApply = $this->get('whs.manager.loan_apply_manager')->getLoanApplyByQuote($quoteApply);

        if (empty($loanApply)) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.LoanApplyNotFound',
                    'message' => 'Loan apply not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $result['user_application'] = [
            'loan_amount'  => $loanApply->getAmount(),
            'monthly'      => $loanApply->getMonthly(),
            'total_repaid' => '',
            'apr'          => $loanApply->getApr(),
            'bonus_amount' => '',
            'bonus_rate' => '',
            'term'         => $loanApply->getTerm()
        ];

        $underwritingResultManager = $this->get('whs.manager.underwriting_result_manager');

        $qsUR = $underwritingResultManager->getUnderwritingResultForQuoteApply($quoteApply, UnderwritingResult::TYPE_QUOTE);
        $caUR = $underwritingResultManager->getUnderwritingResultForQuoteApply($quoteApply, UnderwritingResult::TYPE_CREDIT);

        $result['quoted'] = $qsUR;
        $result['generated'] = $caUR;

        /** @var Agreement $agreement */
        $agreement = $this->getDoctrine()->getManager()->getRepository(Agreement::class)->findOneBy(['loanApply' => $loanApply]);

        if (!empty($agreement)) {
            $result['decision'] = [
                'loan_amount' => $agreement->getAmount(),
                'monthly' => $agreement->getMonthlyRepayment(),
                'total_repaid' => '',
                'apr' =>$agreement->getApr(),
                'bonus_amount' => $agreement->getBonusAmount(),
                'bonus_rate' => $agreement->getBonusRate(),
                'term' => $agreement->getTerm()
            ];
        }

        return $this->getResponse($result);
    }

    /**
     * Confirm credit decision
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="CreditDecision",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="quote_id", "dataType"="string", "required"=true, "description"="Quote ID"},
     *          {"name"="amount", "dataType"="string", "required"=true, "description"="Total Credit"},
     *          {"name"="term", "dataType"="string", "required"=false, "description"="Term"},
     *          {"name"="apr", "dataType"="string", "required"=false, "description"="APR"},
     *          {"name"="monthly_repayment", "dataType"="string", "required"=false, "description"="Monthly Repayment"},
     *          {"name"="bonus_amount", "dataType"="string", "required"=false, "description"="Bonus Amount"},
     *          {"name"="bonus_rate", "dataType"="string", "required"=false, "description"="Bonus Rate"},
     *          {"name"="comment", "dataType"="string", "required"=false, "description"="Comment"}
     *     }
     * )
     *
     * @return View
     * @param Agreement $Agreement
     * @ParamConverter("Agreement", converter="fos_rest.request_body")
     * @POST("/borrowers/confirmCreditDecision")
     */
    public function setConfirmCreditDecisionAction(Request $request, Agreement $Agreement)
    {
        $quoteId = $request->request->get('quote_id');
        $em = $this->getDoctrine()->getManager();

        /**
         * @var $loanApply LoanApply
         */
        $loanApply = $em->getRepository('AppBundle:LoanApply')->findOneBy(['quoteApply' => $quoteId]);
        $loanApply->setStatus(LoanApply::STATUS_ACCEPTANCE);
        $Agreement->setLoanApply($loanApply);
        $Agreement->setBalance($Agreement->getAmount());

        /** @var PaymentsComputerService $paymentsComputer */
        $paymentsComputer = $this->get('app.utils.payments_computer');
        $totals = $paymentsComputer->getCreditPaymentsTotal($Agreement->getApr(), $Agreement->getTerm(), $Agreement->getAmount());
        $Agreement->setFirstMonthlyPayment($totals['firstMonthly']);

        $em->persist($loanApply);
        $em->persist($Agreement);
        $em->flush();


        return $this->getResponse(['loan_apply_status' => !empty($loanApply) ? $loanApply->getStatus() : '']);
    }

    /**
     * Decline credit decision
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     },
     *     section="CreditDecision",
     *     parameters={
     *          {"name"="quote_id", "dataType"="string", "required"=true, "description"="Quote ID"},
     *     }
     * )
     *
     * @return View
     * @POST("/borrowers/declineCreditDecision")
     */
    public function setDeclineCreditDecisionAction(Request $request)
    {
        $quoteId = $request->request->get('quote_id');
        $em = $this->getDoctrine()->getManager();

        /**
         * @var $loanApply LoanApply
         */
        $loanApply = $em->getRepository('AppBundle:LoanApply')->findOneBy(['quoteApply' => $quoteId]);


        if (empty($loanApply)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.loanApplyNotFound',
                    'message' => 'LoanApply for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $loanApply->setStatus(LoanApply::STATUS_DECLINED);

        $quote = $loanApply->getQuoteApply();
        $quote->setStatus(QuoteApply::STATUS_REJECTED);

        $em->persist($quote);
        $em->persist($loanApply);
        $em->flush();

        return $this->getResponse(['loan_apply_status' => !empty($loanApply) ? $loanApply->getStatus() : '']);
    }

    /**
     * Returns Agreement by quote_id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="CreditDecision",
     *     description="The method returns borrower ID check",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/borrowers/{quote_id}/creditDecision")
     *
     * @return View
     */
    public function getCreditDecisionAction(Request $request)
    {
        $quoteId = $request->get('quote_id');
        $em = $this->getDoctrine()->getManager();

        /**
         * @var $loanApply LoanApply
         */
        $loanApply = $em->getRepository('AppBundle:LoanApply')->findOneBy(['quoteApply' => $quoteId]);


        if (empty($loanApply)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.loanApplyNotFound',
                    'message' => 'LoanApply for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        /**
         * @var $agreement Agreement
         */
        $agreement = $em->getRepository('AppBundle:Agreement')->findOneBy(['loanApply' => $loanApply]);

        if (empty($agreement)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.agreementNotFound',
                    'message' => 'Agreement for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }


        return $this->getResponse($agreement);
    }

    /**
     * Returns loan apply details for borrower with specified quote id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     description="The method returns borrower loan apply details",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/borrowers/{quote_id}/matchingDetails")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getBorrowerMatchingDetailsAction(QuoteApply $quoteApply)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var LoanApply $loanApply */
        $loanApply = $this->get('whs.manager.loan_apply_manager')->getLoanApplyByQuote($quoteApply);

        if (empty($loanApply)) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.LoanApplyNotFound',
                    'message' => 'Loan apply not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        /**
         * @var $agreement Agreement
         */
        $agreement = $em->getRepository('AppBundle:Agreement')->findOneBy(['loanApply' => $loanApply]);

        if (empty($agreement)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.agreementNotFound',
                    'message' => 'Agreement for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $data = $this->get('whadmin.borrower_manager')->getAgreementMatchedDetails($agreement);

        return $this->getResponse($data);
    }

    /**
     * Admin - get Acceptance Status
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     description="The method returns Acceptance Status",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/borrowers/{quote_id}/acceptanceStatus")
     *
     * @return View
     */
    public function getAcceptanceStatusAction(Request $request)
    {
        $quoteId = $request->get('quote_id');
        $em = $this->getDoctrine()->getManager();

        /**
         * @var $loanApply LoanApply
         */
        $loanApply = $em->getRepository('AppBundle:LoanApply')->findOneBy(['quoteApply' => $quoteId]);


        if (empty($loanApply)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.loanApplyNotFound',
                    'message' => 'LoanApply for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        /**
         * @var $agreement Agreement
         */
        $agreement = $em->getRepository('AppBundle:Agreement')->findOneBy(['loanApply' => $loanApply]);

        if (empty($agreement)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.agreementNotFound',
                    'message' => 'Agreement for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }


        $data = $this->get('whadmin.borrower_manager')->getAcceptanceStatus($agreement);

        return $this->getResponse($data);
    }

    /**
     * Confirm final contract
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="quote_id", "dataType"="string", "required"=true, "description"="Quote ID"}
     *     }
     * )
     *
     * @return View
     * @POST("/borrowers/confirmFinalContract")
     */
    public function setConfirmFinalContractAction(Request $request)
    {
        $quote_id = $request->request->get('quote_id');
        $em = $this->getDoctrine()->getManager();

        /**
         * @var $loanApply LoanApply
         */
        $loanApply = $em->getRepository('AppBundle:LoanApply')->findOneBy(['quoteApply' => $quote_id]);

        if (empty($loanApply)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.loanApplyNotFound',
                    'message' => 'LoanApply for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        /**
         * @var $agreement Agreement
         */
        $agreement = $em->getRepository('AppBundle:Agreement')->findOneBy(['loanApply' => $loanApply]);

        if (empty($agreement)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.agreementNotFound',
                    'message' => 'Agreement for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }


        /**
         * @var $loanApply LoanApply
         */
        $loanApply = $em->getRepository('AppBundle:LoanApply')->findOneBy(['quoteApply' => $quote_id]);
        $loanApply->setStatus(LoanApply::STATUS_SIGNED);

        $agreement->setSigningDate(new \DateTime());

        $em->persist($loanApply);
        $em->persist($agreement);
        $em->flush();

        return $this->getResponse(['loan_apply_status' => !empty($loanApply) ? $loanApply->getStatus() : '']);
    }


    /**
     * Decline final contract
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="quote_id", "dataType"="string", "required"=true, "description"="Quote ID"},
     *     }
     * )
     * @Rest\View(serializerGroups={"default"})
     * @return View
     * @POST("/borrowers/declineFinalContract")
     */
    public function setDeclineFinalContractAction(Request $request)
    {
        $quoteId = $request->request->get('quote_id');
        $em = $this->getDoctrine()->getManager();

        /**
         * @var $loanApply LoanApply
         */
        $loanApply = $em->getRepository('AppBundle:LoanApply')->findOneBy(['quoteApply' => $quoteId]);

        if (empty($loanApply)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.loanApplyNotFound',
                    'message' => 'LoanApply for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        /**
         * @var $agreement Agreement
         */
        $agreement = $em->getRepository('AppBundle:Agreement')->findOneBy(['loanApply' => $loanApply]);

        if (empty($agreement)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.agreementNotFound',
                    'message' => 'Agreement for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $quoteApply = $loanApply->getQuoteApply();
        $loanApply->setStatus(LoanApply::STATUS_DECLINED);
        $agreement->setIsDeclined(true);
        $agreement->setSigningDate(new \DateTime());
        $quoteApply->setStatus(QuoteApply::STATUS_DECLINED);

        $em->persist($quoteApply);
        $em->persist($loanApply);
        $em->persist($agreement);
        $em->flush();

        /** @var Mailer $mailer */
        $mailer = $this->container->get('whs_mailer');
        $user = $loanApply->getUser();
        $mailer->sendLoanDecline($user->getUsername(), ['name' => $user->getDetails()->getFirstName()]);

        $em->getRepository('AppBundle:ContractMatching')->deleteByAgreement($agreement);


        return $this->getResponse(['loan_apply_status' => !empty($loanApply) ? $loanApply->getStatus() : '']);
    }

    /**
     * Calculate loan properties for **final** contract
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="loan_term", "dataType"="float", "required"=true, "description"="Loan term"},
     *          {"name"="loan_amount", "dataType"="float", "required"=true, "description"="Loan amount"},
     *          {"name"="loan_apr", "dataType"="float", "required"=true, "description"="Loan APR"},
     *          {"name"="loan_bonus_rate", "dataType"="float", "required"=true, "description"="Loan bonus rate"}
     *     }
     * )
     * @return View
     * @Get("/borrowers/calculate_loan_properties")
     */
    public function getCalculateLoanPropertiesAction(Request $request)
    {
        $loanTerm = $request->get('loan_term');
        $loanAmount = $request->get('loan_amount');
        $loanApr = $request->get('loan_apr');
        $loanBonusRate = $request->get('loan_bonus_rate');

        // only for validation
        $quoteTerm = new QuoteTerm();
        $quoteTerm->setTerm($loanTerm);
        $quoteTerm->setAmount($loanAmount);

        $validationErrors = $this->get('validator')->validate($quoteTerm);
        if (count($validationErrors) > 0) {
            return $this->getValidationErrorResponse($validationErrors);
        }

        $paymentsComputer = $this->get('app.utils.payments_computer');
        $creditPayments = $paymentsComputer->getCreditPaymentsTotal($loanApr, $loanTerm, $loanAmount);
        $effectiveRate = $loanApr - $loanBonusRate;
        $effectivePayments = $paymentsComputer->getCreditPaymentsTotal($effectiveRate, $loanTerm, $loanAmount);
        $bonusAmount = $creditPayments['totalInterest'] - $effectivePayments['totalInterest'];

        $result['monthly_payment'] = round($creditPayments['monthlyPayment'], 2);
        $result['interest'] = $creditPayments['totalPaid'] - $loanAmount;
        $result['first_monthly_payment'] = $creditPayments['firstMonthly'];
        $result['total_amount'] = $creditPayments['totalPaid'];
        $result['bonus_amount'] = $bonusAmount;

        return $this->getResponse($result);
    }

    /**
     * This method returns borrower socials
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Borrower",
     * )
     *
     * @param QuoteApply $quoteApply
     * @return View
     *
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     * @Get("/application/{quote_id}/socials_contacts")
     */
    public function getBorrowerSocialsAction(QuoteApply $quoteApply)
    {
        $user = $quoteApply->getUser();

        $userCommunications = $this->get('whs.manager.user_communication_manager')->getCommunicationList($user);

        return $this->getResponse($userCommunications);
    }

    /**
     * This method returns borrower monthly expenses
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Borrower",
     * )
     *
     * @param QuoteApply $quoteApply
     * @return View
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     * @Get("/application/{quote_id}/monthly_expenses")
     */
    public function getBorrowerHouseHoldAction(QuoteApply $quoteApply)
    {
        $user = $quoteApply->getUser();

        $userHouseHold = $this->getDoctrine()->getRepository(UserHousehold::class)->findByUser($user);

        return $this->getResponse($userHouseHold);
    }

    /**
     * This method returns borrower dependents
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Borrower",
     * )
     *
     * @param QuoteApply $quoteApply
     * @return View
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     * @Get("/application/{quote_id}/dependents")
     */
    public function getBorrowerDependentsAction(QuoteApply $quoteApply)
    {
        $user = $quoteApply->getUser();

        $userDependents = $this->get('whs.manager.user_dependent_manager')->getUserDependentList($user);

        return $this->getResponse($userDependents);
    }

    /**
     * This method returns borrower occuptation history
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Borrower",
     * )
     *
     * @param QuoteApply $quoteApply
     * @return View
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     * @Get("/application/{quote_id}/occupation_history")
     */
    public function getBorrowerOccupationHistoryAction(QuoteApply $quoteApply)
    {
        $user = $quoteApply->getUser();

        $userOccupationHistory = $this->getDoctrine()->getRepository(UserEmployment::class)->getEmploymentsForUser($user);

        return $this->getResponse($userOccupationHistory);
    }

    /**
     * This method returns borrower bank account info
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Borrower",
     * )
     *
     * @param QuoteApply $quoteApply
     * @return View
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     * @Get("/application/{quote_id}/bank_account")
     */
    public function getBorrowerBankAccountAction(QuoteApply $quoteApply)
    {
        $user = $quoteApply->getUser();

        $bankAccount = $this->getDoctrine()->getRepository(BankDetails::class)->findByUser($user);

        return $this->getResponse($bankAccount);
    }

    /**
     * This method returns borrower debts
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Borrower",
     * )
     *
     * @param QuoteApply $quoteApply
     * @return View
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     * @Get("/application/{quote_id}/debts")
     */
    public function getBorrowerDebtsAction(QuoteApply $quoteApply)
    {
        $user = $quoteApply->getUser();

        $debts = $this->get('whs.manager.user_creditline_manager')->getCreditLineList($user);
        $loanApply = $this->get('whs.manager.loan_apply_manager')->getLoanApplyByQuote($quoteApply);

        $result = [];
        foreach ($debts as $debt) {
            $declutter = $this->getDoctrine()->getRepository(Declutter::class)->findOneBy(['loanApply' => $loanApply, 'userCreditline' => $debt, 'archive' => false]);
            $result[] = [
                'credit_line' => $debt,
                'dclutter' => !empty($declutter),
                'need_to_close' => !empty($declutter) ? $declutter->getNeedToClose() : false,
            ];
        }

        return $this->getResponse($result);
    }

    /**
     * Method for the admins which returns list of payments allocated to creditors for payment
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     },
     *     section="Borrower",
     * )
     *
     * @return View
     * @Get("/pendingCreditLines")
     */
    public function getPendingCreditLinesAction()
    {
        $transactions = $this->getDoctrine()->getRepository(Transaction::class)->getPendingCreditLines();

        $result = [];
        /**
         * @var Transaction $transaction
         */
        foreach ($transactions as $transaction) {

            $creditLine = new CreditLineInPending();

            $creditLine->setTransactionId($transaction->getId());
            $creditLine->setTransactionAmt($transaction->getTransactionAmount());
            $creditLine->setTransactionCCY($transaction->getTransactionCcy());
            $creditLine->setCustomerId($transaction->getUser()->getId());
            $creditLine->setCustomerName($transaction->getUser()->getFullName());
            $creditLine->setEventId($transaction->getEvent()->getId());

            $loanApply = $this->getDoctrine()->getRepository(LoanApply::class)->findOneBy(['user' => $transaction->getUser()]);
            if (!empty($loanApply)) {
                $creditLine->setLoanApplyId($loanApply->getId());
            }

            array_push($result, $creditLine);
        }

        return $this->getResponse($result);
    }

    /**
     * Admin can mark pending Credit Line as paid
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="id", "dataType"="string", "required"=true, "description"="Transaction ID"}
     *     }
     * )
     *
     * @return View
     * @POST("/setPayCreditLine")
     */
    public function setPayCreditLineAction(Request $request)
    {
        $transaction_id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();

        /**
         * @var Transaction $transaction
         */
        $transaction = $em->getRepository(Transaction::class)->find($transaction_id);

        if (empty($transaction) || empty($transaction->getOfferId())) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.transactionNotFound',
                    'message' => 'Transaction by this ID not found or Offer ID for transaction'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $investOffer = $em->getRepository(InvestOffer::class)->find($transaction->getOfferId());
        if (empty($investOffer)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.investOfferTransactionNotFound',
                    'message' => 'InvestOffer for this Transaction ID not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $loanApply = $this->getDoctrine()->getRepository(LoanApply::class)->findOneBy(['user' => $transaction->getUser()]);
        $agreement = $this->getDoctrine()->getRepository(Agreement::class)->findOneBy(['loanApply' => $loanApply]);

        $agreementProcess = new AgreementProcess();
        $agreementProcess->setAgreement($agreement);
        $agreementProcess->setUser($loanApply->getUser());

        $em->persist($agreementProcess);
        $em->flush();

        $this->get('event_dispatcher')->dispatch(TransactionEvents::CRDS,
            SystemTransactionEvent::create()
                ->addTransaction(
                    Transaction::create()
                        ->setOfferId($transaction->getOfferId())
                        ->setInvestmentType($transaction->getInvestmentType())
                        ->setParentTransaction($transaction)
                        ->setUser($transaction->getUser())
                        ->setTransactionAmount(-1 * $transaction->getTransactionAmount())
                )
                ->setProcess($agreementProcess)
                ->setArgument('contractMatching', false)
        );

        $this->get('whs.manager.transaction_manager')->checkPaidCRDS($transaction->getUser());

        return $this->getResponse('');
    }

    /**
     * Returns ALL WDRC transactions which has no WDRA child
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Withdrawal",
     *     description="The method returns ALL WDRC transactions which has no WDRA child",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/withdrawals")
     *
     * @return View
     */
    public function getWithdrawalsAction()
    {
        $transactions = $this->getDoctrine()->getRepository(Transaction::class)->getPendingWithdrawals();

        $result = [];
        /**
         * @var Transaction $transaction
         */
        foreach ($transactions as $transaction) {

            $creditLine = new CreditLineInPending();

            $creditLine->setTransactionId($transaction->getId());
            $creditLine->setTransactionAmt($transaction->getTransactionAmount());
            $creditLine->setTransactionCCY($transaction->getTransactionCcy());
            $creditLine->setCustomerId($transaction->getUser()->getId());
            $creditLine->setCustomerName($transaction->getUser()->getFullName());
            $creditLine->setEventId($transaction->getEvent()->getId());
            $creditLine->setCreated($transaction->getCreatedAt());
            $creditLine->setType($transaction->getTransactionOwner());

            array_push($result, $creditLine);
        }

        return $this->getResponse($result);
    }

    /**
     * Admin can mark pending Credit Line as paid
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Withdrawal",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="id", "dataType"="string", "required"=true, "description"="Transaction ID"}
     *     }
     * )
     *
     * @return View
     * @POST("/confirmWithdrawal")
     */
    public function setConfirmWithdrawalAction(Request $request)
    {
        $transaction_id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();

        /**
         * @var Transaction $transaction
         */
        $transaction = $em->getRepository(Transaction::class)->find($transaction_id);

        if (empty($transaction)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.transactionNotFound',
                    'message' => 'Transaction by this ID not found or Offer ID for transaction'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $this->get('event_dispatcher')->dispatch(TransactionEvents::WDRA,
            SystemTransactionEvent::create()
                ->addTransaction(
                    Transaction::create()
                        ->setUser($transaction->getUser())
                        ->setInvestmentType($transaction->getInvestmentType())
                        ->setTransactionOwner($transaction->getInvestmentType())
                        ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_WITHDRAWAL_PENDING)
                        ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_WITHDRAW)
                        ->setParentTransaction($transaction)
                        ->setTransactionAmount(-1 * $transaction->getTransactionAmount())
                )
                ->addTransaction(
                    Transaction::create()
                        ->setUser($transaction->getUser())
                        ->setInvestmentType($transaction->getInvestmentType())
                        ->setTransactionOwner(Transaction::TRANSACTION_OWNER_TRUST_BANK_ACCOUNT_BORROWERS)
                        ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_CASH_AVAILABLE)
                        ->setParentTransaction($transaction)
                        ->setTransactionAmount(-1 * $transaction->getTransactionAmount())
                )
        );

        return $this->getResponse('');
    }

    /**
     * Get instalments
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     },
     *     section="Instalment"
     * )
     *
     * @Get("/application/{quote_id}/instalments")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getBorrowerInstalmentsAction(QuoteApply $quoteApply)
    {
        $user = $quoteApply->getUser();

        $loanApplies = $this->getDoctrine()->getRepository(LoanApply::class)->findBy(['status' => LoanApply::STATUS_ACTIVE, 'user' => $user]);
        $instalmentRepository = $this->getDoctrine()->getRepository(Instalment::class);

        $result = [];

        foreach ($loanApplies as $loanApply)
        {
            $instalments = $instalmentRepository->findBy(['loanApply' => $loanApply]);
            $agreement = $this->getDoctrine()->getRepository(Agreement::class)->findOneBy(['loanApply' => $loanApply]);

            /**
             * @var Instalment $instalment
             */
            foreach ($instalments as $instalment)
            {
                $row = [
                    'instalment_id' => $instalment->getId(),
                    'agreement_id' => $agreement->getId(),
                    'loan_apply_id' => $loanApply->getId(),
                    'period' => $instalment->getPeriod(),
                    'payment_date' => $instalment->getPaymentDate(),
                    'instalment' => $instalment->getInstalment(),
                    'interest' => $instalment->getInterest(),
                    'principle' => $instalment->getPrinciple(),
                    'balance' => $instalment->getBalance(),
                    'created_at' => $instalment->getCreatedAt(),
                    'paymentStatus' => $instalment->getPaymentStatus(),
                    'outstanding' => round($instalment->getOutstanding(), 2),
                    'allocation' => round($instalment->getAllocation(), 2),
                    'installmentStatus' => $instalment->getInstallmentStatus(),
                    'paymentAmount' => round($instalment->getPaymentAmount(),2),
                ];

                array_push($result, $row);
            }

            $result = $this->get('whs.manager.instalments_manager')->generateFutureInstalments($agreement->getLoanApply(), $result);
        }

        return $this->getResponse($result);
    }

    /**
     * Get instalments by offer and agreement
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     },
     *     section="Instalment",
     *     parameters={
     *          {"name"="agreement_id", "dataType"="string", "required"=true, "description"="Agreement ID"},
     *          {"name"="offer_id", "dataType"="string", "required"=true, "description"="Offer ID"},
     *     }
     * )
     *
     * @POST("/instalmentsByOffer")
     * @param Request $request
     * @return View
     */
    public function getInstalmentsByOfferAdminAction(Request $request)
    {
        $agreement_id = $request->request->get('agreement_id');
        $offer_id = $request->request->get('offer_id');
        $em = $this->getDoctrine();

        /**
         * @var Agreement $agreement
         */
        $agreement = $em->getRepository(Agreement::class)->find($agreement_id);

        if (empty($agreement)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.agreementNotFound',
                    'message' => 'Agreement by this ID not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        /**
         * @var InvestOffer $investOffer
         */
        $investOffer = $em->getRepository(InvestOffer::class)->find($offer_id);

        if (empty($investOffer)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.investOfferNotFound',
                    'message' => 'InvestOffer by this ID not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }


        $instalmentRepository = $em->getRepository(Instalment::class);

        $result = [];


        $instalments = $instalmentRepository->findBy(['loanApply' => $agreement->getLoanApply()]);


        $matchedSum = $em->getRepository(ContractMatching::class)->getMatchedSumByAgreementOffer(
            $agreement,
            $investOffer
        );

        $offerPercent = $matchedSum / $agreement->getAmount();

        /**
         * @var Instalment $instalment
         */
        foreach ($instalments as $instalment)
        {
            $row = [
                'instalment_id' => $instalment->getId(),
                'loan_apply_id' => $agreement->getLoanApply()->getId(),
                'period' => $instalment->getPeriod(),
                'payment_date' => $instalment->getPaymentDate(),
                'instalment' => $instalment->getInstalment() * $offerPercent,
                'interest' => $instalment->getInterest() * $offerPercent,
                'principle' => $instalment->getPrinciple() * $offerPercent,
                'balance' => $instalment->getBalance() * $offerPercent,
                'created_at' => $instalment->getCreatedAt(),
                'paymentStatus' => $instalment->getPaymentStatus(),
                'outstanding' => $instalment->getOutstanding() * $offerPercent,
                'allocation' => $instalment->getAllocation() * $offerPercent,
                'installmentStatus' => $instalment->getInstallmentStatus(),
                'paymentAmount' => $instalment->getPaymentAmount() * $offerPercent,
            ];

            array_push($result, $row);
        }

        $result = $this->get('whs.manager.instalments_manager')->generateFutureInstalments($agreement->getLoanApply(), $result, $offerPercent);


        return $this->getResponse($result);
    }

    /**
     * Admin can mark pending Credit Line as paid
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Instalment",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="id", "dataType"="string", "required"=true, "description"="Agreement ID"},
     *          {"name"="amount", "dataType"="string", "required"=true, "description"="Amount"},
     *     }
     * )
     *
     * @return View
     * @POST("/confirmInstalment")
     */
    public function setConfirmInstalmentAction(Request $request)
    {
        $agreementId = $request->request->get('id');
        $amount = $request->request->get('amount');
        $em = $this->getDoctrine()->getManager();

        /**
         * @var Agreement $agreement
         */
        $agreement = $em->getRepository(Agreement::class)->find($agreementId);

        if (empty($agreement)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.agreementNotFound',
                    'message' => 'Agreement by this ID not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        /**
         * @var InstalmentPV $paymentValue
         */
        $paymentValue = $this->get('whadmin.manager.admin_instalment')->buildPaymentValues($agreement, $amount);

        $this->get('whadmin.manager.admin_instalment')->createConfirmTransactions($paymentValue, $agreement);
        $this->get('whadmin.manager.admin_instalment')->updateInstalmentRecords($agreement, $amount);

        return $this->getResponse('');
    }

    /**
     * Create instalment
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Borrower",
     *     description="The method creates instalment",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Post("/borrowers/{quote_id}/instalment")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function postBorrowerInstalmentAction(QuoteApply $quoteApply)
    {
        if (!$this->isGranted('PERMISSION_BORROWER_VIEW')) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.accessDenied',
                    'message' => 'Access denied to view borrowers'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }

        $loanApply = $this->get('whs.manager.loan_apply_manager')->getLoanApplyByQuote($quoteApply);

        if ($loanApply->getStatus() != LoanApply::STATUS_ACTIVE) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.failed',
                    'message' => 'Loan Apply is not active'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $agreement = $this->getDoctrine()->getRepository(Agreement::class)->findOneBy(['loanApply' => $loanApply]);

        if (empty($agreement) || $agreement->getBalance() <= 0) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.failed',
                    'message' => 'Agreement not found or balance less than 0'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $instalmentManager = $this->get('whs.manager.instalments_manager');
        $instalment = $instalmentManager->createInstalment($agreement);

        return $this->getResponse($instalment);
    }
}