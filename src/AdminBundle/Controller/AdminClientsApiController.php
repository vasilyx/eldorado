<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 07.02.17
 * Time: 16:22
 */

namespace AdminBundle\Controller;


use AdminBundle\Validator\Constraints\ClearedMetadata;
use AppBundle\Entity\File;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use LogBundle\Entity\AdminActivityLog;
use LogBundle\Handler\AdminActivityHandler;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @package AdminBundle\Controller
 */
class AdminClientsApiController extends AbstractAdminApiController
{

    /**
     * Returns found paginated Users list or all user if search string is empty
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Client",
     *     description="Returns found paginated Users list or all user if search string is empty",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *      {
     *          "name"        =   "search_string",
     *          "dataType"    =   "string",
     *          "required"    =    false,
     *          "description" =   "search string"
     *      },
     *      {
     *          "name"        =   "limit",
     *          "dataType"    =   "string",
     *          "required"    =    false,
     *          "description" =   "items per page"
     *      },
     *      {
     *          "name"        =   "page",
     *          "dataType"    =   "string",
     *          "required"    =    false,
     *          "description" =   "page number"
     *      },
     *      {
     *          "name"        =   "order_field",
     *          "dataType"    =   "string",
     *          "required"    =    false,
     *          "description" =   "order field"
     *      },
     *      {
     *          "name"        =   "order_direction",
     *          "dataType"    =   "string",
     *          "required"    =    false,
     *          "description" =   "order direction (ASC, DESC)"
     *      }
     *     }
     * )
     * @Get("/clients/search")
     * @return View
     *
     * @param Request $request
     */
    public function getClientsSearchAction(Request $request)
    {
        if (!$this->isGranted('PERMISSION_CLIENT_VIEW')) {
            return $this->getResponse(
                [
                    'code'    => 'admin.viewClients.accessDenied',
                    'message' => 'Access denied to view clients'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }

        $userManager = $this->get('whs.manager.user_manager');

        $limit = $request->query->get('limit', 10);
        $page = $request->query->get('page', 1);
        $searchString = $request->query->get('search_string');
        $orderField = $request->query->get('order_field');
        $orderDirection = $request->query->get('order_direction');

        $result = $userManager->searchPaginatedList($limit, $page, $orderField, $orderDirection, $searchString);

        return $this->getResponse($result);
    }

    /**
     *
     * Returns addresses for user with specified id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Client",
     *     description="Returns addresses for user with specified id",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     * @return View
     *
     * @Get("/clients/{id}/addresses")
     * @param User $user
     */
    public function getClientsAddressesAction(User $user)
    {
        if (!$this->isGranted('PERMISSION_CLIENT_VIEW')) {
            return $this->getResponse(
                [
                    'code'    => 'admin.viewClients.accessDenied',
                    'message' => 'Access denied to view clients'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }
        $userAddressManager = $this->get('whs.manager.user_address_manager');
        $userAddresses = $userAddressManager->getAddressList($user);
        return $this->getResponse($userAddresses);
    }

    /**
     *
     * Returns communications for user with specified id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Client",
     *     description="Returns communications for user with specified id",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     * @return View
     *
     * @Get("/clients/{id}/communications")
     * @param User $user
     */
    public function getClientCommunicationsAction(User $user)
    {
        if (!$this->isGranted('PERMISSION_CLIENT_VIEW')) {
            return $this->getResponse(
                [
                    'code'    => 'admin.viewClients.accessDenied',
                    'message' => 'Access denied to view clients'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }
        $userCommunicationsManager = $this->get('whs.manager.user_communication_manager');
        $userCommunications = $userCommunicationsManager->getCommunicationList($user);
        return $this->getResponse($userCommunications);
    }


    /**
     *
     * Returns personal info for user with specified id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Client",
     *     description="Returns personal info for user with specified id",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     * @return View
     *
     * @Get("/clients/{id}/details")
     * @param User $user
     */
    public function getClientPersonalAction(User $user)
    {
        if (!$this->isGranted('PERMISSION_CLIENT_VIEW')) {
            return $this->getResponse(
                [
                    'code'    => 'admin.viewClients.accessDenied',
                    'message' => 'Access denied to view clients'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }
        $details = $this->get('whs.manager.user_details_manager')->getDetailsForUser($user);
        $detailsArr = json_decode($this->get('serializer')->serialize($details, 'json'), true);
        $detailsArr['dependents'] = $user->getDependents();
        return $this->getResponse($detailsArr);
    }

    /**
     * Clear security for user (create log in database **AdminActivityLog table**). This method is under the whadmin firewall (see the security.yml file)
     *
     * <pre>
     * If request has 'Cleared' type, it must contain extra-parameter "metadata". It's associative array with cleared user details (at least three):
     *
     * "metadata":
     *   {
     *     "full_name": "Peter Noble",
     *     "dob": 19690423,
     *     "post_code": "CR8 2GK",
     *     "age": 47,
     *     "screenname": "Nobility"
     *   }
     * </pre>
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Client",
     *     description="Clear security",
     *      parameters={
     *          {"name" = "user_id", "dataType"="integer", "required"=true, "description" = "User_id. Should be unique."},
     *          {"name" = "type", "dataType"="boolean", "required"=true, "description"="1 - 'Admin only' type, 0 - 'Cleared' type"},
     *          {"name" = "metadata", "dataType"="associative array", "required"=false, "description"="Metadata (checkboxed user datails). {'field':'text'}"},
     *     },
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Post("/clear_security")
     *
     * @param Request $request
     * @return View
     */
    public function postClearSecurityAction(Request $request)
    {
        if ($this->isGranted('PERMISSION_CLEARSECURITY_CREATE')) {
            $user = $this->get('doctrine.orm.entity_manager')->getRepository('AppBundle:User')->find($request->request->get('user_id'));

            if ($request->request->get('type')) {
                $this->get('monolog.logger.admin_activity')->info('',
                    array(
                        AdminActivityHandler::CONTEXT_ADMIN  => $this->getUser(),
                        AdminActivityHandler::CONTEXT_USER   => $user,
                        AdminActivityHandler::CONTEXT_CODE   => AdminActivityLog::CLEAR_SECURITY_ADMIN_ONLY_ACTION,
                        AdminActivityHandler::CONTEXT_ACTION => AdminActivityLog::ADMIN_TYPE
                    )
                );
            } else {
                $validationErrors = $this->get('validator')->validate($request->request->get('metadata'), new ClearedMetadata());

                if (count($validationErrors) > 0) {
                    return $this->getValidationErrorResponse($validationErrors);
                }

                $this->get('monolog.logger.admin_activity')->info('',
                    array(
                        AdminActivityHandler::CONTEXT_ADMIN    => $this->getUser(),
                        AdminActivityHandler::CONTEXT_USER     => $user,
                        AdminActivityHandler::CONTEXT_CODE     => AdminActivityLog::CLEAR_SECURITY_CLEARED_ACTION,
                        AdminActivityHandler::CONTEXT_ACTION   => AdminActivityLog::ADMIN_TYPE,
                        AdminActivityHandler::CONTEXT_METADATA => json_encode($request->request->get('metadata'))
                    )
                );
            }

            return $this->getResponse(null);
        }
        return $this->getResponse(
            [
                'code'    => 'admin.clear_security.accessDenied',
                'message' => 'Clear security access denied'
            ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
        );
    }

    /**
     *
     * Returns clear security data for use with specified id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Client",
     *     description="Returns clear security data for use with specified id",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     * @return View
     *
     * @Get("/clear_security/{id}")
     * @param User $user
     */
    public function getClearSecurityAction(User $user)
    {
        if (!$this->isGranted('PERMISSION_CLEARSECURITY_VIEW')) {
            return $this->getResponse(
                [
                    'code'    => 'admin.clear_security.accessDenied',
                    'message' => 'Clear security access denied',
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }
        $addressManager = $this->get('whs.manager.user_address_manager');
        $response = [
            'full_name'  => $user->getDetails()->getFirstName() . ' ' . $user->getDetails()->getLastName(),
            'dob'        => $user->getDetails()->getBirthdate()->format('Ymd'),
            'post_code'  => !empty($addressManager->getCurrentAddress($user)) ? $addressManager->getCurrentAddress($user)->getPostalCode() : '-',
            'age'        => (new \DateTime())->diff($user->getDetails()->getBirthdate())->format('%y'),
            'screenname' => !empty($user->getDetails()->getScreenName()) ? $user->getDetails()->getScreenName() : '-',
        ];

        return $this->getResponse($response);
    }

    /**
     *
     * Method returns activity logs for user/client with page number and limit (amount of logs on the one page)
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Client",
     *     description="Method returns activity logs for user/client with page number and limit (amount of logs on the one page)",
     *     parameters={
     *      {"name"="page", "dataType"="string", "required"=false, "description"="Page number"},
     *      {"name"="limit", "dataType"="string", "required"=false, "description"="Amount of logs on the one page"},
     *     },
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/clients/{id}/activity")
     *
     * @param Request $request
     * @param User $user
     *
     * @return View
     */
    public function getClientsActivityLogsAction(Request $request, User $user)
    {
        if (!$this->isGranted('PERMISSION_CLIENT_VIEW')) {
            return $this->getResponse(
                [
                    'code'    => 'admin.viewClients.accessDenied',
                    'message' => 'Access denied to view clients'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }
        $userManager = $this->get('whs.manager.user_manager');

        $logs = $userManager->getLogsForUser(
            $user,
            $request->get('page', 1),
            $request->get('limit', 10)
        );

        return $this->getResponse($logs);
    }

    /**
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Client",
     *     description="Returns paginated list or borrowers sorted by ID",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BASIC base64EncodedUsernamePassword', where base64EncodedUsernamePassword is string 'username:password' encoded with base64 algorithm",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *      {
     *          "name"        =   "limit",
     *          "dataType"    =   "string",
     *          "required"    =    false,
     *          "description" =   "items per page"
     *      },
     *      {
     *          "name"        =   "page",
     *          "dataType"    =   "string",
     *          "required"    =    false,
     *          "description" =   "page number"
     *      },
     *      {
     *          "name"        =   "order_field",
     *          "dataType"    =   "string",
     *          "required"    =    false,
     *          "description" =   "order field"
     *      },
     *      {
     *          "name"        =   "order_direction",
     *          "dataType"    =   "string",
     *          "required"    =    false,
     *          "description" =   "order direction (ASC, DESC)"
     *      }
     *     }
     * )
     *
     * @param Request $request
     *
     * @Get("/clients/borrowers")
     * @return View
     */
    public function getBorrowersAction(Request $request)
    {
        if (!$this->isGranted('PERMISSION_BORROWER_VIEW')) {
            return $this->getResponse(
                [
                    'code'    => 'admin.viewBorrowers.accessDenied',
                    'message' => 'Access denied to view clients'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }

        $productManager = $this->get('whadmin.product_manager');

        $limit = $request->query->get('limit', 10);
        $page = $request->query->get('page', 1);
        $orderField = $request->query->get('order_field');
        $orderDirection = $request->query->get('order_direction');

        $result = $productManager->getPaginatedBorrowers($limit, $page, $orderField, $orderDirection);

        return $this->getResponse($result);
    }

    /**
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Client",
     *     description="Returns paginated list or investors sorted by ID",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BASIC base64EncodedUsernamePassword', where base64EncodedUsernamePassword is string 'username:password' encoded with base64 algorithm",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *      {
     *          "name"        =   "limit",
     *          "dataType"    =   "string",
     *          "required"    =    false,
     *          "description" =   "items per page"
     *      },
     *      {
     *          "name"        =   "page",
     *          "dataType"    =   "string",
     *          "required"    =    false,
     *          "description" =   "page number"
     *      },
     *      {
     *          "name"        =   "order_field",
     *          "dataType"    =   "string",
     *          "required"    =    false,
     *          "description" =   "order field"
     *      },
     *      {
     *          "name"        =   "order_direction",
     *          "dataType"    =   "string",
     *          "required"    =    false,
     *          "description" =   "order direction (ASC, DESC)"
     *      }
     *     }
     * )
     *
     * @param Request $request
     *
     * @Get("/clients/investors")
     * @return View
     */
    public function getInvestorsAction(Request $request)
    {
        if (!$this->isGranted('PERMISSION_BORROWER_VIEW')) {
            return $this->getResponse(
                [
                    'code'    => 'admin.viewBorrowers.accessDenied',
                    'message' => 'Access denied to view borrowers'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }

        $productManager = $this->get('whadmin.product_manager');

        $limit = $request->query->get('limit', 10);
        $page = $request->query->get('page', 1);
        $orderField = $request->query->get('order_field');
        $orderDirection = $request->query->get('order_direction');

        $result = $productManager->getPaginatedInvestors($limit, $page, $orderField, $orderDirection);

        return $this->getResponse($result);
    }

    /**
     * This method return binary of contract **PDF** file by loan_apply_id / ivest_offer_id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Client",
     *     description="Returns binary PDF",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BASIC base64EncodedUsernamePassword', where base64EncodedUsernamePassword is string 'username:password' encoded with base64 algorithm",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="invest_offer_id", "dataType"="string", "required"=false, "description"="Invest offer ID"},
     *     }
     * )
     *
     * @Get("/clients/contract/{loan_apply_id}/pdf")
     * @ParamConverter("loanApply", options={"mapping": {"loan_apply_id" : "id"}})
     *
     *
     * @param LoanApply $loanApply
     * @param Request $request
     * @return Response
     */
    public function getContractPdfFileAction(LoanApply $loanApply, Request $request)
    {
        $user = $loanApply->getUser();

        if (!$user->getActive()) {
            return $this->getResponse('Borrower isn\'t active', self::STATUS_ERROR);
        }

        if (!in_array($loanApply->getStatus(), [LoanApply::STATUS_ACTIVE, LoanApply::STATUS_SETTLED])) {
            return $this->getResponse('LoanApply hasn\'t active or settled status', self::STATUS_ERROR);
        }

        $investOfferId = $request->get('invest_offer_id');

        $investOffer = $investOfferId ? $this->getDoctrine()->getRepository(InvestOffer::class)->find($investOfferId) : null;

        if (!empty($investOffer) && !$investOffer->getUser()->getActive()) {
            return $this->getResponse('Lender isn\'t active', self::STATUS_ERROR);
        }

        $file = $this->get('whs.manager.file_manager')->getPdfFile($loanApply, $investOffer, File::ACCEPTANCE_BORROWER_AGREEMENT_TYPE);

        if (empty($file)) {
            return $this->getResponse('Contract is not ready yet', self::STATUS_ERROR, Response::HTTP_NOT_FOUND);
        }

        return $this->getPdfResponse($file);
    }

    /**
     * This method returns PDF response
     *
     * @param File $file
     * @return Response
     */
    protected function getPdfResponse(File $file)
    {
        $path = $this->get('kernel')->getRootDir() . $file->getLocation();

        $response = new BinaryFileResponse($path);

        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();

        if($mimeTypeGuesser->isSupported()){
            // Guess the mimetype of the file according to the extension of the file
            $response->headers->set('Content-Type', $mimeTypeGuesser->guess($path));
        } else{
            // Set the mimetype of the file manually, in this case for a text file is text/plain
            $response->headers->set('Content-Type', 'application/pdf');
        }

        // Set content disposition inline of the file
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'file.pdf'
        );

        return $response;
    }
}