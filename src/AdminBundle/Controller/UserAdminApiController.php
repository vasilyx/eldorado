<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 07.02.17
 * Time: 16:22
 */

namespace AdminBundle\Controller;

use AdminBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\View\View;
use LogBundle\Entity\AdminActivityLog;
use LogBundle\Handler\AdminActivityHandler;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Authorization flow consists from the next steps.
 *
 * 1 step
 * The first user must be added to a database manually (with migrations for example). See AdminBundle\Entity\User.php and AdminBundle\Resourses\config\doctrine\User.orm.yml to find out a fields structure. To generate a password from a plain password use an AdminBundle\Manager\UserManager->encodePassword($plainPassword) method. Remember the plain password because only encoded password is stored in the database.
 *
 * 2 step
 * Use UserAdminApiController->getToken() to get token for the user with a username and a plainPassword. This method is not under the security firewall. The username and the plainPassword should be transferred in the Authorization http header in the next format (one space between values)
 * Authorization: BASIC base64(username:plainPassword)
 * The method returns a token if the user is existent and the error response otherwise.
 *
 * 3 step
 * If an admin method under a security firewall (see security.yml section _firewalls_) it is necessary to use token to get access. Token must be transferred in the Authorization http header in the next format (one space between values)
 * Authorization: BEARER tokenFromGetTokenMethod
 * If token is not valid or not existent will be returned error response.
 *
 *
 * Class UserAdminApiController
 * @package AdminBundle\Controller
 */
class UserAdminApiController extends AbstractAdminApiController
{

    /**
     * Signs in user by username and password. Uses the AUTHORIZATION http header, see http://www.ietf.org/rfc/rfc2617.txt.
     * This method is not under a security firewall.
     * Authorization: BASIC credentials
     * Error codes:
     * admin.token.passwordIsExpired - password is expired
     * admin.token.badCredentials - any other errors
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Token",
     *     description="Returns token by username and password",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BASIC base64EncodedUsernamePassword', where base64EncodedUsernamePassword is string 'username:password' encoded with base64 algorithm",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @param Request $request
     *
     * @return View
     */
    public function getTokenAction(Request $request)
    {
        $unauthorizedResponse = $this->getResponse([
            'code'    => 'admin.token.badCredentials',
            'message' => Response::$statusTexts[Response::HTTP_UNAUTHORIZED]
        ], static::STATUS_ERROR, Response::HTTP_UNAUTHORIZED);

        if (null === $header = $request->headers->get('authorization')) {
            return $unauthorizedResponse;
        }

        $credentials = $this->getCredentialsFromHeader($header);
        if ($credentials) {
            $um = $this->get('whadmin.user_manager');
            $username = $credentials['username'];
            $plainPassword = $credentials['plainPassword'];

            /**
             * Now we have username and plain password
             */
            if ($user = $um->findOneByUsername($username)) {
                if ($um->isPlainPasswordValid($user, $plainPassword)) {
                    if (!$um->isPasswordRequestNonExpired($user)) {
                        return $this->getResponse([
                            'code'    => 'admin.token.passwordIsExpired',
                            'message' => Response::$statusTexts[Response::HTTP_UNAUTHORIZED]
                        ], static::STATUS_ERROR, Response::HTTP_UNAUTHORIZED);
                    }
                    $sm = $this->get('whadmin.session_manager');
                    $session = $sm->createSession($user);

                    $this->get('monolog.logger.admin_activity')->info('',
                        array(
                            AdminActivityHandler::CONTEXT_ADMIN  => $user,
                            AdminActivityHandler::CONTEXT_USER   => null,
                            AdminActivityHandler::CONTEXT_CODE   => AdminActivityLog::LOGIN_ACTION,
                            AdminActivityHandler::CONTEXT_ACTION => AdminActivityLog::ADMIN_TYPE,
                        )
                    );
                    return $this->getResponse([
                        'token' => $session->getToken()
                    ]);
                }
            }
        }

        return $unauthorizedResponse;
    }

    /**
     * Logout Admin user, delete session record from admin's session storage (DB table)
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Token",
     *     description="Logout Admin user, delete session record from admin's session storage (DB table)",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @param Request $request
     * @Delete("token")
     *
     * @return View
     */
    public function deleteTokenAction(Request $request)
    {
        $authToken = $request->headers->get('authorization');
        $tokenStr = explode(' ', $authToken)[1];
        $session = $this->get('whadmin.session_manager')->findOneByToken($tokenStr);
        $em = $this->getDoctrine()->getManager();
        $em->remove($session);
        $em->flush();
        $user = $this->getUser();
        $this->get('monolog.logger.admin_activity')->info('',
            array(
                AdminActivityHandler::CONTEXT_ADMIN  => $user,
                AdminActivityHandler::CONTEXT_USER   => null,
                AdminActivityHandler::CONTEXT_CODE   => AdminActivityLog::LOGIN_ACTION,
                AdminActivityHandler::CONTEXT_ACTION => AdminActivityLog::ADMIN_TYPE,
            )
        );

        return $this->getResponse(['Logouted']);
    }

    /**
     * Registers user by user parameters. This method is under whadmin firewall (see security.yml).
     * Authorization: BEARER token
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="User",
     *     description="Posts user by username and plain_password",
     *     parameters={
     *          {"name" = "username", "dataType"="string", "required"=true, "format"="email", "description" = "Should be unique"},
     *          {"name" = "plain_password", "dataType"="string", "required"=true, "format"="length 12 symbols, at least 1 number, 1 alpha, 1 special symbol", "description" = "A plain password"}
     *     },
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @param Request $request
     *
     * @return View
     */
    public function postUserAction(Request $request)
    {
        /**
         * Only super admin can create users
         */
        if ($this->isGranted('PERMISSION_USER_CREATE')) {
            $um = $this->get('whadmin.user_manager');

            /**
             * Deserialize user from request body
             * @var User $user
             */
            $user = $this->getSerialiser()->deserialize($request->getContent(), User::class, 'json');

            $validationErrors = $this->get('validator')->validate($user);

            if (count($validationErrors) > 0) {
                return $this->getValidationErrorResponse($validationErrors);
            }

            $um->updateUser($user);

            /**
             * We make response array manually to have full control
             */
            return $this->getResponse([
                'id'       => $user->getId(),
                'username' => $user->getUsername(),
                'roles'    => $user->getRoles()
            ]);
        }

        return $this->getResponse(
            [
                'code'    => 'admin.users.accessDenied',
                'message' => 'Access denied'
            ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
        );
    }

    /**
     * Returns all not deleted users. This method is under the whadmin firewall (see the security.yml file). Output format:
     *
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="User",
     *     description="Get users",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @return View
     */
    public function getUsersAction()
    {
        if ($this->isGranted('PERMISSION_USER_VIEW')) {
            $um = $this->get('whadmin.user_manager');
            $rpm = $this->get('whadmin.role_permission_manager');

            $users = $um->findAllNonDeleted();

            $result = [];
            if ($users) {
                foreach ($users as $user) {

                    $result[$user->getId()]['username'] = $user->getUsername();
                    $result[$user->getId()]['permissions'] = $um->getPermissions($user);
                    $result[$user->getId()]['permission_groups'] = $rpm->getNotDeniedPermissionsGroups($user);
                    $result[$user->getId()]['enabled'] = $user->isEnabled();
                }
            }

            return $this->getResponse($result);
        }
        return $this->getResponse(
            [
                'code'    => 'admin.users.accessDenied',
                'message' => 'Users view access denied'
            ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
        );
    }


    /**
     * Updates a password for a user. If the authorization scheme is Basic, use username and password to find the user (in case of expired password). If authorization scheme is Forgotten, use forgottenPasswordToken to find the user (in case of resetting password).
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="User",
     *     description="Updates a password for a user. If the authorization scheme is Basic, use username and password to find the user. If authorization scheme is Forgotten, use forgottenPasswordToken to find the user.",
     *     parameters={
     *          {"name" = "plain_password", "dataType"="string", "required"=true, "format"="length 12 symbols, at least 1 number, 1 alpha, 1 special symbol", "description" = "The new plain password"},
     *          {"name" = "repeat_password", "dataType"="string", "required"=true, "format"="length 12 symbols, at least 1 number, 1 alpha, 1 special symbol", "description" = "Repeat the new plain password"}
     *     },
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "May content the next string: 'BASIC base64EncodedUsernamePassword', where base64EncodedUsernamePassword is string 'username:password' encoded with base64 algorithm. Or may content: 'FORGOTTEN forgottenPasswordToken' if forgotten password is reset",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @param Request $request
     *
     * @return View
     */
    public function putUserPasswordAction(Request $request)
    {
        $unauthorizedResponse = $this->getResponse([
            'code'    => 'admin.authentication.badCredentials',
            'message' => Response::$statusTexts[Response::HTTP_UNAUTHORIZED]
        ], static::STATUS_ERROR, Response::HTTP_UNAUTHORIZED);

        $content = json_decode($request->getContent(), true);

        $newPlainPassword = isset($content['plain_password']) ? $content['plain_password'] : null;
        $repeatPassword = isset($content['repeat_password']) ? $content['repeat_password'] : null;
        $user = null;

        if ($newPlainPassword !== $repeatPassword) {
            return $this->getResponse([
                'code'    => 'admin.password.notMatch',
                'message' => 'Password and repeat password are not match'], static::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        if (null === $header = $request->headers->get('authorization')) {
            return $unauthorizedResponse;
        }
        $um = $this->get('whadmin.user_manager');

        /**
         * If authorization scheme is Basic, use username and plainPassword to find the user
         */
        $credentials = $this->getCredentialsFromHeader($header);
        if ($credentials) {

            $username = $credentials['username'];
            $plainPassword = $credentials['plainPassword'];
            $user = $um->findOneByUsername($username);

            if (!$um->isPlainPasswordValid($user, $plainPassword)) {
                return $unauthorizedResponse;
            }
        }

        /**
         * If authorization scheme is Forgotten, use forgottenPasswordToken to find the user
         */
        $forgottenPasswordToken = $this->getForgottenPasswordTokenFromHeader($header);
        if ($forgottenPasswordToken) {
            $user = $um->findOneByForgottenPasswordToken($forgottenPasswordToken);
        }

        if ($user) {
            if ($forgottenPasswordToken) {
                $user->setForgottenPasswordToken(null);
            }

            $user->setPlainPassword($newPlainPassword);

            $validationErrors = $this->get('validator')->validate($user);

            if (count($validationErrors) > 0) {
                return $this->getValidationErrorResponse($validationErrors);
            }

            $um->updateUser($user);

            return $this->getResponse([]);
        }

        return $unauthorizedResponse;
    }

    /**
     * Puts forgotten_password_token to user with username from Authorization header and sends the email to user.
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="User",
     *     description="Puts forgotten_password_token to user with username from Authorization header and sends the email to user",
     *     parameters={
     *          {"name" = "url", "dataType"="string", "required"=true, "format"="string", "description" = "Url for link in the email"},
     *     },
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BASIC base64EncodedUsernamePassword', where base64EncodedUsernamePassword is string 'username:''' encoded with base64 algorithm. PAY ATTENTION: PASSWORD IS EMPTY",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @param Request $request
     * @return View
     */
    public function putUserForgotten_password_tokenAction(Request $request)
    {
        $unauthorizedResponse = $this->getResponse([
            'code'    => 'admin.authentication.badCredentials',
            'message' => Response::$statusTexts[Response::HTTP_UNAUTHORIZED]
        ], static::STATUS_ERROR, Response::HTTP_UNAUTHORIZED);

        $content = json_decode($request->getContent(), true);

        $url = isset($content['url']) ? $content['url'] : null;

        if (null === $url) {
            return $this->getResponse([
                'code'    => 'admin.forgottenPasswordToken.url.isBlank',
                'message' => 'Url should not be blank'], static::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        if (null === $header = $request->headers->get('authorization')) {
            return $unauthorizedResponse;
        }

        $credentials = $this->getCredentialsFromHeader($header);
        if ($credentials) {
            $um = $this->get('whadmin.user_manager');
            $username = $credentials['username'];

            /**
             * Now we have username
             */
            if ($user = $um->findOneByUsername($username)) {
                $sm = $this->get('whadmin.session_manager');

                $user->setForgottenPasswordToken($sm->generateToken());

                $this->get('doctrine.orm.entity_manager')->flush();

                /**
                 * @var \Swift_Message $message
                 */
                $message = \Swift_Message::newInstance()
                    ->addFrom($this->getParameter('mailer_support'))
                    ->addTo($user->getUsername())
                    ->setBody($request->getSchemeAndHttpHost() . $url . '?t=' .
                        $user->getForgottenPasswordToken());

                $this->get('mailer')->send($message);

                return $this->getResponse([]);
            }
        }

        return $unauthorizedResponse;
    }

    /**
     * In the header we have "Authorization: Credentials"
     * Credentials example: "BASIC base64_encoded_parameter", where
     * BASIC - authorization scheme
     * base64_encoded_parameter - base64 encoded PARAMETER, where
     * PARAMETER "username:password"
     *
     * @param $header
     * @return array|null
     */
    private function getCredentialsFromHeader($header)
    {

        if (!isset($header)) {
            return null;
        }

        $credentialsAsArray = explode(' ', $header);
        $authorizationScheme = $credentialsAsArray[0];
        if ('BASIC' === strtoupper($authorizationScheme)) {
            $authorizationParameter = base64_decode($credentialsAsArray[1]);
            $authorizationParameterAsArray = explode(':', $authorizationParameter);
            $username = $authorizationParameterAsArray[0];
            $plainPassword = $authorizationParameterAsArray[1];
            return ['username' => $username, 'plainPassword' => $plainPassword];
        }

        return null;
    }

    /**
     * @param $header
     * @return null
     */
    private function getForgottenPasswordTokenFromHeader($header)
    {
        if (!isset($header)) {
            return null;
        }

        $credentialsAsArray = explode(' ', $header);
        $authorizationScheme = $credentialsAsArray[0];
        if ('FORGOTTEN' === strtoupper($authorizationScheme)) {
            $token = $credentialsAsArray[1];

            return $token;
        }

        return null;
    }

    /**
     * Method returns activity logs for admin with page number and limit (amount of logs on the one page)
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Admin",
     *     description="Method returns activity logs for admin with page number and limit (amount of logs on the one page)",
     *     parameters={
     *      {"name"="page", "dataType"="string", "required"=false, "description"="Page number"},
     *      {"name"="limit", "dataType"="string", "required"=false, "description"="Amount of logs on the one page"},
     *     },
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/admins/{id}/activity")
     *
     * @param Request $request
     * @param User $admin
     *
     * @return View
     */
    public function getAdminActivityLogsAction(Request $request, User $admin)
    {
        //TODO: create and check permission to view admin logs
        $adminManager = $this->get('whadmin.user_manager');
        $logs = $adminManager->getLogsForUser(
            $admin,
            $request->get('page', 1),
            $request->get('limit', 10)
        );

        return $this->getResponse($logs);

    }

    /**
     * Method returns activity logs for current admin with page number and limit (amount of logs on the one page)
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Admin",
     *     description="Method returns activity logs for current admin with page number and limit (amount of logs on the one page)",
     *     parameters={
     *      {"name"="page", "dataType"="string", "required"=false, "description"="Page number"},
     *      {"name"="limit", "dataType"="string", "required"=false, "description"="Amount of logs on the one page"},
     *     },
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/admin/activity")
     *
     * @param Request $request
     *
     * @return View
     */
    public function getCurrentAdminActivityLogsAction(Request $request)
    {
        $adminManager = $this->get('whadmin.user_manager');
        $logs = $adminManager->getLogsForUser(
            $this->getUser(),
            $request->get('page', 1),
            $request->get('limit', 10)
        );

        return $this->getResponse($logs);

    }
}