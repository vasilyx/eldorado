<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 14.02.17
 * Time: 16:07
 */

namespace AdminBundle\Controller;


use AdminBundle\Entity\RolePermission;
use AdminBundle\Security\Permission\MaskBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RolePermissionAdminApiController extends AbstractAdminApiController
{
    /**
     * Returns permissions for a role or for the all roles if parameter role is null
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Permissions",
     *     description="Get permissions",
     *     parameters={
     *          {"name" = "role", "dataType"="string", "required"=false, "format"="ROLE_SOMETHING_ELSE", "description" = "Should have symfony roles format"}
     *     },
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @param Request $request
     * @return array|\FOS\RestBundle\View\View
     */
    public function getPermissionsAction(Request $request)
    {
        if ($this->isGranted('PERMISSION_PERMISSION_VIEW')) {
            $result = [];
            $role = $request->get('role');
            $permissions = $this->get('whadmin.role_permission_manager')->findAllForRole($role);
            foreach ($permissions as $permission) {
                $result[] = [
                    'id' => $permission->getId(),
                    'role' => $permission->getRole(),
                    'permission' => $permission->getPermission(),
                    'mask' => $permission->getMask()
                ];
            }
            return $this->getResponse($result);
        }

        return $this->getResponse([
            'code' => 'admin.permissions.accessDenied',
            'message' => 'Access denied to view permissions'
        ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN);
    }

    /**
     * Patchs permission
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Permissions",
     *     description="Patch permission",
     *     parameters={
     *          {"name" = "id", "dataType"="integer", "required"=true, "format"="/d", "description" = "Id of the patched permission"},
     *          {"name" = "mask", "dataType"="string", "required"=false, "format"="ncrud", "description"="Permission mask: n - no permission, c - create, r - read (view), u - update (edit), d - delete"}
     *     },
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function patchPermissionAction(Request $request)
    {
        if ($this->isGranted('PERMISSION_PERMISSION_UPDATE')) {

            $id = $request->get('id');
            if (!$id) {
                return $this->getResponse([
                    'code' => 'admin.permissions.id.isBlank',
                    'message' => 'Permission id should not be blank'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST);
            }
            $mask = $request->get('mask');
            $mask = $this->parseSymbolicMask($mask);
            /**
             * Deserialize user from request body
             * @var RolePermission $rolePermission
             */
            $rolePermission = $this->getDoctrine()->getRepository(RolePermission::class)->findOneBy(['id' => $id]);

            if (null !== $mask) {
                $rolePermission->setMask($mask);
            }

            $validationErrors = $this->get('validator')->validate($rolePermission);

            if (count($validationErrors) > 0) {
                return $this->getValidationErrorResponse($validationErrors);
            }

            $objectManager = $this->getDoctrine()->getManager();
            $objectManager->flush();

            return $this->getResponse($rolePermission);
        }

        return $this->getResponse([
            'code' => 'admin.permissions.accessDenied',
            'message' => 'Access denied to update permissions'
        ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN);
    }

    /**
     * @param $mask
     * @return int
     */
    private function parseSymbolicMask($mask)
    {
        if ($mask) {
            $maskBuilder = MaskBuilder::create();

            if (stripos($mask, 'c') !== false) {
                $maskBuilder->addCreate();
            }

            if (stripos($mask, 'r') !== false) {
                $maskBuilder->addView();
            }

            if (stripos($mask, 'u') !== false) {
                $maskBuilder->addUpdate();
            }

            if (stripos($mask, 'd') !== false) {
                $maskBuilder->addDelete();
            }

            return $maskBuilder->getMask();
        }

        return null;
    }
}