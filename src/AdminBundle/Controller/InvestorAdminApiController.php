<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 17.05.2018
 * Time: 15:26
 */

namespace AdminBundle\Controller;


use AdminBundle\Entity\AdminDecision;
use AdminBundle\Manager\AdminDecisionManager;
use AdminBundle\Response\InvestorPortfolioSummary;
use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Manager\ApiCallsManager;
use AppBundle\Manager\LoanApplyManager;
use AppBundle\Manager\UserManager;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class InvestorAdminApiController extends AbstractAdminApiController
{
    /**
     * Returns borrower details by specified id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Investor",
     *     description="The method returns investor details",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/investors/{quote_id}/details")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     * @internal param User $user
     */
    public function getInvestorDetailsAction(QuoteApply $quoteApply)
    {
        if (!$this->isGranted('PERMISSION_INVESTOR_VIEW')) {
            return $this->getResponse(
                [
                    'code'    => 'admin.viewInvestors.accessDenied',
                    'message' => 'Access denied to view investors'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }

        $user = $quoteApply->getUser();

        if (empty($user)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.userNotFound',
                    'message' => 'User for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        /** @var UserManager $userManager */
        $userManager = $this->get('whs.manager.user_manager');
        $userDetails = $userManager->getUserPersonalInfo($user);

        return $this->getResponse($userDetails);
    }

    /**
     * Returns addresses for borrower with specified id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Investor",
     *     description="The method returns investor addresses",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/investors/{quote_id}/addresses")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getInvestorAddressesAction(QuoteApply $quoteApply)
    {
        if (!$this->isGranted('PERMISSION_INVESTOR_VIEW')) {
            return $this->getResponse(
                [
                    'code'    => 'admin.viewInvestors.accessDenied',
                    'message' => 'Access denied to view investors'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }

        $user = $quoteApply->getUser();

        if (empty($user)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.userNotFound',
                    'message' => 'User for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }$user = $quoteApply->getUser();

        if (empty($user)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.userNotFound',
                    'message' => 'User for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $userAddressManager = $this->get('whs.manager.user_address_manager');
        $userAddresses = $userAddressManager->getAddressList($user);

        return $this->getResponse($userAddresses);
    }

    /**
     * Returns ID Check for investor with specified quote id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Investor",
     *     description="The method returns investor ID check",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/investors/{quote_id}/id_check")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getInvestorIDCheckAction(QuoteApply $quoteApply)
    {
        if (!$this->isGranted('PERMISSION_INVESTOR_VIEW')) {
            return $this->getResponse(
                [
                    'code'    => 'admin.viewInvestors.accessDenied',
                    'message' => 'Access denied to view investors'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }

        $user = $quoteApply->getUser();

        if (empty($user)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.userNotFound',
                    'message' => 'User for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $callvalidateResponse = $this->getDoctrine()->getRepository(CallCreditAPICalls::class)->getLatestCallValidateForInvestor($user);

        return $this->getResponse($callvalidateResponse);
    }


    /**
     * Returns investor incomes with specified id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Investor",
     *     description="The method returns investor income",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/investors/{quote_id}/finances")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getInvestorFinancesAction(QuoteApply $quoteApply)
    {
        if (!$this->isGranted('PERMISSION_BORROWER_VIEW')) {
            return $this->getResponse(
                [
                    'code'    => 'admin.viewBorrowers.accessDenied',
                    'message' => 'Access denied to view borrowers'
                ], self::STATUS_ERROR, Response::HTTP_FORBIDDEN
            );
        }

        $user = $quoteApply->getUser();

        if (empty($user)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.userNotFound',
                    'message' => 'User for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $quoteIncome = $this->get('whs.manager.quote_income_manager')->getQuoteIncome($user);

        return $this->getResponse($quoteIncome);
    }

    /**
     * Returns quotation details for lender with specified quote id
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Investor",
     *     description="The method returns lender quotation details",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/investors/{quote_id}/quotation_details")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getInvestorQuoteDetailsAction(QuoteApply $quoteApply)
    {
        return $this->getResponse($quoteApply);
    }

    /**
     *
     * This method set admin decision on investor ID check. Created by Shatilenya Vladislav.
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Investor",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BASIC base64EncodedUsernamePassword', where base64EncodedUsernamePassword is string 'username:password' encoded with base64 algorithm",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {
     *              "name" = "api_call_id",
     *              "dataType" = "integer",
     *              "required" = true,
     *              "description" = "API Call identifier"
     *          },
     *          {
     *              "name" = "decision",
     *              "dataType" = "integer",
     *              "required" = true,
     *              "description" = "Admin decision 100 - Approve, 200 - Decline"
     *          },
     *          {
     *              "name" = "comment",
     *              "dataType" = "textarea",
     *              "required" = true,
     *              "description" = "Comment"
     *          }
     *     }
     * )
     *
     * @param Request $request
     * @return View
     *
     * @Post("/investor/decision")
     */
    public function postInvestorAdminDecisionAction(Request $request)
    {
        /** @var AdminDecisionManager $adminDecisionManager */
        $adminDecisionManager = $this->get('whadmin.manager.admin_decision');
        $adminDecision = $adminDecisionManager->createAdminDecision($this->getUser(), $request->request->get('api_call_id'), $request->request->get('decision'), $request->request->get('comment'));

        if (empty($adminDecision)) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.Failed',
                    'message' => 'API Call not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }


        $apiCall = $adminDecision->getApiCall();
        $quote = $apiCall->getQuoteApply();

        if (empty($quote)) {
            return $this->getResponse(
                [
                    'code' => 'admin.viewBorrowers.Failed',
                    'message' => 'Quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $entityManager = $this->getDoctrine()->getManager();
        $offers = $this->get('whs.manager.invest_offer_manager')->getPendingInvestOffersForUser($quote->getUser());

        $decisionValue = $request->request->get('decision');

        if ($decisionValue == AdminDecision::APPROVE_DECISION) {
            //Set Quotation status to Active
            $quote->setStatus(QuoteApply::STATUS_ACTIVE);
            //Set Lender pending offers status to Active
            /** @var InvestOffer $offer */
            foreach ($offers as $offer) {
                $offer->setStatus(InvestOffer::STATUS_ACTIVE);
                $entityManager->persist($offer);
            }
        } elseif ($decisionValue == AdminDecision::DECLINE_DECISION) {
            $quote->setStatus(QuoteApply::STATUS_REJECTED);
            /** @var InvestOffer $offer */
            foreach ($offers as $offer) {
                $offer->setStatus(InvestOffer::STATUS_CANCEL);
                $entityManager->persist($offer);
            }
        }
        $entityManager->persist($quote);
        $entityManager->flush();

        return $this->getResponse(['quote_status' => $quote->getStatus()], self::STATUS_SUCCESS, Response::HTTP_CREATED);
    }

    /**
     * Can look at all Investor offers and related agreements
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Investor",
     *     description="The method returns investor offers list",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/investors/{quote_id}/offer_details_list")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getOfferDetailsListAction(QuoteApply $quoteApply)
    {
        $data = $this->get('whadmin.borrower_manager')->getOfferDetailsList($quoteApply);
        return $this->getResponse($data);
    }

    /**
     * Return Investor Portfolio Summary
     *
     * @ApiDoc(
     *     views={"whadmin"},
     *     section="Investor",
     *     description="The method returns investor Portfolio Summary",
     *     headers={
     *          {
     *              "name" = "Authorization",
     *              "description" = "Must content the next string: 'BEARER token'",
     *              "required" = true
     *          }
     *     }
     * )
     *
     * @Get("/investor/{quote_id}/portfolioSummary")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getInvestorPortfolioSummaryAction(QuoteApply $quoteApply)
    {
        if (empty($quoteApply)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.quoteApplyNotFound',
                    'message' => 'QuoteApply for this id not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $user = $quoteApply->getUser();

        $response = [];
        $repository = $this->getDoctrine()->getRepository(Transaction::class);

        foreach (InvestOffer::getInvestmentTypesMap() as $k => $value) {

            $summary = new InvestorPortfolioSummary();
            $summary->setPortfolioAmount($repository->getPortfolioAmount($k, $user));
            $summary->setCashAvailable($repository->getCashAvailable($k, $user));
            $summary->setOfferPending($repository->getOfferPending($k, $user));
            $summary->setInterestEarned($repository->getInterestEarned($k, $user));
            $summary->setOutstandingLoans($repository->getOutstandingLoans($k, $user));

            $response[$value] = $summary;
        }


        return $this->getResponse($response);
    }
}