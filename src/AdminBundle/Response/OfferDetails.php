<?php

namespace AdminBundle\Response;


class OfferDetails
{
    private $name;
    private $offerId;

    /**
     * @return mixed
     */
    public function getOfferId()
    {
        return $this->offerId;
    }

    /**
     * @param mixed $offerId
     */
    public function setOfferId($offerId)
    {
        $this->offerId = $offerId;
    }

    /**
     * @var float offer total amount
     */
    private $amount;

    /**
     * @var float sum of all contracts with Pending status
     */
    private $pending = 0;

    /**
     * @var float sum of all contracts with Active status
     */
    private $active = 0;

    /**
     * @var float sum of all contracts with Settled status
     */
    private $settled = 0;

    private $agreements = [];

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getPending(): float
    {
        return $this->pending;
    }

    /**
     * @param float $pending
     */
    public function setPending(float $pending)
    {
        $this->pending = $pending;
    }

    /**
     * @return float
     */
    public function getActive(): float
    {
        return $this->active;
    }

    /**
     * @param float $active
     */
    public function setActive(float $active)
    {
        $this->active = $active;
    }

    /**
     * @return float
     */
    public function getSettled(): float
    {
        return $this->settled;
    }

    /**
     * @param float $settled
     */
    public function setSettled(float $settled)
    {
        $this->settled = $settled;
    }

    /**
     * @return array
     */
    public function getAgreements(): array
    {
        return $this->agreements;
    }

    /**
     * @param array $agreement
     */
    public function addAgreement(array $agreement)
    {
        array_push($this->agreements, $agreement);
    }
}