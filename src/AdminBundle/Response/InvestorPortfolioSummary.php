<?php
namespace AdminBundle\Response;


class InvestorPortfolioSummary
{
    protected $portfolioAmount = 0;
    protected $cashAmount = 0;
    protected $interestEarned = 0;
    protected $outstandingLoans = 0;
    protected $offerPending= 0;

    /**
     * @return int
     */
    public function getOfferPending(): int
    {
        return $this->offerPending;
    }

    /**
     * @param int $offerPending
     */
    public function setOfferPending(int $offerPending)
    {
        $this->offerPending = abs($offerPending);
    }

    /**
     * @return float
     */
    public function getPortfolioAmount(): float
    {
        return $this->portfolioAmount;
    }

    /**
     * @param float $portfolioAmount
     */
    public function setPortfolioAmount(float $portfolioAmount)
    {
        $this->portfolioAmount = abs($portfolioAmount);
    }

    /**
     * @return float
     */
    public function getCashAvailable(): float
    {
        return $this->cashAmount;
    }

    /**
     * @param float $cashAmount
     */
    public function setCashAvailable(float $cashAmount)
    {
        $this->cashAmount = abs($cashAmount);
    }

    /**
     * @return float
     */
    public function getInterestEarned(): float
    {
        return $this->interestEarned;
    }

    /**
     * @param float $interestEarned
     */
    public function setInterestEarned(float $interestEarned)
    {
        $this->interestEarned = abs($interestEarned);
    }

    /**
     * @return float
     */
    public function getOutstandingLoans(): float
    {
        return $this->outstandingLoans;
    }

    /**
     * @param float $outstandingLoans
     */
    public function setOutstandingLoans(float $outstandingLoans)
    {
        $this->outstandingLoans = abs($outstandingLoans);
    }



}