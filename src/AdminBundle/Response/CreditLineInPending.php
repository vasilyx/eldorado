<?php

namespace AdminBundle\Response;


class CreditLineInPending
{
    protected $transactionId;
    protected $customerId;
    protected $customerName;
    protected $loanApplyId;
    protected $eventId;
    protected $transactionAmt;
    protected $type;
    protected $created;


    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param mixed $transactionId
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param mixed $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return mixed
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * @param mixed $customerName
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
    }

    /**
     * @return mixed
     */
    public function getLoanApplyId()
    {
        return $this->loanApplyId;
    }

    /**
     * @param mixed $loanApplyId
     */
    public function setLoanApplyId($loanApplyId)
    {
        $this->loanApplyId = $loanApplyId;
    }

    /**
     * @return mixed
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param mixed $eventId
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
    }

    /**
     * @return mixed
     */
    public function getTransactionAmt()
    {
        return $this->transactionAmt;
    }

    /**
     * @param mixed $transactionAmt
     */
    public function setTransactionAmt($transactionAmt)
    {
        $this->transactionAmt = $transactionAmt;
    }

    /**
     * @return mixed
     */
    public function getTransactionCCY()
    {
        return $this->transactionCCY;
    }

    /**
     * @param mixed $transactionCCY
     */
    public function setTransactionCCY($transactionCCY)
    {
        $this->transactionCCY = $transactionCCY;
    }
    protected $transactionCCY;
}