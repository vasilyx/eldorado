<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 16.02.2017
 * Time: 11:57
 */

namespace LogBundle\Handler;


use AdminBundle\Entity\User as AdminUser;
use LogBundle\Entity\AdminActivityLog;

class AdminActivityHandler extends AbstractLogHandler
{
    /**
     * Constant is used in the monolog record context associative array to transfer of a logged admin
     */
    const CONTEXT_ADMIN = 'admin';

    /**
     * Constant is used in the monolog record context associative array to transfer of a logged user
     */
    const CONTEXT_USER = 'user';

    /**
     * Constant is used in the monolog record context associative array to transfer of a logged action type
     */
    const CONTEXT_ACTION = 'action';

    /**
     * Constant is used in the monolog record context associative array to transfer of a logged action code
     */
    const CONTEXT_CODE = 'code';

    /**
     * Constant is used in the monolog record context associative array to transfer of a logged metadata
     */
    const CONTEXT_METADATA = 'metadata';

    /**
     * The channel name
     *
     * @var string
     */
    private $channel = 'admin_activity';

    /**
     * Writes the record down to the log of the implementing handler
     * Realises AbstractProcessingHandler interface. Create and put a record to the AdminActivityLog database table
     *
     * @param array $record Array of monolog parameters such as channel, context etc.
     */
    public function write(array $record)
    {
        if (!$this->initialized) {
            $this->initialize();
        }

        if ($this->channel != $record['channel']) {
            return;
        }

        if (!isset($record['context'][self::CONTEXT_ADMIN]) || !($record['context'][self::CONTEXT_ADMIN] instanceof AdminUser) || !($record['context'][self::CONTEXT_ACTION]) || !($record['context'][self::CONTEXT_CODE])) {
            return;
        }

        /**
         * @var $admin AdminUser
         */
        $admin = $record['context'][self::CONTEXT_ADMIN];

        $log = new AdminActivityLog();
        $log
            ->setAdmin($admin)
            ->setActionType($record['context'][self::CONTEXT_ACTION])
            ->setCode($record['context'][self::CONTEXT_CODE])
        ;

        if (isset($record['context'][self::CONTEXT_USER])) {
            $log->setUser($record['context'][self::CONTEXT_USER]);
        }

        if (isset($record['context'][self::CONTEXT_METADATA])) {
            $log->setMetadata($record['context'][self::CONTEXT_METADATA]);
        }

        $em = $this->container->get('doctrine.orm.entity_manager');

        $em->persist($log);
        $em->flush();
    }
}