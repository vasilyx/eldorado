<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 25.01.17
 * Time: 14:46
 */

namespace LogBundle\Handler;


use AppBundle\Entity\QuoteApply;
use LogBundle\Entity\QuoteApplyStatusLog;

/**
 * This handler is created for "quote&#95;apply&#95;status" monolog channel
 * @package LogBundle\Handler
 * @author MikhailPegasin <mikhail.pegasin@gmail.com>
 */
class QuoteApplyStatusHandler extends AbstractLogHandler
{
    /**
     * The channel name
     * @var string
     */
    private $channel = 'quote_apply_status';

    /**
     * Realises AbstractProcessingHandler interface. Create and put a record to the QuoteApplyStatusLog database table
     * @param array $record Array of monolog parameters such as channel, context etc.
     */
    public function write(array $record)
    {
        if (!$this->initialized) {
            $this->initialize();
        }

        if ($this->channel != $record['channel']) {
            return;
        }

        if (!($record['context'][0] instanceof QuoteApply)) {
            return;
        }
        /**
         * @var $quoteApply QuoteApply
         */
        $quoteApply = $record['context'][0];

        $log = new QuoteApplyStatusLog();
        $log
            ->setQuoteApply($quoteApply->getId())
            ->setStatus($quoteApply->getStatus())
            ;

        $em = $this->container->get('doctrine.orm.entity_manager');

        $em->persist($log);
        $em->flush();
    }
}