<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 25.01.17
 * Time: 14:46
 */

namespace LogBundle\Handler;


use AppBundle\Entity\LoanApply;
use LogBundle\Entity\LoanApplyStatusLog;

/**
 * This handler is created for "loan&#95;request&#95;status" monolog channel
 * @package LogBundle\Handler
 * @author MikhailPegasin <mikhail.pegasin@gmail.com>
 */
class LoanApplyStatusHandler extends AbstractLogHandler
{
    /**
     * The channel name
     * @var string
     */
    private $channel = 'loan_apply_status';

    /**
     * Realises AbstractProcessingHandler interface. Create and put a record to the LoanApplyStatusLog database table
     * @param array $record Array of monolog parameters such as channel, context etc.
     */
    public function write(array $record)
    {

        if (!$this->initialized) {
            $this->initialize();
        } else {
            return;
        }

        if ($this->channel != $record['channel']) {
            return;
        }

        if (!($record['context'][0] instanceof LoanApply)) {
            return;
        }
        /**
         * @var $loanApply LoanApply
         */
        $loanApply = $record['context'][0];

        $log = new LoanApplyStatusLog();
        $log
            ->setLoanApply($loanApply)
            ->setStatus($loanApply->getStatus())
            ;

        $em = $this->container->get('doctrine.orm.entity_manager');

        $em->persist($log);
        $em->flush();
    }
}