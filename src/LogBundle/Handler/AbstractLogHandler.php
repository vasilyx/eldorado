<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 30.01.17
 * Time: 14:08
 */

namespace LogBundle\Handler;


use Monolog\Handler\AbstractProcessingHandler;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

abstract class AbstractLogHandler extends AbstractProcessingHandler
{
    use ContainerAwareTrait;

    /**
     * If handler has been initialised. Used to exclude the loop when nested listeners are processed.
     * @var boolean
     */
    protected $initialized;

    /**
     * Initialises the handler
     */
    protected function initialize()
    {
        $this->initialized = true;
    }
}