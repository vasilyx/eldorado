<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 26.01.17
 * Time: 11:30
 */

namespace LogBundle\Handler;


use AppBundle\Entity\User;
use LogBundle\Entity\UserActivityLog;

/**
 * This handler is created for "user&#95;activity" monolog channel
 * @package LogBundle\Handler
 * @author MikhailPegasin <mikhail.pegasin@gmail.com>
 */
class UserActivityHandler extends AbstractLogHandler
{
    /**
     * Constant is used in the monolog record context associative array to transfer of a logged user
     */
    const CONTEXT_USER = 'user';

    /**
     * Constant is used in the monolog record context associative array to transfer of a logged action type
     */
    const CONTEXT_ACTION = 'action';

    /**
     * Constant is used in the monolog record context associative array to transfer of a logged action code
     */
    const CONTEXT_CODE = 'code';

    /**
     * Constant is used in the monolog record context associative array to transfer of a logged metadata
     */
    const CONTEXT_METADATA = 'metadata';

    /**
     * The channel name
     * @var string
     */
    private $channel = 'user_activity';

    /**
     * Realises AbstractProcessingHandler interface. Create and put a record to the UserActivityLog database table
     * @param array $record Array of monolog parameters such as channel, context etc.
     */
    public function write(array $record)
    {
        if (!$this->initialized) {
            $this->initialize();
        }

        if ($this->channel != $record['channel']) {
            return;
        }

        if (!isset($record['context'][self::CONTEXT_USER]) || !($record['context'][self::CONTEXT_USER] instanceof User) || !isset($record['context'][self::CONTEXT_ACTION]) || !isset($record['context'][self::CONTEXT_CODE])) {
            return;
        }
        /**
         * @var $user User
         */
        $user = $record['context'][self::CONTEXT_USER];

        $log = new UserActivityLog();
        $log
            ->setUser($user)
            ->setActionType($record['context'][self::CONTEXT_ACTION])
            ->setCode($record['context'][self::CONTEXT_CODE])
        ;

        if (isset($record['context'][self::CONTEXT_METADATA])) {
            $log->setMetadata($record['context'][self::CONTEXT_METADATA]);
        }

        $em = $this->container->get('doctrine.orm.entity_manager');

        $em->persist($log);
        $em->flush();
    }
}