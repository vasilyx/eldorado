<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 16.02.2017
 * Time: 12:00
 */

namespace LogBundle\Entity;

use AdminBundle\Entity\User as AdminBundleUser;
use AppBundle\Entity\User as AppBundleUser;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

class AdminActivityLog
{
    use Timestampable;

    const ADMIN_TYPE = 1;

    const CLEAR_SECURITY_ADMIN_ONLY_ACTION = 1001;
    const CLEAR_SECURITY_CLEARED_ACTION = 1002;
    const LOGIN_ACTION = 1020;
    const LOGOUT_ACTION = 1021;

    public static function getTypes()
    {
        return array(
            self::ADMIN_TYPE => 'Admin'
        );
    }

    public static function getActions()
    {
        return array(
            self::CLEAR_SECURITY_ADMIN_ONLY_ACTION => "When admin clear security for user 'Admin only' type",
            self::CLEAR_SECURITY_CLEARED_ACTION    => "When admin clear security for user 'Cleared' type",
            self::LOGIN_ACTION                     => "When admin login",
            self::LOGOUT_ACTION                    => "When admin logout",
        );
    }

    /**
     * @var integer
     */
    private $id;
    /**
     * @var AppBundleUser
     */
    private $user;

    /**
     * @var AdminBundleUser
     */
    private $admin;

    /**
     * @var string
     */
    private $metadata;

    /**
     * @var string
     */
    private $actionType;

    /**
     * @var integer
     */
    private $code;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param $code
     * @return AdminActivityLog
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return AppBundleUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param AppBundleUser $user
     * @return AdminActivityLog
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getActionType()
    {
        return $this->actionType;
    }

    /**
     * @param string $actionType
     * @return AdminActivityLog
     */
    public function setActionType($actionType)
    {
        $this->actionType = $actionType;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param string $metadata
     * @return AdminActivityLog
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
        return $this;
    }

    /**
     * @return AdminBundleUser
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param AdminBundleUser $admin
     * @return AdminActivityLog
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
        return $this;
    }
}