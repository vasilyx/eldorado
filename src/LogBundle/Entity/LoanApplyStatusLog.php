<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 25.01.17
 * Time: 14:25
 */

namespace LogBundle\Entity;


use AppBundle\Entity\LoanApply;

class LoanApplyStatusLog extends StatusLog
{
    /**
     * @var $loanApply LoanApply
     */
    private $loanApply;

    /**
     * @return LoanApply
     */
    public function getLoanApply()
    {
        return $this->loanApply;
    }

    /**
     * @param LoanApply $loanApply
     * @return LoanApplyStatusLog
     */
    public function setLoanApply($loanApply)
    {
        $this->loanApply = $loanApply;
        return $this;
    }
}