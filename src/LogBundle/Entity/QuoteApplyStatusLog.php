<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 25.01.17
 * Time: 14:18
 */

namespace LogBundle\Entity;


use AppBundle\Entity\QuoteApply;

class QuoteApplyStatusLog extends StatusLog
{
    /**
     * @var integer
     */
    private $quoteApply;

    /**
     * @return integer
     */
    public function getQuoteApply()
    {
        return $this->quoteApply;
    }

    /**
     * @param integer $quoteApply
     * @return QuoteApplyStatusLog
     */
    public function setQuoteApply($quoteApply)
    {
        $this->quoteApply = $quoteApply;
        return $this;
    }
}