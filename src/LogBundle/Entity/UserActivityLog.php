<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 26.01.17
 * Time: 11:24
 */

namespace LogBundle\Entity;


use AppBundle\Entity\User;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

class UserActivityLog
{
    use Timestampable;

    const ACCOUNT_TYPE = 1;
    const BORROWER_PRODUCT_TYPE = 2;
    const LENDER_PRODUCT_TYPE = 3;
    const SYSTEM_TYPE = 4;

    const USER_REGISTERED_ACTION = 2000;
    const EMAIL_VERIFIED_ACTION = 2001;
    const MOBILE_VERIFIED_ACTION = 2002;
    const LOGIN_ACTION = 2003;
    const LOGOUT_ACTION = 2004;
    const USER_PREFERENCES_ACTION = 2005;
    const USER_FORGOT_PASSWORD = 2006;
    const USER_RESET_PASSWORD = 2007;
    const EMAIL_ADDRESS_ADDED_ACTION = 2010;
    const COMMUNICATION_ADDED_ACTION = 2011;
    const BANK_DETAILS_ACTION = 2020;
    const CUSTOMER_INCOME_ACTION = 2021;
    const OCCUPATION_ACTION = 2022;
    const HOUSEHOLD_ACTION = 2023;
    const DECLUTTER_ACTION = 2024;
    const CREDITLINE_ACTION = 2025;
    const DEPENDENT_ACTION = 2026;

    const BORROWER_APPLICATION_INITIATED = 2100;
    const BORROWER_APPLICATION_SUBMITTED = 2101;
    const LOAN_REQUEST_CREATED = 2102;
    const LOAN_REQUEST_SUBMITTED = 2103;

    const BORROWER_QUOTATION_ACTION = 2201;
    const BORROWER_CALLREPORT_ACTION = 2202;
    const BORROWER_CALLVALIDATE_ACTION = 2203;
    const BORROWER_AFFORDABILITY_ACTION = 2204;
    const LENDER_QUOTATION_ACTION = 2205;
    const NEW_PRODUCT_ACTION = 2206;

    //constants
    const QUOTATION_ACTION = 2207;
    const CALLREPORT_ACTION = 2208;
    const MANAGEMENT_ACTION = 2209;


    const LENDER_APPLICATION_INITIATED = 2300;
    const LENDER_APPLICATION_SUBMITTED = 2301;
    const CREATE_OFFER_ACTION = 2302;
    const WORLDPAY_PAYMENT_DONE_ACTION = 2303;
    const WORLDPAY_PAYMENT_FAILED_ACTION = 2304;


    public static function getTypes()
    {
        return array(
            self::ACCOUNT_TYPE          => 'Account',
            self::BORROWER_PRODUCT_TYPE => 'Borrower product',
            self::LENDER_PRODUCT_TYPE   => 'Lender product',
            self::SYSTEM_TYPE           => 'System'
        );
    }

    public static function getActions()
    {
        return array(
            self::USER_REGISTERED_ACTION         => 'User created account',
            self::EMAIL_VERIFIED_ACTION          => 'User clicks link in verification email',
            self::MOBILE_VERIFIED_ACTION         => 'User inputs validation code from mobile',
            self::BORROWER_APPLICATION_INITIATED => 'User started Borrower Quote',
            self::BORROWER_APPLICATION_SUBMITTED => 'User submitted Borrower Quote',
            self::LENDER_APPLICATION_INITIATED   => 'User started Lender Quote',
            self::LENDER_APPLICATION_SUBMITTED   => 'User submitted Lender Quote',
            self::LOGIN_ACTION                   => 'Authentication user, on log in',
            self::LOGOUT_ACTION                  => 'Authentication user, on logout',
            self::EMAIL_ADDRESS_ADDED_ACTION     => 'When customer updated email',
            self::COMMUNICATION_ADDED_ACTION     => 'When user add/update communication',
            self::DEPENDENT_ACTION               => 'When customer add/update Dependent',
            self::LOAN_REQUEST_SUBMITTED         => 'User submit Loan Request',
            self::LOAN_REQUEST_CREATED           => 'Loan Request was created',
            self::BORROWER_QUOTATION_ACTION      => 'Borrower Quotation request send',
            self::BORROWER_CALLREPORT_ACTION     => 'Borrower CallReport request send',
            self::BORROWER_CALLVALIDATE_ACTION   => 'Borrower CallValidate request send',
            self::BORROWER_AFFORDABILITY_ACTION  => 'Borrower Affordability request send',
            self::LENDER_QUOTATION_ACTION        => 'Lender Quotation request send',
            self::WORLDPAY_PAYMENT_DONE_ACTION   => 'User made WorldPay payment',
            self::WORLDPAY_PAYMENT_FAILED_ACTION => 'User WorldPay payment failed',
            self::BANK_DETAILS_ACTION            => 'When customer add/update Bank Details',
            self::HOUSEHOLD_ACTION               => 'When customer add/update Household',
            self::CREDITLINE_ACTION              => 'When customer add/update CreditLines',
            self::DECLUTTER_ACTION               => 'When customer add/update DeClutter',
            self::OCCUPATION_ACTION              => 'When customer add/update Occupation',
            self::CUSTOMER_INCOME_ACTION         => 'When customer add/update Customer Income',
            self::CREATE_OFFER_ACTION            => 'When user create Offer',
            self::USER_FORGOT_PASSWORD           => 'User send forgot password request',
            self::USER_RESET_PASSWORD            => 'User reset password',
            self::NEW_PRODUCT_ACTION             => 'Assign new product to the Quote',
            self::QUOTATION_ACTION               => 'Quotation CallCredit request',
            self::CALLREPORT_ACTION              => 'Credit History CallCredit request',
            self::MANAGEMENT_ACTION              => 'Management CallCredit request'
        );
    }

    /**
     * @var integer
     */
    private $id;
    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $actionType;

    /**
     * @var integer
     */
    private $code;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param $code
     * @return UserActivityLog
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return UserActivityLog
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getActionType()
    {
        return $this->actionType;
    }

    /**
     * @param string $actionType
     * @return UserActivityLog
     */
    public function setActionType($actionType)
    {
        $this->actionType = $actionType;
        return $this;
    }

    /**
     * @var string
     */
    private $metadata;


    /**
     * Set metadata
     *
     * @param string $metadata
     *
     * @return UserActivityLog
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return string
     */
    public function getMetadata()
    {
        return $this->metadata;
    }
}
