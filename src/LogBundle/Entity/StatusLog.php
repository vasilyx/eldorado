<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 25.01.17
 * Time: 14:22
 */

namespace LogBundle\Entity;


use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

abstract class StatusLog
{
    use Timestampable;

    protected $id;
    protected $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return StatusLog
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
}