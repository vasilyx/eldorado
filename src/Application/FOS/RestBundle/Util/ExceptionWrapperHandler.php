<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 05.12.16
 * Time: 13:42
 */

namespace Application\FOS\RestBundle\Util;


use FOS\RestBundle\View\ExceptionWrapperHandlerInterface;
use Symfony\Component\Debug\Exception\FlattenException;

class ExceptionWrapperHandler implements ExceptionWrapperHandlerInterface
{
    const STATUS_ERROR = 'error';

    public function wrap($data)
    {
        /**
         * @var FlattenException $exception
         */
        $exception = $data['exception'];
        return array(
            'status' => self::STATUS_ERROR,
            'content' => [
                'code' => $data['status_code'],
                'message' => $exception->getMessage()
            ],
        );
    }
}