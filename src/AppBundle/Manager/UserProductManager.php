<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 02.02.17
 * Time: 10:51
 */

namespace AppBundle\Manager;


use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use AppBundle\Entity\UserProduct;
use AppBundle\Security\Voter\UserProductVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserProductManager extends BaseManager
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    public function setAuthorizationChecker(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * Creates UserProduct if there are no UserProducts for the user and the product
     *
     * @param User $user
     * @param Product $product
     * @return UserProduct
     */
    public function createUserProduct(User $user, Product $product)
    {
        $userProduct = UserProduct::create()
            ->setUser($user)
            ->setProduct($product);

        if ($this->authorizationChecker->isGranted(UserProductVoter::CREATE, $userProduct)) {
            return $userProduct;
        } else {
            throw new AccessDeniedException();
        }
    }

    /**
     * Returns one productUser for user and product
     *
     * @param User $user
     * @param Product $product
     * @return UserProduct|null|object
     */
    public function getUserProduct(User $user, Product $product)
    {
        $productUser = $this->em->getRepository(UserProduct::class)->findOneBy([
            'user' => $user,
            'product' => $product
        ]);

        return $productUser;
    }
}