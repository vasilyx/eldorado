<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 03.02.2017
 * Time: 10:25
 */

namespace AppBundle\Manager;


use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\CallCreditCredentials;
use AppBundle\Repository\CallCreditCredentialsRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use XSLTProcessor;

class CallCreditCredentialsManager extends BaseManager
{
    use ContainerAwareTrait;
    /**
     * @var CallCreditCredentialsRepository
     */
    protected $repository;

    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->repository = $this->em->getRepository(CallCreditCredentials::class);
    }

    public function getCredentialsByType($type)
    {
        if ('prod' === $this->container->getParameter('server_mode')) {

            $credentialsRepository = $this->container->get('doctrine')->getRepository(CallCreditCredentials::class);

            return $credentialsRepository->getCredentialsForApiByType($type);

        } else {
            switch ($this->container->getParameter('call_credit_api_mode')) {
                case (CallCreditCredentials::SLAVE_MODE):
                    $ch = curl_init($this->container->getParameter('call_credit_credentials_url'));
                    $data = json_encode(['type' => $type]);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POST, true);
                    $result = curl_exec($ch);
                    curl_close($ch);

                    return json_decode($result, true)['content'];
                case (CallCreditCredentials::MASTER_MODE):
                    /** @var CallCreditCredentialsRepository $credentialsRepository */
                    $credentialsRepository = $this->container->get('doctrine')->getRepository(CallCreditCredentials::class);

                    return $credentialsRepository->getCredentialsForApiByType($type);
            }
        }
    }
}