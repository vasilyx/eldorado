<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 22.05.2018
 * Time: 15:19
 */

namespace AppBundle\Manager;


use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\QuoteApply;
use AppBundle\Event\CallCreditEvent;
use AppBundle\Event\CallCreditEvents;
use AppBundle\Repository\CallCreditAPICallsRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\EventDispatcher;
use XSLTProcessor;

class ApiCallsManager extends BaseManager
{
    use ContainerAwareTrait;
    /**
     * @var CallCreditAPICallsRepository
     */
    protected $repository;

    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->repository = $this->em->getRepository(CallCreditAPICalls::class);
    }

    /**
     * Rendert xml response to HTML
     *
     * @param $xmlString
     * @param $type
     * @return string
     */
    public function xmlToHtmlRender($xmlString, $type)
    {
        $xp = new XSLTProcessor();
        // create a DOM document and load the XSL stylesheet
        $xsl = new \DomDocument;
        switch ($type) {
            case CallCreditAPICalls::TYPE_CALLVALIDATE:
                $xsl->load(__DIR__ . './../Resources/Schemas/CallValidate/CallValidate-mapping.xslt');
                break;
            case CallCreditAPICalls::TYPE_AFFORDABILITY:
                $xsl->load(__DIR__ . './../Resources/Schemas/Tac/TAC22.xslt');
                break;
            case CallCreditAPICalls::TYPE_CALLREPORT:
                $xmlString = str_replace(' xmlns="urn:callcredit.co.uk/soap:bsbandcreditreport7"', '', $xmlString);
                $xmlString = str_replace(' xmlns="urn:callcredit.co.uk/soap:callreport7"', '',$xmlString);
                $xsl->load(__DIR__ . './../Resources/Schemas/CallReport7.xslt');
                break;
            default:
                break;
        }

        // import the XSL styelsheet into the XSLT process
        $xp->importStylesheet($xsl);

        // create a DOM document and load the XML data
        $xml_doc = new \DomDocument;
        $xml_doc->loadXML($xmlString);

        // transform the XML into HTML using the XSL file
        $html = $xp->transformToXml($xml_doc);

        return $html;
    }

    /**
     * After admin decision send the request by quote and type
     *
     * @param QuoteApply $quoteApply
     * @param $requestEventType
     */
    public function sendCallCreditRequestForQuote(QuoteApply $quoteApply, $requestEventType)
    {
        /** Generate CallCreditEvent which will be handled by CallCreditApiListener method: processCallReportRequest */
        /** @var CallCreditEvent $callCreditEvent */
        $callCreditEvent = $this->container->get('event_dispatcher')->dispatch($requestEventType,
            CallCreditEvent::create()
                ->setCallCreditApiCalls(
                    CallCreditAPICalls::create()
                        ->setQuoteApply($quoteApply)
                        ->setPurpose(CallCreditAPICalls::PURPOSE_CA)
                )
        );

        $callCredit = $callCreditEvent->getCallCreditApiCalls();

        $this->em->persist($callCredit);
        $this->em->flush();
    }
}