<?php
/**
 * User: Vladimir
 * Date: 3/2/17
 * Time: 3:40 PM
 */

namespace AppBundle\Manager;


use AppBundle\Entity\Agreement;
use AppBundle\Entity\Restriction;
use AppBundle\Entity\UserDetails;
use Doctrine\ORM\EntityManager;

class AgreementManager
{
    private $em;

    /**
     * AgreementManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Agreement $agreement
     */
    public function addAgreementTags(Agreement $agreement)
    {
        $restrictionGroups = $this->em->getRepository('AppBundle:RestrictionGroup')->getAllWithRestrictions();
        foreach ($restrictionGroups as $restrictionGroup) {
            switch ($restrictionGroup->getCode()) {
                case 'age' :
                    $this->addAgeTags($agreement, $restrictionGroup->getRestrictions());
                    break;
                case 'gender' :
                    $this->addGenderTags($agreement, $restrictionGroup->getRestrictions());
                    break;
                case 'amount' :
                    $this->addAmountTags($agreement, $restrictionGroup->getRestrictions());
                    break;
                case 'region' :
                    $this->addRegionTags($agreement, $restrictionGroup->getRestrictions());
            }
        }
    }

    /**
     * @param Agreement $agreement
     * @param $restrictions
     */
    private function addAgeTags(Agreement $agreement, $restrictions)
    {
        $user = $agreement->getLoanApply()->getUser();
        foreach ($restrictions as $restriction) {
            switch ($restriction->getCode()) {
                case '18_35' :
                    $age = (new \DateTime())->diff($user->getDetails()->getBirthdate())->format('%y');
                    if ($age >= 18 && $age < 35) {
                        $agreement->addTag($restriction);
                    }
                    break;
                case '35_55' :
                    $age = (new \DateTime())->diff($user->getDetails()->getBirthdate())->format('%y');
                    if ($age >= 35 && $age < 55) {
                        $agreement->addTag($restriction);
                    }
                    break;
                case '55_' :
                    $age = (new \DateTime())->diff($user->getDetails()->getBirthdate())->format('%y');
                    if ($age >= 55) {
                        $agreement->addTag($restriction);
                    }
            }
        }
    }

    /**
     * @param Agreement $agreement
     * @param $restrictions
     */
    private function addGenderTags(Agreement $agreement, $restrictions)
    {
        $user = $agreement->getLoanApply()->getUser();
        foreach ($restrictions as $restriction) {
            switch ($restriction->getCode()) {
                case 'male' :
                    if (UserDetails::GENDER_MALE == $user->getDetails()->getGender()) {
                        $agreement->addTag($restriction);
                    }
                    break;
                case 'female' :
                    if (UserDetails::GENDER_FEMALE == $user->getDetails()->getGender()) {
                        $agreement->addTag($restriction);
                    }
            }
        }
    }

    /**
     * @param Agreement $agreement
     * @param $restrictions
     */
    private function addAmountTags(Agreement $agreement, $restrictions)
    {
        foreach ($restrictions as $restriction) {
            switch ($restriction->getCode()) {
                case '_15K' :
                    if ($agreement->getLoanAmount() < 15000) {
                        $agreement->addTag($restriction);
                    }
                    break;
                case '15K_' :
                    if ($agreement->getLoanAmount() >= 15000) {
                        $agreement->addTag($restriction);
                    }
            }
        }
    }

    /**
     * @param Agreement $agreement
     * @param Restriction[] $restrictions
     */
    private function addRegionTags(Agreement $agreement, $restrictions)
    {
        $user = $agreement->getLoanApply()->getUser();
        $currentAddress = $this->em->getRepository('AppBundle:UserAddress')->getLatestAddressForUser($user);
        $postcodeRegions = $this->em->getRepository('AppBundle:PostcodeRegion')->getByOutwardCode($currentAddress->getOutwardCodePart());
        foreach ($restrictions as $restriction) {
            foreach ($postcodeRegions as $postcodeRegion){
                if($restriction->getName() == $postcodeRegion->getRegion()){
                    $agreement->addTag($restriction);
                }
            }
        }
    }
}