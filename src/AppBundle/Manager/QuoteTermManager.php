<?php
namespace AppBundle\Manager;

use AppBundle\Entity\Product;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\QuoteTerm;
use AppBundle\Entity\User;
use AppBundle\Exception\QuoteApplyNotFoundException;
use AppBundle\Utils\Payment\InterestRatesProviderInterface;
use AppBundle\Utils\Payment\PaymentsComputerService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class QuoteTermManager
{
    private $em;
    private $authorizationChecker;
    private $paymentsComputer;

    /**
     * QuoteTermManager constructor.
     * @param EntityManager $em
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param PaymentsComputerService $paymentsComputer
     */
    public function __construct(EntityManager $em, AuthorizationCheckerInterface $authorizationChecker, PaymentsComputerService $paymentsComputer)
    {
        $this->em = $em;
        $this->authorizationChecker = $authorizationChecker;
        $this->paymentsComputer = $paymentsComputer;
    }


    public function addQuoteTerm(QuoteTerm $quoteTerm, User $user)
    {
        $quoteApply = $this->em->getRepository('AppBundle:QuoteApply')->getQuoteAppliesForUser($user, [QuoteApply::STATUS_DRAFT], Product::TYPE_BORROWER);

        if (!$quoteApply) {
            throw new QuoteApplyNotFoundException(QuoteApplyNotFoundException::DRAFT_QUOTE_APPLY_NOT_FOUND);
        }
        $quoteTerm->setQuoteApply($quoteApply[0]);

        $this->em->detach($quoteTerm);
        $oldQuoteTerm = $this->em->getRepository('AppBundle:QuoteTerm')->getQuoteTermForUser($user);
        if ($oldQuoteTerm instanceof QuoteTerm) {
            if ($oldQuoteTerm == $quoteTerm) {
                $quoteTerm = clone $oldQuoteTerm;
            }
            $oldQuoteTerm->setArchive(true);
        }

        $quoteTerm->setBonusRate($this->paymentsComputer->getDefaultInterestRate($quoteTerm->getTerm(), $quoteTerm->getAmount(), InterestRatesProviderInterface::RATE_TYPE_BONUS));
        $quoteTerm->setBrokerRate($this->paymentsComputer->getDefaultInterestRate($quoteTerm->getTerm(), $quoteTerm->getAmount(), InterestRatesProviderInterface::RATE_TYPE_BROKER));
        $quoteTerm->setContractRate($this->paymentsComputer->getDefaultInterestRate($quoteTerm->getTerm(), $quoteTerm->getAmount(), InterestRatesProviderInterface::RATE_TYPE_CONTRACT));
        $quoteTerm->setEffectiveRate($this->paymentsComputer->getDefaultInterestRate($quoteTerm->getTerm(), $quoteTerm->getAmount(), InterestRatesProviderInterface::RATE_TYPE_EFFECTIVE));
        $quoteTerm->setInvestorRate($this->paymentsComputer->getDefaultInterestRate($quoteTerm->getTerm(), $quoteTerm->getAmount(), InterestRatesProviderInterface::RATE_TYPE_INVESTOR));

        $creditPaymentsTotal = $this->paymentsComputer->getCreditPaymentsTotal($quoteTerm->getContractRate(), $quoteTerm->getTerm(), $quoteTerm->getAmount());
        $quoteTerm->setMonthlyPayment($creditPaymentsTotal['monthlyPayment']);
        $quoteTerm->setTotalPayment($creditPaymentsTotal['totalPaid']);

        $this->em->persist($quoteTerm);
        $this->em->flush();

        return $quoteTerm;
    }

    /**
     * Get quote terms for drafted quotation only
     *
     * @param User $user
     * @return QuoteTerm
     */
    public function getDraftedQuoteTerm(User $user)
    {
        $quoteTerm = $this->em->getRepository('AppBundle:QuoteTerm')
            ->getQuoteTermForUser($user);

        if (null === $quoteTerm) {
            $quoteTerm = new QuoteTerm();
            $quoteApply = $this->em->getRepository('AppBundle:QuoteApply')->getActiveQuoteApplyForUser($user);
            $quoteTerm->setQuoteApply($quoteApply[0]);
        }

        return $quoteTerm;
    }

    /**
     * Get quote term for user
     *
     * @param User $user
     * @return QuoteTerm|null
     */
    public function getQuoteTermForUser(User $user)
    {
        //get quote apply for user
        $quoteApplies = $this->em->getRepository('AppBundle:QuoteApply')->getQuoteAppliesForUser($user);

        $quoteTerm = null;

        //TODO: change to fetching all quote terms independent of quote apply status
        if (count($quoteApplies)) {
            $quoteApply = $quoteApplies[0];
            $quoteTerm = $this->em->getRepository('AppBundle:QuoteTerm')->getQuoteTermForQuoteApply($quoteApply);
        }

        return $quoteTerm;
    }
}