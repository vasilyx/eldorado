<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 24.05.2018
 * Time: 13:49
 */

namespace AppBundle\Manager;


use AdminBundle\Manager\AdminBorrowerManager;
use AppBundle\Entity\Agreement;
use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\QuoteApply;
use AppBundle\Event\CallCreditEvents;
use AppBundle\Repository\AgreementRepository;
use AppBundle\Utils\Payment\PaymentsComputerService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class LoanApplyManager extends BaseManager
{
    use ContainerAwareTrait;

    const TERMS_12 = '12 months';
    const TERMS_24 = '24 months';
    const TERMS_36 = '36 months';
    const TERMS_48 = '48 months';
    const TERMS_60 = '60 months';

    protected $repository;

    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->repository = $this->em->getRepository(LoanApply::class);
    }

    /**
     * Set loanApply status and if it needed in extra check, send CallCredit requests
     *
     * @param QuoteApply $quoteApply
     * @param $status
     * @return LoanApply|null|object
     */
    public function setLoanApplyStatusByQuote(QuoteApply $quoteApply, $status)
    {
        $loanApply = $this->repository->findOneBy(['quoteApply' => $quoteApply]);

        if (empty($loanApply)) {
            return null;
        }

        switch ((int)$status) {
            case LoanApply::STATUS_AFFORDABILITY_CHECK:
                $loanApply->setStatus(LoanApply::STATUS_AFFORDABILITY_CHECK);
                $this->container->get('whs.manager.api_calls_manager')->sendCallCreditRequestForQuote($quoteApply, CallCreditEvents::AFFORDABILITY_REQUEST);
                break;
            case LoanApply::STATUS_CREDIT_HISTORY_CHECK:
                $loanApply->setStatus(LoanApply::STATUS_CREDIT_HISTORY_CHECK);
                $this->container->get('whs.manager.api_calls_manager')->sendCallCreditRequestForQuote($quoteApply, CallCreditEvents::CALL_REPORT_REQUEST);
                break;
            case LoanApply::STATUS_CREDIT_DECISION:
                $loanApply->setStatus(LoanApply::STATUS_CREDIT_DECISION);
                break;
            default:
                $loanApply->setStatus($status);
                break;
        }

        $this->em->persist($loanApply);
        $this->em->flush();

        return $loanApply;
    }

    /**
     * @param QuoteApply $quoteApply
     * @return LoanApply|null|object
     */
    public function getLoanApplyByQuote(QuoteApply $quoteApply)
    {
        return $this->repository->findOneBy(['quoteApply' => $quoteApply]);
    }

    /**
     * @return array
     */
    public function getNotFullyMatchedLoans()
    {
        /** @var AgreementRepository $agreementRepo */
        $agreementRepo = $this->em->getRepository(Agreement::class);
        /** @var ContractMatchingManager $contractMatchingManager */
        $contractMatchingManager = $this->container->get('whs.manager.contract_matching_manager');

        $result = [
            self::TERMS_12 => 0,
            self::TERMS_24 => 0,
            self::TERMS_36 => 0,
            self::TERMS_48 => 0,
            self::TERMS_60 => 0
        ];

        /** @var Agreement $agreement */
        $agreements = $agreementRepo->getContractMatchingAgreement();

        foreach ($agreements as $agreement) {
            $amount = $contractMatchingManager->getNeededAmount($agreement);

            $result[$this->classifyAgreement($agreement)] += $amount;
        }

        /** @var AdminBorrowerManager $borrowerManager */
        $borrowerManager = $this->container->get('whadmin.borrower_manager');
        $response = [];

        foreach ($result as $key => $res) {
            $rate = $this->getOfferRate($key);
            $term = $this->getOfferTerm($key);
            $payments = $borrowerManager->getInterestNew($res, $term, $rate);

            if ($res > 0) {
                $row = [
                    'amount_available' => $res,
                    'term_up' => $key,
                    'rate_up' => $rate,
                    'get_back' => $payments['capital']
                ];

                array_push($response, $row);
            }
        }

        return $response;
    }

    protected function classifyAgreement(Agreement $agreement)
    {
        $term = $agreement->getTerm();

        if ($term <= 12) {
            return self::TERMS_12;
        } elseif ($term <= 24) {
            return self::TERMS_24;
        } elseif ($term <= 36) {
            return self::TERMS_36;
        } elseif ($term <= 48) {
            return self::TERMS_48;
        } elseif ($term <= 60) {
            return self::TERMS_60;
        }

        return 'ongoing';
    }

    protected function getOfferRate($term) {
        $rates = InvestOffer::getRatesByTerms();

        switch ($term) {
            case self::TERMS_12:
                return $rates[InvestOffer::INVEST_TERM_YEAR];
            case self::TERMS_24:
                return $rates[InvestOffer::INVEST_TERM_TWO_YEARS];
            case self::TERMS_36:
                return $rates[InvestOffer::INVEST_TERM_THREE_YEARS];
            case self::TERMS_48:
                return $rates[InvestOffer::INVEST_TERM_FOUR_YEARS];
            case self::TERMS_60:
                return $rates[InvestOffer::INVEST_TERM_FIVE_YEARS];
            default:
                return 0;
        }
    }

    protected  function getOfferTerm($term) {
        switch ($term) {
            case self::TERMS_12:
                return InvestOffer::INVEST_TERM_YEAR;
            case self::TERMS_24:
                return InvestOffer::INVEST_TERM_TWO_YEARS;
            case self::TERMS_36:
                return InvestOffer::INVEST_TERM_THREE_YEARS;
            case self::TERMS_48:
                return InvestOffer::INVEST_TERM_FOUR_YEARS;
            case self::TERMS_60:
                return InvestOffer::INVEST_TERM_FIVE_YEARS;
            default:
                return 0;
        }
    }
}