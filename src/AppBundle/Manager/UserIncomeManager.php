<?php
/**
 * Created by PhpStorm.
 * User: mikhailpegasin
 * Date: 25.11.16
 * Time: 16:58
 */

namespace AppBundle\Manager;


use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\User;
use AppBundle\Entity\UserIncome;
use AppBundle\Exception\QuoteApplyNotFoundException;
use AppBundle\Manager\ActiveManagers\NoUpdate;
use AppBundle\Repository\UserIncomeRepository;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class UserIncomeManager extends NoUpdate
{
    use ContainerAwareTrait;

    public function setParameters()
    {
        $this->repository = $this->em->getRepository(UserIncome::class);
    }

    public function createUserIncomeForUser(User $user, $persist = true, $andFlush = false)
    {
        $userIncome = new UserIncome();

        $userIncome->setUser($user);

        if (true === $persist) {
            $this->em->persist($userIncome);
        }

        if (true === $andFlush) {
            $this->em->flush();
        }

        return $userIncome;
    }

    /**
     * @param UserIncome $userIncome
     * @param User $user
     * @return UserIncome
     */
    public function addUserIncome(UserIncome $userIncome, User $user)
    {
        $quoteApply = $this->em->getRepository('AppBundle:QuoteApply')->getQuoteAppliesForUser($user, [QuoteApply::STATUS_APPROVED]);

        if (!$quoteApply) {
            throw new QuoteApplyNotFoundException(QuoteApplyNotFoundException::APPROVED_QUOTE_APPLY_NOT_FOUND);
        }

        $oldUserIncome = $this->em->getRepository('AppBundle:UserIncome')->getLastForUser($user, false);
        if ($oldUserIncome) {
            $oldUserIncome->setArchiveFalse();
        }

        $userIncome->setUser($user);
        $this->em->persist($userIncome);
        $this->em->flush();

        return $userIncome;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUserIncomeById($id)
    {
        return $this->repository->findOneById($id);
    }

    /**
     * @param User $user
     * @param Criteria|null $criteria
     * @return mixed
     */
    public function getUserIncomeByUser(User $user, Criteria $criteria = null)
    {
        return $this->repository->getUserIncome($user, $criteria);
    }
}