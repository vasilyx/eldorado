<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 1.12.16
 * Time: 8.55
 */

namespace AppBundle\Manager;


use AdminBundle\Response\OfferDetails;
use AppBundle\Entity\ContractMatching;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\InvestOfferSub;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\User;
use AppBundle\Repository\ContractMatchingRepository;
use AppBundle\Repository\InvestOfferRepository;
use AppBundle\Utils\Payment\PaymentsComputerService;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;

class InvestOfferManager extends BaseManager
{
    /**
     * @var InvestOfferRepository
     */
    protected $repository;

    /**
     * @var PaymentsComputerService
     */
    protected $paymentsComputer;

    public function __construct(EntityManager $em, PaymentsComputerService $paymentsComputer)
    {
        parent::__construct($em);
        $this->repository = $this->em->getRepository(InvestOffer::class);
        $this->paymentsComputer = $paymentsComputer;
    }

    public function getLastInvestOfferForUser(User $user)
    {
        /**
         * @var InvestOffer
         */
        if ($investOffer = $this->repository->getInvestOfferForUser($user)) {
            $investOffer = $investOffer[0];
        };

        return $investOffer;
    }

    public function getAllInvestOffersForUser(User $user)
    {
        return $this->repository->findBy(['user' => $user]);
    }

    public function getInvestOffersForUser(User $user)
    {
        return $this->repository->getInvestOfferForUser($user);
    }

    /**
     * @param User $user
     * @return InvestOffer[]
     */
    public function getActiveInvestOffersForUser(User $user)
    {
        return $this->repository->getInvestOfferForUser($user);
    }

    /**
     * @param InvestOffer $investOffer
     */
    public function createSubOffers(InvestOffer $investOffer)
    {
        $countOfSubOffers = $this->getCountOfSubOffers($investOffer->getInvestmentAmount());

        for ($i=1; $i<=$countOfSubOffers; $i++) {
            $subInvestOffer = new InvestOfferSub();
            $subInvestOffer
                ->setInvestOffer($investOffer)
                ->setActive(true)
                ->setAmount(20)
                ;

            $this->em->persist($subInvestOffer);
        }

        $this->em->flush();
    }

    /**
     * @param $amount
     * @return int
     * @throws \Exception
     */
    public function getCountOfSubOffers($amount)
    {
        if (0 !== $amount % 20) {
            throw new \Exception('InvestOffer amount should be multiple 20');
        }
        $countOfSubOffers = intval($amount / 20);

        return $countOfSubOffers;
    }

    /**
     * Get invest offer with STATUS_DRAFT && STATUS_PENDING_FUNDS
     *
     * @param User $user
     * @return array
     */
    public function getPendingInvestOffersForUser(User $user)
    {
        return $this->repository->getPendingInvestOffersForUser($user);
    }

    /**
     * @param User $user
     * @return array
     * @internal param QuoteApply $quoteApply
     */
    public function getOfferDetailsListByUser(User $user)
    {
        /**
         * @var $contractMatching ContractMatchingRepository
         */
        $contractMatching = $this->em->getRepository(ContractMatching::class);
        $offerList = $this->em->getRepository(InvestOffer::class)->findBy(['user' => $user]);

        $responseList = [];
        /**
         * @var $offer InvestOffer
         */
        foreach ($offerList as $offer) {

            if ($offer->getStatus() == InvestOffer::STATUS_CANCEL) {
                continue;
            }

            $offerDetails = new OfferDetails();
            $offerDetails->setName($offer->getInvestmentName());
            $offerDetails->setAmount(
                $offer->getInvestmentAmount()
            );

            $offerDetails->setPending(
                $contractMatching->getSumByOfferStatus( $offer,ContractMatching::STATUS_PENDING)
            );

            $offerDetails->setActive(
                $contractMatching->getSumByOfferStatus( $offer,ContractMatching::STATUS_ACTIVE)
            );
            $offerDetails->setSettled(
                $contractMatching->getSumByOfferStatus( $offer,ContractMatching::STATUS_SETTLED)
            );

            $agreementList = $contractMatching->getMatchedAgreementsByOffer($offer);

            foreach ($agreementList as $agreement) {

                $int_new = $this->getInterestNew(
                    $agreement['sum_amount'],
                    $agreement['term'],
                    InvestOffer::getRatesByTerms()[$offer->getTerm()]
                );
                $agreement['sum_amount'] = (float)$agreement['sum_amount'];
                $agreement['apr'] = (float)$agreement['apr'];
                $agreement['interest'] = $int_new['interest'];
                $agreement['rate'] = (float)InvestOffer::getRatesByTerms()[$offer->getTerm()];

                $offerDetails->addAgreement($agreement);
            }
            $response['details'] = $offerDetails;
            $response['offer'] = $offer;

            array_push($responseList, $response);
        }

        return $responseList;
    }

    protected function getInterest($loanAmount, $loanTerm, $loanApr)
    {
        $creditPayments = $this->paymentsComputer->getCreditPaymentsTotal($loanApr, $loanTerm, $loanAmount);
        return round($creditPayments['totalPaid'] - $loanAmount, 2);
    }


    /**
     * @param User $user
     * @return array
     */
    public function getNotFullyMatchedOffers(User $user)
    {
        $offers = $this->repository->findBy(['user' => $user, 'status' => InvestOffer::STATUS_ACTIVE]);

        $contractRepository = $this->em->getRepository(ContractMatching::class);

        $result = [];

        foreach ($offers as $offer) {
            $contractSum = $contractRepository->getSumForInvestor($offer);
            $amountAvailable = $offer->getInvestmentAmount() - $contractSum;
            if ($amountAvailable > 0) {

                $payments = $this->paymentsComputer->getCreditPaymentsTotal(InvestOffer::getRatesByTerms()[$offer->getTerm()], $offer->getTerm(), $amountAvailable);

                $row = [
                    'offer_id' => $offer->getId(),
                    'amount_available' => $amountAvailable,
                    'rate_up' => InvestOffer::getRatesByTerms()[$offer->getTerm()],
                    'terms' => $offer->getTerm(),
                    'get_back' => $payments['totalInterest']
                ];

                array_push($result, $row);

            } else {
                continue;
            }
        }

        return $result;
    }

    /**
     * @param $amount
     * @param $term
     * @param $percent
     * @return array
     */
    protected function getInterestNew($amount, $term, $percent)
    {
        $percent = $percent / 100;

        $fullYear = floor($term / 12);
        $remainingMonth = $term - $fullYear * 12;

        $capitalInterest = $amount * pow((1+$percent), $fullYear);
        $interest = $capitalInterest * ($percent/12) * $remainingMonth;

        $capital = $capitalInterest + $interest;
        $int = $capital - $amount;

        return ['capital' => round($capital, 2), 'interest' => round($int, 2)];
    }
}