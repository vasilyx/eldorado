<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 13.12.16
 * Time: 10:55
 */

namespace AppBundle\Manager\ActiveManagers;


use AppBundle\Interfaces\ManagerType\OneActiveInterface;
use AppBundle\Manager\BaseManager;


abstract class NoUpdate extends BaseManager implements OneActiveInterface
{
    final public function update()
    {
        return;
    }
}