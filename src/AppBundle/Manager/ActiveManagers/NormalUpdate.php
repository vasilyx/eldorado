<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 13.12.16
 * Time: 10:49
 */

namespace AppBundle\Manager\ActiveManagers;


use AppBundle\Interfaces\ManagerType\AlwaysActiveInterface;
use AppBundle\Manager\BaseManager;

abstract class NormalUpdate extends BaseManager implements AlwaysActiveInterface
{

}