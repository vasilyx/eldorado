<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 20.12.16
 * Time: 10:24
 */

namespace AppBundle\Manager\ActiveManagers;


use AppBundle\Interfaces\ManagerType\ActivableInterface;
use AppBundle\Interfaces\ManagerType\ChangableActiveInterface;

abstract class ActiveUpdateByValue extends ActiveUpdateById  implements ChangableActiveInterface
{
    /**
     * @param ActivableInterface $activable
     * @return mixed
     */
    public function makePrimary(ActivableInterface $activable)
    {
        $activable->setActive(true);

        return $activable;
    }
}