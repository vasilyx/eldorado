<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 20.12.16
 * Time: 10:13
 */

namespace AppBundle\Manager\ActiveManagers;


use AppBundle\Interfaces\ManagerType\ManyActiveInterface;
use AppBundle\Manager\BaseManager;

abstract class ActiveUpdateById extends BaseManager implements ManyActiveInterface
{
    public function updateInTransaction(callable $function)
    {
        $this->em->transactional($function);
    }
}