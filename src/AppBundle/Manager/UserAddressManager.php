<?php

namespace AppBundle\Manager;

use AppBundle\Entity\User;
use AppBundle\Entity\UserAddress;
use AppBundle\Manager\ActiveManagers\ActiveUpdateById;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class UserAddressManager extends ActiveUpdateById
{
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repository;


    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->repository = $this->em->getRepository(UserAddress::class);
    }

    /**
     * @param User $user
     * @return UserAddress[]|array
     */
    public function getAddressList(User $user)
    {
        return $this->repository->findBy([
            'user'    => $user,
            'archive' => false,
        ], ['moved' => 'DESC']);
    }

    /**
     * @param User $user
     * @param Criteria|null $criteria
     * @return UserAddress
     */
    public function getCurrentAddress(User $user, Criteria $criteria = null)
    {
        $criteria = new Criteria();
        $criteria
            ->orderBy(['moved' => Criteria::DESC])
            ->setMaxResults(1);
        $currentAddress = $this->repository->getUserActiveAddresses($user, $criteria);

        return $currentAddress ? $currentAddress[0] : [];
    }

    public function createAddress(UserAddress $userAddress)
    {
        $this->em->persist($userAddress);
        $this->em->flush();

        return $userAddress;
    }

    public function deleteAddressById($id)
    {
        $address = $this->repository->find($id);

        if (!$address instanceof UserAddress) {
            throw new NotFoundHttpException('Address not found');
        }

        $this->em->remove($address);
        $this->em->flush();
    }

    protected function deactivateAddress($id)
    {

    }

    public function updateAddress(UserAddress $userAddress)
    {
        return function () use ($userAddress) {

            $this->em->detach($userAddress);
            try {
                $address = $this->repository->findOneBy(['id' => $userAddress->getId(), 'archive' => false]);
                if ($address == $userAddress) {
                    return;
                }
                $address->setArchive(true);
            } catch (\Error $e) {
                return;
            }

            $this->createAddress($userAddress);
        };

    }

    public function getEarlierMovedForUser(User $user)
    {
        $moved = $this->em->getRepository('AppBundle:UserAddress')->getEarlierMovedForUser($user);
        return $moved;
    }
}