<?php
namespace AppBundle\Manager;


use AppBundle\Entity\ContractMatching;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\InvestOfferSub;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Repository\ContractMatchingRepository;
use AppBundle\Repository\InvestOfferRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Pagination\SlidingPagination;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class TransactionManager extends BaseManager
{

    use ContainerAwareTrait;

    /**
     * @var Transaction
     */
    protected $repository;

    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->repository = $this->em->getRepository(Transaction::class);
    }

    /**
     * When all Pending CRDS records for ONE BORROWER will be paid, when
        change LoanApply status to: Active
        change Quote status to: Active
     */
    public function checkPaidCRDS(User $user)
    {
         $transaction = $this->repository->getPendingCreditLinesUser($user);

        if (empty($transaction)) {

            /**
             * @var LoanApply $loanApply
             */
           $loanApply = $this->em->getRepository(LoanApply::class)->findOneBy(['user' => $user]);

            if (!empty($loanApply)) {

                $loanApply->setStatus(LoanApply::STATUS_ACTIVE);
                $quote = $loanApply->getQuoteApply();

                $quote->setStatus(QuoteApply::STATUS_ACTIVE);
                $this->em->persist($loanApply);
                $this->em->persist($quote);
                $this->em->flush();
            }
        }
    }

    public function checkWithdrawal(User $user, $amount, $investmentType, $glAccount)
    {
        $cashAvailable = $this->repository->getCashAvailable($investmentType, $user, $glAccount);

        return $cashAvailable >= $amount;
    }



    public function getPaginated($page, $limit)
    {
        $qb = $this->repository->createQueryBuilder('cm');
        $qb
            ->orderBy('cm.id', 'DESC');

        $query = $qb->getQuery();

        $paginator = $this->container->get('knp_paginator');
        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($query, $page, $limit);

        return array(
            'rows'  => $pagination->getItems(),
            'count' => $pagination->getTotalItemCount()
        );
    }
}