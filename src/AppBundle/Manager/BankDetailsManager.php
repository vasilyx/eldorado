<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 14.12.16
 * Time: 16:07
 */

namespace AppBundle\Manager;


use AppBundle\Entity\BankDetails;
use AppBundle\Entity\User;

/**
 * Class BankDetailsManager
 * @package AppBundle\Manager
 */
class BankDetailsManager extends BaseManager
{
    /**
     * @param User $user
     * @param integer $bankSortCode
     * @param integer $bankAccountNumber
     * @param string $bankAccountName
     * @param bool $flush
     */
    public function update(User $user, $bankSortCode, $bankAccountNumber, $bankAccountName, $flush = false)
    {
        foreach ($this->em->getRepository(BankDetails::class)->findBy([
            'user' => $user,
            'archive' => false
        ]) as $bankDetails) {
            $bankDetails->setArchive(true);
        }

        if ($bankDetails = $this->em->getRepository(BankDetails::class)->findOneBy([
            'user'  => $user,
            'bankSortCode'  => $bankSortCode,
            'bankAccountNumber' => $bankAccountNumber,
            'bankAccountName' => $bankAccountName
        ])) {
            $bankDetails->setArchive(false);
            return;
        }

        $bankDetails = new BankDetails();

        $bankDetails
            ->setUser($user)
            ->setBankSortCode($bankSortCode)
            ->setBankAccountNumber($bankAccountNumber)
            ->setBankAccountName($bankAccountName)
            ->setArchive(false)
        ;

        $this->save($bankDetails, $flush);
    }
}