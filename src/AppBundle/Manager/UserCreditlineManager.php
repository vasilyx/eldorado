<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 12.12.2016
 * Time: 18:59
 */

namespace AppBundle\Manager;


use AppBundle\Entity\User;
use AppBundle\Entity\UserCreditline;
use AppBundle\Repository\UserCreditlineRepository;
use AppBundle\Utils\Refinancing\RefinancingComputerService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserCreditlineManager extends BaseManager
{
    /** @var  UserCreditlineRepository */
    protected $repository;

    protected $refinancingComputer;

    /**
     * UserCreditlineManager constructor.
     * @param EntityManager $em
     * @param RefinancingComputerService $refinancingComputer
     */
    public function __construct(EntityManager $em, RefinancingComputerService $refinancingComputer)
    {
        parent::__construct($em);
        $this->repository = $em->getRepository(UserCreditline::class);

        $this->refinancingComputer = $refinancingComputer;
    }

    /**
     * @param UserCreditline $userCreditline
     * @return UserCreditline
     */
    public function createCreditLine(UserCreditline $userCreditline)
    {
        $this->save($userCreditline, true);

        return $userCreditline;
    }

    /**
     * @param User $user
     * @return UserCreditline[]|array
     */
    public function getCreditLineList(User $user)
    {
        $userCreditlines = $this->repository->getListByUser($user, ['createdAt' => 'ASC']);

        if (is_array($userCreditlines) && !empty($userCreditlines)) {
            foreach ($userCreditlines as $userCreditline) {
                $this->refinancingComputer->setUserCreditline($userCreditline)->addCalculatedProperties();
            }
        }

        return $userCreditlines;
    }

    /**
     * @param $id
     * @param User $user
     * @return UserCreditline|null|object
     */
    public function getActiveByIdAndUser($id, User $user)
    {
        $creditLine = $this->repository->getActiveByIdAndUser($id, $user);

        if (!$creditLine instanceof UserCreditline) {
            throw new NotFoundHttpException('Creditline not found');
        }

        $this->refinancingComputer->setUserCreditline($creditLine)->addCalculatedProperties();

        return $creditLine;
    }

    /**
     * @param $id
     */
    public function deleteCreditLineById($id)
    {
        $creditLine = $this->repository->find($id);

        if (!$creditLine instanceof UserCreditline) {
            throw new NotFoundHttpException('Creditline not found');
        }

        $creditLine->setArchive(true);
        $this->em->persist($creditLine);
        $this->em->flush();
    }
}