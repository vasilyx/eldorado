<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 04.06.2018
 * Time: 0:15
 */

namespace AppBundle\Manager;


use AppBundle\Entity\Agreement;
use AppBundle\Entity\ContractMatching;
use AppBundle\Entity\File;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\UserAddress;
use AppBundle\Repository\FileRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Validator\Constraints\DateTime;

class FileManager extends BaseManager
{
    use ContainerAwareTrait;

    /**
     * @var FileRepository
     */
    protected $repository;

    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->repository = $this->em->getRepository(File::class);
    }

    /**
     * @param LoanApply $loanApply
     * @param InvestOffer $investOffer
     * @param int $type
     * @return File|null|object
     */
    public function getPdfFile(LoanApply $loanApply, InvestOffer $investOffer = null, $type = File::FINAL_BORROWER_AGREEMENT_TYPE)
    {
        $search = [
            'loanApply' => $loanApply,
            'investOffer' => $investOffer,
            'agreement' => null
        ];

        if (!empty($investOffer)) {
            $agreement = $this->em->getRepository(Agreement::class)->findOneBy(['loanApply' => $loanApply]);
            $search['agreement'] = $agreement;
        }

        return $this->repository->findOneBy($search);
    }


    /**
     * @param LoanApply $loanApply
     * @param int $type
     */
    public function generateFilesByLoanApply(LoanApply $loanApply, $type = File::LENDER_AGREEMENT_TYPE)
    {
        $agreement = $this->em->getRepository(Agreement::class)->findOneBy(['loanApply' => $loanApply]);

        $contracts = $this->em->getRepository(ContractMatching::class)->findBy(['loanApply' => $loanApply, 'agreement' => $agreement]);

        $investOfferIds = [];

        foreach ($contracts as $contract)
        {
            if (in_array($contract->getInvestOffer()->getId(), $investOfferIds)) {
                continue;
            }

            array_push($investOfferIds, $contract->getInvestOffer()->getId());

            $fileName = File::ContractBaseDir;

            if (in_array($type, [File::FINAL_BORROWER_AGREEMENT_TYPE, File::ACCEPTANCE_BORROWER_AGREEMENT_TYPE])) {
                $fileName .= File::ContractsBorrowerDir . File::getFileNamesByType()[File::BORROWER_LOAN_AGREEMENT_TYPE];
            } else {
                $fileName .= File::ContractsInvestorDir . File::getFileNamesByType()[File::INVESTOR_LOAN_AGREEMENT_TYPE];
            }

            $fileName .= '_' . $loanApply->getQuoteApply()->getId() . '_' . uniqid($contract->getId()) . '_' . date_timestamp_get(new \DateTime()) . '.pdf';

            $file = new File();
            $file
                ->setLoanApply($loanApply)
                ->setAgreement($agreement)
                ->setInvestOffer($contract->getInvestOffer())
                ->setType($type)
                ->setSaved(false)
                ->setLocation($fileName);

            $this->em->persist($file);
        }

        $this->em->flush();
    }

    /**
     * @param LoanApply $loanApply
     * @param $type
     * @return string
     */
    public function generateFile(LoanApply $loanApply, $type)
    {
        $fileName = File::ContractBaseDir;

        if (in_array($type, [File::FINAL_BORROWER_AGREEMENT_TYPE, File::ACCEPTANCE_BORROWER_AGREEMENT_TYPE])) {
            $fileName .= File::ContractsBorrowerDir . File::getFileNamesByType()[File::BORROWER_LOAN_AGREEMENT_TYPE];
        } else {
            $fileName .= File::ContractsInvestorDir . File::getFileNamesByType()[File::INVESTOR_LOAN_AGREEMENT_TYPE];
        }

        $fileName .= '_' . $loanApply->getId() . '_' . date_timestamp_get(new \DateTime()) . '.pdf';

        $file = new File();
        $file
            ->setLoanApply($loanApply)
            ->setType($type)
            ->setSaved(false)
            ->setLocation($fileName);

        $this->em->persist($file);
        $this->em->flush();

        return $file;
    }

    /**
     *
     * @param File $file
     * @return null
     */
    public function savePdfFile(File $file)
    {
        $fileType = $file->getType();

        if (in_array($fileType, [File::FINAL_BORROWER_AGREEMENT_TYPE, File::ACCEPTANCE_BORROWER_AGREEMENT_TYPE])) {
            $data = $this->container->get('whs.manager.file_manager')->getDataForBorrowerContractFile($file->getLoanApply(), $file->getType());

            if (!$data) {
                return null;
            }

            $data['doc'] = true;

            $this->container->get('knp_snappy.pdf')->generateFromHtml(
                $this->container->get('twig')->render(
                    '@App/pdf/agreements/agreement.html.twig',
                    $data
                ),
                $this->container->get('kernel')->getRootDir() . $file->getLocation()
            );

            $file->setSaved(true);
            $this->em->persist($file);
            $this->em->flush();
        }

        if (in_array($fileType, [File::LENDER_AGREEMENT_TYPE])) {
            $data = $this->container->get('whs.manager.file_manager')->getDataForLenderContractFile($file->getAgreement(), $file->getInvestOffer(), $file->getType());

            if (!$data) {
                return null;
            }

            $data['doc'] = true;

            $this->container->get('knp_snappy.pdf')->generateFromHtml(
                $this->container->get('twig')->render(
                    '@App/pdf/agreements/agreement_lender.html.twig',
                    $data
                ),
                $this->container->get('kernel')->getRootDir() . $file->getLocation()
            );

            $file->setSaved(true);
            $this->em->persist($file);
            $this->em->flush();
        }

        return $file;
    }

    /**
     * @param Agreement $agreement
     * @param InvestOffer $investOffer
     * @param int $type
     * @return array|null
     * @internal param ContractMatching $contractMatching
     * @internal param InvestOffer $investOffer
     */
    public function getDataForLenderContractFile(Agreement $agreement, InvestOffer $investOffer, $type = File::LENDER_AGREEMENT_TYPE)
    {
        $contracts = $this->em->getRepository(ContractMatching::class)->findBy(['agreement' => $agreement, 'investOffer' => $investOffer]);

        if (empty($contracts)) {
            return null;
        }

        $borrower = $agreement->getLoanApply()->getUser();
        $lender = $investOffer->getUser();
        $loanSum = 0;

        /** @var ContractMatching $contract */
        foreach ($contracts as $contract) {
            $loanSum += $contract->getContractAmount();
        }

        $paymentsComputer = $this->container->get('app.utils.payments_computer');
        $creditPayments = $paymentsComputer->getCreditPaymentsTotal($agreement->getApr(), $agreement->getTerm(), $loanSum);
        $effectiveRate = $agreement->getApr() - $agreement->getBonusRate();
        $effectivePayments = $paymentsComputer->getCreditPaymentsTotal($effectiveRate, $agreement->getTerm(), $loanSum);

        /** @var UserAddressManager $addressManager */
        $addressManager = $this->container->get('whs.manager.user_address_manager');
        /** @var UserAddress $address */
        $address = $addressManager->getCurrentAddress($lender);
        $acceptanceDate = new \DateTime($agreement->getAcceptanceDate()->format('d-M-Y'));
        $data = [
            'first_name' => $borrower->getDetails()->getFirstName(),
            'total_loan_value' => $loanSum,
            'apr' => $agreement->getApr(),
            'term' => $agreement->getTerm(),
            'monthly' => $creditPayments['monthlyPayment'],
            'bonus_rate' => $agreement->getBonusRate(),
            'bonus_value' => $creditPayments['totalInterest'] - $effectivePayments['totalInterest'],
            'product' => $agreement->getLoanApply()->getQuoteApply()->getProduct()->getName(),
            'product_description' => $agreement->getLoanApply()->getQuoteApply()->getProduct()->getDescription(),
            'borrower_id' => $borrower->getId(),
            'borrowers' => [
                [
                    'whs_member_id' => $lender->getId(),
                    'amount' => $loanSum,
                    'monthly' => $creditPayments['monthlyPayment'],
                    'first_monthly' => $creditPayments['firstMonthly'],
                    'tap' => $creditPayments['totalPaid'],
                    'fee' => $creditPayments['totalInterest']
                ]
            ],
            'total_amount' => $loanSum,
            'total_monthly' => $creditPayments['monthlyPayment'],
            'total_first_monthly' => $creditPayments['firstMonthly'],
            'total_tap' => $creditPayments['totalPaid'],
            'total_fee' => $creditPayments['totalInterest'],
            'whs_member_id' => $lender->getId(),
            'lender_name' => $lender->getFullName(),
            'lender_address' => !empty($address) ? $address->getCity() . ', ' . $address->getLine1() . ',' . $address->getPostalCode() : '',
            'tap' => $creditPayments['totalPaid'],
            'first_monthly' => $creditPayments['firstMonthly'],
            'acceptance_date' => $acceptanceDate,
            'first_payment_date' => $this->getPeriodPaymentDate($agreement, 1),
            'charges' => $creditPayments['totalInterest'],
            'repayment_day' => $agreement->getPaymentDay()
        ];

        return $data;
    }

    public function getDataForBorrowerContractFile(LoanApply $loanApply, $type = File::FINAL_BORROWER_AGREEMENT_TYPE)
    {
        /** @var Agreement $agreement */
        $agreement = $this->em->getRepository(Agreement::class)->getAgreementLoanApply($loanApply);

        if (empty($agreement)) {
            return null;
        }

        /** @var UserAddressManager $addressManager */
        $addressManager = $this->container->get('whs.manager.user_address_manager');
        $user = $loanApply->getUser();
        /** @var UserAddress $address */
        $address = $addressManager->getCurrentAddress($user);

        $paymentsComputer = $this->container->get('app.utils.payments_computer');
        $creditPayments = $paymentsComputer->getCreditPaymentsTotal($agreement->getApr(), $agreement->getTerm(), $agreement->getAmount());
        $effectiveRate = $agreement->getApr() - $agreement->getBonusRate();
        $effectivePayments = $paymentsComputer->getCreditPaymentsTotal($effectiveRate, $agreement->getTerm(), $agreement->getAmount());

        $acceptanceDate = $agreement->getAcceptanceDate();

        $data = [
            'full_name' => $user->getFullName(),
            'first_name' => $user->getDetails()->getFirstName(),
            'total' => $agreement->getAmount(),
            'apr' => $agreement->getApr(),
            'term' => $agreement->getTerm(),
            'monthly' => $creditPayments['monthlyPayment'],
            'first_monthly_payment' => abs($creditPayments['firstMonthly']),
            'bonus_rate' => $agreement->getBonusRate(),
            'bonus_value' => $creditPayments['totalInterest'] - $effectivePayments['totalInterest'],
            'whs_member_id' => $user->getId(),
            'address' => !empty($address) ? $address->getCity() . ', ' . $address->getLine1() . ',' . $address->getPostalCode() : '',
            'product' => $loanApply->getQuoteApply()->getProduct()->getName(),
            'product_description' => $loanApply->getQuoteApply()->getProduct()->getDescription(),
            'acceptance_date' => $acceptanceDate,
            'payment_day' => $agreement->getPaymentDay() ? $agreement->getPaymentDay() : '1st day',
            'first_payment_date' => (new \DateTime())->modify('first day of next month'),
            'interest' => $creditPayments['totalPaid'] - $agreement->getAmount(),
            'show_table' => false,
            'doc_type' => $type,
            'total_payable' => $creditPayments['totalPaid'],
            'total_fee' => 0,
            'total_monthly' => 0,
            'total_first_monthly' => 0,
            'total_amount' => 0,
            'contracts' => []
        ];

        if ($type == File::FINAL_BORROWER_AGREEMENT_TYPE) {
            $data['total_payable'] = 0;

            $contracts = $this->em->getRepository(ContractMatching::class)->findBy(['loanApply' => $loanApply, 'agreement' => $agreement]);
            $contactsByLenderId = [];

            foreach ($contracts as $contract) {
                $lender = $contract->getInvestOffer()->getUser();
                if (empty($contactsByLenderId[$lender->getId()])) {
                    $contactsByLenderId[$lender->getId()] = [
                        'whs_member_id' => $lender->getId(),
                        'amount' => 0,
                        'agreement' => $contract->getAgreement()
                    ];
                }

                $contactsByLenderId[$lender->getId()]['amount'] += $contract->getContractAmount();
            }

            $paymentComputer = $this->container->get('app.utils.payments_computer');
            foreach ($contactsByLenderId as $contactValue) {
                /** @var Agreement $agreement */
                $agreement = $contactValue['agreement'];
                $contractValues = $paymentComputer->getCreditPaymentsTotal($agreement->getApr(), $agreement->getTerm(), $contactValue['amount']);

                $contactRow = [
                    'whs_member_id' => $contactValue['whs_member_id'],
                    'amount' => $contactValue['amount'],
                    'monthly' => $contractValues['monthlyPayment'],
                    'first_monthly' => $contractValues['firstMonthly'],
                    'tap' => $contractValues['totalPaid'],
                    'fee' => $contractValues['totalPaid'] - $contactValue['amount'],
                ];

                $data['contracts'][] = $contactRow;

                $data['total_amount'] += $contactRow['amount'];
                $data['total_payable'] += $contactRow['tap'];
                $data['show_table'] = true;
                $data['total_monthly'] += $contactRow['monthly'];
                $data['total_first_monthly'] += $contactRow['first_monthly'];
                $data['total_fee'] += $contactRow['fee'];
                $data['payment_day'] = $agreement->getPaymentDay() ? $agreement->getPaymentDay() : '1st day';
                $data['first_payment_date'] = $this->getPeriodPaymentDate($agreement, 1);
            }
        }

        return $data;
    }

    /**
     * @param Agreement $agreement
     * @param $period
     * @return \DateTime
     */
    protected function getPeriodPaymentDate(Agreement $agreement, $period)
    {
        $acceptanceDate = new \DateTime($agreement->getAcceptanceDate()->format('d-M-Y'));
        $paymentDay = $agreement->getPaymentDay();

        $months = $period - 1;
        if ($months) {
            $acceptanceDate->modify("+$months months");
        }

        $acceptanceDate->modify('first day of next month');

        if (!empty($paymentDay)) {
            $paymentDay -= 1;
            $acceptanceDate->modify("+$paymentDay days");
        }

        return $acceptanceDate;
    }
}