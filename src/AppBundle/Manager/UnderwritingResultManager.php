<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 05.01.2017
 * Time: 16:03
 */

namespace AppBundle\Manager;


use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\Product;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\QuoteTerm;
use AppBundle\Entity\UnderwritingResult;
use AppBundle\Exception\NotAvailableValueException;
use AppBundle\Utils\Payment\InterestRatesProviderInterface;
use AppBundle\Utils\Payment\PaymentsComputerService;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use LogBundle\Entity\UserActivityLog;
use LogBundle\Handler\UserActivityHandler;
use Monolog\Logger;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class UnderwritingResultManager extends BaseManager
{
    private $paymentsService;
    private $logger;
    private $investOfferManager;

    public function __construct(EntityManager $em, PaymentsComputerService $paymentsComputerService, Logger $logger, InvestOfferManager $investOfferManager)
    {
        parent::__construct($em);
        $this->logger = $logger;
        $this->paymentsService = $paymentsComputerService;
        $this->investOfferManager = $investOfferManager;
    }

    /**
     * Parse CallReportResponse and pickup Guage Score
     * Change quote status if necessary
     * Call function for calculating underwritingResult values and commit changes
     *
     * @param CallCreditAPICalls $callCredit
     * @param $type
     * @throws NotAvailableValueException
     */
    public function borrowerUnderwritingAlgorithm(CallCreditAPICalls $callCredit, $type)
    {
        $domDocument = new \DOMDocument();
        $domDocument->loadXML($callCredit->getResponseXML());

        /** @var QuoteApply $quoteApply */
        $quoteApply = $this->em->getRepository(QuoteApply::class)->find($callCredit->getQuoteApply()->getId());

        if ($domDocument->getElementsByTagName('SCORE')->length > 0) {
            $gaugeScore = $domDocument->getElementsByTagName('SCORE')->item(0)
                ->getElementsByTagName('MAIN')->item(0)->getAttribute('JF');
        } else {
            $quoteApply->setStatus(QuoteApply::STATUS_REFER);
            $this->save($quoteApply, true);

            throw new NotAvailableValueException("Guage score is null");
        }

        if ($type == UnderwritingResult::TYPE_QUOTE) {
            $productRepository = $this->em->getRepository(Product::class);

            /**
             *  IT IS IMPORTANT PART!
             *  Change product type based on gauge score BEFORE change quote status
             */
            if ($gaugeScore >= QuoteApply::GUAGE_SCORE_APPROVE) {
                $quoteApply->setProduct($productRepository->findOneByCode(Product::BORCONS1));
                $quoteApply->setStatus(QuoteApply::STATUS_APPROVED);
            } elseif ($gaugeScore < QuoteApply::GUAGE_SCORE_REJECT) {
                $quoteApply->setProduct($productRepository->findOneByCode(Product::BORCONS3));
                $quoteApply->setStatus(QuoteApply::STATUS_REJECTED);
            } else {
                $quoteApply->setProduct($productRepository->findOneByCode(Product::BORCONS2));
                //TODO: TEMPORARY APPROVED (need to be REFER)
                //$quoteApply->setStatus(QuoteApply::STATUS_REFER);
                $quoteApply->setStatus(QuoteApply::STATUS_APPROVED);
            }

            $this->logger->info('', [
                    UserActivityHandler::CONTEXT_USER     => $quoteApply->getUser(),
                    UserActivityHandler::CONTEXT_ACTION   => UserActivityLog::SYSTEM_TYPE,
                    UserActivityHandler::CONTEXT_CODE     => UserActivityLog::NEW_PRODUCT_ACTION,
                    UserActivityHandler::CONTEXT_METADATA => json_encode(['product_description' => $quoteApply->getProduct()->getDescription()]),
                ]
            );

            $this->save($quoteApply, true);
        }

        $upfrontFee = 0;

        $underwritingResult = $this->calculateUnderwritingResult($quoteApply->getId(), $type, $upfrontFee, $gaugeScore);
        $this->createUnderwritingResult($underwritingResult);
    }

    /**
     * Calculate values for underwritingResult object
     */
    private function calculateUnderwritingResult($quoteApplyId, $type, $upfrontFee, $gaugeScore)
    {
        $underwritingResult = new UnderwritingResult();

        $quoteApply = $this->em->getRepository(QuoteApply::class)->find($quoteApplyId);

        $term = null;
        $amount = null;

        if ($type == UnderwritingResult::TYPE_QUOTE) {
            /** @var QuoteTerm $quoteTerm */
            $quoteTerm = $this->em->getRepository(QuoteTerm::class)->getQuoteTermForQuoteApply($quoteApply);
            $term = $quoteTerm->getTerm();
            $amount = $quoteTerm->getAmount();
        } elseif ($type == UnderwritingResult::TYPE_CREDIT) {
            $loanApply = $this->em->getRepository(LoanApply::class)->findOneBy(['quoteApply' => $quoteApply]);
            $term = $loanApply->getTerm();
            $amount = $loanApply->getAmount();
        }

        $bonusRate = $this->paymentsService->getDefaultInterestRate($term, $amount, InterestRatesProviderInterface::RATE_TYPE_BONUS);
        $investorRate = $this->paymentsService->getDefaultInterestRate($term, $amount, InterestRatesProviderInterface::RATE_TYPE_INVESTOR);
        $effectiveRate = $this->paymentsService->getDefaultInterestRate($term, $amount, InterestRatesProviderInterface::RATE_TYPE_EFFECTIVE);
        $apr = $this->paymentsService->getDefaultInterestRate($term, $amount, InterestRatesProviderInterface::RATE_TYPE_CONTRACT);
        $brokerRate = $this->paymentsService->getDefaultInterestRate($term, $amount, InterestRatesProviderInterface::RATE_TYPE_BROKER);
        $loanPrincipleAmount = $amount + $upfrontFee;
        $creditPayments = $this->paymentsService->getCreditPaymentsTotal($apr, $term, $loanPrincipleAmount);
        $effectivePayments = $this->paymentsService->getCreditPaymentsTotal($effectiveRate, $term, $loanPrincipleAmount);
        $bonusAmount = $creditPayments['totalInterest'] - $effectivePayments['totalInterest'];

        $underwritingResult
            ->setQuoteApply($quoteApply)
            ->setType($type)
            ->setLoanAmount($loanPrincipleAmount)
            ->setScore1($gaugeScore)
            ->setBonusRate($bonusRate)
            ->setInvestorRate($investorRate)
            //changes
            ->setEffectiveRate($effectiveRate)
            ->setApr($apr)
            ->setBonusAmount($bonusAmount)
            ->setTotalRepaid($creditPayments['totalPaid'])
            ->setMonthly($creditPayments['monthlyPayment'])
            ->setInvestorInterestAmount($creditPayments['totalInterest'])
            ->setFixedAmount($upfrontFee)
            ->setManagementAmount($brokerRate)
            ->setTerm($term);

        return $underwritingResult;
    }

    public function lenderUnderwritingAlgorithm(CallCreditAPICalls $callCreditAPICalls, $type)
    {
        $qouteApply = $callCreditAPICalls->getQuoteApply();
        //TODO: https://jira.effective-soft.com/browse/CU540-560
        /**
          *  $investOffer = $this->em->getRepository(InvestOffer::class)
          *      ->getInvestOfferForUser($callCreditAPICalls->getUser(), Criteria::create()
          *          ->orderBy(['createdAt' => Criteria::DESC])
          *          ->setMaxResults(1));
          *  if (!$investOffer) {
          *      throw new NotFoundResourceException('InvestOffer not found.');
          *  }
          *
          *  if (is_array($investOffer)) {
          *      $investOffer = $investOffer[0];
          *  }
         **/

        switch ($callCreditAPICalls->getStatus()) {
            case (CallCreditAPICalls::STATUS_SUCCESS):
                //TODO: https://jira.effective-soft.com/browse/CU540-560
                //following the task description: quote - status verification, investOffer stays draft?
                $qouteApply->setStatus(QuoteApply::STATUS_PENDING);
                //TODO: get information about suboffers
                //$this->investOfferManager->createSubOffers($investOffer);

                break;
            case (CallCreditAPICalls::STATUS_ERROR):
                $qouteApply->setStatus(QuoteApply::STATUS_REJECTED);
//                $investOffer->setActive(false);
                break;
            default:
                break;
        }
        $this->em->persist($qouteApply);
        $this->em->flush();
    }

    public function createUnderwritingResult(UnderwritingResult $underwritingResult)
    {
        $this->save($underwritingResult, true);

        return $underwritingResult;
    }

    public function getUnderwritingResultForQuoteApply(QuoteApply $quoteApply, $type = UnderwritingResult::TYPE_QUOTE)
    {
        /** @var UnderwritingResult $underwritingResult */
        $underwritingResult = $this->em->getRepository(UnderwritingResult::class)
            ->getUnderwritingResultsForQuote($quoteApply, $type);

        if (!$underwritingResult) {
            return [];
        }

        $underwritingResult = is_array($underwritingResult) ? $underwritingResult[0] : $underwritingResult;

        $result = array(
            'loan_amount'  => round($underwritingResult->getLoanAmount(), 2),
            'monthly'      => round($underwritingResult->getMonthly(), 2),
            'total_repaid' => round($underwritingResult->getTotalRepaid(), 2),
            'apr'          => round($underwritingResult->getApr(), 2),
            'bonus_amount' => round($underwritingResult->getBonusAmount(), 2),
            'bonus_rate'   => round($underwritingResult->getBonusRate(), 2),
            'term'         => $underwritingResult->getTerm()
        );

        return $result;
    }
}