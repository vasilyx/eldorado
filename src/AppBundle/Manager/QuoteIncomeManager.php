<?php
namespace AppBundle\Manager;

use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\QuoteIncome;
use AppBundle\Entity\User;
use AppBundle\Exception\QuoteApplyNotFoundException;
use AppBundle\Manager\ActiveManagers\NoUpdate;


class QuoteIncomeManager extends NoUpdate
{
    public function addQuoteIncome(QuoteIncome $quoteIncome, User $user)
    {
        $quoteApply = $this->em->getRepository('AppBundle:QuoteApply')->getQuoteAppliesForUser($user, [QuoteApply::STATUS_DRAFT]);

        if (!$quoteApply) {
            throw new QuoteApplyNotFoundException(QuoteApplyNotFoundException::DRAFT_QUOTE_APPLY_NOT_FOUND);
        }

        $oldQuoteIncome = $this->em->getRepository('AppBundle:QuoteIncome')->getQuoteIncomeForUser($user, false);
        if ($oldQuoteIncome) {
            $oldQuoteIncome->setArchiveFalse();
        }
        $quoteIncome->setQuoteApply($quoteApply[0]);
        $this->em->persist($quoteIncome);
        $this->em->flush();

        return $quoteIncome;
    }

    /**
     * @param User $user
     * @param bool $archive
     * @return QuoteIncome
     */
    public function getQuoteIncome(User $user, $archive = false)
    {
        $quoteIncome = $this->em->getRepository('AppBundle:QuoteIncome')->getQuoteIncomeForUser($user, $archive);
        return $quoteIncome;
    }
}