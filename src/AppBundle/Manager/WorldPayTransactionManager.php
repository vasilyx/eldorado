<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 19.12.16
 * Time: 16:40
 */

namespace AppBundle\Manager;


use AppBundle\Entity\User;
use AppBundle\Entity\WorldpayHtmlTransaction;
use AppBundle\Entity\WorldpayTransaction;

class WorldPayTransactionManager extends BaseManager
{
    protected $installationId;
    protected $callbackUrl;

    /**
     * @param mixed $installationId
     */
    public function setInstallationId($installationId)
    {
        $this->installationId = $installationId;
    }

    /**
     * @param mixed $callbackUrl
     */
    public function setCallbackUrl($callbackUrl)
    {
        $this->callbackUrl = $callbackUrl;
    }

    public function createWorldpayHtmlTransaction(User $user, $amount, $customerOrderCode, $currencyCode, $andFlush = false)
    {
        return $this->save(
            WorldpayHtmlTransaction::create($user)
                ->setAmount($amount)
                ->setInstallationId($this->installationId)
                ->setCallbackUrl($this->callbackUrl)
                ->setCustomerOrderCode($customerOrderCode)
                ->setCurrencyCode($currencyCode)
                ->setPaymentStatus(WorldpayTransaction::PAYMENT_STATUS_PENDING)
            ,
            $andFlush);
    }
}