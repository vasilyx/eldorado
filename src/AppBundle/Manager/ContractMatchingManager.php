<?php
namespace AppBundle\Manager;


use AppBundle\Entity\Agreement;
use AppBundle\Entity\AgreementProcess;
use AppBundle\Entity\BaseProcess;
use AppBundle\Entity\ContractMatching;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\InvestOfferProcess;
use AppBundle\Entity\InvestOfferSub;

use AppBundle\Entity\LoanApply;
use AppBundle\Entity\Transaction;
use AppBundle\Event\SystemTransactionEvent;
use AppBundle\Event\TransactionEvents;
use AppBundle\Repository\AgreementRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class ContractMatchingManager extends BaseManager
{
    protected $limit_investor_match_amount = 25000;
    protected $limit_investor_match_percent = 20;

    protected $dispatcher;

    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        parent::__construct($em);
        $this->dispatcher = $dispatcher;
    }

    public function matchOffers()
    {
        /**
         * @var $agreements AgreementRepository
         */
        $agreements = $this->em->getRepository('AppBundle:Agreement')->getContractMatchingAgreement();


        /**
         * @var $agreement Agreement
         */
        foreach ($agreements as $agreement) {

            $agreement_needed_amount = $this->getNeededAmount($agreement);
            $agreement_matched_amount = 0;

            $invest_offer_list = $this->em->getRepository('AppBundle:InvestOffer')->getInvestOfferListContractMatching(
                $agreement
            );

            /**
             *  @var $invest_offer InvestOffer
             */
            foreach ($invest_offer_list as $invest_offer) {

                // Check if investor has matched rows for agreement
                $check_if_matched = $this->em->getRepository('AppBundle:ContractMatching')->getMatchedSumByAgreementOffer(
                    $agreement,
                    $invest_offer
                );

                if ($check_if_matched > 0) {continue;};

                $invested_sum = (int)$this->em->getRepository('AppBundle:ContractMatching')->getSumForInvestor(
                    $invest_offer
                );

                // Investor doesn't have money
                if ($invested_sum === $invest_offer->getInvestmentAmount()) {continue;};


                $previous_matched_amount = $agreement_matched_amount;
                $agreement_matched_amount = $this->matchSubContracts($invest_offer, $agreement, $agreement_matched_amount, $agreement_needed_amount);

                $invest_offer_process = new InvestOfferProcess();
                $invest_offer_process->setInvestOffer($invest_offer);
                $this->em->persist($invest_offer_process);
                $this->em->flush();

                if ($previous_matched_amount == $agreement_matched_amount) {continue;};


                $this->dispatcher->dispatch(TransactionEvents::ALOC,
                    SystemTransactionEvent::create()
                        ->setUser($invest_offer->getUser())
                        ->setProcess($invest_offer_process)
                        ->addTransaction(
                            Transaction::create()
                                ->setAgreementId($agreement->getId())
                                ->setTransactionAmount($agreement_matched_amount)
                                ->setTransactionOwner(Transaction::TRANSACTION_OWNER_INVESTOR)
                                ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_CASH_ALLOCATED)
                        )
                );

                if ($agreement_matched_amount == $agreement_needed_amount) {

                    $agreement_process = new AgreementProcess();
                    $agreement_process->setAgreement($agreement);
                    $this->em->persist($agreement_process);
                    $this->em->flush();

                    $loanApply = $agreement->getLoanApply();
                    $loanApply->setStatus(LoanApply::STATUS_FULLY_MATCHED);
                    $agreement->setFullyMatchedDate(new \DateTime());
                    $this->em->persist($loanApply);
                    $this->em->persist($agreement);


                    $this->generateAGRFTransactions($agreement, $agreement_process);

                    $this->dispatcher->dispatch(TransactionEvents::CRDS,
                        SystemTransactionEvent::create()
                            ->setUser($agreement->getLoanApply()->getUser())
                            ->setProcess($agreement_process)
                            ->addTransaction(
                                Transaction::create()
                                    ->setOfferId($invest_offer->getId())
                                    ->setAgreementId($agreement->getId())
                                    ->setInvestmentType($invest_offer->getInvestmentType())
                                    ->setTransactionOwner(Transaction::TRANSACTION_OWNER_INVESTOR)
                                    ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_CASH_ALLOCATED)
                            )
                            ->setArgument('contractMatching', true)
                    );

                    // CU540-589
                    $loanApply->setStatus(LoanApply::STATUS_SIGNED);
                    $this->em->persist($loanApply);

                    break;
                }
            }

            $this->em->flush();
        }
    }

    /**
     * @param Agreement $agreement
     * @param AgreementProcess $process
     */
    protected function generateAGRFTransactions(Agreement $agreement, AgreementProcess $process)
    {
        $offerList = $this->em->getRepository(ContractMatching::class)->getSumOffersByAgreement(
            $agreement
        );

        foreach ($offerList as $offer) {

            if (!$offer['summ']) {
                continue;
            }

            $investOffer = $this->em->getRepository(InvestOffer::class)->find($offer['offer']);

            $this->dispatcher->dispatch(TransactionEvents::AGRF,
                SystemTransactionEvent::create()
                    ->setUser($investOffer->getUser())
                    ->setProcess($process)
                    ->addTransaction(
                        Transaction::create()
                            ->setOfferId($investOffer->getId())
                            ->setAgreementId($agreement->getId())
                            ->setInvestmentType($investOffer->getInvestmentType())
                            ->setTransactionAmount($offer['summ'])
                            ->setTransactionOwner(Transaction::TRANSACTION_OWNER_INVESTOR)
                            ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_OFFER_ALLOCATED)
                    )
                    ->setArgument('agreementUser', $agreement->getLoanApply()->getUser())
                    ->setArgument('offerUser', $investOffer->getUser())
            );
        }
    }


    protected function matchSubContracts(InvestOffer $invest_offer, Agreement $agreement, $agreement_matched_amount, $agreement_needed_amount)
    {
        $free_invest_offer_sub = $this->em->getRepository('AppBundle:InvestOfferSub')->getNotMatched(
            $invest_offer
        );

        $amount_20_percent = ($invest_offer->getInvestmentAmount() * $this->limit_investor_match_percent) / 100; // 20% of investor sum
        $offer_sub_matched = 0;


        /**
         * @var $invest_offer_sub InvestOfferSub
         */
        foreach ($free_invest_offer_sub as $invest_offer_sub) {
            $contract_matching = new ContractMatching();
            $contract_matching->setAgreement($agreement);
            $contract_matching->setLoanApply($agreement->getLoanApply());
            $contract_matching->setStatus(ContractMatching::STATUS_PENDING);
            $contract_matching->setInvestOffer($invest_offer);
            $contract_matching->setInvestOfferSub($invest_offer_sub);
            $contract_matching->setContractAmount($invest_offer_sub->getAmount());

            $this->em->persist($contract_matching);

            $agreement_matched_amount += $invest_offer_sub->getAmount();
            $offer_sub_matched += $invest_offer_sub->getAmount();

         //   echo $invest_offer->getId() . ' - ' . $amount_20_percent . ' - ' . $offer_sub_matched . "\r\n";

            if (
                $agreement_matched_amount == $agreement_needed_amount
                || $amount_20_percent == $offer_sub_matched
                || $offer_sub_matched == $this->limit_investor_match_amount
            ) {
                return $agreement_matched_amount;
            }
        }

        return $agreement_matched_amount;
    }

    public function getNeededAmount(Agreement $agreement)
    {
        $matched_sum = $this->em->getRepository(ContractMatching::class)->getMatchedSumByAgreement(
            $agreement
        );

        return ((int)$agreement->getAmount() - (int)$matched_sum);
    }


    public function createContractMatchingResult(ContractMatching $contractMatching)
    {
        return $this->save($contractMatching, true);
    }
}