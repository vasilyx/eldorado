<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 11.12.2016
 * Time: 19:13
 */

namespace AppBundle\Manager;


use AppBundle\Entity\User;
use AppBundle\Entity\UserDependent;
use AppBundle\Repository\UserDependentRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class UserDependentManager extends BaseManager
{
    /** @var UserDependentRepository $repository  */
    protected $repository;
    private $authorizationChecker;

    public function __construct(EntityManager $em, AuthorizationCheckerInterface $authorizationChecker)
    {
        parent::__construct($em);
        $this->repository = $this->em->getRepository(UserDependent::class);
        $this->authorizationChecker = $authorizationChecker;
    }

    public function createUserDependent(UserDependent $userDependent)
    {
        $this->em->persist($userDependent);
        $this->em->flush();

        return $userDependent;
    }

    public function updateUserDependent(UserDependent $userDependent)
    {
        try {

            $this->em->persist($userDependent);
            $this->getUserDependentList($userDependent->getUser());

        } catch (\Error $e) {
            return;
        }

        $this->em->flush();
    }

    public function getUserDependentList(User $user)
    {
        return $this->repository->findBy(['user' => $user, 'archive' => false]);
    }

    public function deleteDependentById($id)
    {
        $dependent = $this->repository->find($id);

        if ($dependent instanceof UserDependent){

            $dependent->setArchive(true);
            $this->em->persist($dependent);
            $this->em->flush();

        } else {
            throw new NotFoundHttpException('Dependent not found');
        }
    }

    public function getDependentById($id)
    {
        $dependent = $this->repository->find($id);

        if ($dependent instanceof UserDependent)  {

            return $dependent;

        } else {
            throw new NotFoundHttpException('Dependent not found');
        }
    }

    public function deleteDependentsForUser(User $user)
    {
        $dependents = $this->repository->getAllActiveDependentsForUser($user);

        /** @var UserDependent $dependent */
        foreach ($dependents as $dependent)
        {
            $dependent->setArchive(true);
            $this->em->persist($dependent);
        }

        $this->em->flush();
    }
}