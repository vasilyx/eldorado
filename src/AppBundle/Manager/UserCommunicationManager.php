<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Communication;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCommunication;
use AppBundle\Entity\UserCommunicationVerification;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class UserCommunicationManager
{
    use ContainerAwareTrait;
    private $em;
    private $repository;
    private $authorizationChecker;


    public function __construct(EntityManager $em, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(UserCommunication::class);
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param   User $user
     * @param   Criteria $criteria
     * @return  UserCommunication[]
     */
    public function getCommunicationList(User $user, Criteria $criteria = null)
    {
        $userCommunications = $this->repository->getUserCommunication($user, $criteria);
        $result = [];
        if ($userCommunications) {
            foreach ($userCommunications as $userCommunication) {
                $item['id'] = $userCommunication->getCommunication()->getId();
                $item['type'] = $userCommunication->getCommunication()->getType();
                $item['value'] = $userCommunication->getValue();
                $item['primary'] = $userCommunication->getIsPrimary();
                if (Communication::TYPE_MOBILE_PHONE === $userCommunication->getCommunication()->getType() || Communication::TYPE_EMAIL === $userCommunication->getCommunication()->getType()) {
                    $item['verified'] = $userCommunication->getIsVerified() ? true : false;
                }
                $result[] = $item;
                unset($item);
            }
        }
        return $result;
    }

    /**
     * @param User $user
     * @param $email
     */
    public function addVerifiedEmail(User $user, $email)
    {
        $qb = $this->container->get('doctrine.orm.entity_manager')->getRepository('AppBundle:UserCommunication')->createQueryBuilder('userCommunication');

        $qb->leftJoin('userCommunication.communication', 'communication')
            ->where('userCommunication.user = :user')
            ->andWhere('communication.type = ' . Communication::TYPE_EMAIL)
            ->andWhere('userCommunication.archive = 0')
            ->andWhere('userCommunication.isPrimary = 1');
        $qb->setParameter('user', $user);
        /** @var UserCommunication $existedEmail */
        $existedEmail = $qb->getQuery()->getOneOrNullResult();

        if ($existedEmail) {
            if ($existedEmail->getValue() == $email) {
                $existedEmail->setIsVerified(true);
                $this->em->flush();
                return;
            } else {
                $existedEmail->setIsPrimary(false);
            }
        }

        $emailCommunication = (new UserCommunication())
            ->setUser($user)
            ->setCommunication($this->container->get('doctrine.orm.entity_manager')->getRepository('AppBundle:Communication')->findOneBy(['type' => Communication::TYPE_EMAIL]))
            ->setIsVerified(true)
            ->setIsPrimary(true)
            ->setValue($email)
            ->setArchive(false);
        $this->em->persist($emailCommunication);
        $this->em->flush();
    }

    public function getPrimaryUnverifiedMobile(User $user)
    {
        return $this->repository->getPrimaryUnverifiedMobile($user);
    }

    public function getMobileCommunicationForUser(User $user)
    {
        $userCommunications = $this->repository->getUserCommunication($user, null, false, Communication::TYPE_MOBILE_PHONE);
        if (empty($userCommunications)) {
            throw new NotFoundHttpException('No mobile phone is found for the current user');
        }

        return $userCommunications[0];
    }

    public function save(UserCommunication $userCommunication, $andFlush = false)
    {
        $this->em->persist($userCommunication);

        if (true === $andFlush) {
            $this->em->flush();
        }

        return $userCommunication;
    }

    public function createUserCommunication(User $user, $type, $value, $flush = false)
    {

        /** @var UserCommunication[] $existedUserCommunications */
        $existedUserCommunications = $this->em->getRepository(UserCommunication::class)->getUserCommunication($user, null, false, $type);
        if (!empty($existedUserCommunications)) {
            $update = false;
            foreach ($existedUserCommunications as $existedUserCommunication) {
                if ($existedUserCommunication->getValue() === $value) {
                    $existedUserCommunication->setIsPrimary(true);
                    $update = true;
                } else {
                    $existedUserCommunication->setIsPrimary(false);
                }
                $this->save($existedUserCommunication, $flush);
            }

            if (!$update) {
                $uc = new UserCommunication();
                $uc
                    ->setUser($user)
                    ->setCommunication($this->em->getRepository('AppBundle:Communication')->findOneBy(['type' => $type]))
                    ->setValue($value)
                    ->setArchive(false)
                    ->setIsPrimary(true);

                $this->save($uc, $flush);
            }

        } else {
            $uc = new UserCommunication();

            $uc
                ->setUser($user)
                ->setCommunication($this->em->getRepository('AppBundle:Communication')->findOneBy(['type' => $type]))
                ->setValue($value)
                ->setArchive(false)
                ->setIsPrimary(true);

            $this->save($uc, $flush);
        }
    }

    public function saveUserCommunicationVerificationCode(User $user)
    {
        $communication = $this->getMobileCommunicationForUser($user);
        if (!$communication->getIsVerified()) {
            $verificationCode = $this->getVerificationCode();
            $communication
                ->setVerificationCode($verificationCode);
            $this->sendSms($communication->getValue(), 'Verification code: ' . $verificationCode);

            $this->save($communication, true);

            //TODO: remove
            return $verificationCode;
        }

        return '';
    }

    /**
     * @return string
     */
    public function getVerificationCode()
    {
        $random = sha1(rand(11111, 99999));
        $time = sha1(microtime(true));
        $code = substr($random, 0, 2) . substr($time, 0, 3);

        return $code;
    }

    public function sendSms($phone, $message)
    {
        $phone = preg_replace('#[^\d]+#', '', $phone);

        if ($this->container->hasParameter('sms_proxy') && $proxy = $this->container->getParameter('sms_proxy')) {
            $aContext = array(
                'http' => array(
                    'proxy'           => trim($proxy),
                    'request_fulluri' => true,
                ),
            );
            $cxContext = stream_context_create($aContext);
        } else {
            $cxContext = null;
        }
        $baseurl = 'http://api.clickatell.com';
        $username = $this->container->getParameter('sms_clickatell_user');
        $password = $this->container->getParameter('sms_clickatell_password');
        $apiId = $this->container->getParameter('sms_clickatell_api_id');
        if (!$username || !$password || !$apiId) {
            throw new \InvalidArgumentException('SMS auth parameters are missing');
        }

        $message = urlencode($message);

        // auth call

        $url = "$baseurl/http/auth?user=$username&password=$password&api_id=$apiId";


        $logger = $this->container->get('logger');
        $logger->debug('SMS: ' . $phone . ' begin');
        $logger->debug('SMS: ' . $phone . ' ' . $message);

        // do auth call
        try {
            $ret = file($url, null, $cxContext);
            // explode our response. return string is on first line of the data returned
            $sess = explode(':', $ret[0]);
            if ('OK' == $sess[0]) {
                $sess_id = trim($sess[1]); // remove any whitespace
                $url = "$baseurl/http/sendmsg?session_id=$sess_id&to=$phone&text=$message";
                // do sendmsg call
                $ret = file($url, null, $cxContext);
                $send = explode(':', $ret[0]);
                if ('ID' == $send[0]) {
                    //echo "successnmessage ID: ". $send[1];
                    $logger->debug('SMS: ' . $phone . ' ok');
                    return $send[1];
                } else {
                    $logger->debug('SMS: ' . $phone . ' failed');
                    //echo "send message failed";
                    return null;
                }
            } else {
                $logger->debug('SMS auth failed ' . $ret[0]);
                //echo "Authentication failure: ". $ret[0];
                return null;
            }
        } catch (\Exception $e) {
            return null;
        }
    }
}