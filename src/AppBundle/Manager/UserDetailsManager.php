<?php
namespace AppBundle\Manager;

use AppBundle\Entity\User;
use AppBundle\Entity\UserDetails;
use AppBundle\Manager\ActiveManagers\NormalUpdate;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class UserDetailsManager extends NormalUpdate
{
    public function updateDetails(UserDetails $detailsEntity)
    {
        return $this->save($detailsEntity, true);
    }

    /**
     * @param User $user
     * @return UserDetails
     */
    public function getDetailsForUser(User $user)
    {
        if (!$ud = $this->em->getRepository(UserDetails::class)->findOneByUser($user)) {
            $ud = new UserDetails();
            $ud
                ->setUser($user);
        };

        return $ud;
    }

    public function persist(UserDetails $userDetails)
    {
        $this->em->persist($userDetails);

        return $userDetails;
    }
}