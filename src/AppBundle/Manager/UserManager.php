<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Communication;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCommunication;
use AppBundle\Entity\UserSession;
use AppBundle\Event\AuthEvents;
use AppBundle\Interfaces\RoleResolverInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

class UserManager
{
    use ContainerAwareTrait;

    private $em;
    private $repository;
    private $encoderFactory;
    /**
     * @var RoleResolverInterface $roleResolver
     */
    private $roleResolver;

    public function __construct(EntityManager $em, EncoderFactory $encoderFactory, RoleResolverInterface $roleResolver)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository("AppBundle:User");
        $this->encoderFactory = $encoderFactory;
        $this->roleResolver = $roleResolver;
    }

    public function registerUser(User $user)
    {
        $encoder = $this->getEncoder($user);
        $user->setPassword($encoder->encodePassword($user->getPassword(), $user->getSalt()));
        $user->setPlainPassword($encoder->encodePassword($user->getPlainPassword(), $user->getSalt()));
        $user->generateConfirmationToken();
        $this->em->persist($user);

        $preferences = $this->container->get('whs.manager.user_preferences_manager')->createPreferencesForUser($user);

        $session = new UserSession($user);
        $this->em->persist($session);

        $this->em->flush();

        return $session;
    }

    /**
     * @param $token
     * @return User
     */
    public function setUserEmailConfirmation($token)
    {
        $user = $this->repository->findOneBy([
            'confirmEmailToken' => $token
        ]);

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Registration key is invalid');
        }

        return $this
            ->confirmEmail($user)
            ->saveUser($user, true);

    }

    public function confirmEmail(User $user)
    {
        //when confirm email, save it in user communication as primary
        $this->container->get('whs.manager.user_communication_manager')->addVerifiedEmail($user, $user->getUsername());
        $user
            ->setConfirmEmailToken(null)
            ->setActive(1);

        $event = new GenericEvent(null, ['user_id' => $user->getId()]);
        $this->container->get('event_dispatcher')->dispatch(AuthEvents::EMAIL_IS_CONFIRMED, $event);

        return $this;
    }

    public function updatePassword(User $userEntity, $password, $andFlush = true)
    {
        $userEntity
            ->setForgottenPasswordToken(null)
            ->setPassword($this->getEncoder($userEntity)->encodePassword($password, $userEntity->getSalt()));

        return $this->saveUser($userEntity, $andFlush);
    }

    /**
     * Returns user by username
     *
     * @param $username
     * @param bool $active
     * @return mixed
     */
    public function getUserByUsername($username, $active = true)
    {
        return $this->repository->findOneByUsername($username);
    }


    public function getUserByForgottenPasswordToken($token)
    {
        return $this->repository->findOneByForgottenPasswordToken($token);
    }

    /**
     * Generates for
     *
     * @param User $userEntity
     * @param bool $andFlush
     * @return User
     */
    public function updateForgotPasswordToken(User $userEntity, $andFlush = true)
    {
        $userEntity->generateForgotPasswordToken();

        return $this->saveUser($userEntity, $andFlush);
    }

    /**
     * Finds user by username (or generates an exception if no user is found).
     * Checks the entered password (generates exception if the password is not valid).
     * Creates new session for user.
     *
     * @param $username
     * @param $password
     * @return UserSession
     * @throws EntityNotFoundException | AuthenticationCredentialsNotFoundException
     */
    public function login($username, $password)
    {
        /**
         * @var User $user
         */
        $user = $this->repository->findOneByUsername($username);

        if (!$user) {
            throw new EntityNotFoundException();
        }

        if (!$this->encoderFactory->getEncoder($user)->isPasswordValid(
            $user->getPassword(),
            $password,
            $user->getSalt()
        )
        ) {
            throw new AuthenticationCredentialsNotFoundException();
        }

        /**
         * Creates new session on every login action
         */
        $session = new UserSession($user);
        $this->em->getRepository(UserSession::class)->save($session);

        return $session;
    }

    protected function getEncoder(UserInterface $user)
    {
        return $this->encoderFactory->getEncoder($user);
    }

    protected function saveUser(User $user, $andFlush = false)
    {
        $this->em->persist($user);

        if ($andFlush) {
            $this->em->flush();
        }

        return $user;
    }

    public function checkConfirmation(User $user)
    {
        return null === $user->getConfirmEmailToken() && $user->getActive();
    }

    public function resolveRoles(User $user)
    {
        return $this->roleResolver->resolveRoles($user);
    }

    /**
     * This method returns activity logs for user with offset(page number) and limit (amount of logs on the one page)
     *
     * @param User $user
     * @param $offset
     * @param $limit
     * @return mixed
     */
    public function getLogsForUser($user, $offset, $limit)
    {
        $dql = "SELECT log FROM LogBundle:UserActivityLog log WHERE log.user = :user ORDER BY log.id DESC ";

        $query = $this->em
            ->createQuery($dql)
            ->setParameter('user', $user);

        $paginator = $this->container->get('knp_paginator');
        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($query, $offset, $limit);

        return array(
            'logs'  => $pagination->getItems(),
            'count' => $pagination->getTotalItemCount()
        );
    }

    /**
     * @param string $searchString
     * @param string $orderField
     * @param string $orderDirection
     * @param int $limit
     * @param int $page
     *
     * @return User[]
     */
    public function searchPaginatedList($limit = 10, $page = 1, $orderField = null, $orderDirection = null, $searchString = null)
    {

        $qb = $this->repository->createQueryBuilder('user');
        $qb->addSelect('details');
        $qb->addSelect('address');
        $qb->addSelect('communications');
        $qb->addSelect('preferences');
        $qb2 = $this->em->createQueryBuilder();
        $qb3 = $this->em->createQueryBuilder();

        $qb
            ->leftJoin('user.details', 'details')
            ->leftJoin('user.preferences', 'preferences')
            ->leftJoin('user.addresses', 'address', 'WITH',
                $qb->expr()->in(
                    'address.moved',
                    $qb2->select('MAX(address_current.moved)')
                        ->from('AppBundle:UserAddress', 'address_current')
                        ->where('address_current.user = user')
                        ->getDQL()
                )
            )
            ->leftJoin('user.communications', 'communications', 'WITH',
                $qb->expr()->in(
                    'communications.communication',
                    $qb3->select('communication_type')
                        ->from('AppBundle:Communication', 'communication_type')
                        ->where('communication_type.type = ' . Communication::TYPE_MOBILE_PHONE)
                        ->getDQL()
                )
            );


        if (!empty($searchString)) {
            $searchArray = explode(' ', $searchString);
            for ($i = 0; $i < count($searchArray); $i++) {
                $qb->andWhere(
                    $qb->expr()->orX(
                        is_numeric($searchArray[$i]) ? $qb->expr()->eq('user.id', ':searchedId_' . $i) : $qb->expr()->like('user.username', ':searchString_' . $i),
                        $qb->expr()->like('user.username', ':searchString_' . $i),
                        $qb->expr()->like('details.firstName', ':searchString_' . $i),
                        $qb->expr()->like('details.lastName', ':searchString_' . $i),
                        $qb->expr()->like('details.screenName', ':searchString_' . $i),
                        $qb->expr()->like('address.city', ':searchString_' . $i),
                        $qb->expr()->like('address.postalCode', ':searchString_' . $i),
                        $qb->expr()->like('communications.value', ':searchString_' . $i)
                    )
                );
                if (is_numeric($searchArray[$i])) {
                    $qb->setParameter('searchedId_' . $i, $searchArray[$i]);
                }
                $qb->setParameter('searchString_' . $i, '%' . $searchArray[$i] . '%');
            }
        }

        //$orderField
        switch ($orderField) {
            case 'id' :
                $orderFieldInQuery = 'user.id';
                break;
            case 'first_name' :
                $orderFieldInQuery = 'details.firstName';
                break;
            case 'surname' :
                $orderFieldInQuery = 'details.lastName';
                break;
            case 'birth_date' :
                $orderFieldInQuery = 'details.birthdate';
                break;
            case 'age' :
                $orderFieldInQuery = 'details.birthdate';
                break;
            case 'post_code' :
                $orderFieldInQuery = 'address.postalCode';
                break;
            case 'mobile' :
                $orderFieldInQuery = 'communications.value';
                break;
            case 'city' :
                $orderFieldInQuery = 'address.city';
                break;
            case 'email' :
                $orderFieldInQuery = 'user.username';
                break;
            case 'screenname' :
                $orderFieldInQuery = 'details.screenName';
                break;
            default:
                $orderFieldInQuery = 'user.id';
        }

        //order direction
        if (!in_array(strtoupper($orderDirection), ['ASC', 'DESC'])) {
            $orderDirection = 'ASC';
        }

        $qb->orderBy($orderFieldInQuery, $orderDirection);
        $query = $qb->getQuery();

        $paginator = $this->container->get('knp_paginator');
        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($query, $page, $limit, ['distinct' => false]);
        /** @var User[] $users */
        $users = $pagination->getItems();

        $userItems = [];
        if (!empty($users) && is_array($users)) {
            foreach ($users as $user) {
                $userItems[] = [
                    "id"         => $user->getId(),
                    "first_name" => !empty($user->getDetails()) ? $user->getDetails()->getFirstName() : '-',
                    "surname"    => !empty($user->getDetails()) ? $user->getDetails()->getLastName() : '-',
                    "birth_date" => !empty($user->getDetails()) ? $user->getDetails()->getBirthdate()->format('Y-m-d') : '-',
                    "age"        => !empty($user->getDetails()) ? (new \DateTime())->diff($user->getDetails()->getBirthdate())->format('%y') : '-',
                    "post_code"  => isset($user->getAddresses()[0]) ? $user->getAddresses()[0]->getPostalCode() : '-',
                    "mobile"     => isset($user->getCommunications()[0]) ? $user->getCommunications()[0]->getValue() : '-',
                    "city"       => isset($user->getAddresses()[0]) ? $user->getAddresses()[0]->getCity() : '-',
                    "email"      => $user->getUsername(),
                    "screenname" => !empty($user->getDetails()) ? $user->getDetails()->getScreenName() : '-',
                ];
            }
        }
        return ['clients' => $userItems, "count" => $pagination->getTotalItemCount()];
    }

    /**
     * @param User $user
     * @return array
     */
    public function getUserPersonalInfo(User $user)
    {
        $result = [
            'user_id'        => $user->getId(),
            'email'          => $user->getUsername(),
            'first_name'     => !empty($user->getDetails()->getFirstName()) ? $user->getDetails()->getFirstName() : '-',
            'surname'        => !empty($user->getDetails()->getLastName()) ? $user->getDetails()->getLastName() : '-',
            'gender'         => !empty($user->getDetails()->getGender()) ? $user->getDetails()->getGender() : '-',
            'martial_status' => !empty($user->getDetails()->getMartialStatus()) ? $user->getDetails()->getMartialStatus() : '-',
            'partner_label'  => '-',//TODO: return this value
            'middle_name'    => !empty($user->getDetails()->getMiddleName()) ? $user->getDetails()->getMiddleName() : '-',
            'maiden_name'    => !empty($user->getDetails()->getMaidenName()) ? $user->getDetails()->getMaidenName() : '-',
            'title'          => !empty($user->getDetails()->getTitle()) ? $user->getDetails()->getTitle() : '-',
            'birth_date'     => !empty($user->getDetails()->getBirthdate()) ? $user->getDetails()->getBirthdate() : '-',
            'dependents'     => '-',
        ];
        if (!$user->getPreferences()->getHasNoDependencies() && !empty($user->getDependents()) && is_array($user->getDependents()->toArray())) {
            $result['dependents'] = $user->getDependents();
        }

        if ($user->getCommunications()->count() > 0) {
            $result['communications'] = [];
            /** @var UserCommunication $communication */
            foreach ($user->getCommunications() as $communication) {
                $result['communications'][] = ['type' => $communication->getCommunication()->getType(), 'value' => $communication->getValue()];
            }
        }

        return $result;
    }
}