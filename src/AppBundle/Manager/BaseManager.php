<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 1.12.16
 * Time: 8.50
 */

namespace AppBundle\Manager;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

abstract class BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var EntityRepository
     */
    protected $repository;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function save($entity, $andFlush = false)
    {
        $this->em->persist($entity);
        if ($andFlush) {
            $this->em->flush();
        }

        return $entity;
    }
}