<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 18.06.2018
 * Time: 15:37
 */

namespace AppBundle\Manager;


use AppBundle\Entity\Agreement;
use AppBundle\Entity\Instalment;
use AppBundle\Entity\LoanApply;
use AppBundle\Repository\InstalmentRepository;
use AppBundle\Utils\Payment\PaymentsComputerService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\OrderBy;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class InstalmentManager extends BaseManager
{
    use ContainerAwareTrait;

    /**
     * @var InstalmentRepository
     */
    protected $repository;

    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->repository = $this->em->getRepository(Instalment::class);
    }

    /**
     * @param Agreement $agreement
     * @return Instalment
     */
    public function createInstalment(Agreement $agreement)
    {
        //previous not paid instalments will marked as missed
        $loanApply = $agreement->getLoanApply();
        $instalments = $this->em->getRepository(Instalment::class)->getNotPaidInstalmentsForLoan($loanApply);
        foreach ($instalments as $inst) {
            $this->markInstalmentAsMissed($inst);
        }

        $paymentComputer = $this->container->get('app.utils.payments_computer');

        $interest = $agreement->getBalance() * $agreement->getApr() / 1200;
        $principle = $agreement->getMonthlyRepayment() - $interest;
        /** @var Instalment $prevInstalment */
        $prevInstalment = $this->em->getRepository(Instalment::class)->findOneBy(['loanApply' => $loanApply], ['period' => 'DESC']);

        if (!empty($prevInstalment)) {
            $period = $prevInstalment->getPeriod() + 1;
        } else {
            $period = $paymentComputer->getCurrentPeriod($agreement);
        }

        $instalment = new Instalment();
        $loanApply = $agreement->getLoanApply();

        $instalment
            ->setLoanApply($loanApply)
            ->setPeriod($period)
            ->setPaymentDate($paymentComputer->getPaymentDate($agreement, $period))
            ->setInterest($interest)
            ->setPrinciple($principle)
            ->setBalance($agreement->getBalance() - $principle)
            ->setPaymentStatus(Instalment::PAYMENT_STATUS_PENDING)
            ->setOutstanding($agreement->getMonthlyRepayment())
            ->setAllocation(0)
            ->setInstallmentStatus(Instalment::INSTALLMENT_STATUS_OUTSTANDING)
            ->setPaymentAmount(0)
        ;

        if ($period <= 1) {
            $instalment->setInstalment($agreement->getFirstMonthlyPayment());
        } else {
            $instalment->setInstalment($agreement->getMonthlyRepayment());
        }

        $this->em->persist($instalment);
        $this->em->flush();

        $this->container->get('whadmin.manager.admin_instalment')->createInstalmentTransactions($agreement);

        return $instalment;
    }

    /**
     * @param Instalment $instalment
     */
    public function markInstalmentAsMissed(Instalment $instalment)
    {
        $instalment->setPaymentStatus(Instalment::PAYMENT_STATUS_MISSED);
        $this->em->persist($instalment);

        $loanApply = $instalment->getLoanApply();
        $agreement = $this->em->getRepository(Agreement::class)->findOneBy(['loanApply' => $loanApply]);

        $agreement->setBalance($agreement->getBalance() + (float)$instalment->getInterest());
        $this->em->persist($agreement);

        $this->em->flush();
    }

    /**
     * @param LoanApply $loanApply
     * @param $array
     * @return mixedInterest
     */
    public function generateFutureInstalments(LoanApply $loanApply, $array = [], $percent = 1)
    {
        /**
         * @var Agreement $agreement
         */
        $agreement = $this->em->getRepository(Agreement::class)->findOneBy(['loanApply' => $loanApply]);

        $loanBalanceAmount = $agreement->getBalance();
        /** @var PaymentsComputerService $paymentsComputer */
        $paymentsComputer = $this->container->get('app.utils.payments_computer');

        foreach ($array as &$arr) {
            $arr['paymentStatusText'] = Instalment::getPaymentStatusMap()[$arr['paymentStatus']];
            $arr['installmentStatusText'] = Instalment::getInstallmentStatusMap()[$arr['installmentStatus']];
        }

        $period = (isset($arr) && isset($arr['period'])) ? ($arr['period'] + 1) : 1;

        if (isset($arr) && isset($arr['paymentStatus'])
            && ($arr['paymentStatus'] == Instalment::PAYMENT_STATUS_PENDING || $arr['paymentStatus'] == Instalment::PAYMENT_STATUS_LATE)) {

            $loanBalanceAmount = $agreement->getBalance() -  $arr['principle'];
        }


        while ($loanBalanceAmount > 0) {

            $interest = $loanBalanceAmount * $agreement->getApr() / 100 / 12;

            if ($agreement->getMonthlyRepayment() <= $loanBalanceAmount + $interest) {
                $instalment = $agreement->getMonthlyRepayment();
            } else {
                $instalment = $loanBalanceAmount + $interest;
            }

            $principle = $instalment - $interest;
            $balance = $loanBalanceAmount - $principle;

            $instVal = $period <= 1 ? $agreement->getFirstMonthlyPayment() * $percent : $instalment * $percent;

            $paymentDay = $agreement->getPaymentDay();
            $currentDay = date('j');

            $row = [
                'instalment_id' => '',
                'loan_apply_id' => $loanApply->getId(),
                'period' => $period,
                'payment_date' => $paymentsComputer->getPaymentDate($agreement, $period),
                'instalment' => $instVal,
                'interest' => round($interest * $percent, 2),
                'principle' => round($principle * $percent, 2),
                'balance' => round($balance * $percent, 2),
                'created_at' => '',
                'paymentStatus' => Instalment::PAYMENT_STATUS_PROJECTED,
                'paymentStatusText' => Instalment::getPaymentStatusMap()[Instalment::PAYMENT_STATUS_PROJECTED],
                'outstanding' => round($instalment * $percent, 2),
                'allocation' => 0,
                'installmentStatus' => Instalment::INSTALLMENT_STATUS_OUTSTANDING,
                'installmentStatusText' => Instalment::getInstallmentStatusMap()[Instalment::INSTALLMENT_STATUS_OUTSTANDING],
                'paymentAmount' => 0,
            ];
            $period++;

            array_push($array, $row);

            $loanBalanceAmount = $loanBalanceAmount - $principle;
        }

        return $array;
    }
}