<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Product;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\User;
use AppBundle\Exception\QuoteApplyNotFoundException;
use AppBundle\Exception\QuoteCreationException;
use Doctrine\ORM\EntityManager;


class QuoteApplyManager extends BaseManager
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param QuoteApply $quoteApply
     * @return QuoteApply
     */
    public function addQuote(QuoteApply $quoteApply)
    {
        $this->em->persist($quoteApply);
        $this->em->flush();

        return $quoteApply;
    }

    /**
     * @param User $user
     * @return QuoteApply[]
     */
    public function getQuoteAppliesForLogin(User $user)
    {
        $qa = $this->em->getRepository('AppBundle:QuoteApply')->getActiveQuoteApplyForUser($user, QuoteApply::getAvailableStatuses());

        if (!$qa) {
            throw new QuoteApplyNotFoundException(QuoteApplyNotFoundException::QUOTE_APPLY_NOT_FOUND);
        }

        return $qa;
    }

    /**
     * @param User $user
     * @param mixed $type
     * @return QuoteApply[]
     */
    public function getQuoteApplies(User $user, $type = null)
    {
        $qa = $this->em->getRepository('AppBundle:QuoteApply')->getQuoteAppliesForUser($user, QuoteApply::getAvailableStatuses(), $type);

        return $qa;
    }

    /**
     * @param User $user
     * @return QuoteApply[]
     */
    public function getApprovedQuoteAppliesForLogin(User $user)
    {
        $qa = $this->em->getRepository('AppBundle:QuoteApply')->getQuoteAppliesForUser($user, [QuoteApply::STATUS_APPROVED]);

        return $qa;
    }

    /**
     * @deprecated Use getDraftQuoteApply()
     * @param User $user
     * @return QuoteApply
     */
    public function getQuoteApply(User $user)
    {
        return $this->getDraftQuoteApply($user);
    }

    public function getDraftQuoteApply(User $user)
    {
        $result = $this->em->getRepository('AppBundle:QuoteApply')->getActiveQuoteApplyForUser($user);

        if ($result) {
            $result = $result[0];
        }

        return $result;
    }

    /**
     * @param User $user
     * @return QuoteApply|null
     */
    public function getDraftLenderQuoteApply(User $user)
    {
        /* Only one draft quote should be for lender (investor) */
        $qa = $this->em->getRepository('AppBundle:QuoteApply')->getQuoteAppliesForUser($user, [QuoteApply::STATUS_DRAFT], Product::TYPE_INVESTOR);

        return $qa ? $qa[0] : null;
    }

    /**
     * @param User $user
     * @return QuoteApply|null
     */
    public function getDraftBorrowerQuoteApply(User $user)
    {
        /* Only one draft quote should be for a borrower */
        $qa = $this->em->getRepository('AppBundle:QuoteApply')->getQuoteAppliesForUser($user, [QuoteApply::STATUS_DRAFT], Product::TYPE_BORROWER);

        return $qa ? $qa[0] : null;
    }

    /**
     * @param User $user
     * @return QuoteApply | null
     */
    public function hasLenderApprovedQuote(User $user)
    {
        $qa = $this->em->getRepository(QuoteApply::class)->getQuoteAppliesForUser($user, [QuoteApply::STATUS_APPROVED], Product::TYPE_INVESTOR);

        return $qa ? $qa[0] : null;
    }

    public function hasLenderActiveQuote(User $user)
    {
        $qa = $this->em->getRepository(QuoteApply::class)->getQuoteAppliesForUser($user, [QuoteApply::STATUS_ACTIVE], Product::TYPE_INVESTOR);

        return $qa ? $qa[0] : null;
    }

    /**
     * @param User $user
     * @return null
     */
    public function hasLenderRejectedQuote(User $user)
    {
        $qa = $this->em->getRepository(QuoteApply::class)->getQuoteAppliesForUser($user, [QuoteApply::STATUS_REJECTED], Product::TYPE_INVESTOR);

        return $qa ? $qa[0] : null;
    }

    /**
     * @param User $user
     * @return null|QuoteApply
     */
    public function getFundingRequiredLenderQuoteApply(User $user)
    {
        /* Only one funding_required quote should be for lender (investor) */
        $qa = $this->em->getRepository(QuoteApply::class)->getQuoteAppliesForUser($user, [QuoteApply::STATUS_FUNDING_REQUIRED], Product::TYPE_INVESTOR);

        return $qa ? $qa[0] : null;
    }

    /**
     * @param User $user
     * @return null|QuoteApply
     */
    public function getPendingFundsLenderQuoteApply(User $user)
    {
        /* Only one pending_funds quote should be for lender (investor) */
        $qa = $this->em->getRepository(QuoteApply::class)->getQuoteAppliesForUser($user, [QuoteApply::STATUS_PENDING_FUNDS], Product::TYPE_INVESTOR);

        return $qa ? $qa[0] : null;
    }

    /**
     * @param User $user
     * @return \AppBundle\Entity\QuoteApply[]
     */
    public function getNotActiveUserQuoteApply(User $user)
    {
        $statuses = QuoteApply::getAvailableStatuses();
        $key = array_search(QuoteApply::STATUS_ACTIVE, $statuses, true);
        if (false !== $key) {
            unset($statuses[$key]);
        }
        $qa = $this->em->getRepository(QuoteApply::class)->getQuoteAppliesForUser($user, $statuses);

        return $qa;
    }

    /**
     * @param User $user
     * @param Product $product
     * @return QuoteApply
     * @throws \Exception
     */
    public function createQuoteForUser(User $user, Product $product)
    {
        $draftedQuoteApply = $this->em->getRepository('AppBundle:QuoteApply')->getQuoteAppliesForUser($user, [QuoteApply::STATUS_DRAFT]);
        if (!empty($draftedQuoteApply)) {
            throw new QuoteCreationException();
        }

        $quoteApply = new QuoteApply();

        $quoteApply
            ->setUser($user)
            ->setProduct($product)
            ->setStatus(QuoteApply::STATUS_DRAFT)
        ;

        $this->save($quoteApply);

        return $quoteApply;
    }
}