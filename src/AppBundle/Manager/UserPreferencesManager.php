<?php
/**
 * Created by PhpStorm.
 * User: mikhailpegasin
 * Date: 17.11.16
 * Time: 17:51
 */

namespace AppBundle\Manager;


use AppBundle\Entity\User;
use AppBundle\Entity\UserPreferences;
use AppBundle\Manager\ActiveManagers\NormalUpdate;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class UserPreferencesManager extends NormalUpdate
{
    /**
     * @param User $user
     * @param array $preferences
     *
     * @return UserPreferences
     */
    public function updateUserPreferences(
        User $user,
        array $preferences = []
    )
    {
        if (!$userPreference = $this->em->getRepository('AppBundle:UserPreferences')->findOneBy(['user' => $user])) {
            $userPreference = $this->createPreferencesForUser($user);
        };
        if ($preferences) {
            $refl = new \ReflectionClass($userPreference);
            foreach ($preferences as $key => $value) {
                $methodName = 'set' . ucfirst($key);
                if ($refl->hasMethod($methodName)) {
                    if (!is_null($value)) {
                        $userPreference->$methodName($value);
                    }
                } else {
                    throw new NotFoundHttpException('Method ' . $methodName . ' is absent in ' . __CLASS__);
                }
            }
        }

        $this->em->flush();

        return $userPreference;
    }

    public function createPreferencesForUser(User $user, $andFlash = false)
    {
        $userPreference = new UserPreferences();
        $userPreference->setUser($user);

        $this->em->persist($userPreference);

        if ($andFlash) {
            $this->em->flush();
        }

        return $userPreference;
    }

    /**
     * @param User $user
     * @return UserPreferences|null|object
     */
    public function getPreferencesForUser(User $user) {
        $userPreferences = $this->em->getRepository(UserPreferences::class)->findOneBy([
            'user' => $user
        ]);

        return $userPreferences;
    }
}