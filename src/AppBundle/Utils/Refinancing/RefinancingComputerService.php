<?php
/**
 * User: Vladimir
 * Date: 12/28/16
 * Time: 3:01 PM
 */

namespace AppBundle\Utils\Refinancing;


use AppBundle\Entity\UserCreditline;

class RefinancingComputerService
{

    /**
     * @var UserCreditline
     */
    private $userCreditline;

    public function setUserCreditline(UserCreditline $userCreditline)
    {
        $this->userCreditline = $userCreditline;
        return $this;
    }

    public function addCalculatedProperties()
    {
        $calculatedValues = $this->calculateActualValues();

        $this->userCreditline->creditBalanceNow = round($calculatedValues['creditBalanceNow']);
        $this->userCreditline->totalRepaid = round($calculatedValues['totalRepaid']);
        $this->userCreditline->totalInterest = round($calculatedValues['totalInterest']);
        $this->userCreditline->totalMonthsToClear = $calculatedValues['monthToClear'];
    }

    /**
     * @return int
     */
    public function getTotalBalanceNow()
    {
        $this->addCalculatedProperties();
        return $this->userCreditline->creditBalanceNow;
    }

    private function calculateActualValues()
    {

        $paymentsTable = [0 => ['interestRate' => $this->userCreditline->getApr(), 'balance' => $this->userCreditline->getBalanceInput()]];

        $monthlyInterestRate = $this->userCreditline->getApr() / 12 / 100;
        $monthlyPayment = $this->userCreditline->getMonthlyPayment();

        for ($i = 1; ; $i++) {
            $currentBalance = $paymentsTable[$i - 1]["balance"];
            $monthlyInterest = $currentBalance * $monthlyInterestRate;
            $monthlyRepay = $monthlyPayment - $monthlyInterest;
            $balance = $paymentsTable[$i - 1]["balance"] - $monthlyRepay;
            if ($balance < 0) {
                $lastMonthPayment = $monthlyPayment + $balance;
                $lastMonthInterest = $monthlyInterest;
                $lastMonthRepay = $lastMonthPayment - $lastMonthInterest;

                $paymentsTable[$i] = ["installment" => $lastMonthPayment, "interest" => $lastMonthInterest, "principle" => $lastMonthRepay, "balance" => 0];
                break;
            }
            $paymentsTable[$i] = ["installment" => $monthlyPayment, "interest" => $monthlyInterest, "principle" => $monthlyRepay, "balance" => $balance];

            // If something wrong with data and we can't calculate how to repay credit in 100 years (1200 months),
            // than throw exception and stop looping
            if ($i > 100 * 12) {
                //TODO: remove this temp code
                return ['monthToClear' => "can't be calculated in 100 years", 'totalRepaid' => "can't be calculated in 100 years", 'totalInterest' => "can't be calculated in 100 years", 'creditBalanceNow' => "can't be calculated in 100 years"];
//                throw new \Exception("Impossible loan conditions, can't calculate how to repay it in 100 years (1200 month)");
            }
        }

        $currentDate = new \DateTime();

        $dateCreated = $this->userCreditline->getCreatedAt();
        $dateInterval = $currentDate->diff($dateCreated);
        $monthGone = $dateInterval->format('%m') + $dateInterval->format('%y') * 12;

        $totalRepay = 0;
        $totalInterest = 0;
        $creditBalanceNow = 0;

        $monthToClear = $i - $monthGone;
        if ($monthToClear > 0) {
            for ($j = 1 + $monthGone; $j < count($paymentsTable); $j++) {
                $totalRepay += $paymentsTable[$j]['installment'];
                $totalInterest += $paymentsTable[$j]['interest'];
            }
            $creditBalanceNow = $paymentsTable[$monthGone]['balance'];
        }

        $monthToClear = $monthToClear > 0 ? $monthToClear : 0;

        return ['monthToClear' => $monthToClear, 'totalRepaid' => $totalRepay, 'totalInterest' => $totalInterest, 'creditBalanceNow' => $creditBalanceNow];

    }
}