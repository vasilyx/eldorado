<?php
/**
 * User: Vladimir
 * Date: 12/15/16
 * Time: 12:10 PM
 */

namespace AppBundle\Utils\Payment;


use AppBundle\Entity\Agreement;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints\DateTime;

class PaymentsComputerService
{
    const MIN_LOAN_AMOUNT = 1000;
    const MAX_LOAN_AMOUNT = 30000;

    //months
    const MIN_LOAN_TERM = 12;
    const MAX_LOAN_TERM = 60;

    const MIN_INTEREST_RATE = 0.1;
    const MAX_INTEREST_RATE = 99.9;

    private $contractInterestRates;
    private $bonusInterestRates;
    private $effectiveInterestRates;
    private $brokerInterestRates;
    private $investorInterestRates;

    /**
     * PaymentsComputerService constructor.
     * @param InterestRatesProviderInterface $interestRatesProvider
     */
    public function __construct(InterestRatesProviderInterface $interestRatesProvider)
    {
        $this->contractInterestRates = $interestRatesProvider->getContractInterestRates();
        $this->bonusInterestRates = $interestRatesProvider->getBonusInterestRates();
        $this->effectiveInterestRates = $interestRatesProvider->getEffectiveInterestRates();
        $this->brokerInterestRates = $interestRatesProvider->getBrokerInterestRates();
        $this->investorInterestRates = $interestRatesProvider->getInvestorInterestRates();
    }

    /**
     * @param float $interestRate -- interest rate in percentage
     * @param integer $loanTerm -- loan term in months
     * @param integer $loanAmount
     *
     * @return float $monthlyPayment
     */
    private function calcPMT($interestRate, $loanTerm, $loanAmount)
    {
        $monthlyInterest = $interestRate / (12 * 100);//12 - number of month, 100 - for conversion in proportions form percentage
        $monthlyPayment = (-1) * \PHPExcel_Calculation_Financial::PMT($monthlyInterest, $loanTerm, $loanAmount);

        return $monthlyPayment;
    }

    /**
     * @param integer $loanTerm -- loan term in months
     * @param integer $loanAmount
     * @param string $rateType
     *
     * @return float $interestRate
     * @throws \Exception
     */
    public function getDefaultInterestRate($loanTerm, $loanAmount, $rateType)
    {
        switch ($rateType) {
            case InterestRatesProviderInterface::RATE_TYPE_BONUS :
                $interestRatesArray = $this->bonusInterestRates;
                break;
            case InterestRatesProviderInterface::RATE_TYPE_BROKER :
                $interestRatesArray = $this->brokerInterestRates;
                break;
            case InterestRatesProviderInterface::RATE_TYPE_CONTRACT :
                $interestRatesArray = $this->contractInterestRates;
                break;
            case InterestRatesProviderInterface::RATE_TYPE_EFFECTIVE :
                $interestRatesArray = $this->effectiveInterestRates;
                break;
            case InterestRatesProviderInterface::RATE_TYPE_INVESTOR :
                $interestRatesArray = $this->investorInterestRates;
                break;
            default:
                throw new BadRequestHttpException('Invalid interest rate type');
        }

        $loanAmountStripesKeys = array_keys($interestRatesArray);
        foreach ($loanAmountStripesKeys as $loanAmountStripeKey => $loanAmountStripe) {
            if ($loanAmount < $loanAmountStripe) {
                break;
            } else {
                $loanAmountKey = $loanAmountStripe;
            }
        }

        if (!isset($loanAmountKey)) {
            throw new BadRequestHttpException('Invalid loan amount');
        }

        $loanTermStripesKeys = array_keys($interestRatesArray[0]);
        foreach ($loanTermStripesKeys as $loanTermStripeKey => $loanTermStripe) {
            if ($loanTerm < $loanTermStripe) {
                break;
            } else {
                $loanTermKey = $loanTermStripe;
            }
        }
        if (!isset($loanTermKey)) {
            throw new BadRequestHttpException('Invalid loan term');
        }

        $interestRate = $interestRatesArray[$loanAmountKey][$loanTermKey];
        return $interestRate;
    }

    /**
     * @param $loanTerm
     * @return bool
     *
     * get $loanTerm by reference for casting it to integer
     */
    private function validateAndSanitizeLoanTerm(&$loanTerm)
    {
        if (!is_numeric($loanTerm) || $loanTerm != (int)$loanTerm) {
            return false;
        }
        $loanTerm = (int)$loanTerm;

        if ($loanTerm < $this::MIN_LOAN_TERM || $loanTerm > $this::MAX_LOAN_TERM) {
            return false;
        }
        return true;
    }

    private function validateInterestRate($interestRate)
    {
        if (!is_numeric($interestRate)) {
            return false;
        }

        if ($interestRate < $this::MIN_INTEREST_RATE || $interestRate > $this::MAX_INTEREST_RATE) {
            return false;
        }
        return true;
    }

    /**
     * @param $loanAmount
     * @return bool
     *
     * get $loanAmount by reference for casting it to integer
     */
    private function validateAndSanitizeLoanAmount(&$loanAmount)
    {
        if (!is_numeric($loanAmount) || $loanAmount != (int)$loanAmount) {
            return false;
        }
        $loanAmount = (int)$loanAmount;

        if (/*$loanAmount < $this::MIN_LOAN_AMOUNT || */$loanAmount > $this::MAX_LOAN_AMOUNT) {
            return false;
        }
        return true;
    }

    /**
     * @param integer $loanTerm
     * @param integer $loanAmount
     * @return array
     */
    private function calculatePaymentsTable($loanTerm, $loanAmount)
    {
        $interestRate = $this->getDefaultInterestRate($loanTerm, $loanAmount, InterestRatesProviderInterface::RATE_TYPE_EFFECTIVE);
        $contractRate = $this->getDefaultInterestRate($loanTerm, $loanAmount, InterestRatesProviderInterface::RATE_TYPE_CONTRACT);

        $monthlyPayment = $this->calcPMT($interestRate, $loanTerm, $loanAmount);

        $paymentsArr = $this->buildPaymentsMatrix($interestRate, $loanTerm, $monthlyPayment, $loanAmount);
        $totalPayment = $loanTerm * $monthlyPayment;

        $baseProperties = ["totalPayment" => $totalPayment, "balance" => $loanAmount, "interestRate" => $interestRate, 'contractRate' => $contractRate];
        array_walk_recursive($baseProperties, [$this, 'round8']);
        $paymentsArr["baseProperties"] = $baseProperties;

        return $paymentsArr;
    }

    /**
     * @param float $interestRate
     * @param integer $loanTerm
     * @param float $monthlyPayment
     * @param integer $loanAmount
     * @return array
     */
    private function buildPaymentsMatrix($interestRate, $loanTerm, $monthlyPayment, $loanAmount)
    {
        $paymentsArr = [];
        for ($i = 1; $i <= $loanTerm; $i++) {
            if (array_key_exists($i - 1, $paymentsArr) && array_key_exists("balance", $paymentsArr[$i - 1])) {
                $currentBalance = $paymentsArr[$i - 1]["balance"];
            } else {
                $currentBalance = $loanAmount;
            }
            $interest = $currentBalance * ($interestRate / 100 / 12);
            $principle = $monthlyPayment - $interest;
            $balance = $currentBalance - $principle;
            $paymentsArr[$i] = ["monthlyPayment" => $monthlyPayment, "interest" => $interest, "principle" => $principle, "balance" => $balance];
        }

        array_walk_recursive($paymentsArr, [$this, 'round8']);
        return $paymentsArr;
    }

    /**
     * Function only for debugging, print calculated payments table in csv file
     *
     * @param $arr
     */
    private function printToCsv($arr)
    {
        $csv = '';
        foreach ($arr as $item) {
            $csv .= implode(',', $item);
            $csv .= "\n";
        }
        file_put_contents('payment_table' . '.csv', $csv);
    }

    /**
     * function for rounding values in payment tables of this class
     *
     * @param $value
     */
    private function round8(&$value)
    {
        $value = abs(round($value, 8));
    }

    /**
     * @param integer $loanTerm
     * @param integer $loanAmount
     * @return array
     * @throws BadRequestHttpException
     */
    public function getBaseLoanProperties($loanTerm, $loanAmount)
    {
        if (!$this->validateAndSanitizeLoanTerm($loanTerm)) {
            throw new BadRequestHttpException("Invalid Loan Term");
        }
        if (!$this->validateAndSanitizeLoanAmount($loanAmount)) {
            throw new BadRequestHttpException("Invalid Loan Amount");
        }
        $paymentTable = $this->calculatePaymentsTable($loanTerm, $loanAmount);
        return [
            "interest_rate"   => $paymentTable["baseProperties"]["interestRate"],
            "monthly_payment" => round($paymentTable[1]["monthlyPayment"], 2),
            "total_payment"   => round($paymentTable["baseProperties"]["totalPayment"], 2),
            'contract_rate'   => $paymentTable['baseProperties']['contractRate']
        ];
    }

    /**
     * @param float $interestRate
     * @param integer $loanTerm
     * @param integer $loanAmount
     * @return array
     */
    public function getCreditPaymentsMatrix($interestRate, $loanTerm, $loanAmount)
    {
        if (!$this->validateAndSanitizeLoanTerm($loanTerm)) {
            throw new BadRequestHttpException("Invalid Loan Term");
        }
        if (!$this->validateAndSanitizeLoanAmount($loanAmount)) {
            throw new BadRequestHttpException("Invalid Loan Amount");
        }
        if (!$this->validateInterestRate($interestRate)) {
            throw new BadRequestHttpException("Invalid Interest Rate");
        }
        $monthlyPayment = $this->calcPMT($interestRate, $loanTerm, $loanAmount);
        $paymentMatrix = $this->buildPaymentsMatrix($interestRate, $loanTerm, $monthlyPayment, $loanAmount);
        return $paymentMatrix;
    }

    /**
     * @param float $interestRate
     * @param integer $loanTerm
     * @param integer $loanAmount
     * @return array
     */
    public function getCreditPaymentsTotal($interestRate, $loanTerm, $loanAmount)
    {
        if (!$this->validateAndSanitizeLoanTerm($loanTerm)) {
            throw new BadRequestHttpException("Invalid Loan Term");
        }
        if (!$this->validateAndSanitizeLoanAmount($loanAmount)) {
            throw new BadRequestHttpException("Invalid Loan Amount");
        }
        if (!$this->validateInterestRate($interestRate)) {
            throw new BadRequestHttpException("Invalid Interest Rate");
        }
        $paymentMatrix = $this->getCreditPaymentsMatrix($interestRate, $loanTerm, $loanAmount);
        $totalInterest = 0;
        foreach ($paymentMatrix as $periodRow) {
            $totalInterest += $periodRow['interest'];
        }
        $totalPaid = $loanAmount + $totalInterest;
        $monthlyPayment = round($paymentMatrix[1]['monthlyPayment'], 2);

        $firstMonthly = $monthlyPayment + ($totalPaid - $monthlyPayment * $loanTerm);

        return ['monthlyPayment' => $monthlyPayment, 'totalPaid' => $totalPaid, 'totalInterest' => $totalInterest, 'firstMonthly' => $firstMonthly];
    }

    /**
     *
     * Get payment date by period and agreement
     *
     * @param Agreement $agreement
     * @param $period
     * @return \DateTime
     */
    public function getPaymentDate(Agreement $agreement, $period)
    {
        $acceptanceDate = new \DateTime($agreement->getAcceptanceDate()->format('d-M-Y'));

        if (!$acceptanceDate) {
            return new \DateTime();
        }

        $paymentDay = $agreement->getPaymentDay();
        $months = $period - 1;
        $acceptanceDate->modify("+$months months");
        $acceptanceDate->modify('first day of next month');
        $paymentDay -= 1;
        $acceptanceDate->modify("+$paymentDay days");

        return $acceptanceDate;
    }

    /**
     *
     * Get current payment period
     *
     * @param Agreement $agreement
     * @return int|string
     */
    public function getCurrentPeriod(Agreement $agreement)
    {
        $acceptanceDate = new \DateTime($agreement->getAcceptanceDate()->format('d-M-Y'));

        if (!$acceptanceDate) {
            return 1;
        }

        $firstPaymentDay = $acceptanceDate->modify('first day of next month');
        $paymentDay = $agreement->getPaymentDay() - 1;
        $firstPaymentDay->modify("+$paymentDay days");

        $currentDate = new \DateTime();

        if ($firstPaymentDay->getTimestamp() > $currentDate->getTimestamp()) {
            return 1;
        }

        $diff = $currentDate->diff($firstPaymentDay);
        //+2 because first payment has been passed
        $months = ($diff->format('%y') * 12) + $diff->format('%m') + 2;

        return $months;
    }


    /**
     * @return int
     */
    public function getMinLoanTerm()
    {
        return $this::MIN_LOAN_TERM;
    }

    /**
     * @return int
     */
    public function getMaxLoanTerm()
    {
        return $this::MAX_LOAN_TERM;
    }
}