<?php
/**
 * User: Vladimir
 * Date: 12/16/16
 * Time: 2:39 PM
 */

namespace AppBundle\Utils\Payment;

interface InterestRatesProviderInterface
{
    const RATE_TYPE_CONTRACT = 10;
    const RATE_TYPE_BONUS = 20;
    const RATE_TYPE_EFFECTIVE = 30;
    const RATE_TYPE_BROKER = 40;
    const RATE_TYPE_INVESTOR = 50;

    /**
     * @return mixed
     *
     * ----------------------------------
     * have to return an array like below
     *   [
     *   0     => [12 => 3.4, 18 => 3.9, 24 => 4.2, 36 => 4.7, 48 => 5.2, 60 => 5.7],
     *   3000  => [12 => 3.4, 18 => 3.9, 24 => 4.2, 36 => 4.7, 48 => 5.2, 60 => 5.7],
     *   5000  => [12 => 3.4, 18 => 3.9, 24 => 4.2, 36 => 4.7, 48 => 5.2, 60 => 5.7],
     *   7500  => [12 => 3.4, 18 => 3.9, 24 => 4.2, 36 => 4.7, 48 => 5.2, 60 => 5.7],
     *   10000 => [12 => 3.4, 18 => 3.9, 24 => 4.2, 36 => 4.7, 48 => 5.2, 60 => 5.7],
     *   15000 => [12 => 3.4, 18 => 3.9, 24 => 4.2, 36 => 4.7, 48 => 5.2, 60 => 5.7],
     *   20000 => [12 => 3.4, 18 => 3.9, 24 => 4.2, 36 => 4.7, 48 => 5.2, 60 => 5.7],
     *   ];
     *
     * Rules:
     *
     * [
     * amount_step_1 => [ amount_term_1 => interest_rate_1_1, amount_term_2 => interest_rate_1_2, ..., amount_term_n => interest_rate_1_n]
     * amount_step_2 => [ amount_term_1 => interest_rate_2_1, amount_term_2 => interest_rate_2_2, ..., amount_term_n => interest_rate_2_n]
     * ...
     * amount_step_m => [ amount_term_1 => interest_rate_m_1, amount_term_2 => interest_rate_m_2, ..., amount_term_n => interest_rate_m_n]
     * ]
     */
    public function getContractInterestRates();

    public function getBonusInterestRates();

    public function getEffectiveInterestRates();

    public function getBrokerInterestRates();

    public function getInvestorInterestRates();
}