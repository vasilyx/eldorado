<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 08.12.16
 * Time: 17:40
 */

namespace AppBundle\Utils;
use AppBundle\Entity\QuoteApply;


/**
 * Class QuoteStatusRandomGenerator
 * @package AppBundle\Utils
 */
class QuoteStatusRandomGenerator
{
    /**
     * @param int $approvedPercent
     * @param int $rejectPercent
     * @return int|mixed|string
     */
    public function getStatus($approvedPercent = 60, $rejectPercent = 40)
    {
        $statuses = [
            QuoteApply::STATUS_APPROVED => $approvedPercent,
            QuoteApply::STATUS_REJECTED => $rejectPercent
        ];

        $newStatuses = [];
        foreach ($statuses as $status=>$percent)
        {
            $newStatuses = array_merge($newStatuses, array_fill(0, $percent, $status));
        }

        $status = $newStatuses[array_rand($newStatuses)];

        return $status;
    }
}