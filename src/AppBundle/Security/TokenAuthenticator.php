<?php

namespace AppBundle\Security;

use AppBundle\Entity\User;
use AppBundle\Interfaces\RoleResolverInterface;
use AppBundle\Manager\UserManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Security\Core\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class TokenAuthenticator implements SimplePreAuthenticatorInterface
{
    const TOKEN_HEADER  = 'Auth';
    const AUTH_KEY      = 'whs';

    protected $userProvider;
    protected $userManager;

    public function __construct(TokenUserProvider $userProvider, UserManager $userManager)
    {
        $this->userProvider = $userProvider;
        $this->userManager = $userManager;
    }

    public function createToken(Request $request, $providerKey)
    {
        if (!$request->headers->get(self::TOKEN_HEADER) || 0 !== stripos($request->headers->get(self::TOKEN_HEADER), self::AUTH_KEY)) {
            throw new BadCredentialsException('No API token found');
        }

        $token = trim(substr($request->headers->get(self::TOKEN_HEADER), strlen(self::AUTH_KEY)));
        return new PreAuthenticatedToken('anon.', $token, $providerKey);
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $apiToken = $token->getCredentials();

        if ($user = $this->userProvider->loadUserByToken($apiToken)) {
            $user->setActiveApiToken($apiToken);
            $user->setRoles($this->resolveRoles($user));

            return new PreAuthenticatedToken($user, $apiToken, $providerKey, $user->getRoles());
        }

        throw new AuthenticationException('Wrong API token or token has expired');
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    public function resolveRoles(User $user)
    {
        return $this->userManager->resolveRoles($user);
    }
}