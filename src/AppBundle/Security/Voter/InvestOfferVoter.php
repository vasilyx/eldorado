<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 13.12.16
 * Time: 9:00
 */

namespace AppBundle\Security\Voter;


use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\User;
use AppBundle\Manager\InvestOfferManager;
use AppBundle\Manager\QuoteApplyManager;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class InvestOfferVoter
 * @package AppBundle\Security\Voter
 */
class InvestOfferVoter extends Voter
{
    const CREATE = 'create';

    private $investOfferManager;
    private $quoteApplyManager;

    /**
     * InvestOfferVoter constructor.
     * @param InvestOfferManager $investOfferManager
     * @param QuoteApplyManager $quoteApplyManager
     */
    public function __construct(InvestOfferManager $investOfferManager, QuoteApplyManager $quoteApplyManager)
    {
        $this->investOfferManager = $investOfferManager;
        $this->quoteApplyManager = $quoteApplyManager;
    }
    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::CREATE))) {
            return false;
        }

        if (!$subject instanceof InvestOffer) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return boolean
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        /** @var InvestOffer $investOffer */
        $investOffer = $subject;

        switch ($attribute) {
            case self::CREATE:
                return $this->canCreate($investOffer, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * If QuoteApply in Approved status - unlimited InvestOffers, if in Rejected - no InvestOffers,
     * other statuses - only 1 InvestOffer
     *
     * @param InvestOffer $investOffer
     * @param User $user
     * @param TokenInterface $token
     * @return bool
     */
    private function canCreate(InvestOffer $investOffer, User $user, TokenInterface $token)
    {
        if ($this->quoteApplyManager->hasLenderApprovedQuote($user)) {
            return true;
        }

        if ($this->quoteApplyManager->hasLenderRejectedQuote($user)) {
            return false;
        }

        // Fix: CU540-593 Lender: no possibility to create more than one offer
        if ($this->investOfferManager->getInvestOffersForUser($token->getUser())) {
            if ($this->quoteApplyManager->hasLenderActiveQuote($user)) {
                return true;
            }  else {
                return false;
            }
        }

        return true;
    }

}