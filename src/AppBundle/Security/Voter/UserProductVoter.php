<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 13.12.16
 * Time: 9:00
 */

namespace AppBundle\Security\Voter;


use AppBundle\Entity\User;
use AppBundle\Entity\UserProduct;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class UserProductVoter
 *
 * This voter is used inside UserProductManager, so it is the part of the UserProductManager and should use  UserProductRepository to get UserProduct entity if necessary, not UserProductManager. In other case it`s initialisation in the container will be looped.
 *
 * @package AppBundle\Security\Voter
 */
class UserProductVoter extends Voter
{
    const CREATE = 'create';

    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::CREATE))) {
            return false;
        }

        if (!$subject instanceof UserProduct) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return boolean
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {

        $user = $token->getUser();
        /** @var UserProduct $userProduct */
        $userProduct = $subject;

        switch ($attribute) {
            case self::CREATE:
                return $this->canCreate($userProduct);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * If there is UserProduct for some user and product, new UserProduct for the same user and product can not be created
     *
     * @param UserProduct $userProduct
     *
     * @return bool
     */
    private function canCreate(UserProduct $userProduct)
    {
        if ($this->em->getRepository(UserProduct::class)->findOneBy([
            'user' => $userProduct->getUser(),
            'product' => $userProduct->getProduct()
        ])) {
            return false;
        }

        return true;
    }

}