<?php
/**
 * User: Vladimir
 * Date: 1/13/17
 * Time: 11:47 AM
 */

namespace AppBundle\Security\Voter;


use AppBundle\Entity\LoanApply;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class LoanStatusVoter extends Voter
{
    const HAS_LOAN_DRAFTED = 'HAS_LOAN_DRAFTED';

    const LOAN_NOT_DRAFTED_CONTENT = [
        'message' => 'You do not have loan request in draft state which can be edited',
        'code' => 'loan.not.drafted'
    ];

    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function supports($attribute, $subject)
    {
        if (in_array($attribute, [self::HAS_LOAN_DRAFTED])) {
            return true;
        }
        return false;
    }

    public function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        $loanApply = $this->em->getRepository('AppBundle:LoanApply')->getLoanApplyForUser($user, [LoanApply::STATUS_DRAFT]);
        if(empty($loanApply)){
            return false;
        }

        return true;
    }

}