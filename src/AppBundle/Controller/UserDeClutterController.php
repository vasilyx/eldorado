<?php
namespace AppBundle\Controller;

use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\Declutter;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\QuoteApply;
use AppBundle\Event\CallCreditEvent;
use AppBundle\Event\CallCreditEvents;
use AppBundle\Security\Voter\LoanStatusVoter;
use AppBundle\Validator\Constraints\ObjectNotNull;
use AppBundle\Validator\Constraints\SubmittedLoanApply;
use AppBundle\Validator\Constraints\SubmittedLoanApplyValidator;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use LogBundle\Entity\UserActivityLog;
use LogBundle\Handler\UserActivityHandler;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\ConstraintViolationList;


class UserDeClutterController extends ApiController
{
    /**
     *
     * Need to send declutters array of declutter's objects in body (content) of the request
     * <pre>
     * <strong>example:</strong>
     * [
     *  {"user_creditline": {"id": "40"},"need_to_close": "1"},
     *  {"user_creditline": {"id": "41"},"need_to_close": "0"}
     * ]
     * </pre>
     *
     * @ApiDoc(
     *  section="User/DeClutter",
     *  description="This method saves Declutter records for selected UserCreditlines. Created by Vladimir Glechikov."
     * )
     * @Post("/declutters")
     *
     * @param $request Request
     *
     * @return View
     *
     */
    public function postDeClutterAction(Request $request)
    {
        if (!$this->isGranted(LoanStatusVoter::HAS_LOAN_DRAFTED)) {
            return $this->getResponse(LoanStatusVoter::LOAN_NOT_DRAFTED_CONTENT, self::STATUS_ERROR, Response::HTTP_FORBIDDEN);
        }
        $em = $this->getDoctrine()->getManager();
        $deCluttersJson = $request->getContent();
        $serializer = $this->get('serializer');
        $declutters = $serializer->deserialize($deCluttersJson, 'ArrayCollection<AppBundle\Entity\Declutter>', 'json');

        $validator = $this->get('validator');
        if (empty($declutters)) {
            return $this->getValidationErrorResponse($validator->validate($declutters, (new ObjectNotNull())->setErrorPath('declutter')->setPayload(['code' => 'declutter.isEmpty'])));
        }

        $loanApply = $em->getRepository('AppBundle:LoanApply')->getOneByApprovedQuote($this->getUser());
        if (!$loanApply) {
            throw new NotFoundHttpException("LoanApply not found");
        }
        $errors = new ConstraintViolationList();
        $savedDeclutters = $em->getRepository('AppBundle:Declutter')->getAllByLoanApply($loanApply);
        $decluttersAmount = 0;
        foreach ($declutters as $declutter) {
            /** @var $declutter Declutter */
            $decluttersAmount += $this->get('app.utils.refinancing_computer')->setUserCreditline($declutter->getUserCreditline())->getTotalBalanceNow();
            $declutter->setLoanApply($loanApply);
            $errors->addAll($validator->validate($declutter));
            $em->persist($declutter);
            // check saved declutters,
            // If Close state equal to the existed record state - then no action
            // In other case - mark previous record as "Archive" and create new record.
            foreach ($savedDeclutters as $savedDeclutter) {
                if ($savedDeclutter->getUserCreditline() == $declutter->getUserCreditline()) {
                    if ($savedDeclutter->getNeedToClose() == $declutter->getNeedToClose()) {
                        $em->detach($declutter);
                    } else {
                        $savedDeclutter->setArchive(true);
                        $em->persist($savedDeclutter);
                        $em->persist($declutter);
                    }
                    break;
                }
            }
        }
        $decluttersAmountControlValues = $this->getParameter('quote_term_and_declutters_amount');
        if ($decluttersAmount < $decluttersAmountControlValues['min'] || $decluttersAmount >= $decluttersAmountControlValues['max']) {
            return $this->getResponse(
                [
                    'declutter' => [
                        'code'    => 'declutters.amount.notInRange',
                        'message' => 'Minimal amount ' . $decluttersAmountControlValues['min'] . '. Maximal amount ' . $decluttersAmountControlValues['max'] . '.',
                    ],
                ],
                self::STATUS_ERROR,
                Response::HTTP_BAD_REQUEST
            );
        }
        if (count($errors) > 0) {
            return $this->getValidationErrorResponse($errors);
        }
        $em->flush();

        $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $this->getCurrentUser(), UserActivityHandler::CONTEXT_ACTION => UserActivityLog::BORROWER_PRODUCT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::DECLUTTER_ACTION]);

        return $this->getResponse(self::STATUS_SUCCESS);
    }


    /**
     * @ApiDoc(
     *  section="User/DeClutter",
     *  description="This method validates that user previously had filled all profile data. Then change LoanApply status to 'submitted'.",
     *  parameters={
     *       {
     *          "name" = "loan_amount",
     *          "dataType" = "float",
     *          "required" = true,
     *          "description" = "Loan amount"
     *       },
     *       {
     *          "name" = "loan_term",
     *          "dataType" = "integer",
     *          "required" = true,
     *          "description" = "Loan term"
     *       },
     *       {
     *          "name" = "loan_monthly",
     *          "dataType" = "float",
     *          "required" = true,
     *          "description" = "Loan monthly"
     *       },
     *       {
     *          "name" = "loan_apr",
     *          "dataType" = "float",
     *          "required" = true,
     *          "description" = "Loan rate"
     *       }
     *  }
     * )
     * @Post("/submit_loan_application")
     *
     * @param $request Request
     *
     * @return View
     *
     */
    public function postSubmitLoanApplicationAction(Request $request)
    {
        if (!$this->isGranted(LoanStatusVoter::HAS_LOAN_DRAFTED)) {
            return $this->getResponse(LoanStatusVoter::LOAN_NOT_DRAFTED_CONTENT, self::STATUS_ERROR, Response::HTTP_FORBIDDEN);
        }

        $errors = new ConstraintViolationList();
        $validator = $this->get('validator');
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        /** @var LoanApply $loanApply */
        $loanApply = $em->getRepository('AppBundle:LoanApply')->getOneDrafted($this->getUser());
        if (!empty($loanApply)) {
            $loanStatusConstraint = new EqualTo(['value' => LoanApply::STATUS_DRAFT, 'payload' => ['propertyPath' => 'loanApply.status', 'code' => 'loanApply.statusNotDraft']]);
            $errors->addAll($validator->validate($loanApply->getStatus(), $loanStatusConstraint));

            $quoteApply = $loanApply->getQuoteApply();
            if (!empty($quoteApply)) {
                $quoteStatusConstraint = new EqualTo(['value' => QuoteApply::STATUS_APPROVED, 'payload' => ['propertyPath' => 'quoteApple.status', 'code' => 'quoteApple.statusNotApproved']]);
                $errors->addAll($validator->validate($quoteApply->getStatus(), $quoteStatusConstraint));
            } else {
                $errors->addAll($validator->validate($quoteApply, (new ObjectNotNull())->setErrorPath('quoteApply')->setPayload(['code' => 'quoteApply.isEmpty'])));
            }
        } else {
            $errors->addAll($validator->validate($loanApply, (new ObjectNotNull())->setErrorPath('loanApply')->setPayload(['code' => 'loanApply.isEmpty'])));
        }

        //TODO: validate it
        $loanApply
                  ->setAmount($request->request->get('loan_amount'))
                  ->setApr($request->request->get('loan_apr'))
                  ->setMonthly($request->request->get('loan_monthly'))
                  ->setTerm($request->request->get('loan_term'))
        ;

        $userIncome = $this->get('whs.manager.user_income_manager')->getUserIncomeByUser($user);

        $errors->addAll($validator->validate($userIncome, (new ObjectNotNull())->setErrorPath('userIncome')->setPayload(['code' => 'userIncome.isEmpty'])));

        $userEmploymentCurrent = $em->getRepository('AppBundle:UserEmployment')->getCurrentsForUser($user);
        $userEmploymentEarlier = $em->getRepository('AppBundle:UserEmployment')->getEarlierStartedForUser($user);

        $fiveYearsOldConstraint = new LessThanOrEqual('-5 years');
        $fiveYearsOldConstraint->payload = ['propertyPath' => 'userEmployment.occMonthStarted', 'code' => 'userEmployment.occMonthStarted'];
        $userEmploymentEarlierError = $validator->validate($userEmploymentEarlier->getOccMonthStarted(), $fiveYearsOldConstraint);
        $errors->addAll($userEmploymentEarlierError);
        if (count($userEmploymentEarlierError) === 0) {
            $userEmploymentCurrentConstraint = new Range(['min' => 1, 'payload' => ['propertyPath' => 'userEmployment.current', 'code' => 'userEmployment.current']]);
            $userEmploymentCurrentError = $validator->validate(count($userEmploymentCurrent), $userEmploymentCurrentConstraint);
            if (count($userEmploymentCurrentError)) {
                $errors->addAll($userEmploymentCurrentError);
            }
        }
        $userHousehold = $em->getRepository('AppBundle:UserHousehold')->getOneByUser($user);
        $errors->addAll($validator->validate($userHousehold, (new ObjectNotNull())->setErrorPath('userHousehold')->setPayload(['code' => 'userHousehold.isEmpty'])));

        $userCreditlines = $em->getRepository('AppBundle:UserCreditline')->getActiveForUser($user);
        $errors->addAll($validator->validate($userCreditlines, (new ObjectNotNull())->setErrorPath('userCreditline')->setPayload(['code' => 'userCreditline.isEmpty'])));

        $declutters = $em->getRepository('AppBundle:Declutter')->getAllForDraftedLoanApply($user);
        $errors->addAll($validator->validate($declutters, (new ObjectNotNull())->setErrorPath('declutter')->setPayload(['code' => 'declutter.isEmpty'])));

        if (count($errors) > 0) {
            return $this->getValidationErrorResponse($errors);
        }

        $em = $this->getDoctrine()->getManager();
        $loanApply = $em->getRepository('AppBundle:LoanApply')->getOneDrafted($this->getUser());
        if (empty($loanApply)) {
            throw new NotFoundHttpException('LoanApply not found');
        }
        $loanApply->setStatus(LoanApply::STATUS_CALLVALIDATE_CHECK);
        $quoteApply = $loanApply->getQuoteApply();
        /*
         * Log to database
         */
        $this->get('monolog.logger.loan_apply_status')->info('', [$loanApply]);
        if (empty($quoteApply)) {
            throw new NotFoundHttpException('QuoteApply not found');
        }
        $quoteApply->setStatus(QuoteApply::STATUS_VERIFICATION);

        $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $user, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::BORROWER_PRODUCT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::LOAN_REQUEST_SUBMITTED]);

        $eventDispatcher = $this->get('event_dispatcher');

        /**
         * @var CallCreditEvent $callValidateEvent
         */
        $callValidateEvent = $eventDispatcher->dispatch(CallCreditEvents::CALL_VALIDATE_REQUEST,
            CallCreditEvent::create()
                ->setCallCreditApiCalls(
                    CallCreditAPICalls::create()
                        ->setQuoteApply($quoteApply)
                        ->setPurpose(CallCreditAPICalls::PURPOSE_QS)
                )
        );

        /**
         * Logging
         */
        $this->get('monolog.logger.callcredit')->debug(sprintf('Event %1$s is generated.', CallCreditEvents::CALL_VALIDATE_REQUEST), [$this->getParameter('kernel.environment')]);
        /**
         * End logging
         */
        $callCreditCV = $callValidateEvent->getCallCreditApiCalls();
        $em->persist($callCreditCV);

//        /** Generate CallCreditEvent which will be handled by CallReportListener method: processCallReportRequest */
//        /** @var CallCreditEvent $callCreditEvent */
//        $callCreditEvent = $this->get('event_dispatcher')->dispatch(CallCreditEvents::CALL_REPORT_REQUEST,
//            CallCreditEvent::create()
//                ->setCallCreditApiCalls(
//                    CallCreditAPICalls::create()
//                        ->setQuoteApply($quoteApply)
//                        ->setPurpose(CallCreditAPICalls::PURPOSE_QS)
//                )
//        );
//
//        /**
//         * Logging
//         */
//        $this->get('monolog.logger.callcredit')->debug(sprintf('Event %1$s is generated.', CallCreditEvents::CALL_REPORT_REQUEST), [$this->getParameter('kernel.environment')]);
//        /**
//         * End logging
//         */
//        $callCreditCR = $callCreditEvent->getCallCreditApiCalls();
//        $em->persist($callCreditCR);
//
//        /** @var CallCreditEvent $callCreditAffordabilityEvent */
//        $callCreditAffordabilityEvent = $eventDispatcher->dispatch(CallCreditEvents::AFFORDABILITY_REQUEST,
//            CallCreditEvent::create()
//                ->setCallCreditApiCalls(
//                    CallCreditAPICalls::create()
//                        ->setQuoteApply($quoteApply)
//                        ->setPurpose(CallCreditAPICalls::PURPOSE_QS)
//                )
//        );
//
//        /**
//         * Logging
//         */
//        $this->get('monolog.logger.callcredit')->debug(sprintf('Event %1$s is generated.', CallCreditEvents::AFFORDABILITY_REQUEST), [$this->getParameter('kernel.environment')]);
//        /**
//         * End logging
//         */
//        $affordabilityRequest = $callCreditAffordabilityEvent->getCallCreditApiCalls();
//        $em->persist($affordabilityRequest);

        $em->persist($quoteApply);
        $em->persist($loanApply);

        $em->flush();

        return $this->getResponse(self::STATUS_SUCCESS);
    }

    /**
     * @ApiDoc(
     *  section="User/DeClutter",
     *  description="
     *      Method return DeClutter items of the current DRAFT Loan Request for current User.
     *      If loan_apply_id specified, then return DeClutter items related for this loan_apply_id and current user.
     *      Created by Vladimir Glechikov.",
     *  parameters={
     *      {"name"="loan_apply_id", "dataType"="string", "required"=false, "description"="Id of LoanApply"}
     *  }
     * )
     * @Get("/declutters")
     *
     * @param $request Request
     *
     * @return View
     *
     */
    public function getDeCluttersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if ($request->query->get('loan_apply_id')) {
            /** @var LoanApply $loanApply */
            $loanApply = $em->getRepository('AppBundle:LoanApply')->find($request->query->get('loan_apply_id'));
            if ($loanApply && $loanApply->getUser() == $this->getUser()) {
                $declutters = $em->getRepository('AppBundle:Declutter')->getAllByLoanApply($loanApply);
                return $this->getResponse($declutters);
            } else {
                throw new BadRequestHttpException('Wrong loan_apply_id');
            }
        }
        $declutters = $em->getRepository('AppBundle:Declutter')->getAllForDraftedLoanApply($this->getUser());
        return $this->getResponse($declutters);
    }
}