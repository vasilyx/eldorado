<?php

namespace AppBundle\Controller;


use AdminBundle\Response\InvestorPortfolioSummary;
use AppBundle\Entity\BankDetails;
use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\Communication;
use AppBundle\Entity\ContractMatching;
use AppBundle\Entity\Instalment;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\InvestOfferProcess;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\Agreement;
use AppBundle\Entity\Product;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\QuoteIncome;
use AppBundle\Entity\QuoteTerm;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Entity\UserDetails;
use AppBundle\Entity\UserHousehold;
use AppBundle\Entity\UserIncome;
use AppBundle\Entity\UserPreferences;
use AppBundle\Event\CallCreditEvent;
use AppBundle\Event\CallCreditEvents;
use AppBundle\Event\CallValidateEvent;
use AppBundle\Event\SystemTransactionEvent;
use AppBundle\Event\TransactionEvents;
use AppBundle\Exception\ProductNotFoundException;
use AppBundle\Exception\QuoteApplyNotFoundException;
use AppBundle\Exception\ValidationException;
use AppBundle\Manager\InvestOfferManager;
use AppBundle\Manager\LoanApplyManager;
use AppBundle\Repository\TransactionRepository;
use AppBundle\Validator\Constraints\ObjectNotNull;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use LogBundle\Entity\UserActivityLog;
use LogBundle\Handler\UserActivityHandler;
use Metadata\Tests\Driver\Fixture\T\T;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class UserController extends ApiController
{
    /**
     * <pre>
     * ContentArray{
     *   'token': sessionToken,
     *   'user_status': USER_STATUS
     * }
     * </pre>
     *
     * @ApiDoc(
     *  section="User",
     *  description="Step 1. Borrower registration. Returns Auth token on success in element 'token' of the array ",
     *  parameters={
     *      {"name"="firstname", "dataType"="string", "required"=true, "description"="First name"},
     *      {"name"="lastname", "dataType"="string", "required"=true, "description"="Last name"},
     *      {"name"="username", "dataType"="string", "required"=true, "description"="Email"},
     *      {"name"="password", "dataType"="string", "required"=true, "description"="Password. Password must consist of 8 symbols, 1 capital letter, 1 number."},
     *      {"name"="plain_password", "dataType"="string", "required"=true, "description"="Password Confirmation"},
     *      {"name"="birthdate", "dataType"="string", "required"=true, "description"="Date of birth. ISO8601 base format (Ymd). User must be older than 18 years old"},
     *      {
     *          "name"          = "confirm_url",
     *          "dataType"      = "string",
     *          "required"      =  true,
     *          "description"   = "Frontend confirm url without protocol and domain name. Use '/' as first symbol. Two GET parameters are sent in the url: t - the token, e - email. You should resend this parameters to /email_confirmation api method.",
     *          }
     *  }
     * )
     * @Post("/registration/borrower")
     * @ParamConverter("userEntity", converter="fos_rest.request_body", options={
     *      "validator"={"groups"={"details", "registration"}}
     *  })
     */
    public function registrationBorrowerAction(Request $request, User $userEntity, ConstraintViolationListInterface $validationErrors)
    {
        return $this->registrationUser($request, $userEntity, $validationErrors, Product::BORCONS1);
    }

    /**
     * The method is created to follow DRY principe for registrationBorrowAction and registrationLenderAction
     *
     * @param   Request $request
     * @param   User $userEntity
     * @param   ConstraintViolationListInterface $validationErrors
     * @param   string $productCode
     *
     * @return View
     */
    private function registrationUser(Request $request, User $userEntity, ConstraintViolationListInterface $validationErrors, $productCode)
    {
        /*
         * check if the validation errors occur for the User deserialized from the request body
         */
        if (count($validationErrors) > 0) {
            return $this->getValidationErrorResponse($validationErrors);
        }

        /*
         * the birthdate is not a part of the user entity, it is a part of the userDetails entity
         * we get it from body request manually
         */

        $isoDate = $request->get('birthdate', null);
        $confirmUrl = $request->get('confirm_url', null);

        if (!$confirmUrl) {
            return $this->getResponse([
                'code'    => 'user.registration.confirm_url.isBlank',
                'message' => 'Confirm url should not be blank.'
            ], self::STATUS_ERROR, 400);
        }
        /*
         * if the $isoDate is null, the $birthdate will be false
         */
        $birthdate = \DateTime::createFromFormat('Ymd', $isoDate);
        $userDetails = new UserDetails();
        $userDetails
            ->setUser($userEntity)
            ->setFirstName($request->get('firstname'))
            ->setLastName($request->get('lastname'))
            ->setBirthdate($birthdate ?: new \DateTime());

        $validator = $this->get('validator');

        $errors = $validator->validate($userDetails, null, ['registration']);

        /*
         * check if the validation errors occur for the UserDetails
         */
        if (count($errors) > 0) {
            return $this->getValidationErrorResponse($errors);
        }

        $this->getDoctrine()->getManager()->persist($userDetails);

        $am = $this->get('whs.manager.quote_apply_manager');


        $um = $this->get('whs.manager.user_manager');

        $session = $um->registerUser($userEntity);

        $product = $this->getDoctrine()->getRepository(Product::class)->getProductByCode($productCode);

        if ($product) {
            $quoteApply = $am->createQuoteForUser($userEntity, $product);
        } else {
            throw new ProductNotFoundException(ProductNotFoundException::PRODUCT_CODE_NOT_FOUND, $productCode);
        }

        $isBorrower = true;
        if ($productCode == Product::INVCONS) {
            $isBorrower = false;
        }

        $this->sendRegistrationConfirm($request, $userEntity, $confirmUrl, $isBorrower);

        $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $userEntity, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::ACCOUNT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::USER_REGISTERED_ACTION]);

        switch ($quoteApply->getProduct()->getType()) {
            case (Product::TYPE_BORROWER):
                $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $userEntity, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::BORROWER_PRODUCT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::BORROWER_APPLICATION_INITIATED]);
                break;
            case (Product::TYPE_INVESTOR):
                $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $userEntity, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::LENDER_PRODUCT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::LENDER_APPLICATION_INITIATED]);
                break;
        }

        return $this->getResponse([
            'token'       => $session->getToken(),
            'user_status' => $um->resolveRoles($session->getUser())[0]
        ]);
    }

    private function sendRegistrationConfirm(Request $request, User $user, $url, $isBorrower)
    {
        $this->get('whs_mailer')->sendRegistrationConfirm(
            $user->getUsername(), [
                'full_name' => $user->getFullName(),
                'link' => $request->getSchemeAndHttpHost() . $url . '?t=' .
                    $user->getConfirmEmailToken(),
                'isBorrower' => $isBorrower
            ]
        );
    }

    /**
     * <pre>
     * ContentArray{
     *   'token': sessionToken,
     *   'user_status': USER_STATUS
     * }
     * </pre>
     *
     * @ApiDoc(
     *  section="User",
     *  description="Step 1. Lender Registration. Returns Auth token on success in element 'token' of the array ",
     *  parameters={
     *      {"name"="firstname", "dataType"="string", "required"=true, "description"="First name"},
     *      {"name"="lastname", "dataType"="string", "required"=true, "description"="Last name"},
     *      {"name"="username", "dataType"="string", "required"=true, "description"="Email"},
     *      {"name"="password", "dataType"="string", "required"=true, "description"="Password. Password must consist of 8 symbols, 1 capital letter, 1 number."},
     *      {"name"="plain_password", "dataType"="string", "required"=true, "description"="Password Confirmation"},
     *      {"name"="birthdate", "dataType"="string", "required"=true, "description"="Date of birth. ISO8601 base format  (Ymd). User must be older than 18 years old"},
     *      {
     *          "name"          = "confirm_url",
     *          "dataType"      = "string",
     *          "required"      =  true,
     *          "description"   = "Frontend confirm url without protocol and domain name. Use '/' as first symbol. Two GET parameters are sent in the url: t - the token, e - email. You should resend this parameters to /email_confirmation api method.",
     *          }
     *  }
     * )
     * @Post("/registration/lender")
     * @ParamConverter("userEntity", converter="fos_rest.request_body", options={
     *      "validator"={"groups"={"details", "registration"}}
     *  })
     * @param Request $request
     * @param User $userEntity
     * @param ConstraintViolationListInterface $validationErrors
     * @return View
     */
    public function registrationLenderAction(Request $request, User $userEntity, ConstraintViolationListInterface $validationErrors)
    {
        return $this->registrationUser($request, $userEntity, $validationErrors, Product::INVCONS);
    }

    /**
     * 2-d step
     *
     * @ApiDoc(
     *  section="User",
     *  description="Add qoute term",
     *  parameters={
     *      {"name"="amount", "dataType"="string", "required"=true, "description"="Quote Amount Requested between 1000 and 30000"},
     *      {"name"="term", "dataType"="string", "required"=true, "description"="Quote Term Requested between 12 and 60"},
     *  }
     * )
     *
     * @Post("/add_quote_term")
     * @ParamConverter("quoteTerm", converter="fos_rest.request_body")
     *
     * @param Request $request
     * @param QuoteTerm $quoteTerm
     * @param ConstraintViolationListInterface $validationErrors
     *
     * @return \Symfony\Component\HttpFoundation\Response|View
     */
    public function addQuoteTermAction(Request $request, QuoteTerm $quoteTerm, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            return $this->getValidationErrorResponse($validationErrors);
        }

        $this->get('whs.manager.quote_term_manager')->addQuoteTerm($quoteTerm, $this->getCurrentUser());

        return $this->getResponse([]);
    }

    /**
     * Returns quote term for the borrower QuoteApply with the DRAFT status only.
     *
     * @ApiDoc(
     *      section="User",
     *     description="Get quote term for current user"
     * )
     * @Get("/quote_term")
     */
    public function getQuoteTermAction()
    {

        $quoteTerm = $this->get('whs.manager.quote_term_manager')->getDraftedQuoteTerm($this->getCurrentUser());

        return $this->getResponse(
            $quoteTerm
        );
    }

    /**
     * 4-d step
     *
     * @ApiDoc(
     *      section="UserPreferences",
     *      description="Update user preferences",
     *      parameters={
     *      {"name"="receive_comms", "dataType"="boolean", "required"=false, "description"="Does user wish to receive comms"},
     *     {"name"="has_no_dependencies", "dataType"="boolean", "required"=false, "description"="Does user have dependencies"}
     *  }
     * )
     *
     * @Rest\Patch("/user_preferences")
     *
     * @param Request $request
     * @return View
     */
    public function updateUserPreferencesAction(Request $request)
    {
        $receiveComms = $request->request->get('receive_comms', null);
        $hasNoDependencies = $request->request->get('has_no_dependencies', null);
        /** @var UserPreferences $userReference */
        $userReference = $this->get('whs.manager.user_preferences_manager')->updateUserPreferences($this->getCurrentUser(), [
            'receiveComms'      => $receiveComms,
            'hasNoDependencies' => $hasNoDependencies
        ]);

        $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $userReference->getUser(), UserActivityHandler::CONTEXT_ACTION => UserActivityLog::ACCOUNT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::USER_PREFERENCES_ACTION]);

        return $this->getResponse(
            $userReference
        );
    }

    /**
     * @ApiDoc(
     *      section="UserPreferences",
     *     description="Get user preferences. Created by Mikhail Pegasin"
     * )
     * @Get("/user_preferences")
     */
    public function getUserPreferencesAction(Request $request)
    {
        $result = [];
        $status = self::STATUS_ERROR;
        $code = Response::HTTP_NOT_FOUND;
        if ($userPreferences = $this->get('whs.manager.user_preferences_manager')->getPreferencesForUser($this->getUser())) {
            $result = $userPreferences;
            $status = self::STATUS_SUCCESS;
            $code = Response::HTTP_OK;
        }

        return $this->getResponse($result, $status, $code);
    }

    /**
     * @ApiDoc(
     *  section="User",
     *  description="Update user details",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *      {"name"="first_name", "dataType"="string", "required"=false, "description"="Firstname"},
     *      {"name"="middle_name", "dataType"="string", "required"=false, "description"="Middle name"},
     *      {"name"="last_name", "dataType"="string", "required"=false, "description"="Last name"},
     *      {"name"="maiden_name", "dataType"="string", "required"=false, "description"="Maiden name"},
     *      {"name"="gender", "dataType"="string", "required"=false, "description"="Gender. m - male, f - female"},
     *      {"name"="birthdate", "dataType"="string", "required"=false, "description"="Birthdate"},
     *      {"name"="title", "dataType"="string", "required"=false, "description"="title"},
     *      {"name"="screen_name", "dataType"="string", "required"=false, "description"="screen_name"},
     *      {"name"="martial_status", "dataType"="string", "required"=false, "description"="Martial status"},
     *      {"name"="default_communication_id", "dataType"="string", "required"=false, "description"="Default communication id"},
     *  }
     * )
     * @Post("/update_details")
     * @ParamConverter("detailsEntity", converter="fos_rest.request_body")
     * @param Request $request
     * @param UserDetails $detailsEntity
     * @param ConstraintViolationListInterface $validationErrors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateUserDetailsAction(Request $request, UserDetails $detailsEntity, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            return $this->getValidationErrorResponse($validationErrors);
        }

        $userDetails = $this->get('whs.manager.user_details_manager')->getDetailsForUser($this->getUser());
        $newUserDetails = $this->updateFromRequest($request, $userDetails);
        $this->get('whs.manager.user_details_manager')->updateDetails($newUserDetails);
        return $this->getResponse(null);
    }

    /**
     * This action is called at the end of the user quote journey.
     * It checks that quoteTerm is present, that quoteIncome is present and that 5-years period for the field 'moved' in the address is valid
     *
     * @ApiDoc(
     *      section="User",
     *      description="Checks if the quoteApply is right. Return status 200 if no errors.",
     * )
     *
     * @Get("/apply_quote")
     *
     * @param Request $request
     * @return View
     */
    public function applyQuoteAction(Request $request)
    {
        /**
         * Customer constraint to check that object not null
         * @var ObjectNotNull $onn
         */
        $onn = new ObjectNotNull();
        $errors = new ConstraintViolationList();
        $validator = $this->get('validator');

        /**
         * @var QuoteTerm $quoteTerm
         */
        $quoteTerm = $this->get('doctrine')->getRepository('AppBundle:QuoteTerm')->getQuoteTermForUser($this->getCurrentUser());
        /**
         * Validator uses ObjectNotNull constraint with the errorPath 'qouteTerm'
         */
        $notNullError = $validator->validate($quoteTerm, $onn->setErrorPath('quoteTerm'));
        if (0 === count($notNullError)) {
            /**
             * Validator uses the default constraint (is described in a yml file), validation group 'applyQuote'
             */
            $errors->addAll($validator->validate($quoteTerm, null, 'applyQuote'));
        } else {
            $errors->addAll($notNullError);
        }

        $quoteIncome = $this->get('doctrine')->getRepository('AppBundle:QuoteIncome')->getQuoteIncomeForUser($this->getCurrentUser());
        $notNullError = $validator->validate($quoteIncome, $onn->setErrorPath('quoteIncome'));
        if (0 === count($notNullError)) {
            $errors->addAll($validator->validate($quoteIncome, null, 'applyQuote'));
        } else {
            $errors->addAll($notNullError);
        }
        try {
            $moved = $this->get('whs.manager.user_address_manager')->getEarlierMovedForUser($this->getCurrentUser());
        } catch (\Exception $e) {
            return $this->getResponse([
                'code'    => 'userAddress.isNull',
                'message' => $e->getMessage()
            ], self::STATUS_ERROR, 400);
        }

        $fya = new LessThanOrEqual('-5 years');
        $fya->payload = ['propertyPath' => 'userAddress.moved', 'code' => 'userAddress.moved.smallPeriod'];
        $errors->addAll($validator->validate($moved, $fya));

        if (count($errors) > 0) {
            return $this->getValidationErrorResponse($errors);
        }

        $quoteApply = $this->get('whs.manager.quote_apply_manager')->getDraftBorrowerQuoteApply($this->getCurrentUser());
        $quoteApply
            ->setStatus(QuoteApply::STATUS_PENDING);

        $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $this->getCurrentUser(), UserActivityHandler::CONTEXT_ACTION => UserActivityLog::BORROWER_PRODUCT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::BORROWER_APPLICATION_SUBMITTED]);

        $callCredit = new CallCreditAPICalls();
        $callCredit
            ->setQuoteApply($quoteApply)
            ->setPurpose(CallCreditAPICalls::PURPOSE_QS);
        /** Generate CallCreditEvent which will be handled by CallReportListener method: processCallReportRequest */
        /** @var CallCreditEvent $callCreditEvent */
        $callCreditEvent = $this->get('event_dispatcher')->dispatch(CallCreditEvents::CALL_REPORT_REQUEST,
            CallCreditEvent::create()
                ->setCallCreditApiCalls($callCredit)
        );
        /**
         * Logging
         */
        $this->get('monolog.logger.callcredit')->debug(sprintf('Event %1$s is generated.', CallCreditEvents::CALL_REPORT_REQUEST), [$this->getParameter('kernel.environment')]);
        /**
         * End logging
         */

        $callCredit = $callCreditEvent->getCallCreditApiCalls();

        $em = $this->get('doctrine')->getManager();
        $em->persist($callCredit);
        $em->flush();

        $this->get('event_dispatcher')->dispatch(TransactionEvents::DEPC,
            SystemTransactionEvent::create()
                ->setUser($this->getUser())
                ->addTransaction(
                    Transaction::create()
                        ->setTransactionAmount($quoteTerm->getAmount())
                        ->setTransactionOwner(Transaction::TRANSACTION_OWNER_BORROWER)
                )
        );

        return $this->getResponse($quoteApply);
    }

    /**
     * This method returns current user UserDetails. Created by Mikhail Pegasin <mpegasin@rambler.ru>.
     *
     * @ApiDoc(
     *     section="User"
     * )
     *
     * @return  View
     *
     * @Get("/user_details")
     */
    public function getUserDetailsAction()
    {
        $details = $this->get('whs.manager.user_details_manager')->getDetailsForUser($this->getUser());

        return $this->getResponseWithSerializerGroups($details, ['Default']);

    }

    /**
     * This method returns all Invest Offers of the current user. Created by Mikhail Pegasin <mpegasin@rambler.ru>.
     *
     * @ApiDoc(
     *     section="User/InvestOffer"
     * )
     *
     * @return  View
     *
     * @Get("/invest_offers")
     */
    public function getInvestOffersAction()
    {
        return $this->getResponse($this->get('whs.manager.invest_offer_manager')->getOfferDetailsListByUser($this->getUser()));
    }

    /**
     * This method returns an Invest Offer of the current user by id. Created by Mikhail Pegasin <mpegasin@rambler.ru>.
     *
     * @ApiDoc(
     *     section="User/InvestOffer"
     * )
     *
     * @param   InvestOffer $investOffer
     * @return  View
     *
     * @Get("/invest_offers/{id}")
     *
     * @ParamConverter("investOffer", class="AppBundle:InvestOffer")
     */
    public function getInvestOfferAction(InvestOffer $investOffer)
    {
        $process = $this->getDoctrine()->getManager()->getRepository('AppBundle:InvestOfferProcess')->findOneBy(['investOffer' => $investOffer]);
        $response = json_decode($this->get('serializer')->serialize($investOffer, 'json'), true);
        $response['invest_offer'] = $investOffer;
        if ($process) {
            $response['process_id'] = $process->getId();
        }
        return $this->getResponse($response);
    }

    /**
     * This method set InvestOffer in status STATUS_CANCEL.
     *
     * @ApiDoc(
     *     section="User/InvestOffer"
     * )
     *
     * @param   InvestOffer $investOffer
     * @return  View
     *
     * @Rest\Delete("/invest_offers/{id}")
     *
     * @ParamConverter("investOffer", class="AppBundle:InvestOffer")
     */
    public function deleteInvestOfferAction(InvestOffer $investOffer)
    {
        if ($investOffer->getStatus() != InvestOffer::STATUS_DRAFT) {
            return $this->getResponse(['message' => "You can delete only Drafted Offer.", 'code' => 'investOffer.notDrafted'], self::STATUS_ERROR);
        }
//        elseif (!$this->get('whs.manager.quote_apply_manager')->hasLenderApprovedQuote($this->getUser())) {
//            return $this->getResponse(['message' => "You can't delete Offer if you quote is not in Approved state.", 'code' => 'quoteApply.notApproved'], self::STATUS_ERROR);
//        }
        $investOffer->setStatus(InvestOffer::STATUS_CANCEL);
        $em = $this->getDoctrine()->getManager();
        $em->persist($investOffer);
        $em->flush();

        return $this->getResponse([]);
    }

    /**
     * This method adds InvestOffer. Created by Mikhail Pegasin <mpegasin@rambler.ru>.
     *
     * @ApiDoc(
     *     section="User/InvestOffer",
     *     parameters={
     *          {
     *              "name"          = "investment_type",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Investment type. 100 -ISA, 200 - SIPP, 300 - GEN",
     *          },{
     *              "name"          = "finance_type",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Finance type. 100 - Origin",
     *          },{
     *              "name"          = "investment_name",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Name of Investment Offer",
     *          },{
     *              "name"          = "investment_amount",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Investment Offer Amount",
     *          },{
     *              "name"          = "investment_frequency",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Investment Frequency (100 - Monthly, 200 - Quarterly, 300 - One-off)",
     *          },{
     *              "name"          = "term",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Term of Investment (12, 18, 24, 36, 48, 60, 1000 - ongoing)",
     *          },{
     *              "name"          = "confirm_invest",
     *              "dataType"      = "boolean",
     *              "required"      =  true,
     *              "description"   = "Confirmation statement of invest details",
     *          },{
     *              "name"          = "risk_confirm",
     *              "dataType"      = "boolean",
     *              "required"      =  true,
     *              "description"   = "Confirmation of Understanding of Risks",
     *          },{
     *              "name"          = "accept_terms",
     *              "dataType"      = "boolean",
     *              "required"      =  true,
     *              "description"   = "Acceptance of WH terms and conditions",
     *          },{
     *              "name"          = "rate",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Rate. 100 - Secondary market, 200 - Fixed term payout",
     *          },{
     *              "name"          = "restrictions",
     *              "dataType"      = "array",
     *              "required"      =  false,
     *              "description"   = "Array of restrictions id -- [{'id':1}, {'id':2}]",
     *          },{
     *              "name"          = "priorities",
     *              "dataType"      = "array",
     *              "required"      =  false,
     *              "description"   = "Array of priorities id -- [{'id':1}, {'id':2}]",
     *          }
     *     }
     * )
     *
     * @param   Request $request
     * @param   InvestOffer $investOffer
     * @param   ConstraintViolationListInterface $validationErrors
     * @return  View
     *
     * @Post("/invest_offers")
     *
     * @ParamConverter("investOffer", converter="fos_rest.request_body")
     */
    public function postInvestOfferAction(Request $request, InvestOffer $investOffer, ConstraintViolationListInterface $validationErrors)
    {
        if (!$this->isGranted('create', $investOffer)) {
            return $this->getResponse([
                'code'    => 'investOffer.create.accessDenied',
                'message' => 'User has no rights to create invest offer'
            ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST);
        }
        if (count($validationErrors) > 0) {
            return $this->getValidationErrorResponse($validationErrors);
        }

        $investOffer
            ->setUser($this->getUser())
            ->setStatus(InvestOffer::STATUS_DRAFT);
        $this->get('whs.manager.invest_offer_manager')->save($investOffer, true);
        $quoteApplyManager = $this->get('whs.manager.quote_apply_manager');
        /**
         * If the quoteApply is approved, we should not change it`s status
         */
        $user = $this->getUser();
        if (!$quoteApplyManager->hasLenderApprovedQuote($user) && !$quoteApplyManager->hasLenderActiveQuote($user)) {
            /**
             * If the quoteApply is not approved, it should be draft and needs to change status
             */
            $qa = $quoteApplyManager->getDraftLenderQuoteApply($this->getUser());
            if (!$qa) {
                throw new QuoteApplyNotFoundException(QuoteApplyNotFoundException::DRAFT_QUOTE_APPLY_NOT_FOUND);
            }
            $qa->setStatus(QuoteApply::STATUS_FUNDING_REQUIRED);
            $quoteApplyManager->save($qa, false);
        }

        $process = new InvestOfferProcess();
        $process
            ->setInvestOffer($investOffer)
            ->setUser($this->getUser());
        $this->get('app.invest_offer_process_handler')->start($process, new SystemTransactionEvent());

        $this->getDoctrine()->getManager()->flush();

        $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $this->getCurrentUser(), UserActivityHandler::CONTEXT_ACTION => UserActivityLog::LENDER_PRODUCT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::CREATE_OFFER_ACTION]);

        return $this->getResponse([
            'id'         => $investOffer->getId(),
            'process_id' => $process->getId(),
            'status'     => $investOffer->getStatus()
        ]);
    }

    /**
     * This method patches InvestOffer. Created by Mikhail Pegasin <mpegasin@rambler.ru>.
     *
     * @ApiDoc(
     *     section="User/InvestOffer",
     *     parameters={
     *          {
     *              "name"          = "investment_type",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Investment type. 100 -ISA, 200 - SIPP, 300 - GEN",
     *          },{
     *              "name"          = "finance_type",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Finance type. 100 - Origin",
     *          },{
     *              "name"          = "investment_name",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Name of Investment Offer",
     *          },{
     *              "name"          = "investment_amount",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Investment Offer Amount",
     *          },{
     *              "name"          = "investment_frequency",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Investment Frequency (100 - Monthly, 200 - Quarterly, 300 - One-off)",
     *          },{
     *              "name"          = "term",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Term of Investment (12, 18, 24, 36, 60, Ongoing)",
     *          },{
     *              "name"          = "confirm_invest",
     *              "dataType"      = "boolean",
     *              "required"      =  true,
     *              "description"   = "Confirmation statement of invest details",
     *          },{
     *              "name"          = "risk_confirm",
     *              "dataType"      = "boolean",
     *              "required"      =  true,
     *              "description"   = "Confirmation of Understanding of Risks",
     *          },{
     *              "name"          = "accept_terms",
     *              "dataType"      = "boolean",
     *              "required"      =  true,
     *              "description"   = "Acceptance of WH terms and conditions",
     *          },{
     *              "name"          = "rate",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Rate. 100 - Secondary market, 200 - Fixed term payout",
     *          },{
     *              "name"          = "restrictions",
     *              "dataType"      = "array",
     *              "required"      =  false,
     *              "description"   = "Array of restrictions id -- [{'id':1}, {'id':2}]",
     *          },{
     *              "name"          = "priorities",
     *              "dataType"      = "array",
     *              "required"      =  false,
     *              "description"   = "Array of priorities id -- [{'id':1}, {'id':2}]",
     *          }
     *     }
     * )
     *
     * @param   Request $request
     * @param   InvestOffer $investOffer
     * @return  View
     *
     * @Rest\Patch("/invest_offers/{id}")
     *
     * @ParamConverter("investOffer", class="AppBundle:InvestOffer")
     */
    public function patchInvestOfferAction(Request $request, InvestOffer $investOffer)
    {
        $data = json_decode($request->getContent(), true);
        $investOfferArray = json_decode($this->get('serializer')->serialize($investOffer, 'json'), true);
        $newInvestOfferArray = array_merge($investOfferArray, $data);
        $newInvestOffer = $this->get('serializer')->deserialize(json_encode($newInvestOfferArray), InvestOffer::class, 'json');
        $this->get('whs.manager.invest_offer_manager')->save($newInvestOffer, true);
        return $this->getResponse($newInvestOffer);
    }

    /**
     *
     * @ApiDoc(
     *     section="User/InvestOffer",
     *     description="Return InvestOffer's restriction, if InfestOffer id specified (InvestOffer must belong to current user), otherwise returns all active restrictions. Created by Vladimir Glechikov",
     *     parameters={
     *      {"name"="id", "dataType"="string", "required"=false, "description"="id of invest offer"}
     *     }
     * )
     * @Get("/invest_offers_restrictions")
     * @param Request $request
     *
     * @return View
     */
    public function getOfferRestrictionsAction(Request $request)
    {
        $investOfferId = $request->query->get('id');
        $em = $this->getDoctrine()->getManager();
        if ($investOfferId) {
            if ($investOffer = $em->getRepository('AppBundle:InvestOffer')->findOneBy(['id' => $investOfferId])) {
                //todo: use voter to check access for user to investOffer
                if ($investOffer->getUser() != $this->getUser()) {
                    throw new AccessDeniedHttpException();
                }
                $properties['restrictions'] = $investOffer->getRestrictions();
                $properties['priorities'] = $investOffer->getPriorities();
                return $this->getResponse($properties);
            } else {
                throw new NotFoundHttpException('InvestOffer not found');
            }
        }
        $restrictionGroups = $em->getRepository('AppBundle:RestrictionGroup')->getAllWithRestrictions();
        return $this->getResponse(['restriction_groups' => $restrictionGroups]);
    }

    private function updateFromRequest(Request $request, $object)
    {
        $data = json_decode($request->getContent(), true);
        $objectArray = json_decode($this->get('serializer')->serialize($object, 'json'), true);
        $newObjectArray = array_merge($objectArray, $data);
        $newObject = $this->get('serializer')->deserialize(json_encode($newObjectArray), get_class($object), 'json');

        return $newObject;
    }

    /**
     * This method returns a BankDetails of the current user by id. Created by Mikhail Pegasin <mpegasin@rambler.ru>.
     *
     * @ApiDoc(
     *     section="User"
     * )
     *
     * @Get("/bank_details")
     *
     * @return BankDetails|object
     */
    public function getBankDetailsAction()
    {
        $bankDetails = $this->getDoctrine()->getManager()->getRepository('AppBundle:BankDetails')->findOneBy(['user' => $this->getUser(), 'archive' => false]);

        return $this->getResponse($bankDetails ?: []);
    }

    /**
     * Add Bank details for current user. Created by Vladimir Glechikov.
     *
     * @ApiDoc(
     *     section="User",
     *     parameters={
     *          {
     *              "name"          = "bank_sort_code",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Bank sort code. 6 digits",
     *          },{
     *              "name"          = "bank_account_number",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Bank account number. 12 digits",
     *          },{
     *              "name"          = "bank_account_name",
     *              "dataType"      = "string",
     *              "required"      =  false,
     *              "description"   = "Bank account name",
     *          }
     *     }
     * )
     *
     * @Post("/bank_details")
     *
     * @ParamConverter("bankDetails", converter="fos_rest.request_body")
     *
     * @param BankDetails $bankDetails
     * @param ConstraintViolationListInterface $validationErrors
     * @return View
     */
    public function addBankDetailsAction(BankDetails $bankDetails, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            return $this->getValidationErrorResponse($validationErrors);
        }

        $this->get('whs.manager.bank_details_manger')
            ->update($this->getUser(), $bankDetails->getBankSortCode(), $bankDetails->getBankAccountNumber(), $bankDetails->getBankAccountName(), true);
        $this->get('monolog.logger.user_activity')->info('',
            [
                UserActivityHandler::CONTEXT_USER   => $this->getUser(),
                UserActivityHandler::CONTEXT_ACTION => UserActivityLog::ACCOUNT_TYPE,
                UserActivityHandler::CONTEXT_CODE   => UserActivityLog::BANK_DETAILS_ACTION
            ]
        );

        return $this->getResponse([]);
    }

    /**
     * This method returns a BankDetails of the current user by id. Created by Mikhail Pegasin <mpegasin@rambler.ru>.
     *
     * @ApiDoc(
     *     section="User"
     * )
     *
     * @Get("/dashboard")
     *
     * @return View
     */
    public function getDashboardAction(Request $request)
    {
        $result['quotes'] = [];
        $result['loans'] = [];
        $result['offers'] = [];
        /**
         * Quotes and related Loans
         */
        $quoteApplyManager = $this->get('whs.manager.quote_apply_manager');
        $loanApplyManager = $this->get('whs.manager.loan_apply_manager');
        if ($qa = $quoteApplyManager->getQuoteApplies($this->getUser())) {
            foreach ($qa as $key => $item) {
                $result['quotes'][] = [
                    'quote_id'                  => $item->getId(),
                    'quote_status'              => $item->getStatus(),
                    'quote_product'             => $item->getProduct()->getType(),
                    'quote_product_description' => $item->getProduct()->getDescription(),
                    'quote_product_code'        => $item->getProduct()->getCode(),
                    'quote_datetime_update'     => $item->getUpdatedAt(),
                ];
                $loanApply = $loanApplyManager->getLoanApplyByQuote($item);
                if (!empty($loanApply)) {
                    $result['loans'][] = [
                        'loan_id'                  => $loanApply->getId(),
                        'loan_status'              => $loanApply->getStatus(),
                        'loan_product'             => $loanApply->getQuoteApply()->getProduct()->getType(),
                        'loan_product_description' => $loanApply->getQuoteApply()->getProduct()->getDescription(),
                        'loan_product_code'        => $loanApply->getQuoteApply()->getProduct()->getCode(),
                        'loan_datetime_update'     => $loanApply->getUpdatedAt(),
                    ];
                    $result['quotes'][$key]['loan_status'] = $loanApply->getStatus();
                }
            }
        }

        /**
         * InvestOffers
         */
        if ($io = $this->get('whs.manager.invest_offer_manager')->getAllInvestOffersForUser($this->getUser())) {
            /** @var InvestOffer $item */
            foreach ($io as $item) {
                $process = $this->getDoctrine()->getManager()->getRepository('AppBundle:InvestOfferProcess')->findOneBy(['investOffer' => $io]);
                $result['offers'][] = [
                    'offer_id'         => $item->getId(),
                    'offer_value'      => $item->getInvestmentAmount(),
                    'offer_rate'       => $item->getRate(),
                    'offer_type'       => $item->getInvestmentType(),
                    'offer_term'       => $item->getTerm(),
                    'offer_process_id' => $process ? $process->getId() : null,
                    'offer_status'     => $item->getStatus(),
                ];
            }
        }

        return $this->getResponse($result);
    }

    /**
     * Get QuoteIncome for current user. Created by Vladimir Glechikov.
     * @ApiDoc(
     *     section="User/Income",
     * )
     * @Get("/quote_income")
     */
    public function getQuoteIncomeAction()
    {
        $quoteIncome = $this->get('whs.manager.quote_income_manager')->getQuoteIncome($this->getUser());
        $result = $quoteIncome ?: [];
        return $this->getResponse($result);
    }

    /**
     * QuoteIncome can be added only for QuoteApply in the draft status. Add QuoteIncome for current user. Created by Vladimir Glechikov.
     *
     * @ApiDoc(
     *     section="User/Income",
     *     parameters={
     *      {"name"="total_gross_annual", "dataType"="string", "required"=true, "description"="Quote Total Gross Annual Income. From 1000 to 250 000"},
     *      {"name"="main_source", "dataType"="string", "required"=true, "description"="Quote Primary Income Source"},
     *      {"name"="is_homeowner", "dataType"="boolean", "required"=false, "description"="Is homeowner"},
     *      {"name"="has_c_c_j", "dataType"="boolean", "required"=false, "description"="Does user have County Court Judgment"},
     *      {"name"="has_current_default", "dataType"="boolean", "required"=false, "description"="Does user have current default"},
     *      {"name"="rate", "dataType"="string", "required"=false, "description"="How good does user at handing credit. 10 - poor, 20 - average, 30 - good, 40 - excellent"}
     *     }
     * )
     * @ParamConverter("quoteIncome", converter="fos_rest.request_body")
     *
     * @Post("/quote_income")
     *
     * @param Request $request
     * @param QuoteIncome $quoteIncome
     * @param ConstraintViolationListInterface $validationErrors
     * @return View
     */
    public function addQuoteIncomeAction(Request $request, QuoteIncome $quoteIncome, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            return $this->getValidationErrorResponse($validationErrors);
        }
        $this->get('whs.manager.quote_income_manager')->addQuoteIncome($quoteIncome, $this->getUser());
        return $this->getResponse([]);
    }

    /**
     * Returns UserIncome for current user. Works for Borrower.
     * If Borrower hasn't such record, method return *TotalIncomeGrossAnn* and *MainIncomeSource* from QuoteIncome for drafted Loan.
     * Created by Vladimir Glechikov.
     * @ApiDoc(
     *     section="User/Income",
     * )
     *
     * @Get("/user_income")
     */
    public function getUserIncomeAction()
    {
        $em = $this->getDoctrine()->getManager();
        $userIncome = $em->getRepository('AppBundle:UserIncome')->getLastForUser($this->getUser());

        if (!$userIncome) {
            $userIncome = new UserIncome();
            $quoteIncome = $em->getRepository('AppBundle:QuoteIncome')->getQuoteIncomeForDraftLoan($this->getUser());
            if ($quoteIncome) {
                $userIncome->setTotalIncomeGrossAnn($quoteIncome->getTotalGrossAnnual());
                $userIncome->setMainIncomeSource($quoteIncome->getMainSource());
            }
        }
        return $this->getResponse($userIncome);
    }

    /**
     * UserIncome can be added only for QuoteApply in the Approved status. Add UserIncome for current user. Created by Vladimir Glechikov.
     * @ApiDoc(
     *     section="User/Income",
     *     parameters={
     *          {
     *              "name"          = "total_income_gross_ann",
     *              "dataType"      = "integer",
     *              "required"      =  true,
     *              "description"   = "Total income gross annual",
     *          },{
     *              "name"          = "main_income_source",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Main income source",
     *          },{
     *              "name"          = "main_income_gross_ann",
     *              "dataType"      = "string",
     *              "required"      =  false,
     *              "description"   = "Main income gross annual",
     *          },{
     *              "name"          = "main_income_net_ann",
     *              "dataType"      = "string",
     *              "required"      =  false,
     *              "description"   = "Main income net annual",
     *          },{
     *              "name"          = "other_income_gross_ann",
     *              "dataType"      = "string",
     *              "required"      =  false,
     *              "description"   = "Main income gross annual",
     *          },{
     *              "name"          = "other_income_net_ann",
     *              "dataType"      = "string",
     *              "required"      =  false,
     *              "description"   = "Main income net annual",
     *          },{
     *              "name"          = "other_income_source",
     *              "dataType"      = "string",
     *              "required"      =  false,
     *              "description"   = "Other income source",
     *          },{
     *              "name"          = "spouse_income_gross_ann",
     *              "dataType"      = "string",
     *              "required"      =  false,
     *              "description"   = "Spouse income gross annual",
     *          },{
     *              "name"          = "spouse_income_net_ann",
     *              "dataType"      = "string",
     *              "required"      =  false,
     *              "description"   = "Spouse income net annual",
     *          },{
     *              "name"          = "total_net_income_monthly",
     *              "dataType"      = "string",
     *              "required"      =  false,
     *              "description"   = "Spouse income net annual",
     *          }
     *     }
     * )
     * @param Request $request
     * @param UserIncome $userIncome
     * @param ConstraintViolationListInterface $validationErrors
     *
     * @Post("/user_income")
     * @ParamConverter("userIncome", converter="fos_rest.request_body")
     *
     * @return View
     */
    public function addUserIncomeAction(Request $request, UserIncome $userIncome, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            return $this->getValidationErrorResponse($validationErrors);
        }

        $userIncome = $this->get('whs.manager.user_income_manager')->addUserIncome($userIncome, $this->getUser());
        $this->get('monolog.logger.user_activity')->info('',
            [
                UserActivityHandler::CONTEXT_USER   => $this->getUser(),
                UserActivityHandler::CONTEXT_ACTION => UserActivityLog::ACCOUNT_TYPE,
                UserActivityHandler::CONTEXT_CODE   => UserActivityLog::CUSTOMER_INCOME_ACTION
            ]
        );


        return $this->getResponse($userIncome);
    }

    /**
     * Get logs (and count of logs) for user by page number and limit of logs on the page. Created by Shatilenya Vladislav.
     *
     * @ApiDoc(
     *     section="User",
     *     parameters={
     *      {"name"="offset", "dataType"="string", "required"=true, "description"="Page number"},
     *      {"name"="limit", "dataType"="string", "required"=true, "description"="Amount of logs on the one page"},
     *     }
     * )
     *
     * @Get("/user_activity_logs")
     *
     * @param Request $request
     * @return View
     */
    public function getUserActivityLogsAction(Request $request)
    {
        $userManager = $this->get('whs.manager.user_manager');

        $logs = $userManager->getLogsForUser(
            $this->getUser(),
            $request->get('offset', 1),
            $request->get('limit', 10)
        );

        return $this->getResponse($logs);
    }

    /**
     * Borrower Accept Loan Agreement
     *
     * @ApiDoc(
     *     section="Borrower",
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="quote_id", "dataType"="string", "required"=true, "description"="Quote ID"},
     *          {"name"="payment_day", "dataType"="string", "required"=true, "description"="Payment Day 1-25"},
     *     }
     * )
     *
     * @return View
     * @POST("/confirmLoanAgreement")
     */
    public function setConfirmLoanAgreementAction(Request $request)
    {
        $quoteId = $request->request->get('quote_id');
        $em = $this->getDoctrine()->getManager();
        $payment_day = $request->request->get('payment_day');

        if (!in_array($payment_day, [1, 10, 15, 21, 26])) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.loanApplyNotFound',
                    'message' => 'Payment day should be from list (1, 10, 15, 21, 26)'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        /**
         * @var LoanApply $loanApply
         */
        $loanApply = $em->getRepository('AppBundle:LoanApply')->findOneBy(['quoteApply' => $quoteId]);


        if (empty($loanApply)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.loanApplyNotFound',
                    'message' => 'LoanApply for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        /**
         * @var Agreement $agreement
         */
        $agreement = $em->getRepository('AppBundle:Agreement')->findOneBy(['loanApply' => $loanApply]);

        if (empty($agreement)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.agreementNotFound',
                    'message' => 'Agreement for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $agreement->setAcceptanceDate(new \DateTime());
        $agreement->setPaymentDay($payment_day);
        $loanApply->setStatus(LoanApply::STATUS_MATCHING);
        $em->persist($loanApply);
        $em->persist($agreement);
        $em->flush();

        return $this->getResponse(null);
    }

    /**
     * Borrower Decline Loan Agreement
     *
     * @ApiDoc(
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Borrower",
     *     parameters={
     *          {"name"="quote_id", "dataType"="string", "required"=true, "description"="Quote ID"},
     *     }
     * )
     *
     * @return View
     * @POST("/declineLoanAgreement")
     */
    public function setDeclineLoanAgreementAction(Request $request)
    {
        $quoteId = $request->request->get('quote_id');
        $em = $this->getDoctrine()->getManager();

        /**
         * @var $loanApply LoanApply
         */
        $loanApply = $em->getRepository('AppBundle:LoanApply')->findOneBy(['quoteApply' => $quoteId]);


        if (empty($loanApply)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.loanApplyNotFound',
                    'message' => 'LoanApply for this quotation not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        $quoteApply = $loanApply->getQuoteApply();
        $quoteApply->setStatus(QuoteApply::STATUS_DECLINED);

        $loanApply->setStatus(LoanApply::STATUS_DECLINED);
        $em->persist($loanApply);
        $em->persist($quoteApply);
        $em->flush();

        return $this->getResponse(null);
    }

    /**
     * Based on the Quote ID get the Agreement and calculate Current Payment Period
     *
     * @ApiDoc(
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Payment",
     *     parameters={
     *          {"name"="period", "dataType"="string", "required"=true, "description"="Period"},
     *     }
     * )
     *
     * @Get("/payment/{quote_id}/period_payment_date")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getPeriodPaymentDateAction(QuoteApply $quoteApply, Request $request)
    {
        $loanApply = $this->get('whs.manager.loan_apply_manager')->getLoanApplyByQuote($quoteApply);
        /** @var Agreement $agreement */
        $agreement = $this->getDoctrine()->getRepository(Agreement::class)->getAgreementLoanApply($loanApply);

        $acceptanceDate = $agreement->getAcceptanceDate();

        if (!$acceptanceDate) {
            return $this->getResponse("Agreement isn't accepted", self::STATUS_ERROR, Response::HTTP_BAD_REQUEST);
        }

        $paymentDay = $agreement->getPaymentDay();
        $months = $request->get('period') - 1;
        $acceptanceDate->modify("+$months months");
        $acceptanceDate->modify('first day of next month');
        $paymentDay -= 1;
        $acceptanceDate->modify("+$paymentDay days");

        return $this->getResponse(['payment_date' => $acceptanceDate]);
    }

    /**
     * Based on the Quote ID get the Agreement and calculate Current period
     *
     * @ApiDoc(
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Payment"
     * )
     *
     * @Get("/payment/{quote_id}/current_period")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     * @return View
     */
    public function getCurrentPeriodAction(QuoteApply $quoteApply)
    {
        $loanApply = $this->get('whs.manager.loan_apply_manager')->getLoanApplyByQuote($quoteApply);
        /** @var Agreement $agreement */
        $agreement = $this->getDoctrine()->getRepository(Agreement::class)->getAgreementLoanApply($loanApply);

        $acceptanceDate = $agreement->getAcceptanceDate();

        if (!$acceptanceDate) {
            return $this->getResponse("Agreement isn't accepted", self::STATUS_ERROR, Response::HTTP_BAD_REQUEST);
        }

        $firstPaymentDay = $acceptanceDate->modify('first day of next month');
        $paymentDay = $agreement->getPaymentDay() - 1;
        $firstPaymentDay->modify("+$paymentDay days");

        $currentDate = new \DateTime();

        if ($firstPaymentDay->getTimestamp() > $currentDate->getTimestamp()) {
            return $this->getResponse(['period' => 1]);
        }

        $diff = $currentDate->diff($firstPaymentDay);
        //+2 because first payment has been passed
        $months = ($diff->format('%y') * 12) + $diff->format('%m') + 2;

        return $this->getResponse(['period' => $months]);
    }

    /**
     * Investor Portfolio Summary for current Investor
     *
     * @ApiDoc(
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Lender",
     * )
     *
     * @Get("/portfolioSummary")
     *
     * @return View
     */
    public function getPortfolioSummaryAction()
    {
        $response = [];
        /**
         * @var TransactionRepository $repository
         */
        $repository = $this->getDoctrine()->getRepository(Transaction::class);

        foreach (InvestOffer::getInvestmentTypesMap() as $k => $value) {

            $summary = new InvestorPortfolioSummary();
            $summary->setPortfolioAmount($repository->getPortfolioAmount($k, $this->getUser()));
            $summary->setCashAvailable($repository->getCashAvailable($k, $this->getUser()));
            $summary->setOfferPending($repository->getOfferPending($k, $this->getUser()));
            $summary->setInterestEarned($repository->getInterestEarned($k, $this->getUser()));
            $summary->setOutstandingLoans($repository->getOutstandingLoans($k, $this->getUser()));

            $response[$value] = $summary;
        }

        return $this->getResponse($response);
    }

    /**
     * Get loans details for user
     *
     * @ApiDoc(
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Borrower"
     * )
     *
     * @Get("/loans_details")
     *
     * @return View
     */
    public function getLoansDetailsAction()
    {
        //get current user
        $user = $this->getUser();

        $quotes = $this->get('whs.manager.quote_apply_manager')->getQuoteApplies($user);

        $result = [];
        /** @var QuoteApply $quote */
        foreach ($quotes as $quote) {
            $loanApply = $this->get('whs.manager.loan_apply_manager')->getLoanApplyByQuote($quote);

            if (empty($loanApply)) {
                continue;
            }

            /** @var Agreement $agreement */
            $agreement = $this->getDoctrine()->getRepository(Agreement::class)->findOneBy(['loanApply' => $loanApply]);

            if (empty($agreement)) {
                continue;
            }

            $paymentComputer = $this->get('app.utils.payments_computer');

            $creditValues = $paymentComputer->getCreditPaymentsTotal($agreement->getApr(), $agreement->getTerm(), $agreement->getAmount());

            $result[] = [
                'quote_id' => $quote->getId(),
                'loan_id' => $loanApply->getId(),
                'loan_amount' => $loanApply->getAmount(),
                'loan_term' => $loanApply->getTerm(),
                'activation_date' => $agreement->getAcceptanceDate(),
                'monthly_payment' => $agreement->getMonthlyRepayment(),
                'total_you_repay' => $creditValues['totalPaid'],
                'bonus_amount' => $agreement->getBonusAmount(),
                'bonus_rate' => $agreement->getBonusRate(),
                'contract_rate' => $agreement->getApr(),
                'status' => $loanApply->getStatus(),
                'broker_fee' => $agreement->getApr() - $agreement->getBonusRate(),
                'cost_of_credit' => $creditValues['totalInterest'],
                'payment_day' => $agreement->getPaymentDay()
            ];
        }

        return $this->getResponse($result);
    }

    /**
     * Get borrower opportunities for lender
     *
     * @ApiDoc(
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Investor"
     * )
     *
     * @Get("/borrower_opportunities")
     *
     * @return View
     */
    public function getBorrowerOpportunitiesAction()
    {
        $user = $this->getUser();

        /** @var LoanApplyManager $loanManager */
        $loanManager = $this->get('whs.manager.loan_apply_manager');

        return $this->getResponse($loanManager->getNotFullyMatchedLoans());
    }

    /**
     * Get instalments
     *
     * @ApiDoc(
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Instalment"
     * )
     *
     * @Get("/instalments")
     *
     * @return View
     */
    public function getInstalmentsAction()
    {
        $user = $this->getUser();

        $loanApplies = $this->getDoctrine()->getRepository(LoanApply::class)->findBy(['status' => LoanApply::STATUS_ACTIVE, 'user' => $user]);
        $instalmentRepository = $this->getDoctrine()->getRepository(Instalment::class);

        $result = [];

        foreach ($loanApplies as $loanApply)
        {
            $instalments = $instalmentRepository->findBy(['loanApply' => $loanApply]);

            /**
             * @var Instalment $instalment
             */
            foreach ($instalments as $instalment)
            {
                $row = [
                    'instalment_id' => $instalment->getId(),
                    'loan_apply_id' => $loanApply->getId(),
                    'period' => $instalment->getPeriod(),
                    'payment_date' => $instalment->getPaymentDate(),
                    'instalment' => $instalment->getInstalment(),
                    'interest' => $instalment->getInterest(),
                    'principle' => $instalment->getPrinciple(),
                    'balance' => $instalment->getBalance(),
                    'created_at' => $instalment->getCreatedAt(),
                    'paymentStatus' => $instalment->getPaymentStatus(),
                    'outstanding' => $instalment->getOutstanding(),
                    'allocation' => $instalment->getAllocation(),
                    'installmentStatus' => $instalment->getInstallmentStatus(),
                    'paymentAmount' => $instalment->getPaymentAmount(),
                ];

                array_push($result, $row);
            }

            $result = $this->get('whs.manager.instalments_manager')->generateFutureInstalments($loanApply, $result);
        }


        return $this->getResponse($result);
    }

    /**
     * Get instalments by offer
     *
     * @ApiDoc(
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Instalment",
     *     parameters={
     *          {"name"="agreement_id", "dataType"="string", "required"=true, "description"="Agreement ID"},
     *          {"name"="offer_id", "dataType"="string", "required"=true, "description"="Offer ID"},
     *     }
     * )
     *
     * @POST("/instalmentsByOffer")
     * @return View
     */
    public function getInstalmentsByOfferAction(Request $request)
    {
        $agreement_id = $request->request->get('agreement_id');
        $offer_id = $request->request->get('offer_id');
        $em = $this->getDoctrine();

        /**
         * @var Agreement $agreement
         */
        $agreement = $em->getRepository(Agreement::class)->find($agreement_id);

        if (empty($agreement)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.agreementNotFound',
                    'message' => 'Agreement by this ID not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }

        /**
         * @var InvestOffer $investOffer
         */
        $investOffer = $em->getRepository(InvestOffer::class)->find($offer_id);

        if (empty($investOffer)) {
            return $this->getResponse(
                [
                    'code'  => 'admin.viewBorrowers.investOfferNotFound',
                    'message' => 'InvestOffer by this ID not found'
                ], self::STATUS_ERROR, Response::HTTP_BAD_REQUEST
            );
        }


        $instalmentRepository = $em->getRepository(Instalment::class);

        $result = [];


        $instalments = $instalmentRepository->findBy(['loanApply' => $agreement->getLoanApply()]);


        $matchedSum = (int)$em->getRepository(ContractMatching::class)->getMatchedSumByAgreementOffer(
            $agreement,
            $investOffer
        );

        $offerPercent = $matchedSum / $agreement->getAmount();

        /**
         * @var Instalment $instalment
         */
        foreach ($instalments as $instalment)
        {
            $row = [
                'instalment_id' => $instalment->getId(),
                'loan_apply_id' => $agreement->getLoanApply()->getId(),
                'period' => $instalment->getPeriod(),
                'payment_date' => $instalment->getPaymentDate(),
                'instalment' => $instalment->getInstalment() * $offerPercent,
                'interest' => $instalment->getInterest() * $offerPercent,
                'principle' => $instalment->getPrinciple() * $offerPercent,
                'balance' => $instalment->getBalance() * $offerPercent,
                'created_at' => $instalment->getCreatedAt(),
                'paymentStatus' => $instalment->getPaymentStatus(),
                'outstanding' => $instalment->getOutstanding() * $offerPercent,
                'allocation' => $instalment->getAllocation() * $offerPercent,
                'installmentStatus' => $instalment->getInstallmentStatus(),
                'paymentAmount' => $instalment->getPaymentAmount() * $offerPercent,
            ];

            array_push($result, $row);
        }

        $result = $this->get('whs.manager.instalments_manager')->generateFutureInstalments($agreement->getLoanApply(), $result, $offerPercent);

        return $this->getResponse($result);
    }


    /**
     * Create investor Withdrawal
     *
     * @ApiDoc(
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Withdrawal",
     *     parameters={
     *          {"name"="amount", "dataType"="string", "required"=true, "description"="Amount"},
     *          {"name"="investment_type", "dataType"="string", "required"=true,
     *              "description"   = "Investment type. 100 -ISA, 200 - SIPP, 300 - GEN"},
     *     }
     * )
     *
     * @return View
     * @POST("/withdrawal/createInvestor")
     */
    public function createInvestorWithdrawalAction(Request $request)
    {
        return $this->createWithdrawal($request, Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR);
    }

    /**
     * Create borrower Withdrawal
     *
     * @ApiDoc(
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "required" = true
     *          }
     *     },
     *     section="Withdrawal",
     *     parameters={
     *          {"name"="amount", "dataType"="string", "required"=true, "description"="Amount"},
     *          {"name"="investment_type", "dataType"="string", "required"=true,
     *              "description"   = "Investment type. 100 -ISA, 200 - SIPP, 300 - GEN"},
     *     }
     * )
     *
     * @return View
     * @POST("/withdrawal/createBorrower")
     */
    public function createBorrowerWithdrawalAction(Request $request)
    {
        return $this->createWithdrawal($request, Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_BORROWER);
    }

    protected function createWithdrawal(Request $request, $glAccount)
    {
        $amount = $request->get('amount');
        $investment_type = $request->get('investment_type');

        if ($this->get('whs.manager.transaction_manager')->checkWithdrawal(
            $this->getUser(), $amount, $investment_type, $glAccount
        )) {

            if ($glAccount == Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR) {
                $owner = Transaction::TRANSACTION_OWNER_INVESTOR;
                $subAccount = Transaction::TRANSACTION_OWNER_TRUST_BANK_ACCOUNT_LENDERS;
            } else {
                $owner = Transaction::TRANSACTION_OWNER_BORROWER;
                $subAccount = Transaction::TRANSACTION_OWNER_TRUST_BANK_ACCOUNT_BORROWERS;
            }

            $this->get('event_dispatcher')->dispatch(TransactionEvents::WDRC,
                SystemTransactionEvent::create()
                    ->setUser($this->getUser())
                    ->addTransaction(
                        Transaction::create()
                            ->setUser($this->getUser())
                            ->setInvestmentType($investment_type)
                            ->setTransactionAmount($amount)
                            ->setTransactionOwner($owner)
                            ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_AVAILABLE)
                            ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_WITHDRAWAL_PENDING)
                            ->setGlAccount($glAccount)
                    )
                    ->addTransaction(
                        Transaction::create()
                            ->setUser($this->getUser())
                            ->setInvestmentType($investment_type)
                            ->setTransactionAmount(-1 * $amount)
                            ->setTransactionOwner($subAccount)
                            ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_WITHDRAW)
                            ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_CASH_AVAILABLE)
                            ->setGlAccount($glAccount)
                    )
            );
        }
    }
}