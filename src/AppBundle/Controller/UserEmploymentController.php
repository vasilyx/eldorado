<?php
namespace AppBundle\Controller;

use AppBundle\Entity\UserEmployment;
use AppBundle\Security\Voter\LoanStatusVoter;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use LogBundle\Entity\UserActivityLog;
use LogBundle\Handler\UserActivityHandler;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;


class UserEmploymentController extends ApiController
{

    /**
     * This method returns all User Employments of the current user. Created by Vladimir Glechikov
     *
     * @ApiDoc(
     *     section="User/Employment"
     * )
     * @Get("/employments")
     * @return View
     */
    public function getUserEmploymentAction()
    {
        return $this->getResponse($this->getDoctrine()->getRepository('AppBundle:UserEmployment')->getOrderedEmploymentsForUser($this->getUser()));
    }

    /**
     *
     * @ApiDoc(
     *     section="User/Employment",
     *     description="This method adds or updates (id specified) UserEmployment for current user. Created by Vladimir Glechikov.",
     *     parameters={
     *      {"name"="id", "dataType"="string", "required"=false, "description"="Id of UserEmployment (needed only for update)"},
     *      {"name"="occ_month_started", "dataType"="datetime", "required"=true, "description"="Occupation Start Month. SO8601 base format (Ymd)"},
     *      {"name"="occ_month_ended", "dataType"="datetime", "required"=false, "description"="Occupation End Month. SO8601 base format (Ymd)"},
     *      {"name"="business_firm_name", "dataType"="string", "required"=true, "description"="Business Firm's Name"},
     *      {"name"="industry", "dataType"="string", "required"=true, "description"="Industry or Profession"},
     *      {"name"="occ_postal_code", "dataType"="string", "required"=true, "description"="Business Post Code"},
     *      {"name"="occ_country", "dataType"="string", "required"=true, "description"="Business' Country"},
     *      {"name"="occ_status", "dataType"="string", "required"=true, "description"="Status at work"},
     *      {"name"="job_description", "dataType"="string", "required"=true, "description"="Job description"},
     *      {"name"="prof_qual", "dataType"="string", "required"=true, "description"="Professional qualification"},
     *      {"name"="current_status", "dataType"="boolean", "required"=true, "description"="Is this current work"},
     *     }
     * )
     * @Post("/employments")
     *
     * @param $request Request
     * @param $userEmployment UserEmployment
     * @param $validationErrors ConstraintViolationListInterface
     *
     * @return View
     *
     * @ParamConverter("userEmployment", converter="fos_rest.request_body")
     *
     */
    public function postUserEmploymentAction(Request $request, UserEmployment $userEmployment, ConstraintViolationListInterface $validationErrors)
    {
        if (!$this->isGranted(LoanStatusVoter::HAS_LOAN_DRAFTED)) {
            return $this->getResponse(LoanStatusVoter::LOAN_NOT_DRAFTED_CONTENT, self::STATUS_ERROR, Response::HTTP_FORBIDDEN);
        }
        if (count($validationErrors) > 0) {
            return $this->getValidationErrorResponse($validationErrors);
        }
        $em = $this->getDoctrine()->getManager();

        if ($userEmployment->getId()) {//update if have an id
            $em->detach($userEmployment);
            $oldUserEmployment = $this->getDoctrine()->getRepository('AppBundle:UserEmployment')->getActiveByIdAndUser($userEmployment->getId(), $this->getUser());
            if (!$oldUserEmployment) {
                throw new NotFoundHttpException('UserEmployment not found');
            }
            if ($oldUserEmployment != $userEmployment) {//if something changed
                $oldUserEmployment->setArchive(true);
                $em->persist($userEmployment);
                $em->flush();
            }
            return $this->getResponse($this->getDoctrine()->getRepository('AppBundle:UserEmployment')->getOrderedEmploymentsForUser($this->getUser()));
        }
        //else create
        $userEmployment->setUser($this->getUser());
        $em->persist($userEmployment);
        $em->flush();

        $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $this->getCurrentUser(), UserActivityHandler::CONTEXT_ACTION => UserActivityLog::ACCOUNT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::OCCUPATION_ACTION]);

        return $this->getResponse([]);
    }

    /**
     * @ApiDoc(
     *  section="User/Employment",
     *  description="This method adds UserEmployment for current user. Created by Vladimir Glechikov.",
     *  parameters={
     *     {"name"="id", "dataType"="string", "required"=true, "description"="Id of UserEmployment"},
     *  }
     * )
     * @Delete("/employments")
     */
    public function deleteEmploymentAction(Request $request)
    {
        if (!$this->isGranted(LoanStatusVoter::HAS_LOAN_DRAFTED)) {
            return $this->getResponse(LoanStatusVoter::LOAN_NOT_DRAFTED_CONTENT, self::STATUS_ERROR, Response::HTTP_FORBIDDEN);
        }
        $userEmployment = $this->getDoctrine()->getRepository('AppBundle:UserEmployment')->getActiveByIdAndUser($request->request->get('id'), $this->getUser());

        if (!$userEmployment) {
            throw new NotFoundHttpException('UserEmployment not found');
        }

        $userEmployment->setArchive(true);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
    }
}