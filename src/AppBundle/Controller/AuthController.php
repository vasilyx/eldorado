<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\Event;
use AppBundle\Entity\QuoteApply;
use AppBundle\Event\CallCreditEvent;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\View\View;
use LogBundle\Entity\UserActivityLog;
use LogBundle\Handler\UserActivityHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\Routing\RequestContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Contains Authentication actions
 *
 * @package AppBundle\Controller
 */
class AuthController extends ApiController
{
    /**
     * Try to login user by username and password (generates error response if login is not valid).
     * Checks for user's email is confirmed (generates error response if not).
     * PreAuthentication mechanism is not used.
     *
     * <pre>
     * If login is successful, method returns an array:
     * {
     *  "token": theTokenFromTheUserSession
     *  "user_status": ROLE_EMAIL_NOT_CONFIRMED | ROLE_USER
     * }
     *
     * </pre>
     * @ApiDoc(
     *  section="Auth",
     *  description="Login. Returns Auth token on success in element 'token' of the array ",
     *  parameters={
     *      {"name"="username", "dataType"="string", "required"=true, "description"="Username"},
     *      {"name"="password", "dataType"="string", "required"=true, "description"="Password"},
     *  }
     * )
     * @Post("/login")
     */
    public function loginAction(Request $request)
    {
        $um = $this->get('whs.manager.user_manager');
        try {
            $session = $um->login(
                $request->request->get('username'),
                $request->request->get('password')
            );
        } catch (\Exception $exception) {
            return $this->getResponse([
                'code' => 'user.badCredentials',
                'message' => 'Bad credentials'
            ], self::STATUS_ERROR, 400);
        }

        $user = $session->getUser();
        if (!$um->checkConfirmation($user)) {
            return $this->getResponse([
                'code' => 'user.emailNotConfirmed',
                'message' => 'Your email address has not confirmed. Please follow the instructions sent on the Registration Confirmation email.'
            ], self::STATUS_ERROR, 400);
        };

        $products = [];

        if ($approvedQuoteApplies = $this->get('whs.manager.quote_apply_manager')->getQuoteApplies($user)) {
            foreach ($approvedQuoteApplies as $approvedQuoteApply) {
                $products = array_unique(array_merge($products, [$approvedQuoteApply->getProduct()->getType()]));
            }
        };

        $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $user, UserActivityHandler::CONTEXT_CODE => UserActivityLog::LOGIN_ACTION, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::ACCOUNT_TYPE]);

        $result = [
            'token' => $session->getToken(),
            'user_status' => $um->resolveRoles($session->getUser())[0],
            'products' => $products
        ];

        return $result;
    }

    /**
     * The method is under the preAuthentication mechanism. If user token is not valid, the method returns 401 unauthorised error.
     *
     * @ApiDoc(
     *  section="Auth",
     *  description="Returns 200 response if the user is signed in and 401 response otherwise",
     *  parameters={
     *  }
     * )
     * @Get("/check")
     *
     * @return Response
     */
    public function checkAction()
    {
        return new Response(null);
    }

    /**
     * User logout.
     *
     * @ApiDoc(
     *  section="Auth",
     *  description="Logout. Return 200 if OK. Auth token must be removed",
     *  parameters={
     *  }
     * )
     * @Post("/logout")
     *
     * @param Request $request
     * @return Response
     */
    public function logoutAction(Request $request)
    {
        $user = $this->getUser();

        if ($user instanceof User) {
            $token = $user->getActiveApiToken();

            $this->getDoctrine()->getRepository('AppBundle:UserSession')->removeByToken($token);

            $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $user, UserActivityHandler::CONTEXT_CODE => UserActivityLog::LOGOUT_ACTION, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::ACCOUNT_TYPE]);
        }

        return new Response(null);
    }

    /**
     * Prepares a forgotten password token for the ResetPasswordAction and sends to user email a url for password resetting.
     *
     * @ApiDoc(
     *  section="Auth",
     *  description="Forgot password. Enter your username and check email",
     *  parameters={
     *      {"name"="username", "dataType"="string", "required"=true, "description"="Username"},
     *      {"name"="url", "dataType"="string", "required"=false, "description"="Url for reset password page"}
     *  }
     * )
     * @Post("/forgotten")
     * @param Request $request
     * @return View
     */
    public function forgottenAction(Request $request)
    {
        $user = $this->get('whs.manager.user_manager')->getUserByUsername(
            $request->request->get('username')
        );

        if (!$user instanceof User) {
            return $this->getResponse(null);
        }

        $this->get('whs.manager.user_manager')->updateForgotPasswordToken($user);

        $this->sendResetPasswordMail($request, $user, $request->get('url'));

        $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $user, UserActivityHandler::CONTEXT_CODE => UserActivityLog::USER_FORGOT_PASSWORD, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::ACCOUNT_TYPE]);

        return $this->getResponse(null);
    }

    /**
     * Takes a forgotten password token and sets new password for the user with the token. Should be called after forgottenAction().
     *
     * @ApiDoc(
     *  section="Auth",
     *  description="Reset password. Enter your new password",
     *  parameters={
     *      {"name"="password", "dataType"="string", "required"=true, "description"="Password"},
     *      {"name"="plain_password", "dataType"="string", "required"=true, "description"="Plain password"},
     *      {"name"="t", "dataType"="string", "required"=true, "description"="Parametr 't' in email"},
     *  }
     * )
     * @Post("/reset_password")
     * @ParamConverter("userEntity", converter="fos_rest.request_body", options={
     *      "mapping": {"t": "forgottenPasswordToken"},
     *      "validator"={"groups"={"reset"}}
     *  })
     *
     * @param Request $request
     * @param User $userEntity
     * @param ConstraintViolationListInterface $validationErrors
     *
     * @return View
     */
    public function resetPasswordAction(Request $request, User $userEntity, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            return $this->getValidationErrorResponse($validationErrors);
        }

        $userManager = $this->get('whs.manager.user_manager');
        $user = $userManager->getUserByForgottenPasswordToken(
            $request->request->get('t')
        );

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Invitation key is invalid');
        }

        $userManager
            ->confirmEmail($user)
            ->updatePassword(
            $user,
            $request->request->get('password')
        );

        $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $user, UserActivityHandler::CONTEXT_CODE => UserActivityLog::USER_RESET_PASSWORD, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::ACCOUNT_TYPE]);

//        $this->sendPasswordUpdatedMail($userEntity);

        return $this->getResponse(null);
    }

    /**
     * Confirms email by a token
     *
     * @ApiDoc(
     *  section="Auth",
     *  description="User email confirmation",
     *  parameters={
     *      {"name"="t", "dataType"="string", "required"=true, "description"="token"}
     *  }
     * )
     *
     * @Post("/email_confirmation")
     *
     * @param   Request     $request
     * @return  View
     */
    public function emailConfirmationAction(Request $request)
    {
        $user = $this->getDoctrine()
            ->getRepository(User::class)->findOneBy(['confirmEmailToken' => $request->request->get('t')]);

        $this->get('whs.manager.user_manager')->setUserEmailConfirmation(
            $request->request->get('t')
        );

        $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $user, UserActivityHandler::CONTEXT_CODE => UserActivityLog::EMAIL_VERIFIED_ACTION, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::ACCOUNT_TYPE]);

        return $this->getResponse(null);
    }


    /**
     * Sends email to user emailbox with url for resetting password. The url includes the user token only, without username.
     *
     * @param Request $request
     * @param User $userEntity
     * @param $url
     * @return int
     */
    private function sendResetPasswordMail(Request $request, User $userEntity, $url)
    {
        return $this->get('whs_mailer')->sendForgottenPassword(
            $userEntity->getUsername(), [
                'name'  => $userEntity->getUsername(),
                'link'  => $request->getSchemeAndHttpHost() . $url . '?t=' .
                    $userEntity->getForgottenPasswordToken()
            ]
        );
    }

    /**
     * @param User $userEntity
     * @return int
     */
    private function sendPasswordUpdatedMail(User $userEntity)
    {
        return $this->get('whs_mailer')->sendResetPassword(
            $userEntity->getUsername(),
            ['name'=> $userEntity->getUsername()]
        );
    }
}