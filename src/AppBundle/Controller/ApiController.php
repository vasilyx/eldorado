<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Class ApiController
 * @package AppBundle\Controller
 */
abstract class ApiController extends FOSRestController
{
    protected $container;

    /**
     * Error status for api response
     */
    const STATUS_ERROR = 'error';
    const STATUS_SUCCESS = 'success';

    /**
     * Get array of constraint violations messages. Prepares validation errors for api response.
     *
     * @param  ConstraintViolationListInterface $violations
     * @return array
     */
    protected function getConstraintViolations(ConstraintViolationListInterface $violations)
    {
        $errors = [];

        /* @var ConstraintViolation $violation */
        foreach ($violations as $violation) {
            $constraint = $violation->getConstraint();

            /*
             * We try to use payload property path before take it from getPropertyPath
             */
            $propertyPath = isset($violation->getConstraint()->payload['propertyPath']) ? $violation->getConstraint()->payload['propertyPath'] : $violation->getPropertyPath();

            $errors[$propertyPath] = [
                'code'    => isset($constraint->payload['code']) ? $constraint->payload['code'] : $violation->getMessage(),
                'message' => $violation->getMessage()
            ];
        }

        return $errors;
    }

    /**
     * Response view for request data validation violations
     *
     * @param  ConstraintViolationListInterface $violations
     * @return View
     */
    protected function getValidationErrorResponse(ConstraintViolationListInterface $violations)
    {
        return $this->getResponse($this->getConstraintViolations($violations), self::STATUS_ERROR, Response::HTTP_BAD_REQUEST);
    }

    /**
     * @deprecated Use $this->getUser() instead of this method
     * @return \AppBundle\Entity\User
     */
    protected function getCurrentUser()
    {
        return $this->getUser();
    }

    /**
     * Common Api response with content, status and code
     *
     * @param array|object $content Any content to response
     * @param string $status Status of the response: success or error
     * @param int $code Http code of the response
     * @return View FosRestBundle view
     * @internal param array $groups
     */
    public function getResponse($content, $status = self::STATUS_SUCCESS, $code = Response::HTTP_OK)
    {
        $view = View::create(['status'  => $status, 'content' => $content], $code);

        $view->setSerializationContext($this->getSerializationContext());

        return $view;
    }

    /*
     * Common Api response with content, status and code with serialization groups
     *
     * @param array|object $content Any content to response
     * @param string $status Status of the response: success or error
     * @param int $code Http code of the response
     * @return View FosRestBundle view
     * @internal param array $groups
     */
    public function getResponseWithSerializerGroups($content, $groups = [], $status = self::STATUS_SUCCESS, $code = Response::HTTP_OK)
    {
        $view = View::create(['status'  => $status, 'content' => $content], $code);

        $view->setSerializationContext($this->getSerializationContext($groups));

        return $view;
    }

    /**
     * @param array $groups
     *
     * @return SerializationContext
     */
    private function getSerializationContext($groups = [])
    {
        $context = SerializationContext::create()->enableMaxDepthChecks();

        if ($groups && count($groups)) {
            $context->setGroups($groups);
        }

        return $context;
    }
}