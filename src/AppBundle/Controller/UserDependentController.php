<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 09.12.2016
 * Time: 14:01
 */

namespace AppBundle\Controller;


use AppBundle\Entity\UserDependent;
use AppBundle\Security\Voter\LoanStatusVoter;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use LogBundle\Entity\UserActivityLog;
use LogBundle\Handler\UserActivityHandler;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserDependentController extends ApiController
{
    /**
     * This method create or update UserDependent. Created by Vladislav Shatilenya.
     *
     * @ApiDoc(
     *     section="User/Dependent",
     *     statusCodes={
     *      200="Success"
     *     },
     *     parameters={
     *         {"name"="first_name", "dataType"="string", "required"=true, "description"="First name"},
     *         {"name"="birthdate", "dataType"="datetime", "required"=true, "description"="Date of birth. ISO8601 base format  (Ymd)"}
     *     }
     * )
     * @Post("/dependent")
     * @ParamConverter("userDependent", converter="fos_rest.request_body")
     *
     * @param Request $request
     * @param UserDependent $userDependent
     *
     * @return View
     */
    public function postDependentAction(Request $request, UserDependent $userDependent)
    {
        if (!$this->isGranted(LoanStatusVoter::HAS_LOAN_DRAFTED)) {
            return $this->getResponse(LoanStatusVoter::LOAN_NOT_DRAFTED_CONTENT, self::STATUS_ERROR, Response::HTTP_FORBIDDEN);
        }
        $userDependent
            ->setUser($this->getCurrentUser())
            ->setArchive(false);

        $validator = $this->get('validator');
        $errors = $validator->validate($userDependent, null);

        /*
         * check if the validation errors occur for the UserDependent
         */
        if (count($errors) > 0) {
            return $this->getValidationErrorResponse($errors);
        }

        $dependentManager = $this->get('whs.manager.user_dependent_manager');
        $dependentManager->createUserDependent($userDependent);

        $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $this->getCurrentUser(), UserActivityHandler::CONTEXT_ACTION => UserActivityLog::ACCOUNT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::DEPENDENT_ACTION]);

        return $this->getResponse($dependentManager->getUserDependentList($this->getCurrentUser()));
    }

    /**
     * This method delete UserDependent by id. Created by Vladislav Shatilenya.
     *
     * @ApiDoc(
     *  section="User/Dependent",
     *  statusCodes={
     *     204="No Content",
     *     404="Dependent not found"
     *  },
     *  parameters={
     *     {"name"="id", "dataType"="string", "required"=true, "description"="ID of row"},
     *  }
     * )
     * @Delete("/dependent")
     *
     * @param Request $request
     * @return View
     */
    public function deleteDependentAction(Request $request)
    {
        if (!$this->isGranted(LoanStatusVoter::HAS_LOAN_DRAFTED)) {
            return $this->getResponse(LoanStatusVoter::LOAN_NOT_DRAFTED_CONTENT, self::STATUS_ERROR, Response::HTTP_FORBIDDEN);
        }
        return $this->getResponse($this->get('whs.manager.user_dependent_manager')->deleteDependentById($request->request->get('id')));
    }

    /**
     * This method return UserDependent list or one UserDependent by id. Created by Vladislav Shatilenya.
     *
     * @ApiDoc(
     *  section="User/Dependent",
     *  statusCodes={
     *     200="Success",
     *     404="Dependent not found"
     *  },
     *  parameters={
     *     {"name"="id", "dataType"="string", "required"=false, "description"="ID of row"}
     *  }
     * )
     * @Get("/dependent")
     *
     * @param Request $request
     * @return View
     *
     */
    public function getDependentAction(Request $request)
    {
        if ($id = $request->get('id')) {
            $response = $this->get('whs.manager.user_dependent_manager')->getDependentById($id);
        } else {
            $response = $this->get('whs.manager.user_dependent_manager')->getUserDependentList($this->getCurrentUser());
        }

        return $this->getResponse($response);
    }

    /**
     * This method update UserDependent by id. Created by Vladislav Shatilenya.
     *
     * @ApiDoc(
     *  section="User/Dependent",
     *  statusCodes={
     *     200="Success",
     *     404="Not Found"
     *  },
     *  parameters={
     *     {"name"="first_name", "dataType"="string", "required"=true, "description"="First name"},
     *     {"name"="birthdate", "dataType"="datetime", "required"=true, "description"="Date of birth. ISO8601 base format  (Ymd)"}
     *
     *  }
     * )
     *
     * @Patch("/dependent/{id}")
     *
     * @param Request $request
     * @param UserDependent $userDependent
     * @return View
     *
     * @ParamConverter("userDependent", class="AppBundle:UserDependent")
     */
    public function patchUserDependentAction(Request $request, UserDependent $userDependent)
    {
        if (!$this->isGranted(LoanStatusVoter::HAS_LOAN_DRAFTED)) {
            return $this->getResponse(LoanStatusVoter::LOAN_NOT_DRAFTED_CONTENT, self::STATUS_ERROR, Response::HTTP_FORBIDDEN);
        }
        $data = json_decode($request->getContent(), true);
        $userDependentArray = json_decode($this->get('serializer')->serialize($userDependent, 'json'), true);
        $newUserDependentArray = array_merge($userDependentArray, $data);
        $newUserDependent = $this->get('serializer')->deserialize(json_encode($newUserDependentArray), UserDependent::class, 'json');
        $this->get('whs.manager.user_dependent_manager')->save($newUserDependent, true);

        $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $this->getCurrentUser(), UserActivityHandler::CONTEXT_ACTION => UserActivityLog::ACCOUNT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::DEPENDENT_ACTION]);

        return $this->getResponse($newUserDependent);
    }

    /**
     * This method delete(**archive**) **all** UserDependents for current user. Created by Vladislav Shatilenya.
     *
     * @ApiDoc(
     *  section="User/Dependent",
     *  statusCodes={
     *     200="Success",
     *  },
     * )
     *
     * @Delete("/dependents")
     *
     * @return View
     *
     */
    public function deleteDependentsAction()
    {
        if (!$this->isGranted(LoanStatusVoter::HAS_LOAN_DRAFTED)) {
            return $this->getResponse(LoanStatusVoter::LOAN_NOT_DRAFTED_CONTENT, self::STATUS_ERROR, Response::HTTP_FORBIDDEN);
        }
        $this->get('whs.manager.user_dependent_manager')->deleteDependentsForUser($this->getCurrentUser());

        return $this->getResponse(null);
    }
}