<?php

namespace AppBundle\Controller;
use AppBundle\Entity\UserAddress;
use AppBundle\Manager\UserAddressManager;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use phpDocumentor\Reflection\DocBlock\Tag\UsesTag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Validator\ConstraintViolationListInterface;


class UserAddressController extends ApiController
{
    /**
     * @ApiDoc(
     *  section="Address",
     *  description="Get user address list",
     *  parameters={
     *  }
     * )
     * @Get("/address")
     */
    public function getAddressAction()
    {
        return $this->get('whs.manager.user_address_manager')->getAddressList($this->getUser());
    }

    /**
     * @ApiDoc(
     *  section="Address",
     *  description="Add user address",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=false, "description"="id for update"},
     *      {"name"="postal_code", "dataType"="string", "required"=true, "description"="PostalCode"},
     *      {"name"="address_number", "dataType"="string", "required"=false, "description"="value"},
     *      {"name"="city", "dataType"="string", "required"=false, "description"="City"},
     *      {"name"="street", "dataType"="string", "required"=false, "description"="Street"},
     *      {"name"="building_number", "dataType"="string", "required"=false, "description"="BuildingNumber"},
     *      {"name"="building_name", "dataType"="string", "required"=false, "description"="BuildingName"},
     *      {"name"="sub_building_name", "dataType"="string", "required"=false, "description"="subBuildingName"},
     *      {"name"="dependant_locality", "dataType"="string", "required"=false, "description"="dependantLocality"},
     *      {"name"="line1", "dataType"="string", "required"=false, "description"="line1"},
     *      {"name"="line2", "dataType"="string", "required"=false, "description"="line2"},
     *      {"name"="country", "dataType"="string", "required"=false, "description"="CountryName"},
     *      {"name"="region", "dataType"="string", "required"=false, "description"="Region"},
     *      {"name"="state", "dataType"="string", "required"=false, "description"="State"},
     *      {"name"="moved", "dataType"="datetime", "required"=true, "description"="Moved (month and year). ISO8601 base format  (Ymd)"},
     *  }
     * )
     * @Post("/address")
     * @ParamConverter("userAddress", converter="fos_rest.request_body")
     *
     * @param Request $request
     * @param UserAddress $userAddress
     *
     * @return View
     */
    public function addAddressAction(Request $request, UserAddress $userAddress)
    {
        $userAddress
            ->setUser($this->getCurrentUser())
            ->setArchive(false)
        ;
        /*
         * We can not use the validationErrors variable because at the desiralization moment
         * the field user is null and unique address constraint always ok
         */
        $validator = $this->get('validator');

        $errors = $validator->validate($userAddress, null);

        /*
         * check if the validation errors occur for the UserAddress
         */
        if (count($errors) > 0) {
            return $this->getValidationErrorResponse($errors);
        }

        $userAddressManager = $this->get('whs.manager.user_address_manager');
        if ($userAddress->getId()) {
            $userAddressManager->updateInTransaction($userAddressManager->updateAddress($userAddress));
        } else {
            $userAddressManager->createAddress($userAddress);
        }

        return $userAddressManager->getAddressList($this->getCurrentUser());
    }


    /**
     * @ApiDoc(
     *  section="Address",
     *  description="Delete user address",
     *  parameters={
     *     {"name"="id", "dataType"="string", "required"=true, "description"="ID of row"},
     *  }
     * )
     * @Delete("/address")
     */
    public function deleteAddressAction(Request $request)
    {
        return $this->get('whs.manager.user_address_manager')->deleteAddressById($request->request->get('id'));
    }


    /**
     * @ApiDoc(
     *  section="Address",
     *  description="Get current address",
     *  parameters={
     *  }
     * )
     * @Get("/address_current")
     */
    public function getCurrentAddressAction()
    {
        return $this->get('whs.manager.user_address_manager')->getCurrentAddress($this->getCurrentUser());
    }
}
