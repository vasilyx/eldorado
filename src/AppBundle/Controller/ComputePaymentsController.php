<?php
namespace AppBundle\Controller;

use AppBundle\Entity\QuoteTerm;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;

class ComputePaymentsController extends ApiController
{
    /**
     *
     * @ApiDoc(
     *     section="ComputePayments",
     *     description="This method takes loan_term, loan_amount and returns interest_rate, monthly_payment, total_payment. Created by Vladimir Glechikov.",
     *     parameters={
     *      {"name"="loan_term", "dataType"="string", "required"=true, "description"="loanTerm"},
     *      {"name"="loan_amount", "dataType"="string", "required"=true, "description"="loanAmount"},
     *     }
     * )
     * @Get("/base_loan_properties")
     * @param $request Request
     * @return View
     */
    public function getBaseLoanPropertiesAction(Request $request)
    {
        $loanTerm = $request->get('loan_term');
        $loanAmount = $request->get('loan_amount');

        // only for validation
        $quoteTerm = new QuoteTerm();
        $quoteTerm->setTerm($loanTerm);
        $quoteTerm->setAmount($loanAmount);

        $validationErrors = $this->get('validator')->validate($quoteTerm);
        if (count($validationErrors) > 0) {
            return $this->getValidationErrorResponse($validationErrors);
        }

        $paymentsComputer = $this->get("app.utils.payments_computer");
        $baseProperties = $paymentsComputer->getBaseLoanProperties($loanTerm, $loanAmount);

        return $this->getResponse($baseProperties);
    }

    /**
     * Method receives **loan_amount** and returns array of total payment values for all terms from 12 to 60 month.
     * <pre>
     * <b>array example</b>
     * </br>
     * [
     *  {
     *      "contract_monthly_payment": 879.53,
     *      "contract_total_paid": 10554.37,
     *      "contract_total_interest": 554.37,
     *      "effective_total_interest": 456.93,
     *      "loan_term": 12
     *  },
     *  {
     *      "contract_monthly_payment": 815.22,
     *      "contract_total_paid": 10597.84,
     *      "contract_total_interest": 597.84,
     *      "effective_total_interest": 492.64,
     *      "loan_term": 13
     *      },
     *  ...
     *  {
     *      "contract_monthly_payment": 212.86,
     *      "contract_total_paid": 12771.86,
     *      "contract_total_interest": 2771.86,
     *      "effective_total_interest": 2260.82,
     *      "loan_term": 60
     *  }
     * ]
     * </pre>
     *
     * @ApiDoc(
     *     section="ComputePayments",
     *     description="Method receives loan_amount and returns array of total payment values for all terms from 12 to 60 month. Created by Vladimir Glechikov.",
     *     parameters={
     *      {"name"="loan_amount", "dataType"="string", "required"=true, "description"="loanAmount"}
     *     }
     * )
     * @Get("/all_terms_payment_values")
     * @param $request Request
     * @return View
     */
    public function getAllTermsPaymentValuesAction(Request $request)
    {
        $loanAmount = $request->get('loan_amount');
        $user = $this->getUser();

        $quoteTerm = $this->getDoctrine()->getRepository('AppBundle:QuoteTerm')->getQuoteTermForDraftLoan($user);

        $paymentsComputer = $this->get('app.utils.payments_computer');

        $allTermPaymentsArr = [];
        for ($loanTerm = $paymentsComputer->getMinLoanTerm(); $loanTerm <= $paymentsComputer->getMaxLoanTerm(); $loanTerm++) {
            $paymentContractDetails = $paymentsComputer->getCreditPaymentsTotal($quoteTerm->getContractRate(), $loanTerm, $loanAmount);
            $row['contract_monthly_payment'] = $paymentContractDetails['monthlyPayment'];
            $row['contract_total_paid'] = $paymentContractDetails['totalPaid'];
            $row['contract_total_interest'] = $paymentContractDetails['totalInterest'];

            $paymentEffectiveDetails = $paymentsComputer->getCreditPaymentsTotal($quoteTerm->getEffectiveRate(), $loanTerm, $loanAmount);
            $row['effective_total_interest'] = $paymentEffectiveDetails['totalInterest'];
            $row['loan_term'] = $loanTerm;
            $allTermPaymentsArr[] = $row;
        }
        //recursive rounding returning values
        array_walk_recursive(
            $allTermPaymentsArr,
            function (&$value) {
                $value = round($value, 2);
            }
        );
        return $this->getResponse($allTermPaymentsArr);
    }
}