<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Communication;
use AppBundle\Entity\UserCommunication;
use AppBundle\Entity\UserDetails;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use LogBundle\Entity\UserActivityLog;
use LogBundle\Handler\UserActivityHandler;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class UserCommunicationController
 * @package AppBundle\Controller
 */
class UserCommunicationController extends ApiController
{
    /**
     * @ApiDoc(
     *  section="Communication",
     *  description="Check verification code"
     * )
     * @Post("/receive_verification_code")
     *
     * @param   Request $request
     * @return View
     */
    public function receiveVerificationCodeAction(Request $request)
    {
        $ucm = $this->get('whs.manager.user_communication_manager');
        $code = $ucm->saveUserCommunicationVerificationCode($this->getUser());

        return $this->getResponse(['code' => $code]);
    }

    /**
     * @ApiDoc(
     *  section="Communication",
     *  description="Check verification code",
     *  parameters={
     *       {"name"="code", "dataType"="string", "required"=true, "description"="Verification code"},
     *  }
     * )
     * @Post("/check_verification_code")
     *
     * @param   Request $request
     * @return View
     */
    public function checkVerificationCodeAction(Request $request)
    {
        $code = $request->get('code');
        if (null === $code) {
            return $this->getResponse([
                'code'    => 'verified_code.isNull',
                'message' => 'Verified code is null.'
            ], self::STATUS_ERROR, Response::HTTP_NOT_FOUND);
        }

        $ucm = $this->get('whs.manager.user_communication_manager');
        /** @var UserCommunication $userCommunication */
        if (!$userCommunication = $ucm->getPrimaryUnverifiedMobile($this->getUser())) {
            return $this->getResponse([
                'code'    => 'mobile_communication.no.primary.unverified',
                'message' => 'You have no unverified Primary mobile Number',
            ], self::STATUS_ERROR);
        }

        if ($userCommunication->getVerificationCode() == $code) {
            $userCommunication
                ->setVerificationCode(null)
                ->setIsVerified(true)
                ->setIsPrimary(true);

            $ucm->save($userCommunication, true);

            $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $userCommunication->getUser(), UserActivityHandler::CONTEXT_CODE => UserActivityLog::MOBILE_VERIFIED_ACTION, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::ACCOUNT_TYPE]);

            return $this->getResponse(self::STATUS_SUCCESS);
        }

        return $this->getResponse([
            'code'    => 'verified_code.isNull',
            'message' => 'Verified code has not found.'
        ], self::STATUS_ERROR, Response::HTTP_NOT_FOUND);
    }

    /**
     * This method saves data from two entities: UserDetails and UserCommunications. Created by Mikhail Pegasin <mpegasin@rambler.ru>.
     *
     * @ApiDoc(
     *     section="Communication",
     *     parameters={
     *          {
     *              "name"          = "communications",
     *              "dataType"      = "array",
     *              "required"      =  true,
     *              "format"        = "{type(string):value(string)}. Available communication type codes: 100 - skype, 200 - mobile, 300 - home phone, 400 - work phone, 500 - linkedIn, 600 - facebook",
     *              "description"   = "An array of communications",
     *          },{
     *              "name"          = "title",
     *              "dataType"      = "string",
     *              "required"      =  false,
     *              "description"   = "Title",
     *          },{
     *              "name"          = "screen_name",
     *              "dataType"      = "string",
     *              "required"      =  false,
     *              "description"   = "Screen name",
     *          },
     *     }
     * )
     *
     * @param   Request $request
     * @return  View
     *
     * @Post("/add_investor_contacts")
     */
    public function addInvestorContactsAction(Request $request)
    {
        $communications = $request->get('communications', null);

        if (null === $communications) {
            return $this->getResponse([
                'code'    => 'communications.isBlank',
                'message' => 'No communications in request'
            ], self::STATUS_ERROR, 404);
        }

        $errorMessageTemplate = '%type% should not be blank';
        $errorCodeTemplate = '%type%.isBlank';

        $typesMap = [
            'mobile_phone' => Communication::TYPE_MOBILE_PHONE,
            'home_phone'   => Communication::TYPE_HOME_PHONE,
            'work_phone'   => Communication::TYPE_WORK_PHONE,
            'skype'        => Communication::TYPE_SKYPE,
            'facebook'     => Communication::TYPE_FACEBOOK,
            'linkedIn'     => Communication::TYPE_LINKED_IN,
            'email'        => Communication::TYPE_EMAIL,
        ];

        $flippedTypesMap = array_flip($typesMap);

        $ucm = $this->get('whs.manager.user_communication_manager');
        foreach ($communications as $type => $value) {
            if (!in_array($type, array_keys(Communication::getCommunicationTypes()))) {
                return $this->getResponse([
                    'code'    => 'communication.type.notAvailable',
                    'message' => 'Bad communication type. Available types: ' . implode(", ", array_keys(Communication::getCommunicationTypes()))
                ]);
            }
            if (!$value) {
                return $this->getResponse([
                    'code'    => str_replace('%type%', $flippedTypesMap[$type], $errorCodeTemplate),
                    'message' => str_replace('%type%', ucfirst(Communication::getCommunicationTypes()[$type]), $errorMessageTemplate)
                ], self::STATUS_ERROR, 400);
            }

            $ucm->createUserCommunication($this->getUser(), $type, $value);

            $this->get('monolog.logger.user_activity')->info('',
                [
                    UserActivityHandler::CONTEXT_USER     => $this->getCurrentUser(),
                    UserActivityHandler::CONTEXT_CODE     => UserActivityLog::COMMUNICATION_ADDED_ACTION,
                    UserActivityHandler::CONTEXT_ACTION   => UserActivityLog::ACCOUNT_TYPE,
                    UserActivityHandler::CONTEXT_METADATA => json_encode(['type' => $type]),
                ]
            );
        }

        $udm = $this->get('whs.manager.user_details_manager');
        $serializer = $this->get('serializer');
        /**
         * An object from the database
         * @var UserDetails $userDetails
         */
        $userDetails = $udm->getDetailsForUser($this->getUser());
        /**
         * Get json_decode array from serialized object from the database
         * @var array $userDetailsFromJson
         */
        $userDetailsFromJson = json_decode($serializer->serialize($userDetails, 'json'), true);
        /**
         * Get json_decode array from serialized object from the request
         * @var array $userDetailsFromJson
         */
        $inputUserDetailsFromJson = json_decode($serializer->serialize($serializer->deserialize($request->getContent(), UserDetails::class, 'json'), 'json'), true);
        $resultArray = array_merge($userDetailsFromJson, $inputUserDetailsFromJson);
        $userDetails = $serializer->deserialize(json_encode($resultArray), UserDetails::class, 'json');
        $udm->persist($userDetails);

        $this->getDoctrine()->getManager()->flush();

        return $this->getUserCommunicationsAction();
    }

    /**
     * This method returns current user communications. Created by Mikhail Pegasin <mpegasin@rambler.ru>.
     *
     * @ApiDoc(
     *     section="Communication"
     * )
     *
     * @return  View
     *
     * @Get("/investor_communications")
     */
    public function getUserCommunicationsAction()
    {
        $ucm = $this->get('whs.manager.user_communication_manager');
        $userCommunications = $ucm->getCommunicationList($this->getUser());
        return $this->getResponse($userCommunications);
    }

}