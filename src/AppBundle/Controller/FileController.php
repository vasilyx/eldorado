<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 04.06.2018
 * Time: 0:41
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Agreement;
use AppBundle\Entity\File;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\QuoteApply;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class FileController extends ApiController
{
    /**
     * This method generates Acceptance Agreement HTML template with all data for the Borrower
     *
     * @ApiDoc(
     *     section="File",
     *     description="This method generates Acceptance Agreement HTML template with all data for the Borrower by Quote ID",
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "description" = "Must content the next string: 'whsAToken'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="quote_id", "dataType"="string", "required"=true, "description"="Quote ID"},
     *     }
     * )
     *
     * @Get("/{quote_id}/acceptance_agreement")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     *
     * @return View
     */
    public function getAcceptanceAgreementAction(QuoteApply $quoteApply)
    {
        if ($this->getUser() != $quoteApply->getUser()) {
            return $this->getResponse('Quote not found for this user', self::STATUS_ERROR);
        }

        $loanApply = $this->get('whs.manager.loan_apply_manager')->getLoanApplyByQuote($quoteApply);

        $data = $this->get('whs.manager.file_manager')->getDataForBorrowerContractFile($loanApply, File::ACCEPTANCE_BORROWER_AGREEMENT_TYPE);

        if (!$data) {
            return $this->getResponse('Agreement not found for this user');
        }

        return $this->getResponse($this->renderView('@App/pdf/agreements/agreement.html.twig', $data));
    }


    /**
     * This method return binary of contract **PDF** file by loan_apply_id / ivest_offer_id
     *
     * @ApiDoc(
     *     section="File",
     *     description="This method returns binary of contract PDF file",
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "description" = "Must content the next string: 'whsAToken'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="invest_offer_id", "dataType"="string", "required"=false, "description"="Invest offer ID"},
     *     }
     * )
     *
     * @Get("/contract/{loan_apply_id}/pdf")
     * @ParamConverter("loanApply", options={"mapping": {"loan_apply_id" : "id"}})
     *
     *
     * @param LoanApply $loanApply
     * @param Request $request
     * @return Response
     */
    public function getContractPdfAction(LoanApply $loanApply, Request $request)
    {
        $user = $loanApply->getUser();

        if (!$user->getActive()) {
            return $this->getResponse('Borrower isn\'t active', self::STATUS_ERROR);
        }

        if (!in_array($loanApply->getStatus(), [LoanApply::STATUS_ACTIVE, LoanApply::STATUS_SETTLED])) {
            return $this->getResponse('LoanApply hasn\'t active or settled status', self::STATUS_ERROR);
        }

        $investOfferId = $request->get('invest_offer_id');

        $investOffer = $investOfferId ? $this->getDoctrine()->getRepository(InvestOffer::class)->find($investOfferId) : null;

        if (!empty($investOffer) && !$investOffer->getUser()->getActive()) {
            return $this->getResponse('Lender isn\'t active', self::STATUS_ERROR);
        }

        $file = $this->get('whs.manager.file_manager')->getPdfFile($loanApply, $investOffer, File::ACCEPTANCE_BORROWER_AGREEMENT_TYPE);

        if (empty($file)) {
            return $this->getResponse('Your contract is not ready yet', self::STATUS_ERROR, Response::HTTP_NOT_FOUND);
        }

        return $this->getPdfResponse($file);
    }

    /**
     * This method generates Acceptance Agreement **PDF** template with all data for the Borrower
     *
     * @ApiDoc(
     *     section="File",
     *     description="This method generates Acceptance Agreement **PDF** template with all data for the Borrower by Quote ID",
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "description" = "Must content the next string: 'whsAToken'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="quote_id", "dataType"="string", "required"=true, "description"="Quote ID"},
     *     }
     * )
     *
     * @Get("/pdf/{quote_id}/acceptance_agreement")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     *
     * @return Response
     */
    public function getAcceptanceAgreementPDFAction(QuoteApply $quoteApply)
    {
        if ($this->getUser() != $quoteApply->getUser()) {
            return $this->getResponse('Quote not found for this user', self::STATUS_ERROR);
        }

        $file = $this->get('whs.manager.file_manager')->getPdfFileByQuoteAndType($quoteApply, File::ACCEPTANCE_BORROWER_AGREEMENT_TYPE);

        if (empty($file)) {
            return $this->getResponse('Your contract is not ready yet', self::STATUS_ERROR, Response::HTTP_NOT_FOUND);
        }

        return $this->getPdfResponse($file);
    }

    /**
     * This method generates Signing Agreement HTML template with all data for the Borrower
     *
     * @ApiDoc(
     *     section="File",
     *     description="This method generates Signing Agreement HTML template with all data for the Borrower by Quote ID",
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "description" = "Must content the next string: 'whsAToken'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="quote_id", "dataType"="string", "required"=true, "description"="Quote ID"},
     *     }
     * )
     *
     * @Get("/{quote_id}/signing_agreement")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     *
     * @return View
     */
    public function getSigningAgreementAction(QuoteApply $quoteApply)
    {
        if ($this->getUser() != $quoteApply->getUser()) {
            return $this->getResponse('Quote not found for this user', self::STATUS_ERROR);
        }

        $loanApply = $this->get('whs.manager.loan_apply_manager')->getLoanApplyByQuote($quoteApply);

        $data = $this->get('whs.manager.file_manager')->getDataForBorrowerContractFile($loanApply, File::FINAL_BORROWER_AGREEMENT_TYPE);

        if (!$data) {
            return $this->getResponse('Agreement not found for this user');
        }

        return $this->getResponse($this->renderView('@App/pdf/agreements/agreement.html.twig', $data));
    }

    /**
     * This method generates Signing Agreement **PDF** with all data for the Borrower
     *
     * @ApiDoc(
     *     section="File",
     *     description="This method generates Signing Agreement PDF with all data for the Borrower by Quote ID",
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "description" = "Must content the next string: 'whsAToken'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="quote_id", "dataType"="string", "required"=true, "description"="Quote ID"},
     *     }
     * )
     *
     * @Get("/pdf/{quote_id}/signing_agreement")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     *
     * @return Response
     */
    public function getSigningAgreementPDFAction(QuoteApply $quoteApply)
    {
        if ($this->getUser() != $quoteApply->getUser()) {
            return $this->getResponse('Quote not found for this user', self::STATUS_ERROR);
        }

        $file = $this->get('whs.manager.file_manager')->getPdfFileByQuoteAndType($quoteApply, File::FINAL_BORROWER_AGREEMENT_TYPE);

        if (empty($file)) {
            return $this->getResponse('Your contract is not ready yet', self::STATUS_ERROR, Response::HTTP_NOT_FOUND);
        }

        return $this->getPdfResponse($file);
    }

    /**
     * This method generates Lender Agreement HTML template with all data
     *
     * @ApiDoc(
     *     section="File",
     *     description="This method generates Lender Agreement HTML template with all data by Quote ID",
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "description" = "Must content the next string: 'whsAToken'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="quote_id", "dataType"="string", "required"=true, "description"="Quote ID"},
     *     }
     * )
     *
     * @Get("/{invest_offer_id}/investor_agreement")
     * @ParamConverter("investOffer", options={"mapping": {"invest_offer_id" : "id"}})
     *
     * @param InvestOffer $investOffer
     *
     * @return View
     */
    public function getInvestorAgreementAction(InvestOffer $investOffer)
    {
        if ($this->getUser() != $investOffer->getUser()) {
            return $this->getResponse('Invest offer not found for this user', self::STATUS_ERROR);
        }

        $data = $this->get('whs.manager.file_manager')->getDataForLenderContractFile($investOffer, File::LENDER_AGREEMENT_TYPE);

        if (!$data) {
            return $this->getResponse('Contracts not found for this user');
        }

        return $this->getResponse($this->renderView('@App/pdf/agreements/agreement_lender.html.twig', $data));
    }

    /**
     * This method generates Lender Agreement **PDF** with all data
     *
     * @ApiDoc(
     *     section="File",
     *     description="This method generates Lender Agreement PDF with all data by Quote ID",
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "description" = "Must content the next string: 'whsAToken'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="quote_id", "dataType"="string", "required"=true, "description"="Quote ID"},
     *     }
     * )
     *
     * @Get("/pdf/{quote_id}/investor_agreement")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     *
     * @param QuoteApply $quoteApply
     *
     * @return Response
     */
    public function getLenderAgreementPDFAction(QuoteApply $quoteApply)
    {
        if ($this->getUser() != $quoteApply->getUser()) {
            return $this->getResponse('Quote not found for this user', self::STATUS_ERROR);
        }

        $file = $this->get('whs.manager.file_manager')->getPdfFileByQuoteAndType($quoteApply, File::LENDER_AGREEMENT_TYPE);

        if (empty($file)) {
            return $this->getResponse('Your contract is not ready yet', self::STATUS_ERROR, Response::HTTP_NOT_FOUND);
        }

        return $this->getPdfResponse($file);
    }

    /**
     * This method returns PDF response
     *
     * @param File $file
     * @return Response
     */
    protected function getPdfResponse(File $file)
    {
        $path = $this->get('kernel')->getRootDir() . $file->getLocation();

        $response = new BinaryFileResponse($path);

        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();

        if($mimeTypeGuesser->isSupported()){
            // Guess the mimetype of the file according to the extension of the file
            $response->headers->set('Content-Type', $mimeTypeGuesser->guess($path));
        } else{
            // Set the mimetype of the file manually, in this case for a text file is text/plain
            $response->headers->set('Content-Type', 'application/pdf');
        }

        // Set content disposition inline of the file
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'file.pdf'
        );

        return $response;
    }
}