<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 12.12.2016
 * Time: 19:09
 */

namespace AppBundle\Controller;


use AppBundle\Entity\UserCreditline;
use AppBundle\Security\Voter\LoanStatusVoter;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use LogBundle\Entity\UserActivityLog;
use LogBundle\Handler\UserActivityHandler;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class UserCreditlineController extends ApiController
{

    /**
     * This method create new UserCreditline. Created by Vladislav Shatilenya.
     *
     * @ApiDoc(
     *     section="User/Creditline",
     *     statusCodes={
     *      200="Success",
     *      400="Bad request (wrong request parameters)",
     *      403="Forbidden (unconfirmed user \ need sign in)"
     *     },
     *     parameters={
     *        {"name"="id", "dataType"="string", "required"=false, "description"="Id of UserCreditline (needed only for update)"},
     *        {
     *              "name"        =  "type",
     *              "dataType"    =  "string",
     *              "required"    =   true,
     *              "description" =  "Type of credit (100 - Credit card, 200 - Personal loan, 300 - Store card, 400 - Mortgage,
    500 - Car finance, 600 - Hire Purchase, 700 - Overdraft, 800 - Payday, 900 - Friends & Family"
     *        },{
     *              "name"        =   "name",
     *              "dataType"    =   "string",
     *              "required"    =    true,
     *              "description" =   "Name of Creditor/Lender"
     *        },{
     *              "name"        =    "credit_limit",
     *              "dataType"    =    "string",
     *              "required"    =    true,
     *              "description" =    "Credit limit"
     *        },{
     *              "name"        =    "balance_input",
     *              "dataType"    =    "string",
     *              "required"    =     true,
     *              "description" =     "Credit balance"
     *        },{
     *              "name"        =    "apr",
     *              "dataType"    =    "string",
     *              "required"    =     true,
     *              "description" =    "Average percentage rate (APR)"
     *        },{
     *              "name"        =    "monthly_payment",
     *              "dataType"    =    "string",
     *              "required"    =     true,
     *              "description" =    "Amount paid monthly"
     *        }
     *     }
     * )
     *
     * @Rest\Post("/credit_line")
     * @param Request $request
     * @param UserCreditline $userCreditline
     * @param ConstraintViolationListInterface $validationErrors
     *
     * @return View
     *
     * @ParamConverter("userCreditline", converter="fos_rest.request_body", options={"deserializationContext"={"groups"={"POST"}}})
     *
     *
     */
    public function postCreditlineAction(Request $request, UserCreditline $userCreditline, ConstraintViolationListInterface $validationErrors)
    {
        if (!$this->isGranted(LoanStatusVoter::HAS_LOAN_DRAFTED)) {
            return $this->getResponse(LoanStatusVoter::LOAN_NOT_DRAFTED_CONTENT, self::STATUS_ERROR, Response::HTTP_FORBIDDEN);
        }
        if (count($validationErrors) > 0) {
            return $this->getValidationErrorResponse($validationErrors);
        }
        $em = $this->getDoctrine()->getManager();

        if ($userCreditline->getId()) {//update if have an id
            $em->detach($userCreditline);
            $oldUserCreditLine = $this->getDoctrine()->getRepository('AppBundle:UserCreditline')->getActiveByIdAndUser($userCreditline->getId(), $this->getUser());
            if (!$oldUserCreditLine) {
                throw new NotFoundHttpException('UserCreditline not found');
            }
            if ($oldUserCreditLine != $userCreditline) {//if something changed
                $oldUserCreditLine->setArchive(true);
                $em->persist($userCreditline);
                $em->flush();
            }
            return $this->getResponse([]);

        }

        $userCreditline->setUser($this->getUser());
        $em->persist($userCreditline);
        $em->flush();

        $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $this->getCurrentUser(), UserActivityHandler::CONTEXT_ACTION => UserActivityLog::ACCOUNT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::CREDITLINE_ACTION]);

        return $this->getResponse([]);
    }

    /**
     * This method return UserCreditline list or one UserCreditline if id exist. Created by Vladislav Shatilenya.
     *
     * @ApiDoc(
     *     section="User/Creditline",
     *     statusCodes={
     *        200="Success"
     *     },
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id of row"}
     *     },
     * )
     *
     * @Rest\Get("/credit_line")
     *
     * @return View
     * @param Request $request
     */
    public function getCreditLineAction(Request $request)
    {
        if ($request->get('id')) {
            $response = $this->get('whs.manager.user_creditline_manager')->getActiveByIdAndUser($request->get('id'), $this->getUser());
        } else {
            $response = $this->get('whs.manager.user_creditline_manager')->getCreditLineList($this->getCurrentUser());
        }

        return $this->getResponse($response);
    }

    /**
     * This method delete UserCreditline by id. Created by Vladislav Shatilenya.
     *
     * @ApiDoc(
     *     section="User/Creditline",
     *     statusCodes={
     *        200="Success"
     *     },
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=true, "description"="id of row"}
     *     },
     * )
     *
     * @Rest\Delete("/credit_line")
     *
     * @return View
     * @param Request $request
     */
    public function deleteCreditLineAction(Request $request)
    {
        if (!$this->isGranted(LoanStatusVoter::HAS_LOAN_DRAFTED)) {
            return $this->getResponse(LoanStatusVoter::LOAN_NOT_DRAFTED_CONTENT, self::STATUS_ERROR, Response::HTTP_FORBIDDEN);
        }
        return $this->getResponse($this->get('whs.manager.user_creditline_manager')->deleteCreditLineById($request->request->get('id')));
    }

    /**
     * This method return UserCreditline Types list . Created by Vladimir Glechikov.
     *
     * @ApiDoc(
     *     section="User/Creditline",
     *     statusCodes={
     *        200="Success"
     *     },
     * )
     *
     * @Rest\Get("/credit_line/types")
     *
     * @return View
     */
    public function getCreditLineTypesAction()
    {
        $creditLineTypes = UserCreditline::getCreditLineTypesMap();
        return $this->getResponse($creditLineTypes);
    }
}