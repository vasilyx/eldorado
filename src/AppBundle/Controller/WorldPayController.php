<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 16.12.16
 * Time: 15:43
 */

namespace AppBundle\Controller;


use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\InvestOfferProcess;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\WorldpayHtmlTransaction;
use AppBundle\Entity\WorldpayJsonTransaction;
use AppBundle\Entity\WorldpayTransaction;
use AppBundle\Event\SystemTransactionEvent;
use AppBundle\Event\TransactionEvents;
use AppBundle\Event\WorldpayEvents;
use AppBundle\Process\Handlers\InvestOfferProcessHandler;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Worldpay\Worldpay;
use Worldpay\WorldpayException;

/**
 * Class WorldPayController
 * @package AppBundle\Controller
 * @author Mikhail Pegasin <mikhail.pegasin@gmail.com>
 */
class WorldPayController extends ApiController
{
    /**
     * This method is a part of JSON api usage. It creates Worldpay order by token that getWorldpayTokenAction returns.
     *
     * @ApiDoc(
     *     section="Worldpay",
     *     parameters={
     *          {
     *              "name"          = "token",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "A unique token which the WorldPay.js library added to your checkout form, or obtained via the token API. This token represents the customer's card details/payment method which was stored on our server. One of token or paymentMethod must be specified",
     *          },{
     *              "name"          = "amount",
     *              "dataType"      = "integer",
     *              "required"      =  true,
     *              "description"   = "The amount to be charged in pennies/cents (or whatever the smallest unit is of the currencyCode that you specified)",
     *          },{
     *              "name"          = "name",
     *              "dataType"      = "string",
     *              "required"      =  false,
     *              "description"   = "The name of the cardholder or payee",
     *          },{
     *              "name"          = "order_description",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "The description of the order provided by you",
     *          }
     *     }
     * )
     *
     * @Post("/orders")
     * @ParamConverter("worldPayTransaction", converter="fos_rest.request_body")
     *
     * @param Request $request
     * @param WorldPayTransaction $worldPayTransaction
     * @return mixed
     */
    public function createOrderAction(Request $request, WorldpayTransaction $worldPayTransaction)
    {
        $em = $this->getDoctrine()->getManager();
        $worldpay = new Worldpay($this->getParameter('worldpay_service_key'));

        $billing_address = [];
        if ($address = $this->get('whs.manager.user_address_manager')->getCurrentAddress($this->getUser())) {
            $billing_address = array(
                "address1"=> $address->getLine1(),
                "address2"=> $address->getLine2(),
                "address3"=> '',
                "postalCode"=> $address->getPostalCode(),
                "city"=> $address->getCity(),
                "state"=> $address->getState(),
                "countryCode"=> $address->getCountry(),
            );
        }

        $this->get('whs.manager.world_pay_transaction_manager')->createTransaction(
            $worldPayTransaction
        );

        try {
            $response = $worldpay->createOrder(array(
                'token' => $request->get('token'),
                'amount' => (int) $request->get('amount'),
                'currencyCode' => 'GBP',
                'name' => $request->get('name'),
                'billingAddress' => $billing_address,
                'orderDescription' => $request->get('order_description'),
                'customerOrderCode' => $worldPayTransaction->getId()
            ));

            $worldPayTransaction->setPaymentStatus(constant('AppBundle\Entity\WorldPayTransaction::PAYMENT_STATUS_'.strtoupper($response['paymentStatus'])));

            if ($response['paymentStatus'] === 'SUCCESS') {
                $worldpayOrderCode = $response['orderCode'];
                $worldPayTransaction->setOrderCode($worldpayOrderCode);
                return $this->getResponse($response);
            } else {
                throw new WorldpayException(print_r($response, true));
            }

        } catch (WorldpayException $e) {
            $worldPayTransaction->setPaymentStatus(WorldpayTransaction::PAYMENT_STATUS_ERROR);
            return $this->getResponse([
                'code'  => $e->getHttpStatusCode(),
                'message' => $e->getDescription()
            ], ApiController::STATUS_ERROR, $e->getHttpStatusCode());
        } catch (\Exception $e) {
            $worldPayTransaction->setPaymentStatus(WorldpayTransaction::PAYMENT_STATUS_ERROR);
            return $this->getResponse([
                'code'  => $e->getCode(),
                'message' => $e->getMessage()
            ], ApiController::STATUS_ERROR, $e->getCode());
        } finally {
            $em->flush();
        }
    }

    /**
     * This method is a part of JSON api usage. It returns token by security sensitive card data.
     *
     * @ApiDoc(
     *     section="Worldpay",
     *     parameters={
     *          {
     *              "name"          = "name",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "",
     *          },{
     *              "name"          = "expiry_month",
     *              "dataType"      = "integer",
     *              "required"      =  true,
     *              "description"   = "",
     *          },{
     *              "name"          = "expiry_year",
     *              "dataType"      = "string",
     *              "required"      =  false,
     *              "description"   = "",
     *          },{
     *              "name"          = "card_number",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "",
     *          },{
     *              "name"          = "cvc",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "",
     *          }
     *
     *     }
     * )
     * @Post("/tokens")
     * @param Request $request
     * @return mixed
     */
    public function getWorldpayTokenAction(Request $request)
    {
        $curl = curl_init('https://api.worldpay.com/v1/tokens');
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-type: application/json'
        ]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode([
            "reusable" => true,
            "paymentMethod" => [
                "name" => $request->get('name'),
                "expiryMonth" => $request->get('expiry_month'),
                "expiryYear" => $request->get('expiry_year'),
                "cardNumber" => $request->get('card_number'),
                "type" => "Card",
                "cvc" => $request->get('cvc')
            ],
            "clientKey" => "T_C_c98eff90-109c-4e6c-b9b7-3eb7f62c35ad"
        ]));
        $result = json_decode(curl_exec($curl), true);
        curl_close($curl);

        return $this->getResponse($result);
    }

    /**
     * This method is a part of HTML api usage. Returns parameters for Worldpay form by user.
     * <pre>
     *  Form parameters example:
     * action="https://secure-test.worldpay.com/wcc/purchase" method="POST"
        input type="hidden" name="testMode" value="100" />
        input type="hidden" name="instId" value="1122806">
        input type="hidden" name="cartId" value="DEPC6f5140655244241bf8609b54249599a7">
        input type="hidden" name="amount" value="100">
        input type="hidden" name="currency" value="GBP">
        input type="hidden" name="MC_callback" value="http://cu540.effective-soft.com/app_dev.php/api/worldpay/callback">
        input type="hidden" name="MC_redirectUrl" value="/#/portal/invest/transfer_money_result">
        </form>
     * </pre>
     *
     * @ApiDoc(
     *     section="Worldpay",
     *     parameters={
     *          {
     *              "name"          = "test_mode",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Available values are: 0 - production mode, 100 - test mode. Default: 100",
     *          },{
     *              "name"          = "redirect_url",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Shopper redirect url",
     *          },{
     *              "name"          = "process_id",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "Process id",
     *          }
     *
     *     }
     * )
     *
     * @Get("/form_parameters")
     *
     * @param Request $request A Symfony's Request object
     * @return View
     */
    public function getWorldpayFormParametersAction(Request $request)
    {
        $inputResolver = new OptionsResolver();
        $outputResolver = new OptionsResolver();

        $inputResolver
            ->setDefined([
            ])
            ->setRequired([
                'redirect_url',
                'process_id'
            ])
            ->setDefaults([
                'test_mode' => "100"
            ])
            ;

        $outputResolver
            ->setRequired([
                'test_mode',
                'inst_id',
                'cart_id',
                'amount',
                'currency',
                'callback_url',
                'redirect_url'
            ])
            ;

        try {
            $input = $inputResolver->resolve($request->query->all());
        } catch (MissingOptionsException $e) {
            return $this->getResponse([
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => $e->getMessage()
            ], ApiController::STATUS_ERROR, Response::HTTP_BAD_REQUEST);
        }

        $event = SystemTransactionEvent::create()
            ->setUser($this->getUser());

        $investorOwnerTransaction = Transaction::create()
            ->setTransactionOwner(Transaction::TRANSACTION_OWNER_INVESTOR)
        ;

        $worldpayOwnerTransaction = Transaction::create()
            ->setTransactionOwner(Transaction::TRANSACTION_OWNER_WORLDPAY)
            ->setParentTransaction($investorOwnerTransaction)
        ;

        $process = $this->getDoctrine()->getRepository(InvestOfferProcess::class)->find($request->get('process_id'));

        /**
         * @var InvestOfferProcessHandler $processHandler
         */
        $processHandler = $this->get('app.process_handler_resolver')->resolve($process);

        $event
            ->setTransactions([
                $investorOwnerTransaction,
                $worldpayOwnerTransaction
            ])
            ->setAmount($processHandler->getAmount($process))
        ;

        $processHandler->nextStep($process, $event);
        /**
         * Logging
         */
        $this->get('monolog.logger.callcredit')->debug(sprintf('Event %1$s is generated.', TransactionEvents::DEPC), [$this->getParameter('kernel.environment')]);
        /**
         * End logging
         */
        /**
         * @var WorldpayHtmlTransaction $worldpayTransaction
         */
        $worldpayTransaction = $event->getArgument('worldpay_transaction');

        $result = [
            'test_mode' =>  $input['test_mode'],
            'inst_id'   =>  $worldpayTransaction->getInstallationId(),
            'cart_id'   =>  $worldpayTransaction->getCustomerOrderCode(),
            'amount'    =>  $worldpayTransaction->getAmount(),
            'currency'  =>  $worldpayTransaction->getCurrencyCode(),
            'callback_url'  => $this->generateUrl($worldpayTransaction->getCallbackUrl(), [], UrlGeneratorInterface::ABSOLUTE_URL),
            'redirect_url'  => $input['redirect_url']
        ];


        return $this->getResponse($outputResolver->resolve($result));
    }

    /**
     * This action is a part of HTML api usage. After a payment has been processed, either through to success or to cancellation, information about that payment is sent to this action by using the Payment Response feature.
     * http://support.worldpay.com/support/kb/bg/paymentresponse/pr0000.html?_ga=1.188687404.546273885.1481637739
     * Action returns system worldpay transaction in response
     *
     * @Post("/callback")
     *
     * @return Response
     */
    public function worldpayCallbackAction(Request $request)
    {
        if (!$parameters = $request->request->all()) {
            return;
        }
        $event = SystemTransactionEvent::create();
        $this->get('event_dispatcher')->dispatch(WorldpayEvents::CALLBACK_RECEIVED, $event
            ->setArgument('parameters', $parameters)
        );
        /**
         * Logging worldpay
         */
        $this->get('monolog.logger.worldpay')->debug(sprintf('There are %1$s elements in parameters array from worldpay. CartId = %2$s. TransStatus = %3$s', count($parameters), $parameters['cartId'], $parameters['transStatus']), [$this->getParameter('kernel.environment')]);
        /**
         * End logging
         */
        /**
         * Logging callcredit
         */
        $this->get('monolog.logger.callcredit')->debug(sprintf('Event %1$s is generated.', WorldpayEvents::CALLBACK_RECEIVED), [$this->getParameter('kernel.environment')]);
        /**
         * End logging
         */
        $redirectUrl = $request->getHttpHost() . $request->get('MC_redirectUrl') . '?cartId=' . $request->get('cartId');

        return $this->handleView($this->view($redirectUrl, 200)
            ->setFormat('html')
            ->setTemplate('AppBundle:worldpay:redirect.html.twig')
            ->setTemplateVar('redirectUrl')
        );
    }

    /**
     *
     * @ApiDoc(
     *     section="Worldpay",
     *     parameters={
     *          {
     *              "name"          = "cart_id",
     *              "dataType"      = "string",
     *              "required"      =  true,
     *              "description"   = "cartId parameter from the Worldpay Payment Response",
     *          }
     *
     *     }
     * )
     * @param Request $request
     *
     * @Get("/response")
     *
     * @return View
     */
    public function getPaymentResultsAction(Request $request)
    {
        $customerOrderCode = $request->get('cart_id');
        if (!$customerOrderCode) {
            return $this->getResponse(
                [
                    'code' => 'cart_id.isBlank',
                    'message' => 'Parameter cart_id should not be blank'
                ],
                static::STATUS_ERROR,
                Response::HTTP_BAD_REQUEST
            );
        }
        $transaction = $this->getDoctrine()->getRepository(WorldpayHtmlTransaction::class)->findOneBy(
            ['customerOrderCode' => $customerOrderCode]
        );

        return $this->getResponse($transaction);
    }
}
