<?php
namespace AppBundle\Controller;

use AppBundle\Entity\UserHousehold;
use AppBundle\Security\Voter\LoanStatusVoter;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use LogBundle\Entity\UserActivityLog;
use LogBundle\Handler\UserActivityHandler;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;


class UserHouseholdController extends ApiController
{

    /**
     * This method returns User Household items for the current user. Created by Vladimir Glechikov
     *
     * @ApiDoc(
     *     section="User/Household"
     * )
     * @Get("/household")
     * @return View
     */
    public function getUserHouseholdAction()
    {
        $userHousehold = $this->getDoctrine()->getRepository('AppBundle:UserHousehold')->getOneByUser($this->getUser());
        if ($userHousehold) {
            return $this->getResponse($userHousehold);
        } else {
            return $this->getResponse([]);
        }
    }

    /**
     *
     * @ApiDoc(
     *     section="User/Household",
     *     description="This method adds or updates User Household for current user. Created by Vladimir Glechikov.",
     *     parameters={
     *      {"name"="monthly_rent", "dataType"="string", "required"=true, "description"="monthlyRent"},
     *      {"name"="monthly_utilities", "dataType"="string", "required"=true, "description"="monthlyUtilities"},
     *      {"name"="monthly_travel", "dataType"="string", "required"=false, "description"="monthlyTravel"},
     *      {"name"="monthly_food", "dataType"="string", "required"=true, "description"="monthlyFood"},
     *      {"name"="monthly_savings", "dataType"="string", "required"=true, "description"="monthlySavings"},
     *      {"name"="monthly_entertainment", "dataType"="string", "required"=true, "description"="monthlyEntertainment"},
     *     }
     * )
     * @Post("/household")
     *
     * @param $request Request
     * @param $userHousehold UserHousehold
     * @param $validationErrors ConstraintViolationListInterface
     *
     * @return View
     *
     * @ParamConverter("userHousehold", converter="fos_rest.request_body")
     *
     */
    public function postUserHouseholdAction(Request $request, UserHousehold $userHousehold, ConstraintViolationListInterface $validationErrors)
    {
        if (!$this->isGranted(LoanStatusVoter::HAS_LOAN_DRAFTED)) {
            return $this->getResponse(LoanStatusVoter::LOAN_NOT_DRAFTED_CONTENT, self::STATUS_ERROR, Response::HTTP_FORBIDDEN);
        }
        if (count($validationErrors) > 0) {
            return $this->getValidationErrorResponse($validationErrors);
        }
        $em = $this->getDoctrine()->getManager();

        $existingHousehold = $this->getDoctrine()->getRepository('AppBundle:UserHousehold')->getOneByUser($this->getUser());
        if ($existingHousehold) {
            $existingHousehold->setArchive(true);
        }

        $userHousehold->setUser($this->getUser());
        $em->persist($userHousehold);
        $em->flush();

        $this->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $this->getCurrentUser(), UserActivityHandler::CONTEXT_ACTION => UserActivityLog::ACCOUNT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::HOUSEHOLD_ACTION]);

        return $this->getResponse([]);
    }
}