<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 30.11.16
 * Time: 15.22
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Agreement;
use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\Product;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\QuoteTerm;
use AppBundle\Manager\AgreementManager;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class QuoteController extends ApiController
{
    /**
     * <pre>
     *  This method returns quoteApplies for dashboard. Created by Mikhail Pegasin <mpegasin@rambler.ru>.
     *  {
     *      status: success|error,
     *      content: {
     *          user_product: {
     *              product: {
     *                  type: 0(borrower)|1(lender)
     *              }
     *          }
     *          status: 10(draft)|20(pending)|30(approved)|40(rejected),
     *          created_at: ISO8601 advanced format
     *          updated_at: ISO8601 advanced format
     *      }
     *  }
     * </pre>
     *
     * @ApiDoc(
     *     section="Quote",
     *     parameters={
     *          {
     *              "name"          = "product_type",
     *              "dataType"      = "integer",
     *              "required"      =  false,
     *              "description"   = "If null, the method returns all quoteApplies. If 0 - for borrower, if 1 - for lender.",
     *          }
     *     }
     * )
     *
     * @param   Request $request
     * @return  View
     *
     * @Get("/quotes")
     */
    public function getQuotesAction(Request $request)
    {
        $productType = $request->get('product_type');

        $qam = $this->get('whs.manager.quote_apply_manager');
        $quoteApplies = $qam->getQuoteApplies($this->getCurrentUser(), $productType);

        $response = $this->getResponse($quoteApplies);

        $context = new SerializationContext();
        $context->setGroups('quoteApply');
        $response->setSerializationContext($context);

        return $response;
    }

    /**
     *
     * This method returns Quote Details for **Draft Loan** for current user. Created by Vladimir Glechikov
     *
     * @ApiDoc(
     *     section="Quote"
     * )
     *
     * @return View
     *
     * @Get("/quote_details_for_draft_loan")
     */
    public function getQuoteDetailsForDraftLoanAction()
    {
        $em = $this->getDoctrine()->getManager();
        $loanApply = $em->getRepository('AppBundle:LoanApply')->getOneDrafted($this->getUser());
        if (empty($loanApply)) {
            throw new BadRequestHttpException('There is no drafted LoanApply for this user');
        }
        $quoteTerm = $em->getRepository('AppBundle:QuoteTerm')->getQuoteTermForQuoteApply($loanApply->getQuoteApply());
        if (!$quoteTerm) {
            throw new BadRequestHttpException('There is no Quote Term for that user');
        }
        return $this->getResponse($quoteTerm);
    }

    /**
     *
     * This method returns **Quote underwriting details** for current user. Created by Shatilenya Vladislav.
     *
     * @ApiDoc(
     *     section="Quote",
     *     parameters={
     *          {
     *              "name" = "id",
     *              "dataType" = "integer",
     *              "required" = true,
     *              "description" = "QuoteApply id"
     *          }
     *     }
     * )
     *
     * @return View
     *
     * @Post("/quote_underwriting_details")
     */
    public function getQuoteUnderwritingDetailsAction(Request $request)
    {
        $quoteApplyID = $request->request->get('id');

        $em = $this->getDoctrine()->getManager();
        $quoteApply = $em->getRepository(QuoteApply::class)->find($quoteApplyID);

        if (!$quoteApply || $quoteApply->getUser()->getId() != $this->getCurrentUser()->getId()) {
            return $this->getResponse('QuoteApply with this quote_apply_id not found for this user', 'error', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $response = '';
        switch ($quoteApply->getStatus()) {
            case (QuoteApply::STATUS_APPROVED):
                $response = $this->get('whs.manager.underwriting_result_manager')->getUnderwritingResultForQuoteApply($quoteApply);
                break;
            case (QuoteApply::STATUS_REJECTED):
                /** @var QuoteTerm $quoteTerm */
                $quoteTerm = $em->getRepository(QuoteTerm::class)->getQuoteTermForQuoteApply($quoteApply);
                $response = [
                    'quote_amount' => $quoteTerm->getAmount(),
                    'quote_term'   => $quoteTerm->getTerm()
                ];
                break;
        }

        return $this->getResponse($response);
    }

    /**
     *
     * This method returns **XML with response** which was gotten at Quotation. Created by Shatilenya Vladislav.
     *
     * @ApiDoc(
     *     section="CallCredit",
     *     parameters={
     *          {
     *              "name" = "id",
     *              "dataType" = "integer",
     *              "required" = true,
     *              "description" = "QuoteApply id"
     *          }
     *     }
     * )
     *
     * @param Request $request
     * @return View
     * @Post("/get_quote_response")
     */
    public function getQuoteResponseAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var QuoteApply $quoteApply */
        $quoteApply = $em->getRepository(QuoteApply::class)->find($request->request->get('id'));

        if (!$quoteApply) {
            return $this->getResponse('QuoteApply not found', self::STATUS_ERROR);
        }

        $callCredit = '';

        switch ($quoteApply->getProduct()->getType()) {
            case (Product::TYPE_BORROWER):
                $callCredit = $em->getRepository(CallCreditAPICalls::class)->findOneBy([
                    'quoteApply' => $quoteApply,
                    'user' => $quoteApply->getUser(),
                    'purpose' => CallCreditAPICalls::PURPOSE_QS,
                    'type' => CallCreditAPICalls::TYPE_CALLREPORT
                ]);
                break;
            case (Product::TYPE_INVESTOR):
                $callCredit = $em->getRepository(CallCreditAPICalls::class)->findOneBy([
                    'quoteApply' => $quoteApply,
                    'user' => $quoteApply->getUser(),
                    'purpose' => CallCreditAPICalls::PURPOSE_QS,
                    'type' => CallCreditAPICalls::TYPE_CALLVALIDATE
                ]);
                break;
        }

        if (!$callCredit) {
            return $this->getResponse('Response XML not found', self::STATUS_ERROR);
        }

        return $this->getResponse($callCredit->getResponseXML());
    }

    /**
     *
     * This method returns **XML with responses** which was gotten from Callreport (CA with score), Callvalidate and Affordability requests. Created by Shatilenya Vladislav.
     *
     * @ApiDoc(
     *     section="CallCredit",
     *     parameters={
     *          {
     *              "name" = "id",
     *              "dataType" = "integer",
     *              "required" = true,
     *              "description" = "LoanApply id"
     *          }
     *     }
     * )
     *
     * @param Request $request
     * @return View
     * @Post("/get_loan_response")
     */
    public function getLoanResponseAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var LoanApply $loanApply */
        $loanApply = $em->getRepository(LoanApply::class)->find($request->request->get('id'));

        if (!$loanApply) {
            return $this->getResponse('loanApply not found', self::STATUS_ERROR);
        }

        $quoteApply = $loanApply->getQuoteApply();

        if ($quoteApply->getUser() != $this->getUser()) {
            return $this->getResponse('loanApply not found for this user', self::STATUS_ERROR);
        }

        $response = '';

        /** @var CallCreditAPICalls $callCredit */
        $callCredit = $em->getRepository(CallCreditAPICalls::class)
            ->getCallCreditForLoanByQuoteAndType($quoteApply, CallCreditAPICalls::TYPE_CALLVALIDATE);

        if ($callCredit) {
            $response[] = $callCredit[0]->getResponseXML();
        }

        /** @var CallCreditAPICalls $callCredit */
        $callCredit = $em->getRepository(CallCreditAPICalls::class)
            ->getCallCreditForLoanByQuoteAndType($quoteApply, CallCreditAPICalls::TYPE_AFFORDABILITY);

        if ($callCredit) {
            $response[] = $callCredit[0]->getResponseXML();
        }

        /** @var CallCreditAPICalls $callCredit */
        $callCredit = $em->getRepository(CallCreditAPICalls::class)
            ->getCallCreditForLoanByQuoteAndType($quoteApply, CallCreditAPICalls::TYPE_CALLREPORT);

        if ($callCredit) {
            $response[] = $callCredit[0]->getResponseXML();
        }

        return $this->getResponse($response);
    }

    /**
     * Get information from Agreements table based on the Borrower Quote ID
     *
     * @ApiDoc(
     *     section="Agreement",
     *     description="The method returns information from Agreements table based on the Borrower Quote ID",
     *     headers={
     *          {
     *              "name" = "Auth",
     *              "description" = "Must content the next string: 'whsAToken'",
     *              "required" = true
     *          }
     *     },
     *     parameters={
     *          {"name"="quote_id", "dataType"="string", "required"=true, "description"="Quote ID"},
     *     }
     * )
     *
     * @Get("/quote/{quote_id}/agreement_details")
     * @ParamConverter("quoteApply", options={"mapping": {"quote_id" : "id"}})
     * @param QuoteApply $quoteApply
     *
     * @return View
     */
    public function getAgreementDetailsAction(QuoteApply $quoteApply)
    {
        if ($this->getUser() != $quoteApply->getUser()) {
            return $this->getResponse('Quote not found for this user', self::STATUS_ERROR);
        }

        $loanApply = $this->get('whs.manager.loan_apply_manager')->getLoanApplyByQuote($quoteApply);

        if (empty($loanApply)) {
            return $this->getResponse('loanApply not found', self::STATUS_ERROR);
        }

        /** @var Agreement $agreement */
        $agreement = $this->getDoctrine()->getRepository(Agreement::class)->getAgreementLoanApply($loanApply);

        if (empty($agreement)) {
            return $this->getResponse('Agreement not found', self::STATUS_ERROR);
        }

        $result = [
            'total' => $agreement->getAmount(),
            'apr' => $agreement->getApr(),
            'term' => $agreement->getTerm(),
            'monthly' => $agreement->getMonthlyRepayment(),
            'bonus_rate' => $agreement->getBonusRate(),
            'bonus_value' => $agreement->getBonusAmount()
        ];

        return $this->getResponse($result);
    }
}