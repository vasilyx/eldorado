<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 02.02.2017
 * Time: 16:47
 */

namespace AppBundle\Controller;

use AppBundle\Entity\CallCreditCredentials;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 *
 * @package AppBundle\Controller
 */
class CallCreditController extends ApiController
{
    /**
     *
     * This method returns **credentials** for **CallCredit API** by type. Created by Vladislav Shatilenya.
     *
     * @ApiDoc(
     *     section="CallCredit",
     *     parameters={
     *          {
     *              "name"          = "type",
     *              "dataType"      = "integer",
     *              "required"      =  true,
     *              "description"   = "Type of API: callreport - 1, callvalidate - 2, affordability - 3",
     *          }
     *     }
     * )
     *
     * @return View
     * @param  Request $request
     *
     * @Post("/credentials")
     */
    public function postCredentialsAction(Request $request)
    {
        /**
         * Method should return result only for NOT prod server mode
         */
        if ('prod' !== $this->getParameter('server_mode')) {
            $type = $request->request->get('type');

            if (!in_array((int)$type, array_keys(CallCreditCredentials::getTypes()))) {
                return $this->getResponse(null);
            }

            $credentials = $this->getDoctrine()->getRepository(CallCreditCredentials::class)->getCredentialsForApiByType($type);

            if (!$credentials) {
                return $this->getResponse(null);
            }

            return $this->getResponse($credentials);
        }

        return $this->getResponse([]);
    }
}