<?php
namespace AppBundle\Repository;

use AppBundle\Entity\Agreement;
use AppBundle\Entity\ContractMatching;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\LoanApply;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class ContractMatchingRepository extends EntityRepository
{

    /**
     * @param Agreement $agreement
     * @return mixed
     */
    public function getMatchedSumByAgreement(Agreement $agreement)
    {
        $qb = $this->createQueryBuilder('cm')
            ->select('SUM(cm.contractAmount)')
            ->andWhere('cm.agreement=:agreement')
            ->setParameter('agreement', $agreement)
        ;

        return (int)$qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Agreement $agreement
     * @param InvestOffer $investOffer
     * @return mixed
     */
    public function getMatchedSumByAgreementOffer(Agreement $agreement, InvestOffer $investOffer)
    {
        $qb = $this->createQueryBuilder('cm')
            ->select('SUM(cm.contractAmount)')
            ->andWhere('cm.agreement=:agreement')
            ->andWhere('cm.investOffer=:investOffer')
            ->setParameter('investOffer', $investOffer)
            ->setParameter('agreement', $agreement)
        ;

        $res = $qb->getQuery()->getOneOrNullResult();

        return is_array($res) ? array_shift($res) : 0;
    }


    public function getSumByOfferStatus(InvestOffer $investOffer, $status)
    {
        $qb = $this->createQueryBuilder('cm')
            ->select('SUM(cm.contractAmount)')
            ->andWhere('cm.investOffer=:investOffer')
            ->andWhere('cm.status=:status')
            ->setParameter('investOffer', $investOffer)
            ->setParameter('status', $status)
            ->groupBy('cm.investOffer')
        ;

        $res = $qb->getQuery()->getOneOrNullResult();

        return is_array($res) ? array_shift($res) : 0;
    }

    /**
     * @param Agreement $agreement
     * @return array
     */
    public function getMatchedLenderSumByAgreement(Agreement $agreement)
    {
        $loanApply = $agreement->getLoanApply();

        $qb = $this->createQueryBuilder('cm')
            ->select('SUM(cm.contractAmount) sum_amount, (io.user) lender_id, 
            u.username, (cm.investOffer) investOffer_id' .
                ", {$loanApply->getId()} as request_id " .
                ", {$loanApply->getUser()->getId()} as borrower_id" .
                ", io.term as lending_rate" .
                ", {$agreement->getTerm()} loan_term"
            )
            ->leftJoin('AppBundle\Entity\InvestOffer', 'io', Join::WITH, 'cm.investOffer = io.id')
            ->leftJoin('AppBundle\Entity\User', 'u', Join::WITH, 'u.id = io.user')
            ->andWhere('cm.agreement=:agreement')
            ->setParameter('agreement', $agreement)
            ->groupBy('cm.investOffer')
        ;

        $rows =  $qb->getQuery()->getResult();

        foreach ($rows as &$row) {

            $row['lending_rate'] = InvestOffer::getRatesByTerms()[$row['lending_rate']];
        }

        return $rows;
    }




    /**
     * @param InvestOffer $investOffer
     * @param int $status
     * @return mixed
     */
    public function updateStatusByOffer(InvestOffer $investOffer, $status = ContractMatching::STATUS_ACTIVE)
    {
        return $this->createQueryBuilder('cm')
            ->update()
            ->set('cm.status', ':status')
            ->setParameter('status', $status)
            ->where('cm.investOffer = :investOffer')
            ->setParameter('investOffer', $investOffer)
            ->getQuery()->getSingleScalarResult();
    }

    /**
     * @param LoanApply $loanApply
     * @param int $status
     * @return mixed
     */
    public function updateStatusByLoanApply(LoanApply $loanApply, $status = ContractMatching::STATUS_ACTIVE)
    {
        return $this->createQueryBuilder('cm')
            ->update()
            ->set('cm.status', ':status')
            ->setParameter('status', $status)
            ->where('cm.loanApply = :loanApply')
            ->setParameter('loanApply', $loanApply)
            ->getQuery()->getSingleScalarResult();
    }

    /**
     * @param InvestOffer $investOffer
     * @return array
     */
    public function getMatchedAgreementsByOffer(InvestOffer $investOffer)
    {
        $qb = $this->createQueryBuilder('cm')
            ->select('SUM(cm.contractAmount) sum_amount, 
            u.id as borrower_id, 
            a.term, 
            a.id as agreement_id, 
            lp.id as loanApply_id,
            lp.status,
            io.id as offer_id,
            a.apr,
            a.bonusRate as rate')
            ->leftJoin('AppBundle\Entity\InvestOffer', 'io', Join::WITH, 'cm.investOffer = io.id')
            ->leftJoin('AppBundle\Entity\Agreement', 'a', Join::WITH, 'cm.agreement = a.id')
            ->leftJoin('AppBundle\Entity\LoanApply', 'lp', Join::WITH, 'cm.loanApply = lp.id')
            ->leftJoin('AppBundle\Entity\User', 'u', Join::WITH, 'u.id = lp.user')

            ->andWhere('cm.investOffer=:investOffer')
            ->setParameter('investOffer', $investOffer)
            ->groupBy('cm.investOffer, agreement_id, loanApply_id, borrower_id, a.term, rate, lp.status, a.apr')
        ;

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param Agreement $agreement
     * @return mixed
     */
    public function deleteByAgreement(Agreement $agreement)
    {
        return $this->createQueryBuilder('cm')
            ->delete()
            ->where('cm.agreement = :agreement')
            ->setParameter('agreement', $agreement)
            ->getQuery()->getResult();
    }

    /**
     * @param InvestOffer $invest_offer
     * @return mixed
     */
    public function getSumForInvestor(InvestOffer $invest_offer)
    {
        $qb = $this->createQueryBuilder('cm')
            ->select('SUM(cm.contractAmount)')
            ->andWhere('cm.investOffer= :investOffer')
            ->setParameter('investOffer', $invest_offer)
        ;

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Agreement $agreement
     * @return mixed
     */
    public function getSumOffersByAgreement(Agreement $agreement)
    {
        $qb = $this->createQueryBuilder('cm')
            ->select('SUM(cm.contractAmount) as summ', '(cm.investOffer) as offer')
            ->where('cm.agreement = :agreement')
            ->setParameter('agreement', $agreement)
            ->groupBy('cm.investOffer')
        ;

        return $qb->getQuery()->getResult();
    }


}