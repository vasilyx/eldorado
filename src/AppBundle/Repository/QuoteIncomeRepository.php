<?php
/**
 * Created by PhpStorm.
 * User: mikhailpegasin
 * Date: 17.11.16
 * Time: 15:25
 */

namespace AppBundle\Repository;


use AppBundle\Entity\LoanApply;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\QuoteIncome;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class QuoteIncomeRepository extends EntityRepository
{
    /**
     * @param User $user
     * @param bool $archive
     * @return QuoteIncome
     */
    public function getQuoteIncomeForUser(User $user, $archive = false)
    {
        $qb = $this->createQueryBuilder('quoteIncome');

        $qb
            ->leftJoin('AppBundle\Entity\QuoteApply', 'quoteApply', Join::WITH, 'quoteIncome.quoteApply = quoteApply.id')
            ->leftJoin('AppBundle\Entity\User', 'user', Join::WITH, 'quoteApply.user = user.id')
            ->where('user.id = :user')
            ->andWhere('quoteIncome.archive = :archive')
            ->setParameter('user', $user->getId())
            ->setParameter('archive', $archive)
            ->orderBy('quoteIncome.createdAt', 'DESC')
            ->setMaxResults(1);

        $result = $qb->getQuery()->getResult();

        $quoteIncome = $result ? $result[0] : null;

        return $quoteIncome;
    }

    /**
     * @param User $user
     * @return QuoteIncome
     */
    public function getQuoteIncomeForDraftLoan(User $user)
    {
        $qb = $this->createQueryBuilder('quoteIncome')
            ->join('quoteIncome.quoteApply', 'quoteApply')
            ->join('AppBundle:LoanApply', 'loanApply', 'WITH', 'loanApply.quoteApply = quoteApply')
            ->where('loanApply.user = :user')
            ->andWhere('loanApply.status = :status')
            ->setParameter('user', $user)
            ->setParameter('status', LoanApply::STATUS_DRAFT)
            ->setMaxResults(1);
        $quoteIncome = $qb->getQuery()->getOneOrNullResult();
        return $quoteIncome;
    }
}