<?php
/**
 * Created by PhpStorm.
 * User: mikhailpegasin
 * Date: 17.11.16
 * Time: 13:35
 */

namespace AppBundle\Repository;


use AppBundle\Entity\LoanApply;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\QuoteTerm;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class QuoteTermRepository extends EntityRepository
{
    /**
     * @param User $user
     * @return null|QuoteTerm
     */
    public function getQuoteTermForUser(User $user)
    {
        $qb = $this->createQueryBuilder('quoteTerm');

        $qb
            ->leftJoin('AppBundle\Entity\QuoteApply', 'quoteApply', Join::WITH, 'quoteTerm.quoteApply = quoteApply.id')
            ->leftJoin('AppBundle\Entity\User', 'user', Join::WITH, 'quoteApply.user = user.id')
            ->where('quoteApply.status = :status')
            ->andWhere('user.id = :user_id')
            ->andWhere('quoteTerm.archive = 0')
            ->setParameter('user_id', $user->getId())
            ->setParameter('status', QuoteApply::STATUS_DRAFT)
            ->orderBy('quoteTerm.createdAt', 'DESC')
            ->setMaxResults(1);

        $result = $qb->getQuery()->getResult();

        $quoteTerm = $result ? $result[0] : null;

        return $quoteTerm;
    }

    /**
     * @param QuoteApply $quoteApply
     * @param bool $archive
     * @return null|QuoteTerm
     */
    public function getQuoteTermForQuoteApply(QuoteApply $quoteApply, $archive = false)
    {
        $result = $this->findBy(['quoteApply' => $quoteApply, 'archive' => $archive]);
        if (empty($result)) {
            throw new NotFoundHttpException('QuoteTerm not found');
        } elseif (count($result) > 1) {
            throw new BadRequestHttpException('Wrong count of Quote Terms in the system');
        }
        return $result[0];
    }

    /**
     * @param User $user
     * @param bool $archive
     * @return QuoteTerm
     * @throws NotFoundHttpException
     * @throws BadRequestHttpException
     */
    public function getQuoteTermForDraftLoan(User $user, $archive = false)
    {
        $qb = $this->createQueryBuilder('qt')
            ->join('AppBundle:QuoteApply', 'qa', Join::WITH, 'qa = qt.quoteApply')
            ->join('AppBundle:LoanApply', 'la', Join::WITH, 'la.quoteApply = qa')
            ->join('AppBundle:User', 'u', Join::WITH, 'u = qa.user')
            ->where('u = :user')
            ->andWhere('qt.archive = :archive')
            ->andWhere('qa.status = ' . QuoteApply::STATUS_APPROVED)
            ->andWhere('la.status = ' . LoanApply::STATUS_DRAFT)
            ->setParameter('user', $user)
            ->setParameter('archive', $archive);

        $result = $qb->getQuery()->getResult();
        if (empty($result)) {
            throw new NotFoundHttpException('QuoteTerm not found');
        } elseif (count($result) > 1) {
            throw new BadRequestHttpException('Wrong count of Quote Terms in the system');
        }
        return $result[0];
    }
}