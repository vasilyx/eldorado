<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 05.01.2017
 * Time: 12:34
 */

namespace AppBundle\Repository;

use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\UnderwritingResult;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Config\Definition\Exception\Exception;

class UnderwritingResultRepository extends EntityRepository
{
    /** This method returns all underwritingResults for quoteApply
     * @param QuoteApply $quoteApply
     * @param int $type
     * @param Criteria $criteria
     * @return array
     */
    public function getUnderwritingResultsForQuote(QuoteApply $quoteApply, $type = UnderwritingResult::TYPE_QUOTE, Criteria $criteria = null)
    {

        $qb = $this->createQbForQuote($quoteApply, $type);

        $qb->orderBy('underwritingResult.id', Criteria::DESC);

        if ($criteria) {
            $qb->addCriteria($criteria);
        }

        try {
            $underwritingResults = $qb->getQuery()->getResult();
        } catch (NoResultException $exception) {
            throw new Exception("UnderwritingResult not found for this quote_apply_id");
        }

        return $underwritingResults;
    }

    /**
     * @param QuoteApply $quoteApply
     * @return UnderwritingResult | null
     */
    public function findLastUnderwritingResultForQuote(QuoteApply $quoteApply)
    {
        return $this
            ->createQbForQuote($quoteApply)
            ->orderBy('underwritingResult.id', Criteria::DESC)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param QuoteApply $quoteApply
     * @param int $type
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function createQbForQuote(QuoteApply $quoteApply, $type = UnderwritingResult::TYPE_QUOTE)
    {
        $qb = $this->createQueryBuilder('underwritingResult');

        $qb
            ->select('underwritingResult')
            ->where('underwritingResult.quoteApply = :quoteApply')
            ->andWhere('underwritingResult.type = :type')
            ->setParameters(['quoteApply' => $quoteApply, 'type' => $type]);

        return $qb;
    }
}