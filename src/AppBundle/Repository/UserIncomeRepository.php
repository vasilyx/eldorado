<?php
/**
 * Created by PhpStorm.
 * User: mikhailpegasin
 * Date: 25.11.16
 * Time: 17:08
 */

namespace AppBundle\Repository;


use AppBundle\Entity\User;
use AppBundle\Entity\UserIncome;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;

class UserIncomeRepository extends EntityRepository
{
    /**
     * @param User|null $user
     * @param Criteria|null $criteria
     * @return UserIncome|null
     */
    public function getUserIncome(User $user = null, Criteria $criteria = null)
    {
        $qb = $this->createQueryBuilder('userIncome')
            ->where('userIncome.archive = 0');

        if ($user) {
            $qb
                ->andWhere('userIncome.user = :user')
                ->setParameter('user', $user);
        }

        if ($criteria) {
            $qb->addCriteria($criteria);
        }

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    /**
     * @param User $user
     * @param bool $archive
     * @return UserIncome|null
     */
    public function getLastForUser(User $user, $archive = false)
    {
        $qb = $this->createQueryBuilder('userIncome');
        $qb->where('userIncome.user = :user')
            ->andWhere('userIncome.archive = :archive')
            ->setParameter('user', $user)
            ->setParameter('archive', $archive)
            ->orderBy('userIncome.createdAt', 'DESC')
            ->setMaxResults(1);
        return $qb->getQuery()->getOneOrNullResult();
    }
}