<?php
/**
 * Created by PhpStorm.
 * User: mikhailpegasin
 * Date: 21.11.16
 * Time: 17:23
 */

namespace AppBundle\Repository;


use AppBundle\Entity\User;
use AppBundle\Entity\UserAddress;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Config\Definition\Exception\Exception;

class UserAddressRepository extends EntityRepository
{
    /**
     * The method selects an active userAddress for the user
     *
     * @param User $user
     * @return \DateTime
     */
    public function getEarlierMovedForUser(User $user)
    {
        $qb = $this->createQueryBuilder('userAddress');

        $qb
            ->select('userAddress.moved')
            ->where('userAddress.user = :user')
            ->andWhere('userAddress.archive = 0')
            ->setParameter('user', $user)
            ->orderBy('userAddress.moved')
            ->setMaxResults(1);

        try {
            $moved = $qb->getQuery()->getSingleResult();
        } catch (NoResultException $exception) {
            throw new Exception('Active userAddress does not found for current user');
        }

        return $moved['moved'];
    }

    /**
     * @param User $user
     * @return UserAddress|null
     */
    public function getLatestAddressForUser(User $user)
    {
        $qb = $this->createQueryBuilder('userAddress');
        $qb
            ->where('userAddress.user = :user')
            ->setParameter('user', $user)
            ->orderBy('userAddress.moved', 'DESC')
            ->setMaxResults(1);
        $address = $qb->getQuery()->getOneOrNullResult();
        return $address;
    }

    public function getUserActiveAddresses(User $user, Criteria $criteria = null)
    {
        $qb = $this->createQueryBuilder('userAddress');

        $qb
            ->select('userAddress')
            ->where('userAddress.user = :user')
            ->andWhere('userAddress.archive = 0')
            ->setParameter('user', $user);

        if ($criteria) {
            $qb->addCriteria($criteria);
        }

        $addresses = $qb->getQuery()->getResult();


        return $addresses;
    }
}