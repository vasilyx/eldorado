<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\UserSession;

class UserSessionRepository extends EntityRepository
{
    public function findUserByToken($token, $expirePeriod = 'PT4H')
    {
        $validCreatedAt = new \DateTime();
        $validCreatedAt->sub(new \DateInterval($expirePeriod));

        $session = $this->createQueryBuilder('s')
            ->where('s.token = :token')
            ->andWhere('s.createdAt > :date')
            ->setParameter('token', $token)
            ->setParameter('date', $validCreatedAt)
            ->getQuery()
            ->getOneOrNullResult()
            ;

        return $session ? $session->getUser() : null;
    }

    public function save(UserSession $session)
    {
        $this->getEntityManager()->persist($session);
        $this->getEntityManager()->flush($session);
    }

    public function removeByToken($token)
    {
        $session = $this->findOneByToken($token);

        $this->getEntityManager()->remove($session);
        $this->getEntityManager()->flush($session);
    }
}