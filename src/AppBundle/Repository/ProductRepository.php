<?php

namespace AppBundle\Repository;
use AppBundle\Entity\Product;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;


class ProductRepository  extends EntityRepository
{

    /**
     * @param Criteria|null $criteria
     * @return Product[]
     */
    public function getProducts(Criteria $criteria = null)
    {
        $qb = $this->createQueryBuilder('product');

        if ($criteria) {
            $qb->addCriteria($criteria);
        }

        $result = $qb
            ->getQuery()
            ->getResult()
        ;

        return $result;
    }

    /**
     * @deprecated Use getProductByType
     * @param integer $type
     * @return Product
     */
    public function getProduct($type)
    {
        return $this->getProductByCode($type);
    }


    /**
     * @param $type
     * @return Product[]
     */
    public function getProductsByType($type)
    {
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('product.type', $type))
        ;

        return $this->getProducts($criteria);
    }

    /**
     * @param $code
     * @return Product
     */
    public function getProductByCode($code)
    {
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('product.code', $code))
            ;

        return $this->getProducts($criteria)[0];
    }
}