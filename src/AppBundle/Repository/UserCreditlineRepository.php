<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 12.12.2016
 * Time: 18:58
 */

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use AppBundle\Entity\UserCreditline;
use Doctrine\ORM\EntityRepository;

class UserCreditlineRepository extends EntityRepository
{
    /**
     * @param User $user
     * @return UserCreditline[]
     */
    public function getActiveForUser(User $user)
    {
        $qb = $this->createQueryBuilder('c')
            ->where('c.user = :user')
            ->andWhere('c.archive = 0')
            ->setParameter('user', $user);
        return $qb->getQuery()->getResult();
    }

    /**
     * @param integer $id
     * @param User $user
     * @return UserCreditline
     */
    public function getActiveByIdAndUser($id, User $user)
    {
        return $this->findOneBy(['id' => $id, 'user' => $user, 'archive' => false]);
    }

    /**
     * @param User $user
     * @param array|null $orderBy
     * @return array
     */
    public function getListByUser(User $user, $orderBy = null)
    {
        return $this->findBy(['user' => $user, 'archive' => false], $orderBy);
    }
}