<?php

namespace AppBundle\Repository;

use AppBundle\Entity\UserSession;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function loadByToken($token)
    {
        return $this->getEntityManager()->getRepository(UserSession::class)
            ->findUserByToken($token);
    }

    public function loadByUsername($username)
    {
        return $this->findOneByUsername($username);
    }
}
