<?php
/**
 * Created by PhpStorm.
 * User: mikhailpegasin
 * Date: 28.11.16
 * Time: 14:19
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Communication;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCommunication;
use AppBundle\Exception\NotAvailableValueException;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;

class UserCommunicationRepository extends EntityRepository
{
    /**
     * @param       User $user
     * @param       Criteria|null $criteria
     * @param       bool $archive
     * @param       integer|null $type
     * @return UserCommunication[]
     * @throws NotAvailableValueException
     */
    public function getUserCommunication(User $user, Criteria $criteria = null, $archive = false, $type = null)
    {
        $qb = $this->createQueryBuilder('userCommunication')
            ->leftJoin('userCommunication.communication', 'communication')
            ->where('userCommunication.user = :user')
            ->setParameter('user', $user)
            ->orderBy('userCommunication.isPrimary', 'DESC');

        if (null !== $archive) {
            $qb
                ->andWhere('userCommunication.archive = :archive')
                ->setParameter('archive', $archive);
        }

        if ($type) {
            $qb
                ->andWhere('communication.type = :type')
                ->setParameter('type', $type);
        }

        if (null !== $type && !in_array($type, array_keys(Communication::getCommunicationTypes()))) {
            throw new NotAvailableValueException(NotAvailableValueException::NOT_AVAILABLE_COMMUNICATION_TYPE);
        }

        if ($criteria) {
            $qb->addCriteria($criteria);
        }

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    /**
     * @param User $user
     * @return UserCommunication
     */
    public function getPrimaryUnverifiedMobile(User $user)
    {
        $qb = $this->createQueryBuilder('userCommunication');
        $qb
            ->leftJoin('userCommunication.communication', 'communication')
            ->where('userCommunication.user = :user')
            ->andWhere('communication.type = ' . Communication::TYPE_MOBILE_PHONE)
            ->andWhere('userCommunication.isPrimary = 1')
            ->andWhere($qb->expr()->orX($qb->expr()->isNull('userCommunication.isVerified'), 'userCommunication.isVerified = 0'))
            ->setParameter('user', $user)
            ->setMaxResults(1);
        $result = $qb->getQuery()->getOneOrNullResult();
        return $result;
    }

}