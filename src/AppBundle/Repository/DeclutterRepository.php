<?php

namespace AppBundle\Repository;


use AppBundle\Entity\Declutter;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class DeclutterRepository extends EntityRepository
{
    /**
     * @param User $user
     * @return Declutter[]
     */
    public function getAllForDraftedLoanApply(User $user)
    {
        $loanApply = $this->getEntityManager()->getRepository('AppBundle:LoanApply')->getOneDrafted($user);
        $declutters = $this->findBy(['loanApply' => $loanApply, 'archive' => false]);
        return $declutters;
    }

    /**
     * @param LoanApply $loanApply
     * @return Declutter[]
     */
    public function getAllByLoanApply(LoanApply $loanApply)
    {
        $declutters = $this->findBy(['loanApply' => $loanApply, 'archive' => false]);
        return $declutters;
    }
}