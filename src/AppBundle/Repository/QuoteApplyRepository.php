<?php
/**
 * Created by PhpStorm.
 * User: mikhailpegasin
 * Date: 17.11.16
 * Time: 9:45
 */

namespace AppBundle\Repository;


use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\User;
use Application\FOS\RestBundle\Util\ExceptionWrapperHandler;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class QuoteApplyRepository extends EntityRepository
{
    /**
     * The method selects an active quoteApply for the user
     *
     * @deprecated
     * @param User $user
     * @param $statuses
     * @param Criteria $criteria
     * @return QuoteApply
     * @throws NoResultException
     */
    public function getActiveQuoteApplyForUser(User $user, $statuses = [QuoteApply::STATUS_DRAFT], Criteria $criteria = null)
    {
        $qb = $this->createQueryBuilder('quoteApply');

        $qb
            ->leftJoin('quoteApply.user', 'user')
            ->where('quoteApply.status IN (:status)')
            ->andWhere('user.id = :user_id')
            ->setParameter('user_id', $user->getId())
            ->setParameter('status', $statuses)
        ;

        if ($criteria) {
            $qb
                ->addCriteria($criteria);
        }

        $quoteApply = $qb->getQuery()->getResult();

        return $quoteApply;
    }

    /**
     * The methods selects all quoteApplies for user if $type is null, or quoteApplies with a need type product.
     *
     * @param User $user
     * @param null $statuses
     * @param null $type
     * @return QuoteApply[]
     */
    public function getQuoteAppliesForUser(User $user, $statuses = null, $type = null)
    {
        if (null === $statuses) {
            $statuses = QuoteApply::getAvailableStatuses();
        }

        $qb = $this->createQueryBuilder('quoteApply');

        $qb
            ->leftJoin('quoteApply.product', 'product')
            ->leftJoin('quoteApply.user', 'user')
            ->where('quoteApply.status IN (:status)')
            ->andWhere('user.id = :user_id')
            ->setParameter('user_id', $user->getId())
            ->setParameter('status', $statuses)
        ;


        if (null !== $type) {
            $qb
                ->andWhere('product.type = :type')
                ->setParameter('type', $type)
                ;
        }

        $quoteApply = $qb->getQuery()->getResult();

        return $quoteApply;

    }

    public function getQuoteApplyContractMatching()
    {
        /*$qb = $this->createQueryBuilder('quoteApply');

        $qb
            ->leftJoin('quoteApply.product', 'product')
            ->leftJoin('quoteApply.user', 'user')
            ->where('quoteApply.status IN (:status)')
            ->andWhere('user.id = :user_id')
            ->setParameter('user_id', $user->getId())
            ->setParameter('status', $statuses)
        ;


        if (null !== $type) {
            $qb
                ->andWhere('product.type = :type')
                ->setParameter('type', $type)
            ;
        }

        $quoteApply = $qb->getQuery()->getResult();

        return $quoteApply;*/
    }
}