<?php

namespace AppBundle\Repository;


use AppBundle\Entity\User;
use AppBundle\Entity\UserHousehold;
use Doctrine\ORM\EntityRepository;

class UserHouseholdRepository extends EntityRepository
{
    /**
     * @param User $user
     * @return UserHousehold
     */
    public function getOneByUser(User $user)
    {
        $qb = $this->createQueryBuilder('h')
            ->where('h.user = :user')
            ->andWhere('h.archive = 0')
            ->orderBy('h.createdAt', 'DESC')
            ->setParameter('user', $user)
            ->setMaxResults(1);
        return $qb->getQuery()->getOneOrNullResult();
    }
}