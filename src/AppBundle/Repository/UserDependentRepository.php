<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 11.12.2016
 * Time: 19:06
 */

namespace AppBundle\Repository;


use AppBundle\Entity\User;
use AppBundle\Entity\UserDependent;
use Doctrine\ORM\EntityRepository;

class UserDependentRepository extends EntityRepository
{
    public function getAllActiveDependentsForUser(User $user)
    {
        return $this->findBy(['user' => $user, 'archive' => false]);
    }
}