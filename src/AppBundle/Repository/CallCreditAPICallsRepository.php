<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 23.01.2017
 * Time: 16:53
 */

namespace AppBundle\Repository;


use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Config\Definition\Exception\Exception;

class CallCreditAPICallsRepository extends EntityRepository
{
    public function getApiCallByQuoteAndType(QuoteApply $quoteApply, $type, Criteria $criteria = null)
    {
        $qb = $this->createQueryBuilder('callCreditAPICalls');

        $qb
            ->select('callCreditAPICalls')
            ->where('callCreditAPICalls.quoteApply = :quoteApply')
            ->andWhere('callCreditAPICalls.type = :type')
            ->andWhere('callCreditAPICalls.status IN (:statuses)')
            ->setParameters(array(
                'quoteApply' => $quoteApply,
                'type' => $type,
                'statuses' => [
                    CallCreditAPICalls::STATUS_SUCCESS,
                    CallCreditAPICalls::STATUS_ERROR],

            ))
            ->orderBy('callCreditAPICalls.id', Criteria::DESC)
            ->setMaxResults(1);

        if ($criteria) {
            $qb->addCriteria($criteria);
        }

        try {
            $callCreditAPICalls = $qb->getQuery()->getOneOrNullResult();
        } catch (NoResultException $noResultException) {
            throw new Exception("CallCreditAPICalls not found for this quoteApply");
        }

        return $callCreditAPICalls;
    }

    public function getCallCreditForLoanByQuoteAndType(QuoteApply $quoteApply, $type, Criteria $criteria = null)
    {
        $qb = $this->createQueryBuilder('callCreditAPICalls');

        $qb
            ->select('callCreditAPICalls')
            ->where('callCreditAPICalls.quoteApply = :quoteApply')
            ->andWhere('callCreditAPICalls.type = :type')
            ->andWhere('callCreditAPICalls.purpose IN (:purposes)')
            ->setParameters(array(
                             'quoteApply' => $quoteApply,
                             'type' => $type,
                             'purposes' => [CallCreditAPICalls::PURPOSE_CA,
                                            CallCreditAPICalls::PURPOSE_QS,
                                            CallCreditAPICalls::PURPOSE_MN],

            ))
            ->orderBy('callCreditAPICalls.id', Criteria::DESC)
            ->setMaxResults(1);

        if ($criteria) {
            $qb->addCriteria($criteria);
        }

        try {
            $callCreditAPICalls = $qb->getQuery()->getResult();
        } catch (NoResultException $noResultException) {
            throw new Exception("CallCreditAPICalls not found for this quoteApply");
        }

        return $callCreditAPICalls;
    }

    /**
     * Get ID Check for investor (callvalidate request)
     *
     * @param User $user
     * @param Criteria|null $criteria
     * @return array
     */
    public function getLatestCallValidateForInvestor(User $user, Criteria $criteria = null)
    {
        $qb = $this->createQueryBuilder('callCreditAPICalls');

        $qb
            ->select('callCreditAPICalls')
            ->where('callCreditAPICalls.type = :type')
            ->andWhere('callCreditAPICalls.purpose = :purpose')
            ->setParameters([
                    'type' => CallCreditAPICalls::TYPE_CALLVALIDATE,
                    'purpose' => CallCreditAPICalls::PURPOSE_QS
                ]
            )
            ->setMaxResults(1);

        if ($criteria) {
            $qb->addCriteria($criteria);
        }

        try {
            /** @var CallCreditAPICalls $callCreditAPICalls */
            $callCreditAPICalls = $qb->getQuery()->getOneOrNullResult();
        } catch (NoResultException $noResultException) {
            throw new Exception("CallCreditAPICalls not found for this quoteApply");
        }

        $result = [
            'xml' => $callCreditAPICalls->getResponseXML(),
            'response_date' => $callCreditAPICalls->getResponseDate(),
            'request_id' => $callCreditAPICalls->getRequestID(),
            'status' => CallCreditAPICalls::getStatuses()[$callCreditAPICalls->getStatus()]
        ];

        return $result;
    }
}