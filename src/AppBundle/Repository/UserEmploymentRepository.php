<?php

namespace AppBundle\Repository;


use AppBundle\Entity\User;
use AppBundle\Entity\UserEmployment;
use Doctrine\ORM\EntityRepository;

class UserEmploymentRepository extends EntityRepository
{
    /**
     * @param User $user
     * @return array
     */
    public function getEmploymentsForUser(User $user)
    {
        $qb = $this->getQueryBuilderForActiveEmployments($user);
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * return current employments first (ordered by started date), and than non current (ordered by ended date)
     * @param User $user
     * @return array
     */
    public function getOrderedEmploymentsForUser(User $user)
    {
        $qbCurrent = $this->getQueryBuilderForActiveEmployments($user);
        $qbCurrent->andWhere('e.currentStatus = 1')
            ->orderBy('e.occMonthStarted', 'DESC');
        $resultCurrent = $qbCurrent->getQuery()->getResult();

        $qbPrevious = $this->getQueryBuilderForActiveEmployments($user);
        $qbPrevious->andWhere('e.currentStatus = 0')
            ->orderBy('e.occMonthEnded', 'DESC');
        $resultPrevious = $qbPrevious->getQuery()->getResult();

        $result = array_merge($resultCurrent, $resultPrevious);
        return $result;
    }

    /**
     * @param User $user
     * @return integer
     */
    public function countCurrentEmploymentForUser(User $user)
    {
        $qb = $this->getQueryBuilderForActiveEmployments($user);
        $qb->andWhere('e.currentStatus = 1')
            ->select('count(e.id)');
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function getQueryBuilderForActiveEmployments(User $user)
    {
        $qb = $this->createQueryBuilder('e')
            ->where('e.user = :user')
            ->andWhere('e.archive = 0')
            ->setParameter('user', $user);
        return $qb;
    }

    /**
     * @param $user
     * @return UserEmployment
     */
    public function getEarlierStartedForUser($user)
    {
        $qb = $this->createQueryBuilder('e')
            ->where('e.user = :user')
            ->andWhere('e.archive = 0')
            ->setParameter('user', $user)
            ->orderBy('e.occMonthStarted', 'ASC')
            ->setMaxResults(1);;
        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param $user
     * @return UserEmployment[]
     */
    public function getCurrentsForUser($user)
    {
        $qb = $this->createQueryBuilder('e')
            ->where('e.user = :user')
            ->andWhere('e.currentStatus = 1')
            ->andWhere('e.archive = 0')
            ->setParameter('user', $user);
        return $qb->getQuery()->getResult();
    }

    /**
     * @param integer $id
     * @param User $user
     * @return UserEmployment
     */
    public function getActiveByIdAndUser($id, User $user)
    {
        return $this->findOneBy(['id' => $id, 'user' => $user, 'archive' => false]);
    }
}