<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 1.12.16
 * Time: 8.50
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Agreement;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use AppBundle\Traits\CriteriaAddable;
use AppBundle\Traits\CriteriaAddableInterface;
use Doctrine\ORM\QueryBuilder;

class InvestOfferRepository extends EntityRepository implements CriteriaAddableInterface
{
    use CriteriaAddable;

    public function getInvestOfferForUser(User $user, Criteria $criteria = null)
    {
        $qb = $this->createQueryBuilder('investOffer')
            ->where('investOffer.user = :user')
            ->andWhere('investOffer.status not in (:statuses)')
            ->setParameter('user', $user)
            ->setParameter('statuses', [InvestOffer::STATUS_CANCEL])
            ->setMaxResults(1)
            ;

        $qb = $this->addCriteriaToQueryBuilder($qb, $criteria);
        
        $result = $qb->getQuery()->getOneOrNullResult();

        return $result;
    }

    /**
     * Get Drafted invest offers
     *
     * @param User $user
     * @param Criteria|null $criteria
     * @return array
     */
    public function getPendingInvestOffersForUser(User $user, Criteria $criteria = null)
    {
        $qb = $this->createQueryBuilder('investOffer')
            ->where('investOffer.user = :user')
            ->andWhere('investOffer.status in (:statuses)')
            ->setParameter('user', $user)
            ->setParameter('statuses', [InvestOffer::STATUS_PENDING_FUNDS, InvestOffer::STATUS_DRAFT, InvestOffer::STATUS_PENDING])
        ;

        if (!empty($criteria)) {
            $qb = $this->addCriteriaToQueryBuilder($qb, $criteria);
        }

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getInvestOfferListContractMatching(Agreement $agreement)
    {
        $terms = $this->getOfferTermLimits($agreement->getTerm());
        $qb = $this->createQueryBuilder('investOffer')
             ->andWhere('investOffer.user <> :user')
             ->andWhere('investOffer.status = :status')
             ->andWhere('investOffer.term >= :termStart AND investOffer.term <= :termEnd')
             ->setParameter('status', InvestOffer::STATUS_ACTIVE)
             ->setParameter('termStart', $terms[0])
             ->setParameter('termEnd', $terms[1])
             ->setParameter('user', $agreement->getLoanApply()->getUser())
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $term
     * @return array
     */
    protected function getOfferTermLimits($term)
    {
        $start = 0;
        $end = 0;

        if ($term <= 12) {
            $start = 0;
            $end = 12;
        }

        if ($term > 12 && $term <= 24) {
            $start = 13;
            $end = 24;
        }

        if ($term > 24 && $term <= 36) {
            $start = 25;
            $end = 36;
        }

        if ($term > 36 && $term <= 48) {
            $start = 37;
            $end = 48;
        }

        if ($term > 48) {
            $start = 49;
            $end = 60;
        }


        return [$start, $end];
    }
}