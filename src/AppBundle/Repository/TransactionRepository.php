<?php
namespace AppBundle\Repository;

use AppBundle\Entity\Agreement;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Event\TransactionEvents;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use phpDocumentor\Reflection\Types\Integer;


class TransactionRepository extends EntityRepository
{

    public function getPendingCreditLines()
    {
        $qb = $this->createQueryBuilder('cm')
            ->select('cm')
            ->leftJoin('AppBundle\Entity\Transaction', 'io', Join::WITH, 'io.parentTransaction = cm.id')
            ->andWhere('cm.glAccount = ' . Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_BORROWER)
            ->andWhere('cm.transactionSubAccount = ' .  Transaction::TRANSACTION_SUB_ACCOUNT_SETTLING)
            ->andWhere('cm.transactionOwner = ' .  Transaction::TRANSACTION_OWNER_BORROWER)
            ->andWhere('cm.transactionAccount = ' .  Transaction::TRANSACTION_ACCOUNT_CASH_AVAILABLE)
            ->andWhere('cm.parentTransaction IS NULL AND io.parentTransaction IS NULL')
        ;

        return $qb->getQuery()->getResult();
    }

    public function getPendingCreditLinesUser(User $user)
    {
        $qb = $this->createQueryBuilder('cm')
            ->select('cm')
            ->leftJoin('AppBundle\Entity\Transaction', 'io', Join::WITH, 'io.parentTransaction = cm.id')
            ->andWhere('cm.user = ' . $user)
            ->andWhere('cm.glAccount = ' . Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_BORROWER)
            ->andWhere('cm.transactionSubAccount = ' .  Transaction::TRANSACTION_SUB_ACCOUNT_SETTLING)
            ->andWhere('cm.transactionOwner = ' .  Transaction::TRANSACTION_OWNER_BORROWER)
            ->andWhere('cm.transactionAccount = ' .  Transaction::TRANSACTION_ACCOUNT_CASH_AVAILABLE)
            ->andWhere('cm.parentTransaction IS NULL AND io.parentTransaction IS NULL')
        ;

        return $qb->getQuery()->getResult();
    }

    public function getPortfolioAmount($type, User $user)
    {
        $qb = $this->createQueryBuilder('cm')
            ->select('SUM(cm.transactionAmount)')
            ->andWhere('cm.user=:user')
            ->andWhere('cm.glAccount = ' . Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR)
            ->andWhere('cm.investmentType = ' . $type)
            ->setParameter('user', $user);

        $res = $qb->getQuery()->getOneOrNullResult();
        $res1 =  is_array($res) ? (float)array_shift($res) : 0;

        $qb = $this->createQueryBuilder('cm')
            ->select('SUM(cm.transactionAmount)')
            ->andWhere('cm.user=:user')
            ->andWhere('cm.transactionSubAccount = ' . Transaction::TRANSACTION_SUB_ACCOUNT_CASH_PENDING)
            ->andWhere('cm.investmentType = ' . $type)
            ->setParameter('user', $user);

        $res = $qb->getQuery()->getOneOrNullResult();
        $res2 =  is_array($res) ? (float)array_shift($res) : 0;

        return $res1 - $res2;
    }

    public function getCashAvailable($type, User $user, $glAccount = Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR)
    {
        $qb = $this->createQueryBuilder('cm')
            ->select('SUM(cm.transactionAmount)')
            ->andWhere('cm.user=:user')
            ->andWhere('cm.investmentType = ' . $type)
            ->andWhere('cm.glAccount = ' . $glAccount)
            ->andWhere('(cm.transactionSubAccount = ' . Transaction::TRANSACTION_SUB_ACCOUNT_CASH_AVAILABLE . ' OR ' .
                'cm.transactionSubAccount = ' . Transaction::TRANSACTION_SUB_ACCOUNT_CASH_OFFER . ')'
            )
            ->groupBy('cm.user')
            ->setParameter('user', $user);

        $res = $qb->getQuery()->getOneOrNullResult();
        return is_array($res) ? (float)array_shift($res) : 0;
    }

    public function getOfferPending($type, User $user, $glAccount = Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR)
    {
        $qb = $this->createQueryBuilder('cm')
            ->select('SUM(cm.transactionAmount)')
            ->andWhere('cm.user=:user')
            ->andWhere('cm.investmentType = ' . $type)
            ->andWhere('cm.glAccount = ' . $glAccount)
            ->andWhere('cm.transactionSubAccount = ' . Transaction::TRANSACTION_SUB_ACCOUNT_CASH_ALLOC)
            ->groupBy('cm.user')
            ->setParameter('user', $user);

        $res = $qb->getQuery()->getOneOrNullResult();
        return is_array($res) ? (float)array_shift($res) : 0;
    }

    public function getInterestEarned($type, User $user)
    {
        $qb = $this->createQueryBuilder('cm')
            ->select('SUM(cm.transactionAmount)')
            ->andWhere('cm.user=:user')
            ->andWhere('cm.investmentType = ' . $type)
            ->andWhere('cm.glAccount = ' . Transaction::TRANSACTION_GLACCOUNT_LOAN_ACCOUNT_INVESTOR)
            ->andWhere('cm.transactionSubAccount = ' . Transaction::TRANSACTION_SUB_ACCOUNT_INTEREST)
            ->setParameter('user', $user);

        $res = $qb->getQuery()->getOneOrNullResult();


        return is_array($res) ? (float)array_shift($res) : 0;
    }

    public function getOutstandingLoans($type, User $user)
    {
        $qb = $this->createQueryBuilder('cm')
            ->select('SUM(cm.transactionAmount)')
            ->andWhere('cm.user=:user')
            ->andWhere('cm.investmentType = ' . $type)
            ->andWhere('cm.glAccount = ' . Transaction::TRANSACTION_GLACCOUNT_LOAN_ACCOUNT_INVESTOR)
            ->andWhere('( cm.transactionSubAccount = ' . Transaction::TRANSACTION_SUB_ACCOUNT_PRINCIPLE . ' OR ' .
                     'cm.transactionSubAccount = ' . Transaction::TRANSACTION_SUB_ACCOUNT_CASH_REINVEST . ')'
            )
            ->setParameter('user', $user);

        $res = $qb->getQuery()->getOneOrNullResult();
        return is_array($res) ? (float)array_shift($res) : 0;
    }

    public function getPendingWithdrawals()
    {
        $qb = $this->createQueryBuilder('cm')
            ->select('cm')
            ->leftJoin('AppBundle\Entity\Transaction', 'io', Join::WITH, 'io.parentTransaction = cm.id')
            ->leftJoin('AppBundle\Entity\Event', 'ev', Join::WITH, 'cm.event=ev.id')
            ->andWhere('ev.type = :eventType1')
            ->andWhere('cm.parentTransaction IS NULL AND io.parentTransaction IS NULL')
            ->andWhere('cm.transactionSubAccount = ' . Transaction::TRANSACTION_SUB_ACCOUNT_CASH_WITHDRAW)
            ->andWhere('cm.transactionAccount = ' . Transaction::TRANSACTION_ACCOUNT_CASH_AVAILABLE)
            ->setParameter('eventType1',TransactionEvents::WDRC)
            ;
         ;

        return $qb->getQuery()->getResult();
    }


    /**
     * @param $agreementId
     * @return int
     */
    public function getBalanceByAgreement($agreementId)
    {
        $qb = $this->createQueryBuilder('cm')
            ->select('SUM(cm.transactionAmount)')
            ->andWhere('cm.agreementId=:agreementId')
            ->andWhere('cm.glAccount = ' . Transaction::TRANSACTION_GLACCOUNT_LOAN_ACCOUNT_BORROWER)
            ->andWhere('cm.transactionSubAccount = ' . Transaction::TRANSACTION_SUB_ACCOUNT_PRINCIPLE)
            ->setParameter('agreementId', $agreementId)
        ;


        return (int)$qb->getQuery()->getSingleScalarResult();
    }
}