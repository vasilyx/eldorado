<?php

namespace AppBundle\Repository;

use AppBundle\Entity\LoanApply;
use AppBundle\Entity\Product;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


class LoanApplyRepository extends EntityRepository
{
    /**
     * @param User $user
     * @return LoanApply
     */
    public function getOneByApprovedQuote(User $user)
    {
        $query = $this->createQueryBuilder('l')
            ->select()
            ->leftJoin('l.quoteApply', 'q', 'WITH', 'l.quoteApply = q.id')
            ->leftJoin('q.user', 'u', 'WITH', 'q.user = u.id')
            ->leftJoin('q.product', 'p', Join::WITH, 'q.product = p.id')
            ->where('u.id = :user')
            ->andWhere('p.type = :product_type')
            ->andWhere('q.status = :status')
            ->setParameters(['user' => $user->getId(), 'product_type' => Product::TYPE_BORROWER, 'status' => QuoteApply::STATUS_APPROVED])
            ->setMaxResults(1)
            ->getQuery();
        $loanApply = $query->getOneOrNullResult();
        return $loanApply;
    }

    /**
     * @param User $user
     * @return LoanApply
     */
    public function getOneDrafted(User $user)
    {
        $loanApplys = $this->findBy(['user' => $user, 'status' => LoanApply::STATUS_DRAFT]);
        if (!$loanApplys) {
            return null;
        } elseif (count($loanApplys) > 1) {
            throw new BadRequestHttpException('Wrong count of draft Loans in the system');
        } else {
            return $loanApplys[0];
        }
    }

    /**
     * @param User $user
     * @param null $statuses
     * @return LoanApply[]
     */
    public function getLoanApplyForUser(User $user, $statuses = null)
    {
        if (null === $statuses) {
            $statuses = LoanApply::getStatusCodes();
        }

        $qb = $this->createQueryBuilder('loanApply');

        $qb
            ->where('loanApply.status IN (:statuses)')
            ->andWhere('loanApply.user = :user')
            ->setParameter('user', $user)
            ->setParameter('statuses', $statuses)
            ->setMaxResults(1)
        ;

        $loanApply = $qb->getQuery()->getOneOrNullResult();

        return $loanApply;
    }
}