<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 01.02.2017
 * Time: 13:53
 */

namespace AppBundle\Repository;


use AppBundle\Entity\CallCreditCredentials;
use Doctrine\ORM\EntityRepository;

class CallCreditCredentialsRepository extends EntityRepository
{
    public function getCredentialsForApiByType($type)
    {
        /** @var CallCreditCredentials $credentials */
        $credentials = $this->findOneByType($type);

        if (!$credentials) {
            return null;
        }

        return array(
                  'headers' => array(
                      'company' => $credentials->getCompany(),
                      'username' => $credentials->getLogin(),
                      'password' => $credentials->getPassword()
                  ),
                  'url' => $credentials->getUrl(),
                  'application' => $credentials->getApplicationName()
        );
    }
}