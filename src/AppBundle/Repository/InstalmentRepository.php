<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 15.06.2018
 * Time: 14:48
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Instalment;
use AppBundle\Entity\LoanApply;
use Doctrine\ORM\EntityRepository;

class InstalmentRepository extends EntityRepository
{
    public function getLateInstalments()
    {
        $qb = $this->createQueryBuilder('i')
                ->where('i.paymentStatus in (:statuses)')
                ->andWhere('i.createdAt < :date')
                ->setParameter('statuses', [Instalment::PAYMENT_STATUS_PENDING])
                ->setParameter('date', new \DateTime('-10 days'))
        ;

        return $qb->getQuery()->getResult();
    }

    public function getNotPaidInstalmentsForLoan(LoanApply $loanApply)
    {
        $qb = $this->createQueryBuilder('i')
                ->where('i.paymentStatus != :status')
                ->andWhere('i.loanApply = :loan')
                ->setParameter('status', [Instalment::PAYMENT_STATUS_PAID])
                ->setParameter('loan', $loanApply)
            ;

        return $qb->getQuery()->getResult();
    }

    public function getMissedInstalments()
    {
        $qb = $this->createQueryBuilder('i')
            ->where('i.paymentStatus in (:statuses)')
            ->andWhere('i.createdAt < :date')
            ->setParameter('statuses', [Instalment::PAYMENT_STATUS_LATE, Instalment::PAYMENT_STATUS_PENDING])
            ->setParameter('date', new \DateTime('-30 days'))
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $loanApply
     * @return mixed
     */
    public function getLastInstalmentByLoanApply($loanApply)
    {
        $qb = $this->createQueryBuilder('i')
            ->where('i.loanApply = :loanApply')
            ->setParameter('loanApply', $loanApply)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(1)
        ;

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param $loanApply
     * @return mixed
     */
    public function getInstalmentsByLoanApply($loanApply)
    {
        $qb = $this->createQueryBuilder('i')
            ->where('i.loanApply = :loanApply')
            ->andWhere('i.paymentStatus in (:statuses)')
            ->andWhere('i.installmentStatus in (:installmentStatus)')
            ->setParameter('statuses', [Instalment::PAYMENT_STATUS_LATE, Instalment::PAYMENT_STATUS_MISSED])
            ->setParameter('installmentStatus', [Instalment::INSTALLMENT_STATUS_OUTSTANDING, Instalment::INSTALLMENT_STATUS_PART_CLEARED])
            ->setParameter('loanApply', $loanApply)
            ->orderBy('i.id', 'ASC')
        ;

        return $qb->getQuery()->getResult();
    }
}