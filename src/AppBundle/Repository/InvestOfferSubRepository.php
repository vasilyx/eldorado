<?php
namespace AppBundle\Repository;

use AppBundle\Entity\InvestOffer;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class InvestOfferSubRepository extends EntityRepository
{
    public function getNotMatched(InvestOffer $invest_offer)
    {
        $qb = $this->createQueryBuilder('investOfferSub')
            ->select('investOfferSub')
            ->leftJoin('AppBundle\Entity\ContractMatching', 'cm',
                Expr\Join::WITH,
                'investOfferSub.id = cm.investOfferSub')
            ->andWhere('investOfferSub.investOffer=:investOffer AND cm.id IS NULL')
            ->setParameter('investOffer', $invest_offer)
        ;

        return $qb->getQuery()->getResult();
    }
}