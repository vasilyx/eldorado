<?php
namespace AppBundle\Repository;

use AppBundle\Entity\LoanApply;
use AppBundle\Entity\QuoteApply;
use Doctrine\ORM\EntityRepository;

class AgreementRepository extends EntityRepository
{

    /**
     * @return array
     */
    public function getContractMatchingAgreement()
    {
        $qb = $this->createQueryBuilder('agreement')
                ->andWhere('agreement.acceptanceDate IS NOT NULL')
                ->andWhere('agreement.fullyMatchedDate IS NULL')
                ->andWhere('agreement.isDeclined=0')
                ->orderBy('agreement.acceptanceDate', 'DESC')
            ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param LoanApply $loanApply
     * @return mixed
     */
    public function getAgreementLoanApply(LoanApply $loanApply)
    {
        return $this->findOneBy(['loanApply' => $loanApply]);
    }
}