<?php
namespace AppBundle\Command;


use AppBundle\Entity\Instalment;
use AppBundle\Entity\LoanApply;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateInstalmentCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:update-instalment')
            ->setDescription('This command creates instalment');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $instalmentRepository = $em->getRepository(Instalment::class);

        $lateInstalments = $instalmentRepository->getLateInstalments();

        /** @var Instalment $lateInstalment */
        foreach ($lateInstalments as $lateInstalment)
        {
            $lateInstalment->setPaymentStatus(Instalment::PAYMENT_STATUS_LATE);
            $em->persist($lateInstalment);
        }

        $missedInstalments = $instalmentRepository->getMissedInstalments();

        /** @var Instalment $missedInstalment */
        foreach ($missedInstalments as $missedInstalment)
        {
            $missedInstalment->setPaymentStatus(Instalment::PAYMENT_STATUS_MISSED);
            $em->persist($missedInstalment);
        }

        $em->flush();
    }
}