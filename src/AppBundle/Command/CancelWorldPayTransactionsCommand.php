<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 30.05.2018
 * Time: 10:53
 */

namespace AppBundle\Command;


use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\WorldpayTransaction;
use AppBundle\Repository\WorldPayTransactionRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CancelWorldPayTransactionsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:cancel_worldpay_transactions')
            ->setDescription('This command 1 time per 1 hour cancel pending worldpay transactions');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        /** @var WorldPayTransactionRepository $worldPayRepo */
        $worldPayRepo = $doctrine->getRepository(WorldpayTransaction::class);
        $date = new \DateTime();
        $date->modify('-1 hour');
        $qb = $worldPayRepo->createQueryBuilder('transaction')
            ->where('transaction.paymentStatus IN (:status)')
            ->andWhere('transaction.createdAt < :date')
            ->setParameter('date', $date)
            ->setParameter('status', [WorldpayTransaction::PAYMENT_STATUS_PENDING])
        ;
        $em = $doctrine->getManager();
        if ($results = $qb->getQuery()->getResult()) {
            if (count($results)) {
                /** @var WorldpayTransaction $result */
                foreach ($results as $result) {
                    $result->setPaymentStatus(WorldpayTransaction::PAYMENT_STATUS_CANCELLED);
                    $em->persist($result);
                }
            }
        }

        $em->flush();
    }
}