<?php

namespace AppBundle\Command;

use AppBundle\Entity\LoanApply;
use AppBundle\Entity\QuoteApply;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ChangeQuoteStatusCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:change_quote_status_command')
            ->setDescription('This command changes status of a quote from pending to approved or rejected according to status generator returning');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $qb = $doctrine->getRepository('AppBundle:QuoteApply')->createQueryBuilder('quoteApply')
            ->where('quoteApply.status IN (:status)')
            ->setParameter('status', [QuoteApply::STATUS_PENDING])
            ->setMaxResults(1)
        ;
        if ($qa = $qb->getQuery()->getOneOrNullResult()) {
            $qa->setStatus($this->getContainer()->get('app.utils.quote_status_random_generator')->getStatus());
            $loanApply = LoanApply::create()
                ->setStatus(LoanApply::STATUS_DRAFT)
                ->setQuoteApply($qa)
                /**
                 * The QuoteApply status may be changed from action which does not need authorisation
                 */
                ->setUser($qa->getUser());
            $doctrine->getManager()->persist($loanApply);
            $doctrine->getManager()->flush();
        }
    }
}
