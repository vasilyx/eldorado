<?php

namespace AppBundle\Command;

use AppBundle\Entity\LoanRequest;
use AppBundle\Entity\QuoteApply;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ContractMatchingCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:contract-matching')
            ->setDescription('This command match invert and loan contract and fill contract_matching table');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('whs.manager.contract_matching_manager')->matchOffers();
    }
}
