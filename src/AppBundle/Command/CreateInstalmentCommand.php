<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 15.06.2018
 * Time: 15:26
 */

namespace AppBundle\Command;

use AppBundle\Entity\Agreement;
use AppBundle\Entity\Instalment;
use AppBundle\Entity\LoanApply;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateInstalmentCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:create-instalment')
            ->setDescription('This command creates instalment')
            ->addArgument('agreement', InputArgument::OPTIONAL, 'Agreement ID', null)
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $agreementId = $input->getArgument('agreement');
        $agreementRepository = $em->getRepository(Agreement::class);

        $instalmentManager = $this->getContainer()->get('whs.manager.instalments_manager');

        if (!empty($agreementId)) {
            $agreement = $agreementRepository->find($agreementId);

            if (!empty($agreement)) {
                $instalmentManager->createInstalment($agreement);
            }

        } else {
            $loanApplies = $em->getRepository(LoanApply::class)->findBy(['status' => LoanApply::STATUS_ACTIVE]);
            $currentDayNumber = date('j');

            foreach ($loanApplies as $loanApply) {
                /** @var Agreement $agreement */
                $agreement = $agreementRepository->findOneBy(['loanApply' => $loanApply, 'paymentDay' => $currentDayNumber]);

                if (!empty($agreement)) {
                    $instalmentManager->createInstalment($agreement);
                }
            }
        }
    }
}