<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 06.06.2018
 * Time: 11:16
 */

namespace AppBundle\Command;


use AppBundle\Entity\File;
use AppBundle\Entity\QuoteApply;
use AppBundle\Manager\FileManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GeneratePDFCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:generate_pdf')
            ->setDescription('This command generates pdf files');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $files = $em->getRepository(File::class)->findBy(['saved' => false]);

        /** @var FileManager $fileManager */
        $fileManager = $this->getContainer()->get('whs.manager.file_manager');

        foreach ($files as $file) {
            $fileManager->savePdfFile($file);
        }
    }
}