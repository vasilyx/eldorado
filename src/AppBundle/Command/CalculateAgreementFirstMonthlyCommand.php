<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 26.06.2018
 * Time: 12:41
 */

namespace AppBundle\Command;


use AppBundle\Entity\Agreement;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CalculateAgreementFirstMonthlyCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:calculate_agreement_fmp')
            ->setDescription('This command calculates first monthly payment for agreement with fmp = NULL');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $agreements = $em->getRepository(Agreement::class)->findAll();//$em->getRepository(Agreement::class)->findByFirstMonthlyPayment(null);
        $paymentsComputer = $this->getContainer()->get('app.utils.payments_computer');

        /** @var Agreement $agreement */
        foreach ($agreements as $agreement) {
            $creditPayments = $paymentsComputer->getCreditPaymentsTotal($agreement->getApr(), $agreement->getTerm(), $agreement->getAmount());
            $agreement->setFirstMonthlyPayment($creditPayments['firstMonthly']);

            $em->persist($agreement);
        }

        $em->flush();
    }
}