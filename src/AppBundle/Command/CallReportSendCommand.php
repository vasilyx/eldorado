<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 12.01.2017
 * Time: 16:20
 */

namespace AppBundle\Command;


use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\QuoteApply;
use AppBundle\Event\CallCreditEvents;
use Doctrine\Common\Collections\Criteria;
use SoapFault;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class CallReportSendCommand
 * @package AppBundle\Command
 */
class CallReportSendCommand extends BaseCallSendCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();
        $this
            ->setName('app:call_report_send')
            ->setDescription('This command send callreport request')
            ->addOption('test-score', sprintf('%1$s - approved, %2$s - rejected, %3$s - referred', QuoteApply::GUAGE_SCORE_APPROVE, QuoteApply::GUAGE_SCORE_REJECT, '500'))
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setAnonToken();
        $doctrine = $this->getContainer()->get('doctrine');

        if ($callCredit = $doctrine->getRepository(CallCreditAPICalls::class)->findBy(['status' => CallCreditAPICalls::STATUS_PENDING, 'type' => CallCreditAPICalls::TYPE_CALLREPORT], ['id' => Criteria::DESC], 1)) {
            if (is_array($callCredit)) {
                $callCredit = $callCredit[0];
            }

            //$this->getContainer()->get('app.callreport_process_request')->sendApiRequest($callCredit, $this->getTestMode($input), $input->getOption('test-score'));
            $this->getContainer()->get('app.callreport_process_request')->sendApiRequest($callCredit);
        }
    }
}