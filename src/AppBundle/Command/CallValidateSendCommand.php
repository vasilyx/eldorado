<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 12.01.17
 * Time: 16:25
 */

namespace AppBundle\Command;


use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\RabbitMQ\Consumer\CallvalidateConsumer;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CallValidateSendCommand extends BaseCallSendCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();
        $this
            ->setName('app:call_validate_send')
            ->setDescription('This command sends callvalidate')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setAnonToken();

        $doctrine = $this->getContainer()->get('doctrine');

        if ($callCredit = $doctrine->getRepository('AppBundle:CallCreditAPICalls')->findOneBy(['status' => CallCreditAPICalls::STATUS_PENDING, 'type' => CallCreditAPICalls::TYPE_CALLVALIDATE], ['id' => Criteria::DESC])) {

            //TODO: uncomment it
            //$this->getContainer()->get('app.callvalidate_process_request')->sendApiRequest($callCredit, $this->getTestMode($input));
            $this->getContainer()->get('app.callvalidate_process_request')->sendApiRequest($callCredit);
        }
    }
}