<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 01.02.2017
 * Time: 10:50
 */

namespace AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CallReportChangePasswordCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();
        $this
            ->setName('app:call_report_change_password')
            ->setDescription('This command change password for CallReport API')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('app.callreport_process_request')->changePassword();
    }
}