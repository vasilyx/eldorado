<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 18.01.2017
 * Time: 16:55
 */

namespace AppBundle\Command;


use AppBundle\Entity\CallCreditAPICalls;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AffordabilitySendCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:affordability_send')
            ->setDescription('This command send affordability request');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');

        if ($callCredit = $doctrine->getRepository(CallCreditAPICalls::class)->findBy(['status' => CallCreditAPICalls::STATUS_PENDING, 'type' => CallCreditAPICalls::TYPE_AFFORDABILITY], ['id' => Criteria::DESC], 1)) {
            if (is_array($callCredit)) {
                $callCredit = $callCredit[0];
            }

            $this->getContainer()->get('app.affordability_process_request')->sendApiRequest($callCredit);
        }
    }
}