<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 20.01.17
 * Time: 9:59
 */

namespace AppBundle\Command;


use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;

abstract class BaseCallSendCommand extends ContainerAwareCommand
{
    public function __construct()
    {
        parent::__construct();

    }

    protected function configure()
    {
        parent::configure();
        $this
            ->addOption('test-mode', sprintf('%1$s - SUCCESS, %2$s - ERROR', CallCreditAPICalls::STATUS_SUCCESS, CallCreditAPICalls::STATUS_ERROR))
        ;
    }

    protected function getTestMode(InputInterface $input)
    {
        $testMode = $input->getOption('test-mode');
        if (!in_array($testMode, [
            CallCreditAPICalls::STATUS_SUCCESS,
            CallCreditAPICalls::STATUS_ERROR
        ])) {
            $testMode = CallCreditAPICalls::STATUS_SUCCESS;
        }

        return $testMode;
    }

    /**
     * Sets anonymous token to be able to work with isGranted() function
     */
    protected function setAnonToken()
    {
        if (null === $this->getContainer()->get('security.token_storage')->getToken()) {
            $this->getContainer()->get('security.token_storage')->setToken(new AnonymousToken('secret', new User()));
        }
    }
}