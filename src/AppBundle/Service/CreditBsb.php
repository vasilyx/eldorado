<?php

namespace AppBundle\Service;


use AppBundle\Libs\CreditReportBSBApi;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


class CreditBsb
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var CreditReportBSBApi
     */
    protected $BSBApi;

    /**
     * @var string
     */
    protected $requestXml = '';

    /**
     * @var string
     */
    protected $responseXml = '';

    protected $changePwdAttemptsCnt = 0;

    protected $debug = false;

    /**
     * @param ContainerInterface $container
     * @param bool $debug
     */
    public function __construct(ContainerInterface $container, $debug = false)
    {
        $this->debug = $debug;
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getManager();
        $this->BSBApi = new CreditReportBSBApi();
    }

    /**
     * Set Headers
     * @return string
     */
    protected function setHeader()
    {
        $company = $this->container->getParameter('api_bsbacr_company');
        $username = $this->container->getParameter('api_bsbacr_username');
        $password = $this->getPassword();
        $this->BSBApi->setHeader($company, $username, $password);
    }


    public function caValidateApplication(LoanApplication $loanApplication, $modifiedSearchParameters = null)
    {
        return $this->processResponse($loanApplication);
    }

    function setSoapClient()
    {
        $proxyHost = $this->container->getParameter('api_proxy_host');
        $proxyPort = $this->container->getParameter('api_proxy_port');
        if (!$proxyHost || !$proxyPort) {
            $proxyHost = null;
            $proxyPort = null;
        }
        if ($this->container->hasParameter('api_bsbacr_url')) {
            $apiUrl = trim($this->container->getParameter('api_bsbacr_url'));
        } else {
            $apiUrl = null;
        }
        $this->setHeader();
        $this->BSBApi->setClient($proxyHost, $proxyPort, $apiUrl, $this->debug);
    }

    /**
     * @param array $data
     * @param null $xmlId
     * @param null $modifiedSearchParameters
     * @return bool
     */
    public function processRequestForReport($data = [], $xmlId = null, $modifiedSearchParameters = null)
    {
        $requestHelper = new RequestHelper();
        $requestHelper->setApplicantEmail($data["email"]);
        $requestHelper->addAddress($data["address"]);

        $n = 0;
        if ($data["home_phone"]) {
            if ($requestHelper->setRawApplicantPhone($data["home_phone"], '13', $n)) {
                $n++;
            }
        }
        if ($data["mobile"]) {
            if ($requestHelper->setRawApplicantPhone($data["mobile"], '15', $n)) {
                $n++;
            }
        }
        if ($data["alternative_phone"]) {
            if ($requestHelper->setRawApplicantPhone($data["alternative_phone"], '13', $n)) {
                $n++;
            }
        }

        $requestHelper->setApplicantName('forename', $data["first_name"]);
        $requestHelper->setApplicantName('surname', $data["last_name"]);
        $requestHelper->setApplicantParameter('dob', $data["dob"]);

        $this->setSoapClient();
        $searchParameters = $requestHelper->getSearchParameters();
        $searchParameters['SearchDefinition']['creditrequest']['schemaversion'] = '7.1';
        $this->BSBApi->setSearchParameters($searchParameters);
        if ($modifiedSearchParameters) {
            $this->BSBApi->resetSearchParameters($modifiedSearchParameters);
        }
        $this->BSBApi->setSearchPurpose('MS');

        $apiResult = null;
        $resultObject = null;

        try {
            $apiResult = $this->BSBApi->cr70Search();
            $this->responseXml = $this->BSBApi->getXMLResult();
            $this->requestXml = $this->BSBApi->getXMLRequest();
            $resultObject = $this->BSBApi->getSearchResult();
        }
        catch (\SoapFault $e) {
            $lastResponse = $this->BSBApi->getLastResponse();
            if (!$lastResponse) {
                $lastResponse = '<error>Can not connect to SOAP-server</error>';
            }
            $this->responseXml = $lastResponse;
            $this->requestXml = $this->BSBApi->getLastRequest();
            $apiResult = "error";
        }

        if(!empty($xmlId)){
            $creditReport = $this->em->getRepository('QuidCycleApiBundle:CreditReport')->find($xmlId);
        } else {
            $creditReport = new CreditReport();
        }
        $creditReport->setDataRequest((string)$this->requestXml);
        $creditReport->setDataResponse((string)$this->responseXml);
        $creditReport->setResultObject(serialize($resultObject));
        $purpose = $this->BSBApi->getSearchPurpose();
        $creditReport->setSearchType($purpose);
        $creditReport->setRequestId($data["ca_request_id"]);
        $this->em->persist($creditReport);
        $this->em->flush();

        $caRequest = null;
        if($data["ca_request"]){
            $caRequest = $data["ca_request"];
        }

        $repositoryStatus =  $this->em->getRepository('QuidCycleCommonBundle:CreditAlgo\ValidationStatus');
        $caStatus = $repositoryStatus->findOneBy(
            array('request' => $caRequest, 'type' => ValidationStatus::TYPE_CALL_REPORT, 'xmlRequestId' => $xmlId )
        );
        if(empty($caStatus)){
            $caStatus = new ValidationStatus();
            $caStatus->setType(ValidationStatus::TYPE_CALL_REPORT);
            $caStatus->setRequestType(ValidationStatus::REQUEST_TYPE_MS);
        }

        switch ($apiResult) {
            case 'error':
                $status = ValidationStatus::STATUS_ERROR;
                break;
            case 'picklist':
                $status = ValidationStatus::STATUS_PICKLISTS;
                break;
            default:
                $status = ValidationStatus::STATUS_READY;
                break;
        }
        $caStatus->setStatus($status);
        unset($data["address"]);
        unset($data["ca_request"]);
        $caStatus->setReportRequestData($data);
        $caStatus->setXmlRequestId($creditReport->getId());
        $caRequest->addValidationStatus($caStatus);
        $this->em->persist($caStatus);
        $this->em->flush();

        return true;
    }

    /**
     * @param LoanApplication $loanApplication
     * @param null $modifiedSearchParameters
     * @param bool $quotation
     * @param  integer|null  $xmlId
     * @param  boolean       $batchRequest
     * @return LoanApplication
     */
    public function processRequest(LoanApplication $loanApplication, $modifiedSearchParameters = null, $quotation = false, $xmlId = null, $batchRequest = false)
    {
        /** @var $requestHelper RequestHelper */
        $requestHelper = $this->loanApplicationToRequestHelper($loanApplication);
        $this->setSoapClient();
        $searchParameters = $requestHelper->getSearchParameters();
        $searchParameters['SearchDefinition']['creditrequest']['schemaversion'] = '7.1';

        $this->BSBApi->setSearchParameters($searchParameters);


        if ($quotation) {
            $this->BSBApi->setSearchPurpose('QS');
        }

        /*
        if ($jointCustomer = $loanApplication->getJointCustomer()) {
            $jointApplicant = $this->customerToApplicantData($jointCustomer);
            $this->creditAlgo->addJointApplicant($jointApplicant);
        }
        */
        if ($modifiedSearchParameters) {
            $this->BSBApi->resetSearchParameters($modifiedSearchParameters);
        }
        $apiResult = null;
        $resultObject = null;

        try {
            $apiResult = $this->BSBApi->cr70Search();
            $this->responseXml = $this->BSBApi->getXMLResult();
            $this->requestXml = $this->BSBApi->getXMLRequest();
            $resultObject = $this->BSBApi->getSearchResult();
        }
        catch (\SoapFault $e) {
            $lastResponse = $this->BSBApi->getLastResponse();
            if (!$lastResponse) {
                $lastResponse = '<error>Can not connect to SOAP-server</error>';
            }
            $this->responseXml = $lastResponse;
            $this->requestXml = $this->BSBApi->getLastRequest();
            $apiResult = 'error';
        }

        $qout = null;
        if ($quotation) {
            $qout = $loanApplication->getQuotationData();
            $qout->setRequestBsbXml((string)$this->requestXml);
            $qout->setResponseBsbXml((string)$this->responseXml);
            $qout->setResultBsbObject(serialize($resultObject));
            $this->em->persist($qout);

        }
        else {
            // send cr xml to reporting server
            if (($apiResult == 'match') && !$batchRequest) {
                /** @var QuotationServerProcessService $service */
                $service = $this->container->get('quidcycle.quotationServerProcess');
                $service->sendCrXmlToImport($loanApplication, $this->responseXml);
            }
        }

        if(!empty($xmlId)){
            $creditReport = $this->em->getRepository('QuidCycleApiBundle:CreditReport')->find($xmlId);
        } else {
            $creditReport = new CreditReport();
        }

        // todo
        $creditReport->setDataRequest((string)$this->requestXml);
        // todo
        $purpose = $this->BSBApi->getSearchPurpose();
        $creditReport->setSearchType($purpose);
        $creditReport->setDataResponse((string)$this->responseXml);
        $creditReport->setResultObject(serialize($resultObject));
        $this->em->persist($creditReport);

        $this->em->flush();

        if (!$quotation) {
            $result = $this->processResponse($loanApplication, $apiResult, $creditReport, $xmlId, $batchRequest);
        }
        else {
            $result = $this->processResponseWithQuotation($loanApplication, $apiResult, $qout);
        }

        return $result;
    }

    /**
     * Temporary function for getting data from Entity
     * @param LoanApplication $loanApplication
     * @return array
     */
    protected function loanApplicationToRequestHelper(LoanApplication $loanApplication)
    {
        $customer = $loanApplication->getCustomer();
        $requestHelper = $this->getCustomerData($customer);

        return $requestHelper;
    }

    /**
     * Temporary function for getting data from Entity
     * @param \QuidCycle\CommonBundle\Entity\Customer $customer
     * @return array
     */
    protected function customerToApplicantData(Customer $customer)
    {
        $requestHelper = $this->getCustomerData($customer);

        return $requestHelper->getApplicationData();
    }




    /**
     * Temporary function for process response
     * @param LoanApplication $loanApplication
     * @param string $apiResult
     * @param \QuidCycle\ApiBundle\Entity\CreditReport $creditReport
     * @param  integer|null  $xmlId
     * @param  boolean       $batchRequest
     * @throws \Exception
     * @return LoanApplication
     */
    protected function processResponse(LoanApplication $loanApplication, $apiResult = '', CreditReport $creditReport = null, $xmlId = null, $batchRequest = false)
    {
        $token = $this->container->get('security.context')->getToken();
        if ($token) {
            $user = $token->getUser();
        } else {
            $user = null;
        }

        if (!$user instanceof AdminUser) {
            $user = null;
        }

        // ToDo: error checking
        $caRequest = $loanApplication->getCaRequest();
        $caStatus = null;
        if (!$caRequest) {
            $caRequest = new RequestBorrower();
            $loanApplication->setCaRequest($caRequest);
            $caRequest->setLoanApplication($loanApplication);
        } else {
            // initial state of statuses
            //$caRequest->toInitialState();
            $repositoryStatus =  $this->em->getRepository('QuidCycleCommonBundle:CreditAlgo\ValidationStatus');

            $where = array('request' => $caRequest, 'type' => ValidationStatus::TYPE_CALL_REPORT);

            // fix for batching RUN
            if($batchRequest){
                $where['requestType'] = ValidationStatus::REQUEST_TYPE_MS;
                $where['status'] = ValidationStatus::STATUS_PENDING;
            }

            $caStatus = $repositoryStatus->findOneBy($where);
        }
        $customerName = $loanApplication->getCustomer()->getFirstName().' '.$loanApplication->getCustomer()->getLastName();
        $caRequest->setCustomerName($customerName);

        if (!$caStatus) {
            $caStatus = new ValidationStatus();
            $caStatus->setType(ValidationStatus::TYPE_CALL_REPORT);
            $caStatus->setRequestType(ValidationStatus::REQUEST_TYPE_CA);
        }
        // store only in CreditReport
        //$caStatus->setComment($xmlResponse);
        switch ($apiResult) {
            case 'error':
                $status = ValidationStatus::STATUS_ERROR;
                break;
            case 'picklist':
                $status = ValidationStatus::STATUS_PICKLISTS;
                break;
            default:
                $status = ValidationStatus::STATUS_READY;
        }
        $caStatus->setStatus($status);
        $caStatus->setRequest($caRequest);

        // there no admin user while customer submits the application
        //$caStatus->setUser(null);

        $this->em->persist($caStatus);

        if ($caRequest) {
            $creditReport->setRequestId($caRequest->getId());
        }

        if($creditReport){
            $caStatus->setXmlRequestId($creditReport->getId());
            $this->em->persist($caStatus);
        }

        $this->em->persist($creditReport);
        //$caRequest->setStatus(Request::STATUS_READY);

        //$this->em->persist($caRequest);
        //$this->em->flush();

        /*
        if(!$batchRequest){
            $this->addStatus($caRequest, ValidationStatus::TYPE_ID_CHECK, $user);
            $this->addStatus($caRequest, ValidationStatus::TYPE_AFFORDABILITY, $user);
        }
        */

        // save all
        $this->em->flush();

        return $loanApplication;
    }

    /**
     * Temporary function for process response
     * @param LoanApplication $loanApplication
     * @param string $apiResult
     * @param QuotationData $quotation
     * @throws \Exception
     * @return LoanApplication
     */
    protected function processResponseWithQuotation(LoanApplication $loanApplication, $apiResult = ''
            , QuotationData $quotation = null)
    {
        $token = $this->container->get('security.context')->getToken();
        if ($token) {
            $user = $token->getUser();
        } else {
            $user = null;
        }

        if (!$user instanceof AdminUser) {
            $user = null;
        }

        // store only in CreditReport
        //$caStatus->setComment($xmlResponse);
        switch ($apiResult) {
            case 'error':
                $status = ValidationStatus::STATUS_ERROR;
                break;
            case 'picklist':
                $status = ValidationStatus::STATUS_PICKLISTS;
                break;
            default:
                $status = ValidationStatus::STATUS_READY;
        }
        // there no admin user while customer submits the application
        //$caStatus->setUser(null);

        /*
         * Disable emails after quotation received
         */
        if (false) {
            /** @var MailManager $mm */
            $mm = $this->container->get('quidcycle.mail_manager');
            if ($status == ValidationStatus::STATUS_PICKLISTS) {
                $mm->sendQuotationResponseRequires($loanApplication->getId());

            } else {
                $mm->sendQuotationReceived($loanApplication->getId());
            }
        }

        // it is a ValidationStatus status, but it is consistent with QuotationData statuses
        $quotation->setStatusBsbRequest($status);
        $this->em->persist($quotation);

        // save all
        $this->em->flush();

        return $loanApplication;
    }

    /**
     * @param $xmlString
     * @return string
     */
    public function xmlRender($xmlString)
    {
        $html = '';

        $xmlString = str_replace(' xmlns="urn:callcredit.co.uk/soap:bsbandcreditreport7"', '', $xmlString);
        try {
            $html = $this->BSBApi->xmlRender($xmlString);
        } catch(\Exception $e) {

        }

        return $html;
    }

    /**
     * Convert api-address to string
     * @param $addressObject
     * @return string
     */
    protected function addressToString($addressObject)
   	{
   		$stringAddress = null;
   		foreach ($addressObject as $addressKey => $addressItem) {
   			if (!empty($addressItem)) $stringAddress .= $addressItem.', ';
   			if ($addressKey == 'postcode') return rtrim($stringAddress,', ');
   		}

   		return rtrim($stringAddress,', ');
   	}

    /**
     * @param $requestId
     * @param null $xmlId
     * @return mixed
     */
    protected function getCreditReportEntity($requestId, $xmlId = null)
    {
        $data = $this->em->getRepository('QuidCycleApiBundle:CreditReport')->getCreditReportEntity($requestId, $xmlId);

        return $data;
    }

    /**
     * @param $requestId
     * @param null $xmlId
     * @return mixed
     */
    public function getReportId($requestId, $xmlId = null)
    {
        $data = $this->getCreditReportEntity($requestId, $xmlId);
        if(empty($data)){
            return null;
        }

        return $data->getId();
    }

    /**
     * @param $requestId
     * @param null $xmlId
     * @return mixed
     */
    protected function getStdObject($requestId, $xmlId = null) {

        $data = $this->getCreditReportEntity($requestId, $xmlId);

        return unserialize($data->getResultObject());
    }

    /**
     * @param $requestId
     * @param bool $joint
     * @param null $xmlId
     * @return bool
     */
    public function getXml($requestId, $joint = false, $xmlId = null) {
        $data = $this->getCreditReportEntity($requestId, $xmlId);
        if (!$data) {
            return false;
        }

        return $data->getDataResponse();
    }

    /**
     * Fetch saved xml date
     * @param $requestId
     * @param $date
     * @return mixed
     */
    public function getXmlDate($requestId, $date = null) {
        $data = $this->getCreditReportEntity($requestId, $date);
        if (!$data) {
            return false;
        }

        return $data->getCreatedAt();
    }

    /**
     * @param $requestId
     * @param null $stdObject
     * @param integer $xmlId
     * @return array|bool
     */
    public function getPicklist($requestId, $stdObject = null, $xmlId = null)
    {
        if($requestId == null && !empty($stdObject)){
            $data = $stdObject;
        } else {
            $data = $this->getStdObject($requestId, $xmlId);
        }
        if (!$data) {
            return false;
        }
        $names = array();
        $addresses = array();
        //for each applicant (CallReport allows upto 2)
        $applicantId = 0;
        foreach ($data->picklist->applicant as $applicant) {
            //var_dump($applicant); exit;
            //for each address entered in the search (CallReport allows upto 3)
            //foreach ($applicant->address as $address) {
            foreach ($applicant->address as $address) {
                if (!isset($address->fullmatches->reporttype)) {
                    //then for this address we have a picklist
                    //for each of the matches found
                    foreach ($address->fullmatches->fullmatch as $fullmatch) {
                        //var_dump($fullmatch->{"@attributes"}); exit;
                        //check each one first to see if selected = 1, if it does then we actually have a name picklist
                        if ($fullmatch->selected == 1) {
                            $namePicklist = true;
                            //for each name submitted (incase an alias was also submit)
                            foreach ($fullmatch->name as $name) {
                                if (!isset($name->namematches->reporttype)) {
                                    //for each name that was match output a radio button
                                    foreach ($name->namematches->namematch as $namematch) {
                                        //note, we are passing through the applicantID, addressid and nameid as the name of the radio button group.  This will allow us to locate the correct branch later on when setting the 'selected' attribute.
                                        $nameString = $namematch->title.' '.$namematch->forename;
                                        if (isset($namematch->othernames)) {
                                            $nameString .= ' '.$namematch->othernames;
                                        }
                                        $nameString .= ' '.$namematch->surname;
                                        if ($namematch->dob != '') {
                                            $nameString .= ' (<b>DOB:</b>' . $namematch->dob . ')';
                                        }
                                        $names[$applicantId][] = array(
                                            'addressId' => $address->addressid,
                                            'nameId' => $name->nameid,
                                            'residenceId' => $namematch->residenceid,
                                            'nameTitle' => $nameString,
                                            'addressTitle' => $this->addressToString($fullmatch->addressmatch),
                                        );
                                    }
                                } else {
                                    switch ($name->namematches->reporttype) {
                                        case 0:
                                            echo '<b>No matches for this name</b><br /><br />';
                                            break;
                                        case 1:
                                        case 2:
                                        case 3:
                                        {
                                            //for each of the matches found, should only be one one but just incase
                                            foreach ($name->namematches->namematch as $namematch) {
                                                //check each one first to see if selected = 1, if it does then this is the name matched
                                                if (1 == $namematch->selected)                                                 {
                                                    //echo '<b>Matched Name:</b> ' . nameToString($namematch) . '<br /><br />';
                                                }
                                            }
                                            break;
                                        }
                                        case 4:
                                            // Too many name matches = error?
                                            //$tooMany = true;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    //if we've not already had a name picklist, then it must be an address picklist
                    if (!isset($namePicklist)) {
                        //for each address that was matched output a radio button
                        foreach ($address->fullmatches->fullmatch as $fullmatch) {

                            //note, we are passing through the applicantID and addressid as the name of the radio button group.  This will allow us to locate the correct branch later on when setting the 'selected' attribute.
                            $addressString = (string)$fullmatch->addressmatch->domicileid;
                            if (!empty($fullmatch->addressmatch->abodeno)) {
                                $addressString .= ', '.$fullmatch->addressmatch->abodeno;
                            }
                            if (!empty($fullmatch->addressmatch->buildingno)) {
                                $addressString .= ', '.$fullmatch->addressmatch->buildingno;
                            }
                            if (!empty($fullmatch->addressmatch->buildingname)) {
                                $addressString .= ', '.$fullmatch->addressmatch->buildingname;
                            }
                            if (!empty($fullmatch->addressmatch->street1)) {
                                $addressString .= ', '.$fullmatch->addressmatch->street1;
                            }
                            if (!empty($fullmatch->addressmatch->street2)) {
                                $addressString .= ', '.$fullmatch->addressmatch->street2;
                            }
                            if (!empty($fullmatch->addressmatch->sublocality)) {
                                $addressString .= ', '.$fullmatch->addressmatch->sublocality;
                            }
                            if (!empty($fullmatch->addressmatch->locality)) {
                                $addressString .= ', '.$fullmatch->addressmatch->locality;
                            }
                            if (!empty($fullmatch->addressmatch->posttown)) {
                                $addressString .= ', '.$fullmatch->addressmatch->posttown;
                            }
                            if (!empty($fullmatch->addressmatch->postcode)) {
                                $addressString .= ', '.$fullmatch->addressmatch->postcode;
                            }
                            $addresses[$applicantId][] = array(
                                'addressId' => (string) $address->addressid,
                                'domicileId' => (string)$fullmatch->addressmatch->domicileid,
                                'addressTitle' => $addressString,
                            );
                        }

                    }
                } else {
                    //this particular address actually matched, so lets just output it, this logic should only every be entered if more than 1 address was submit
                    switch ($address->fullmatches->reporttype) {
                        case 0: //reporttype of 0 means no match for this address
                            //echo '<b>No matches for this address</b><br /><br />';
                            break;
                        case 1:
                        case 2:
                        case 3: //report type 1-3 means the address was matched
                            //for each of the matches found, should only be one one but just incase
                            foreach ($address->fullmatches->fullmatch as $fullmatch) {
                                //check each one first to see if selected = 1, if it does then this is the address matched
                                if ($fullmatch->selected == 1) {
                                    //echo '<b>Matched Address:</b> ' . addressToString($fullmatch->addressmatch) . '<br /><br />';
                                }
                            }
                            break;
                        case 4: //too many matches
                            $tooMany = true;
                            break;
                    }
                }
            }
            $applicantId++;
        }

        $picklist = array(
            'addresses' => $addresses,
            'names' => $names,
        );
        $formItems = array(
            'type' => null,
            'applicantId' => null,
            'addressId' => null,
            'values' => array(),
        );
        //get items for choose
        foreach ($picklist as $_type => $_dataType) {
            // for the first applicant for now
            if (count($_dataType) == 0) {
                continue;
            }
            foreach ($_dataType as $_applicantId => $_dataTypeData) {
                foreach ($_dataTypeData as $_item) {
                    if (isset($_item['nameTitle'])) {
                        $value = $_item['nameTitle'];
                    } else {
                        $value = $_item['addressTitle'];
                    }

                    $formItems['type'] = $_type;
                    $formItems['applicantId'] = $_applicantId;
                    $formItems['addressId'] = $_item['addressId'];
                    if (isset($_item['nameId'])) {
                        $formItems['nameId'] = $_item['nameId'];
                    }
                    if (isset($_item['domicileId'])) {
                        $index = $_item['domicileId'];
                    } else {
                        $index = $_item['residenceId'];
                    }
                    $formItems['values'][$index] = $value;
                }
            }
        }
        $picklist['formItems'] = $formItems;

        return $picklist;
    }

    /**
     * Process selected item from picklist
     *
     * @param $data
     * @param $reviewId
     * @param $stdObject - in case we already have $stdObject
     * @param integer $xmlId
     *
     * @throws \Exception
     * @return mixed
     */
    public function processPicklistData($data, $reviewId = null, $stdObject = null, $xmlId = null)
    {
        if (!is_object($stdObject)) {
            if (empty($reviewId)) {
                throw new \Exception(__METHOD__ . ': Need either $reviewId or $stdObject params, bot both are empty');
            }

            $stdObject = $this->getStdObject($reviewId, $xmlId);
        }

        $addressId = $data['addressId'] - 1;
        $matches = $stdObject->picklist->applicant[$data['applicantId']]->address[$addressId]->fullmatches->fullmatch;

        if('addresses' == $data['type']) {
            //we have an addess pickist{
            $matchPos = 0;
            foreach ($matches as $_matchId => $_fullmatch) {
                if($_fullmatch->addressmatch->domicileid == $data['items']) {
                    //$fullmatchArray = array($addresses->)
                    $stdObject->picklist->applicant[$data['applicantId']]->address[$addressId]->fullmatches->fullmatch[$matchPos]->selected = 1;
                }
                $matchPos++;
            }
            //echo $dataXml[$data['applicantId']]->asXML();
        } else {
            //we have a name picklist
            $nameId = $data['nameId'] - 1;
            $matchPos = 0;
            foreach ($matches as $_matchId => $_fullmatch) {
                if (1 == $_fullmatch->selected) {
                    //then we are in the correct address branch
                    foreach ($_fullmatch->name[$nameId]->namematches->namematch as $_nameMatchId => $_namematch) {
                        //check each name match in the selected address branch
                        if ($_namematch->residenceid == $data['items']) {
                            //if the residenceid matches the selected residence id, set selected to 1
                            $stdObject->picklist->applicant[$data['applicantId']]->address[$addressId]->fullmatches->fullmatch[$matchPos]->name[$nameId]->namematches->namematch[$_nameMatchId]->selected = 1;
                        }
                    }
                } else {
                    /*
                    //they have selected none of the above or abort
                    $matchedAdd = 0; //initialise a pointer so that we know what matched address we are in
                    foreach ($resultObject->picklist->applicant[$applicantID]->address[$addressID]->fullmatches->fullmatch as $fullmatch)
                    {
                        if($fullmatch->selected == 1) //then we are in the correct address branch
                        {
                            if($residence == 'none') //set the matchstatus to 0 as per the API Reference Guide
                            {
                                $resultObject->picklist->applicant[$applicantID]->address[$addressID]->fullmatches->fullmatch[$matchedAdd]->name[$nameID]->namematches->matchstatus = 0;
                            }
                            else //set the matchstatus to 1 as per the API Reference Guide
                            {
                                $resultObject->picklist->applicant[$applicantID]->address[$addressID]->fullmatches->fullmatch[$matchedAdd]->name[$nameID]->namematches->matchstatus = 1;
                            }
                        }
                        $matchedAdd++;
                    }
                    */
                }
                $matchPos++;
            }
        }

        return $stdObject;
    }

    /**
     * @param $request
     * @param $type
     * @param int $status
     * @param int $requestType
     * @param CreditReport $creditReport
     */
    protected function addStatus($request, $type, $status = 0, $requestType = ValidationStatus::REQUEST_TYPE_CA, CreditReport $creditReport = null)
    {
        $repositoryStatus =  $this->em->getRepository('QuidCycleCommonBundle:CreditAlgo\ValidationStatus');
        $caStatus = $repositoryStatus->findOneBy(
            array('request' => $request, 'type' => $type), array('id' => 'desc')
        );
        if (!$caStatus) {
            $caStatus = new ValidationStatus();
            $caStatus->setType($type);
            $caStatus->setRequestType($requestType);
            if($creditReport){
                $caStatus->setXmlRequestId($creditReport->getId());
            }
            $request->addValidationStatus($caStatus);
            $this->em->persist($caStatus);
        }
    }

    //change a CallReport 7.0 password using the 'ChangePassword07a' method
    public function changePassword()
    {
        $this->setSoapClient();
        $tokenGenerator = $this->container->get('fos_user.util.token_generator');
        $password = substr($tokenGenerator->generateToken(), 0, 6);
        $password = preg_replace('#[\-\_]#', 'a', $password);
        $password .= chr(rand(65, 90)) . rand(0, 9);
        $apiPasswordRequest = new ApiPasswordRequest();
        $apiPasswordRequest->setPassword($password);
        $apiPasswordRequest->setType(ApiPasswordRequest::TYPE_CALL_REPORT);
        $needTryAgain = false;
        $username = $this->container->getParameter('api_cr_username');
        try {
            $this->BSBApi->changePassword($password, $password);
            $responseXml = $this->BSBApi->getXMLResult();
            $requestXml = $this->BSBApi->getXMLRequest();
            $apiPasswordRequest->setStatus(ApiPasswordRequest::STATUS_SUCCESS);
            $this->SendEmailChangePassword(
                "CallReport API Password Change for $username",
                "Password changed successfully for $username on " . date('jS F h:iA') . '.'
            );
        } catch (\SoapFault $e) {
            $responseXml = $this->BSBApi->getLastResponse();
            if (!$responseXml) {
                $responseXml = '<error>Can not connect to SOAP-server</error>';
            }
            $requestXml =  $this->BSBApi->getLastRequest();
            $apiPasswordRequest->setStatus(ApiPasswordRequest::STATUS_FAIL);
            if (!$this->changePwdAttemptsCnt) {
                $this->changePwdAttemptsCnt++;
                $needTryAgain = true;
            } else {
                $error = preg_replace('#^.*<faultstring>(.+)</faultstring>.*$#is', '\\1', $responseXml);
                $error = trim($error);
                // after the second attempt send email to admin
                $this->SendEmailChangePassword(
                    "CallReport API Password not Change for $username",
                    "<p>Password not changed for $username on " . date('jS F h:iA') . '.</p>' .
                    "<p>$error</p>"
                );
            }
        }
        $apiPasswordRequest->setDataRequest($requestXml);
        $apiPasswordRequest->setDataResponse($responseXml);
        $this->em->persist($apiPasswordRequest);
        $this->em->flush();
        if ($needTryAgain) {
            $this->changePassword();
        }
    }

    protected function SendEmailChangePassword($topic, $body) {
        /** @var \Swift_Mailer $mailer */
        $mailer = $this->container->get('mailer');
        if ($this->container->hasParameter('mail_from')) {
            $from = trim($this->container->getParameter('mail_from'));
        } else {
            $from = 'techops@quidcycle.com';
        }
        if ($this->container->hasParameter('mail_to')) {
            $to = trim($this->container->getParameter('mail_to'));
        } else {
            $to = 'support@quidcycle.com';
        }
        $message = \Swift_Message::newInstance()
            ->setSubject($topic)
            ->setFrom($from)
            ->setTo($to)
            ->setContentType('text/html');
        $message->setBody($body);

        return $mailer->send($message);
    }

    /**
     * Fetch saved password
     * @return mixed
     */
    protected function getPassword() {
        $data = $this->em->getRepository('QuidCycleApiBundle:ApiPasswordRequest')->findOneBy(
            array('type' => ApiPasswordRequest::TYPE_CALL_REPORT, 'status' => ApiPasswordRequest::STATUS_SUCCESS), array('id' => 'DESC')
        );
        if ($data) {
            $password = $data->getPassword();
        } else {
            $password = $this->container->getParameter('api_bsbacr_password');
        }

        return $password;
    }

}
