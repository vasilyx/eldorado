<?php

/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 23.12.2016
 * Time: 1:37
 */

namespace AppBundle\Service\callvalidate;

use AppBundle\Entity\CallCreditCredentials;
use AppBundle\Entity\User;
use AppBundle\Entity\UserAddress;
use AppBundle\Entity\UserDetails;
use AppBundle\Interfaces\DataProviderInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class CallValidateDataProvider implements DataProviderInterface
{
    /** @var  EntityManager */
    private $entityManger;
    private $container;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->container = $container;
        $this->entityManger = $entityManager;
    }

    /**
     * This method returns data as array for API
     *
     * @param User $user
     * @param string $purpose
     * @return mixed
     */
    public function getDataForXmlByUser(User $user, $purpose = '')
    {
        $credentialsManager = $this->container->get('whs.manager.call_credit_credentials_manager');
        $credentials = $credentialsManager->getCredentialsByType(CallCreditCredentials::CALLVALIDATE_TYPE);

        $data['authentication'] = $credentials['headers'];

        $address = $this->entityManger->getRepository(UserAddress::class)->getUserActiveAddresses($user);

        is_array($address) ? $data['address'] = $address[0] : $data['address'] = $address;

        $data['userDetails'] = $this->entityManger->getRepository(UserDetails::class)->findOneByUser($user);
        $data['application'] = $credentials['application'];
        $data['requestID'] = md5(time() . random_int(1, 1000));

        return $data;
    }
}