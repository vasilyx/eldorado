<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 23.12.2016
 * Time: 1:43
 */

namespace AppBundle\Service\callvalidate;


use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\CallCreditCredentials;
use AppBundle\Entity\User;
use AppBundle\Event\CallCreditEvents;
use AppBundle\Interfaces\DataProviderInterface;
use AppBundle\Interfaces\ProcessApiRequestInterface;
use CG\Tests\Generator\Fixture\Entity;
use Doctrine\ORM\EntityManager;
use LogBundle\Entity\UserActivityLog;
use LogBundle\Handler\UserActivityHandler;
use Symfony\Bundle\TwigBundle\DependencyInjection\Configurator\EnvironmentConfigurator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Security\Core\User\UserInterface;

class CallValidateProcessRequest implements ProcessApiRequestInterface
{
    private $container;
    /** @var CallValidateDataProvider */
    private $dataProvider;

    public function __construct(ContainerInterface $container, CallValidateDataProvider $dataProvider)
    {
        $this->dataProvider = $dataProvider;
        $this->container = $container;
    }

    /**
     * This method init CallCreditAPICalls object for CallValidate request;
     * Example of xml for request is in AppBundle:XmlRequest:callvalidate_xml.html.twig
     *
     * @param CallCreditAPICalls $callCreditApiCalls
     * @return CallCreditAPICalls
     */
    public function processApiRequest(CallCreditAPICalls $callCreditApiCalls)
    {
        $user = $callCreditApiCalls->getUser();
        //Get data for XML
        $data = $this->dataProvider->getDataForXmlByUser($user);

        /** @var \Twig_Environment $twig */
        $twig = $this->container->get("templating");
        //return XML with correct data
        $requestXML = $twig->render('AppBundle:XmlRequest:callvalidate_xml.html.twig', $data);

        $callCreditApiCalls
            ->setRequestID($data['requestID'])
            ->setRequestXML($requestXML)
            ->setType(CallCreditAPICalls::TYPE_CALLVALIDATE)
            ->setStatus(CallCreditAPICalls::STATUS_NEW);

        return $callCreditApiCalls;
    }

    /**
     * This method send CallValidate request
     * if response status in SUCCESS will be run UnderwritingResult algorithm for LENDER
     * (method: lenderUnderwritingAlgorithm in UnderwritingResultManager)
     *
     * @param CallCreditAPICalls $callCredit
     * @param int $testMode
     */
    public function sendApiRequest(CallCreditAPICalls $callCredit, $testMode = CallCreditAPICalls::STATUS_SUCCESS)
    {
        $this->logSendRequest($callCredit->getUser(), $callCredit->getPurpose());

        //TODO: hotfix for expired CallCredit account change to enviroment checkong
        if ($this->container->get('kernel')->getEnvironment() === 'test') {
        //if ($testMode) {
            $callCredit->setStatus($testMode);
            switch ($testMode) {
                case CallCreditAPICalls::STATUS_SUCCESS:
                    $callCredit->setResponseXML($this->getTestResponse());
                    break;
                case CallCreditAPICalls::STATUS_ERROR:
                    $callCredit->setResponseXML($this->getTestResponse());
                    break;
            }
            $callCredit->setRequestDate(new \DateTime());
            $callCredit->setResponseDate(new \DateTime());
        } else {
            $requestXML = $callCredit->getRequestXML();
            $apiUrl = $this->container->get('whs.manager.call_credit_credentials_manager')
                                                ->getCredentialsByType(CallCreditCredentials::CALLVALIDATE_TYPE)['url'];

            $apiUrl = $apiUrl ? $apiUrl : 'https://ct.callcreditsecure.co.uk/callvalidateapi/incomingserver.php';

            //Send CallValidate request
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);

            $callCredit->setRequestDate(new \DateTime());
            $responseXML = curl_exec($ch);
            $callCredit->setResponseDate(new \DateTime());

            if ($responseXML === false) {
                $callCredit->setStatus(CallCreditAPICalls::STATUS_ERROR);
                $responseXML = curl_error($ch);
            } else {
                $callCredit->setStatus(CallCreditAPICalls::STATUS_SUCCESS);
            }

            $callCredit->setResponseXML($responseXML);

            curl_close($ch);
        }
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();
        $em->persist($callCredit);
        $em->flush();

        if ($callCredit->getStatus() == CallCreditAPICalls::STATUS_SUCCESS) {
            $this->container->get('event_dispatcher')->dispatch(CallCreditEvents::CALL_CREDIT_SUCCESS,
                new GenericEvent(null, ['callcredit_id' => $callCredit->getId()])
            );
        }
    }

    public function changePassword()
    {
    }

    public function logSendRequest(User $user, $purpose = CallCreditAPICalls::PURPOSE_QS)
    {
        switch ($purpose) {
            case (CallCreditAPICalls::PURPOSE_QS):
                $this->container->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $user, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::SYSTEM_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::QUOTATION_ACTION]);
                break;
            case (CallCreditAPICalls::PURPOSE_CA):
                $this->container->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $user, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::SYSTEM_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::CALLREPORT_ACTION]);
                break;
            case (CallCreditAPICalls::PURPOSE_MN):
                $this->container->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $user, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::SYSTEM_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::MANAGEMENT_ACTION]);
                break;
        }
    }

    protected function getTestResponse()
    {
        return '<?xml version="1.0" encoding="UTF-8"?>
<Results xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:/Users/hughc/PhpstormProjects/callvalidate/mantis-setup/Schema/5.7.0/Customer-Output-V5-7-0.xsd" APIVERSION="5.7.0 - 20150811">
  <Result RID="54cf5d1fe37ecec9cd38bc5cb0efe5e7" PID="LTJ-CT5-1907-31484-83718-7" DateTime="19-01-2017 14:46">
    <Displays>
      <ChecksCompleted>
        <BankStandard>no</BankStandard>
        <BankEnhanced>no</BankEnhanced>
        <CardLive>no</CardLive>
        <CardEnhanced>no</CardEnhanced>
        <IDEnhanced>no</IDEnhanced>
        <NCOAAlert>no</NCOAAlert>
        <CallValidate3D>no</CallValidate3D>
        <TheAffordabilityReport>no</TheAffordabilityReport>
        <DeliveryFraud>no</DeliveryFraud>
        <CreditScore>no</CreditScore>
        <Zodiac>no</Zodiac>
        <BankAccountPlus>no</BankAccountPlus>
        <BankOFA>no</BankOFA>
        <CardOFA>no</CardOFA>
        <RealTimeFraudAlerts>yes</RealTimeFraudAlerts>
        <DeviceRisk>no</DeviceRisk>
        <MobileRisk>no</MobileRisk>
      </ChecksCompleted>
      <InputData>
        <Individual>
          <Dateofbirth>01-01-1990</Dateofbirth>
          <Firstname>firstname</Firstname>
          <Surname>lastname</Surname>
        </Individual>
        <Address>
          <Address1>Berkeley Square House</Address1>
          <Town>London</Town>
          <Postcode>W1J 6BQ</Postcode>
        </Address>
      </InputData>
      <AgeVerify>
        <DateOfBirth>unknown</DateOfBirth>
        <Over18>unknown</Over18>
      </AgeVerify>
      <OtherChecks>
        <IdentityResult>Pass</IdentityResult>
        <IdentityScore>0</IdentityScore>
      </OtherChecks>
      <RealTimeFraudAlerts>
        <Summary>
          <VelocityThreshold>2</VelocityThreshold>
          <VelocityPeriod>24 hours</VelocityPeriod>
          <InconsistencyThreshold>2</InconsistencyThreshold>
          <InconsistencyPeriod>3 months</InconsistencyPeriod>
          <TotalVelocityWarnings>0</TotalVelocityWarnings>
          <TotalInconsistencyWarnings>0</TotalInconsistencyWarnings>
        </Summary>
        <VelocityData/>
        <InconsistencyData/>
      </RealTimeFraudAlerts>
      <DeviceRisk/>
      <Phone>
        <MobileRisk>
          <Standard/>
          <Live/>
          <Score/>
        </MobileRisk>
      </Phone>
      <Warnings>
        <NonGBRCardWarning>false</NonGBRCardWarning>
        <PAFNonValidWarning>false</PAFNonValidWarning>
        <CardAccountClosedWarning>false</CardAccountClosedWarning>
        <BankAccountClosedWarning>false</BankAccountClosedWarning>
      </Warnings>
    </Displays>
  </Result>
  <Errors/>
</Results>';
    }
}