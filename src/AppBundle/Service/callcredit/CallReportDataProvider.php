<?php

namespace AppBundle\Service\callcredit;

use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\CallCreditCredentials;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\User;
use AppBundle\Entity\UserAddress;
use AppBundle\Entity\UserDetails;
use AppBundle\Exception\BadApiCredentialsException;
use AppBundle\Interfaces\DataProviderInterface;
use AppBundle\Manager\QuoteTermManager;
use Doctrine\ORM\EntityManager;
use SoapFault;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\DataCollector\DataCollectorInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class CallReportDataProvider implements DataProviderInterface
{
    //CallReport 7.0 SOAP class, contains working examples of most of the functions available from the CallReport 7.0 web service

    //set the location of the CallReport 7.0 WSDL file
    //Callcredit do not make any Service Descriptors available over the web for security reasons, please use the schemas provided on your toolkit locally, this path may differ on a Linux platform

    /** @var  EntityManager */
    private $entityManager;
    private $container;

    private $searchParameters = array(
        'SearchDefinition' => array(
            'yourreference' => 'TP148488396',
            'creditrequest' => array(
                'purpose' => 'CA', //Search purpose, YOU WILL NEED TO CHANGE THIS TO YOUR SEARCH PURPOSE e.g. TV for Tenant Vet
                'autosearch' => 1, //Auto Search Address Links, default YES
                'autosearchmaximum' => 3, //Maximum addresses to autosearch, maximum 10
                'schemaversion' => '7.1', //mandatory attribute
                'datasets' => 255, //mandatory attribute, default 255
                'score' => 1 //default "YES"
                //addional parameters can be added here or using the set function below.
            )
        )
    );

    /**
     * CallCreditDataProvider constructor.
     * @param EntityManager $entityManager
     * @param ContainerInterface $container
     */
    public function __construct(EntityManager $entityManager,  ContainerInterface $container)
    {
        $this->container = $container;
        $this->entityManager = $entityManager;
    }

    //set variables in the applicant block
    //usage: $object->setApplicatant('dob',$dob);
    private function setApplicant($tag, $value)
    {
        $this->searchParameters['SearchDefinition']['creditrequest']['applicant'][$tag] = $value;
    }

    private function setSearchPurpose($purpose)
    {
        $this->searchParameters['SearchDefinition']['creditrequest']['purpose'] = $purpose;
    }

    //set variables in the applicant name block
    //usage: $object->setApplicantName('forename',$forename, 'P); for a previous name (alias)
    private function setApplicantName($tag, $value, $type = 'C')
    {
        if ($type == 'P') {
            $this->searchParameters['SearchDefinition']['creditrequest']['applicant']['name'][1][$tag] = $value;
        } else {
            $this->searchParameters['SearchDefinition']['creditrequest']['applicant']['name'][0][$tag] = $value;
        }
    }

    //set variables in the applicant address block, the default is current but the function provides the ability to define a previous address also
    //usage (current address): $object->setApplicantAddress('abodeno', $abodeno);
    //usage (previous address): $object->setApplicantAddress('buildingname', $buildingname,'P');
    private function setApplicantAddress($tag, $value, $type = 'C')
    {
        if ($type == 'P') {
            $this->searchParameters['SearchDefinition']['creditrequest']['applicant']['address'][1][$tag] = $value;
        } else {
            $this->searchParameters['SearchDefinition']['creditrequest']['applicant']['address'][0][$tag] = $value;
        }
    }

    //set variables in the applicantdemographics phone block
    //usage: $object->setApplicatantPhone('dob',$dob);
    private function setApplicantPhone($std, $number, $type, $n)
    {
        $this->searchParameters['SearchDefinition']['creditrequest']['applicant']['applicantdemographics']['contact']['telephone'][$n]['type'] = $type;
        $this->searchParameters['SearchDefinition']['creditrequest']['applicant']['applicantdemographics']['contact']['telephone'][$n]['std'] = $std;
        $this->searchParameters['SearchDefinition']['creditrequest']['applicant']['applicantdemographics']['contact']['telephone'][$n]['number'] = $number;
    }


    //set variables in the applicantdemographics email block
    //usage: $object->setApplicatantEmail('dob',$dob);
    private function setApplicantEmail($address, $type, $n)
    {
        $this->searchParameters['SearchDefinition']['creditrequest']['applicant']['applicantdemographics']['contact']['email'][$n]['type'] = $type;
        $this->searchParameters['SearchDefinition']['creditrequest']['applicant']['applicantdemographics']['contact']['email'][$n]['address'] = $address;
    }

    //set parameters in the creditrequest block
    //usage: $object->setParameter('autosearch', 1);
    private function setParameter($tag, $value)
    {
        $this->searchParameters['SearchDefinition']['creditrequest'][$tag] = $value;
    }

    /**
     * @param User $user
     * @param $purpose
     * @return array
     */
    private function getSearchParameters(User $user, $purpose)
    {
        $this->setSearchPurpose($purpose);

        /** @var UserAddress $address */
        $address = $this->entityManager->getRepository(UserAddress::class)->getUserActiveAddresses($user);

        $address = is_array($address) ? $address[0] : $address;

        $this->setApplicantAddress('buildingno', $address->getBuildingNumber());
        $this->setApplicantAddress('street1', $address->getLine1());
        $this->setApplicantAddress('posttown', $address->getCity());
        $this->setApplicantAddress('postcode', $address->getPostalCode());

        /** @var UserDetails $userDetails */
        $userDetails = $this->entityManager->getRepository(UserDetails::class)->findOneByUser($user);
        $this->setApplicantName('title', $userDetails->getTitle());
        $this->setApplicantName('forename', $userDetails->getFirstName());
        $this->setApplicantName('surname', $userDetails->getLastName());

        if ($purpose == CallCreditAPICalls::getPurposes()[CallCreditAPICalls::PURPOSE_CA]) {
            /** @var QuoteTermManager $quoteTermManager */
            $quoteTermManager = $this->container->get('whs.manager.quote_term_manager');
            $quoteTerm = $quoteTermManager->getQuoteTermForUser($user);
            //$this->searchParameters['SearchDefinition']['creditrequest']['amount'] = $quoteTerm->getAmount();
            //$this->searchParameters['SearchDefinition']['creditrequest']['term'] = $quoteTerm->getTerm();
        } else {
            $quoteApplies = $this->container->get('whs.manager.quote_apply_manager')->getQuoteApplies($user);
            if (!empty($quoteApplies)) {
                $quote = $quoteApplies[0];
                /** @var LoanApply $loanApply */
                $loanApply = $this->container->get('whs.manager.loan_apply_manager')->getLoanApplyByQuote($quote);
                if (!empty($loanApply)) {
                    $this->setApplicant('amount', $loanApply->getAmount());
                    $this->setApplicant('term', $loanApply->getTerm());
                }
            }
        }

        $this->setApplicant('dob', date_format($userDetails->getBirthdate(), 'Y-m-d'));

        return $this->searchParameters;
    }

    /**
     * Get Data for CallReport request as array
     *
     * @param User $user
     * @param int|string $purpose
     * @return mixed
     * @throws BadApiCredentialsException
     */
    public function getDataForXmlByUser(User $user, $purpose = CallCreditAPICalls::PURPOSE_QS)
    {
        //TODO: Change to CA/QS purpose
        $purpose = CallCreditAPICalls::getPurposes()[CallCreditAPICalls::PURPOSE_CA];
        $credentialsManager = $this->container->get('whs.manager.call_credit_credentials_manager');
        $credentials = $credentialsManager->getCredentialsByType(CallCreditCredentials::CALLREPORT_TYPE);

        if (!isset($credentials['headers']) || !isset($credentials['url'])) {
            throw new BadApiCredentialsException();
        }
        $result['headers'] = $credentials['headers'];
        $result['apiurl'] = $credentials['url'];
        $result['searchParameters'] = $this->getSearchParameters($user, $purpose);

        return $result;
    }
}
