<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 25.12.2016
 * Time: 2:00
 */

namespace AppBundle\Service\callcredit;


use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\CallCreditCredentials;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\UnderwritingResult;
use AppBundle\Entity\User;
use AppBundle\Event\CallCreditEvents;
use AppBundle\Exception\BadApiCredentialsException;
use AppBundle\Interfaces\DataProviderInterface;
use AppBundle\Interfaces\ProcessApiRequestInterface;
use Doctrine\ORM\EntityManager;
use LogBundle\Entity\UserActivityLog;
use LogBundle\Handler\UserActivityHandler;
use SoapFault;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class CallReportProcessRequest implements ProcessApiRequestInterface
{
    use ContainerAwareTrait;
    /** @var CallReportDataProvider */
    private $dataProvider;

    public function __construct(CallReportDataProvider $dataProvider)
    {
        $this->dataProvider = $dataProvider;
    }

    /**
     * This method set request data in JSON format to CallCreditApiCalls object.
     *
     * @param CallCreditAPICalls $callCreditAPICalls
     * @return CallCreditAPICalls
     * @throws BadApiCredentialsException
     */
    public function processApiRequest(CallCreditAPICalls $callCreditAPICalls)
    {
        $callCreditAPICalls
            ->setType(CallCreditAPICalls::TYPE_CALLREPORT)
            ->setStatus(CallCreditAPICalls::STATUS_NEW);
        try {
            $user = $callCreditAPICalls->getUser();
            $data = $this->dataProvider->getDataForXmlByUser($user, $callCreditAPICalls->getPurpose());
            $apiUrl = $data['apiurl'] ? $data['apiurl'] : 'https://ct.callcreditsecure.co.uk/Services/BSB/CRBSB7.asmx';
            $data['apiurl'] = $apiUrl;
            $callCreditAPICalls->setRequestID(md5(time() . random_int(1, 1000)));
            $callCreditAPICalls->setRequestXML(json_encode($data));
        } catch (BadApiCredentialsException $exception) {
            throw new BadApiCredentialsException();
        }


        return $callCreditAPICalls;
    }

    /**
     * This method got data for request from CallCreditAPICalls in JSON format; parse it and send.
     * If CallCreditApiCallsStatus changed on SUCCESS, will be run UnderwritingResult algorithm
     * (method: borrowerUnderwritingAlgorithm in UnderwritingResultManager)
     *
     * @param CallCreditAPICalls $callCredit
     * @param int $testMode
     * @param int $score
     */
    public function sendApiRequest(CallCreditAPICalls $callCredit, $testMode = CallCreditAPICalls::STATUS_SUCCESS, $score = QuoteApply::GUAGE_SCORE_APPROVE)
    {
        $this->logSendRequest($callCredit->getUser(), $callCredit->getPurpose());

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();
        //TODO: hotfix for expired CallCredit account change to enviroment checkong
        if ($this->container->get('kernel')->getEnvironment() === 'test') {
        //if ($testMode) {
            $callCredit->setStatus($testMode);
            switch ($testMode) {
                case CallCreditAPICalls::STATUS_SUCCESS:
                    $callCredit->setResponseXML($this->getTestResponseWithJFScore($score));
                    break;
                case CallCreditAPICalls::STATUS_ERROR:
                    $callCredit->setResponseXML($this->getErrorTestResponse());
                    break;
            }

            $callCredit->setRequestDate(new \DateTime());
            $callCredit->setResponseDate(new \DateTime());
            $em->persist($callCredit);
            $em->flush();
        } else {
            $data = json_decode($callCredit->getRequestXML(), true);

            if (!isset($data['headers'])) {
                return;
            }

            $serviceDescriptor = __DIR__ . "/../BsbWsdl/BsbAndCreditReport7.wsdl";

            $ns = "urn:callcredit.co.uk/soap:bsbandcreditreport7";

            //set the SOAP headers using the PHP5 SOAP Extension
            $headers = new \SOAPHeader($ns, 'callcreditheaders', $data['headers']);
            /** @var \SoapClient $soapClient */
            $soapClient = new \SoapClient($serviceDescriptor, array('trace' => 1, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS));
            //set the url to the client test site, NOTE. comment this out to target the LIVE site
            $soapClient->__setLocation($data['apiurl']);

            try {
                //perform the CallReport 7.0 search passing in the SOAP Headers and the Search Parameters, __soapCall stores the response from the server as an stdClass Object
                $callCredit->setRequestDate(new \DateTime());
                $soapClient->__soapCall('Search07a', array($data['searchParameters']), null, $headers);
                $callCredit->setResponseDate(new \DateTime());

                $callCredit->setStatus(CallCreditAPICalls::STATUS_SUCCESS);

                $callCredit->setRequestXML($soapClient->__getLastRequest());
                $callCredit->setResponseXML($soapClient->__getLastResponse());

            } catch (SoapFault $fault) {
                /** If request to api have been failed */
                $callCredit->setResponseDate(new \DateTime());
                $callCredit->setStatus(CallCreditAPICalls::STATUS_ERROR);

                $callCredit->setRequestXML($soapClient->__getLastRequest());
                $callCredit->setResponseXML($soapClient->__getLastResponse());

            } finally {
                /** Commit all changes in db regardless of the response */
                $em->persist($callCredit);
                $em->flush();
            }
        }
        /**
         * See CallCreditResponseListener class for more info.
         */
        if ($callCredit->getStatus() == CallCreditAPICalls::STATUS_SUCCESS) {
                $this->container->get('event_dispatcher')->dispatch(CallCreditEvents::CALL_CREDIT_SUCCESS,
                new GenericEvent(null, ['callcredit_id' => $callCredit->getId()])
            );
        }
    }

    /**
     * This method changes a password for the CallReport API
     */
    public function changePassword()
    {
        $ns = "urn:callcredit.co.uk/soap:bsbandcreditreport7";
        //set the SOAP headers using the PHP5 SOAP Extension
        $credentialsRepository = $this->container->get('doctrine')->getRepository(CallCreditCredentials::class);

        $headers = new \SOAPHeader($ns, 'callcreditheaders', $credentialsRepository->getCredentialsForApiByType(CallCreditCredentials::CALLREPORT_TYPE));

        $serviceDescriptor = __DIR__ . "/../BsbWsdl/BsbAndCreditReport7.wsdl";
        /** @var \SoapClient $soapClient */
        $soapClient = new \SoapClient($serviceDescriptor, array('trace' => 1, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS));
        $url = ($apiUrl = $credentialsRepository->findOneByType(CallCreditCredentials::CALLREPORT_TYPE)->getUrl())
            ? $apiUrl
            : 'https://ct.callcreditsecure.co.uk/Services/BSB/CRBSB7.asmx';
        $soapClient->__setLocation($url);

        /** Generate password for CallReport API */
        $tokenGenerator = $this->container->get('fos_user.util.token_generator');
        $password = substr($tokenGenerator->generateToken(), 0, 6);
        $password = preg_replace('#[\-\_]#', 'a', $password);
        $password .= chr(rand(65, 90)) . rand(0, 9);

        /** Data for changePassword request */
        $changePasswordParameters = array('newpwd' => $password, 'confirmpwd' => $password);

        try {
            //Call change password request by passing new password in parameters
            $soapClient->__soapCall('ChangePassword07a', array($changePasswordParameters), null, array($headers));

            /** If password have been changed, find row in db for CallReport API type and change password*/
            /** @var CallCreditCredentials $credentials */
            $credentials = $credentialsRepository->findOneByType(CallCreditCredentials::CALLREPORT_TYPE);
            $credentials
                ->setUpdatedAt(new \DateTime())
                ->setPassword($password);

            $manager = $this->container->get('doctrine')->getManager();
            $manager->persist($credentials);
            $manager->flush();

        } catch (SoapFault $soapFault) {

            $this->container->get('whs_mailer')->sendCallReportChangePasswordFailed();

        }
    }

    public function logSendRequest(User $user, $purpose = CallCreditAPICalls::PURPOSE_QS)
    {
        switch ($purpose) {
            case (CallCreditAPICalls::PURPOSE_QS):
                $this->container->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $user, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::SYSTEM_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::QUOTATION_ACTION]);
                break;
            case (CallCreditAPICalls::PURPOSE_CA):
                $this->container->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $user, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::SYSTEM_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::CALLREPORT_ACTION]);
                break;
        }
    }

    //Test response if request have been failed.
    protected function getErrorTestResponse()
    {
        return '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><soap:Fault><faultcode>soap:Client</faultcode><faultstring>Error(s)</faultstring><detail><ErrorBlock><Error> You are not authorised to use the requested search purpose</Error></ErrorBlock></detail></soap:Fault></soap:Body></soap:Envelope>';
    }

    /**
     * @param int $score (See score constants in QuoteApply entity class)
     *
     * Gauge score (JF score) >= 501,            quote will be approved
     *                        < 501 and >= 490,  quote will be referred
     *                        < 490,             quote will be rejected
     *
     * @return string
     */
    protected function getTestResponseWithJFScore($score = QuoteApply::GUAGE_SCORE_APPROVE)
    {
        return '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><Search07aResponse xmlns="urn:callcredit.co.uk/soap:bsbandcreditreport7"><SearchResult><jobdetails><searchid>{6DFF2445-E45B-4867-98C0-E148D83AB108}</searchid><cast>006.004.7.2.1.1.0.31673210.0</cast><pstv>31673210</pstv><ls>0</ls><search' .
                'date>2017-01-21T09:36:04</searchdate></jobdetails><yourreference>TP148488396</yourreference><creditrequest schemaversion="7.1" datasets="255"><applicant><address><street1>test</street1><posttown>London</posttown><postcode>SW1A 0PW</postcode></address><name><forename>firstname</forename><surname>lastname</surname></name><dob>1990-01-01</dob></applicant><score>1</score><purpose>CA</purpose><autosearch>1</autosearch><autosearchmaximum>3</autosearchmaximum></creditrequest>' .
                '<picklist maxaddressitems="20" maxnameitems="9" picklist="1" regenerated="0"><applicant id="1" reporttype="0"><address addressid="1"><addressinput><abodeno /><buildingno /><buildingname /><street1>test</street1><street2 /><sublocality /><locality /><posttown>London</posttown><postcode>SW1A 0PW</postcode></addressinput><fullmatches reporttype="0" /></address></applicant></picklist><creditreport searchid="{6DFF2445-E45B-4867-98C0-E148D83AB108}" linktype="0"><applicant ' .
                'reporttype="0" tpoptout="0" reporttitle="Address not found"><creditscore><score class="2">9999</score><reasons><code>0</code><code>0</code><code>0</code><code>0</code></reasons></creditscore></applicant></creditreport><BSB searchid="{6DFF2445-E45B-4867-98C0-E148D83AB108}" linktype="0"><OVERALL><MAIN B="0" /><CUGMEMB K="P" L="0" /></OVERALL><APPLICANT app_no="1"><ID><APPL DC="{ND}" EC="{ND}" FC="{ND}" GC="NM" HC="NM" IC="{ND}" /></ID><SCORE><MAIN JF="' . $score .
                '" KF="{ND}" MF="0" NF="0" OF="0" PF="0" QF="{ND}" SF="{ND}" TF="{ND}" UF="{ND}" VF="{ND}" WF="{ND}" YF="{ND}" ZF="{ND}" AG="{ND}" CG="{ND}" /></SCORE></APPLICANT></BSB></SearchResult></Search07aResponse></soap:Body></soap:Envelope>';
    }
}