<?php

namespace AppBundle\Service;

use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;


class Mailer
{
    private $mailer;
    /**
     * @var TwigEngine
     */
    private $twig;

    private $container;
    private $defaultParams = [];

    public function __construct(\Swift_Mailer $mailer, $templating, Container $container)
    {
        $this->mailer      = $mailer;
        $this->twig        = $templating;
        $this->container    = $container;

        $this->setDefaultParams();
    }

    protected function setDefaultParams()
    {
        $this->defaultParams = [
            'baseUrl'       => $this->container->get('router')->getContext()->getScheme() . "://" .
                $this->container->get('router')->getContext()->getHost(),
            'frontEndUrl'   => $this->container->get('router')->getContext()->getScheme() . "://" .
                $this->container->get('router')->getContext()->getHost() . "/frontend/index.html#/"
        ];
    }

    //Send mail to admin if CallReport API password hasn't been changed
    public function sendCallReportChangePasswordFailed()
    {
        $message = $this->mailer->createMessage()
            ->setSubject("CallReport API password hasn't been changed")
            ->setFrom($this->container->getParameter('mailer_no_reply'))
            ->setTo($this->container->getParameter('mailer_admin'))
            ->setBody(
                $this->twig->render('AppBundle:emails:changing_callreport_password.html.twig'),
                'text/html'
            );

        return $this->send($message);
    }

    public function sendForgottenPassword($to, $renderParams = array())
    {
        $message = $this->mailer->createMessage()
            ->setSubject('Forgotten password')
            ->setFrom($this->container->getParameter('mailer_no_reply'))
            ->setTo($to)
            ->setBody(
                $this->twig->render(
                    'AppBundle:emails:forgotten_password.html.twig',
                    $renderParams
                ),
                'text/html'
            );

        return $this->send($message);
    }

    public function sendLoanDecline($to, $renderParams = array())
    {
        $message = $this->mailer->createMessage()
            ->setSubject('Loan has been declined')
            ->setFrom($this->container->getParameter('mailer_no_reply'))
            ->setTo($to)
            ->setBody(
                $this->twig->render(
                    'AppBundle:emails:loan_declined.html.twig',
                    array_merge($renderParams, $this->defaultParams)
                ),
                'text/html'
            );

        return $this->send($message);
    }

    public function sendResetPassword($to, $renderParams = array())
    {
        $message = $this->mailer->createMessage()
            ->setSubject('Reset password')
            ->setFrom($this->container->getParameter('mailer_no_reply'))
            ->setTo($to)
            ->setBody(
                $this->twig->render(
                    'AppBundle:emails:resseting_password.html.twig',
                    array_merge($renderParams, $this->defaultParams)
                ),
                'text/html'
            );

        return $this->send($message);
    }

    public function sendRegistrationConfirm($to, $renderParams = array())
    {
        $message = $this->mailer->createMessage()
            ->setSubject('Registration confirmation')
            ->setFrom($this->container->getParameter('mailer_no_reply'))
            ->setTo($to)
            ->setBody(
                $this->twig->render(
                    'AppBundle:emails:confirm_reg.html.twig',
                    array_merge($renderParams, $this->defaultParams)
                ),
                'text/html'
            );

        return $this->send($message);
    }

    public function sendAcceptanceNotification($to, $renderParams = array())
    {
        $message = $this->mailer->createMessage()
            ->setSubject('Loan Agreement needs your review and acceptance')
            ->setFrom($this->container->getParameter('mailer_no_reply'))
            ->setTo($to)
            ->setBody(
                $this->twig->render(
                    'AppBundle:emails:agreement_acceptance.html.twig',
                    array_merge($renderParams, $this->defaultParams)
                ),
                'text/html'
            );

        return $this->send($message);
    }

    private function send($message)
    {
        $sent = $this->mailer->send($message);

//        $this->mailer->getTransport()->stop(); // should fix: Expected response code 250 but got code "451", with message "451 4.4.2 Timeout - closing connection. l6sm253049wjz.4 - gsmtp

        return $sent;
    }
}
