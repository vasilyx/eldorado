<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 12.01.2017
 * Time: 14:29
 */

namespace AppBundle\Service\affordability;

use AppBundle\Entity\CallCreditCredentials;
use AppBundle\Entity\User;
use AppBundle\Entity\UserAddress;
use AppBundle\Entity\UserDetails;
use AppBundle\Entity\UserIncome;
use AppBundle\Interfaces\DataProviderInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class AffordabilityDataProvider implements DataProviderInterface
{
    //CallReport 7.0 SOAP class, contains working examples of most of the functions available from the CallReport 7.0 web service

    //set the location of the CallReport 7.0 WSDL file
    //Callcredit do not make any Service Descriptors available over the web for security reasons, please use the schemas provided on your toolkit locally, this path may differ on a Linux platform
    private $container;
    private $entityManager;

    private $searchParameters = array(
        'searchDefinition' => array(
            'parameters' => array(
                'searchpurpose' => 'NewApplication',  //Score required, default YES
                'producttype' => 'Loan', //Search purpose, YOU WILL NEED TO CHANGE THIS TO YOUR SEARCH PURPOSE e.g. TV for Tenant Vet
                //addional parameters can be added here or using the set function below.
            )
        )
    );

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    //set variables in the applicant block
    //usage: $object->setApplicatant('dob',$dob);
    public function setApplicant($tag, $value)
    {
        $this->searchParameters['searchDefinition']['parameters']['applicant'][$tag] = $value;
    }

    //set variables in the applicant name block
    //usage: $object->setApplicantName('forename',$forename, 'P); for a previous name (alias)
    public function setApplicantName($tag, $value)
    {
        $this->searchParameters['searchDefinition']['parameters']['applicant']['name'][$tag] = $value;
    }

    //set variables in the applicant address block, the default is current but the function provides the ability to define a previous address also
    //usage (current address): $object->setApplicantAddress('abodeno', $abodeno);
    //usage (previous address): $object->setApplicantAddress('buildingname', $buildingname,'P');
    public function setApplicantAddress($tag, $value)
    {
        $this->searchParameters['searchDefinition']['parameters']['applicant']['address'][$tag] = $value;
    }

    //set variables in the applicantdemographics phone block
    //usage: $object->setApplicatantPhone('dob',$dob);
    public function setApplicantPhone($std, $number, $type, $n)
    {
        $this->searchParameters['searchDefinition']['parameters']['applicant']['applicantdemographics']['contact']['telephone'][$n]['type'] = $type;
        $this->searchParameters['searchDefinition']['parameters']['applicant']['applicantdemographics']['contact']['telephone'][$n]['std'] = $std;
        $this->searchParameters['searchDefinition']['parameters']['applicant']['applicantdemographics']['contact']['telephone'][$n]['number'] = $number;
    }


    //set variables in the applicantdemographics email block
    //usage: $object->setApplicatantEmail('dob',$dob);
    public function setApplicantEmail($address, $type, $n)
    {
        $this->searchParameters['searchDefinition']['parameters']['applicant']['applicantdemographics']['contact']['email'][$n]['type'] = $type;
        $this->searchParameters['searchDefinition']['parameters']['applicant']['applicantdemographics']['contact']['email'][$n]['address'] = $address;
    }

    //set parameters in the creditrequest block
    //usage: $object->setParameter('autosearch', 1);
    public function setParameter($tag, $value)
    {
        $this->searchParameters['searchDefinition']['parameters'][$tag] = $value;
    }

    //set parameters in the creditrequest block
    //usage: $object->setParameter('autosearch', 1);
    public function setSearchParameters(array $searchParameters)
    {
        $this->searchParameters = $searchParameters;
    }

    public function addJointApplicant($applicantData)
    {
        $this->searchParameters['searchDefinition']['parameters']['applicant'] = array($this->searchParameters['searchDefinition']['parameters']['applicant']);
        $applicantData['joint'] = 1;
        $applicantData['jointapplication'] = 1;
        $this->searchParameters['searchDefinition']['parameters']['applicant'][] = $applicantData;
    }

    public function setApplicantIncomes($netMonthly, $annualGross)
    {
        $this->searchParameters['searchDefinition']['parameters']['applicant']['income']['netmonthlyincome'] = $netMonthly;
        $this->searchParameters['searchDefinition']['parameters']['applicant']['income']['annualgrossincome'] = $annualGross;
    }

    public function getSearchParameters(User $user)
    {
        /** @var UserAddress $address */
        $address = $this->entityManager->getRepository(UserAddress::class)->getUserActiveAddresses($user);

        $address = is_array($address) ? $address[0] : $address;

        $this->setApplicantAddress('buildingnumber', $address->getBuildingNumber());
        $this->setApplicantAddress('street1', $address->getLine1());
        $this->setApplicantAddress('posttown', $address->getCity());
        $this->setApplicantAddress('postcode', $address->getPostalCode());
        $this->setApplicantAddress('addresstype', 'ShortAddress');

        /** @var UserDetails $userDetails */
        $userDetails = $this->entityManager->getRepository(UserDetails::class)->findOneByUser($user);
        $this->setApplicantName('title', $userDetails->getTitle());
        $this->setApplicantName('forename', $userDetails->getFirstName());
        $this->setApplicantName('surname', $userDetails->getLastName());

        $this->setApplicant('dob', date_format($userDetails->getBirthdate(), 'Y-m-d'));

        /** @var UserIncome $userIncome */
        $userIncome = $this->entityManager->getRepository(UserIncome::class)->findOneByUser($user);
        $this->setApplicantIncomes($userIncome->getTotalNetIncomeMonthly(), $userIncome->getTotalIncomeGrossAnn());

        return $this->searchParameters;
    }

    /**
     * This method returns data as array for API
     *
     * @param User $user
     * @param string $purpose
     * @return mixed
     */
    public function getDataForXmlByUser(User $user, $purpose = '')
    {
        $credentialsManager = $this->container->get('whs.manager.call_credit_credentials_manager');
        $credentials = $credentialsManager->getCredentialsByType(CallCreditCredentials::AFFORDABILITY_TYPE);

        $result['headers'] = $credentials['headers'];
        $result['apiurl'] = $credentials['url'];

        $result['searchParameters'] = $this->getSearchParameters($user);

        return $result;
    }
}