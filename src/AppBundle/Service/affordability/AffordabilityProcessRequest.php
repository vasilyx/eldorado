<?php

/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 12.01.2017
 * Time: 14:30
 */

namespace AppBundle\Service\affordability;

use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\CallCreditCredentials;
use AppBundle\Entity\User;
use AppBundle\Event\CallCreditEvents;
use AppBundle\Interfaces\ProcessApiRequestInterface;
use AppBundle\Repository\CallCreditCredentialsRepository;
use Doctrine\ORM\EntityManager;
use LogBundle\Entity\UserActivityLog;
use LogBundle\Handler\UserActivityHandler;
use SoapFault;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\GenericEvent;

class AffordabilityProcessRequest implements ProcessApiRequestInterface
{
    use ContainerAwareTrait;
    /** @var AffordabilityDataProvider */
    private $dataProvider;

    public function __construct(AffordabilityDataProvider $dataProvider)
    {
        $this->dataProvider = $dataProvider;
    }

    /**
     * This method creates JSON with data for API request.
     *
     * CallCreditAPICalls has status NEW, and not sent yet
     *
     * @param CallCreditAPICalls $callCreditAPICalls
     * @return CallCreditAPICalls
     */
    public function processApiRequest(CallCreditAPICalls $callCreditAPICalls)
    {
        $callCreditAPICalls
            ->setType(CallCreditAPICalls::TYPE_AFFORDABILITY)
            ->setStatus(CallCreditAPICalls::STATUS_NEW);

        $user = $callCreditAPICalls->getUser();
        /** Get data for API by user */
        $data = $this->dataProvider->getDataForXmlByUser($user);

        $apiUrl = $data['apiurl'] ? $data['apiurl'] : 'https://ct.callcreditsecure.co.uk/services/affordability/affordability2.asmx';
        $data['apiurl'] = $apiUrl;
        $callCreditAPICalls->setRequestID(md5(time() . random_int(1, 1000)));
        $callCreditAPICalls->setRequestXML(json_encode($data));

        return $callCreditAPICalls;
    }

    /**
     * This method got data for request from CallCreditAPICalls in JSON format; parse it and send.
     *
     * @param CallCreditAPICalls $callCredit
     * @param int $testMode
     */
    public function sendApiRequest(CallCreditAPICalls $callCredit, $testMode = CallCreditAPICalls::STATUS_SUCCESS)
    {
        $data = json_decode($callCredit->getRequestXML(), true);

        $ns = "urn:callcredit.plc.uk/api5";
        //set the SOAP headers using the PHP5 SOAP Extension
        $soapHeaders = new \SOAPHeader($ns, 'callcreditheaders', $data['headers']);

        $serviceDescriptor = __DIR__ . "/../BsbWsdl/Tac/Callcredit.Affordability.wsdl";
        //initialise with tracing to enable request and response XML capture and single elements as arrays instead of objects
        $soapClient = new \SoapClient(
            $serviceDescriptor,
            array(
                'trace' => 1,
                'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
            )
        );

        //set the url to the client test site, NOTE. comment this out to target the LIVE site
        $soapClient->__setLocation($data['apiurl']);

        /** LOG: Borrower Quotation request send */
        $this->logSendRequest($callCredit->getUser());

        try {

            //TODO: remove it (just for hotfix)
//            if ($testMode) {
//                $callCredit->setRequestDate(new \DateTime());
//                $callCredit->setResponseXML($this->getTestResponse());
//                $callCredit->setResponseDate(new \DateTime());
//                $callCredit->setStatus(CallCreditAPICalls::STATUS_SUCCESS);
//            }

            //perform the Affordability 7.0 search passing in the SOAP Headers and the Search Parameters, __soapCall stores the response from the server as an stdClass Object
            //TODO: remove comments
            $callCredit->setRequestDate(new \DateTime());
            $soapClient->__soapCall('Search02c', array($data['searchParameters']), null, $soapHeaders);
            $callCredit->setResponseDate(new \DateTime());
            $callCredit->setStatus(CallCreditAPICalls::STATUS_SUCCESS);
            $callCredit->setRequestXML($soapClient->__getLastRequest());
            $callCredit->setResponseXML($soapClient->__getLastResponse());

        } catch (SoapFault $fault) {
            $callCredit->setResponseDate(new \DateTime());
            $callCredit->setStatus(CallCreditAPICalls::STATUS_ERROR);
            $callCredit->setRequestXML($soapClient->__getLastRequest());
            $callCredit->setResponseXML($soapClient->__getLastResponse());
        } finally {
            /** @var EntityManager $entityManager */
            $entityManager = $this->container->get('doctrine')->getManager();
            $entityManager->persist($callCredit);
            $entityManager->flush();
        }

        if ($callCredit->getStatus() == CallCreditAPICalls::STATUS_SUCCESS) {
            $this->container->get('event_dispatcher')->dispatch(CallCreditEvents::CALL_CREDIT_SUCCESS,
                new GenericEvent(null, ['callcredit_id' => $callCredit->getId()])
            );
        }
    }

    public function changePassword()
    {
    }

    public function logSendRequest(User $user)
    {
        $this->container->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $user, UserActivityHandler::CONTEXT_ACTION => UserActivityLog::SYSTEM_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::BORROWER_AFFORDABILITY_ACTION]);
    }

    public function getTestResponse()
    {
        return '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><Search02cResponse xmlns="urn:callcredit.co.uk/soap:affordabilityapi2"><Search02cResult><parameters><applicant><name><title>Mr</title><forename>Peter</forename><surname>Manx</surname></name><address><buildingnumber>28</buildingnumber><street1>28 Woodhatch Spinney</street1><posttown>Coulsdon</posttown><postcode>CR5 2SU</postcode><addresstype>ShortAddress</addresstype></address><dob>1980-10-30T00:00:00</dob><income><netmonthlyincome>5600</netmonthlyincome><annualgrossincome>88700</annualgrossincome></income></applicant><searchpurpose>NewApplication</searchpurpose><producttype>Loan</producttype></parameters><results><matchlevel>NoMatch</matchlevel><searchdate>2018-05-31T23:49:02</searchdate><searchid>{A36E9D64-CF02-4F9A-B034-811932989858}</searchid></results></Search02cResult></Search02cResponse></soap:Body></soap:Envelope>';
    }
}