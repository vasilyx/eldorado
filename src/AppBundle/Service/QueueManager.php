<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 29.12.2016
 * Time: 14:45
 */

namespace AppBundle\Service;

use AppBundle\Entity\CallCreditAPICalls;
use Doctrine\DBAL\Migrations\OutputWriter;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Process;

class QueueManager
{
    use ContainerAwareTrait;

    public function addCallreportRequestToQueue(CallCreditAPICalls $callCreditAPICalls)
    {
        /**
         * Logging
         */
        $this->container->get('monolog.logger.callcredit')->debug(sprintf('CallreportRequest %1$s is added to queue.', $callCreditAPICalls->getId()), [$this->container->getParameter('kernel.environment')]);
        /**
         * End logging
         */
        $this->container->get("old_sound_rabbit_mq.send_callreport_request_producer")
            ->publish(serialize(array('callcredit_id' => $callCreditAPICalls->getId())));

        $kernel = $this->container->get('kernel');

        $app = new Application($kernel);
        $app->setAutoExit(false);

        $input = new ArrayInput(array(
            'command' => 'rabbitmq:consumer',
            'name' => 'send_callreport_request',
            '--messages' => 1
        ));

        $output = new BufferedOutput();
        $app->run($input, $output);
    }

    public function addCallvalidateRequestToQueue(CallCreditAPICalls $callCreditAPICalls)
    {

        $this->container->get("old_sound_rabbit_mq.send_callvalidate_request_producer")
            ->publish(serialize(array('callcredit_id' => $callCreditAPICalls->getId())));
        /**
         * Logging
         */
        $this->container->get('monolog.logger.callcredit')->debug(sprintf('CallvalidateRequest %1$s is added to queue.', $callCreditAPICalls->getId()), [$this->container->getParameter('kernel.environment')]);
        /**
         * End logging
         */
//        $kernel = $this->container->get('kernel');
//
//        $app = new Application($kernel);
//        $app->setAutoExit(true);
//
//        $input = new ArrayInput(array(
//            'command' => 'rabbitmq:consumer',
//            'name' => 'send_callvalidate_request',
//            '--messages' => 1
//        ));
//
//        $output = new BufferedOutput();
//        $app->run($input, $output);
        /**
         * Logging
         */
        $this->container->get('monolog.logger.callcredit')->debug(sprintf('Consumer send_callvalidate_request is started.'), [$this->container->getParameter('kernel.environment')]);
        /**
         * End logging
         */

    }

    public function addAffordabilityRequestToQueue(CallCreditAPICalls $callCreditAPICalls)
    {
        //TODO: add to queue affordability request
    }
}