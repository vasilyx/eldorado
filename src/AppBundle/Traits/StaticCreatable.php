<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 28.12.16
 * Time: 14:59
 */

namespace AppBundle\Traits;


trait StaticCreatable
{
    /**
     * @return static
     */
    public static function create()
    {
        $class = __CLASS__;
        return new $class();
    }
}