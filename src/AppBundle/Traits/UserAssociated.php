<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 26.12.16
 * Time: 10:45
 */

namespace AppBundle\Traits;


use AppBundle\Entity\User;

trait UserAssociated
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }
}