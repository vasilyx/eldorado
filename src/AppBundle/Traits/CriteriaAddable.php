<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 1.12.16
 * Time: 9.21
 */

namespace AppBundle\Traits;


use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;

trait CriteriaAddable
{
    public function addCriteriaToQueryBuilder(QueryBuilder $queryBuilder, Criteria $criteria = null)
    {
        if (null !== $criteria) {
            $queryBuilder->addCriteria($criteria);
        }

        return $queryBuilder;
    }
}