<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 16.12.16
 * Time: 9:45
 */

namespace AppBundle\Traits\CreditCard;


trait CreditCard
{
    use CreditCardProperties;
    use CreditCardMethods;
}