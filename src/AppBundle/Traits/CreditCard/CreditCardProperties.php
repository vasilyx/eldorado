<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 16.12.16
 * Time: 9:46
 */

namespace AppBundle\Traits\CreditCard;


/**
 * Trait CreditCardProperties
 * @package AppBundle\Traits\CreditCard
 */
trait CreditCardProperties
{
    /**
     * The name of a cardholder
     * @var string
     */
    protected $name;
    /**
     * 20 digits
     * @var string
     */
    protected $cardNumber;
    /**
     * 2 digits
     * @var string
     */
    protected $expiryMonth;
    /**
     * 4 digits
     * @var string
     */
    protected $expiryYear;
    /**
     * 4 digits
     * @var string
     */
    protected $cvc;
}