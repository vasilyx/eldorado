<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 24.01.17
 * Time: 11:33
 */

namespace AppBundle\Traits\Archivable;


/**
 * Class Archivable
 * Trait to realise AppBundle\Interfaces\ArchivableInterface
 * @package AppBundle\Traits\Archivable
 */
trait Archivable
{
    use ArchivableProperties;
    use ArchivableMethods;
}