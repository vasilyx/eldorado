<?php
namespace AppBundle\Traits\Archivable;
/**
 * User: Vladimir
 * Date: 1/25/17
 * Time: 2:21 PM
 */
trait SetArchiveFalseTrait
{
    /**
     * @return $this
     */
    public function setArchiveFalse()
    {
        $this->setArchive(false);
        return $this;
    }
}