<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 24.01.17
 * Time: 11:37
 */

namespace AppBundle\Traits\Archivable;


trait ArchivableProperties
{
    /**
     * @var boolean
     */
    protected $archive;
}