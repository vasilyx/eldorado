<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 24.01.17
 * Time: 11:37
 */

namespace AppBundle\Traits\Archivable;


trait ArchivableMethods
{
    /**
     * @param $archive boolean
     * @return $this
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getArchive()
    {
        return $this->archive;
    }
}