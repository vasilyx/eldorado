<?php

/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 29.12.2016
 * Time: 11:01
 */
namespace AppBundle\RabbitMQ\Consumer;

use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Event\CallCreditEvents;
use Doctrine\ORM\EntityManager;
use \OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use \PhpAmqpLib\Message\AMQPMessage;
use SoapFault;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class CallreportConsumer implements ConsumerInterface
{

    private $entityManager;
    private $container;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        $callCreditId = unserialize($msg->body)['callcredit_id'];
        $callCredit = $this->entityManager->getRepository(CallCreditAPICalls::class)->find($callCreditId);

        $this->container->get('app.callreport_process_request')->sendApiRequest($callCredit);

        return true;
    }
}