<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 09.01.2017
 * Time: 16:28
 */

namespace AppBundle\RabbitMQ\Consumer;


use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Event\CallCreditEvents;
use Doctrine\ORM\EntityManager;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class CallvalidateConsumer implements ConsumerInterface
{
    private $entityManager;
    private $container;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        $callCreditId = unserialize($msg->body)['callcredit_id'];
        $callCredit = $this->entityManager->getRepository(CallCreditAPICalls::class)->find($callCreditId);

        $this->container->get('app.callvalidate_process_request')->sendApiRequest($callCredit);

        return true;
    }
}