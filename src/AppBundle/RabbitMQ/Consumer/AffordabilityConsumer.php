<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 16.01.2017
 * Time: 14:40
 */

namespace AppBundle\RabbitMQ\Consumer;


use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Event\CallCreditEvents;
use Doctrine\ORM\EntityManager;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use SoapFault;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class AffordabilityConsumer implements ConsumerInterface
{
    /** @var EntityManager */
    private $entityManager;
    private $container;

    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->entityManager = $em;
        $this->container = $container;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        $callCreditId = unserialize($msg->body)['callcredit_id'];
        $callCredit = $this->entityManager->getRepository(CallCreditAPICalls::class)->find($callCreditId);

        $this->container->get('app.affordability_process_request')->sendApiRequest($callCredit);

        return true;
    }
}