<?php

/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 23.12.2016
 * Time: 3:42
 */

namespace AppBundle\Listener;

use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Event\CallCreditEvent;
use AppBundle\Exception\BadApiCredentialsException;
use AppBundle\Interfaces\DataProviderInterface;
use AppBundle\Interfaces\ProcessApiRequestInterface;
use AppBundle\Service\affordability\AffordabilityProcessRequest;
use AppBundle\Service\callcredit\CallReportProcessRequest;
use AppBundle\Service\callvalidate\CallValidateDataProvider;
use AppBundle\Service\callvalidate\CallValidateProcessRequest;
use AppBundle\Service\QueueManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class CallCreditApiListener
{
    protected $container;
    /** @var QueueManager */
    protected $queueManager;

    public function __construct(ContainerInterface $container, QueueManager $queueManager)
    {
        $this->container = $container;
        $this->queueManager = $queueManager;
    }

    /**
     * Init CallCreditApiCalls object and if user has ROLE_USER  add to queue message,
     *  else return and flush initialized object.
     */
    public function processCallReportRequest(CallCreditEvent $callCreditEvent)
    {
        /** @var CallReportProcessRequest $processRequest */
        $processRequest = $this->container->get("app.callreport_process_request");
        $tokenStorage = $this->container->get("security.token_storage");
        /** @var CallCreditAPICalls $callCreditApiCalls */
        $callCreditApiCalls = $callCreditEvent->getCallCreditApiCalls();
        $user = $callCreditApiCalls->getQuoteApply()->getUser();
        $callCreditApiCalls->setUser($user);
        $callCreditApiCalls->setStatus($callCreditApiCalls::STATUS_NEW);
        $callCreditApiCalls = $processRequest->processApiRequest($callCreditApiCalls);

        /** @var AuthorizationChecker $authChecker */
        $authChecker = $this->container->get("security.authorization_checker");
        $em = $this->container->get('doctrine.orm.entity_manager');

        if ($authChecker->isGranted('ROLE_USER')) {
            $callCreditApiCalls->setStatus(CallCreditAPICalls::STATUS_PENDING);
            /**
             * Logging
             */
            $this->container->get('monolog.logger.callcredit')->debug(sprintf('CallReportRequest %1$s is ready to be added to queue.', $callCreditApiCalls->getId()), [$this->container->getParameter('kernel.environment')]);
            /**
             * End logging
             */
            //$this->queueManager->addCallreportRequestToQueue($callCreditApiCalls);
        }

        $em->persist($callCreditApiCalls);
        $em->flush();
    }

    /** Init CallCreditAPICalls object before request processing */
    public function processCallValidateRequest(CallCreditEvent $callCreditEvent)
    {
        /** @var CallValidateProcessRequest $processRequest */
        $processRequest = $this->container->get("app.callvalidate_process_request");
        $tokenStorage = $this->container->get("security.token_storage");
        $user = $callCreditEvent->getCallCreditApiCalls()->getQuoteApply()->getUser();
        /** @var CallCreditAPICalls $callCreditApiCalls */
        $callCreditApiCalls = $callCreditEvent->getCallCreditApiCalls();
        $callCreditApiCalls->setUser($user);
        try {
            $callCreditApiCalls = $processRequest->processApiRequest($callCreditApiCalls);
        } catch (BadApiCredentialsException $exception) {
            $this->container->get('logger')->debug($exception->getMessage());
            throw new BadApiCredentialsException();
        }

        $callCreditApiCalls->setStatus(CallCreditAPICalls::STATUS_PENDING);
        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($callCreditApiCalls);
        $em->flush();

        /**
         * Logging
         */
        $this->container->get('monolog.logger.callcredit')->debug(sprintf('CallValidateRequest %1$s is ready to be added to queue.', $callCreditApiCalls->getId()), [$this->container->getParameter('kernel.environment')]);
        /**
         * End logging
         */

        //$this->queueManager->addCallvalidateRequestToQueue($callCreditApiCalls);
    }

    /** Init CallCreditAPICalls object before request processing */
    public function processAffordabilityRequest(CallCreditEvent $callCreditEvent)
    {
        /** @var AffordabilityProcessRequest $processRequest */
        $processRequest = $this->container->get("app.affordability_process_request");
        $user = $callCreditEvent->getCallCreditApiCalls()->getQuoteApply()->getUser();
        /** */
        $calCreditApiCalls = $callCreditEvent->getCallCreditApiCalls();
        $calCreditApiCalls->setUser($user);
        $calCreditApiCalls = $processRequest->processApiRequest($calCreditApiCalls);
        $calCreditApiCalls->setStatus(CallCreditAPICalls::STATUS_PENDING);

        $em = $this->container->get('doctrine')->getManager();
        $em->persist($calCreditApiCalls);
        $em->flush();

        /**
         * Logging
         */
        $this->container->get('monolog.logger.callcredit')->debug(sprintf('AffordabilityRequest %1$s is ready to be added to queue.', $calCreditApiCalls->getId()), [$this->container->getParameter('kernel.environment')]);
        /**
         * End logging
         */

        //$this->queueManager->addAffordabilityRequestToQueue($callCreditApiCalls);
    }
}