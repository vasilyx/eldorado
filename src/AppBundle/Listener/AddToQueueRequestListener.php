<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 29.12.2016
 * Time: 16:29
 */

namespace AppBundle\Listener;


use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\User;
use AppBundle\Service\QueueManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\GenericEvent;

class AddToQueueRequestListener
{
    private $em;
    private $queueManager;

    public function __construct(QueueManager $queueManager, EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->queueManager = $queueManager;
    }

    /** Send to queue message with callCredit object id after user_email confirmation. */
    public function addToQueue(GenericEvent $event)
    {
        $userId = $event->getArgument('user_id');
        $user = $this->em->getRepository(User::class)->find($userId);

        $callcredit = $this->em->getRepository(CallCreditAPICalls::class)->findOneBy(['user' => $user, 'status' => CallCreditAPICalls::STATUS_NEW, 'type' => CallCreditAPICalls::TYPE_CALLREPORT]);

        if ($callcredit) {
            $callcredit->setStatus(CallCreditAPICalls::STATUS_PENDING);
            $this->em->persist($callcredit);
            $this->em->flush();

           // $this->queueManager->addCallreportRequestToQueue($callcredit);
        }
    }
}