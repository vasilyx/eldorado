<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 13.01.2017
 * Time: 11:52
 */

namespace AppBundle\Listener;


use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\Product;
use AppBundle\Entity\UnderwritingResult;
use Prophecy\Call\Call;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\GenericEvent;

class CallCreditResponseListener
{
    use ContainerAwareTrait;

    /**
     * If CallCredit response is successful, start underwritingResult
     * algorithm based on TYPE and PURPOSE
     *
     */
    public function processCallCreditResponse(GenericEvent $event)
    {
        $callCreditId = $event->getArgument('callcredit_id');
        /** @var CallCreditAPICalls $callCredit */
        $callCredit = $this->container->get('doctrine')->getRepository(CallCreditAPICalls::class)->find($callCreditId);

        switch ($callCredit->getType()) {
            case (CallCreditAPICalls::TYPE_CALLREPORT):
                if ($callCredit->getQuoteApply()->getProduct()->getType() == Product::TYPE_BORROWER) {
                    switch ($callCredit->getPurpose()) {
                        case (CallCreditAPICalls::PURPOSE_QS):
                            $this->container->get("whs.manager.underwriting_result_manager")
                                ->borrowerUnderwritingAlgorithm($callCredit, UnderwritingResult::TYPE_QUOTE);
                            break;
                        case (CallCreditAPICalls::PURPOSE_CA):
                            $this->container->get("whs.manager.underwriting_result_manager")
                                ->borrowerUnderwritingAlgorithm($callCredit, UnderwritingResult::TYPE_CREDIT);
                            break;
                        default:
                            break;
                    }
                }
                break;
            case (CallCreditAPICalls::TYPE_CALLVALIDATE):
                if ($callCredit->getQuoteApply()->getProduct()->getType() == Product::TYPE_INVESTOR) {
                    switch ($callCredit->getPurpose()) {
                        case (CallCreditAPICalls::PURPOSE_QS):
                            $this->container->get("whs.manager.underwriting_result_manager")
                                ->lenderUnderwritingAlgorithm($callCredit, UnderwritingResult::TYPE_QUOTE);
                            break;
                        default:
                            break;
                    }
                    break;
                }
                break;
            default:
                break;
        }
    }
}