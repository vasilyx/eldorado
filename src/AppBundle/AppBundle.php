<?php

namespace AppBundle;

use AppBundle\DependencyInjection\CompilerPass\ProcessHandlersPass;
use AppBundle\DependencyInjection\CompilerPass\SubscriberHandlersPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new SubscriberHandlersPass());
        $container->addCompilerPass(new ProcessHandlersPass());
    }
}
