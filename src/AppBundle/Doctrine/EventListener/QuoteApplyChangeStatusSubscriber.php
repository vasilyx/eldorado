<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 09.01.17
 * Time: 10:16
 */

namespace AppBundle\Doctrine\EventListener;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\Product;
use AppBundle\Entity\QuoteApply;
use AppBundle\Event\EntityLifecycleEvents;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use LogBundle\Entity\UserActivityLog;
use LogBundle\Handler\UserActivityHandler;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\GenericEvent;


/**
 * Class QuoteApplyChangeStatusSubscriber
 * When the QuoteApply status is changed with Doctrine from any place we need to be notified about this
 * @package AppBundle\Doctrine\EventListener
 */
class QuoteApplyChangeStatusSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;

    public function getSubscribedEvents()
    {
        return array(
            'onFlush',
            'postFlush'
        );
    }

    /**
     * @var GenericEvent $event
     */
    private $quoteApplyStatusChangedEvent;

    private $loanApplyIsCreatedForQuoteApply;


    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        /**
         * Search quote_apply entities with updated status and create event
         */
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof QuoteApply && !empty($uow->getEntityChangeSet($entity)['status'])) {
                $event = new GenericEvent($entity);
                $this->quoteApplyStatusChangedEvent = $event
                    ->setArgument('oldStatus', $uow->getEntityChangeSet($entity)['status'][0])
                    ->setArgument('newStatus', $uow->getEntityChangeSet($entity)['status'][1]);

                if ($this->quoteApplyStatusChangedEvent) {
                    /**
                     * We generate an own event to decouple our domain-listener from the Doctrine
                     */
                    /**
                     * @var $event GenericEvent
                     */
                    $event = $this->container->get('event_dispatcher')->dispatch(EntityLifecycleEvents::QUOTE_APPLY_STATUS_UPDATED,
                        $this->quoteApplyStatusChangedEvent
                    );
                    if ($event->hasArgument('entitiesToPersist')) {
                        foreach ($event->getArgument('entitiesToPersist') as $entityToPersist) {

                            /*
                             * to know that LoanApply is persisted to log in the postFlush
                             */
                            if ($entityToPersist instanceof LoanApply) {
                                $this->loanApplyIsCreatedForQuoteApply = $entityToPersist->getQuoteApply();
                            }

                            $em->persist($entityToPersist);

                            $classMetadata = $em->getClassMetadata(get_class($entityToPersist));
                            $uow->computeChangeSet($classMetadata, $entityToPersist);
                            /**
                             * Logging
                             */
                            $this->container->get('monolog.logger.callcredit')->debug(sprintf('LoanApply %1$s is persist.', $entityToPersist->getId()), [$this->container->getParameter('kernel.environment')]);
                            /**
                             * End logging
                             */
                        }
                    }
                }
            }
        }
    }

    public function postFlush(PostFlushEventArgs $args)
    {
        /*
         * We need log LoanApply creation
         */
        /** @var LoanApply $loanApply */
        if ($loanApply = $args->getEntityManager()->getRepository(LoanApply::class)->findOneBy(['quoteApply' => $this->loanApplyIsCreatedForQuoteApply])) {
            $this->container->get('monolog.logger.loan_apply_status')->info('', [$loanApply]);
            /*
             * to send record to logger only once
             */
            $this->loanApplyIsCreatedForQuoteApply = null;

            $this->container->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $loanApply->getUser(), UserActivityHandler::CONTEXT_ACTION => UserActivityLog::BORROWER_PRODUCT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::LOAN_REQUEST_CREATED]);
        }
    }
}