<?php
namespace AppBundle\Doctrine\EventListener;
use AppBundle\Entity\Agreement;
use AppBundle\Entity\ContractMatching;
use AppBundle\Entity\File;
use AppBundle\Entity\LoanApply;

use AppBundle\Manager\FileManager;
use AppBundle\Service\Mailer;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\GenericEvent;



class LoanApplyChangeStatusSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;

    protected $loanApplyEntity = [];

    public function getSubscribedEvents()
    {
        return array(
            Events::postUpdate
        );
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $em = $args->getEntityManager();
        if ($entity instanceof LoanApply) {

            if ($entity->getStatus() == LoanApply::STATUS_ACTIVE) {
                $em->getRepository(ContractMatching::class)->updateStatusByLoanApply($entity, ContractMatching::STATUS_ACTIVE);
                /** @var FileManager $fm */
                $fm = $this->container->get('whs.manager.file_manager');
                $fm->generateFile($entity, File::FINAL_BORROWER_AGREEMENT_TYPE);
                $fm->generateFilesByLoanApply($entity, File::LENDER_AGREEMENT_TYPE);
            }

            if ($entity->getStatus() == LoanApply::STATUS_SETTLED) {
                $em->getRepository(ContractMatching::class)->updateStatusByLoanApply($entity, ContractMatching::STATUS_SETTLED);
            }

            if ($entity->getStatus() == LoanApply::STATUS_ACCEPTANCE) {
                /** @var Mailer $mailer */
                $mailer = $this->container->get('whs_mailer');
                $user = $entity->getUser();
                $mailer->sendAcceptanceNotification($user->getUsername(), ['name' => $user->getDetails()->getFirstName()]);
            }
        }
    }
}