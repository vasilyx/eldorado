<?php
/**
 * Created by PhpStorm.
 * User: mikhailpegasin
 * Date: 18.11.16
 * Time: 15:28
 */

namespace AppBundle\Doctrine\Filters;


use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class SoftDeletableFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        // Check if the entity implements the LocalAware interface
        if (!$targetEntity->reflClass->implementsInterface('AppBundle\Interfaces\SoftDeletableInterface')) {
            return "";
        }

        return $targetTableAlias.'.deleted_at IS NULL';
    }
}