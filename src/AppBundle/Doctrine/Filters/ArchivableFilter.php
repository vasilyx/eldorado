<?php
/**
 * User: Vladimir
 * Date: 2/2/17
 * Time: 4:44 PM
 */

namespace AppBundle\Doctrine\Filters;


use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class ArchivableFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        // Check if the entity implements the ArchivableInterface interface
        if (!$targetEntity->reflClass->implementsInterface('AppBundle\Interfaces\ArchivableInterface')) {
            return "";
        }
        return $targetTableAlias . '.archive = ' . 0;
    }

}