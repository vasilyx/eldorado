<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 2.12.16
 * Time: 11.37
 */

namespace AppBundle\Role;


use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\User;
use AppBundle\Interfaces\RoleResolverInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class QuoteStatusRoleResolver implements RoleResolverInterface
{
    use ContainerAwareTrait;

    const ROLE_QUOTE_DRAFT = 'ROLE_QUOTE_DRAFT';
    const ROLE_QUOTE_PENDING = 'ROLE_QUOTE_PENDING';
    const ROLE_QUOTE_REJECTED = 'ROLE_QUOTE_REJECTED';
    const ROLE_QUOTE_APPROVED = 'ROLE_QUOTE_APPROVED';
    const ROLE_USER = 'ROLE_USER';

    public static function getStatuses()
    {
        return [
            QuoteApply::STATUS_DRAFT => self::ROLE_QUOTE_DRAFT,
            QuoteApply::STATUS_PENDING => self::ROLE_USER,
            QuoteApply::STATUS_APPROVED => self::ROLE_USER,
            QuoteApply::STATUS_REJECTED => self::ROLE_USER
        ];
    }

    public function resolveRoles(User $user)
    {
        $roles = ['ROLE_USER'];

        if ($quoteApply = $this->container->get('whs.manager.quote_apply_manager')->getDraftQuoteApply($user)) {
            $roles = [
                static::getStatuses()[$quoteApply->getStatus()]
            ];
        };

        if (!$user->getActive() && $user->getConfirmEmailToken()) {
            $roles = [
                User::ROLE_EMAIL_NOT_CONFIRMED
            ];
        }

        return $roles;
    }
}