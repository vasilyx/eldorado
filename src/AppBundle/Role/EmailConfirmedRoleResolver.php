<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 15.12.16
 * Time: 12:01
 */

namespace AppBundle\Role;


use AppBundle\Entity\User;
use AppBundle\Interfaces\RoleResolverInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class EmailConfirmedRoleResolver implements RoleResolverInterface
{
    use ContainerAwareTrait;

    const ROLE_USER = 'ROLE_USER';

    public function resolveRoles(User $user)
    {
        $roles = [self::ROLE_USER];

        if (!$user->getActive() && $user->getConfirmEmailToken()) {
            $roles = [
                User::ROLE_EMAIL_NOT_CONFIRMED
            ];
        }

        return $roles;
    }
}