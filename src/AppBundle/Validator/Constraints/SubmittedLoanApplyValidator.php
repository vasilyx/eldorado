<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 25.05.2018
 * Time: 16:30
 */

namespace AppBundle\Validator\Constraints;


use AppBundle\Entity\LoanApply;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class SubmittedLoanApplyValidator extends ConstraintValidator
{

    /**
     * Checks if the passed value is valid.
     *
     * @param LoanApply $loanApply
     * @param Constraint $constraint The constraint for the validation
     * @internal param mixed $value The value that should be validated
     */
    public function validate($loanApply, Constraint $constraint)
    {
        if (empty($loanApply->getApr())) {
            $this->context->buildViolation($constraint->message)
                          ->setParameter('%string%', 'loan_apr')
                          ->addViolation()
            ;
        }

        if (empty($loanApply->getMonthly())) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', 'loan_monthly')
                ->addViolation()
            ;
        }

        if (empty($loanApply->getAmount())) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', 'loan_amount')
                ->addViolation()
            ;
        }

        if (empty($loanApply->getTerm())) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', 'loan_term')
                ->addViolation()
            ;
        }
    }
}