<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 25.05.2018
 * Time: 16:31
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class SubmittedLoanApply extends Constraint
{
    public $message = 'The %property% not specified in the request body';
}