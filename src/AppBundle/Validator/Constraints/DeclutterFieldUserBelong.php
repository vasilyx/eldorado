<?php
/**
 * User: Vladimir
 * Date: 1/3/17
 * Time: 2:08 PM
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class DeclutterFieldUserBelong extends Constraint
{
    public $message = '%entity% must belong to current user';
    public $entityName;
}