<?php
/**
 * Created by PhpStorm.
 * User: mikhailpegasin
 * Date: 15.11.16
 * Time: 14:35
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class PasswordConfirmed extends Constraint
{
    public $message = 'Password confirmation does not match password';
    public $errorPath = null;

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}