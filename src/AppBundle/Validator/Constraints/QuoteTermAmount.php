<?php
/**
 * User: Vladimir
 * Date: 2/7/17
 * Time: 4:28 PM
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class QuoteTermAmount extends Constraint
{
    public $message = 'Minimal amount "min". Maximal amount "max".';
}