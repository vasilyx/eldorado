<?php
/**
 * User: Vladimir
 * Date: 12/7/16
 * Time: 5:32 PM
 */

namespace AppBundle\Validator\Constraints;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UserEmploymentCurrentValidator extends ConstraintValidator
{
    private $em;
    private $tokenStorage;

    public function __construct(EntityManager $em, TokenStorage $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    public function validate($value, Constraint $constraint)
    {
        if (true == $value) {

            $validatedUserEmployment = $this->context->getObject();
            //if try to update existing entity which already has current status, then omit this validation rule
            if ($id = $validatedUserEmployment->getId()) {
                $this->em->detach($validatedUserEmployment);
                $oldUserEmployment = $this->em->getRepository("AppBundle:UserEmployment")->find($id);
                if ($validatedUserEmployment->getCurrentStatus() && $oldUserEmployment->getCurrentStatus()) {
                    return;
                }
            }

            $user = $this->tokenStorage->getToken()->getUser();
            $currentCount = $this->em->getRepository("AppBundle:UserEmployment")->countCurrentEmploymentForUser($user);
            if (null !== $constraint->max && $currentCount >= $constraint->max) {
                $this->context
                    ->buildViolation($constraint->message)
                    ->setParameter('%max_current_count%', $constraint->max)
                    ->setParameter('%current_count%', $currentCount)
                    ->addViolation();
            }
        }
    }
}