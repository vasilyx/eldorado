<?php
/**
 * Created by PhpStorm.
 * User: mikhailpegasin
 * Date: 21.11.16
 * Time: 16:19
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ObjectNotNullValidator extends ConstraintValidator
{
    public function validate($object, Constraint $constraint)
    {
        if (null !== $constraint->errorPath && !is_string($constraint->errorPath)) {
            throw new UnexpectedTypeException($constraint->errorPath, 'string or null');
        }

        $errorPath = null !== $constraint->errorPath ? $constraint->errorPath : 'noErrorPath';

        if (empty($object)) {
            /**
             * @var ExecutionContextInterface $context
             */
            $context = $this->context;
            $context->buildViolation($constraint->message)
                ->atPath($errorPath)
                ->setParameter('%object%', $errorPath)
                ->addViolation();
        }

    }
}