<?php
/**
 * Created by PhpStorm.
 * User: mikhailpegasin
 * Date: 21.11.16
 * Time: 16:18
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class ObjectNotNull extends Constraint
{
    public $message = 'Object %object% must not be null';
    public $errorPath = null;

    public function setErrorPath($errorPath)
    {
        $this->errorPath = $errorPath;
        return $this;
    }

    public function setPayload(array $payload)
    {
        $this->payload = $payload;
        return $this;
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}