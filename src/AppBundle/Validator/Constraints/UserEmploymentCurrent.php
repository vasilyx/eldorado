<?php
/**
 * User: Vladimir
 * Date: 12/7/16
 * Time: 5:26 PM
 */

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UserEmploymentCurrent extends Constraint
{
    public $message = 'User may have only %max_current_count% (now %current_count%) or less current employments.';
    public $max;

}