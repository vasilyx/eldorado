<?php
/**
 * User: Vladimir
 * Date: 2/7/17
 * Time: 4:28 PM
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class QuoteTermAmountValidator extends ConstraintValidator
{
    private $quoteTermAndDecluttersAmount;

    public function __construct($quoteTermAndDecluttersAmount)
    {
        $this->quoteTermAndDecluttersAmount = $quoteTermAndDecluttersAmount;
    }

    public function validate($amount, Constraint $constraint)
    {
        if ($amount < $this->quoteTermAndDecluttersAmount['min'] || $amount > $this->quoteTermAndDecluttersAmount['max']) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('min', $this->quoteTermAndDecluttersAmount['min'])
                ->setParameter('max', $this->quoteTermAndDecluttersAmount['max'])
                ->addViolation();
        }
    }
}