<?php
/**
 * User: Vladimir
 * Date: 1/13/17
 * Time: 2:11 PM
 */

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CreditlineMonthlyPaymentValidator extends ConstraintValidator
{
    public function validate($monthlyPayment, Constraint $constraint)
    {
        $userCreditline = $this->context->getObject();

        if ($userCreditline->getBalanceInput() == 0 && $monthlyPayment != 0) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }

        if ($userCreditline->getBalanceInput() != 0 && $monthlyPayment == 0) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}