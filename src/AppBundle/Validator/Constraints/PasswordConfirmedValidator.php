<?php
/**
 * Created by PhpStorm.
 * User: mikhailpegasin
 * Date: 15.11.16
 * Time: 14:37
 */

namespace AppBundle\Validator\Constraints;


use AppBundle\Entity\User;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class PasswordConfirmedValidator extends ConstraintValidator
{
    public function validate($user, Constraint $constraint)
    {
        if (null !== $constraint->errorPath && !is_string($constraint->errorPath)) {
            throw new UnexpectedTypeException($constraint->errorPath, 'string or null');
        }

        $errorPath = null !== $constraint->errorPath ? $constraint->errorPath : 'password';

        if ($user instanceof User) {
            if ($user->getPlainPassword() !== $user->getPassword()) {
                $this->context->buildViolation($constraint->message)
                    ->atPath($errorPath)
                    ->addViolation();
            }
        }
    }
}