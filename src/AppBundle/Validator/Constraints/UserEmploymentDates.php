<?php
/**
 * User: Vladimir
 * Date: 12/8/16
 * Time: 11:53 AM
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class UserEmploymentDates extends Constraint
{
    public $message = 'End date must be great then start date. End data should not be blank if employment not current.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}