<?php
/**
 * User: Vladimir
 * Date: 1/13/17
 * Time: 2:08 PM
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class CreditlineLimit extends Constraint
{
    public $message = 'CreditLimit must be more than BalanceInput';
}