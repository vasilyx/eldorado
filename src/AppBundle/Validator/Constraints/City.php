<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 09.12.16
 * Time: 16:01
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class City extends Constraint
{
    public $message = 'The %string% is not valid city name.';

    public function getTargets()
    {
        return self::PROPERTY_CONSTRAINT;
    }
}