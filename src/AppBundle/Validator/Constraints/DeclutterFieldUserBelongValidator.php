<?php
/**
 * User: Vladimir
 * Date: 1/3/17
 * Time: 2:11 PM
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DeclutterFieldUserBelongValidator extends ConstraintValidator
{
    private $tokenStorage;
    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function validate($entity, Constraint $constraint)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if($user != $entity->getUser()){
            $this->context->buildViolation($constraint->message)
                ->setParameter('%entity%', $constraint->entityName)
                ->addViolation();
        }
    }
}