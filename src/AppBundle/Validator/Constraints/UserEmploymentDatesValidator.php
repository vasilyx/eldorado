<?php
/**
 * User: Vladimir
 * Date: 12/8/16
 * Time: 12:04 PM
 */

namespace AppBundle\Validator\Constraints;


use AppBundle\Entity\UserEmployment;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UserEmploymentDatesValidator extends ConstraintValidator
{
    /**
     * @param UserEmployment $userEmployment
     * @param Constraint $constraint
     */
    public function validate($userEmployment, Constraint $constraint)
    {
        if (!$userEmployment->getCurrentStatus()) {
            if (!($userEmployment->getOccMonthStarted() instanceof \DateTime) || !($userEmployment->getOccMonthEnded() instanceof \DateTime)) {
                $this->context->buildViolation($constraint->message)
                    ->atPath('occMonthEnded')
                    ->addViolation();
                return;
            }
            //set null day and time because of comparing only months
            $monthStarted = $userEmployment->getOccMonthStarted()->modify("first day of this month")->setTime(0, 0, 0);
            $monthEnded = $userEmployment->getOccMonthEnded()->modify("first day of this month")->setTime(0, 0, 0);

            if ($monthStarted >= $monthEnded) {
                $this->context->buildViolation($constraint->message)
                    ->atPath('occMonthEnded')
                    ->addViolation();
            }
        }
    }
}