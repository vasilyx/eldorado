<?php
/**
 * User: Vladimir
 * Date: 1/13/17
 * Time: 2:11 PM
 */

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CreditlineLimitValidator extends ConstraintValidator
{
    public function validate($creditLimit, Constraint $constraint)
    {
        $userCreditline = $this->context->getObject();
        if ($creditLimit < $userCreditline->getBalanceInput()) {
            $this->context->buildViolation($constraint->message)->addViolation();

        }
    }
}