<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 09.12.16
 * Time: 16:01
 */

namespace AppBundle\Validator\Constraints;


use AppBundle\Entity\UserAddress;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CityValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!preg_match('/^[A-Z\s\.\-\']+$/i', $value, $matches)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $value)
                ->addViolation();
        }
    }
}