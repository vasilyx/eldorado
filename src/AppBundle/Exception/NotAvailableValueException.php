<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 08.12.16
 * Time: 11:25
 */

namespace AppBundle\Exception;
use AppBundle\Entity\Communication;
use Exception;


/**
 * Class NotAvailableValueException
 * @package AppBundle\Exception
 */
class NotAvailableValueException extends \Exception
{
    const NOT_AVAILABLE_COMMUNICATION_TYPE = 5001;

    /**
     * NotAvailableValueException constructor.
     * @param int $code
     * @param Exception $previous
     */
    public function __construct($code, Exception $previous = null)
    {
        parent::__construct('File: ' . self::getFile() . '. Line: ' . self::getLine() . '. ' . self::getMessages()[$code], $code, $previous);
    }

    private static function getMessages()
    {
        return [
          self::NOT_AVAILABLE_COMMUNICATION_TYPE        =>  'Communication type value not available. Available values are ' . implode(', ', array_keys(Communication::getCommunicationTypes()))
        ];
    }
}