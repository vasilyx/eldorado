<?php
/**
 * User: Vladimir
 * Date: 1/27/17
 * Time: 1:20 PM
 */

namespace AppBundle\Exception;


use Exception;

class QuoteCreationException extends \Exception
{
    const DRAFTED_QUOTE_EXIST = ['message' => "Drafted Quote for user exist. Can't create new.", 'code' => 5600];

    public function __construct(Exception $previous = null)
    {
        parent::__construct(self::DRAFTED_QUOTE_EXIST['message'], self::DRAFTED_QUOTE_EXIST['code'], $previous);
    }
}