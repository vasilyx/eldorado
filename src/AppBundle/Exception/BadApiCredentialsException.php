<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 06.02.17
 * Time: 13:48
 */

namespace AppBundle\Exception;


class BadApiCredentialsException extends \Exception
{
    protected $message = 'Bad api credentials';

    public function __construct()
    {
        parent::__construct($this->message);
    }
}