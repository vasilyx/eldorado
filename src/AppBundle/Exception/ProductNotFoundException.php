<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 01.02.17
 * Time: 14:19
 */

namespace AppBundle\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductNotFoundException extends NotFoundHttpException
{
    const PRODUCT_CODE_NOT_FOUND = 5510;

    public static function getExceptions()
    {
        return [
            self::PRODUCT_CODE_NOT_FOUND             =>  'product with code %1$s not found',
        ];
    }

    /**
     * Constructor
     * @param int        $code     The internal exception code
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     */
    public function __construct($code = null, $message = null, \Exception $previous = null)
    {
        if (null === $message) {
            $message = ucfirst(sprintf(static::getExceptions()[$code], $message));
        }
        parent::__construct($message, $previous, $code);
    }
}