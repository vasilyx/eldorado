<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 06.12.16
 * Time: 15:41
 */

namespace AppBundle\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class QuoteApplyNotFoundException extends NotFoundHttpException
{
    const QUOTE_APPLY_NOT_FOUND = 5551;
    const DRAFT_QUOTE_APPLY_NOT_FOUND = 5552;
    const APPROVED_QUOTE_APPLY_NOT_FOUND = 5553;

    public static function getExceptions()
    {
        return [
            self::QUOTE_APPLY_NOT_FOUND             =>  'active',
            self::DRAFT_QUOTE_APPLY_NOT_FOUND       =>  'draft',
            self::APPROVED_QUOTE_APPLY_NOT_FOUND    =>  'approved'
        ];
    }

    /**
     * Constructor.
     * @param int        $code     The internal exception code
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     */
    public function __construct($code = 5551, $message = null, \Exception $previous = null)
    {
        if (null === $message) {
            $message = ucfirst(static::getExceptions()[$code]) . ' QuoteApply does not found for current user';
        }
        parent::__construct($message, $previous, $code);
    }
}