<?php
namespace AppBundle\Event\TransactionEventsHandler;

use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\InvestOfferProcess;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\WorldpayHtmlTransaction;
use AppBundle\Event\CallCreditEvent;
use AppBundle\Event\CallCreditEvents;
use AppBundle\Event\SystemTransactionEvent;
use AppBundle\Manager\QuoteApplyManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class OFREventsHandler.
 * We may save entities without flush because it is used in subscriber.
 * @package AppBundle\Event\TransactionEventsHandler
 */
class OFREventsHandler
{
    use ContainerAwareTrait;

    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function onOFRF(SystemTransactionEvent $systemEvent, $eventName, EventDispatcherInterface $eventDispatcher)
    {
        /** @var InvestOffer $investOffer */
        $investOffer = $systemEvent->getProcess()->getInvestOffer();
        /**
         * @var ContainerAwareEventDispatcher $eventDispatcher
         */
        $container = $eventDispatcher->getContainer();
        $container->get('whs.manager.invest_offer_manager')->createSubOffers($investOffer);


        if ($container->get('whs.manager.quote_apply_manager')->hasLenderActiveQuote($investOffer->getUser())) {
            $investOffer->setStatus(InvestOffer::STATUS_ACTIVE);
            $this->entityManager->persist($investOffer);
            $this->entityManager->flush();
        } else {
            $investOffer->setStatus(InvestOffer::STATUS_PENDING);
            $this->entityManager->persist($investOffer);
            $this->entityManager->flush();
        }

        $systemEvent
            ->addTransaction(
                Transaction::create()
                    ->setUser($systemEvent->getUser())
                    ->setEvent($systemEvent->getInnerEvent())
                    ->setTransactionAmount(-1 * $systemEvent->getProcess()->getInvestOffer()->getInvestmentAmount())
                    ->setTransactionOwner(Transaction::TRANSACTION_OWNER_INVESTOR)
                    ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_CASH_PENDING)
                    ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_OFFER)
                    ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR)
                    ->setOfferId($investOffer->getId())
                    ->setInvestmentType($investOffer->getInvestmentType())
            )
            ->addTransaction(
                Transaction::create()
                    ->setUser($systemEvent->getUser())
                    ->setEvent($systemEvent->getInnerEvent())
                    ->setTransactionAmount($systemEvent->getProcess()->getInvestOffer()->getInvestmentAmount())
                    ->setTransactionOwner(Transaction::TRANSACTION_OWNER_INVESTOR)
                    ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_CASH_AVAILABLE)
                    ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_AVAILABLE)
                    ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR)
                    ->setOfferId($investOffer->getId())
                    ->setInvestmentType($investOffer->getInvestmentType())
            )
        ;
    }

    /**
     * This
     *
     * @param SystemTransactionEvent $systemEvent
     * @param $eventName
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function onALOC(SystemTransactionEvent $systemEvent, $eventName, EventDispatcherInterface $eventDispatcher)
    {
        /**
         * @var InvestOffer $investOffer
         */
        $investOffer = $systemEvent->getProcess()->getInvestOffer();

        /**
         * @var Transaction $transaction
         */
        foreach ($systemEvent->getTransactions() as $transaction) {

            $transaction
                ->setUser($systemEvent->getUser())
                ->setEvent($systemEvent->getInnerEvent())
                ->setTransactionAmount(-1 * $transaction->getTransactionAmount())
                ->setTransactionOwner(Transaction::TRANSACTION_OWNER_INVESTOR)
                ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_OFFER_ALLOCATED)
                ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_ALLOC)
                ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR)
                ->setOfferId($investOffer->getId())
                ->setInvestmentType($investOffer->getInvestmentType())
            ;
        }

        $systemEvent
            ->addTransaction(
                Transaction::create()
                    ->setUser($systemEvent->getUser())
                    ->setEvent($systemEvent->getInnerEvent())
                    ->setTransactionAmount(-1 * $transaction->getTransactionAmount())
                    ->setTransactionOwner(Transaction::TRANSACTION_OWNER_INVESTOR)
                    ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_CASH_ON_OFFER)
                    ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_OFFER)
                    ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR)
                    ->setOfferId($investOffer->getId())
                    ->setInvestmentType($investOffer->getInvestmentType())
            )
        ;
    }

    protected function transactionLiquidate(Transaction $transaction)
    {
        $systemTransaction = Transaction::create()
            ->setTransactionOwner($transaction->getTransactionOwner())
            ->setTransactionAccount($transaction->getTransactionAccount())
            ->setTransactionAmount(-1 * $transaction->getTransactionAmount())
            ->setParentTransaction($transaction->getParentTransaction() ?: $transaction)
        ;

        return $systemTransaction;
    }
}