<?php
namespace AppBundle\Event\TransactionEventsHandler;

use AppBundle\Entity\Agreement;
use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\Declutter;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\WorldpayHtmlTransaction;
use AppBundle\Event\CallCreditEvent;
use AppBundle\Event\CallCreditEvents;
use AppBundle\Event\SystemTransactionEvent;
use AppBundle\Manager\QuoteApplyManager;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class CRDEventsHandler.
 * We may save entities without flush because it is used in subscriber.
 * @package AppBundle\Event\TransactionEventsHandler
 */
class CRDEventsHandler
{
    public function onCRDS(SystemTransactionEvent $systemEvent, $eventName, EventDispatcherInterface $eventDispatcher)
    {
        if ($systemEvent->getArgument('contractMatching') === false) {

            foreach ($systemEvent->getTransactions() as $transaction) {

                $transaction
                    ->setEvent($systemEvent->getInnerEvent())
                    ->setUser($systemEvent->getUser())
                ;
            }
            return;
        }


        /**
         * @var ContainerAwareEventDispatcher $eventDispatcher
         */
        $container = $eventDispatcher->getContainer();

        /**
         * @var Agreement $agreement
         */
        $agreement = $systemEvent->getProcess()->getAgreement();;


        $declutterList = $container->get('doctrine')->getRepository(Declutter::class)->findBy([
            'loanApply' => $agreement->getLoanApply()
        ]);


        $summ = 0;

        /**
         * @var Declutter $declutter
         */
        foreach ($declutterList as $declutter) {
            $summ += $declutter->getUserCreditline()->getBalanceInput();
        }

        foreach ($systemEvent->getTransactions() as $transaction) {

             $transaction
                ->setEvent($systemEvent->getInnerEvent())
                ->setUser($systemEvent->getUser())
                ->setTransactionAmount($summ)
                ->setTransactionOwner(Transaction::TRANSACTION_OWNER_BORROWER)
                ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_PRINCIPLE)
                ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_AVAILABLE)
                ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_BORROWER)
            ;

            $investmentType = $transaction->getInvestmentType();
            $offerId = $transaction->getOfferId();
        }

        $summ = 0;

        /**
         * @var Declutter $declutter
         */
        foreach ($declutterList as $declutter) {
            $summ += $declutter->getUserCreditline()->getBalanceInput();

            $systemEvent
                ->addTransaction(
                    Transaction::create()
                        ->setEvent($systemEvent->getInnerEvent())
                        ->setUser($systemEvent->getUser())
                        ->setTransactionAmount(-1 * $declutter->getUserCreditline()->getBalanceInput())
                        ->setTransactionOwner(Transaction::TRANSACTION_OWNER_BORROWER)
                        ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_CASH_AVAILABLE)
                        ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_SETTLING)
                        ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_BORROWER)
                        ->setOfferId($offerId)
                        ->setInvestmentType($investmentType)
                )
            ;
        }
    }

    protected function transactionLiquidate(Transaction $transaction)
    {
        $systemTransaction = Transaction::create()
            ->setTransactionOwner($transaction->getTransactionOwner())
            ->setTransactionAccount($transaction->getTransactionAccount())
            ->setTransactionAmount(-1 * $transaction->getTransactionAmount())
            ->setParentTransaction($transaction->getParentTransaction() ?: $transaction)
        ;

        return $systemTransaction;
    }
}