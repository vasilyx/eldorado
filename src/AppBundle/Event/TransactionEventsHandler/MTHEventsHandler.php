<?php
namespace AppBundle\Event\TransactionEventsHandler;

use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\InvestOfferProcess;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\WorldpayHtmlTransaction;
use AppBundle\Event\CallCreditEvent;
use AppBundle\Event\CallCreditEvents;
use AppBundle\Event\SystemTransactionEvent;
use AppBundle\Manager\QuoteApplyManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class MTHEventsHandler.
 * We may save entities without flush because it is used in subscriber.
 * @package AppBundle\Event\TransactionEventsHandler
 */
class MTHEventsHandler
{
    use ContainerAwareTrait;

    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function onMTHC(SystemTransactionEvent $systemEvent, $eventName, EventDispatcherInterface $eventDispatcher)
    {
        /**
         * @var Transaction $transaction
         */
        foreach ($systemEvent->getTransactions() as $transaction) {

            $transaction
                ->setEvent($systemEvent->getInnerEvent())
            ;
        }
    }

    public function onMTHA(SystemTransactionEvent $systemEvent, $eventName, EventDispatcherInterface $eventDispatcher)
    {
        /**
         * @var Transaction $transaction
         */
        foreach ($systemEvent->getTransactions() as $transaction) {

            $transaction
                ->setEvent($systemEvent->getInnerEvent())
            ;
        }
    }

    public function onMTHD(SystemTransactionEvent $systemEvent, $eventName, EventDispatcherInterface $eventDispatcher)
    {
        /**
         * @var Transaction $transaction
         */
        foreach ($systemEvent->getTransactions() as $transaction) {

            $transaction
                ->setEvent($systemEvent->getInnerEvent())
            ;
        }
    }
}