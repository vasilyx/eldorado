<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 28.12.16
 * Time: 11:09
 */

namespace AppBundle\Event\TransactionEventsHandler;


use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\InvestOfferProcess;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\WorldpayHtmlTransaction;
use AppBundle\Event\CallCreditEvent;
use AppBundle\Event\CallCreditEvents;
use AppBundle\Event\SystemTransactionEvent;
use AppBundle\Event\TransactionEvents;
use AppBundle\Manager\QuoteApplyManager;
use AppBundle\Process\Handlers\InvestOfferProcessHandler;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class DEPEventsHandler.
 * We may save entities without flush because it is used in subscriber.
 * @package AppBundle\Event\TransactionEventsHandler
 */
class DEPEventsHandler
{
    public function onDEPC(SystemTransactionEvent $systemEvent, $eventName, EventDispatcherInterface $eventDispatcher)
    {
        /**
         * @var ContainerAwareEventDispatcher $eventDispatcher
         */
        $container = $eventDispatcher->getContainer();

        $investOffer = null;

        if ($systemEvent->getProcess() instanceof InvestOfferProcess) {
            /**
             * @var InvestOffer $investOffer
             */
            $investOffer = $systemEvent->getProcess()->getInvestOffer();
        }

        /**
         * @var InvestOffer $investOffer
         */

        /**
         * @var Transaction $transaction
         */
        foreach ($systemEvent->getTransactions() as $transaction) {

            $transaction
                ->setUser($systemEvent->getUser())
                ->setEvent($systemEvent->getInnerEvent())
                ->setTransactionAmount($systemEvent->getAmount())
                ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_CASH_PENDING)
                ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_PENDING)
                ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR)
            ;

            if($investOffer) {
                $transaction->setInvestmentType($investOffer->getInvestmentType());
            }

            switch ($transaction->getTransactionOwner()) {
                /**
                 * For system transaction which is coupled with Worldpay we should create worldpay transaction
                 */
                case Transaction::TRANSACTION_OWNER_WORLDPAY :
                    $worldpayTransaction = $container->get('whs.manager.world_pay_transaction_manager')->createWorldpayHtmlTransaction(
                        $systemEvent->getUser(),
                        $systemEvent->getAmount(),
                        $transaction->getTransactionId(),
                        $transaction->getTransactionCcy()
                    );

                    $systemEvent->setArgument('worldpay_transaction', $worldpayTransaction);
                    $transaction->setTransactionAmount(-1 * $systemEvent->getAmount());
                    $transaction->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_3DPARTY_PAYMENTS);
                    $transaction->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_WORLDPAY);
                    break;
                case Transaction::TRANSACTION_OWNER_INVESTOR:
                    /**
                     * For system transaction which is coupled with Investor we should change the quote status to pending_funds. The quote will have this status until Worldpay sends callback.
                     */
                    $quoteApplyManager = $container->get('whs.manager.quote_apply_manager');
                    if ($quoteApply = $quoteApplyManager->getFundingRequiredLenderQuoteApply($systemEvent->getUser())) {
                        $quoteApply->setStatus(QuoteApply::STATUS_PENDING_FUNDS);
                        $quoteApplyManager->save($quoteApply);
                    }
            }
        }
    }

    /**
     * This
     *
     * @param SystemTransactionEvent $systemEvent
     * @param $eventName
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function onDEPA(SystemTransactionEvent $systemEvent, $eventName, EventDispatcherInterface $eventDispatcher)
    {
        /**
         * @var ContainerAwareEventDispatcher $eventDispatcher
         */
        $container = $eventDispatcher->getContainer();
        $transactionId = $systemEvent->getArgument('transactionId');
        /**
         * @var InvestOffer $investOffer
         */
        $investOffer = $systemEvent->getProcess()->getInvestOffer();
        $transactions = [];

        /**
         * @var Transaction $transactionByOrderCode
         */
        $transactionByOrderCode = $container->get('doctrine')->getRepository(Transaction::class)->findOneBy([
            'transactionId' => $transactionId
        ]);

        $transactions[] = $transactionByOrderCode;

        $transactions[] = $transactionByOrderCode->getParentTransaction();

        foreach ($transactions as $c => $transaction) {
            $systemTransaction = Transaction::create()
                ->setTransactionOwner($transaction->getTransactionOwner())
                ->setTransactionAccount($transaction->getTransactionAccount())
                ->setTransactionAmount($transaction->getTransactionAmount())
                ->setParentTransaction($transaction->getParentTransaction() ?: $transaction)
            ;

            if ($c>0) {
                $systemTransaction->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR);
                $systemTransaction->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_PENDING);
                $systemTransaction->setTransactionAmount($transaction->getTransactionAmount());
            } else {
                $systemTransaction->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_3DPARTY_PAYMENTS);
                $systemTransaction->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_WORLDPAY);
                $systemTransaction->setTransactionAmount($transaction->getTransactionAmount());
            }

            $systemEvent->addTransaction($systemTransaction);
        }


        $systemEvent
            ->addTransaction(
                Transaction::create()
                ->setTransactionOwner($transactionByOrderCode->getParentTransaction()->getTransactionOwner())
                ->setTransactionAmount(-1 * $transactionByOrderCode->getParentTransaction()->getTransactionAmount())
                ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_CASH_AVAILABLE)
                ->setParentTransaction($transactionByOrderCode->getParentTransaction())
                ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR)
                ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_AVAILABLE)
            )
            ->addTransaction(
                Transaction::create()
                ->setTransactionOwner(Transaction::TRANSACTION_OWNER_TRUST_BANK_ACCOUNT_LENDERS)
                ->setTransactionAmount($transactionByOrderCode->getParentTransaction()->getTransactionAmount())
                ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_CASH_AVAILABLE)
                ->setParentTransaction($transactionByOrderCode->getParentTransaction())
                ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_BANK_ACCOUNT_INVESTOR)
            )
        ;

        foreach ($systemEvent->getTransactions() as $transaction) {
            $transaction
                ->setEvent($systemEvent->getInnerEvent())
                ->setUser($systemEvent->getUser())
                ->setOfferId($investOffer->getId())
                ->setInvestmentType($investOffer->getInvestmentType())
            ;
        }

        /**
         * Lender has single draft quote. Only first payment can change lender quote status. Only first payment can send Callvalidate request.
         */
        $quoteApplyManager = $container->get('whs.manager.quote_apply_manager');
        if ($quoteApply = $quoteApplyManager->getPendingFundsLenderQuoteApply($systemEvent->getUser())) {
            $quoteApply->setStatus(QuoteApply::STATUS_PENDING);
            $quoteApplyManager->save($quoteApply);
            /**
             * @var CallCreditEvent $event
             */
            $event = $container->get('event_dispatcher')->dispatch(CallCreditEvents::CALL_VALIDATE_REQUEST,
                CallCreditEvent::create()
                    ->setCallCreditApiCalls(
                        CallCreditAPICalls::create()
                            ->setQuoteApply($quoteApply)
                            ->setPurpose(CallCreditAPICalls::PURPOSE_QS)
                    )
            );
            /**
             * Logging
             */
            $container->get('monolog.logger.callcredit')->debug(sprintf('Event %1$s is generated.', CallCreditEvents::CALL_VALIDATE_REQUEST), [$container->getParameter('kernel.environment')]);
            /**
             * End logging
             */

            $callCredit = $event->getCallCreditApiCalls();

            $container->get('doctrine')->getManager()->persist($callCredit);
        }
    }

    public function onDEPX(SystemTransactionEvent $systemEvent, $eventName, EventDispatcherInterface $eventDispatcher)
    {
        /**
         * @var ContainerAwareEventDispatcher $eventDispatcher
         */
        $container = $eventDispatcher->getContainer();
        $transactionId = $systemEvent->getArgument('transactionId');

        $transactions = [];

        /**
         * @var Transaction $transactionByOrderCode
         */
        $transactionByOrderCode = $container->get('doctrine')->getRepository(Transaction::class)->findOneBy([
            'transactionId' => $transactionId
        ]);

        $transactions[] = $transactionByOrderCode;

        $transactions[] = $transactionByOrderCode->getParentTransaction();

        foreach ($transactions as $transaction) {
            $systemTransaction = $this->transactionLiquidate($transaction)
                ->setEvent($systemEvent->getInnerEvent())
                ->setUser($systemEvent->getUser())
            ;
            $systemEvent->addTransaction($systemTransaction);
        }
        /**
         * If the user cancels a payment, we should return status to previous: funding_required.
         */
        $quoteApplyManager = $container->get('whs.manager.quote_apply_manager');
        if ($quoteApply = $quoteApplyManager->getPendingFundsLenderQuoteApply($systemEvent->getUser())) {
            $quoteApply->setStatus(QuoteApply::STATUS_FUNDING_REQUIRED);
            $quoteApplyManager->save($quoteApply);
        }

        if ($systemEvent->getProcess() instanceof InvestOfferProcess) {
            /**
             * @var InvestOffer $investOffer
             */
            $investOffer = $systemEvent->getProcess()->getInvestOffer();
            $investOffer->setStatus(InvestOffer::STATUS_DRAFT);
            $quoteApplyManager->save($investOffer);
        }
    }

    protected function transactionLiquidate(Transaction $transaction)
    {
        $systemTransaction = Transaction::create()
            ->setTransactionOwner($transaction->getTransactionOwner())
            ->setTransactionAccount($transaction->getTransactionAccount())
            ->setTransactionAmount(-1 * $transaction->getTransactionAmount())
            ->setParentTransaction($transaction->getParentTransaction() ?: $transaction)
        ;

        return $systemTransaction;
    }
}