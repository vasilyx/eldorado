<?php
namespace AppBundle\Event\TransactionEventsHandler;

use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\WorldpayHtmlTransaction;
use AppBundle\Event\CallCreditEvent;
use AppBundle\Event\CallCreditEvents;
use AppBundle\Event\SystemTransactionEvent;
use AppBundle\Event\TransactionEvents;
use AppBundle\Manager\QuoteApplyManager;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class AGREventsHandler.
 * We may save entities without flush because it is used in subscriber.
 * @package AppBundle\Event\TransactionEventsHandler
 */
class AGREventsHandler
{
    public function onAGRF(SystemTransactionEvent $systemEvent, $eventName, EventDispatcherInterface $eventDispatcher)
    {
        $agreementUser = $systemEvent->getArgument('agreementUser');
        $offerUser = $systemEvent->getArgument('offerUser');

        /**
         * @var Transaction $transaction
         */
        foreach ($systemEvent->getTransactions() as $transaction) {

            $transaction
                ->setUser($offerUser)
                ->setEvent($systemEvent->getInnerEvent())
                ->setTransactionAmount($transaction->getTransactionAmount())
                ->setTransactionOwner(Transaction::TRANSACTION_OWNER_INVESTOR)
                ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_OFFER_ALLOCATED)
                ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_ALLOC)
                ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR)
            ;

            $transactionAmount = $transaction->getTransactionAmount();
            $investmentType = $transaction->getInvestmentType();
            $offerId = $transaction->getOfferId();
        }

        $systemEvent
            ->addTransaction(
                Transaction::create()
                    ->setUser($offerUser)
                    ->setEvent($systemEvent->getInnerEvent())
                    ->setTransactionAmount(-1 * $transactionAmount)
                    ->setTransactionOwner(Transaction::TRANSACTION_OWNER_INVESTOR)
                    ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_OFFER_ALLOCATED)
                    ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_PRINCIPLE)
                    ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_LOAN_ACCOUNT_INVESTOR)
            )
            ->addTransaction(
                Transaction::create()
                    ->setUser($agreementUser)
                    ->setEvent($systemEvent->getInnerEvent())
                    ->setTransactionAmount($transactionAmount)
                    ->setTransactionOwner(Transaction::TRANSACTION_OWNER_BORROWER)
                    ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_PRINCIPLE)
                    ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_PRINCIPLE)
                    ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_LOAN_ACCOUNT_BORROWER)
            )
            ->addTransaction(
                Transaction::create()
                    ->setUser($agreementUser)
                    ->setEvent($systemEvent->getInnerEvent())
                    ->setTransactionAmount(-1 * $transactionAmount)
                    ->setTransactionOwner(Transaction::TRANSACTION_OWNER_BORROWER)
                    ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_BORROWER_ADVANCE)
                    ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_AVAILABLE)
                    ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_BORROWER)
            )
            ->addTransaction(
                Transaction::create()
                    ->setUser($agreementUser)
                    ->setEvent($systemEvent->getInnerEvent())
                    ->setTransactionAmount($transactionAmount)
                    ->setTransactionOwner(Transaction::TRANSACTION_OWNER_TRUST_BANK_ACCOUNT_BORROWERS)
                    ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_CASH_AVAILABLE)
            //        ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_AVAILABLE)
                    ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_BANK_ACCOUNT_BORROWER)
            )
            ->addTransaction(
                Transaction::create()
                    ->setUser($offerUser)
                    ->setEvent($systemEvent->getInnerEvent())
                    ->setTransactionAmount(-1 * $transactionAmount)
                    ->setTransactionOwner(Transaction::TRANSACTION_OWNER_TRUST_BANK_ACCOUNT_LENDERS)
                    ->setTransactionAccount(Transaction::TRANSACTION_ACCOUNT_CASH_AVAILABLE)
       //             ->setTransactionSubAccount(Transaction::TRANSACTION_SUB_ACCOUNT_CASH_AVAILABLE)
                    ->setGlAccount(Transaction::TRANSACTION_GLACCOUNT_BANK_ACCOUNT_INVESTOR)
            )
        ;

        foreach ($systemEvent->getTransactions() as $transaction) {
            $transaction
                ->setOfferId($offerId)
                ->setInvestmentType($investmentType)
            ;
        }
    }
}