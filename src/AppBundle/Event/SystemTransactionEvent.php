<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 27.12.16
 * Time: 17:10
 */

namespace AppBundle\Event;

use AppBundle\Entity\BaseProcess;
use AppBundle\Entity\Event as InnerEvent;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Traits\StaticCreatable;
use Symfony\Component\EventDispatcher\GenericEvent;

class SystemTransactionEvent extends GenericEvent
{
    use StaticCreatable;
    /**
     * @var string
     */
    protected $type;
    /**
     * @var Transaction[]
     */
    protected $transactions;

    /**
     * This entity is saved to the database (something like log)
     *
     * @var InnerEvent
     */
    protected $innerEvent;

    /**
     * @var
     */
    protected $process;

    /**
     * @var string
     */
    protected $ccy;

    /**
     * @var string
     */
    protected $amount;

    /**
     * @var User $user
     */
    protected $user;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return Transaction[]
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * @param Transaction[] $transactions
     * @return $this
     */
    public function setTransactions(array $transactions)
    {
        $this->transactions = $transactions;
        return $this;
    }


    /**
     * @param Transaction $transaction
     * @return $this
     */
    public function addTransaction(Transaction $transaction)
    {
        $this->transactions[] = $transaction;

        return $this;
    }

    /**
     * @return InnerEvent
     */
    public function getInnerEvent()
    {
        return $this->innerEvent;
    }

    /**
     * @param InnerEvent $event
     * @return $this
     */
    public function setInnerEvent($event)
    {
        $this->innerEvent = $event;
        return $this;
    }

    /**
     * @return string
     */
    public function getCcy()
    {
        return $this->ccy;
    }

    /**
     * @param string $ccy
     * @return SystemTransactionEvent
     */
    public function setCcy($ccy)
    {
        $this->ccy = $ccy;
        return $this;
    }

    /**
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     * @return SystemTransactionEvent
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return SystemTransactionEvent
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return BaseProcess
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param BaseProcess $process
     * @return SystemTransactionEvent
     */
    public function setProcess(BaseProcess $process)
    {
        $this->process = $process;
        return $this;
    }
}