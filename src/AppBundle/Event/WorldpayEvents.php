<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 05.01.17
 * Time: 9:48
 */

namespace AppBundle\Event;


class WorldpayEvents
{
    /**
     * When the callback action receives a response from the WorldPay, the 'worldpay.callback.received' event is generated.
     */
    const CALLBACK_RECEIVED = 'worldpay.callback.received';
}