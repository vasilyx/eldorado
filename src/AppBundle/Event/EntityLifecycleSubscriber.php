<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 09.01.17
 * Time: 11:48
 */

namespace AppBundle\Event;


use AppBundle\Entity\LoanApply;
use AppBundle\Entity\Product;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\UserProduct;
use AppBundle\Security\Voter\CreateVoter;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class EntityLifecycleSubscriber implements EventSubscriberInterface
{
    /**
     * @var QuoteApply
     */
    private $quoteApply;

    /**
     * This entities will be persisted in onFlush listener with UnitOfWork api
     * @var array
     */
    private $entitiesToPersist = [];

    public static function getSubscribedEvents()
    {
        return [
          EntityLifecycleEvents::QUOTE_APPLY_STATUS_UPDATED => 'onQuoteApplyStatusUpdated'
        ];
    }

    public function onQuoteApplyStatusUpdated(GenericEvent $event, $eventName, EventDispatcherInterface $eventDispatcher)
    {
        /**
         * @var ContainerAwareEventDispatcher $eventDispatcher
         */
        $container = $eventDispatcher->getContainer();

        /**
         * @var QuoteApply $quoteApply
         */
        $quoteApply = $event->getSubject();

        /**
         * We can use flush in nested functions and loop process. To avoid this we check for quote_apply is analysed first time
         */
        if ($this->quoteApply === $quoteApply)
        {
            return;
        } else {
            $this->quoteApply = $quoteApply;
        }
        /**
         * Logging
         */
        $container->get('monolog.logger.callcredit')->debug(sprintf('QuoteApply with id=%4$s and type %1$s has changed status from %2$s to %3$s.', $quoteApply->getProduct()->getName(), QuoteApply::getStatusesMap()[$event->getArgument('oldStatus')], QuoteApply::getStatusesMap()[$event->getArgument('newStatus')], $quoteApply->getId()), [$container->getParameter('kernel.environment')]);

        $container->get('monolog.logger.quote_apply_status')->info('', [$quoteApply]);
        /**
         * End logging
         */


            /**
             * If the QuoteApply status is changed from pending to approved,
             */
            if (QuoteApply::STATUS_PENDING == $event->getArgument('oldStatus') && QuoteApply::STATUS_APPROVED == $event->getArgument('newStatus')) {

                try {
                    $userProduct = $container->get('whs.manager.user_product_manager')->createUserProduct($quoteApply->getUser(), $quoteApply->getProduct());

                    $this->entitiesToPersist[] = $userProduct;
                } catch (AccessDeniedException $e) {
                    printf($e->getMessage());
                }


                /**
                 * For the Borrower Quote only
                 */
                if (Product::TYPE_BORROWER == $quoteApply->getProduct()->getType()) {
                /**
                 * New loan request creating
                 */
                $loanApply = LoanApply::create()
                    ->setStatus(LoanApply::STATUS_DRAFT)
                    ->setQuoteApply($quoteApply)
                    /**
                     * The QuoteApply status may be changed from action which does not need authorisation
                     */
                    ->setUser($quoteApply->getUser())
                ;
                /**
                 * Logging
                 */
                $container->get('monolog.logger.callcredit')->debug(sprintf('LoanApply for QuoteApply %1$s is added to persist.', $quoteApply->getId()), [$container->getParameter('kernel.environment')]);
                /**
                 * End logging
                 */


                $this->entitiesToPersist[] = $loanApply;
            }
         }
        $event->setArgument('entitiesToPersist', $this->entitiesToPersist);
    }
}