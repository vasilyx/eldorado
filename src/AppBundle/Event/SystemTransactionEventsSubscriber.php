<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 27.12.16
 * Time: 16:50
 */

namespace AppBundle\Event;


use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SystemTransactionEventsSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var array
     */
    protected $handlers;

    /**
     * SystemEventSubscriber constructor.
     */
    public function __construct(EntityManager $entityManager, TokenStorageInterface $tokenStorage)
    {
        $this->em = $entityManager;
        $this->tokenStorage = $tokenStorage;
    }


    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        $result = [];
        foreach (TransactionEvents::getSystemEventsCodes() as $systemEventsCode) {
            $result[$systemEventsCode] = 'on' . $systemEventsCode;
        }
        return $result;
    }

    /**
     * @param $name
     * @param $arguments
     */
    public function __call($name, $arguments)
    {
        $event = $arguments[0];
        $eventName = $arguments[1];
        $dispatcher = $arguments[2];

        $this->preExecuteHandlers($event, $eventName, $dispatcher);

        foreach ($this->handlers as $handler) {
            if (method_exists($handler, $name)) {
                $handler->$name($event, $eventName, $dispatcher);
            }
        }

        $this->postExecuteHandlers($event, $eventName, $dispatcher);

        $this->em->flush();
    }

    /**
     * @param mixed $handler
     */
    public function addHandler($handler)
    {
        $this->handlers[] = $handler;
    }

    /**
     * @param SystemTransactionEvent $systemEvent
     * @param $eventName
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function preExecuteHandlers(SystemTransactionEvent $systemEvent, $eventName, EventDispatcherInterface $eventDispatcher)
    {
        $innerEvent = \AppBundle\Entity\Event::create()
            ->setActive(true)
            ->setType($eventName)
            ->setUser($systemEvent->getUser())
        ;

        if ($process = $systemEvent->getProcess()) {
            $innerEvent->setProcess($process);
        }

        $systemEvent
            ->setInnerEvent($innerEvent);

        $this->em->persist($innerEvent);

        /**
         * Event must be saved in any case
         */
        $this->em->flush();
    }

    /**
     * @param SystemTransactionEvent $systemEvent
     * @param $eventName
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function postExecuteHandlers(SystemTransactionEvent $systemEvent, $eventName, EventDispatcherInterface $eventDispatcher)
    {
        if ($systemEvent->getTransactions()) {
            foreach ($systemEvent->getTransactions() as $transaction) {
                $this->em->persist($transaction);
            }
        }
    }
}