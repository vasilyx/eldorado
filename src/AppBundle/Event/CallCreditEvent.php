<?php

/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 23.12.2016
 * Time: 3:36
 */
namespace AppBundle\Event;

use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Traits\StaticCreatable;
use Symfony\Component\EventDispatcher\Event;

class CallCreditEvent extends Event
{
    use StaticCreatable;
    /** @var CallCreditAPICalls */
    protected $callCreditApiCalls;

    /**
     * @return CallCreditAPICalls
     */
    public function getCallCreditApiCalls()
    {
        return $this->callCreditApiCalls;
    }

    /**
     * @param CallCreditAPICalls $callCreditAPICalls
     *
     * @return CallCreditEvent
     */
    public function setCallCreditApiCalls(CallCreditAPICalls $callCreditAPICalls)
    {
        $this->callCreditApiCalls = $callCreditAPICalls;

        return $this;
    }
}