<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 05.01.17
 * Time: 9:58
 */

namespace AppBundle\Event;




use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\WorldpayHtmlTransaction;
use AppBundle\Entity\WorldpayTransaction;
use LogBundle\Entity\UserActivityLog;
use LogBundle\Handler\UserActivityHandler;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class WorldpayEventsSubscriber implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            WorldpayEvents::CALLBACK_RECEIVED => 'onCallbackReceived'
        ];
    }


    /**
     * When the callback action receives a response from the WorldPay, the 'worldpay.callback.received' event is generated.
     * This handler takes parameters from the response, finds a transaction by a transactionId and sets the success status for the transaction. After that the handler generates the DEPA system event to create a set of system transactions.
     *
     * @param SystemTransactionEvent $systemEvent
     * @param $eventName
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function onCallbackReceived(SystemTransactionEvent $systemEvent, $eventName, EventDispatcherInterface $eventDispatcher)
    {
        /**
         * @var ContainerAwareEventDispatcher $eventDispatcher
         */
        $container = $eventDispatcher->getContainer();

        $parameters = $systemEvent->getArgument('parameters');

        $transStatus = $parameters['transStatus'];
        $transactionId = $parameters['cartId'];

        /**
         * @var WorldpayHtmlTransaction $transaction
         */
        $transaction = $container->get('doctrine')->getRepository(WorldpayHtmlTransaction::class)->findOneBy([
            'customerOrderCode' => $transactionId
        ]);

        // flush will be called in event subscriber
        $transaction->setPaymentResponse($parameters);

        $process = $container->get('doctrine')->getRepository(Transaction::class)->findOneBy(['transactionId' => $transactionId])->getEvent()->getProcess();

        $systemEvent = SystemTransactionEvent::create()
            ->setUser($transaction->getUser())
            ->setArgument('transactionId', $transactionId)
            ->setArgument('transStatus', $transStatus)
        ;



        switch ($transStatus) {
            case WorldpayHtmlTransaction::TRANS_STATUS_SUCCESS:

                $transaction
                    ->setPaymentStatus(WorldpayTransaction::PAYMENT_STATUS_SUCCESS)
                ;

                $container->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $transaction->getUser(), UserActivityHandler::CONTEXT_ACTION => UserActivityLog::LENDER_PRODUCT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::WORLDPAY_PAYMENT_DONE_ACTION]);
                $container->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $transaction->getUser(), UserActivityHandler::CONTEXT_ACTION => UserActivityLog::LENDER_PRODUCT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::LENDER_APPLICATION_SUBMITTED]);

                break;
            case WorldpayHtmlTransaction::TRANS_STATUS_CANCEL:

                $transaction
                    ->setPaymentStatus(WorldpayTransaction::PAYMENT_STATUS_CANCELLED)
                ;

                $container->get('monolog.logger.user_activity')->info('', [UserActivityHandler::CONTEXT_USER => $transaction->getUser(), UserActivityHandler::CONTEXT_ACTION => UserActivityLog::LENDER_PRODUCT_TYPE, UserActivityHandler::CONTEXT_CODE => UserActivityLog::WORLDPAY_PAYMENT_FAILED_ACTION]);

                break;

        }

        $processHandler = $container->get('app.process_handler_resolver')->resolve($process);
        $processHandler->nextStep($process, $systemEvent);

    }
}