<?php

namespace AppBundle\Event;


class TransactionEvents
{
    /**
     * Client makes a pledge to deposit and awaiting confirmation from Worldpay, Bank Transfer or ISA Transfer
     */
    const DEPC = 'DEPC';

    /**
     * Confirmation is received from WorldPay or funds confirmed as received by Finance in the bank account
     */
    const DEPA = 'DEPA';

    /**
     * 	Client or admin cancel the deposit pledge before the money is received.
     */
    const DEPX = 'DEPX';


    /**
     * 	OfferFunded
     */
    const OFRF = 'OFRF';

    /**
     * 	OfferAllocated
     */
    const ALOC = 'ALOC';

    /**
     * 	AgreementFunded
     */
    const AGRF = 'AGRF';

    /**
     * 	CreditorSettled
     */
    const CRDS = 'CRDS';

    /**
     * 	WithdrawalCreated
     */
    const WDRC = 'WDRC';

    /**
     * 	WithdrawalApproved
     */
    const WDRA = 'WDRA';

    /**
     * 	InstalmentCreated
     */
    const MTHC = 'MTHC';

    /**
     * 	InstalmentPaid
     */
    const MTHA = 'MTHA';

    /**
     * 	InstalmentDistribution
     */
    const MTHD = 'MTHD';

    public static function getSystemEventsMap()
    {
        return [
            self::DEPC      => 'DEPC',
            self::DEPA      => 'DEPA',
            self::DEPX      => 'DEPX',
            self::OFRF      => 'OFRF',
            self::ALOC      => 'ALOC',
            self::AGRF      => 'AGRF',
            self::CRDS      => 'CRDS',
            self::WDRC      => 'WDRC',
            self::WDRA      => 'WDRA',
            self::MTHC      => 'MTHC',
            self::MTHA      => 'MTHA',
            self::MTHD      => 'MTHD',
        ];
    }

    public static function getSystemEventsCodes()
    {
        return array_keys(self::getSystemEventsMap());
    }
}