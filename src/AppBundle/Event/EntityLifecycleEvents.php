<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 09.01.17
 * Time: 11:32
 */

namespace AppBundle\Event;


/**
 * Class EntityLifecycleEvents
 * This class describes 'created', 'updated', 'deleted' events for entities. The structure of the event name: 'entity_name.lifecycle_event.field_name'.
 * If a field_name does not exist: entity_name.lifecycle_event
 * @package AppBundle\Event
 */
class EntityLifecycleEvents
{
    const QUOTE_APPLY_STATUS_UPDATED = 'quoteApply.updated.status';
    const LOAN_APPLY_STATUS_UPDATED = 'quoteApply.updated.status';
}