<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 06.01.17
 * Time: 15:26
 */

namespace AppBundle\Event;


class CallCreditEvents
{
    const CALL_VALIDATE_REQUEST = 'api.callvalidate.request';
    const CALL_REPORT_REQUEST = 'api.callreport.request';
    const AFFORDABILITY_REQUEST = 'api.affordability.request';
    const CALL_CREDIT_SUCCESS = 'api.callcredit.success';
}