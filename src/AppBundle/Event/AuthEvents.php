<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 30.12.16
 * Time: 10:49
 */

namespace AppBundle\Event;


class AuthEvents
{
    const EMAIL_IS_CONFIRMED = 'email.is_confirmed';

    public static function getAuthEvents()
    {
        return [
          self::EMAIL_IS_CONFIRMED
        ];
    }
}