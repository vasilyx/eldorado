<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 24.02.17
 * Time: 15:23
 */

namespace AppBundle\DependencyInjection\CompilerPass;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ProcessHandlersPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('app.process_handler_resolver')) {
            return;
        }

        $definition = $container->findDefinition('app.process_handler_resolver');

        $taggedServices = $container->findTaggedServiceIds('app.process_handler');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addProcessHandler', array(new Reference($id)));
        }
    }
}