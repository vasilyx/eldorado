<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 28.12.16
 * Time: 11:31
 */

namespace AppBundle\DependencyInjection\CompilerPass;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class SubscriberHandlersPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('app.system_event_subcriber')) {
            return;
        }

        $definition = $container->findDefinition('app.system_event_subcriber');

        $taggedServices = $container->findTaggedServiceIds('app.subscriber_handler');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addHandler', array(new Reference($id)));
        }
    }
}