<?php

namespace AppBundle\DependencyInjection;

use AppBundle\Event\AuthEvents;
use AppBundle\Listener\AddToQueueRequestListener;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class AppExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $servicesLoader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config/services'));
        $servicesLoader->load('managers.yml');
        $servicesLoader->load('voter.yml');
        $servicesLoader->load('process.yml');

        $container->register('app.add_to_queue.listener', AddToQueueRequestListener::class)
            ->addTag('kernel.event_listener', array('event' => AuthEvents::EMAIL_IS_CONFIRMED, 'method' => 'addToQueue' ))
            ->addArgument(new Reference('app.queue_manager'))
            ->addArgument(new Reference('doctrine.orm.entity_manager'));

        $container->setParameter('server_mode', $configs[0]['server_mode']);
    }
}
