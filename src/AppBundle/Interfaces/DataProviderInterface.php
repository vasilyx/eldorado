<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 23.12.2016
 * Time: 1:29
 */

namespace AppBundle\Interfaces;

use AppBundle\Entity\User;

interface DataProviderInterface
{
    public function getDataForXmlByUser(User $user, $purpose = '');
}