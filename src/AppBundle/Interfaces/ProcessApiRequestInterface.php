<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 23.12.2016
 * Time: 1:32
 */

namespace AppBundle\Interfaces;

use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\User;

interface ProcessApiRequestInterface
{
    public function processApiRequest(CallCreditAPICalls $callCreditAPICalls);
    public function sendApiRequest(CallCreditAPICalls $callCreditAPICalls);
    public function changePassword();
    public function logSendRequest(User $user);
}