<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 13.12.16
 * Time: 10:49
 */

namespace AppBundle\Interfaces\ManagerType;


/**
 * Interface AlwaysActiveInterface.
 * This interface is implemented by managers that control an "always active" records. The record is created once for user and is updated without changing id (for example UserDetails, UserPreferences)
 * @package AppBundle\Interfaces\ManagerType
 */

interface AlwaysActiveInterface
{

}