<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 21.12.16
 * Time: 10:13
 */

namespace AppBundle\Interfaces\ManagerType;


/**
 * Interface ChangableActiveInterface
 * There are many records and many groups of records relating to user, many records but only one record from each group can be active (for example UserCommunications, BankDetails). For updating record with value1 (no id!) to value2, look for unactive record with value2 and if it has been find - change active property to true. If there is no record with value2 in table - create it.
 * @package AppBundle\Interfaces\ManagerType
 */
interface ChangableActiveInterface extends ManyActiveInterface
{
    public function makePrimary(ActivableInterface $activable);
}