<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 21.12.16
 * Time: 10:43
 */

namespace AppBundle\Interfaces\ManagerType;


interface ActivableInterface
{
    public function setActive($boolean);
    public function getActive();
}