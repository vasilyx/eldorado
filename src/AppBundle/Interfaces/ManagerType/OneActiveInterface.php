<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 21.12.16
 * Time: 10:12
 */

namespace AppBundle\Interfaces\ManagerType;

/**
 * Interface OneActiveInterface.
 *  This interface is implemented by managers that control an "only one active" records.
 *  there are many records for user but only one of them can be active (for example QuoteIncome).
 *  There is no update action for such records - every record is new and active is the last created
 * @package AppBundle\Interfaces\ManagerType
 */
interface OneActiveInterface
{

}