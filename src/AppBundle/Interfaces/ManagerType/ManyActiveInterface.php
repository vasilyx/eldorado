<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 21.12.16
 * Time: 10:12
 */

namespace AppBundle\Interfaces\ManagerType;


/**
 * Interface ManyActiveInterface
 * There are many records for user and many of them can be active (for example UserAddress). When record with id1 is updated its active property will be changed to false (it will never be true again) and new record with updated parameters will be created. The created record will have new id.
 * @package AppBundle\Interfaces\ManagerType
 */
interface ManyActiveInterface
{
    public function updateInTransaction(callable $function);
}