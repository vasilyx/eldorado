<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 24.01.17
 * Time: 11:34
 */

namespace AppBundle\Interfaces;


/**
 * This interface should be implemented by entities which should not be deleted even by soft deletable way. We need have an actions history for the archivable entities.
 * Interface ArchivableInterface
 * @package AppBundle\Interfaces
 */
interface ArchivableInterface
{
    /**
     * @param $archive boolean
     * @return mixed
     */
    public function setArchive($archive);

    /**
     * @return mixed
     */
    public function getArchive();
}