<?php
/**
 * Created by PhpStorm.
 * User: mikhailpegasin
 * Date: 18.11.16
 * Time: 15:24
 */

namespace AppBundle\Interfaces;


interface SoftDeletableInterface
{
    public function getDeletedAt();
}