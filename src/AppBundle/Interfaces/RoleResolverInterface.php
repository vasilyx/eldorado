<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 2.12.16
 * Time: 11.35
 */

namespace AppBundle\Interfaces;


use AppBundle\Entity\User;

interface RoleResolverInterface
{
    public function resolveRoles(User $user);
}