<?php

namespace AppBundle\Interfaces;
/**
 * User: Vladimir
 * Date: 1/25/17
 * Time: 4:51 PM
 */

interface SetArchiveFalseInterface
{
    /**
     * @return mixed
     */
    public function setArchiveFalse();
}