<?php
namespace AppBundle\Tests;
use AppBundle\Entity\UserSession;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\Entity\User;

class AppTest extends WebTestCase
{
    /** @var Client */
    protected static $client;
    protected static $server;
    protected static $content;
    protected static $token;
    protected static $fake_email;
    protected static $fake_psw;
    protected static $fake_firstname;
    protected static $fake_lastname;
    protected static $birthdate;
    /** @var  EntityManager */
    protected static $em;
    /**
     * @var UserSession
     */
    protected static $session;

    protected $api_url = 'api';

    public $url_without_token = array('/auth/login', '/auth/forgotten', '/user/registration/borrower');

    /**
     * @return void
     */
    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        static::$client = static::createClient(array());
        static::$server = array();
        static::$content = '{"param": "value"}';
        static::$fake_firstname = 'firstname';
        static::$fake_lastname = 'lastname';
        static::$fake_email = random_int(1, 10000) . time() . '@test.com';
        static::$fake_psw = '1234567Q';

//        static::$em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
//        static::$em->getConnection()->beginTransaction();
    }

    /**
     * @param $url
     * @param array $params
     * @param string $method
     * @param string $custom_token
     * @return mixed
     */
    protected function send($url, $params = array(), $method = 'GET', $custom_token = '')
    {
        $server = static::$server;

        if (!in_array($url, $this->url_without_token)) {
            if ($custom_token) {
                $server = array_merge($server, array('HTTP_Auth' => 'whs' . $custom_token));
            } else {
                $server = $this->addToken($server);
            }
        }

        $server['CONTENT_TYPE'] = 'application/json';
        return static::$client->request($method, '/'. $this->api_url . $url, $params, array(), $server, static::$content);
    }

    protected function addToken($array = array())
    {
        return array_merge($array, array('HTTP_Auth' => 'whs' . static::$token));
    }

    protected static function signUp($productType = 'borrower')
    {
        $params = array(
            'firstname' => static::$fake_firstname,
            'lastname' => static::$fake_lastname,
            'username' => static::$fake_email,
            'password' => static::$fake_psw,
            'plain_password' => static::$fake_psw,
            'birthdate' => '19900101',
            'confirm_url' => '/frontend_confirm_url'
        );

        static::$content = json_encode($params);
        $server = static::$server;
        $server['CONTENT_TYPE'] = 'application/json';
        static::$client->request('POST', '/api/user/registration/' . $productType, array(), array(), $server, static::$content);
        static::$token = json_decode(static::$client->getResponse()->getContent())->content->token;
        static::$session = static::$em->getRepository(UserSession::class)->findOneBy(['token' => static::$token]);

    }

    protected static function verifiedAndLogin()
    {
        $user = static::$em->getRepository(User::class)->findOneByUsername(static::$fake_email);
        $confirmationEmailToken = $user->getConfirmEmailToken();

        $params = array(
            'e' => static::$fake_email,
            't' => $confirmationEmailToken
        );
        static::$content = json_encode($params);
        $server = static::$server;
        $server['CONTENT_TYPE'] = 'application/json';
        static::$client->request('POST', '/api/auth/email_confirmation', array(), array(), $server, static::$content);

        $params = array(
            'username' => static::$fake_email,
            'password' => static::$fake_psw
        );
        static::$content = json_encode($params);
        static::$client->request('POST', '/api/auth/login', array(), array(), $server, static::$content);

        static::$token = json_decode(static::$client->getResponse()->getContent())->token;
    }

    /**
     * @return void
     */
    public static function tearDownAfterClass()
    {
//        static::$em->getConnection()->rollBack();
//        static::$em->close();
    }

    public function testTemp()
    {
        
    }

    public function setUp()
    {
        parent::setUp();
        static::$em = static::createClient()->getContainer()->get('doctrine')->getManager();
//        static::$em->getConnection()->beginTransaction();
    }

    public function tearDown()
    {
        parent::tearDown();
//        static::$em->getConnection()->rollBack();
        static::$em->close();
    }

    /**
     * Returns response content as array
     *
     * @return array
     */
    protected function getContentAsArray()
    {
        return json_decode(static::$client->getResponse()->getContent(), true);
    }

    /**
     * Returns response content as object
     *
     * @return object
     */
    protected function getContentAsObject()
    {
        return json_decode(static::$client->getResponse()->getContent());
    }
}
