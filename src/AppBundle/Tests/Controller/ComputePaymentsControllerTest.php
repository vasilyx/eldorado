<?php
namespace AppBundle\Tests\Controller;

use AppBundle\Tests\AppTest;
use Symfony\Component\HttpFoundation\Response;


/*
 * simple tests of api method
 */

class ComputePaymentsControllerTest extends AppTest
{
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        static::signUp();
    }

    /*
     * test that GET request will be successful with correct params
     */
    public function testGetBaseLoanProperties()
    {
        $params = ['loan_term' => '18', 'loan_amount' => '5000'];
        $this->send('/compute/base_loan_properties', $params);
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
    }

    /*
     * test that GET request will fail without params
     */
    public function testFailGetBaseLoanProperties()
    {
        $this->send('/compute/base_loan_properties');
        $this->assertNotTrue(static::$client->getResponse()->isSuccessful());
    }
}

