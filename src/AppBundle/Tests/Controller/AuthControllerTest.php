<?php
namespace AppBundle\Tests\Controller;

use AppBundle\Entity\User;
use AppBundle\Tests\AppTest;
use Symfony\Bundle\SwiftmailerBundle\DataCollector\MessageDataCollector;
use Symfony\Component\HttpFoundation\Response;

class AuthControllerTest extends AppTest
{
    protected static $confirmationEmailToken;
    protected static $forgottenPasswordToken;

    public function testRegistration()
    {
        $params = array(
            'username'       => static::$fake_email,
            'password'       => static::$fake_psw,
            'birthdate'      => '19900101',
            'plain_password' => static::$fake_psw
        );

        /** Try sign up without firstname or lastname */
        $params['firstname'] = static::$fake_firstname;
        static::$content = json_encode($params);
        $this->send('/user/registration/borrower', array(), 'POST');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());

        $params['lastname'] = static::$fake_lastname;
        unset($params['firstname']);
        static::$content = json_encode($params);
        $this->send('/user/registration/borrower', array(), 'POST');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());
        $params['firstname'] = static::$fake_firstname;

        /** Try sign up with not same password and plain_password EXPECTED BAD_REQUEST **/
        $params['plain_password'] = static::$fake_psw . 'test';
        static::$content = json_encode($params);
        $this->send('/user/registration/borrower', array(), 'POST');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());

        /** Try sign up with empty confirm_url EXPECTED BAD_REQUEST**/
        $params['confirm_url'] = '';
        $params['plain_password'] = static::$fake_psw;
        static::$content = json_encode($params);
        $this->send('/user/registration/borrower', array(), 'POST');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());

        static::$client->enableProfiler();

        /** Trie sign up with correct data **/
        $params['confirm_url'] = '/frontend_confirm_url';
        static::$content = json_encode($params);
        $this->send('/user/registration/borrower', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        $response = json_decode(static::$client->getResponse()->getContent());
        /** check existing token */
        $this->assertNotEmpty($response->content->token);
        /** check role after registration */
        $this->assertEquals($response->content->user_status, 'ROLE_EMAIL_NOT_CONFIRMED');

        $messageDataCollector = static::$client->getProfile()->getCollector('swiftmailer');

        /** Test sending mail */
        $this->assertEquals(1, $messageDataCollector->getMessageCount());
        $message = $messageDataCollector->getMessages()[0];
        $this->assertInstanceOf('Swift_Message', $message);
        /** Check if mail was sended to the correct email */
        $this->assertEquals(static::$fake_email, key($message->getTo()));
        $user = static::$em->getRepository(User::class)->findOneByUsername(static::$fake_email);
        static::$confirmationEmailToken = $user->getConfirmEmailToken();
        $confirmEmailToken = $message->getBody();
        /** Check if sended confirm_email_token equals to confirm_email_token in the target Entity */
        $this->assertNotFalse(strpos($confirmEmailToken, static::$confirmationEmailToken));
        /** Check if subject of mail is correct */
        $this->assertEquals('Registration confirmation', $message->getSubject());

        /** If user try sign up again with unconfirmed credentials EXPECTED BAD_REQUEST **/
        $this->send('/user/registration/borrower', array(), 'POST');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());
    }

    public function testConfirmation()
    {
        $params = array(
            'e' => static::$fake_email,
            't' => ''
        );

        /** Try confirm without confirm_email_token EXPECTED HTTP_NOT_FOUND*/
        static::$content = json_encode($params);
        $this->send('/auth/email_confirmation', array(), 'POST');
        $this->assertEquals(Response::HTTP_NOT_FOUND, static::$client->getResponse()->getStatusCode());

        /** Try confirm with correct params (email and confirm_email_token) */
        $params['t'] = static::$confirmationEmailToken;
        static::$content = json_encode($params);
        $this->send('/auth/email_confirmation', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        /** Try confirm email again just after confirmation*/
        $this->send('/auth/email_confirmation', array(), 'POST');
        $this->assertEquals(Response::HTTP_NOT_FOUND, static::$client->getResponse()->getStatusCode());
    }

    public function testLogin()
    {
        $params = array(
            'username' => static::$fake_email,
            'password' => ''
        );
        /** Try log in without password */
        static::$content = json_encode($params);
        $this->send('/auth/login', array(), 'POST');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());

        /** Login with correct credentials */
        $params['password'] = static::$fake_psw;
        static::$content = json_encode($params);
        $this->send('/auth/login', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        $response = json_decode(static::$client->getResponse()->getContent());
        static::$token = $response->token;
        /** Check token */
        $this->assertNotEmpty($response->token);
        /** Check role EXPECTED ROLE_QUOTE_DRAFT */
        $this->assertEquals('ROLE_USER', $response->user_status);
        /** Check non-empty product_id (Borrower or Lender) */
        $this->assertNotEmpty($response->products);
    }

    public function testCheck()
    {
        /** Test check if user logged in EXPECTED 200 RESPONSE CODE */
        static::$content = '';
        $this->send('/auth/check');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        /** Try to check if user logged in with fake Auth token (whsTEST_FAILED) */
        $this->send('/auth/check', array(), 'GET', 'TEST_FAILED');
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, static::$client->getResponse()->getStatusCode());
    }

    public function testLogout()
    {
        /** Test logout */
        $this->send('/auth/logout', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        static::$token = '';
        /** Try logout again after logout */
        $this->send('/auth/check');
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, static::$client->getResponse()->getStatusCode());
    }

    public function testForgotten()
    {
        /** Test with fake email */
        $params['username'] = static::$fake_email . 'FAILED';
        static::$content = json_encode($params);
        $this->send('/auth/forgotten', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        static::$client->enableProfiler();

        /** Test with correct email */
        $params['username'] = static::$fake_email;
        static::$content = json_encode($params);
        $this->send('/auth/forgotten', array(), 'POST');
        /**
         * Profiler message data collector is used to check the count of mails have been sent
         * @var MessageDataCollector $messageDataCollector
         */
        $messageDataCollector = static::$client->getProfile()->getCollector('swiftmailer');
        /** Check response code */
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        /** Check messageCount */
        $this->assertEquals(1, $messageDataCollector->getMessageCount());
        /**
         * @var \Swift_Message $message
         */
        $message = $messageDataCollector->getMessages()[0];
        /** Check subject of message*/
        $this->assertEquals('Forgotten password', $message->getSubject());
        /** Check if mail sended to the correct email */
        $this->assertEquals(static::$fake_email, key($message->getTo()));
        //TODO: need to get forgotterpasswordtoken
        /*$user = static::$em->getRepository(User::class)->findOneByUsername(static::$fake_email);
        static::$forgottenPasswordToken = $user->getForgottenPasswordToken();
        $forgottenPasswordToken = $message->getBody();*/
        /** Check if sended forgotten_email_token equals to forgotten_email_token in the target Entity */
        /*  $this->assertNotFalse(strpos($forgottenPasswordToken, static::$forgottenPasswordToken));*/
    }
}
