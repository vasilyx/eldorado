<?php
/**
 * User: Vladimir
 * Date: 1/27/17
 * Time: 10:02 AM
 */

namespace AppBundle\Tests\Controller;


use AppBundle\Entity\Product;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\User;
use AppBundle\Entity\UserProduct;
use AppBundle\Exception\QuoteCreationException;
use AppBundle\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\Container;

class UserManagerTest extends WebTestCase
{
    /** @var Client $client */
    private $client;
    /** @var Container $container */
    private $container;

    public function setUp()
    {
        $this->client = static::createClient();
        $this->container = $this->client->getContainer();
    }

    /**
     *
     */
    public function testAddProductToUser()
    {
//        $quoteManager = $this->container->get('whs.manager.quote_apply_manager');
//        $user = new User();
//        $user->setUsername('testUserName' . microtime() . '@example.com');
//        $user->setPassword('testPassword');
//        $em = $this->container->get('doctrine.orm.entity_manager');
//        $userProduct = new UserProduct();
//        $userProduct->setUser($user);
//        $userProduct->setProduct($em->getRepository('AppBundle:Product')->getProduct(Product::TYPE_BORROWER));
//        $em->persist($user);
//        $em->persist($userProduct);
//        $em->flush();
//        $quote1 = $quoteManager->createQuoteForUser($user);
//        $this->assertTrue($quote1 instanceof QuoteApply);
//        $quote1->setStatus(QuoteApply::STATUS_DRAFT);
//        $quote1->setUserProduct($userProduct);
//        $em->persist($quote1);
//        $em->flush();
//        $exception = null;
//        try {
//            $quoteManager->createQuoteForUser($user);
//        } catch (\Exception $exception) {
//            $exception = $exception;
//        } finally {
//            $this->assertTrue($exception instanceof QuoteCreationException);
//            $this->assertEquals(QuoteCreationException::DRAFTED_QUOTE_EXIST['code'], $exception->getCode());
//        }
    }
}