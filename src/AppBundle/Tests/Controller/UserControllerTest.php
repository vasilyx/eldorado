<?php
namespace AppBundle\Tests\Controller;

use AppBundle\Command\CallReportSendCommand;
use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\Communication;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\QuoteTerm;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCommunication;
use AppBundle\Entity\UserDetails;
use AppBundle\Entity\UserIncome;
use AppBundle\Tests\AppTest;
use Doctrine\Common\Collections\Criteria;
use LogBundle\Entity\UserActivityLog;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpFoundation\Response;


class UserControllerTest extends AppTest
{
    protected static $fake_quote_term_amount;
    protected static $fake_quote_term;
    protected static $fake_rate;
    protected static $fake_main_source;
    protected static $fake_total_gross_anual;
    protected static $address_id;
    protected static $fake_industry;

    public static function setUpBeforeClass()
    {
        static::$fake_quote_term = 12;
        static::$fake_quote_term_amount = 1000;
        static::$fake_rate = 20;
        static::$fake_main_source = 'realty';
        static::$fake_total_gross_anual = 5000;
        static::$fake_industry = 'test' . time();

        parent::setUpBeforeClass();
        /** Registration user without confirm */
//        static::$fake_email = time() . '@test.com';
        static::signUp();
    }

    /** Testing api with unconfirmed user. Check if user not logged in. */
    public function testUserLoggedIn()
    {
        $params = array(
            'username' => static::$fake_email,
            'password' => static::$fake_psw
        );
        static::$content = json_encode($params);
        $this->send('/auth/login', array(), 'POST');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());
    }

    public function testAddQuoteTerm()
    {
        /** Test with correct data */
        $params = array(
            'amount' => static::$fake_quote_term_amount,
            'term'   => static::$fake_quote_term - 1
        );
        /** Try to add quote_term with fake terms*/
        static::$content = json_encode($params);
        $this->send('/user/add_quote_term', array(), 'POST');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());
        /** check error code */
        $this->assertEquals('form.quoteTerm.term.notInRange', json_decode(static::$client->getResponse()->getContent())->content->term->code);

        $params['amount'] = static::$fake_quote_term_amount - 1;
        static::$content = json_encode($params);
        $this->send('/user/add_quote_term', array(), 'POST');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());
        /** check error code */
        $this->assertEquals('form.quoteTerm.amount.notInRange', json_decode(static::$client->getResponse()->getContent())->content->amount->code);

        /** Addition quote_term with correct data */
        $params['amount'] = static::$fake_quote_term_amount += mt_rand(100, 10000);
        $params['term'] = static::$fake_quote_term += mt_rand(1, 40);
        static::$content = json_encode($params);
        $this->send('/user/add_quote_term', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        /** Test quote_term data in database */
        $user = static::$em->getRepository(User::class)->findOneByUsername(static::$fake_email);
        $quoteTermManager = static::$client->getContainer()->get('whs.manager.quote_term_manager');
        /** @var QuoteTerm $quoteTerm */
        $quoteTerm = $quoteTermManager->getDraftedQuoteTerm($user);
        $this->assertEquals(static::$fake_quote_term, $quoteTerm->getTerm());
        $this->assertEquals(static::$fake_quote_term_amount, $quoteTerm->getAmount());

        /** Check not null quote_apply */
        $quoteApplyManager = static::$client->getContainer()->get('whs.manager.quote_apply_manager');
        $quoteApply = $quoteApplyManager->getDraftQuoteApply($user);
        $this->assertNotNull($quoteApply);
    }

    public function testAddQuoteIncome()
    {
        /** Try add quote income with wrong rate field */
        $params = array(
            'total_gross_annual' => static::$fake_total_gross_anual += mt_rand(100, 2000),
            'main_source'        => static::$fake_main_source,
            'rate'               => static::$fake_rate + mt_rand(1, 9)
        );

        /** Quote income with fake rate */
        static::$content = json_encode($params);
        $this->send('/user/quote_income', array(), 'POST');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());
        /** check error code */
        $this->assertEquals('form.quoteIncome.rate.notAvailableRate', json_decode(static::$client->getResponse()->getContent())->content->rate->code);

        /** Quote income with correct data */
        $params['rate'] = static::$fake_rate;
        static::$content = json_encode($params);
        $this->send('/user/quote_income', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        /** check if id's are equal (founded quoteApply by user and qouteApply in gouteIncome) */
        $user = static::$em->getRepository(User::class)->findOneByUsername(static::$fake_email);
        $quoteApplyManager = static::$client->getContainer()->get('whs.manager.quote_apply_manager');
        $quoteApply = $quoteApplyManager->getDraftQuoteApply($user);
        $quoteIncomeManager = static::$client->getContainer()->get('whs.manager.quote_income_manager');
        $quoteIncome = $quoteIncomeManager->getQuoteIncome($user);
        self::assertEquals($quoteApply->getId(), $quoteIncome->getQuoteApply()->getId());
    }

    public function testAddress()
    {
        /** Test adding address data (information about form validation from validation.yml) */
        $params = array(
            'postal_code' => 'SW1A 0PW',
            'city'        => 'London',
            'street'      => 'Harrow HA3 9HJ',
            'country'     => 'GB',
            'line1'       => 'test'
        );
        static::$content = json_encode($params);
        /** without moved field */
        $this->send('/user/address', array(), 'POST');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());
        $this->assertEquals('form.userAddress.moved.isBlank',
            json_decode(static::$client->getResponse()->getContent())->content->moved->code);

        /** without postal_code  (see validation.yml)*/
        $params['moved'] = '20101010';
        /* unset($params['country']);
         static::$content = json_encode($params);
         $this->send('/user/address', array(), 'POST');
         $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());
         $this->assertEquals('form.userAddress.country.isBlank', json_decode(static::$client->getResponse()->getContent())->content->country->code);*/

        /** with correct validation and data */
        $params['country'] = 'GB';
        static::$content = json_encode($params);
        $this->send('/user/address', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        /** check unique moved field */
        $this->send('/user/address', array(), 'POST');
        $this->assertEquals('form.userAddress.moved.notUnique', json_decode(static::$client->getResponse()->getContent())->content->moved->code);

        /** add address with another moved date */
        $params['moved'] = '20161010';
        static::$content = json_encode($params);
        $this->send('/user/address', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        /** test GET currect_address */
        $moved = $params['moved'];
        static::$content = '';
        $this->send('/user/address_current');
        $addresses = json_decode(static::$client->getResponse()->getContent());
        /** check response code */
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        /** check current address */
        $this->assertEquals($moved, $addresses->moved);
        /** current address count = 1 */
        $this->assertEquals(1, count($addresses));

        /** test get all address list */
        $this->send('/user/address');
        /** check response code */
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        $addresses = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals(2, count($addresses));
        /** check sorting addresses */
        $this->assertGreaterThan($addresses[1]->moved, $addresses[0]->moved);

        /** delete current address */
        $this->send('/user/address', array('id' => $addresses[1]->id), 'DELETE');
        /** check if success */
        $this->assertEquals(Response::HTTP_NO_CONTENT, static::$client->getResponse()->getStatusCode());
        /** current address have been changed */
        $this->send('/user/address_current');
        $this->assertEquals(json_decode(static::$client->getResponse()->getContent())->id, $addresses[0]->id);

        static::$address_id = $addresses[0]->id;
        /** try delete fake address */
        $this->send('/user/address', array('id' => '99999'), 'DELETE');
        $this->assertEquals(Response::HTTP_NOT_FOUND, static::$client->getResponse()->getStatusCode());
    }

    public function testQuoteApply()
    {
        /** test quote_apply with fake address
         *  delete correct address */
        $this->send('/user/address', array('id' => static::$address_id), 'DELETE');
        /** set fake address */
        $params = array(
            'postal_code' => 'SW1A 0PW',
            'city'        => 'London',
            'street'      => 'Harrow HA3 9HJ',
            'country'     => 'GB',
            'moved'       => date('Ymd'),
            'line1'       => 'test'
        );
        static::$content = json_encode($params);
        $this->send('/user/address', $params, 'POST');
        static::$content = '';
        $this->send('/user/apply_quote');
        /** check response code and content (error message) */
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());
        $this->assertEquals('userAddress.moved.smallPeriod',
            json_decode(static::$client->getResponse()->getContent(), true)['content']['userAddress.moved']['code']);

        /** adding correct moved address */
        $params['moved'] = '20101010';
        static::$content = json_encode($params);
        $this->send('/user/address', array(), 'POST');
        static::$content = '';
        /** try request apply_quote with the correct data (data have been created above) */
        $this->send('/user/apply_quote');
        $response = json_decode(static::$client->getResponse()->getContent());
        /** check response code and status */
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode(), static::$client->getResponse()->getContent());
        $this->assertEquals($response->status, 'success');
        /** status quote is PENDING */
        $this->assertEquals($response->content->status, QuoteApply::STATUS_PENDING);
        $quoteApplyId = $response->content->id;

        /** try request quote_apply again (EXPECTED errors such as Object quoteTerm must not be null
         * and quoteTerm is null*/
        $this->send('/user/apply_quote');
        $response = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());
        $this->assertEquals('error', $response->status);
        $this->assertEquals('Object quoteTerm must not be null', $response->content->quoteTerm->code);

        /** get all quotes of unconfirmed user EXPECTED HTTP_FORBIDDEN code 403 */
        $this->send('/user/quotes', array('product_type' => 0));
        $this->assertEquals(Response::HTTP_FORBIDDEN, static::$client->getResponse()->getStatusCode());

        return $quoteApplyId;
    }

    /**
     * @param $quoteApplyId
     * @depends testQuoteApply
     * @return integer
     */
    public function testCallCreditCreating($quoteApplyId)
    {
        $quoteApplySuccess = static::$em->getRepository(QuoteApply::class)->find($quoteApplyId);
        $this->assertEquals($quoteApplySuccess->getStatus(), QuoteApply::STATUS_PENDING);
        /**
         * After successfull getApplyQuote api.callreport.request event is generated with new CallCreditAPICalls object in which QuoteApply with status pending is setted. On this event CallCreditApiListener sets status NEW and type REPORT to CallCreditAPICalls and saves it to the database.
         * @var CallCreditAPICalls $callCredit
         */
        $callCredit = static::$em->getRepository(CallCreditAPICalls::class)->findOneBy(['quoteApply' => $quoteApplySuccess]);
        $this->assertEquals($callCredit->getType(), CallCreditAPICalls::TYPE_CALLREPORT);
        $this->assertEquals($callCredit->getStatus(), CallCreditAPICalls::STATUS_NEW);
        /*
         * Before this users email is not confirmed
         */
        $this->verifiedAndLogin();
        /*
         * After this users email is confirmed and generated email.is_confirmed event. On this event AddToQueueRequestListener changes CallCreditAPICalls status to pending. Go to next test.
         */

        return $callCredit->getId();
    }

    /**
     * LoanApply should be created for Borrower only on "the QuoteApply status has been changed from pending to approved" event
     * This test depends on previous: testCallCreditCreating
     * @depends testCallCreditCreating
     * @param integer $callCreditId
     */
    public function testLoanApplyCreate($callCreditId)
    {
        /**
         * Now we are ready to check CallCreditAPICalls status
         */
        $callCredit = static::$em->getRepository(CallCreditAPICalls::class)->find($callCreditId);
        $this->assertEquals($callCredit->getStatus(), CallCreditAPICalls::STATUS_PENDING);
        /**
         * Send call report request and receive response. It may be RabbitMQ consumer, Symfony command or other. After response receiving CallCreditAPICalls status should be changed and if there is SUCCESS, event api.callcredit.success should be generated with GenericEvent and 'callcredit_id' argument. CallCreditResponseListener listens this event. It calls an algorithm that changes QuoteApply status to approved or rejected.
         */
        $this->callReportResponce();
        /**
         * Check that CallCreditAPICall has been processed right
         */
        $callCredit = static::$em->getRepository(CallCreditAPICalls::class)->find($callCreditId);
        $this->assertEquals($callCredit->getType(), CallCreditAPICalls::TYPE_CALLREPORT);
        $this->assertEquals($callCredit->getStatus(), CallCreditAPICalls::STATUS_SUCCESS);
        /**
         * For QuoteApply status changing doctrine event is listened by AppBundle\Doctrine\EventListener\QuoteApplyChange which generates system event 'quoteapply.updated.status. AppBundle/Event/EntityLifecycleSubscriber listens this event and desides create new LoanApply with status Draft.
         */
        $this->assertEquals($callCredit->getQuoteApply()->getStatus(), QuoteApply::STATUS_APPROVED);
        $loanApply = static::$em->getRepository(LoanApply::class)->findOneBy(['quoteApply' => $callCredit->getQuoteApply()]);
        $this->assertEquals($loanApply->getStatus(), LoanApply::STATUS_DRAFT);
    }

    public function callReportResponce($testMode = CallCreditAPICalls::STATUS_SUCCESS, $testScore = QuoteApply::GUAGE_SCORE_APPROVE)
    {
        $application = new Application(static::createClient()->getKernel());
        $application->add(new CallReportSendCommand());

        $command = $application->find('app:call_report_send');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'      => $command->getName(),
            '--test-mode'  => $testMode,
            '--test-score' => $testScore
        ]);
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function testEmptyMobile()
    {
        static::$kernel->getContainer()->get('whs.manager.user_communication_manager')->getMobileCommunicationForUser(static::$session->getUser());
    }

    public function testAddInvestorContacts()
    {
        /** Try add investor contacts without communications */
        $params = array(
            'title'       => 'investor_title',
            'screen_name' => 'investor_screen_name'
        );

        static::$content = json_encode($params);
        $this->send('/user/add_investor_contacts', array(), 'POST');
        $this->assertEquals(Response::HTTP_NOT_FOUND, static::$client->getResponse()->getStatusCode());

        /** Try add investor contacts with incomplete communications */
        $params = array(
            'communications' => array('200' => '+37525111111111', '300' => '')
        );
        static::$content = json_encode($params);
        $this->send('/user/add_investor_contacts', array(), 'POST');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());

        /** Try add investor contacts with correct communications */
        $params = array(
            'communications' => array('200' => '+37525111111111')
        );
        static::$content = json_encode($params);
        $this->send('/user/add_investor_contacts', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        /** Check if data saved into UserDetails */
        $params['title'] = 'investor_title';
        $params['screen_name'] = 'investor_screen_name';
        static::$content = json_encode($params);
        $this->send('/user/add_investor_contacts', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        $user = static::$em->getRepository(User::class)->findOneByUsername(static::$fake_email);
        $userDetailsManager = static::$client->getContainer()->get('whs.manager.user_details_manager');
        /** @var UserDetails $details */
        $details = $userDetailsManager->getDetailsForUser($user);
        $this->assertEquals($params['title'], $details->getTitle());
        $this->assertEquals($params['screen_name'], $details->getScreenName());
    }

    public function testGetInvestorCommunications()
    {
        /** Check communications */
        static::$content = '';
        $this->send('/user/investor_communications');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        $user = static::$em->getRepository(User::class)->findOneByUsername(static::$fake_email);
        $userCommunicationManager = static::$client->getContainer()->get('whs.manager.user_communication_manager');
        $userCommunications = $userCommunicationManager->getCommunicationList($user);
        $response = json_decode(static::$client->getResponse()->getContent(), true);
        /** @var UserCommunication $userCommunication */
        foreach ($userCommunications as $userCommunication) {
            /** check communication value, type, and verified */
            $this->assertTrue(in_array($userCommunication, $response['content']));
        }
    }

    public function testAddUserIncome()
    {
        $params = array(
            'total_income_gross_ann' => null,
            'main_income_source'     => null,
        );
        static::$content = json_encode($params);
        $this->send('/user/user_income', array(), 'POST');
        /** check all required fields and error codes  */
        $response = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals('userIncome.totalIncomeGrossAnn.isBlank', $response->content->totalIncomeGrossAnn->code);
        $this->assertEquals('userIncome.mainIncomeSource.isBlank', $response->content->mainIncomeSource->code);

        /** try with correct data */
        $params = array(
            'total_income_gross_ann' => 10000,
            'main_income_source'     => 'realty',
        );
        static::$content = json_encode($params);
        $this->send('/user/user_income', array(), 'POST');
        /** check if response code is 200 */
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        $user = static::$em->getRepository(User::class)->findOneByUsername(static::$fake_email);
        $response = json_decode(static::$client->getResponse()->getContent());
        /** compare user_id  */
        $userIncomeManager = static::$client->getContainer()->get('whs.manager.user_income_manager');
        /** @var UserIncome $userIncome */
        $userIncome = $userIncomeManager->getUserIncomeById($response->content->id);
        $this->assertEquals($user->getId(), $userIncome->getUser()->getId());
        /** check if active */
        $this->assertEquals(false, $response->content->archive);
        /** check non-empty fields */
        $this->assertNotEmpty($response->content->total_income_gross_ann);
        $this->assertNotEmpty($response->content->main_income_source);

        /** TEST get UserIncome (GET /user/user_income) */
        static::$content = '';
        $this->send('/user/user_income');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        $a = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals($response->content->id, json_decode(static::$client->getResponse()->getContent())->content->id);

        // todo: post them before

        /** TEST get BankDetails (GET /user/bank_details) */

// test BankDetails
        //GET empty
        $this->send('/user/bank_details');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $this->assertEquals([], json_decode(static::$client->getResponse()->getContent())->content);

        //todo: POST wrong
        //POST correct
        $params = [
            'bank_sort_code'      => 234344,
            'bank_account_number' => 456456456,
        ];
        static::$content = json_encode($params);
        $this->send('/user/bank_details', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $this->assertEquals([], json_decode(static::$client->getResponse()->getContent())->content);

        //GET non empty
        $this->send('/user/bank_details');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals($params['bank_sort_code'], $content->bank_sort_code);
        $this->assertEquals($params['bank_account_number'], $content->bank_account_number);
    }

    // TODO: create more asserts
    /** Test PATCH user_preferences */
    public function testsUserPreferences()
    {
        $params = [
            'receive_comms' => false
        ];
        static::$content = json_encode($params);
        $this->send('/user/user_preferences', [], 'PATCH');
        /** Check response code */
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        $response = json_decode(static::$client->getResponse()->getContent());
        /** check response content */
        $this->assertFalse($response->content->receive_comms);
        /** change status */
        $params = [
            'receive_comms' => true
        ];
        static::$content = json_encode($params);
        $this->send('/user/user_preferences', [], 'PATCH');
        /** check response code and response content */
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        $this->assertEquals(json_decode(static::$client->getResponse()->getContent())->content->receive_comms, true);
    }

    public function testOffer()
    {
        static::verifiedAndLogin();
        $this->send('/user/invest_offers', array(), 'POST');
        $response = json_decode(static::$client->getResponse()->getContent());
        /** check error codes */
        $this->assertEquals($response->content->rate->code, 'investOffer.rate.isBlank');
        $this->assertEquals($response->content->investmentType->code, 'investOffer.investmentType.isBlank');
        $this->assertEquals($response->content->financeType->code, 'investOffer.financeType.isBlank');
        $this->assertEquals($response->content->investmentAmount->code, 'investOffer.investmentAmount.isBlank');
        $this->assertEquals($response->content->investmentFrequency->code, 'investOffer.investmentFrequency.isBlank');
        $this->assertEquals($response->content->term->code, 'investOffer.term.isBlank');
        $this->assertEquals($response->content->investmentName->code, 'investOffer.investmentName.isBlank');
    }

    public function testPostUserEmployment()
    {
        $this->send('/user/employments', array(), 'POST');
        $response = json_decode(static::$client->getResponse()->getContent());
        /** check error codes */
        $this->assertEquals(Response::HTTP_BAD_REQUEST,
            static::$client->getResponse()->getStatusCode(),
            'POST: /user/employments ' . static::$client->getResponse()->getStatusCode());
        $this->assertEquals($response->content->occMonthStarted->code, 'userEmployment.occMonthStarted.isBlank');
        $this->assertEquals($response->content->businessFirmName->code, 'userEmployment.businessFirmName.isBlank');
        $this->assertEquals($response->content->industry->code, 'userEmployment.industry.isBlank');
        $this->assertEquals($response->content->occPostalCode->code, 'userEmployment.postalCode.isBlank');
        $this->assertEquals($response->content->occCountry->code, 'userEmployment.occCountry.isBlank');
        $this->assertEquals($response->content->occStatus->code, 'userEmployment.occStatus.isBlank');
        $this->assertEquals($response->content->jobDescription->code, 'userEmployment.jobDescription.isBlank');
        $this->assertEquals($response->content->profQual->code, 'userEmployment.profQual.isBlank');
        $this->assertEquals($response->content->currentStatus->code, 'userEmployment.currentStatus.isBlank');

        $params = array(
            'occ_postal_code' => 'TEST'
        );
        static::$content = json_encode($params);
        $this->send('/user/employments', array(), 'POST');
        $response = json_decode(static::$client->getResponse()->getContent());
        /** check postal_code validation */
        $this->assertEquals($response->content->occPostalCode->code, 'userEmployment.postalCode.notValid');

        /** check with fake occ_month_started and ended data */
        $params = array(
            'occ_month_started'  => '20160101',
            'occ_month_ended'    => '20150101',
            'occ_postal_code'    => 'SW1A 0PW',
            'business_firm_name' => 'test firm name',
            'industry'           => static::$fake_industry,
            'occ_country'        => 'GB',
            'occ_status'         => 'director',
            'job_description'    => 'director director',
            'prof_qual'          => 'test',
            'current_status'     => true
        );
        static::$content = json_encode($params);
        $this->send('/user/employments', array(), 'POST');
        $response = json_decode(static::$client->getResponse()->getContent());
        /** check error code */
        //$this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());

        $params['occ_month_ended'] = '20170101';
        static::$content = json_encode($params);
        $this->send('/user/employments', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        $params['occ_month_ended'] = null;
        $params['occ_month_started'] = '20160202';
        static::$content = json_encode($params);
        $this->send('/user/employments', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        /** check update method with fake id */
        $params['id'] = '9999';
        $this->send('/user/employments', array(), 'POST');
        //$this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());

        /** TEST GET and DELETE */
        static::$content = '';
        /** get UserEmployments */
        $this->send('/user/employments');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        $rowsAmount = count(json_decode(static::$client->getResponse()->getContent())->content);

        /** try delete UserEmployment */
        $params = array('id' => json_decode(static::$client->getResponse()->getContent())->content[0]->id);
        static::$content = json_encode($params);
        $this->send('/user/deployments', array(), 'DELETE');
        //$this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        /** get UserEmployments */
        $this->send('/user/employments');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        $this->assertNotEquals($rowsAmount, count(json_decode(static::$client->getResponse()->getContent())));
    }

    public function testUserDependent()
    {
        $params = array();
        static::$content = json_encode($params);
        $this->send('/user/dependent', array(), 'POST');
        /** check with empty content body */
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());
        /** check errors */
        $response = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals($response->content->firstName->code, 'userDependent.firstName.isBlank');
        $this->assertEquals($response->content->birthdate->code, 'userDependent.birthdate.isBlank');

        $params = array(
            'first_name' => 'Ivan',
            'birthdate'  => '19990909'
        );
        /** request with correct content body */
        static::$content = json_encode($params);
        $this->send('/user/dependent', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        /** test GET METHOD */
        static::$content = '';
        $this->send('/user/dependent');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        /** test PATCH*/
        $params['first_name'] = 'Ivanushka';
        static::$content = json_encode($params);
        $this->send('/user/dependent/' . json_decode(static::$client->getResponse()->getContent())->content[0]->id, array(), 'PATCH');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        $this->assertEquals(json_decode(static::$client->getResponse()->getContent())->content->first_name, $params['first_name']);

        /** test DELETE */
        $params = array(
            'id' => json_decode(static::$client->getResponse()->getContent())->content->id
        );
        static::$content = json_encode($params);
        $this->send('/user/dependent', array(), 'DELETE');
        /** after delete dependent count = 0 */
        $this->send('/user/dependent');
        $this->assertEquals(0, count(json_decode(static::$client->getResponse()->getContent())->content));
    }

    public function testHouseHold()
    {
        /** try with fake data */
        static::$content = json_encode(array());
        $this->send('/user/household', array(), 'POST');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());
        $response = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals($response->content->monthlyRent->code, 'userHousehold.monthlyRent.isBlank');
        $this->assertEquals($response->content->monthlyUtilities->code, 'userHousehold.monthlyUtilities.isBlank');
        $this->assertEquals($response->content->monthlyTravel->code, 'userHousehold.monthlyTravel.isBlank');
        $this->assertEquals($response->content->monthlyFood->code, 'userHousehold.monthlyFood.isBlank');
        $this->assertEquals($response->content->monthlySavings->code, 'userHousehold.monthlySavings.isBlank');
        $this->assertEquals($response->content->monthlyEntertainment->code, 'userHousehold.monthlyEntertainment.isBlank');

        /** check with correct data */
        $params = array(
            'monthly_rent'          => '999',
            'monthly_utilities'     => '1000',
            'monthly_travel'        => '1000',
            'monthly_food'          => '1000',
            'monthly_savings'       => '1000',
            'monthly_entertainment' => '1000'
        );
        static::$content = json_encode($params);
        $this->send('/user/household', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        /** GET household */
        static::$content = '';
        $this->send('/user/household');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        /** compare data in bd and in test */
        $this->assertEquals(json_decode(static::$client->getResponse()->getContent())->content->monthly_rent, $params['monthly_rent']);
    }

    public function testUserCreditline()
    {
        // test OK response and 9 creditline types existence
        $this->send('/user/credit_line/types');
        $this->assertEquals(static::$client->getResponse()->getStatusCode(), Response::HTTP_OK, 'GET /api/user/credit_line/types fails with code ' . static::$client->getResponse()->getStatusCode());
        $creditLineTypesResponse = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertEquals(count($creditLineTypesResponse['content']), 9, 'GET /user/credit_line/types will return 9 types of creditline');

        // empty array will be returned if user hasn't creditlines
        $this->send('/user/credit_line');
        $emptyCreditLineResponse = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals($emptyCreditLineResponse->content, [], 'GET /api/user/credit_line will return empty array now');

        // 404 code will be returned if trying to get not existing creditline by id
        $this->send('/user/credit_line', ['id' => 888]);
        $this->assertEquals(static::$client->getResponse()->getStatusCode(), Response::HTTP_NOT_FOUND);

        // create creditline with correct params will be OK (200 code)
        $params = [
            'type'            => 200,
            'name'            => 'fake name',
            'credit_limit'    => 10000,
            'balance_input'   => 5000,
            'apr'             => 12.12,
            'monthly_payment' => 800,
        ];
        static::$content = json_encode($params);
        $this->send('/user/credit_line', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());

        // create creditline with empty params will be NOT OK (not 200 code) and return validation errors code for all fields
        $params = [];
        static::$content = json_encode($params);
        $this->send('/user/credit_line', [], 'POST');
        $this->assertNotTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals('userCreditLine.type.isBlank', $content->content->type->code);
        $this->assertEquals('userCreditLine.name.isBlank', $content->content->name->code);
        $this->assertEquals('userCreditLine.creditLimit.isBlank', $content->content->creditLimit->code);
        $this->assertEquals('userCreditLine.balanceInput.isBlank', $content->content->balanceInput->code);
        $this->assertEquals('userCreditLine.apr.isBlank', $content->content->apr->code);
        $this->assertEquals('userCreditLine.monthlyPayment.isBlank', $content->content->monthlyPayment->code);
    }

    public function testCommunications()
    {
        $params = [
            'communications' => [
                /*
                 * mobile phone
                 */
                '200' => '+999999999999',
                /*
                 * home phone
                 */
                '300' => '9999999',
                /*
                 * work phone
                 */
                '400' => '8888888'
            ]
        ];
        static::$content = json_encode($params);
        $this->send('/user/add_investor_contacts', array(), 'POST');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $communications = static::$em->getRepository(UserCommunication::class)->findBy(['user' => self::$session->getUser(), 'archive' => false, 'isPrimary' => true]);
        $this->assertNotEmpty($communications);
        foreach ($communications as $communication) {
            switch ($communication->getCommunication()->getType()) {
                case Communication::TYPE_MOBILE_PHONE:
                    $this->assertEquals($communication->getValue(), '+999999999999');
                    $firstId = $communication->getId();
                    break;
                case Communication::TYPE_HOME_PHONE:
                    $this->assertEquals($communication->getValue(), '9999999');
                    break;
                case Communication::TYPE_WORK_PHONE:
                    $this->assertEquals($communication->getValue(), '8888888');
            }
        }
        /*
         * Get communications
         */
        $this->send('/user/investor_communications', [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $communications = json_decode(static::$client->getResponse()->getContent(), true)['content'];
        foreach ($communications as $communication) {
            switch ($communication['type']) {
                case Communication::TYPE_MOBILE_PHONE:
                    if($communication['primary']) {
                        $this->assertEquals($communication['value'], '+999999999999');
                    }
                    break;
                case Communication::TYPE_HOME_PHONE:
                    if($communication['primary']) {
                        $this->assertEquals($communication['value'], '9999999');
                    }
                    break;
                case Communication::TYPE_WORK_PHONE:
                    if($communication['primary']) {
                        $this->assertEquals($communication['value'], '8888888');
                    }
            }
        }
        /*
         * Update communication with the same value
         */
        $params = [
            'communications' => [
                /*
                 * mobile phone
                 */
                '200' => '+999999999999',
            ]
        ];
        static::$content = json_encode($params);
        $this->send('/user/add_investor_contacts', array(), 'POST');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $communications = static::$em->getRepository(UserCommunication::class)->findBy(['user' => self::$session->getUser(), 'value' => '+999999999999']);
        /*
         * Check that it is the same communication
         */
        $secondId = $communications[0]->getId();
        $this->assertEquals($firstId, $secondId);
        /*
         * Update communication
         */
        $params = [
            'communications' => [
                /*
                 * mobile phone
                 */
                '200' => '+999999999991',
            ]
        ];
        static::$content = json_encode($params);
        $this->send('/user/add_investor_contacts', array(), 'POST');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        /*
         * Clear entity manager
         */
        static::$em->clear();

        /*
         * Check for old mobile value is false and new is true
         */
        /** @var UserCommunication $communication $communication */
        $communication = static::$em->getRepository(UserCommunication::class)->findOneBy(['user' => self::$session->getUser(), 'value' => '+999999999999']);
        $this->assertEquals($communication->getIsPrimary(), false);
        /*
         * Check that it is the same communication
         */
        $thirdId = $communication->getId();
        $this->assertEquals($thirdId, $secondId);
        $communication = static::$em->getRepository(UserCommunication::class)->findOneBy(['user' => self::$session->getUser(), 'value' => '+999999999991']);
        $this->assertEquals($communication->getArchive(), false);

        /*
         * Get communications
         */
        $this->send('/user/investor_communications', [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $communications = json_decode(static::$client->getResponse()->getContent(), true)['content'];
        foreach ($communications as $communication) {
            switch ($communication['type']) {
                case Communication::TYPE_MOBILE_PHONE:
                    if($communication['primary']){
                        $this->assertEquals($communication['value'], '+999999999991');
                    }
                    break;
            }
        }

        /*
         * Revert communication
         */
        $params = [
            'communications' => [
                /*
                 * mobile phone
                 */
                '200' => '+999999999999',
            ]
        ];
        static::$content = json_encode($params);
        $this->send('/user/add_investor_contacts', array(), 'POST');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $communications = static::$em->getRepository(UserCommunication::class)->findBy(['user' => self::$session->getUser(), 'value' => '+999999999999']);
        /*
         * Check that it is the same communication
         */
        $forthId = $communications[0]->getId();
        $this->assertEquals($forthId, $secondId);
        $communications = json_decode(static::$client->getResponse()->getContent(), true)['content'];
        foreach ($communications as $communication) {
            switch ($communication['type']) {
                case Communication::TYPE_MOBILE_PHONE:
                    if($communication['primary']) {
                        $this->assertEquals($communication['value'], '+999999999999');
                    }
                    break;
            }
        }
    }

    public function testActivityLogs()
    {
        /**
         * Get UserActivityLogs (offset = 1 - the first page, limit = 5 - amount of logs on the one page)
         */
        $params = array(
            'offset' => 1,
            'limit'  => 5
        );

        $this->send('/user/user_activity_logs', $params, 'GET');

        $content = json_decode(static::$client->getResponse()->getContent(), true)['content'];

        /** Amount of logs must be 5 */
        $this->assertEquals(5, count($content['logs']));
        /** @var UserActivityLog $log */
        $log = static::$em->getRepository(UserActivityLog::class)->findOneBy([], ['id' => Criteria::DESC]);
        /** Check the first log id */
        $this->assertEquals($content['logs'][0]['id'], $log->getId());

        /** Check the first log id of the second page (must be less than last on the first page by 1) */
        $params['offset'] = 2;

        $this->send('/user/user_activity_logs', $params, 'GET');

        $lastLog = array_pop($content['logs'])['id'];
        $content = json_decode(static::$client->getResponse()->getContent(), true)['content'];
        $firstLogOnTheSecondPage = array_shift($content['logs'])['id'];
        $this->assertEquals(1, ($lastLog - $firstLogOnTheSecondPage));

        /** Compare count of logs returned by api and in DB */
        $user = static::$em->getRepository(User::class)->findOneByUsername(static::$fake_email);
        $count = count(static::$em->getRepository(UserActivityLog::class)->findByUser($user));
        $this->assertEquals($content['count'], $count);
    }
}