<?php
namespace AppBundle\Tests\Controller;


/*
 * test 'app.utils.payments_computer' service calculations
 */

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PaymentsComputerServiceTest extends WebTestCase
{
    /**
     * @dataProvider sampleDataProvider
     */
    public function testCalculation($sampleData)
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $paymentsComputer = $container->get('app.utils.payments_computer');
        $result = $paymentsComputer->getBaseLoanProperties($sampleData['requesterTerm'], $sampleData['requestedAmount']);
        $this->assertEquals($sampleData['calculatedValues'], $result);
    }

    public function sampleDataProvider()
    {
        //calculated values are values for "Effective" table from excel file
        return [

            [['requesterTerm' => '12', 'requestedAmount' => '1000', 'calculatedValues' => ['interest_rate' => 14.58, 'monthly_payment' => 90.06, 'total_payment' => 1080.72]]],
            [['requesterTerm' => '12', 'requestedAmount' => '3000', 'calculatedValues' => ['interest_rate' => 10.41, 'monthly_payment' => 264.32, 'total_payment' => 3171.84]]],
            [['requesterTerm' => '12', 'requestedAmount' => '5000', 'calculatedValues' => ['interest_rate' => 8.33, 'monthly_payment' => 435.71, 'total_payment' => 5228.47]]],
            [['requesterTerm' => '12', 'requestedAmount' => '7500', 'calculatedValues' => ['interest_rate' => 8.33, 'monthly_payment' => 653.56, 'total_payment' => 7842.70]]],
            [['requesterTerm' => '12', 'requestedAmount' => '10000', 'calculatedValues' => ['interest_rate' => 8.33, 'monthly_payment' => 871.41, 'total_payment' => 10456.93]]],
            [['requesterTerm' => '12', 'requestedAmount' => '15000', 'calculatedValues' => ['interest_rate' => 8.33, 'monthly_payment' => 1307.12, 'total_payment' => 15685.40]]],
            [['requesterTerm' => '12', 'requestedAmount' => '20000', 'calculatedValues' => ['interest_rate' => 8.33, 'monthly_payment' => 1742.82, 'total_payment' => 20913.86]]],

            [['requesterTerm' => '18', 'requestedAmount' => '1000', 'calculatedValues' => ['interest_rate' => 13.54, 'monthly_payment' => 61.70, 'total_payment' => 1110.60]]],
            [['requesterTerm' => '18', 'requestedAmount' => '3000', 'calculatedValues' => ['interest_rate' => 9.67, 'monthly_payment' => 179.72, 'total_payment' => 3234.88]]],
            [['requesterTerm' => '18', 'requestedAmount' => '5000', 'calculatedValues' => ['interest_rate' => 7.74, 'monthly_payment' => 295.11, 'total_payment' => 5311.95]]],
            [['requesterTerm' => '18', 'requestedAmount' => '7500', 'calculatedValues' => ['interest_rate' => 7.74, 'monthly_payment' => 442.66, 'total_payment' => 7967.93]]],
            [['requesterTerm' => '18', 'requestedAmount' => '10000', 'calculatedValues' => ['interest_rate' => 7.74, 'monthly_payment' => 590.22, 'total_payment' => 10623.91]]],
            [['requesterTerm' => '18', 'requestedAmount' => '15000', 'calculatedValues' => ['interest_rate' => 7.74, 'monthly_payment' => 885.33, 'total_payment' => 15935.86]]],
            [['requesterTerm' => '18', 'requestedAmount' => '20000', 'calculatedValues' => ['interest_rate' => 7.74, 'monthly_payment' => 1180.43, 'total_payment' => 21247.82]]],

            [['requesterTerm' => '24', 'requestedAmount' => '1000', 'calculatedValues' => ['interest_rate' => 12.50, 'monthly_payment' => 47.31, 'total_payment' => 1135.38]]],
            [['requesterTerm' => '24', 'requestedAmount' => '3000', 'calculatedValues' => ['interest_rate' => 8.93, 'monthly_payment' => 136.96, 'total_payment' => 3286.99]]],
            [['requesterTerm' => '24', 'requestedAmount' => '5000', 'calculatedValues' => ['interest_rate' => 7.14, 'monthly_payment' => 224.18, 'total_payment' => 5380.33]]],
            [['requesterTerm' => '24', 'requestedAmount' => '7500', 'calculatedValues' => ['interest_rate' => 7.14, 'monthly_payment' => 336.27, 'total_payment' => 8070.49]]],
            [['requesterTerm' => '24', 'requestedAmount' => '10000', 'calculatedValues' => ['interest_rate' => 7.14, 'monthly_payment' => 448.36, 'total_payment' => 10760.66]]],
            [['requesterTerm' => '24', 'requestedAmount' => '15000', 'calculatedValues' => ['interest_rate' => 7.14, 'monthly_payment' => 672.54, 'total_payment' => 16140.99]]],
            [['requesterTerm' => '24', 'requestedAmount' => '20000', 'calculatedValues' => ['interest_rate' => 7.14, 'monthly_payment' => 896.72, 'total_payment' => 21521.32]]],

            [['requesterTerm' => '36', 'requestedAmount' => '1000', 'calculatedValues' => ['interest_rate' => 10.41, 'monthly_payment' => 32.46, 'total_payment' => 1168.56]]],
            [['requesterTerm' => '36', 'requestedAmount' => '3000', 'calculatedValues' => ['interest_rate' => 7.44, 'monthly_payment' => 93.24, 'total_payment' => 3356.50]]],
            [['requesterTerm' => '36', 'requestedAmount' => '5000', 'calculatedValues' => ['interest_rate' => 5.95, 'monthly_payment' => 152.00, 'total_payment' => 5471.87]]],
            [['requesterTerm' => '36', 'requestedAmount' => '7500', 'calculatedValues' => ['interest_rate' => 5.95, 'monthly_payment' => 227.99, 'total_payment' => 8207.81]]],
            [['requesterTerm' => '36', 'requestedAmount' => '10000', 'calculatedValues' => ['interest_rate' => 5.95, 'monthly_payment' => 303.99, 'total_payment' => 10943.74]]],
            [['requesterTerm' => '36', 'requestedAmount' => '15000', 'calculatedValues' => ['interest_rate' => 5.95, 'monthly_payment' => 455.99, 'total_payment' => 16415.62]]],
            [['requesterTerm' => '36', 'requestedAmount' => '20000', 'calculatedValues' => ['interest_rate' => 5.95, 'monthly_payment' => 607.99, 'total_payment' => 21887.49]]],

            [['requesterTerm' => '48', 'requestedAmount' => '1000', 'calculatedValues' => ['interest_rate' => 11.97, 'monthly_payment' => 26.32, 'total_payment' => 1263.32]]],
            [['requesterTerm' => '48', 'requestedAmount' => '3000', 'calculatedValues' => ['interest_rate' => 8.55, 'monthly_payment' => 74.02, 'total_payment' => 3552.76]]],
            [['requesterTerm' => '48', 'requestedAmount' => '5000', 'calculatedValues' => ['interest_rate' => 6.84, 'monthly_payment' => 119.36, 'total_payment' => 5729.30]]],
            [['requesterTerm' => '48', 'requestedAmount' => '7500', 'calculatedValues' => ['interest_rate' => 6.84, 'monthly_payment' => 179.04, 'total_payment' => 8593.95]]],
            [['requesterTerm' => '48', 'requestedAmount' => '10000', 'calculatedValues' => ['interest_rate' => 6.84, 'monthly_payment' => 238.72, 'total_payment' => 11458.60]]],
            [['requesterTerm' => '48', 'requestedAmount' => '15000', 'calculatedValues' => ['interest_rate' => 6.84, 'monthly_payment' => 358.08, 'total_payment' => 17187.90]]],
            [['requesterTerm' => '48', 'requestedAmount' => '20000', 'calculatedValues' => ['interest_rate' => 6.84, 'monthly_payment' => 477.44, 'total_payment' => 22917.20]]],

            [['requesterTerm' => '60', 'requestedAmount' => '1000', 'calculatedValues' => ['interest_rate' => 13.54, 'monthly_payment' => 23.03, 'total_payment' => 1381.83]]],
            [['requesterTerm' => '60', 'requestedAmount' => '3000', 'calculatedValues' => ['interest_rate' => 9.67, 'monthly_payment' => 63.26, 'total_payment' => 3795.31]]],
            [['requesterTerm' => '60', 'requestedAmount' => '5000', 'calculatedValues' => ['interest_rate' => 7.74, 'monthly_payment' => 100.76, 'total_payment' => 6045.66]]],
            [['requesterTerm' => '60', 'requestedAmount' => '7500', 'calculatedValues' => ['interest_rate' => 7.74, 'monthly_payment' => 151.14, 'total_payment' => 9068.49]]],
            [['requesterTerm' => '60', 'requestedAmount' => '10000', 'calculatedValues' => ['interest_rate' => 7.74, 'monthly_payment' => 201.52, 'total_payment' => 12091.31]]],
            [['requesterTerm' => '60', 'requestedAmount' => '15000', 'calculatedValues' => ['interest_rate' => 7.74, 'monthly_payment' => 302.28, 'total_payment' => 18136.97]]],
            [['requesterTerm' => '60', 'requestedAmount' => '20000', 'calculatedValues' => ['interest_rate' => 7.74, 'monthly_payment' => 403.04, 'total_payment' => 24182.63]]],

        ];
    }
}

