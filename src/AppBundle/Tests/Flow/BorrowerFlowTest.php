<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 17.01.17
 * Time: 13:45
 */

namespace AppBundle\Tests\Flow;


use AppBundle\Command\CallReportSendCommand;
use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\Communication;
use AppBundle\Entity\LoanApply;
use AppBundle\Entity\Product;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\UserCommunication;
use AppBundle\Entity\UserCreditline;
use AppBundle\Tests\AppTest;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpFoundation\Response;

class BorrowerFlowTest extends AppTest
{
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        static::signUp();
    }

    /**
     * This method is created to decouple call report algorithm from test logic
     * @param integer $testMode
     * @param integer $testScore
     */
    public function callReportResponce($testMode = CallCreditAPICalls::STATUS_SUCCESS, $testScore = QuoteApply::GUAGE_SCORE_APPROVE)
    {
        $application = new Application(static::createClient()->getKernel());
        $application->add(new CallReportSendCommand());

        $command = $application->find('app:call_report_send');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'      => $command->getName(),
            '--test-mode'  => $testMode,
            '--test-score' => $testScore
        ]);
    }

    public function testBorrowerRegistrationResults()
    {
        /*
         * To make sure that all dependencies are created for user
         */

        /*
         * Check the manager
         */
        $quoteApply = static::$kernel->getContainer()->get('whs.manager.quote_apply_manager')->getDraftBorrowerQuoteApply(static::$session->getUser());
        $this->assertEquals($quoteApply->getUser(), static::$session->getUser());
        $this->assertEquals($quoteApply->getProduct()->getType(), Product::TYPE_BORROWER);
    }

    public function testFirstStep()
    {
        /*
         * First get should be empty
         */
        $this->send('/user/quote_term', [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $this->assertEmpty(json_decode(static::$client->getResponse()->getContent(), true)['content']);

        /** Test with correct data */
        $params = array(
            'loan_amount' => 5000,
            'loan_term'   => 12
        );

        /*
         * When user moves scroll bars with amount and term, UI sends this query
         * Mothly payment is calculated by effective rate
         */
        $this->send('/compute/base_loan_properties', $params, 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $content = json_decode(static::$client->getResponse()->getContent(), true)['content'];
        $this->assertEquals($content['interest_rate'], 8.33);
        $this->assertEquals($content['monthly_payment'], 435.71);
        $this->assertEquals($content['total_payment'], 5228.47);

        /** Test flow when user input data, than come back and change it */
        $params = array(
            'amount' => 5100,
            'term'   => 24
        );


        static::$content = json_encode($params);
        $this->send('/user/add_quote_term', array(), 'POST');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $this->assertEmpty(json_decode(static::$client->getResponse()->getContent(), true)['content']);

        /*
         * Second get should be not empty
         */
        $this->send('/user/quote_term', [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $content = json_decode(static::$client->getResponse()->getContent(), true)['content'];
        $this->assertNotEmpty($content);
        $this->assertEquals($content['amount'], 5100);
        $this->assertEquals($content['term'], 24);

        /** come back and change it */
        $params = array(
            'amount' => 5000,
            'term'   => 12
        );


        static::$content = json_encode($params);
        $this->send('/user/add_quote_term', array(), 'POST');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $this->assertEmpty(json_decode(static::$client->getResponse()->getContent(), true)['content']);

        /*
         * Second get should be not empty
         * Pay attention that monthly_payment and total_payment are different from ones in base_loan_properties method. Because monthly payment is calculated by contract rate = effective rate + bonus rate
         */
        $this->send('/user/quote_term', [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $content = json_decode(static::$client->getResponse()->getContent(), true)['content'];
        $this->assertNotEmpty($content);
        $this->assertEquals($content['amount'], 5000);
        $this->assertEquals($content['term'], 12);
        $this->assertEquals($content['monthly_payment'], 439.77);
        $this->assertEquals($content['total_payment'], 5277.19);
        $this->assertEquals($content['rate'], 8.33);
    }

    public function testSecondStep()
    {
        /*
         * First get should be empty
         */
        $this->send('/user/quote_income', [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $this->assertEmpty(json_decode(static::$client->getResponse()->getContent(), true)['content']);

        /*
         * Send quote_income
         */
        $params = array(
            'has_c_c_j'           => true,
            'has_current_default' => true,
            'is_homeowner'        => true,
            'main_source'         => 'Permanent',
            'rate'                => 30,
            'total_gross_annual'  => 19900
        );
        static::$content = json_encode($params);
        $this->send('/user/quote_income', array(), 'POST');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $this->assertEmpty(json_decode(static::$client->getResponse()->getContent(), true)['content']);

        /*
         * Second get should not be empty
         */
        $this->send('/user/quote_income', [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $content = json_decode(static::$client->getResponse()->getContent(), true)['content'];
        $this->assertEquals($content['has_c_c_j'], true);
        $this->assertEquals($content['has_current_default'], true);
        $this->assertEquals($content['is_homeowner'], true);
        $this->assertEquals($content['main_source'], 'Permanent');
        $this->assertEquals($content['rate'], 30);
        $this->assertEquals($content['total_gross_annual'], 19900);
    }

    public function testThirdStep()
    {
        /*
         * First get address should be empty
         */
        $this->send('/user/address', [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $this->assertEmpty(json_decode(static::$client->getResponse()->getContent(), true));

        /*
         * User preferences should not be empty
         */
        $this->send('/user/user_preferences', [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $content = json_decode(static::$client->getResponse()->getContent(), true)['content'];
        $this->assertEquals($content['receive_comms'], false);
        $this->assertEquals($content['has_no_dependencies'], false);

        /*
         * Send address
         */
        $params = array(
            'postal_code' => 'SW1A 0PW',
            'city'        => 'London',
            'street'      => 'Harrow HA3 9HJ',
            'country'     => 'GB',
            'moved'       => '20101010',
            'line1'       => 'test'
        );

        static::$content = json_encode($params);
        $this->send('/user/address', array(), 'POST');
        $content = json_decode(static::$client->getResponse()->getContent(), true)[0];
        $this->assertNotNull($content['id']);
        $this->assertEquals($content['postal_code'], $params['postal_code']);
        $this->assertEquals($content['city'], $params['city']);
        $this->assertEquals($content['street'], $params['street']);
        $this->assertEquals($content['moved'], $params['moved']);
        $this->assertEquals($content['line1'], $params['line1']);
        $this->assertEquals($content['archive'], false);

        /**
         * Second get address should not be empty
         */
        static::$content = '';
        $this->send('/user/address', array(), 'GET');
        $content = json_decode(static::$client->getResponse()->getContent(), true)[0];
        $this->assertNotNull($content['id']);
        $this->assertEquals($content['postal_code'], $params['postal_code']);
        $this->assertEquals($content['city'], $params['city']);
        $this->assertEquals($content['street'], $params['street']);
        $this->assertEquals($content['moved'], $params['moved']);
        $this->assertEquals($content['line1'], $params['line1']);
        $this->assertEquals($content['archive'], false);

        /*
         * Patch receive_comms
         */
        $params = [
            'receive_comms'       => true,
            'has_no_dependencies' => true
        ];
        static::$content = json_encode($params);
        $this->send('/user/user_preferences', [], 'PATCH');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $content = json_decode(static::$client->getResponse()->getContent(), true)['content'];
        $this->assertEquals($content['receive_comms'], $params['receive_comms']);
        $this->assertEquals($content['has_no_dependencies'], $params['has_no_dependencies']);
    }

    public function testQuoteApply()
    {

        static::$content = '';
        /** try request apply_quote with the correct data (data have been created above) */
        $this->send('/user/apply_quote');
        $response = json_decode(static::$client->getResponse()->getContent());
        /** check response code and status */
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        $this->assertEquals($response->status, 'success');
        /** status quote is PENDING */
        $this->assertEquals($response->content->status, QuoteApply::STATUS_PENDING);
        $quoteApplyId = $response->content->id;
        /** get all quotes of unconfirmed user EXPECTED HTTP_FORBIDDEN code 403 */
        $this->send('/user/quotes', array('product_type' => 0));
        $this->assertEquals(Response::HTTP_FORBIDDEN, static::$client->getResponse()->getStatusCode());

        return $quoteApplyId;
    }

    /**
     * @param $quoteApplyId
     * @depends testQuoteApply
     * @return integer
     */
    public function testCallCreditCreating($quoteApplyId)
    {
        $quoteApplySuccess = static::$em->getRepository(QuoteApply::class)->find($quoteApplyId);
        $this->assertEquals($quoteApplySuccess->getStatus(), QuoteApply::STATUS_PENDING);
        /**
         * After successfull getApplyQuote api.callreport.request event is generated with new CallCreditAPICalls object in which QuoteApply with status pending is set. On this event CallCreditApiListener sets status NEW and type REPORT to CallCreditAPICalls and saves it to the database.
         * @var CallCreditAPICalls $callCredit
         */
        $callCredit = static::$em->getRepository(CallCreditAPICalls::class)->findOneBy(['quoteApply' => $quoteApplySuccess]);
        $this->assertEquals(CallCreditAPICalls::getTypes()[$callCredit->getType()], CallCreditAPICalls::getTypes()[CallCreditAPICalls::TYPE_CALLREPORT]);
        /**
         * Purpose must be Quotation (PURPOSE_QS)
         */
        $this->assertEquals(CallCreditAPICalls::getPurposes()[CallCreditAPICalls::PURPOSE_QS], CallCreditAPICalls::getPurposes()[$callCredit->getPurpose()]);
        $this->assertEquals($callCredit->getStatus(), CallCreditAPICalls::STATUS_NEW);
        /*
         * Before this users email is not confirmed
         */
        $this->verifiedAndLogin();
        /*
         * After this users email is confirmed and generated email.is_confirmed event. On this event AddToQueueRequestListener changes CallCreditAPICalls status to pending.
         */

        static::$em->clear();

        $callCredit = static::$em->getRepository(CallCreditAPICalls::class)->find($callCredit->getId());
        $this->assertEquals($callCredit->getStatus(), CallCreditAPICalls::STATUS_PENDING);

        $data = json_decode($callCredit->getRequestXML(), true);
        $this->assertNotEmpty($data['headers']);
        $this->assertNotEmpty($data['apiurl']);
        $this->assertNotEmpty($data['headers']['company']);
        $this->assertNotEmpty($data['headers']['username']);
        $this->assertNotEmpty($data['headers']['password']);

        return $callCredit->getId();
    }

    /**
     * LoanApply should be created for Borrower only on "the QuoteApply status has been changed from pending to approved" event
     * This test depends on previous: testCallCreditCreating
     * @depends testCallCreditCreating
     * @param integer $callCreditId
     * @return integer
     */
    public function testLoanApplyCreate($callCreditId)
    {

        /*
         * Send call report request and receive response. It may be RabbitMQ consumer, Symfony command or other. After response receiving CallCreditAPICalls status should be changed and if there is SUCCESS, event api.callcredit.success should be generated with GenericEvent and 'callcredit_id' argument. CallCreditResponseListener listens this event. It calls an algorithm that changes QuoteApply status to approved or rejected.
         */

        /*
         * First of all check for error call_report response
         */
        $this->callReportResponce(CallCreditAPICalls::STATUS_ERROR);
        /**
         * Check that CallCreditAPICall has been processed right
         */
        $callCredit = static::$em->getRepository(CallCreditAPICalls::class)->find($callCreditId);
        $this->assertEquals(CallCreditAPICalls::getTypes()[$callCredit->getType()], CallCreditAPICalls::getTypes()[CallCreditAPICalls::TYPE_CALLREPORT]);
        /**
         * Purpose must be Quotation (PURPOSE_QS)
         */
        $this->assertEquals(CallCreditAPICalls::getPurposes()[CallCreditAPICalls::PURPOSE_QS], CallCreditAPICalls::getPurposes()[$callCredit->getPurpose()]);
        /*
         * CallCreditApiCalls should change the status to error
         */
        $this->assertEquals($callCredit->getStatus(), CallCreditAPICalls::STATUS_ERROR);
        /*
         * QuoteApply should not change the status
         */
        $this->assertEquals($callCredit->getQuoteApply()->getStatus(), QuoteApply::STATUS_PENDING);
        $loanApply = static::$em->getRepository(LoanApply::class)->findOneBy(['quoteApply' => $callCredit->getQuoteApply()]);
        $this->assertNull($loanApply);
        /*
         * Now we should return the pending status to call_credit_api_calls record
         */
        static::$em->clear();
        $callCredit = static::$em->getRepository(CallCreditAPICalls::class)->find($callCreditId);
        $callCredit
            ->setStatus(CallCreditAPICalls::STATUS_PENDING)
            ->setResponseDate(null)
            ->setResponseXML(null);
        static::$em->flush();
        static::$em->clear();
        /**
         * Check SUCCESS response with score < 490 (reject)
         */
        $this->callReportResponce(CallCreditAPICalls::STATUS_SUCCESS, 480);
        /**
         * Check that CallCreditAPICall has been processed right
         */
        $callCredit = static::$em->getRepository(CallCreditAPICalls::class)->find($callCreditId);
        $this->assertEquals(CallCreditAPICalls::getTypes()[$callCredit->getType()], CallCreditAPICalls::getTypes()[CallCreditAPICalls::TYPE_CALLREPORT]);
        /**
         * Purpose must be Quotation (PURPOSE_QS)
         */
        $this->assertEquals(CallCreditAPICalls::getPurposes()[CallCreditAPICalls::PURPOSE_QS], CallCreditAPICalls::getPurposes()[$callCredit->getPurpose()]);
        /*
         * CallCreditApiCalls should change the status to success
         */
        $this->assertEquals($callCredit->getStatus(), CallCreditAPICalls::STATUS_SUCCESS);
        /*
         * QuoteApply should change the status to rejected
         */
        $this->assertEquals($callCredit->getQuoteApply()->getStatus(), QuoteApply::STATUS_REJECTED);
        /*
         * LoanApply should be null
         */
        $loanApply = static::$em->getRepository(LoanApply::class)->findOneBy(['quoteApply' => $callCredit->getQuoteApply()]);
        $this->assertNull($loanApply);
        /** After successful response CallCredit API, quoteApply got new product:
         * if quoteApply approved - dClutter Premium
         *               refer    - dClutter Standard
         *               reject   - dClutter Social
         */
        $quoteApply = $callCredit->getQuoteApply();
        /** Now product should be dClutter Social (quoteApply has status rejected) */
        $this->assertEquals($quoteApply->getProduct()->getCode(), Product::BORCONS3);
        /*
         * Now we should return the pending status to call_credit_api_calls record and quote_apply
         */
        static::$em->clear();
        $callCredit = static::$em->getRepository(CallCreditAPICalls::class)->find($callCreditId);
        $callCredit
            ->setStatus(CallCreditAPICalls::STATUS_PENDING)
            ->setResponseDate(null)
            ->setResponseXML(null);
        static::$em->flush();
        static::$em->clear();
        $quoteApply = static::$em->getRepository(QuoteApply::class)->find($callCredit->getQuoteApply()->getId());
        $quoteApply
            ->setStatus(QuoteApply::STATUS_PENDING);
        static::$em->flush();
        static::$em->clear();

        /**
         * Check SUCCESS response with score = 500 (refer)
         */
        $this->callReportResponce(CallCreditAPICalls::STATUS_SUCCESS, 500);

        /*
         * CallCreditApiCalls should stay in the pending status
         */
        $this->assertEquals($callCredit->getStatus(), CallCreditAPICalls::STATUS_PENDING);
        /*
         * QuoteApply should change the status to refer
         */
        static::$em->clear();
        $quoteApply = static::$em->getRepository(QuoteApply::class)->find($callCredit->getQuoteApply()->getId());
        $this->assertEquals($quoteApply->getStatus(), QuoteApply::STATUS_REFER);
        /** After successful response CallCredit API, quoteApply got new product:
         * if quoteApply approved - dClutter Premium
         *               refer    - dClutter Standard
         *               reject   - dClutter Social
         *
         * Now product should be dClutter Standard (quoteApply has status refer)
         */
        $this->assertEquals($quoteApply->getProduct()->getCode(), Product::BORCONS2);

        /*
         * LoanApply should be null
         */
        $loanApply = static::$em->getRepository(LoanApply::class)->findOneBy(['quoteApply' => $callCredit->getQuoteApply()]);
        $this->assertNull($loanApply);
        /*
         * Now we should return the pending status to call_credit_api_calls quote_apply
         */

        static::$em->clear();
        $callCredit = static::$em->getRepository(CallCreditAPICalls::class)->find($callCreditId);
        $callCredit
            ->setStatus(CallCreditAPICalls::STATUS_PENDING)
            ->setResponseDate(null)
            ->setResponseXML(null);
        static::$em->flush();
        static::$em->clear();
        $quoteApply = static::$em->getRepository(QuoteApply::class)->find($callCredit->getQuoteApply()->getId());

        $quoteApply
            ->setStatus(QuoteApply::STATUS_PENDING);
        static::$em->flush();
        static::$em->clear();


        /**
         * Check that UserProduct has not been created yet
         */
        $userProduct = static::$kernel->getContainer()->get('whs.manager.user_product_manager')->getUserProduct($quoteApply->getUser(), $quoteApply->getProduct());
        $this->assertNull($userProduct);
        /**
         * Check SUCCESS response with score = 501
         */
        $this->callReportResponce();
        /**
         * Check that CallCreditAPICall has been processed right
         */
        $callCredit = static::$em->getRepository(CallCreditAPICalls::class)->find($callCreditId);
        $this->assertEquals(CallCreditAPICalls::getTypes()[$callCredit->getType()], CallCreditAPICalls::getTypes()[CallCreditAPICalls::TYPE_CALLREPORT]);
        /**
         * Purpose must be Quotation (PURPOSE_QS)
         */
        $this->assertEquals(CallCreditAPICalls::getPurposes()[CallCreditAPICalls::PURPOSE_QS], CallCreditAPICalls::getPurposes()[$callCredit->getPurpose()]);
        $this->assertEquals($callCredit->getStatus(), CallCreditAPICalls::STATUS_SUCCESS);
        $this->assertEquals($callCredit->getQuoteApply()->getStatus(), QuoteApply::STATUS_APPROVED);
        /**
         * For QuoteApply status changing doctrine event is listened by AppBundle\Doctrine\EventListener\QuoteApplyChange which generates system event 'quoteapply.updated.status. AppBundle/Event/EntityLifecycleSubscriber listens this event and desides create new LoanApply
         */
        $loanApply = static::$em->getRepository(LoanApply::class)->findOneBy(['quoteApply' => $callCredit->getQuoteApply()]);
        /**
         * Check that userProduct not null
         */
        $quoteApply = $callCredit->getQuoteApply();
        $userProduct = static::$kernel->getContainer()->get('whs.manager.user_product_manager')->getUserProduct($quoteApply->getUser(), $quoteApply->getProduct());
        $this->assertNotNull($userProduct);

        $this->assertEquals($loanApply->getStatus(), LoanApply::STATUS_DRAFT);
        /** After successful response CallCredit API, quoteApply got new product:
         * if quoteApply approved - dClutter Premium
         *               refer    - dClutter Standard
         *               reject   - dClutter Social
         *
         * Now product should be dClutter Premium (quoteApply has status approved)
         */
        $this->assertEquals($quoteApply->getProduct()->getCode(), Product::BORCONS1);

        return $loanApply->getId();
    }

    /**
     * @param $callCreditId
     * @depends testCallCreditCreating
     */
    public function testUnderwritingResults($callCreditId)
    {
        /** @var CallCreditAPICalls $callCredit */
        $callCredit = static::$em->getRepository(CallCreditAPICalls::class)->find($callCreditId);
        /**
         * Get Last underwritingResult as from UnderwritingResultManager (function
         * getUnderwritingResultForQuoteApply) for quoteApply in callCreditApiCalls object
         *
         */
        $underwritingResult = static::$client->getContainer()
            ->get('whs.manager.underwriting_result_manager')->getUnderwritingResultForQuoteApply($callCredit->getQuoteApply());

        /** This values for check data in underwritingResult object from "Interest calculation excel file" */
        $calculatedValues = array(
            'loan_amount'  => '5000',
            'monthly'      => '439.77',
            'total_repaid' => '5277.19',
            'apr'          => '8.33',
            'bonus_amount' => '48.72',
            'term'         => '12'
        );

        /** Check if underwritingResults calculated correctly */
        $this->assertEquals($calculatedValues['loan_amount'], $underwritingResult['loan_amount'], 'Loan amount have been calculated incorrectly');
        $this->assertEquals($calculatedValues['monthly'], $underwritingResult['monthly'], 'Monthly payment have been calculated incorrectly');
        $this->assertEquals($calculatedValues['total_repaid'], $underwritingResult['total_repaid'], 'Total to be repaid have been calculated incorrectly');
        $this->assertEquals($calculatedValues['apr'], $underwritingResult['apr'], 'APR have been calculated incorrectly');
        $this->assertEquals($calculatedValues['bonus_amount'], $underwritingResult['bonus_amount'], 'Bonus amount have been calculated incorrectly');
        $this->assertEquals($calculatedValues['term'], $underwritingResult['term'], 'Term have been calculated incorrectly');
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Borrower LoanApply flow
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @param $loanApplyId
     * @depends testLoanApplyCreate
     */
    public function testLoanFlow($loanApplyId)
    {
//      test dashboard
        $this->send('/user/dashboard');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $response = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertEquals('success', $response['status']);
        $this->assertTrue(is_array($response['content']['quotes']));
        $this->assertTrue(is_array($response['content']['loans']));
        $this->assertTrue(is_array($response['content']['offers']));

        //test POST /quote_underwriting_details
        $loanApply = static::$em->getRepository('AppBundle:LoanApply')->find($loanApplyId);
        static::$content = json_encode(['id' => $loanApply->getQuoteApply()->getId()]);
        $this->send('/user/quote_underwriting_details', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());


        //Test that now user can't change(add new) QuoteTerm
        //because there is no drafted QuoteApply
        $params = [
            'loan_amount' => 5000,
            'loan_term'   => 12
        ];
        static::$content = json_encode($params);
        $this->send('/user/add_quote_term', [], 'POST');
        $this->assertNotTrue(static::$client->getResponse()->isSuccessful());

        //test GET quote_details_for_draft_loan
        $this->send('/user/quote_details_for_draft_loan');
        $this->assertTrue(static::$client->getResponse()->isOk());

        //test Addresses
        $this->send('/user/address');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        /*
         * there will be at least 1 address
         */
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertGreaterThan(0, count($content), 'there will be at least 1 address');

//        test update details. OK if empty details
        $this->send('/user/update_details', [], 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

//        Also OK if fully filled details
        $params = [
            'middle_name' => 'MiddleName',
            'maiden_name' => 'MaidenName',
            'gender'      => 'm',
        ];
        $this->send('/user/update_details', $params, 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        /*
         * empty communications
         */
        $this->send('/user/investor_communications', [], 'GET');
        $response = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $this->arrayHasKey('content', $response);
        $this->arrayHasKey('id', $response['content'][0]);
        $this->arrayHasKey('primary', $response['content'][0]);
        $this->assertEquals(Communication::TYPE_EMAIL, $response['content'][0]['type']);
        $this->assertEquals(static::$fake_email, $response['content'][0]['value']);
        /*
         *  add communications
         */
        $params = [
            'communications' => [
                /*
                 * mobile phone
                 */
                '200' => '+000000000000',
                /*
                 * home phone
                 */
                '300' => '77777777',
                /*
                 * work phone
                 */
                '400' => '123123123'
            ]
        ];
        static::$content = json_encode($params);
        $this->send('/user/add_investor_contacts', array(), 'POST');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());

        /*
         * our communications
         */
        $user = static::$em->getRepository('AppBundle:User')->findOneByUsername(static::$fake_email);
        $userCommunicationManager = static::$client->getContainer()->get('whs.manager.user_communication_manager');
        $userCommunications = $userCommunicationManager->getCommunicationList($user);
        $response = json_decode(static::$client->getResponse()->getContent(), true);
        /** @var UserCommunication $userCommunication */
        foreach ($userCommunications as $userCommunication) {
            /** check communication value, type, and verified */
            $this->assertTrue(in_array($userCommunication, $response['content']));
        }

        /**
         * check that verified code is null
         * @var UserCommunication $userMobile
         */
        $userMobile = static::$kernel->getContainer()->get('whs.manager.user_communication_manager')->getMobileCommunicationForUser(static::$session->getUser());
        $this->assertNull($userMobile->getVerificationCode());

//        get empty employments is successful
        $this->send('/user/employments');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $employments = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals(0, count($employments));

//        get empty bank details is successful
        $this->send('/user/bank_details');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());

//        test UserEmployments
        $this->send('/user/employments', array(), 'POST');
        $response = json_decode(static::$client->getResponse()->getContent());
        /** check error codes */
        $this->assertEquals(Response::HTTP_BAD_REQUEST,
            static::$client->getResponse()->getStatusCode(),
            'POST: /user/employments ' . static::$client->getResponse()->getStatusCode());
        $this->assertEquals($response->content->occMonthStarted->code, 'userEmployment.occMonthStarted.isBlank');
        $this->assertEquals($response->content->businessFirmName->code, 'userEmployment.businessFirmName.isBlank');
        $this->assertEquals($response->content->industry->code, 'userEmployment.industry.isBlank');
        $this->assertEquals($response->content->occPostalCode->code, 'userEmployment.postalCode.isBlank');
        $this->assertEquals($response->content->occCountry->code, 'userEmployment.occCountry.isBlank');
        $this->assertEquals($response->content->occStatus->code, 'userEmployment.occStatus.isBlank');
        $this->assertEquals($response->content->jobDescription->code, 'userEmployment.jobDescription.isBlank');
        $this->assertEquals($response->content->profQual->code, 'userEmployment.profQual.isBlank');
        $this->assertEquals($response->content->currentStatus->code, 'userEmployment.currentStatus.isBlank');

        $params = array(
            'occ_postal_code' => 'TEST'
        );
        static::$content = json_encode($params);
        $this->send('/user/employments', array(), 'POST');
        $response = json_decode(static::$client->getResponse()->getContent());
        /** check postal_code validation */
        $this->assertEquals($response->content->occPostalCode->code, 'userEmployment.postalCode.notValid');

        /** check with fake occ_month_started and ended data */
        $params = array(
            'occ_month_started'  => '20160101',
            'occ_month_ended'    => '20150101',
            'occ_postal_code'    => 'SW1A 0PW',
            'business_firm_name' => 'test firm name',
            'industry'           => 'test industry',
            'occ_country'        => 'GB',
            'occ_status'         => 'director',
            'job_description'    => 'director director',
            'prof_qual'          => 'test',
            'current_status'     => false,
        );
        static::$content = json_encode($params);
        $this->send('/user/employments', array(), 'POST');
        /** check error code, because month ended will be greater then month started */
        $this->assertEquals(Response::HTTP_BAD_REQUEST, static::$client->getResponse()->getStatusCode());

        $params['occ_month_started'] = '20140101';
        static::$content = json_encode($params);
        $this->send('/user/employments', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        $params['occ_month_ended'] = null;
        $params['occ_month_started'] = '20150202';
        $params['current_status'] = true;
        static::$content = json_encode($params);
        $this->send('/user/employments', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());


        /** check update method with fake id */
        $params['id'] = '-1';
        static::$content = json_encode($params);
        $this->send('/user/employments', [], 'POST');
        $this->assertNotTrue(static::$client->getResponse()->isSuccessful());

        /** TEST GET 2 added employments */
        static::$content = '';
        /** get UserEmployments */
        $this->send('/user/employments');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        $employments = json_decode(static::$client->getResponse()->getContent())->content;
        $employmentAmount = count($employments);
        // now there will be 2 employments
        $this->assertEquals(2, $employmentAmount);

        //test update
        $updatedDescription = 'updated description';
        $params = array(
            'id'              => $employments[0]->id,
            'job_description' => $updatedDescription,

        );
        static::$content = json_encode($params);
        $this->send('/user/employments', array(), 'POST');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        /** get after update */
        $this->send('/user/employments');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        $employments = json_decode(static::$client->getResponse()->getContent())->content;
        $employmentsAmount = count($employments);
        // will have 2 employments and updated field
        $this->assertEquals(2, $employmentsAmount);
        $this->assertEquals($updatedDescription, $employments[0]->job_description);

        /** try delete UserEmployment */
        $params = array('id' => json_decode(static::$client->getResponse()->getContent())->content[0]->id);
        static::$content = json_encode($params);
        $this->send('/user/employments', [], 'DELETE');
        $this->assertEquals(Response::HTTP_NO_CONTENT, static::$client->getResponse()->getStatusCode());

        /** get UserEmployments */
        $this->send('/user/employments');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        // we delete one, so amount will be less for one
        $this->assertEquals($employmentAmount - 1, count(json_decode(static::$client->getResponse()->getContent())->content));

        //add 5 years old
        $params = array(
            'occ_month_started'  => '20110101',
            'occ_month_ended'    => '20150101',
            'occ_postal_code'    => 'SW1A 0PW',
            'business_firm_name' => 'test firm name',
            'industry'           => 'test industry',
            'occ_country'        => 'GB',
            'occ_status'         => 'director',
            'job_description'    => 'director director',
            'prof_qual'          => 'test',
            'current_status'     => true,
        );
        static::$content = json_encode($params);
        $this->send('/user/employments', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());

//        test income records
        //GET before POST
        $this->send('/user/user_income');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertNotEmpty($content->total_income_gross_ann);
        $this->assertNotEmpty($content->main_income_source);

        //POST empty user income will fail
        static::$content = json_encode([]);
        $this->send('/user/user_income', [], 'POST');
        $this->assertNotTrue(static::$client->getResponse()->isOk());

        //POST correct user income
        $params = [
            'total_income_gross_ann' => 123123123,
            'main_income_source'     => 'IncomeSource',
        ];
        static::$content = json_encode($params);
        $this->send('/user/user_income', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());

        // test that now get stored values from POST /user/user_income
        $this->send('/user/user_income');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals($params['total_income_gross_ann'], $content->content->total_income_gross_ann);
        $this->assertEquals($params['main_income_source'], $content->content->main_income_source);

        /**
         * we need sleep 1 second to check that the old and the new records have different created_at
         */
        sleep(1);
        //POST correct user income
        $params = [
            'total_income_gross_ann' => 123123124,
            'main_income_source'     => 'IncomeSource',
        ];
        static::$content = json_encode($params);
        $this->send('/user/user_income', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());

        // test that now get stored values from POST /user/user_income
        $this->send('/user/user_income');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals($params['total_income_gross_ann'], $content->content->total_income_gross_ann);
        $this->assertEquals($params['main_income_source'], $content->content->main_income_source);

// test BankDetails

        //GET empty
        $this->send('/user/bank_details');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $this->assertEquals([], json_decode(static::$client->getResponse()->getContent())->content);

        //todo: POST wrong
        //POST correct
        $params = [
            'bank_sort_code'      => 234344,
            'bank_account_number' => 456456456,
        ];
        static::$content = json_encode($params);
        $this->send('/user/bank_details', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $this->assertEquals([], json_decode(static::$client->getResponse()->getContent())->content);

        //GET non empty
        $this->send('/user/bank_details');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals($params['bank_sort_code'], $content->bank_sort_code);
        $this->assertEquals($params['bank_account_number'], $content->bank_account_number);

//        test UserHousehold
        //get when empty
        $this->send('/user/household');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());

        //post empty
        $params = [];
        static::$content = json_encode($params);
        $this->send('/user/household', [], 'POST');
        $this->assertNotTrue(static::$client->getResponse()->isOk());
        //check validation errors
        $content = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals($content->content->monthlyRent->code, 'userHousehold.monthlyRent.isBlank');
        $this->assertEquals($content->content->monthlyUtilities->code, 'userHousehold.monthlyUtilities.isBlank');
        $this->assertEquals($content->content->monthlyTravel->code, 'userHousehold.monthlyTravel.isBlank');
        $this->assertEquals($content->content->monthlyFood->code, 'userHousehold.monthlyFood.isBlank');
        $this->assertEquals($content->content->monthlySavings->code, 'userHousehold.monthlySavings.isBlank');
        $this->assertEquals($content->content->monthlyEntertainment->code, 'userHousehold.monthlyEntertainment.isBlank');

        $params = [
            'monthly_rent'          => 12.12,
            'monthly_utilities'     => 123.15,
            'monthly_travel'        => 3333,
            'monthly_food'          => 2222.22,
            'monthly_savings'       => 4444.44,
            'monthly_entertainment' => 555.55,
        ];
        static::$content = json_encode($params);
        $this->send('/user/household', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());

//        test depends

//        GET empty
        $this->send('/user/dependent');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent());
        $this->assertEmpty($content->content);

//        POST empty
        static::$content = json_encode([]);
        $this->send('/user/dependent', [], 'POST');
        $this->assertNotTrue(static::$client->getResponse()->isSuccessful());
        $content = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals($content->content->firstName->code, 'userDependent.firstName.isBlank');
        $this->assertEquals($content->content->birthdate->code, 'userDependent.birthdate.isBlank');

//        POST with wrong date
        $params = [
            'first_name' => 'FirstName',
            'birthdate'  => 'notDateInFormat_Ymd',
        ];
        static::$content = json_encode($params);
        $this->send('/user/dependent', [], 'POST');
        $this->assertNotTrue(static::$client->getResponse()->isSuccessful());
        $content = json_decode(static::$client->getResponse()->getContent());
        $this->assertContains('Invalid datetime', $content->content->message);

//        POST with correct params
        $params = [
            'first_name' => 'FirstName',
            'birthdate'  => '20161230',
        ];
        static::$content = json_encode($params);
        $this->send('/user/dependent', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());

//        GET non empty
        $this->send('/user/dependent');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals(1, count($content->content));
        $dependentId = $content->content[0]->id;

//        GET by id
        static::$content = [];
        $this->send('/user/dependent', ['id' => $dependentId]);
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent());
        $this->assertNotEmpty($content->content);

//        PATCH by id
        $params = [
            'first_name' => 'FirstName updated',
            'birthdate'  => '20151230',
        ];
        static::$content = json_encode($params);
        $this->send('/user/dependent/' . $dependentId, [], 'PATCH');
        $this->assertTrue(static::$client->getResponse()->isOk());

//        DELETE
        static::$content = json_encode(['id' => $dependentId]);
        $this->send('/user/dependent', [], "DELETE");
        $this->assertTrue(static::$client->getResponse()->isOk());

//        add two and then delete all
        $params = [
            'first_name' => 'FirstName',
            'birthdate'  => '20161230',
        ];
        static::$content = json_encode($params);
        $this->send('/user/dependent', [], 'POST');
        $this->send('/user/dependent', [], 'POST');

        //delete all
        static::$content = json_encode([]);
        $this->send('/user/dependents', [], "DELETE");
        $this->assertTrue(static::$client->getResponse()->isOk());

        //check that all deleted
        $this->send('/user/dependent');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent());
        $this->assertEmpty($content->content);

        //POST with correct params
        $params = [
            'first_name' => 'FirstName',
            'birthdate'  => '20161230',
        ];
        static::$content = json_encode($params);
        $this->send('/user/dependent', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());

//        UserCreditlines
        // GET empty
        $this->send('/user/credit_line');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertEquals(0, count($content['content']));

        //GET creditline types
        $this->send('/user/credit_line/types');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertEquals(count(UserCreditline::getCreditLineTypesMap()), count($content['content']));

        // POST empty
        static::$content = json_encode([]);
        $this->send('/user/credit_line', [], 'POST');
        $this->assertNotTrue(static::$client->getResponse()->isSuccessful());
        $content = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals('userCreditLine.type.isBlank', $content->content->type->code);
        $this->assertEquals('userCreditLine.name.isBlank', $content->content->name->code);
        $this->assertEquals('userCreditLine.creditLimit.isBlank', $content->content->creditLimit->code);
        $this->assertEquals('userCreditLine.balanceInput.isBlank', $content->content->balanceInput->code);
        $this->assertEquals('userCreditLine.apr.isBlank', $content->content->apr->code);
        $this->assertEquals('userCreditLine.monthlyPayment.isBlank', $content->content->monthlyPayment->code);

        // POST first valid
        $params = [
            'type'            => 200,
            'name'            => 'creditor name 1',
            'credit_limit'    => 10000,
            'balance_input'   => 8500,
            'apr'             => 10,
            'monthly_payment' => 500,
        ];
        static::$content = json_encode($params);
        $this->send('/user/credit_line', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());

        // POST second valid
        $params = [
            'type'            => 300,
            'name'            => 'creditor name 2',
            'credit_limit'    => 20000,
            'balance_input'   => 900,
            'apr'             => 15,
            'monthly_payment' => 800,
        ];
        static::$content = json_encode($params);
        $this->send('/user/credit_line', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());

        //POST third valid
        $params = [
            'type'            => 400,
            'name'            => 'creditor name 3',
            'credit_limit'    => 20000,
            'balance_input'   => 10000,
            'apr'             => 5,
            'monthly_payment' => 600,
        ];
        static::$content = json_encode($params);
        $this->send('/user/credit_line', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());

        //POST forth valid
        $params = [
            'type'            => 400,
            'name'            => 'creditor name 3',
            'credit_limit'    => 30000,
            'balance_input'   => 25000,
            'apr'             => 5,
            'monthly_payment' => 600,
        ];
        static::$content = json_encode($params);
        $this->send('/user/credit_line', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());

        // GET 4 creditlines
        $this->send('/user/credit_line');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertEquals(4, count($content['content']));

        //DELETE first creditline
        static::$content = json_encode(['id' => $content['content'][0]['id']]);
        $this->send('/user/credit_line', [], 'DELETE');
        $this->assertTrue(static::$client->getResponse()->isOk());

        // now GET will return 3 creditlines
        $this->send('/user/credit_line');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $creditlinesArr = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertEquals(3, count($creditlinesArr['content']));

//        DeClutters
        //Get empty
        $this->send('/user/declutters');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertEquals(0, count($content['content']));

        //POST empty
        static::$content = json_encode([]);
        $this->send('/user/declutters', [], 'POST');
        $this->assertNotTrue(static::$client->getResponse()->isSuccessful());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals('declutter.isEmpty', $content->declutter->code);

        //Post 1 with amount < 1000 (unvalid)
        $params = [
            ['user_creditline' => ['id' => $creditlinesArr['content'][0]['id']], 'need_to_close' => 1],
        ];
        static::$content = json_encode($params);
        $this->send('/user/declutters', [], 'POST');
        $this->assertNotTrue(static::$client->getResponse()->isSuccessful());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals('declutters.amount.notInRange', $content->declutter->code);

        //Post 2 with amount > 30000 (unvalid)
        $params = [
            ['user_creditline' => ['id' => $creditlinesArr['content'][1]['id']], 'need_to_close' => 1],
            ['user_creditline' => ['id' => $creditlinesArr['content'][2]['id']], 'need_to_close' => 0],
        ];
        static::$content = json_encode($params);
        $this->send('/user/declutters', [], 'POST');
        $this->assertNotTrue(static::$client->getResponse()->isSuccessful());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals('declutters.amount.notInRange', $content->declutter->code);

        //Post 2 valid
        $params = [
            ['user_creditline' => ['id' => $creditlinesArr['content'][0]['id']], 'need_to_close' => 1],
            ['user_creditline' => ['id' => $creditlinesArr['content'][2]['id']], 'need_to_close' => 0],
        ];
        static::$content = json_encode($params);
        $this->send('/user/declutters', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());

        //Post with wrong creditline_id
        $params = [
            ['user_creditline' => ['id' => -1], 'need_to_close' => 1],
            ['user_creditline' => ['id' => -2], 'need_to_close' => 0],
        ];
        static::$content = json_encode($params);
        $this->send('/user/declutters', [], 'POST');
        $this->assertNotTrue(static::$client->getResponse()->isOk());

        // now will get 2
        $this->send('/user/declutters');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent(), true);
        $this->assertEquals(2, count($content['content']));

        // test GET /all_terms_payment_values
        $params = [
            'loan_amount'   => '10000',
            'interest_rate' => '10',
        ];
        $this->send('/compute/all_terms_payment_values', $params);
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent(), true)['content'];
        //array will contain 49 items (for 12 to 60 terms inclusive)
        $this->assertEquals(49, count($content));

//        Submit LoanApply
        // check that before submit QuoteApply in status APPROVED
        // and LoanApply in status DRAFTED
        static::$em->clear();
        $loanApply = static::$em->getRepository('AppBundle:LoanApply')->find($loanApplyId);
        $this->assertEquals(LoanApply::STATUS_DRAFT, $loanApply->getStatus());
        $this->assertEquals(QuoteApply::STATUS_APPROVED, $loanApply->getQuoteApply()->getStatus());

        static::$content = json_encode(['loan_amount' => '2000.11', 'loan_term' => '12', 'loan_apr' => '7.65', 'loan_monthly' => '173.62']);
        $this->send('/user/submit_loan_application', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());

        // check that after submit QuoteApply in status APPROVED
        // and LoanApply in status Submitted
        static::$em->clear();
        $loanApply = static::$em->getRepository('AppBundle:LoanApply')->find($loanApplyId);
        $this->assertEquals(LoanApply::STATUS_SUBMITTED, $loanApply->getStatus());
        $this->assertEquals(QuoteApply::STATUS_ACTIVE, $loanApply->getQuoteApply()->getStatus());

        /** After loan submit three CallCredit API request should be init */
        $quoteApply = $loanApply->getQuoteApply();
        /** @var CallCreditAPICalls $callCredit */
        $callCredit = static::$em->getRepository(CallCreditAPICalls::class)->getCallCreditForLoanByQuoteAndType($quoteApply, CallCreditAPICalls::TYPE_CALLREPORT);

        $this->assertNotNull($callCredit);
        /** Purpose should be PURPOSE_CA (Credit) */
        $this->assertEquals(CallCreditAPICalls::getPurposes()[CallCreditAPICalls::PURPOSE_QS], CallCreditAPICalls::getPurposes()[$callCredit[0]->getPurpose()]);

        /** @var CallCreditAPICalls $callCreditAR */
        $callCreditAR = static::$em->getRepository(CallCreditAPICalls::class)->getCallCreditForLoanByQuoteAndType($quoteApply, CallCreditAPICalls::TYPE_AFFORDABILITY);
        $this->assertNotNull($callCreditAR);
        /** Purpose should be PURPOSE_AR (Affordability) */
        $this->assertEquals(CallCreditAPICalls::getPurposes()[CallCreditAPICalls::PURPOSE_QS], CallCreditAPICalls::getPurposes()[$callCreditAR[0]->getPurpose()]);

        /** @var CallCreditAPICalls $callCreditCV */
        $callCreditCV = static::$em->getRepository(CallCreditAPICalls::class)->getCallCreditForLoanByQuoteAndType($quoteApply, CallCreditAPICalls::TYPE_CALLVALIDATE);
        $this->assertNotNull($callCreditCV);
        /** Purpose should be PURPOSE_BORROWER (Affordability) */
        $this->assertEquals(CallCreditAPICalls::getPurposes()[CallCreditAPICalls::PURPOSE_QS], CallCreditAPICalls::getPurposes()[$callCreditCV[0]->getPurpose()]);

        return $loanApplyId;
    }

    /**
     * @param $loanApplyId
     * @depends testLoanFlow
     */
    public function testUpdateRestriction($loanApplyId)
    {
//        try update userEmployments
        //try POST
        $params = array(
            'occ_month_started'  => '20160101',
            'occ_month_ended'    => '20170101',
            'occ_postal_code'    => 'SW1A 0PW',
            'business_firm_name' => 'test firm name',
            'industry'           => 'test industry',
            'occ_country'        => 'GB',
            'occ_status'         => 'director',
            'job_description'    => 'director director',
            'prof_qual'          => 'test',
            'current_status'     => false,
        );
        static::$content = json_encode($params);
        $this->send('/user/employments', array(), 'POST');
        $this->assertEquals(Response::HTTP_FORBIDDEN, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals('loan.not.drafted', $content->code);

        // try GET (OK)
        $this->send('/user/employments');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $employments = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertGreaterThan(0, count($employments));


        //try update
        $params['id'] = $employments[0]->id;
        static::$content = json_encode($params);
        $this->send('/user/employments', [], 'POST');
        $this->assertEquals(Response::HTTP_FORBIDDEN, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals('loan.not.drafted', $content->code);

        //try DELETE
        $params = ['id' => $employments[0]->id];
        static::$content = json_encode($params);
        $this->send('/user/employments', [], 'DELETE');
        $this->assertEquals('loan.not.drafted', $content->code);

//        try update userHousehold
        //try GET (OK)
        $this->send('/user/household');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());

        //try POST
        $params = [
            'monthly_rent'          => 12.12,
            'monthly_utilities'     => 123.15,
            'monthly_travel'        => 3333,
            'monthly_food'          => 2222.22,
            'monthly_savings'       => 4444.44,
            'monthly_entertainment' => 555.55,
        ];
        static::$content = json_encode($params);
        $this->send('/user/household', [], 'POST');
        $this->assertEquals(Response::HTTP_FORBIDDEN, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals('loan.not.drafted', $content->code);

//      try update creditLine
        // GET
        $this->send('/user/credit_line');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $creditlines = json_decode(static::$client->getResponse()->getContent(), true)['content'];
        $this->assertGreaterThan(0, count($creditlines));

        // calculated fields must present
        $this->assertArrayHasKey('credit_balance_now', $creditlines[0]);
        $this->assertArrayHasKey('total_repaid', $creditlines[0]);
        $this->assertArrayHasKey('total_interest', $creditlines[0]);
        $this->assertArrayHasKey('total_months_to_clear', $creditlines[0]);

        // POST valid
        $params = [
            'type'            => 200,
            'name'            => 'creditor name',
            'credit_limit'    => 10000,
            'balance_input'   => 8500,
            'apr'             => 10,
            'monthly_payment' => 500,
        ];
        static::$content = json_encode($params);
        $this->send('/user/credit_line', [], 'POST');
        $this->assertEquals(Response::HTTP_FORBIDDEN, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals('loan.not.drafted', $content->code);

        //try DELETE
        static::$content = json_encode(['id' => $creditlines[0]['id']]);
        $this->send('/user/credit_line', [], 'DELETE');
        $this->assertEquals(Response::HTTP_FORBIDDEN, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals('loan.not.drafted', $content->code);

//        DeClutters
        //Get without loan_apply_id will be empty (because loan is submitted)
        $this->send('/user/declutters', []);
        $this->assertTrue(static::$client->getResponse()->isOk());
        $declutters = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals(0, count($declutters));

        //Get with loan_apply_id will return some ones
        $this->send('/user/declutters', ['loan_apply_id' => $loanApplyId]);
        $this->assertTrue(static::$client->getResponse()->isOk());
        $declutters = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertGreaterThan(0, count($declutters));

        //Post
        $params = [
            ['user_creditline' => ['id' => $creditlines[0]['id']], 'need_to_close' => 1],
        ];
        static::$content = json_encode($params);
        $this->send('/user/declutters', [], 'POST');
        $this->assertEquals(Response::HTTP_FORBIDDEN, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals('loan.not.drafted', $content->code);

//        Dependents
        //GET
        $this->send('/user/dependent');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $dependents = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertGreaterThan(0, count($dependents));

        //POST with correct params
        $params = [
            'first_name' => 'FirstName',
            'birthdate'  => '20161230',
        ];
        static::$content = json_encode($params);
        $this->send('/user/dependent', [], 'POST');
        $this->assertEquals(Response::HTTP_FORBIDDEN, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals('loan.not.drafted', $content->code);

        //GET by id
        static::$content = [];
        $this->send('/user/dependent', ['id' => $dependents[0]->id]);
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent());
        $this->assertNotEmpty($content->content);

        //PATCH by id
        $this->send('/user/dependent/' . $dependents[0]->id, [], 'PATCH');
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals(Response::HTTP_FORBIDDEN, static::$client->getResponse()->getStatusCode());
        $this->assertEquals('loan.not.drafted', $content->code);

        //DELETE by id
        static::$content = json_encode(['id' => $dependents[0]->id]);
        $this->send('/user/dependent', [], "DELETE");
        $this->assertEquals(Response::HTTP_FORBIDDEN, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals('loan.not.drafted', $content->code);

        //DELETE all
        static::$content = json_encode([]);
        $this->send('/user/dependents', [], "DELETE");
        $this->assertEquals(Response::HTTP_FORBIDDEN, static::$client->getResponse()->getStatusCode());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals('loan.not.drafted', $content->code);
    }
}