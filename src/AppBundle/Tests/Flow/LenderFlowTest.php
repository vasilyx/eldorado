<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 17.01.17
 * Time: 15:32
 */

namespace AppBundle\Tests\Flow;


use AppBundle\Command\CallValidateSendCommand;
use AppBundle\Entity\CallCreditAPICalls;
use AppBundle\Entity\Event;
use AppBundle\Entity\InvestOffer;
use AppBundle\Entity\InvestOfferProcess;
use AppBundle\Entity\InvestOfferSub;
use AppBundle\Entity\Product;
use AppBundle\Entity\QuoteApply;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCommunication;
use AppBundle\Entity\WorldpayHtmlTransaction;
use AppBundle\Entity\WorldpayTransaction;
use AppBundle\Event\TransactionEvents;
use AppBundle\Tests\AppTest;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class LenderFlowTest extends AppTest
{
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        static::signUp('lender');
    }

    public function getWorldpayParametersJson($cartId, $transStatus = WorldpayHtmlTransaction::TRANS_STATUS_SUCCESS)
    {
        return '{"region": "Cambridgeshire",
      "authAmountString": "&#163;10.99",
      "_SP_charEnc": "UTF-8",
      "desc": "",
      "tel": "0870 366 1233",
      "MC_redirectUrl": "/redirecdt",
      "address1": "Worldpay Ltd",
      "countryMatch": "S",
      "cartId": "' . $cartId . '",
      "address2": "270-289 The Science Park",
      "address3": "Milton Road",
      "lang": "en",
      "callbackPW": "your_password",
      "rawAuthCode": "A",
      "transStatus": "' . $transStatus . '",
      "amountString": "&#163;10.99",
      "authCost": "10.99",
      "currency": "GBP",
      "installation": "211616",
      "amount": "10.99",
      "countryString": "United Kingdom",
      "displayAddress": "Worldpay Ltd\n270-289 The Science Park\nMilton Road\nCambridge\nCambridgeshire",
      "transTime": "1331631533596",
      "name": "CARD HOLDER",
      "testMode": "100",
      "routeKey": "VISA-SSL",
      "ipAddress": "62.249.232.76",
      "fax": "",
      "rawAuthMessage": "cardbe.msg.authorised",
      "instId": "211616",
      "AVS": "0001",
      "compName": "Admin - Company Name",
      "authAmount": "10.99",
      "postcode": "CB4 0WE",
      "cardType": "Visa",
      "cost": "10.99",
      "authCurrency": "GBP",
      "country": "GB",
      "charenc": "UTF-8",
      "email": "support@worldpay.com",
      "address": "Worldpay Ltd\n270-289 The Science Park\nMilton Road\nCambridge\nCambridgeshire",
      "transId": "129170095",
      "msgType": "authResult",
      "town": "Cambridge",
      "authMode": "AB"}';
    }

    /**
     * This method is created to decouple call validate algorithm from test logic
     * @param $testMode
     */
    public function callValidateResponce($testMode = CallCreditAPICalls::STATUS_SUCCESS)
    {
        $application = new Application(static::$kernel);
        $application->add(new CallValidateSendCommand());

        $command = $application->find('app:call_validate_send');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'     => $command->getName(),
            '--test-mode' => CallCreditAPICalls::STATUS_SUCCESS
        ]);
    }

    /**
     * First step after registration
     */
    public function testFirstStep()
    {
        /*
         * empty address
         */
        $this->send('/user/address', array(), 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $this->assertEmpty(json_decode(static::$client->getResponse()->getContent()));
        /*
         * add address
         */
        $params = array(
            'postal_code' => 'W1J 6BQ',
            'country'     => 'GB',
            'city'        => 'London',
            'line1'       => 'Berkeley Square House',
            'moved'       => '19901010',
            'street'      => 'Berkeley Square'
        );
        static::$content = json_encode($params);
        $this->send('/user/address', array(), 'POST');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        /*
         * our address
         */
        $this->send('/user/address', array(), 'GET');
        $result = json_decode(static::$client->getResponse()->getContent(), true)[0];
        $this->assertArrayHasKey('moved', $result);
        $this->assertEquals($result['moved'], '19901010');

        /*
         * empty communications
         */
        $this->send('/user/investor_communications', [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $this->assertEmpty(json_decode(static::$client->getResponse()->getContent())->content);
        /*
         *  add communications
         */
        $params = [
            'communications' => [
                /*
                 * mobile phone
                 */
                '200' => '+000000000000',
                /*
                 * home phone
                 */
                '300' => '77777777',
                /*
                 * work phone
                 */
                '400' => '123123123'
            ]
        ];
        static::$content = json_encode($params);
        $this->send('/user/add_investor_contacts', array(), 'POST');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());

        /*
         * our communications
         */
        $this->send('/user/investor_communications', [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $this->assertEquals(json_decode(static::$client->getResponse()->getContent())->content[0]->value, '+000000000000');

        /**
         * check that verified code is null
         * @var UserCommunication $userMobile
         */
        $userMobile = static::$kernel->getContainer()->get('whs.manager.user_communication_manager')->getMobileCommunicationForUser(static::$session->getUser());
        $this->assertNull($userMobile->getVerificationCode());

        /*
         * empty preferences
         */
        $this->send('/user/user_preferences', [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $response = json_decode(static::$client->getResponse()->getContent());
        $this->assertFalse($response->content->receive_comms);

        /*
         * add preferences
         */
        $params = array(
            'receive_comms' => true
        );
        static::$content = json_encode($params);
        $this->send('/user/user_preferences', [], 'PATCH');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $response = json_decode(static::$client->getResponse()->getContent());
        $this->assertTrue($response->content->receive_comms);

        /**
         * our preferences
         */
        $this->send('/user/user_preferences', [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $this->assertTrue(json_decode(static::$client->getResponse()->getContent())->content->receive_comms);
    }

    public function testSecondStep()
    {
        /*
         * empty quote income
         */
        $this->send('/user/quote_income', [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $this->assertEmpty(json_decode(static::$client->getResponse()->getContent())->content);

        /*
         * add quote income
         */

        $parameters = [
            'total_gross_annual' => 5500,
            'main_source'        => 'Contractor',
        ];
        static::$content = json_encode($parameters);
        $this->send('/user/quote_income', [], 'POST');
        $cont = static::$client->getResponse()->getContent();
        $this->assertTrue(static::$client->getResponse()->isSuccessful(), $cont);

        /*
         * our user income
         */
        $this->send('/user/quote_income', [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals($content->total_gross_annual, 5500);

// test BankDetails

        //GET empty
        $this->send('/user/bank_details');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $this->assertEquals([], json_decode(static::$client->getResponse()->getContent())->content);

        //todo: POST wrong
        //POST correct
        $params = [
            'bank_sort_code'      => 234344,
            'bank_account_number' => 456456456,
        ];
        static::$content = json_encode($params);
        $this->send('/user/bank_details', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $this->assertEquals([], json_decode(static::$client->getResponse()->getContent())->content);

        //GET non empty
        $this->send('/user/bank_details');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals($params['bank_sort_code'], $content->bank_sort_code);
        $this->assertEquals($params['bank_account_number'], $content->bank_account_number);
    }

    public function testThirdStep()
    {
        /*
         * before email confirmation user is not active
         */
        $this->assertNull(static::$session->getUser()->getActive());
        $this->verifiedAndLogin();
        /*
         * after email confimation user should be active. see next step
         */
    }

    public function testForthStep()
    {
        /*
         * check that user has become active after email confirmation and set new user to the test static session
         */
        $userAfterConfirmEmail = static::$em->getRepository(User::class)->find(static::$session->getUser()->getId());
        $this->assertTrue($userAfterConfirmEmail->getActive());
        static::$session->setUser($userAfterConfirmEmail);

        /*
         * retrieve a verify code for mobile. we can not check SMS sending, just verify code generating
         */
        $this->send('/user/receive_verification_code', [], 'POST');

        /**
         * check that verified code is not null
         * @var UserCommunication $userMobile
         */
        $userMobile = static::$kernel->getContainer()->get('whs.manager.user_communication_manager')->getMobileCommunicationForUser(static::$session->getUser());
        /*
         * check that the code length equals to 5
         */
        $this->assertEquals(strlen($userMobile->getVerificationCode()), 5);

        /*
         * check mobile verifying with error code
         */
        $parameters = [
            /*
             * error in the parameter name
             */
            'codee' => '123456'
        ];
        static::$content = json_encode($parameters);
        $this->send('/user/check_verification_code', [], 'POST');
        $result = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals($result->content->code, 'verified_code.isNull');
        $parameters = [
            /*
             * six digits - fail code
             */
            'code' => '123456'
        ];
        static::$content = json_encode($parameters);
        $this->send('/user/check_verification_code', [], 'POST');
        $result = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals($result->content->code, 'verified_code.isNull');
        /*
         * check mobile verifying with valid code
         */
        $parameters = [
            'code' => $userMobile->getVerificationCode()
        ];
        static::$content = json_encode($parameters);
        $this->send('/user/check_verification_code', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
    }

    public function testFifthStep()
    {
        /*
         * QuoteApply should have draft status before offer creation
         * needs __toString() in User entity
         */
        $draftQuoteApply = static::$kernel->getContainer()->get('whs.manager.quote_apply_manager')->getDraftLenderQuoteApply(static::$session->getUser());
        $this->assertNotNull($draftQuoteApply);

        //check restrictions
        $this->send('/user/invest_offers_restrictions');
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        //will have 3 restrictions groups with non empty restrictions
        $this->assertEquals(4, count($content->restriction_groups));

        /*
         * create offer
         */
        //have restrictions in DB with such ids
        $restrictions = [
            ['id' => 1],
            ['id' => 2],
            ['id' => 3],
        ];
        $priorities = [
            ['id' => 4],
            ['id' => 5],
            ['id' => 6],
        ];

        //try add invest offer with amount not multiple 20
        $params = array(
            'finance_type'         => 100,
            'investment_amount'    => 1010,
            'investment_frequency' => 300,
            'investment_type'      => 100,
            'rate'                 => 200,
            'term'                 => 48,
            'investment_name'      => 'Investment Name',
            'accept_terms'         => true,
            'confirm_invest'       => true,
            'risk_confirm'         => true,
            'restrictions'         => $restrictions,
            'priorities'           => $priorities,
        );
        static::$content = json_encode($params);
        $this->send('/user/invest_offers', array(), 'POST');
        $this->assertTrue(static::$client->getResponse()->isClientError());
        $this->assertEquals($this->getContentAsObject()->content->investmentAmount->code, 'investOffer.invesmentAmount.badFormat');

        $params = array(
            'finance_type'         => 100,
            'investment_amount'    => 1000,
            'investment_frequency' => 300,
            'investment_type'      => 100,
            'rate'                 => 200,
            'term'                 => 48,
            'investment_name'      => 'Investment Name',
            'accept_terms'         => true,
            'confirm_invest'       => true,
            'risk_confirm'         => true,
            'restrictions'         => $restrictions,
            'priorities'           => $priorities,
        );
        static::$content = json_encode($params);
        $this->send('/user/invest_offers', array(), 'POST');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());

        $this->assertEquals($this->getContentAsObject()->status, 'success');
        $this->assertNotNull($this->getContentAsObject()->content->process_id);
        $this->assertNotNull($this->getContentAsObject()->content->status);

        //try to create another
        $params = array(
            'finance_type'         => 100,
            'investment_amount'    => 1000,
            'investment_frequency' => 300,
            'investment_type'      => 100,
            'rate'                 => 200,
            'term'                 => 48,
            'investment_name'      => 'Investment Name',
            'accept_terms'         => true,
            'confirm_invest'       => true,
            'risk_confirm'         => true,
            'restrictions'         => $restrictions,
            'priorities'           => $priorities,
        );
        static::$content = json_encode($params);
        $this->send('/user/invest_offers', array(), 'POST');
        //but can't create another , because now quote in STATUS_PENDING_FUNDS
        $this->assertEquals(400, static::$client->getResponse()->getStatusCode(), $content);
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals('investOffer.create.accessDenied', $content->code, static::$client->getResponse()->getContent());

        /*
         * QuoteApply should have FUNDING REQUIRED status after offer creation
         */
        $fundingRequiredQuoteApply = static::$kernel->getContainer()->get('whs.manager.quote_apply_manager')->getFundingRequiredLenderQuoteApply(static::$session->getUser());
        $this->assertNotNull($fundingRequiredQuoteApply);

        /*
         * check get invest offers
         */
        $this->send('/user/invest_offers', array(), 'GET');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $content = json_decode(static::$client->getResponse()->getContent())->content[0];
        $this->assertEquals($content->finance_type, 100);
        $this->assertEquals($content->investment_amount, 1000);
        $this->assertEquals($content->investment_frequency, 300);
        $this->assertEquals($content->investment_type, 100);
        $this->assertEquals($content->rate, 200);
        $this->assertEquals($content->term, 48);
        $this->assertEquals($content->investment_name, 'Investment Name');
        $this->assertEquals($content->accept_terms, true);
        $this->assertEquals($content->confirm_invest, true);
        $this->assertEquals($content->risk_confirm, true);
        //check that have properties and restrictions && have ids as POSTed above
        $this->assertEquals($restrictions[0]['id'], $content->restrictions[0]->id);
        $this->assertEquals($restrictions[1]['id'], $content->restrictions[1]->id);
        $this->assertEquals($restrictions[2]['id'], $content->restrictions[2]->id);
        $this->assertEquals($priorities[0]['id'], $content->priorities[0]->id);
        $this->assertEquals($priorities[1]['id'], $content->priorities[1]->id);
        $this->assertEquals($priorities[2]['id'], $content->priorities[2]->id);

        //check get invest equal investOffer by id
        $this->send('/user/invest_offers/'.$content->id, [], 'GET');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertEquals($content->finance_type, 100);
        $this->assertEquals($content->investment_amount, 1000);
        $this->assertEquals($content->investment_frequency, 300);
        $this->assertEquals($content->investment_type, 100);
        $this->assertEquals($content->rate, 200);
        $this->assertEquals($content->term, 48);
        $this->assertEquals($content->investment_name, 'Investment Name');
        $this->assertEquals($content->accept_terms, true);
        $this->assertEquals($content->confirm_invest, true);
        $this->assertEquals($content->risk_confirm, true);
        $this->assertObjectHasAttribute('process_id', $content);
        //check that have properties and restrictions && have ids as POSTed above
        $this->assertEquals($restrictions[0]['id'], $content->restrictions[0]->id);
        $this->assertEquals($restrictions[1]['id'], $content->restrictions[1]->id);
        $this->assertEquals($restrictions[2]['id'], $content->restrictions[2]->id);
        $this->assertEquals($priorities[0]['id'], $content->priorities[0]->id);
        $this->assertEquals($priorities[1]['id'], $content->priorities[1]->id);
        $this->assertEquals($priorities[2]['id'], $content->priorities[2]->id);

        $params = array(
            'finance_type'         => 200,
            'investment_amount'    => 2000,
            'investment_frequency' => 200,
            'investment_type'      => 200,
            'rate'                 => 100,
            'term'                 => 24,
            'investment_name'      => 'Investment Name 2',
            'accept_terms'         => true,
            'confirm_invest'       => true,
            'risk_confirm'         => true,
            'restrictions'         => [],
            'priorities'           => [['id' => 7]],
        );

        //test investOffer PATCH
        static::$content = json_encode($params);
        //in $content->id now we have the id of offer that we previously GET
        $this->send('/user/invest_offers/'.$content->id, array(), 'PATCH');
        $content = json_decode(static::$client->getResponse()->getContent())->content;
        $this->assertTrue(static::$client->getResponse()->isOk(), static::$client->getResponse()->getStatusCode());
        $this->assertEquals($content->finance_type, 200);
        $this->assertEquals($content->investment_amount, 2000);
        $this->assertEquals($content->investment_frequency, 200);
        $this->assertEquals($content->investment_type, 200);
        $this->assertEquals($content->rate, 100);
        $this->assertEquals($content->term, 24);
        $this->assertEquals($content->investment_name, 'Investment Name 2');
        $this->assertEquals($content->accept_terms, true);
        $this->assertEquals($content->confirm_invest, true);
        $this->assertEquals($content->risk_confirm, true);
        //check that have properties and restrictions are empty now
        $this->assertEquals([], $content->restrictions);
        $this->assertEquals(1, count($content->priorities));
        $this->assertEquals(7, $content->priorities[0]->id);

        //test DELETE investOffer
        $this->send('/user/invest_offers/'.$content->id, array(), 'DELETE');
        $this->assertTrue(static::$client->getResponse()->isOk());
        $response = json_decode(static::$client->getResponse()->getContent());
        $this->assertEquals('error', $response->status);
        $this->assertEquals('quoteApply.notApproved', $response->content->code);
    }

    public function testGetWorldpayParameters()
    {
        /*
         * Worldpay transaction should be null before getting Worldpay parameters
         */
        $worldpayTransaction = static::$em->getRepository(WorldpayTransaction::class)->findOneBy(['user' => static::$session->getUser()]);
        $this->assertNull($worldpayTransaction);
        /*
         * Get Worldpay parameters. This action genereate DEPC event (see AppBundle\Event\TransactionEventsHandler\DEPEventsHandler)
         */
        $investOffer = static::$em->getRepository(InvestOffer::class)->findOneBy(['user' => static::$session->getUser()]);

        $process = static::$em->getRepository(InvestOfferProcess::class)->findOneBy(['investOffer' => $investOffer]);

        $params = [
            'redirect_url' => '/url',
            'process_id' => $process->getId()
        ];
        $this->send('/worldpay/form_parameters', $params);
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $cartId = json_decode(static::$client->getResponse()->getContent())->content->cart_id;

        /*
         * QuoteApply should have PENDING FUNDS status after getting worldpay parameters
         */
        $pendingFundsQuoteApply = static::$kernel->getContainer()->get('whs.manager.quote_apply_manager')->getPendingFundsLenderQuoteApply(static::$session->getUser());
        $this->assertNotNull($pendingFundsQuoteApply);

        /*
         * Worldpay transaction should be created after getting Worldpay parameters
         */
        $worldpayTransaction = static::$em->getRepository(WorldpayTransaction::class)->findOneBy(['user' => static::$session->getUser()]);
        $this->assertNotNull($worldpayTransaction);
        /*
         * Worldpay transaction status should be pending
         */
        $this->assertEquals($worldpayTransaction->getPaymentStatus(), WorldpayTransaction::PAYMENT_STATUS_PENDING);

        /*
         * Check that system event DEPC has been created
         */
        $systemEventDEPC = static::$em->getRepository(Event::class)->findOneBy(['user' => static::$session->getUser()]);
        $this->assertNotNull($systemEventDEPC);
        $this->assertEquals($systemEventDEPC->getType(), TransactionEvents::DEPC);
        /*
         * Check investor_owner Cash Pending system transactions which should be created on DEPC
         */
        $systemTransactionInvestor = static::$em->getRepository(Transaction::class)->findOneBy([
            'user'               => static::$session->getUser(),
            'transactionOwner'   => Transaction::TRANSACTION_OWNER_INVESTOR,
            'transactionAccount' => Transaction::TRANSACTION_ACCOUNT_CASH_PENDING,
            'event'              => $systemEventDEPC
        ]);
        $this->assertNotNull($systemTransactionInvestor);
        $this->assertEquals($systemTransactionInvestor->getTransactionAmount(), 2000);

        /*
         * Check worldpay_owner Cash Pending system transactions which should be created on DEPC
         */
        $systemTransactionWorldpay = static::$em->getRepository(Transaction::class)->findOneBy([
            'user'               => static::$session->getUser(),
            'transactionOwner'   => Transaction::TRANSACTION_OWNER_WORLDPAY,
            'transactionAccount' => Transaction::TRANSACTION_ACCOUNT_CASH_PENDING,
            'event'              => $systemEventDEPC
        ]);
        $this->assertNotNull($systemTransactionWorldpay);
        $this->assertEquals($systemTransactionWorldpay->getTransactionAmount(), -2000);
        /*
         * System investor transaction should be a parent for system worldpay transaction
         */
        $this->assertEquals($systemTransactionWorldpay->getParentTransaction()->getId(), $systemTransactionInvestor->getId());
        /*
         * We wait callback for this transaction
         */
        return $cartId;
    }

    /**
     * @depends testGetWorldpayParameters
     * @param string $cartId
     * @return string
     */
    public function testCancelWorldpayCallback($cartId)
    {
        static::$content = $this->getWorldpayParametersJson($cartId, WorldpayHtmlTransaction::TRANS_STATUS_CANCEL);
        $this->send('/worldpay/callback', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());

        return $cartId;
    }

    /**
     * @depends testCancelWorldpayCallback
     * @param string $cartId
     */
    public function testCancelPayment($cartId)
    {
        /*
        * QuoteApply should have FUNDING REQUIRED status after getting worldpay parameters
        */
        $quoteApply = static::$em->getRepository(QuoteApply::class)->getQuoteAppliesForUser(static::$session->getUser(), [QuoteApply::STATUS_FUNDING_REQUIRED], Product::TYPE_INVESTOR);
        $this->assertNotEmpty($quoteApply);

        /*
         * Worldpay transaction status should be changed to CANCELLED
         */
        $worldpayTransaction = static::$em->getRepository(WorldpayTransaction::class)->findOneBy(['user' => static::$session->getUser(), 'customerOrderCode' => $cartId]);
        $this->assertEquals($worldpayTransaction->getPaymentStatus(), WorldpayTransaction::PAYMENT_STATUS_CANCELLED);

        /*
         * Check that system event DEPX has been created
         */
        $systemEventDEPX = static::$em->getRepository(Event::class)->findOneBy(['user' => static::$session->getUser(), 'type' => TransactionEvents::DEPX]);
        $this->assertNotNull($systemEventDEPX);

        /*
         * Check investor_owner Cash Pending system transactions which should be created on DEPX
         */
        $systemTransactionInvestor = static::$em->getRepository(Transaction::class)->findOneBy([
            'user'               => static::$session->getUser(),
            'transactionOwner'   => Transaction::TRANSACTION_OWNER_INVESTOR,
            'transactionAccount' => Transaction::TRANSACTION_ACCOUNT_CASH_PENDING,
            'event'              => $systemEventDEPX
        ]);
        $this->assertNotNull($systemTransactionInvestor);
        /*
         * The amount should be inverted
         */
        $this->assertEquals($systemTransactionInvestor->getTransactionAmount(), -2000);

        /*
         * Check worldpay_owner Cash Pending system transactions which should be created on DEPX
         */
        $systemTransactionInvestor = static::$em->getRepository(Transaction::class)->findOneBy([
            'user'               => static::$session->getUser(),
            'transactionOwner'   => Transaction::TRANSACTION_OWNER_WORLDPAY,
            'transactionAccount' => Transaction::TRANSACTION_ACCOUNT_CASH_PENDING,
            'event'              => $systemEventDEPX
        ]);
        $this->assertNotNull($systemTransactionInvestor);
        /*
         * The amount should be inverted
         */
        $this->assertEquals($systemTransactionInvestor->getTransactionAmount(), 2000);
    }

    public function testSuccessWorldpayCallback()
    {
        /*
         * Get new Worldpay parameters. This action genereate DEPC event (see AppBundle\Event\TransactionEventsHandler\DEPEventsHandler)
         */
        $investOffer = static::$em->getRepository(InvestOffer::class)->findOneBy(['user' => static::$session->getUser()]);

        $process = static::$em->getRepository(InvestOfferProcess::class)->findOneBy(['investOffer' => $investOffer]);

        $params = [
            'redirect_url' => '/url',
            'process_id' => $process->getId()
        ];
        $this->send('/worldpay/form_parameters', $params);
        $this->assertTrue(static::$client->getResponse()->isSuccessful());
        $cartId = json_decode(static::$client->getResponse()->getContent())->content->cart_id;
        /*
         * Worldpay transaction should be created after getting Worldpay parameters
         */
        $worldpayTransaction = static::$em->getRepository(WorldpayTransaction::class)->findOneBy(['user' => static::$session->getUser()]);

        /*
         * Send test callback
         */
        static::$content = $this->getWorldpayParametersJson($cartId, WorldpayHtmlTransaction::TRANS_STATUS_SUCCESS);
        $this->send('/worldpay/callback', [], 'POST');
        $this->assertTrue(static::$client->getResponse()->isSuccessful());

        return $cartId;
    }

    /**
     * The next events are generated on payment:
     * worldpay.callback.received,
     * DEPA,
     * api.callvalidate.request
     * @depends testSuccessWorldpayCallback
     * @param string $cartId
     *
     * @return integer
     */
    public function testSuccessPayment($cartId)
    {
        /*
         * Worldpay transaction status should be changed to SUCCESS
         */
        $worldpayTransaction = static::$em->getRepository(WorldpayTransaction::class)->findOneBy(['user' => static::$session->getUser(), 'customerOrderCode' => $cartId]);
        $this->assertEquals($worldpayTransaction->getPaymentStatus(), WorldpayTransaction::PAYMENT_STATUS_SUCCESS);

        /*
         * Check that system event DEPA has been created
         */
        $systemEventDEPA = static::$em->getRepository(Event::class)->findOneBy(['user' => static::$session->getUser(), 'type' => TransactionEvents::DEPA]);
        $this->assertNotNull($systemEventDEPA);

        /*
         * Check investor_owner Cash Pending system transactions which should be created on DEPA
         */
        $systemTransactionInvestor = static::$em->getRepository(Transaction::class)->findOneBy([
            'user'               => static::$session->getUser(),
            'transactionOwner'   => Transaction::TRANSACTION_OWNER_INVESTOR,
            'transactionAccount' => Transaction::TRANSACTION_ACCOUNT_CASH_PENDING,
            'event'              => $systemEventDEPA
        ]);
        $this->assertNotNull($systemTransactionInvestor);
        /*
         * The amount should be inverted
         */
        $this->assertEquals($systemTransactionInvestor->getTransactionAmount(), -2000);

        /*
         * Check worldpay_owner Cash Pending system transactions which should be created on DEPA
         */
        $systemTransactionInvestor = static::$em->getRepository(Transaction::class)->findOneBy([
            'user'               => static::$session->getUser(),
            'transactionOwner'   => Transaction::TRANSACTION_OWNER_WORLDPAY,
            'transactionAccount' => Transaction::TRANSACTION_ACCOUNT_CASH_PENDING,
            'event'              => $systemEventDEPA
        ]);
        $this->assertNotNull($systemTransactionInvestor);
        /*
         * The amount should be inverted
         */
        $this->assertEquals($systemTransactionInvestor->getTransactionAmount(), 2000);

        /*
         * Check investor_owner Cash Available system transactions which should be created on DEPA
         */
        $systemTransactionInvestor = static::$em->getRepository(Transaction::class)->findOneBy([
            'user'               => static::$session->getUser(),
            'transactionOwner'   => Transaction::TRANSACTION_OWNER_INVESTOR,
            'transactionAccount' => Transaction::TRANSACTION_ACCOUNT_CASH_AVAILABLE,
            'event'              => $systemEventDEPA
        ]);
        $this->assertNotNull($systemTransactionInvestor);
        $this->assertEquals($systemTransactionInvestor->getTransactionAmount(), 2000);

        /*
         * Check trust_bank_account_lenders Cash Available system transactions which should be created on DEPA
         */
        $systemTransactionInvestor = static::$em->getRepository(Transaction::class)->findOneBy([
            'user'               => static::$session->getUser(),
            'transactionOwner'   => Transaction::TRANSACTION_OWNER_TRUST_BANK_ACCOUNT_LENDERS,
            'transactionAccount' => Transaction::TRANSACTION_ACCOUNT_CASH_AVAILABLE,
            'event'              => $systemEventDEPA
        ]);
        $this->assertNotNull($systemTransactionInvestor);
        /*
         * The amount should be inverted
         */
        $this->assertEquals($systemTransactionInvestor->getTransactionAmount(), -2000);
        /**
         * QuoteApply should have PENDING status after getting worldpay parameters
         *
         * @var QuoteApply[] $pendingQuoteApply
         */
        $pendingQuoteApply = static::$em->getRepository(QuoteApply::class)->getQuoteAppliesForUser(static::$session->getUser(), [QuoteApply::STATUS_PENDING], Product::TYPE_INVESTOR);
        $this->assertNotEmpty($pendingQuoteApply);
        /*
         * Check that call_credit_api_calls has created on api.callvalidate.request and has status PENDING
         */
        $callCreditApiCalls = static::$em->getRepository(CallCreditAPICalls::class)->findOneBy(['quoteApply' => $pendingQuoteApply]);
        $this->assertNotNull($callCreditApiCalls);
        $this->assertEquals($callCreditApiCalls->getStatus(), CallCreditAPICalls::STATUS_PENDING);
        $this->assertEquals($callCreditApiCalls->getType(), CallCreditAPICalls::TYPE_CALLVALIDATE);
        $this->assertEquals($callCreditApiCalls->getPurpose(), CallCreditAPICalls::PURPOSE_QS);

        $pendingQuoteApplyId = $pendingQuoteApply[0]->getId();

        return $pendingQuoteApplyId;
    }

    /**
     * @depends testSuccessPayment
     * @param integer $quoteApplyId
     */
    public function testSendCallValidateRequest($quoteApplyId)
    {
        /**
         * InvestOffer should have null active value
         * @var InvestOffer[] $investOffer
         */
        $investOffer = static::$em->getRepository(InvestOffer::class)
            ->getInvestOfferForUser(static::$session->getUser(), Criteria::create()
                ->orderBy(['createdAt' => Criteria::DESC])
                ->setMaxResults(1));
        $this->assertNotEmpty($investOffer);
        $this->assertEquals($investOffer[0]->getStatus(), InvestOffer::STATUS_DRAFT);

        $quoteApply = static::$em->getRepository(QuoteApply::class)->find($quoteApplyId);

        /**
         * Check that UserProduct has not been created yet
         */
        $userProduct = static::$kernel->getContainer()->get('whs.manager.user_product_manager')->getUserProduct($quoteApply->getUser(), $quoteApply->getProduct());
        $this->assertNull($userProduct);

        /*
         * Send call validate response
         */
        $this->callValidateResponce();

        /**
         * QuoteApply should have APPROVED status after getting worldpay parameters
         * @var QuoteApply $approvedQuoteApply
         */
        $approvedQuoteApply = static::$em->getRepository(QuoteApply::class)->getQuoteAppliesForUser(static::$session->getUser(), [QuoteApply::STATUS_APPROVED], Product::TYPE_INVESTOR);
        $this->assertNotEmpty($approvedQuoteApply);

        /**
         * Check that userProduct not null
         */
        $userProduct = static::$kernel->getContainer()->get('whs.manager.user_product_manager')->getUserProduct($quoteApply->getUser(), $quoteApply->getProduct());
        $this->assertNotNull($userProduct);

        /**
         * InvestOffer should have true active value
         */
        $investOffer = static::$em->getRepository(InvestOffer::class)
            ->getInvestOfferForUser(static::$session->getUser(), Criteria::create()
                ->orderBy(['createdAt' => Criteria::DESC])
                ->setMaxResults(1));
        $this->assertEquals($investOffer[0]->getStatus(), InvestOffer::STATUS_ACTIVE);

        $subOffers = static::$em->getRepository(InvestOfferSub::class)->findBy(['investOffer' => $investOffer]);
        $this->assertCount(100, $subOffers);
    }
}