<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 23.02.17
 * Time: 14:51
 */

namespace AppBundle\Tests\Utills;


use AppBundle\Manager\InvestOfferManager;
use AppBundle\Tests\AppTest;
use FOS\RestBundle\Tests\Functional\WebTestCase;

class InvestOfferManagerTest extends AppTest
{
    /**
     * @expectedException \Exception
     */
    public function testGetCountOfSuboffersException()
    {
//        static::bootKernel();
        $em = static::$em;
        $investOfferManager = new InvestOfferManager($em);

        $countOfSuboffers = $investOfferManager->getCountOfSubOffers(210);

    }

    public function testGetCountOfSuboffers()
    {
//        static::bootKernel();
        $em = static::$em;
        $investOfferManager = new InvestOfferManager($em);

        $countOfSuboffers = $investOfferManager->getCountOfSubOffers(200);
        $this->assertEquals(10, $countOfSuboffers);
    }
}