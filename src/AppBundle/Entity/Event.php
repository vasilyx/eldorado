<?php

namespace AppBundle\Entity;

use AppBundle\Traits\StaticCreatable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * Event
 */
class Event
{
    use Timestampable;
    use StaticCreatable;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $adminId;

    /**
     * Event Type (refer EventTypeList)
     *
     * @var string
     */
    private $type;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * DepositID / OfferID / ContractID/ WithdrawalID/ InstalmentID
     *
     * @var integer
     */
    private $productId;

    /**
     * @var BaseProcess
     */
    private $process;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set adminId
     *
     * @param integer $adminId
     * @return Event
     */
    public function setAdminId($adminId)
    {
        $this->adminId = $adminId;

        return $this;
    }

    /**
     * Get adminId
     *
     * @return integer 
     */
    public function getAdminId()
    {
        return $this->adminId;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Event
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Event
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Event
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     * @return Event
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
        return $this;
    }

    /**
     * Set process
     *
     * @param \AppBundle\Entity\BaseProcess $process
     *
     * @return Event
     */
    public function setProcess(\AppBundle\Entity\BaseProcess $process = null)
    {
        $this->process = $process;

        return $this;
    }

    /**
     * Get process
     *
     * @return \AppBundle\Entity\BaseProcess
     */
    public function getProcess()
    {
        return $this->process;
    }
}
