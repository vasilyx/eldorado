<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 24.02.17
 * Time: 7:40
 */

namespace AppBundle\Entity;


use AppBundle\Process\BaseProcessInterface;
use Doctrine\Common\Collections\ArrayCollection;

abstract class BaseProcess implements BaseProcessInterface
{
    /**
     * @var ArrayCollection
     */
    private $events;

    /**
     * @var int
     */
    private $id;

    /**
     * @var User
     */
    private $user;

    public function __construct()
    {
        $this->events = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add event
     *
     * @param \AppBundle\Entity\Event $event
     *
     * @return BaseProcess
     */
    public function addEvent(\AppBundle\Entity\Event $event)
    {
        $event->setProcess($this);
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \AppBundle\Entity\Event $event
     */
    public function removeEvent(\AppBundle\Entity\Event $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param Event $event
     * @return bool
     */
    public function hasEvent(\AppBundle\Entity\Event $event)
    {
        return $this->events->contains($event);
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return $this
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
