<?php

namespace AppBundle\Entity;

use AppBundle\Traits\StaticCreatable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * LoanApply
 */
class LoanApply
{
    use StaticCreatable;
    use Timestampable;

    /*
     * User filling details of the Loan
     */
    const STATUS_DRAFT = 100;
    /*
     * Waiting of sending API call to Call Credit
     */
    const STATUS_SUBMITTED = 200;
    /**
     * Loan has been submitted for validation and waiting of Call Validate Check decision
     */
    const STATUS_CALLVALIDATE_CHECK = 300;
    /*
     * waiting of Affordability Check decision
     */
    const STATUS_AFFORDABILITY_CHECK = 320;
    /**
     * waiting of Credit History decision
     */
    const STATUS_CREDIT_HISTORY_CHECK = 330;
    /**
     * Administrator is able to either Approve the Credit Decision created by the algorithm
     */
    const STATUS_CREDIT_DECISION = 350;
    /**
     * Loan Agreement sent to the borrower and waiting confirmation
     */
    const STATUS_ACCEPTANCE = 400;
    /**
     * Loan Apply has been declined
     */
    const STATUS_DECLINED = 450;
    /**
     * Loan confirmed by the borrower but not fully matched yet
     */
    const STATUS_MATCHING = 500;
    /**
     * Loan was fully matched
     */
    const STATUS_FULLY_MATCHED = 550;
    /**
     * Contract was signed by the Borrower and transaction on money movement was created
     */
    const STATUS_SIGNED = 600;
    /**
     * Loan request was declined
     */
    const STATUS_ABORTED = 700;
    /**
     * Loan is active
     */
    const STATUS_ACTIVE = 800;
    /**
     * Loan was closed as user paid all debts
     */
    const STATUS_SETTLED = 900;

    public static function getStatusMap()
    {
        return [
            self::STATUS_DRAFT     => 'draft',
            self::STATUS_SUBMITTED => 'submitted',
            self::STATUS_CALLVALIDATE_CHECK => 'callvalidate_check',
            self::STATUS_AFFORDABILITY_CHECK => 'affordability_check',
            self::STATUS_CREDIT_HISTORY_CHECK => 'credit_history_check',
            self::STATUS_CREDIT_DECISION => 'credit_decision',
            self::STATUS_ACCEPTANCE => 'acceptance',
            self::STATUS_MATCHING => 'matching',
            self::STATUS_FULLY_MATCHED => 'fully_matched',
            self::STATUS_SIGNED => 'signed',
            self::STATUS_DECLINED => 'declined',
            self::STATUS_ACTIVE => 'active',
            self::STATUS_SETTLED => 'settled'
        ];
    }

    public static function getStatusCodes()
    {
        return array_keys(self::getStatusMap());
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var float
     */
    private $monthly;

    /**
     * @var integer
     */
    private $term;

    /**
     * @var float
     */
    private $apr;

    /**
     * @var \AppBundle\Entity\QuoteApply
     */
    private $quoteApply;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var string
     */
    private $status;

    /**
     * @return QuoteApply
     */
    public function getQuoteApply()
    {
        return $this->quoteApply;
    }

    /**
     * Set quoteApply
     *
     * @param QuoteApply $quoteApply
     * @return LoanApply
     */
    public function setQuoteApply($quoteApply)
    {
        $this->quoteApply = $quoteApply;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return LoanApply
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return float
     */
    public function getMonthly()
    {
        return $this->monthly;
    }

    /**
     * @param float $monthly
     * @return $this
     */
    public function setMonthly($monthly)
    {
        $this->monthly = $monthly;

        return $this;
    }

    /**
     * @return int
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * @param int $term
     * @return $this
     */
    public function setTerm($term)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * @return float
     */
    public function getApr()
    {
        return $this->apr;
    }

    /**
     * @param float $apr
     * @return $this
     */
    public function setApr($apr)
    {
        $this->apr = $apr;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return LoanApply
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
}
