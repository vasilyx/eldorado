<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 15.06.2018
 * Time: 11:54
 */

namespace AppBundle\Entity;

use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * Class Instalment
 * @package AppBundle\Entity
 */
class Instalment
{
    use Timestampable;

    const PAYMENT_STATUS_PENDING = 10;
    const PAYMENT_STATUS_PAID = 20;
    const PAYMENT_STATUS_LATE = 30;
    const PAYMENT_STATUS_MISSED = 40;
    const PAYMENT_STATUS_RECOVERED = 50;
    const PAYMENT_STATUS_PROJECTED = 1000;


    const INSTALLMENT_STATUS_OUTSTANDING = 10;
    const INSTALLMENT_STATUS_PART_CLEARED = 20;
    const INSTALLMENT_STATUS_CLEARED = 30;
    const INSTALLMENT_STATUS_OVERPAID = 40;

    public static function getPaymentStatusMap()
    {
        return [
            self::PAYMENT_STATUS_PENDING             => 'pending',
            self::PAYMENT_STATUS_PAID                => 'paid',
            self::PAYMENT_STATUS_LATE                => 'late',
            self::PAYMENT_STATUS_MISSED              => 'missed',
            self::PAYMENT_STATUS_RECOVERED           => 'recovered',
            self::PAYMENT_STATUS_PROJECTED           => 'projected'
        ];
    }

    public static function getInstallmentStatusMap()
    {
        return [
            self::INSTALLMENT_STATUS_OUTSTANDING             => 'outstanding',
            self::INSTALLMENT_STATUS_PART_CLEARED                => 'part cleared',
            self::INSTALLMENT_STATUS_CLEARED                => 'cleared',
            self::INSTALLMENT_STATUS_OVERPAID              => 'overpaid',
        ];
    }


    /**
     * @var integer
     */
    private $id;

    /**
     * @var LoanApply
     */
    private $loanApply;

    /**
     * @var int
     */
    private $period;

    /**
     * @var \DateTime
     */
    private $paymentDate;

    /**
     * @var string
     */
    private $instalment;

    /**
     * @var string
     */
    private $interest;

    /**
     * @var string
     */
    private $principle;

    /**
     * @var string
     */
    private $balance;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return LoanApply
     */
    public function getLoanApply()
    {
        return $this->loanApply;
    }

    /**
     * @param LoanApply $loanApply
     * @return $this
     */
    public function setLoanApply(LoanApply $loanApply)
    {
        $this->loanApply = $loanApply;

        return $this;
    }

    /**
     * @return int
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @param int $period
     * @return $this
     */
    public function setPeriod($period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * @param \DateTime $paymentDate
     * @return $this
     */
    public function setPaymentDate(\DateTime $paymentDate)
    {
        $this->paymentDate = $paymentDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getInstalment()
    {
        return $this->instalment;
    }

    /**
     * @param string $instalment
     * @return $this
     */
    public function setInstalment($instalment)
    {
        $this->instalment = $instalment;

        return $this;
    }

    /**
     * @return string
     */
    public function getInterest()
    {
        return $this->interest;
    }

    /**
     * @param string $interest
     * @return $this
     */
    public function setInterest($interest)
    {
        $this->interest = $interest;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrinciple()
    {
        return $this->principle;
    }

    /**
     * @param string $principle
     * @return $this
     */
    public function setPrinciple($principle)
    {
        $this->principle = $principle;

        return $this;
    }

    /**
     * @return string
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param string $balance
     * @return $this
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

     /**
     * @var integer
     */
    private $paymentStatus;

    /**
     * @var integer
     */
    private $installmentStatus;

    /**
     * @var string
     */
    private $outstanding;

    /**
     * @var string
     */
    private $allocation;

    /**
     * @var string
     */
    private $paymentAmount;


    /**
     * Set paymentStatus
     *
     * @param integer $paymentStatus
     *
     * @return Instalment
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    /**
     * Get paymentStatus
     *
     * @return integer
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * Set installmentStatus
     *
     * @param integer $installmentStatus
     *
     * @return Instalment
     */
    public function setInstallmentStatus($installmentStatus)
    {
        $this->installmentStatus = $installmentStatus;

        return $this;
    }

    /**
     * Get installmentStatus
     *
     * @return integer
     */
    public function getInstallmentStatus()
    {
        return $this->installmentStatus;
    }

    /**
     * Set outstanding
     *
     * @param string $outstanding
     *
     * @return Instalment
     */
    public function setOutstanding($outstanding)
    {
        $this->outstanding = $outstanding;

        return $this;
    }

    /**
     * Get outstanding
     *
     * @return string
     */
    public function getOutstanding()
    {
        return $this->outstanding;
    }

    /**
     * Set allocation
     *
     * @param string $allocation
     *
     * @return Instalment
     */
    public function setAllocation($allocation)
    {
        $this->allocation = $allocation;

        return $this;
    }

    /**
     * Get allocation
     *
     * @return string
     */
    public function getAllocation()
    {
        return $this->allocation;
    }

    /**
     * Set paymentAmount
     *
     * @param string $paymentAmount
     *
     * @return Instalment
     */
    public function setPaymentAmount($paymentAmount)
    {
        $this->paymentAmount = $paymentAmount;

        return $this;
    }

    /**
     * Get paymentAmount
     *
     * @return string
     */
    public function getPaymentAmount()
    {
        return $this->paymentAmount;
    }
}
