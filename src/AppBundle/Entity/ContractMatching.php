<?php

namespace AppBundle\Entity;

/**
 * ContractMatching
 */
class ContractMatching
{
    const STATUS_PENDING = 100;
    const STATUS_ACTIVE = 200;
    const STATUS_SETTLED = 300;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var float
     */
    private $contractAmount;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var \AppBundle\Entity\LoanApply
     */
    private $loanApply;

    /**
     * @var \AppBundle\Entity\InvestOffer
     */
    private $investOffer;

    /**
     * @var \AppBundle\Entity\InvestOfferSub
     */
    private $investOfferSub;

    /**
     * @var \AppBundle\Entity\Agreement
     */
    private $agreement;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contractAmount
     *
     * @param float $contractAmount
     * @return ContractMatching
     */
    public function setContractAmount($contractAmount)
    {
        $this->contractAmount = $contractAmount;

        return $this;
    }

    /**
     * Get contractAmount
     *
     * @return float 
     */
    public function getContractAmount()
    {
        return $this->contractAmount;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return ContractMatching
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return ContractMatching
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set loanApply
     *
     * @param \AppBundle\Entity\LoanApply $loanApply
     * @return ContractMatching
     */
    public function setLoanApply(\AppBundle\Entity\LoanApply $loanApply = null)
    {
        $this->loanApply = $loanApply;

        return $this;
    }

    /**
     * Get loanApply
     *
     * @return \AppBundle\Entity\LoanApply
     */
    public function getLoanApply()
    {
        return $this->loanApply;
    }

    /**
     * Set investOffer
     *
     * @param \AppBundle\Entity\InvestOffer $investOffer
     * @return ContractMatching
     */
    public function setInvestOffer(\AppBundle\Entity\InvestOffer $investOffer = null)
    {
        $this->investOffer = $investOffer;

        return $this;
    }

    /**
     * Get investOffer
     *
     * @return \AppBundle\Entity\InvestOffer 
     */
    public function getInvestOffer()
    {
        return $this->investOffer;
    }

    /**
     * Set investOfferSub
     *
     * @param \AppBundle\Entity\InvestOfferSub $investOfferSub
     * @return ContractMatching
     */
    public function setInvestOfferSub(\AppBundle\Entity\InvestOfferSub $investOfferSub = null)
    {
        $this->investOfferSub = $investOfferSub;

        return $this;
    }

    /**
     * Get investOfferSub
     *
     * @return \AppBundle\Entity\InvestOfferSub 
     */
    public function getInvestOfferSub()
    {
        return $this->investOfferSub;
    }

    /**
     * Set agreement
     *
     * @param \AppBundle\Entity\Agreement $agreement
     * @return ContractMatching
     */
    public function setAgreement(\AppBundle\Entity\Agreement $agreement = null)
    {
        $this->agreement = $agreement;

        return $this;
    }

    /**
     * Get agreement
     *
     * @return \AppBundle\Entity\Agreement 
     */
    public function getAgreement()
    {
        return $this->agreement;
    }
}
