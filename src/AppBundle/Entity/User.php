<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 */
class User implements UserInterface, \JsonSerializable
{
    use Timestampable;

    const ROLE_EMAIL_NOT_CONFIRMED = 'ROLE_EMAIL_NOT_CONFIRMED';
    const MAIN_TRANSACTION_USER_ID = 1;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $plainPassword;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $datetimeRegistration;

    /**
     * @var \DateTime
     */
    private $datetimeLastLogin;

    /**
     * @var string
     */
    private $forgottenPasswordToken;

    /**
     * @var string
     */
    private $confirmEmailToken;

    /**
     * @var integer
     */
    private $partnerId;

    private $activeApiToken;

    private $preferences;


    /**
     * @var UserDetails
     */
    private $details;

    /**
     * @var ArrayCollection
     */
    private $addresses;

    /**
     * @var ArrayCollection
     */
    private $communications;

    /**
     * @var ArrayCollection
     */
    private $dependents;

    /**
     * @var array
     */
    private $roles;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->addresses = new ArrayCollection();
        $this->communications = new ArrayCollection();
        $this->dependents = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;

        return $this;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function isPasswordLegal()
    {
        return $this->password === $this->plainPassword;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set datetimeRegistration
     *
     * @param \DateTime $datetimeRegistration
     * @return User
     */
    public function setDatetimeRegistration($datetimeRegistration)
    {
        $this->datetimeRegistration = $datetimeRegistration;

        return $this;
    }

    /**
     * Get datetimeRegistration
     *
     * @return \DateTime
     */
    public function getDatetimeRegistration()
    {
        return $this->datetimeRegistration;
    }

    /**
     * Set datetimeLastLogin
     *
     * @param \DateTime $datetimeLastLogin
     * @return User
     */
    public function setDatetimeLastLogin($datetimeLastLogin)
    {
        $this->datetimeLastLogin = $datetimeLastLogin;

        return $this;
    }

    /**
     * Get datetimeLastLogin
     *
     * @return \DateTime
     */
    public function getDatetimeLastLogin()
    {
        return $this->datetimeLastLogin;
    }

    /**
     * Set forgottenPasswordToken
     *
     * @param string $forgottenPasswordToken
     * @return User
     */
    public function setForgottenPasswordToken($forgottenPasswordToken)
    {
        $this->forgottenPasswordToken = $forgottenPasswordToken;

        return $this;
    }

    /**
     * Get forgottenPasswordToken
     *
     * @return string
     */
    public function getForgottenPasswordToken()
    {
        return $this->forgottenPasswordToken;
    }

    /**
     * Set confirmEmailToken
     *
     * @param string $confirmEmailToken
     * @return User
     */
    public function setConfirmEmailToken($confirmEmailToken)
    {
        $this->confirmEmailToken = $confirmEmailToken;

        return $this;
    }

    /**
     * Get confirmEmailToken
     *
     * @return string
     */
    public function getConfirmEmailToken()
    {
        return $this->confirmEmailToken;
    }

    /**
     * Set partnerId
     *
     * @param integer $partnerId
     * @return User
     */
    public function setPartnerId($partnerId)
    {
        $this->partnerId = $partnerId;

        return $this;
    }

    /**
     * Get partnerId
     *
     * @return integer
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }

    public function getRoles()
    {
        if (!$this->roles) {
            $this->roles[] = static::ROLE_EMAIL_NOT_CONFIRMED;
        }
        return $this->roles;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return mixed
     */
    public function getActiveApiToken()
    {
        return $this->activeApiToken;
    }

    /**
     * @param mixed $activeApiToken
     * @return $this
     */
    public function setActiveApiToken($activeApiToken)
    {
        $this->activeApiToken = $activeApiToken;
        return $this;
    }

    /**
     * @return $this
     */
    public function generateConfirmationToken()
    {
        $this->setConfirmEmailToken(
            base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
        );
        return $this;
    }

    /**
     * Generates forgotten password token and sets it to User entity
     *
     * @return $this
     */
    public function generateForgotPasswordToken()
    {
        $this->setForgottenPasswordToken(
            base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
        );
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'id'       => $this->getId(),
            'username' => $this->getUsername(),
            'password' => $this->getPassword()
        ];
    }

    /**
     * Set preferences
     *
     * @param \AppBundle\Entity\UserPreferences $preferences
     * @return User
     */
    public function setPreferences(\AppBundle\Entity\UserPreferences $preferences = null)
    {
        $this->preferences = $preferences;

        return $this;
    }

    /**
     * Get preferences
     *
     * @return \AppBundle\Entity\UserPreferences
     */
    public function getPreferences()
    {
        return $this->preferences;
    }

    /**
     * Set details
     *
     * @param \AppBundle\Entity\UserDetails $details
     * @return User
     */
    public function setDetails(\AppBundle\Entity\UserDetails $details = null)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return \AppBundle\Entity\UserDetails
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Get Full name
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->details->getFirstName() . ' ' . $this->details->getLastName();
    }

    public function setRoles(array $roles)
    {
        $this->roles = array();

        foreach ($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }

    public function addRole($role)
    {
        $role = strtoupper($role);
        if ($role === static::ROLE_EMAIL_NOT_CONFIRMED) {
            return $this;
        }

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        /* for LenderFlowTest\testFifthStep() */
        return (string)$this->id;
    }


    /**
     * Add address
     *
     * @param \AppBundle\Entity\UserAddress $address
     *
     * @return User
     */
    public function addAddress(\AppBundle\Entity\UserAddress $address)
    {
        $this->addresses[] = $address;

        return $this;
    }

    /**
     * Remove address
     *
     * @param \AppBundle\Entity\UserAddress $address
     */
    public function removeAddress(\AppBundle\Entity\UserAddress $address)
    {
        $this->addresses->removeElement($address);
    }

    /**
     * Get addresses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Add communication
     *
     * @param \AppBundle\Entity\UserCommunication $communication
     *
     * @return User
     */
    public function addCommunication(\AppBundle\Entity\UserCommunication $communication)
    {
        $this->communications[] = $communication;

        return $this;
    }

    /**
     * Remove communication
     *
     * @param \AppBundle\Entity\UserCommunication $communication
     */
    public function removeCommunication(\AppBundle\Entity\UserCommunication $communication)
    {
        $this->communications->removeElement($communication);
    }

    /**
     * Get communications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommunications()
    {
        return $this->communications;
    }

    /**
     * Add dependent
     *
     * @param \AppBundle\Entity\UserDependent $dependent
     *
     * @return User
     */
    public function addDependent(\AppBundle\Entity\UserDependent $dependent)
    {
        $this->dependents[] = $dependent;

        return $this;
    }

    /**
     * Remove dependent
     *
     * @param \AppBundle\Entity\UserDependent $dependent
     */
    public function removeDependent(\AppBundle\Entity\UserDependent $dependent)
    {
        $this->dependents->removeElement($dependent);
    }

    /**
     * Get dependents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDependents()
    {
        return $this->dependents;
    }
}
