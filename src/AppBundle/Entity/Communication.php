<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Communication
 */
class Communication
{
    const TYPE_SKYPE = 100;
    const TYPE_MOBILE_PHONE = 200;
    const TYPE_HOME_PHONE = 300;
    const TYPE_WORK_PHONE = 400;
    const TYPE_LINKED_IN = 500;
    const TYPE_FACEBOOK = 600;
    const TYPE_EMAIL = 700;

    public static function getCommunicationTypes()
    {
        return [
            self::TYPE_SKYPE        => 'skype',
            self::TYPE_MOBILE_PHONE => 'mobile phone number',
            self::TYPE_HOME_PHONE   => 'home phone number',
            self::TYPE_WORK_PHONE   => 'work phone number',
            self::TYPE_LINKED_IN    => 'linked in account',
            self::TYPE_FACEBOOK     => 'facebook',
            self::TYPE_EMAIL        => 'email',
        ];
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var string
     */
    private $value;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Communication
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Communication
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }
}
