<?php

namespace AppBundle\Entity;

use AppBundle\Interfaces\ArchivableInterface;
use AppBundle\Interfaces\SetArchiveFalseInterface;
use AppBundle\Traits\Archivable\Archivable;
use AppBundle\Traits\Archivable\SetArchiveFalseTrait;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * UserCreditline
 */
class UserCreditline implements ArchivableInterface, SetArchiveFalseInterface
{
    use Timestampable;
    use Archivable;
    use SetArchiveFalseTrait;

    const CREDIT_CARD_TYPE = 100;
    const PERSONAL_LOAN_TYPE = 200;
    const STORE_CARD_TYPE = 300;
    const MORTGAGE_TYPE = 400;
    const CAR_FINANCE_TYPE = 500;
    const HIRE_PURCHASE_TYPE = 600;
    const OVERDRAFT_TYPE = 700;
    const PAY_DAY_TYPE = 800;
    const FRIENDS_AND_FAMILY_TYPE = 900;

    public static function getCreditLineTypesMap()
    {
        return [
            self::CREDIT_CARD_TYPE        => 'Credit card',
            self::PERSONAL_LOAN_TYPE      => 'Personal loan',
            self::STORE_CARD_TYPE         => 'Store card',
            self::MORTGAGE_TYPE           => 'Mortgage',
            self::CAR_FINANCE_TYPE        => 'Car finance',
            self::HIRE_PURCHASE_TYPE      => 'Hire purchase',
            self::OVERDRAFT_TYPE          => 'Overdraft',
            self::PAY_DAY_TYPE            => 'Payday',
            self::FRIENDS_AND_FAMILY_TYPE => 'Friends & Family'
        ];
    }

    public static function getCreditLineTypes()
    {
        return array_keys(self::getCreditLineTypesMap());
    }

//   fields generated with service '@app.utils.refinancing_computer'
    /**
     * @var integer
     */
    public $creditBalanceNow;
    /**
     * @var integer
     */
    public $totalRepaid;
    /**
     * @var integer
     */
    public $totalInterest;
    /**
     * @var integer
     */
    public $totalMonthsToClear;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $creditLimit;

    /**
     * @var integer
     */
    private $balanceInput;

    /**
     * @var string
     */
    private $apr;

    /**
     * @var integer
     */
    private $monthlyPayment;

    /**
     * @var integer
     */
    private $settleBalance;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return UserCreditline
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return UserCreditline
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set creditLimit
     *
     * @param integer $creditLimit
     *
     * @return UserCreditline
     */
    public function setCreditLimit($creditLimit)
    {
        $this->creditLimit = $creditLimit;

        return $this;
    }

    /**
     * Get creditLimit
     *
     * @return integer
     */
    public function getCreditLimit()
    {
        return $this->creditLimit;
    }

    /**
     * Set balanceInput
     *
     * @param integer $balanceInput
     *
     * @return UserCreditline
     */
    public function setBalanceInput($balanceInput)
    {
        $this->balanceInput = $balanceInput;

        return $this;
    }

    /**
     * Get balanceInput
     *
     * @return integer
     */
    public function getBalanceInput()
    {
        return $this->balanceInput;
    }

    /**
     * Set apr
     *
     * @param string $apr
     *
     * @return UserCreditline
     */
    public function setApr($apr)
    {
        $this->apr = $apr;

        return $this;
    }

    /**
     * Get apr
     *
     * @return string
     */
    public function getApr()
    {
        return $this->apr;
    }

    /**
     * Set monthlyPayment
     *
     * @param integer $monthlyPayment
     *
     * @return UserCreditline
     */
    public function setMonthlyPayment($monthlyPayment)
    {
        $this->monthlyPayment = $monthlyPayment;

        return $this;
    }

    /**
     * Get monthlyPayment
     *
     * @return integer
     */
    public function getMonthlyPayment()
    {
        return $this->monthlyPayment;
    }

    /**
     * Set settleBalance
     *
     * @param integer $settleBalance
     *
     * @return UserCreditline
     */
    public function setSettleBalance($settleBalance)
    {
        $this->settleBalance = $settleBalance;

        return $this;
    }

    /**
     * Get settleBalance
     *
     * @return integer
     */
    public function getSettleBalance()
    {
        return $this->settleBalance;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return UserCreditline
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
