<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 8/22/16
 * Time: 3:02 PM
 */

namespace AppBundle\Entity;


use AppBundle\Interfaces\ArchivableInterface;
use AppBundle\Interfaces\SetArchiveFalseInterface;
use AppBundle\Traits\Archivable\Archivable;
use AppBundle\Traits\Archivable\SetArchiveFalseTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

class QuoteIncome implements ArchivableInterface, SetArchiveFalseInterface
{
    use Timestampable;
    use Archivable;
    use SetArchiveFalseTrait;

    const RATE_POOR = 10;
    const RATE_AVERAGE = 20;
    const RATE_GOOD = 30;
    const RATE_EXCELLENT = 40;

    public static function getRates()
    {
        return [
            self::RATE_POOR,
            self::RATE_AVERAGE,
            self::RATE_GOOD,
            self::RATE_EXCELLENT
        ];
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $totalGrossAnnual;

    /**
     * @var string
     */
    private $mainSource;

    /**
     * @var \AppBundle\Entity\QuoteApply
     */
    private $quoteApply;

    /**
     * @var boolean
     */
    private $isHomeowner;

    /**
     * CCJ - County Court Judgment
     * @var boolean
     */
    private $hasCCJ;

    /**
     * @var boolean
     */
    private $hasCurrentDefault;

    /**
     * @var integer
     */
    private $rate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quoteApply
     *
     * @param \AppBundle\Entity\QuoteApply $quoteApply
     * @return QuoteIncome
     */
    public function setQuoteApply(\AppBundle\Entity\QuoteApply $quoteApply = null)
    {
        $this->quoteApply = $quoteApply;

        return $this;
    }

    /**
     * Get quoteApply
     *
     * @return \AppBundle\Entity\QuoteApply 
     */
    public function getQuoteApply()
    {
        return $this->quoteApply;
    }

    /**
     * Set totalGrossAnnual
     *
     * @param integer $totalGrossAnnual
     * @return QuoteIncome
     */
    public function setTotalGrossAnnual($totalGrossAnnual)
    {
        $this->totalGrossAnnual = $totalGrossAnnual;

        return $this;
    }

    /**
     * Get totalGrossAnnual
     *
     * @return integer 
     */
    public function getTotalGrossAnnual()
    {
        return $this->totalGrossAnnual;
    }

    /**
     * Set mainSource
     *
     * @param string $mainSource
     * @return QuoteIncome
     */
    public function setMainSource($mainSource)
    {
        $this->mainSource = $mainSource;

        return $this;
    }

    /**
     * Get mainSource
     *
     * @return string 
     */
    public function getMainSource()
    {
        return $this->mainSource;
    }

    /**
     * Set isHomeowner
     *
     * @param boolean $isHomeowner
     * @return QuoteIncome
     */
    public function setIsHomeowner($isHomeowner)
    {
        $this->isHomeowner = $isHomeowner;

        return $this;
    }

    /**
     * Get isHomeowner
     *
     * @return boolean 
     */
    public function getIsHomeowner()
    {
        return $this->isHomeowner;
    }

    /**
     * Set hasCCJ
     *
     * @param boolean $hasCCJ
     * @return QuoteIncome
     */
    public function setHasCCJ($hasCCJ)
    {
        $this->hasCCJ = $hasCCJ;

        return $this;
    }

    /**
     * Get hasCCJ
     *
     * @return boolean 
     */
    public function getHasCCJ()
    {
        return $this->hasCCJ;
    }

    /**
     * Set hasCurrentDefault
     *
     * @param boolean $hasCurrentDefault
     * @return QuoteIncome
     */
    public function setHasCurrentDefault($hasCurrentDefault)
    {
        $this->hasCurrentDefault = $hasCurrentDefault;

        return $this;
    }

    /**
     * Get hasCurrentDefault
     *
     * @return boolean 
     */
    public function getHasCurrentDefault()
    {
        return $this->hasCurrentDefault;
    }

    /**
     * Set rate
     *
     * @param integer $rate
     * @return QuoteIncome
     */
    public function setRate($rate)
    {
        if (in_array($rate, self::getRates())) {
            $this->rate = $rate;
        }


        return $this;
    }

    /**
     * Get rate
     *
     * @return integer 
     */
    public function getRate()
    {
        return $this->rate;
    }
}
