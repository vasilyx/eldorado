<?php
namespace AppBundle\Entity;


class InvestOfferProcess extends BaseProcess
{
    private $investOffer;

    /**
     * Set investOffer
     *
     * @param \AppBundle\Entity\InvestOffer $investOffer
     *
     * @return InvestOfferProcess
     */
    public function setInvestOffer(\AppBundle\Entity\InvestOffer $investOffer = null)
    {
        $this->investOffer = $investOffer;

        return $this;
    }

    /**
     * Get investOffer
     *
     * @return \AppBundle\Entity\InvestOffer
     */
    public function getInvestOffer()
    {
        return $this->investOffer;
    }
}
