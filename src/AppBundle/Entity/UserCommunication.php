<?php

namespace AppBundle\Entity;

use AppBundle\Traits\Archivable\Archivable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * UserCommunication
 */
class UserCommunication
{
    use Archivable;
    use Timestampable;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $value;

    /**
     * @var integer
     */
    private $isVerified;

    /**
     * @var \AppBundle\Entity\Communication
     */
    private $communication;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var string
     */
    private $verificationCode;

    /**
     * @var boolean
     */
    private $isPrimary = '0';

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return UserCommunication
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set isVerified
     *
     * @param integer $isVerified
     * @return UserCommunication
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * Get isVerified
     *
     * @return integer
     */
    public function getIsVerified()
    {
        return $this->isVerified;
    }

    /**
     * Set communication
     *
     * @param \AppBundle\Entity\Communication $communication
     * @return UserCommunication
     */
    public function setCommunication(\AppBundle\Entity\Communication $communication = null)
    {
        $this->communication = $communication;

        return $this;
    }

    /**
     * Get communication
     *
     * @return \AppBundle\Entity\Communication
     */
    public function getCommunication()
    {
        return $this->communication;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return UserCommunication
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $user->addCommunication($this);
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getVerificationCode()
    {
        return $this->verificationCode;
    }

    /**
     * @param string $verificationCode
     * @return UserCommunication
     */
    public function setVerificationCode($verificationCode)
    {
        $this->verificationCode = $verificationCode;
        return $this;
    }

    /**
     * Set isPrimary
     *
     * @param boolean $isPrimary
     *
     * @return UserCommunication
     */
    public function setIsPrimary($isPrimary)
    {
        $this->isPrimary = $isPrimary;

        return $this;
    }

    /**
     * Get isPrimary
     *
     * @return boolean
     */
    public function getIsPrimary()
    {
        return $this->isPrimary;
    }
}
