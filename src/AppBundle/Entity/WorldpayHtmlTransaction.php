<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 03.01.17
 * Time: 11:51
 */

namespace AppBundle\Entity;


/**
 * Class WorldpayHtmlTransaction
 * @package AppBundle\Entity
 * @author Mikhail Pegasin <mikhail.pegasin@gmail.com>
 */
class WorldpayHtmlTransaction extends WorldpayTransaction
{
    const TRANS_STATUS_SUCCESS = 'Y';
    const TRANS_STATUS_CANCEL = 'C';
    /**
     * @var integer
     */
    private $id;
    /**
     * @var string
     */
    protected $callbackUrl;

    /**
     * Installation ID
     *
     * @var string
     */
    protected $installationId;

    /**
     * @var string
     */
    protected $paymentResponse;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCallbackUrl()
    {
        return $this->callbackUrl;
    }

    /**
     * @param string $callbackUrl
     * @return WorldpayHtmlTransaction
     */
    public function setCallbackUrl($callbackUrl)
    {
        $this->callbackUrl = $callbackUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getInstallationId()
    {
        return $this->installationId;
    }

    /**
     * @param string $installationId
     * @return WorldpayHtmlTransaction
     */
    public function setInstallationId($installationId)
    {
        $this->installationId = $installationId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentResponse()
    {
        return $this->paymentResponse;
    }

    /**
     * @param string $paymentResponse
     * @return WorldpayHtmlTransaction
     */
    public function setPaymentResponse($paymentResponse)
    {
        $this->paymentResponse = $paymentResponse;
        return $this;
    }
}
