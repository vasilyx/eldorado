<?php

namespace AppBundle\Entity;

use AppBundle\Interfaces\ArchivableInterface;
use AppBundle\Interfaces\SetArchiveFalseInterface;
use AppBundle\Traits\Archivable\Archivable;
use AppBundle\Traits\Archivable\SetArchiveFalseTrait;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * UserHousehold
 */
class UserHousehold implements ArchivableInterface, SetArchiveFalseInterface
{
    use Timestampable;
    use Archivable;
    use SetArchiveFalseTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $spouseFirstName;

    /**
     * @var string
     */
    private $monthlyRent;

    /**
     * @var string
     */
    private $monthlyUtilities;

    /**
     * @var string
     */
    private $monthlyTravel;

    /**
     * @var string
     */
    private $monthlyFood;

    /**
     * @var string
     */
    private $monthlySavings;

    /**
     * @var string
     */
    private $monthlyEntertainment;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set spouseFirstName
     *
     * @param string $spouseFirstName
     * @return UserHousehold
     */
    public function setSpouseFirstName($spouseFirstName)
    {
        $this->spouseFirstName = $spouseFirstName;

        return $this;
    }

    /**
     * Get spouseFirstName
     *
     * @return string 
     */
    public function getSpouseFirstName()
    {
        return $this->spouseFirstName;
    }

    /**
     * Set monthlyRent
     *
     * @param string $monthlyRent
     * @return UserHousehold
     */
    public function setMonthlyRent($monthlyRent)
    {
        $this->monthlyRent = $monthlyRent;

        return $this;
    }

    /**
     * Get monthlyRent
     *
     * @return string 
     */
    public function getMonthlyRent()
    {
        return $this->monthlyRent;
    }

    /**
     * Set monthlyUtilities
     *
     * @param string $monthlyUtilities
     * @return UserHousehold
     */
    public function setMonthlyUtilities($monthlyUtilities)
    {
        $this->monthlyUtilities = $monthlyUtilities;

        return $this;
    }

    /**
     * Get monthlyUtilities
     *
     * @return string 
     */
    public function getMonthlyUtilities()
    {
        return $this->monthlyUtilities;
    }

    /**
     * Set monthlyTravel
     *
     * @param string $monthlyTravel
     * @return UserHousehold
     */
    public function setMonthlyTravel($monthlyTravel)
    {
        $this->monthlyTravel = $monthlyTravel;

        return $this;
    }

    /**
     * Get monthlyTravel
     *
     * @return string 
     */
    public function getMonthlyTravel()
    {
        return $this->monthlyTravel;
    }

    /**
     * Set monthlyFood
     *
     * @param string $monthlyFood
     * @return UserHousehold
     */
    public function setMonthlyFood($monthlyFood)
    {
        $this->monthlyFood = $monthlyFood;

        return $this;
    }

    /**
     * Get monthlyFood
     *
     * @return string 
     */
    public function getMonthlyFood()
    {
        return $this->monthlyFood;
    }

    /**
     * Set monthlySavings
     *
     * @param string $monthlySavings
     * @return UserHousehold
     */
    public function setMonthlySavings($monthlySavings)
    {
        $this->monthlySavings = $monthlySavings;

        return $this;
    }

    /**
     * Get monthlySavings
     *
     * @return string 
     */
    public function getMonthlySavings()
    {
        return $this->monthlySavings;
    }

    /**
     * Set monthlyEntertainment
     *
     * @param string $monthlyEntertainment
     * @return UserHousehold
     */
    public function setMonthlyEntertainment($monthlyEntertainment)
    {
        $this->monthlyEntertainment = $monthlyEntertainment;

        return $this;
    }

    /**
     * Get monthlyEntertainment
     *
     * @return string 
     */
    public function getMonthlyEntertainment()
    {
        return $this->monthlyEntertainment;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return UserHousehold
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
