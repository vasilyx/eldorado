<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * QuoteApply
 */
class QuoteApply
{
    use Timestampable;

    const GUAGE_SCORE_APPROVE = 501;
    const GUAGE_SCORE_REJECT = 490;

    const STATUS_DRAFT = 10;
    const STATUS_FUNDING_REQUIRED = 15;
    const STATUS_PENDING_FUNDS = 16;
    const STATUS_PENDING = 20;
    const STATUS_REFER = 30;
    const STATUS_REJECTED = 40;
    const STATUS_APPROVED = 50;
    const STATUS_VERIFICATION = 60;
    const STATUS_DECLINED = 70;
    const STATUS_ACTIVE = 80;
    const STATUS_CLOSED = 100;

    public static function getAvailableStatuses()
    {
        return array_keys(self::getStatusesMap());
    }

    public static function getStatusesMap()
    {
        return [
            self::STATUS_DRAFT => 'Draft',
            self::STATUS_FUNDING_REQUIRED => 'Funding required',
            self::STATUS_PENDING_FUNDS => 'Pending funds',
            self::STATUS_PENDING => 'Pending',
            self::STATUS_REFER => 'Refer',
            self::STATUS_REJECTED => 'Rejected',
            self::STATUS_APPROVED => 'Approved',
            self::STATUS_VERIFICATION => 'Verification',
            self::STATUS_DECLINED => 'Declined',
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_CLOSED => 'Closed',
        ];
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \AppBundle\Entity\UserProduct
     */
    private $userProduct;

    private $status;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Product
     */
    private $product;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userProduct
     *
     * @param \AppBundle\Entity\UserProduct $userProduct
     * @return QuoteApply
     */
    public function setUserProduct(\AppBundle\Entity\UserProduct $userProduct = null)
    {
        $this->userProduct = $userProduct;

        return $this;
    }

    /**
     * Get userProduct
     *
     * @return \AppBundle\Entity\UserProduct
     */
    public function getUserProduct()
    {
        return $this->userProduct;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return QuoteApply
     * @throws \Exception
     */
    public function setStatus($status)
    {
        if (in_array($status, self::getAvailableStatuses())) {
            $this->status = $status;
        } else {
            throw new \Exception("Status is not available");
        }

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return QuoteApply
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return QuoteApply
     */
    public function setProduct($product)
    {
        $this->product = $product;
        return $this;
    }
}
