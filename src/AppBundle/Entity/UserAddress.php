<?php

namespace AppBundle\Entity;

use AppBundle\Interfaces\ArchivableInterface;
use AppBundle\Interfaces\SetArchiveFalseInterface;
use AppBundle\Interfaces\SoftDeletableInterface;
use AppBundle\Traits\Archivable\Archivable;
use AppBundle\Traits\Archivable\SetArchiveFalseTrait;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\SoftDeletable\SoftDeletable;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * UserAddress
 */
class UserAddress implements SoftDeletableInterface, ArchivableInterface, SetArchiveFalseInterface
{
    use Archivable;
    use Timestampable;
    use SoftDeletable;
    use SetArchiveFalseTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $postalCode;

    /**
     * @var integer
     */
    private $addressNumber;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $buildingName;

    /**
     * @var string
     */
    private $buildingNumber;

    /**
     * @var string
     */
    private $subBuildingName;

    /**
     * @var string
     */
    private $dependantLocality;

    /**
     * @var string
     */
    private $line1;

    /**
     * @var string
     */
    private $line2;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $state;

    /**
     * @var \DateTime
     */
    private $moved;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     * @return UserAddress
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @return string
     */
    public function getOutwardCodePart()
    {
        if ($this->postalCode) {
            return explode(' ', $this->postalCode)[0];
        }
    }

    /**
     * Set addressNumber
     *
     * @param integer $addressNumber
     * @return UserAddress
     */
    public function setAddressNumber($addressNumber)
    {
        $this->addressNumber = $addressNumber;

        return $this;
    }

    /**
     * Get addressNumber
     *
     * @return integer
     */
    public function getAddressNumber()
    {
        return $this->addressNumber;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return UserAddress
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return UserAddress
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set buildingNumber
     *
     * @param string $buildingNumber
     * @return UserAddress
     */
    public function setBuildingNumber($buildingNumber)
    {
        $this->buildingNumber = $buildingNumber;

        return $this;
    }

    /**
     * Get buildingNumber
     *
     * @return string
     */
    public function getBuildingNumber()
    {
        return $this->buildingNumber;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return UserAddress
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return UserAddress
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return UserAddress
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set moved
     *
     * @param \DateTime $moved
     * @return UserAddress
     */
    public function setMoved(\DateTime $moved)
    {
        $this->moved = $moved;

        return $this;
    }

    /**
     * Get moved
     *
     * @return \DateTime
     */
    public function getMoved()
    {
        return $this->moved;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return UserAddress
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $user->addAddress($this);
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set buildingName
     *
     * @param string $buildingName
     * @return UserAddress
     */
    public function setBuildingName($buildingName)
    {
        $this->buildingName = $buildingName;

        return $this;
    }

    /**
     * Get buildingName
     *
     * @return string
     */
    public function getBuildingName()
    {
        return $this->buildingName;
    }

    /**
     * Set subBuildingName
     *
     * @param string $subBuildingName
     * @return UserAddress
     */
    public function setSubBuildingName($subBuildingName)
    {
        $this->subBuildingName = $subBuildingName;

        return $this;
    }

    /**
     * Get subBuildingName
     *
     * @return string
     */
    public function getSubBuildingName()
    {
        return $this->subBuildingName;
    }

    /**
     * Set dependantLocality
     *
     * @param string $dependantLocality
     * @return UserAddress
     */
    public function setDependantLocality($dependantLocality)
    {
        $this->dependantLocality = $dependantLocality;

        return $this;
    }

    /**
     * Get dependantLocality
     *
     * @return string
     */
    public function getDependantLocality()
    {
        return $this->dependantLocality;
    }

    /**
     * Set line1
     *
     * @param string $line1
     * @return UserAddress
     */
    public function setLine1($line1)
    {
        $this->line1 = $line1;

        return $this;
    }

    /**
     * Get line1
     *
     * @return string
     */
    public function getLine1()
    {
        return $this->line1;
    }

    /**
     * Set line2
     *
     * @param string $line2
     * @return UserAddress
     */
    public function setLine2($line2)
    {
        $this->line2 = $line2;

        return $this;
    }

    /**
     * Get line2
     *
     * @return string
     */
    public function getLine2()
    {
        return $this->line2;
    }
}
