<?php

namespace AppBundle\Entity;

use AppBundle\Interfaces\SoftDeletableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\SoftDeletable\SoftDeletable;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * InvestOffer
 */
class InvestOffer implements SoftDeletableInterface
{
    use Timestampable;
    use SoftDeletable;

    const INVEST_TYPE_ISA = 100;
    const INVEST_TYPE_SIPP = 200;
    const INVEST_TYPE_GEN = 300;
    const INVEST_TYPE_PIONEER = 400;

    const FINANCE_TYPE_ORIGIN = 100;

    const INVEST_FREQ_MONTHLY = 100;
    const INVEST_FREQ_QUARTERLY = 200;
    const INVEST_FREQ_ONE_OFF = 300;

    const INVEST_TERM_YEAR = 12;
    const INVEST_TERM_YEAR_AND_HALF = 18;
    const INVEST_TERM_TWO_YEARS = 24;
    const INVEST_TERM_THREE_YEARS = 36;
    const INVEST_TERM_FOUR_YEARS = 48;
    const INVEST_TERM_FIVE_YEARS = 60;
    const INVEST_TERM_ONGOING = 1000;

    const CASH_BACK_TYPE_SECONDARY_MARKET = 100;
    const CASH_BACK_TYPE_FIXED_TERM_PAYOUT = 200;

    /**
     *	offer was created by there is no assassinated payment
     */
    const STATUS_DRAFT = 100;
    /**
     * Payment was done and now the System is waiting while deposit will be confirmed
     */
    const STATUS_PENDING_FUNDS = 200;
    /**
     * Payment was done but quote isn't active
     */
    const STATUS_PENDING = 250;
    /**
     * Offer is available in marketplace
     */
    const STATUS_ACTIVE = 300;
    /**
     * 	Offer is ended and all money was paid to the client.
     */
    const STATUS_SETTLED = 400;
    /**
     * 	Offer has been canceled
     */
    const STATUS_CANCEL = 500;

    public static function getInvestOfferStatusMap()
    {
        return [
            self::STATUS_DRAFT          => 'draft',
            self::STATUS_PENDING_FUNDS  => 'pending_funds',
            self::STATUS_PENDING        => 'pending',
            self::STATUS_ACTIVE         => 'active',
            self::STATUS_SETTLED        => 'settled',
            self::STATUS_CANCEL         => 'cancel'
        ];
    }

    public static function getInvestmentTypesMap()
    {
        return [
            self::INVEST_TYPE_ISA       =>  'ISA',
            self::INVEST_TYPE_SIPP      =>  'SIPP',
            self::INVEST_TYPE_GEN       =>  'GEN',
            self::INVEST_TYPE_PIONEER   =>  'Pioneer'
        ];
    }

    public static function getInvestmentTypes()
    {
        return array_keys(self::getInvestmentTypesMap());
    }

    public static function getFinanceTypesMap()
    {
        return [
            self::FINANCE_TYPE_ORIGIN   =>  'Origin'
        ];
    }

    public static function getFinanceTypes()
    {
        return array_keys(self::getFinanceTypesMap());
    }

    public static function getInvestFreqMap()
    {
        return [
            self::INVEST_FREQ_MONTHLY       => 'monthly',
            self::INVEST_FREQ_QUARTERLY     => 'quarterly',
            self::INVEST_FREQ_ONE_OFF       => 'one-off'
        ];
    }

    public static function getInvestFreq()
    {
        return array_keys(self::getInvestFreqMap());
    }

    public static function getInvestTermsMap()
    {
        return [
            self::INVEST_TERM_YEAR          => '12-month',
            self::INVEST_TERM_YEAR_AND_HALF => '18-month',
            self::INVEST_TERM_TWO_YEARS     => '2-years',
            self::INVEST_TERM_THREE_YEARS   => '3-years',
            self::INVEST_TERM_FOUR_YEARS    => '4-years',
            self::INVEST_TERM_FIVE_YEARS    => '5-years',
            self::INVEST_TERM_ONGOING       => 'ongoing'
        ];
    }

    public static function getRatesByTerms()
    {
        return [
            self::INVEST_TERM_YEAR          => '3.4',
            self::INVEST_TERM_YEAR_AND_HALF => '3.9',
            self::INVEST_TERM_TWO_YEARS     => '4.2',
            self::INVEST_TERM_THREE_YEARS   => '4.7',
            self::INVEST_TERM_FOUR_YEARS    => '5.2',
            self::INVEST_TERM_FIVE_YEARS    => '5.7',
            self::INVEST_TERM_ONGOING       => '5.7'
        ];
    }

    public static function getInvestTerms()
    {
        return array_keys(self::getInvestTermsMap());
    }

    public static function getCashBackTypesMap()
    {
        return [
            self::CASH_BACK_TYPE_SECONDARY_MARKET       =>  'secondary market',
            self::CASH_BACK_TYPE_FIXED_TERM_PAYOUT      =>  'fixed term payout'
        ];
    }

    public static function getCashBackTypes()
    {
        return array_keys(self::getCashBackTypesMap());
    }
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $investmentType;

    /**
     * @var string
     */
    private $financeType;

    /**
     * @var integer
     */
    private $investmentAmount;

    /**
     * @var string
     */
    private $investmentFrequency;

    /**
     * @var integer
     */
    private $term;

    /**
     * @var string
     */
    private $prioritisationTags;

    /**
     * @var string
     */
    private $restrictionTags;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var string
     */
    private $rate;

    /**
     * @var boolean
     */
    private $confirmInvest;

    /**
     * @var boolean
     */
    private $riskConfirm;

    /**
     * @var boolean
     */
    private $acceptTerms;

    /**
     * @var integer
     */
    private $investmentName;

    /**
     * @var ArrayCollection
     */
    private $restrictions;

    /**
     * @var ArrayCollection
     */
    private $priorities;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set investmentType
     *
     * @param string $investmentType
     * @return InvestOffer
     */
    public function setInvestmentType($investmentType)
    {
        $this->investmentType = $investmentType;

        return $this;
    }

    /**
     * Get investmentType
     *
     * @return string 
     */
    public function getInvestmentType()
    {
        return $this->investmentType;
    }

    /**
     * Set financeType
     *
     * @param string $financeType
     * @return InvestOffer
     */
    public function setFinanceType($financeType)
    {
        $this->financeType = $financeType;

        return $this;
    }

    /**
     * Get financeType
     *
     * @return string 
     */
    public function getFinanceType()
    {
        return $this->financeType;
    }

    /**
     * Set investmentAmount
     *
     * @param integer $investmentAmount
     * @return InvestOffer
     */
    public function setInvestmentAmount($investmentAmount)
    {
        $this->investmentAmount = $investmentAmount;

        return $this;
    }

    /**
     * Get investmentAmount
     *
     * @return integer 
     */
    public function getInvestmentAmount()
    {
        return $this->investmentAmount;
    }

    /**
     * Set investmentFrequency
     *
     * @param string $investmentFrequency
     * @return InvestOffer
     */
    public function setInvestmentFrequency($investmentFrequency)
    {
        $this->investmentFrequency = $investmentFrequency;

        return $this;
    }

    /**
     * Get investmentFrequency
     *
     * @return string 
     */
    public function getInvestmentFrequency()
    {
        return $this->investmentFrequency;
    }

    /**
     * Set term
     *
     * @param integer $term
     * @return InvestOffer
     */
    public function setTerm($term)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return integer 
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return InvestOffer
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set rate
     *
     * @param string $rate
     * @return InvestOffer
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return string 
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set confirmInvest
     *
     * @param boolean $confirmInvest
     * @return InvestOffer
     */
    public function setConfirmInvest($confirmInvest)
    {
        $this->confirmInvest = $confirmInvest;

        return $this;
    }

    /**
     * Get confirmInvest
     *
     * @return boolean 
     */
    public function getConfirmInvest()
    {
        return $this->confirmInvest;
    }

    /**
     * Set riskConfirm
     *
     * @param boolean $riskConfirm
     * @return InvestOffer
     */
    public function setRiskConfirm($riskConfirm)
    {
        $this->riskConfirm = $riskConfirm;

        return $this;
    }

    /**
     * Get riskConfirm
     *
     * @return boolean 
     */
    public function getRiskConfirm()
    {
        return $this->riskConfirm;
    }

    /**
     * Set acceptTerms
     *
     * @param boolean $acceptTerms
     * @return InvestOffer
     */
    public function setAcceptTerms($acceptTerms)
    {
        $this->acceptTerms = $acceptTerms;

        return $this;
    }

    /**
     * Get acceptTerms
     *
     * @return boolean 
     */
    public function getAcceptTerms()
    {
        return $this->acceptTerms;
    }


    /**
     * Set investmentName
     *
     * @param string $investmentName
     * @return InvestOffer
     */
    public function setInvestmentName($investmentName)
    {
        $this->investmentName = $investmentName;

        return $this;
    }

    /**
     * Get investmentName
     *
     * @return string 
     */
    public function getInvestmentName()
    {
        return $this->investmentName;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->restrictions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->priorities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add restriction
     *
     * @param \AppBundle\Entity\Restriction $restriction
     *
     * @return InvestOffer
     */
    public function addRestriction(\AppBundle\Entity\Restriction $restriction)
    {
        $this->restrictions[] = $restriction;

        return $this;
    }

    /**
     * Remove restriction
     *
     * @param \AppBundle\Entity\Restriction $restriction
     */
    public function removeRestriction(\AppBundle\Entity\Restriction $restriction)
    {
        $this->restrictions->removeElement($restriction);
    }

    /**
     * Get restrictions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRestrictions()
    {
        return $this->restrictions;
    }

    /**
     * Add priority
     *
     * @param \AppBundle\Entity\Restriction $priority
     *
     * @return InvestOffer
     */
    public function addPriority(\AppBundle\Entity\Restriction $priority)
    {
        $this->priorities[] = $priority;

        return $this;
    }

    /**
     * Remove priority
     *
     * @param \AppBundle\Entity\Restriction $priority
     */
    public function removePriority(\AppBundle\Entity\Restriction $priority)
    {
        $this->priorities->removeElement($priority);
    }

    /**
     * Get priorities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPriorities()
    {
        return $this->priorities;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return InvestOffer
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $investOfferSub;


    /**
     * Add investOfferSub
     *
     * @param \AppBundle\Entity\InvestOfferSub $investOfferSub
     *
     * @return InvestOffer
     */
    public function addInvestOfferSub(\AppBundle\Entity\InvestOfferSub $investOfferSub)
    {
        $this->investOfferSub[] = $investOfferSub;

        return $this;
    }

    /**
     * Remove investOfferSub
     *
     * @param \AppBundle\Entity\InvestOfferSub $investOfferSub
     */
    public function removeInvestOfferSub(\AppBundle\Entity\InvestOfferSub $investOfferSub)
    {
        $this->investOfferSub->removeElement($investOfferSub);
    }

    /**
     * Get investOfferSub
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvestOfferSub()
    {
        return $this->investOfferSub;
    }
}
