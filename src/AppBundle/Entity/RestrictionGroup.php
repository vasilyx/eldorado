<?php
/**
 * User: Vladimir
 * Date: 2/1/17
 * Time: 1:55 PM
 */

namespace AppBundle\Entity;


class RestrictionGroup
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $restrictions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->restrictions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getCode();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return RestrictionGroup
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return RestrictionGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add restriction
     *
     * @param \AppBundle\Entity\Restriction $restriction
     *
     * @return RestrictionGroup
     */
    public function addRestriction(\AppBundle\Entity\Restriction $restriction)
    {
        $this->restrictions[] = $restriction;

        return $this;
    }

    /**
     * Remove restriction
     *
     * @param \AppBundle\Entity\Restriction $restriction
     */
    public function removeRestriction(\AppBundle\Entity\Restriction $restriction)
    {
        $this->restrictions->removeElement($restriction);
    }

    /**
     * Get restrictions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRestrictions()
    {
        return $this->restrictions;
    }
}
