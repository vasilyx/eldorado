<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 07.12.16
 * Time: 14:15
 */

namespace AppBundle\Entity;


use AppBundle\Interfaces\ArchivableInterface;
use AppBundle\Traits\Archivable\Archivable;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

class BankDetails implements ArchivableInterface
{
    use Archivable;
    use Timestampable;
    /**
     * @var integer
     */
    private $id;
    /**
     * @var string
     */
    private $bankSortCode;

    /**
     * @var string
     */
    private $bankAccountNumber;

    /**
     * @var string
     */
    private $bankAccountName;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getBankSortCode()
    {
        return $this->bankSortCode;
    }

    /**
     * @param string $bankSortCode
     *
     * @return $this
     */
    public function setBankSortCode($bankSortCode)
    {
        $this->bankSortCode = $bankSortCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getBankAccountNumber()
    {
        return $this->bankAccountNumber;
    }

    /**
     * @param string $bankAccountNumber
     *
     * @return $this
     */
    public function setBankAccountNumber($bankAccountNumber)
    {
        $this->bankAccountNumber = $bankAccountNumber;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBankAccountName()
    {
        return $this->bankAccountName;
    }

    /**
     * @param string $bankAccountName
     * @return BankDetails
     */
    public function setBankAccountName($bankAccountName)
    {
        $this->bankAccountName = $bankAccountName;
        return $this;
    }
}
