<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 15.12.16
 * Time: 17:40
 */

namespace AppBundle\Entity;

/**
 * Class WorldpayJsonTransaction
 * https://developer.worldpay.com/jsonapi/api#orders
 * https://developer.worldpay.com/jsonapi/faq/articles/what-do-the-different-order-states-mean
 *
 * @package AppBundle\Entity
 */
class WorldpayJsonTransaction extends WorldpayTransaction
{
    /**
     * A unique token which the WorldPay.js library added to your checkout form, or obtained via the token API. This token represents the customer's card details/payment method which was stored on our server. One of token or paymentMethod must be specified
     *
     * @var string
     */
    protected $token;
    /**
     * The name of the cardholder or payee. Optional
     * @var string
     */
    protected $name;
    /**
     * The description of the order provided by you. Mandatory
     *
     * @var string
     */
    protected $orderDescription;

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return WorldPayTransaction
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

     /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return WorldPayTransaction
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderDescription()
    {
        return $this->orderDescription;
    }

    /**
     * @param string $orderDescription
     * @return WorldPayTransaction
     */
    public function setOrderDescription($orderDescription)
    {
        $this->orderDescription = $orderDescription;
        return $this;
    }
}
