<?php
namespace AppBundle\Entity;


class AgreementProcess extends BaseProcess
{
    private $agreement;

    /**
     * Set Agreement
     *
     * @param \AppBundle\Entity\Agreement $agreement
     *
     * @return AgreementProcess
     */
    public function setAgreement(\AppBundle\Entity\Agreement $agreement = null)
    {
        $this->agreement = $agreement;

        return $this;
    }

    /**
     * Get Agreement
     *
     * @return \AppBundle\Entity\Agreement
     */
    public function getAgreement()
    {
        return $this->agreement;
    }
}
