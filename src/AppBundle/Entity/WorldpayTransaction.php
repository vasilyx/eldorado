<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 03.01.17
 * Time: 11:54
 */

namespace AppBundle\Entity;


use AppBundle\Interfaces\SoftDeletableInterface;
use AppBundle\Traits\UserAssociated;
use Knp\DoctrineBehaviors\Model\SoftDeletable\SoftDeletable;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

abstract class WorldpayTransaction implements SoftDeletableInterface
{
    use Timestampable;
    use SoftDeletable;
    use UserAssociated;

    /**
     * The order was created successfully and the funds were charged to the customer's account
     */
    const PAYMENT_STATUS_SUCCESS = 100;
    /**
     * The order was not successful
     */
    const PAYMENT_STATUS_FAILED = 200;
    /**
     * A full or part refund has been requested for the order and is being processed
     */
    const PAYMENT_STATUS_SENT_FOR_REFUND = 300;
    /**
     * The order has been wholly refunded to the customer
     */
    const PAYMENT_STATUS_REFUNDED = 400;
    /**
     * Part of the order value has been refunded to the customer
     */
    const PAYMENT_STATUS_PARTIALLY_REFUNDED = 500;
    /**
     * The order was authorised by the payment provider and the funds are reserved on the customer's account awaiting capture
     */
    const PAYMENT_STATUS_AUTHORIZED = 600;
    /**
     * The authorization has been cancelled and the funds released to the customer's account
     */
    const PAYMENT_STATUS_CANCELLED = 700;
    /**
     * The authorization expired and the funds released on the customer's account
     */
    const PAYMENT_STATUS_EXPIRED = 800;
    /**
     * The order value has been received from the payment provider
     */
    const PAYMENT_STATUS_SETTLED = 900;
    /**
     * The customer disputed this order and the money was returned
     */
    const PAYMENT_STATUS_CHARGED_BACK = 1000;
    /**
     * When WorldPay disputes a charge-back, we will ask you for supporting information and documentation
     */
    const PAYMENT_STATUS_INFORMATION_REQUESTED = 1100;
    /**
     * You have supplied us information with which to dispute the charge-back
     */
    const PAYMENT_STATUS_INFORMATION_SUPPLIED = 1200;

    const PAYMENT_STATUS_PENDING = 1300;

    const PAYMENT_STATUS_ERROR = 1400;


    public static function getPaymentStatusesMap()
    {
        return [
            static::PAYMENT_STATUS_SUCCESS                  => 'The order was created successfully and the funds were charged to the customer\'s account',
            static::PAYMENT_STATUS_FAILED                   => 'The order was not successful',
            static::PAYMENT_STATUS_SENT_FOR_REFUND          => 'A full or part refund has been requested for the order and is being processed',
            static::PAYMENT_STATUS_REFUNDED                 => 'The order has been wholly refunded to the customer',
            static::PAYMENT_STATUS_PARTIALLY_REFUNDED       => 'Part of the order value has been refunded to the customer',
            static::PAYMENT_STATUS_AUTHORIZED               => 'The order was authorised by the payment provider and the funds are reserved on the customer\'s account awaiting capture',
            static::PAYMENT_STATUS_CANCELLED                => 'The authorization has been cancelled and the funds released to the customer\'s account',
            static::PAYMENT_STATUS_EXPIRED                  => 'The authorization expired and the funds released on the customer\'s account',
            static::PAYMENT_STATUS_SETTLED                  => 'The order value has been received from the payment provider',
            static::PAYMENT_STATUS_CHARGED_BACK             => 'The customer disputed this order and the money was returned',
            static::PAYMENT_STATUS_INFORMATION_REQUESTED    => 'When WorldPay disputes a charge-back, we will ask you for supporting information and documentation',
            static::PAYMENT_STATUS_INFORMATION_SUPPLIED     => 'You have supplied us information with which to dispute the charge-back',
            static::PAYMENT_STATUS_PENDING                  => 'Pending',
            static::PAYMENT_STATUS_ERROR                    => 'Error'

        ];
    }

    public static function getPaymentStatusCodes()
    {
        return array_keys(static::getPaymentStatusesMap());
    }

    /**
     * @param User $user
     * @return static
     */
    public static function create(User $user)
    {
        return new static($user);
    }

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * The amount to be charged in pennies/cents (or whatever the smallest unit is of the currencyCode that you specified, see currencyCodeExponent below). Mandatory
     * @var integer
     */
    protected $amount;
    /**
     * The ISO currency code of the currency you want to charge your customer in. Mandatory
     *
     * @var string
     */
    protected $currencyCode;
    /**
     * A WorldPay generated unique code for the order. This order code will be referred to in all reports.
     *
     * @var string
     */
    protected $orderCode;
    /**
     * The current status of the Order. Only included in API response
     *
     * @var integer
     */
    protected $paymentStatus;

    /**
     * The code or ID under which this order is known in your systems. Optional
     *
     * @var string
     */
    protected $customerOrderCode;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param integer $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     * @return $this
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getoOrderCode()
    {
        return $this->orderCode;
    }

    /**
     * @param string $orderCode
     * @return $this
     */
    public function setOrderCode($orderCode)
    {
        $this->orderCode = $orderCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * @param int $paymentStatus
     * @return $this
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;
        return $this;
    }

    public function getCustomerOrderCode()
    {
        return $this->customerOrderCode;
    }

    public function setCustomerOrderCode($orderCode)
    {
        $this->customerOrderCode = $orderCode;
        return $this;
    }

    /**
     * Get orderCode
     *
     * @return string
     */
    public function getOrderCode()
    {
        return $this->orderCode;
    }
}
