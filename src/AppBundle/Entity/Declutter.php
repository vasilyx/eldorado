<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * Declutter
 */
class Declutter
{
    use Timestampable;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $needToClose = '0';

    /**
     * @var \AppBundle\Entity\UserCreditline
     */
    private $userCreditline;

    /**
     * @var \AppBundle\Entity\LoanApply
     */
    private $loanApply;

    /**
     * @var boolean
     */
    private $archive = '0';

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set needToClose
     *
     * @param boolean $needToClose
     *
     * @return Declutter
     */
    public function setNeedToClose($needToClose)
    {
        $this->needToClose = $needToClose;

        return $this;
    }

    /**
     * Get needToClose
     *
     * @return boolean
     */
    public function getNeedToClose()
    {
        return $this->needToClose;
    }

    /**
     * Set userCreditline
     *
     * @param \AppBundle\Entity\UserCreditline $userCreditline
     *
     * @return Declutter
     */
    public function setUserCreditline(\AppBundle\Entity\UserCreditline $userCreditline)
    {
        $this->userCreditline = $userCreditline;

        return $this;
    }

    /**
     * Get userCreditline
     *
     * @return \AppBundle\Entity\UserCreditline
     */
    public function getUserCreditline()
    {
        return $this->userCreditline;
    }

    /**
     * Set loanApply
     *
     * @param \AppBundle\Entity\LoanApply $loanApply
     *
     * @return Declutter
     */
    public function setLoanApply(\AppBundle\Entity\LoanApply $loanApply)
    {
        $this->loanApply = $loanApply;

        return $this;
    }

    /**
     * Get loanApply
     *
     * @return \AppBundle\Entity\LoanApply
     */
    public function getLoanApply()
    {
        return $this->loanApply;
    }

    /**
     * Set active
     *
     * @param boolean $archive
     *
     * @return Declutter
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;

        return $this;
    }

    /**
     * Get archive
     *
     * @return boolean
     */
    public function getArchive()
    {
        return $this->archive;
    }
}
