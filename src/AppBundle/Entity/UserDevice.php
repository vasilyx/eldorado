<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserDevice
 */
class UserDevice
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var \AppBundle\Entity\UserMarketing
     */
    private $userMarketing;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return UserDevice
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userMarketing
     *
     * @param \AppBundle\Entity\UserMarketing $userMarketing
     * @return UserDevice
     */
    public function setUserMarketing(\AppBundle\Entity\UserMarketing $userMarketing = null)
    {
        $this->userMarketing = $userMarketing;

        return $this;
    }

    /**
     * Get userMarketing
     *
     * @return \AppBundle\Entity\UserMarketing 
     */
    public function getUserMarketing()
    {
        return $this->userMarketing;
    }
}
