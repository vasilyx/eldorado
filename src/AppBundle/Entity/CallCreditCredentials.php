<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 01.02.2017
 * Time: 11:47
 */

namespace AppBundle\Entity;


use AppBundle\Traits\StaticCreatable;

class CallCreditCredentials
{
    use StaticCreatable;

    const CALLREPORT_TYPE = 1;
    const CALLVALIDATE_TYPE = 2;
    const AFFORDABILITY_TYPE = 3;

    const MASTER_MODE = 'master';
    const SLAVE_MODE = 'slave';

    static public function getTypes()
    {
        return [
            self::CALLREPORT_TYPE => 'callreport',
            self::CALLVALIDATE_TYPE => 'callvalidate',
            self::AFFORDABILITY_TYPE => 'affordability'
        ];
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string;
     */
    private $company;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $applicationName;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return CallCreditCredentials
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set login
     *
     * @param string $login
     * @return CallCreditCredentials
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return CallCreditCredentials
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param int $type
     * @return CallCreditCredentials
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return CallCreditCredentials
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * Set url
     *
     * @param string $url
     * @return CallCreditCredentials
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get applicationName
     *
     * @return mixed
     */
    public function getApplicationName()
    {
        return $this->applicationName;
    }

    /**
     * Set applicationName
     *
     * @param string $applicationName
     * @return CallCreditCredentials
     */
    public function setApplicationName($applicationName)
    {
        $this->applicationName = $applicationName;

        return $this;
    }
}
