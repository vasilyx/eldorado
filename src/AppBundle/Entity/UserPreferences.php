<?php
/**
 * Created by PhpStorm.
 * User: mikhailpegasin
 * Date: 17.11.16
 * Time: 17:27
 */

namespace AppBundle\Entity;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserPreferences
{
    private $id;
    private $user;
    protected $receiveComms;
    protected $hasNoDependencies;

    public function __construct(array $preferences = [], $allTo = false)
    {
        $refl = new \ReflectionClass($this);
        if ($preferences) {
            foreach ($preferences as $key => $value) {
                $methodName = 'set' . ucfirst($key);
                if ($refl->hasMethod($methodName)) {
                    $this->$methodName($value);
                } else {
                    throw new NotFoundHttpException('Method ' . $methodName . 'is absent in ' . __CLASS__);
                }
            }
        } else {
            foreach ($refl->getProperties(\ReflectionProperty::IS_PROTECTED) as $property) {
                $this->{$property->getName()} = $allTo;
            }
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set receiveComms
     *
     * @param boolean $receiveComms
     * @return UserPreferences
     */
    public function setReceiveComms($receiveComms)
    {
        $this->receiveComms = $receiveComms;

        return $this;
    }

    /**
     * Get receiveComms
     *
     * @return boolean 
     */
    public function getReceiveComms()
    {
        return $this->receiveComms;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return UserPreferences
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $user->setPreferences($this);
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getHasNoDependencies()
    {
        return $this->hasNoDependencies;
    }

    /**
     * @param mixed $hasNoDependencies
     * @return UserPreferences
     */
    public function setHasNoDependencies($hasNoDependencies)
    {
        $this->hasNoDependencies = $hasNoDependencies;
        return $this;
    }
}
