<?php

namespace AppBundle\Entity;

/**
 * PostcodeRegion
 */
class PostcodeRegion
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $postcode;

    /**
     * @var string
     */
    private $region;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return PostcodeRegion
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return PostcodeRegion
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }
}
