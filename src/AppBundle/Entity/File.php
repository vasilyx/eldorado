<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 04.06.2018
 * Time: 0:09
 */

namespace AppBundle\Entity;


use AppBundle\Traits\StaticCreatable;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * PDF files (agreements)
 *
 * Class File
 * @package AppBundle\Entity
 */
class File
{
    use Timestampable;
    use StaticCreatable;

    const BORROWER_LOAN_AGREEMENT_TYPE = 10;
    const INVESTOR_LOAN_AGREEMENT_TYPE = 20;

    const ACCEPTANCE_BORROWER_AGREEMENT_TYPE = 101;
    const FINAL_BORROWER_AGREEMENT_TYPE = 102;
    const LENDER_AGREEMENT_TYPE = 103;

    const ContractBaseDir = '/contracts';
    const ContractsBorrowerDir = '/borrowers/';
    const ContractsInvestorDir = '/investors/';

    static public function getFileTypes()
    {
        return [
            self::BORROWER_LOAN_AGREEMENT_TYPE => 'Borrower Loan Agreement',
            self::INVESTOR_LOAN_AGREEMENT_TYPE => 'Investor Loan Agreement'
        ];
    }

    static public function getFileNamesByType()
    {
        return [
            self::BORROWER_LOAN_AGREEMENT_TYPE => 'Borrower_LoanAgreement',
            self::INVESTOR_LOAN_AGREEMENT_TYPE => 'Investor_LoanAgreement'
        ];
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var LoanApply
     */
    private $loanApply;

    /**
     * File type
     *
     * @var integer
     */
    private $type;

    /**
     * @var string
     */
    private $location;

    /**
     * @var boolean
     */
    private $saved;

    /**
     * @var InvestOffer
     */
    private $investOffer;

    /**
     * @var Agreement
     */
    private $agreement;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return InvestOffer
     */
    public function getInvestOffer()
    {
        return $this->investOffer;
    }

    /**
     * @param InvestOffer $investOffer
     * @return $this
     */
    public function setInvestOffer(InvestOffer $investOffer)
    {
        $this->investOffer = $investOffer;

        return $this;
    }

    /**
     * @return Agreement
     */
    public function getAgreement()
    {
        return $this->agreement;
    }

    /**
     * @param Agreement $agreement
     * @return $this
     */
    public function setAgreement(Agreement $agreement)
    {
        $this->agreement = $agreement;

        return $this;
    }


    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isSaved()
    {
        return $this->saved;
    }

    /**
     * @param boolean $saved
     * @return $this
     */
    public function setSaved(bool $saved)
    {
        $this->saved = $saved;

        return $this;
    }

    /**
     * @return LoanApply
     */
    public function getLoanApply()
    {
        return $this->loanApply;
    }

    /**
     * @param LoanApply $loanApply
     * @return $this
     */
    public function setLoanApply($loanApply)
    {
        $this->loanApply = $loanApply;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }



    /**
     * Get saved
     *
     * @return boolean
     */
    public function getSaved()
    {
        return $this->saved;
    }
}
