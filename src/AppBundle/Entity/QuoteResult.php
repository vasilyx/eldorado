<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QuoteResult
 */
class QuoteResult
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $quoteBonusAmt;

    /**
     * @var integer
     */
    private $platformFixedAmt;

    /**
     * @var integer
     */
    private $platformMgmtAmt;

    /**
     * @var float
     */
    private $bonusRate;

    /**
     * @var float
     */
    private $rate;

    /**
     * @var float
     */
    private $investorRate;

    /**
     * @var float
     */
    private $apr;

    /**
     * @var integer
     */
    private $monthly;

    /**
     * @var integer
     */
    private $totalRepaid;

    /**
     * @var integer
     */
    private $score1;

    /**
     * @var integer
     */
    private $score2;

    /**
     * @var integer
     */
    private $score3;

    /**
     * @var \AppBundle\Entity\QuoteApply
     */
    private $quoteApply;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quoteBonusAmt
     *
     * @param integer $quoteBonusAmt
     * @return QuoteResult
     */
    public function setQuoteBonusAmt($quoteBonusAmt)
    {
        $this->quoteBonusAmt = $quoteBonusAmt;

        return $this;
    }

    /**
     * Get quoteBonusAmt
     *
     * @return integer 
     */
    public function getQuoteBonusAmt()
    {
        return $this->quoteBonusAmt;
    }

    /**
     * Set platformFixedAmt
     *
     * @param integer $platformFixedAmt
     * @return QuoteResult
     */
    public function setPlatformFixedAmt($platformFixedAmt)
    {
        $this->platformFixedAmt = $platformFixedAmt;

        return $this;
    }

    /**
     * Get platformFixedAmt
     *
     * @return integer 
     */
    public function getPlatformFixedAmt()
    {
        return $this->platformFixedAmt;
    }

    /**
     * Set platformMgmtAmt
     *
     * @param integer $platformMgmtAmt
     * @return QuoteResult
     */
    public function setPlatformMgmtAmt($platformMgmtAmt)
    {
        $this->platformMgmtAmt = $platformMgmtAmt;

        return $this;
    }

    /**
     * Get platformMgmtAmt
     *
     * @return integer 
     */
    public function getPlatformMgmtAmt()
    {
        return $this->platformMgmtAmt;
    }

    /**
     * Set bonusRate
     *
     * @param float $bonusRate
     * @return QuoteResult
     */
    public function setBonusRate($bonusRate)
    {
        $this->bonusRate = $bonusRate;

        return $this;
    }

    /**
     * Get bonusRate
     *
     * @return float 
     */
    public function getBonusRate()
    {
        return $this->bonusRate;
    }

    /**
     * Set rate
     *
     * @param float $rate
     * @return QuoteResult
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return float 
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set investorRate
     *
     * @param float $investorRate
     * @return QuoteResult
     */
    public function setInvestorRate($investorRate)
    {
        $this->investorRate = $investorRate;

        return $this;
    }

    /**
     * Get investorRate
     *
     * @return float 
     */
    public function getInvestorRate()
    {
        return $this->investorRate;
    }

    /**
     * Set apr
     *
     * @param float $apr
     * @return QuoteResult
     */
    public function setApr($apr)
    {
        $this->apr = $apr;

        return $this;
    }

    /**
     * Get apr
     *
     * @return float 
     */
    public function getApr()
    {
        return $this->apr;
    }

    /**
     * Set monthly
     *
     * @param integer $monthly
     * @return QuoteResult
     */
    public function setMonthly($monthly)
    {
        $this->monthly = $monthly;

        return $this;
    }

    /**
     * Get monthly
     *
     * @return integer 
     */
    public function getMonthly()
    {
        return $this->monthly;
    }

    /**
     * Set totalRepaid
     *
     * @param integer $totalRepaid
     * @return QuoteResult
     */
    public function setTotalRepaid($totalRepaid)
    {
        $this->totalRepaid = $totalRepaid;

        return $this;
    }

    /**
     * Get totalRepaid
     *
     * @return integer 
     */
    public function getTotalRepaid()
    {
        return $this->totalRepaid;
    }

    /**
     * Set score1
     *
     * @param integer $score1
     * @return QuoteResult
     */
    public function setScore1($score1)
    {
        $this->score1 = $score1;

        return $this;
    }

    /**
     * Get score1
     *
     * @return integer 
     */
    public function getScore1()
    {
        return $this->score1;
    }

    /**
     * Set score2
     *
     * @param integer $score2
     * @return QuoteResult
     */
    public function setScore2($score2)
    {
        $this->score2 = $score2;

        return $this;
    }

    /**
     * Get score2
     *
     * @return integer 
     */
    public function getScore2()
    {
        return $this->score2;
    }

    /**
     * Set score3
     *
     * @param integer $score3
     * @return QuoteResult
     */
    public function setScore3($score3)
    {
        $this->score3 = $score3;

        return $this;
    }

    /**
     * Get score3
     *
     * @return integer 
     */
    public function getScore3()
    {
        return $this->score3;
    }

    /**
     * Set quoteApply
     *
     * @param \AppBundle\Entity\QuoteApply $quoteApply
     * @return QuoteResult
     */
    public function setQuoteApply(\AppBundle\Entity\QuoteApply $quoteApply = null)
    {
        $this->quoteApply = $quoteApply;

        return $this;
    }

    /**
     * Get quoteApply
     *
     * @return \AppBundle\Entity\QuoteApply 
     */
    public function getQuoteApply()
    {
        return $this->quoteApply;
    }
}
