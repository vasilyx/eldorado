<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserAsset
 */
class UserAsset
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $home;

    /**
     * @var integer
     */
    private $shares;

    /**
     * @var integer
     */
    private $cash;

    /**
     * @var integer
     */
    private $isa;

    /**
     * @var integer
     */
    private $sipp;

    /**
     * @var integer
     */
    private $investable;

    /**
     * @var integer
     */
    private $property;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set home
     *
     * @param integer $home
     * @return UserAsset
     */
    public function setHome($home)
    {
        $this->home = $home;

        return $this;
    }

    /**
     * Get home
     *
     * @return integer 
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * Set shares
     *
     * @param integer $shares
     * @return UserAsset
     */
    public function setShares($shares)
    {
        $this->shares = $shares;

        return $this;
    }

    /**
     * Get shares
     *
     * @return integer 
     */
    public function getShares()
    {
        return $this->shares;
    }

    /**
     * Set cash
     *
     * @param integer $cash
     * @return UserAsset
     */
    public function setCash($cash)
    {
        $this->cash = $cash;

        return $this;
    }

    /**
     * Get cash
     *
     * @return integer 
     */
    public function getCash()
    {
        return $this->cash;
    }

    /**
     * Set isa
     *
     * @param integer $isa
     * @return UserAsset
     */
    public function setIsa($isa)
    {
        $this->isa = $isa;

        return $this;
    }

    /**
     * Get isa
     *
     * @return integer 
     */
    public function getIsa()
    {
        return $this->isa;
    }

    /**
     * Set sipp
     *
     * @param integer $sipp
     * @return UserAsset
     */
    public function setSipp($sipp)
    {
        $this->sipp = $sipp;

        return $this;
    }

    /**
     * Get sipp
     *
     * @return integer 
     */
    public function getSipp()
    {
        return $this->sipp;
    }

    /**
     * Set investable
     *
     * @param integer $investable
     * @return UserAsset
     */
    public function setInvestable($investable)
    {
        $this->investable = $investable;

        return $this;
    }

    /**
     * Get investable
     *
     * @return integer 
     */
    public function getInvestable()
    {
        return $this->investable;
    }

    /**
     * Set property
     *
     * @param integer $property
     * @return UserAsset
     */
    public function setProperty($property)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return integer 
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return UserAsset
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
