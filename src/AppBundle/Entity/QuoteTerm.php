<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 8/22/16
 * Time: 3:03 PM
 */

namespace AppBundle\Entity;


use AppBundle\Interfaces\ArchivableInterface;
use AppBundle\Interfaces\SetArchiveFalseInterface;
use AppBundle\Traits\Archivable\Archivable;
use AppBundle\Traits\Archivable\SetArchiveFalseTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

class QuoteTerm implements ArchivableInterface, SetArchiveFalseInterface
{
    use Archivable;
    use Timestampable;
    use SetArchiveFalseTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $amount;

    /**
     * @var integer
     */
    private $term;

    /**
     * @var string
     */
    private $monthlyPayment;

    /**
     * @var string
     */
    private $totalPayment;

    /**
     * @var string
     */
    private $contractRate;

    /**
     * @var string
     */
    private $bonusRate;

    /**
     * @var string
     */
    private $effectiveRate;

    /**
     * @var string
     */
    private $brokerRate;

    /**
     * @var string
     */
    private $investorRate;

    /**
     * @var \AppBundle\Entity\QuoteApply
     */
    private $quoteApply;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return QuoteTerm
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set term
     *
     * @param integer $term
     *
     * @return QuoteTerm
     */
    public function setTerm($term)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return integer
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Set monthlyPayment
     *
     * @param string $monthlyPayment
     *
     * @return QuoteTerm
     */
    public function setMonthlyPayment($monthlyPayment)
    {
        $this->monthlyPayment = $monthlyPayment;

        return $this;
    }

    /**
     * Get monthlyPayment
     *
     * @return string
     */
    public function getMonthlyPayment()
    {
        return $this->monthlyPayment;
    }

    /**
     * Set totalPayment
     *
     * @param string $totalPayment
     *
     * @return QuoteTerm
     */
    public function setTotalPayment($totalPayment)
    {
        $this->totalPayment = $totalPayment;

        return $this;
    }

    /**
     * Get totalPayment
     *
     * @return string
     */
    public function getTotalPayment()
    {
        return $this->totalPayment;
    }

    /**
     * Set contractRate
     *
     * @param string $contractRate
     *
     * @return QuoteTerm
     */
    public function setContractRate($contractRate)
    {
        $this->contractRate = $contractRate;

        return $this;
    }

    /**
     * Get contractRate
     *
     * @return string
     */
    public function getContractRate()
    {
        return $this->contractRate;
    }

    /**
     * Set bonusRate
     *
     * @param string $bonusRate
     *
     * @return QuoteTerm
     */
    public function setBonusRate($bonusRate)
    {
        $this->bonusRate = $bonusRate;

        return $this;
    }

    /**
     * Get bonusRate
     *
     * @return string
     */
    public function getBonusRate()
    {
        return $this->bonusRate;
    }

    /**
     * Set effectiveRate
     *
     * @param string $effectiveRate
     *
     * @return QuoteTerm
     */
    public function setEffectiveRate($effectiveRate)
    {
        $this->effectiveRate = $effectiveRate;

        return $this;
    }

    /**
     * Get effectiveRate
     *
     * @return string
     */
    public function getEffectiveRate()
    {
        return $this->effectiveRate;
    }

    /**
     * Set brokerRate
     *
     * @param string $brokerRate
     *
     * @return QuoteTerm
     */
    public function setBrokerRate($brokerRate)
    {
        $this->brokerRate = $brokerRate;

        return $this;
    }

    /**
     * Get brokerRate
     *
     * @return string
     */
    public function getBrokerRate()
    {
        return $this->brokerRate;
    }

    /**
     * Set investorRate
     *
     * @param string $investorRate
     *
     * @return QuoteTerm
     */
    public function setInvestorRate($investorRate)
    {
        $this->investorRate = $investorRate;

        return $this;
    }

    /**
     * Get investorRate
     *
     * @return string
     */
    public function getInvestorRate()
    {
        return $this->investorRate;
    }

    /**
     * Set quoteApply
     *
     * @param \AppBundle\Entity\QuoteApply $quoteApply
     *
     * @return QuoteTerm
     */
    public function setQuoteApply(\AppBundle\Entity\QuoteApply $quoteApply = null)
    {
        $this->quoteApply = $quoteApply;

        return $this;
    }

    /**
     * Get quoteApply
     *
     * @return \AppBundle\Entity\QuoteApply
     */
    public function getQuoteApply()
    {
        return $this->quoteApply;
    }
}
