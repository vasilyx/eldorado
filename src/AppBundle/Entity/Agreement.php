<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Agreement
 */
class Agreement
{
    /**
     * @var integer
     */
    private $id;


    /**
     * @var \AppBundle\Entity\LoanApply
     */
    private $loanApply;


    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tags;

    /**
     * @var float
     */
    private $balance;

    /**
     * @var float
     */
    private $firstMonthlyPayment;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return float
     */
    public function getFirstMonthlyPayment()
    {
        return $this->firstMonthlyPayment;
    }

    /**
     * @param float $firstMonthlyPayment
     * @return $this
     */
    public function setFirstMonthlyPayment($firstMonthlyPayment)
    {
        $this->firstMonthlyPayment = $firstMonthlyPayment;

        return $this;
    }

    /**
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param float $balance
     * @return $this
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set loanApply
     *
     * @param \AppBundle\Entity\LoanApply $loanApply
     * @return Agreement
     */
    public function setLoanApply(\AppBundle\Entity\LoanApply $loanApply = null)
    {
        $this->loanApply = $loanApply;

        return $this;
    }

    /**
     * Get loanApply
     *
     * @return \AppBundle\Entity\LoanApply
     */
    public function getLoanApply()
    {
        return $this->loanApply;
    }

    /**
     * Add tag
     *
     * @param \AppBundle\Entity\Restriction $tag
     *
     * @return Agreement
     */
    public function addTag(\AppBundle\Entity\Restriction $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \AppBundle\Entity\Restriction $tag
     */
    public function removeTag(\AppBundle\Entity\Restriction $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }
    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var integer
     */
    private $amount;

    /**
     * @var integer
     */
    private $term;

     /**
     * @var integer
     */
    private $paymentDay;

    /**
     * @var string
     */
    private $monthlyRepayment;

    /**
     * @var string
     */
    private $bonusAmount;

    /**
     * @var string
     */
    private $agreementPlatformFixedAmt;


    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return Agreement
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Agreement
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set term
     *
     * @param integer $term
     *
     * @return Agreement
     */
    public function setTerm($term)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return integer
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Set paymentDay
     *
     * @param integer $paymentDay
     *
     * @return Agreement
     */
    public function setPaymentDay($paymentDay)
    {
        $this->paymentDay = $paymentDay;

        return $this;
    }

    /**
     * Get paymentDay
     *
     * @return integer
     */
    public function getPaymentDay()
    {
        return $this->paymentDay;
    }

    /**
     * Set monthlyRepayment
     *
     * @param string $monthlyRepayment
     *
     * @return Agreement
     */
    public function setMonthlyRepayment($monthlyRepayment)
    {
        $this->monthlyRepayment = $monthlyRepayment;

        return $this;
    }

    /**
     * Get monthlyRepayment
     *
     * @return string
     */
    public function getMonthlyRepayment()
    {
        return $this->monthlyRepayment;
    }

    /**
     * Set bonusAmount
     *
     * @param string $bonusAmount
     *
     * @return Agreement
     */
    public function setBonusAmount($bonusAmount)
    {
        $this->bonusAmount = $bonusAmount;

        return $this;
    }

    /**
     * Get bonusAmount
     *
     * @return string
     */
    public function getBonusAmount()
    {
        return $this->bonusAmount;
    }

    /**
     * Set agreementPlatformFixedAmt
     *
     * @param string $agreementPlatformFixedAmt
     *
     * @return Agreement
     */
    public function setAgreementPlatformFixedAmt($agreementPlatformFixedAmt)
    {
        $this->agreementPlatformFixedAmt = $agreementPlatformFixedAmt;

        return $this;
    }

    /**
     * Get agreementPlatformFixedAmt
     *
     * @return string
     */
    public function getAgreementPlatformFixedAmt()
    {
        return $this->agreementPlatformFixedAmt;
    }
    /**
     * @var string
     */
    private $bonusRate;


    /**
     * Set bonusRate
     *
     * @param string $bonusRate
     *
     * @return Agreement
     */
    public function setBonusRate($bonusRate)
    {
        $this->bonusRate = $bonusRate;

        return $this;
    }

    /**
     * Get bonusRate
     *
     * @return string
     */
    public function getBonusRate()
    {
        return $this->bonusRate;
    }
    /**
     * @var integer
     */
    private $apr;

    /**
     * Set apr
     *
     * @param integer $apr
     *
     * @return Agreement
     */
    public function setApr($apr)
    {
        $this->apr = $apr;

        return $this;
    }

    /**
     * Get apr
     *
     * @return integer
     */
    public function getApr()
    {
        return $this->apr;
    }

    /**
     * @var string
     */
    private $comment;


    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Agreement
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
    /**
     * @var \DateTime
     */
    private $acceptanceDate;

    /**
     * @var \DateTime
     */
    private $fullyMatchedDate;


    /**
     * Set acceptanceDate
     *
     * @param \DateTime $acceptanceDate
     *
     * @return Agreement
     */
    public function setAcceptanceDate($acceptanceDate)
    {
        $this->acceptanceDate = $acceptanceDate;

        return $this;
    }

    /**
     * Get acceptanceDate
     *
     * @return \DateTime
     */
    public function getAcceptanceDate()
    {
        return $this->acceptanceDate;
    }

    /**
     * Set fullyMatchedDate
     *
     * @param \DateTime $fullyMatchedDate
     *
     * @return Agreement
     */
    public function setFullyMatchedDate($fullyMatchedDate)
    {
        $this->fullyMatchedDate = $fullyMatchedDate;

        return $this;
    }

    /**
     * Get fullyMatchedDate
     *
     * @return \DateTime
     */
    public function getFullyMatchedDate()
    {
        return $this->fullyMatchedDate;
    }
    /**
     * @var \DateTime
     */
    private $signingDate;

    /**
     * @var boolean
     */
    private $isDeclined = '0';


    /**
     * Set signingDate
     *
     * @param \DateTime $signingDate
     *
     * @return Agreement
     */
    public function setSigningDate($signingDate)
    {
        $this->signingDate = $signingDate;

        return $this;
    }

    /**
     * Get signingDate
     *
     * @return \DateTime
     */
    public function getSigningDate()
    {
        return $this->signingDate;
    }

    /**
     * Set isDeclined
     *
     * @param boolean $isDeclined
     *
     * @return Agreement
     */
    public function setIsDeclined($isDeclined)
    {
        $this->isDeclined = $isDeclined;

        return $this;
    }

    /**
     * Get isDeclined
     *
     * @return boolean
     */
    public function getIsDeclined()
    {
        return $this->isDeclined;
    }
}
