<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AgreementPerformance
 */
class AgreementPerformance
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $performance;

    /**
     * @var float
     */
    private $creditRiskIndicator;

    /**
     * @var float
     */
    private $paymentRiskIndicator;

    /**
     * @var \AppBundle\Entity\Agreement
     */
    private $agreement;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set performance
     *
     * @param string $performance
     * @return AgreementPerformance
     */
    public function setPerformance($performance)
    {
        $this->performance = $performance;

        return $this;
    }

    /**
     * Get performance
     *
     * @return string 
     */
    public function getPerformance()
    {
        return $this->performance;
    }

    /**
     * Set creditRiskIndicator
     *
     * @param float $creditRiskIndicator
     * @return AgreementPerformance
     */
    public function setCreditRiskIndicator($creditRiskIndicator)
    {
        $this->creditRiskIndicator = $creditRiskIndicator;

        return $this;
    }

    /**
     * Get creditRiskIndicator
     *
     * @return float 
     */
    public function getCreditRiskIndicator()
    {
        return $this->creditRiskIndicator;
    }

    /**
     * Set paymentRiskIndicator
     *
     * @param float $paymentRiskIndicator
     * @return AgreementPerformance
     */
    public function setPaymentRiskIndicator($paymentRiskIndicator)
    {
        $this->paymentRiskIndicator = $paymentRiskIndicator;

        return $this;
    }

    /**
     * Get paymentRiskIndicator
     *
     * @return float 
     */
    public function getPaymentRiskIndicator()
    {
        return $this->paymentRiskIndicator;
    }

    /**
     * Set agreement
     *
     * @param \AppBundle\Entity\Agreement $agreement
     * @return AgreementPerformance
     */
    public function setAgreement(\AppBundle\Entity\Agreement $agreement = null)
    {
        $this->agreement = $agreement;

        return $this;
    }

    /**
     * Get agreement
     *
     * @return \AppBundle\Entity\Agreement 
     */
    public function getAgreement()
    {
        return $this->agreement;
    }
}
