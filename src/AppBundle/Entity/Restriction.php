<?php
/**
 * User: Vladimir
 * Date: 2/1/17
 * Time: 1:55 PM
 */

namespace AppBundle\Entity;


use AppBundle\Interfaces\ArchivableInterface;
use AppBundle\Interfaces\SetArchiveFalseInterface;
use AppBundle\Traits\Archivable\Archivable;
use AppBundle\Traits\Archivable\SetArchiveFalseTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

class Restriction implements ArchivableInterface, SetArchiveFalseInterface
{
    use Timestampable;
    use Archivable;
    use SetArchiveFalseTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \AppBundle\Entity\RestrictionGroup
     */
    private $restrictionGroup;

    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Restriction
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Restriction
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set restrictionGroup
     *
     * @param \AppBundle\Entity\RestrictionGroup $restrictionGroup
     *
     * @return Restriction
     */
    public function setRestrictionGroup(\AppBundle\Entity\RestrictionGroup $restrictionGroup)
    {
        $this->restrictionGroup = $restrictionGroup;

        return $this;
    }

    /**
     * Get restrictionGroup
     *
     * @return \AppBundle\Entity\RestrictionGroup
     */
    public function getRestrictionGroup()
    {
        return $this->restrictionGroup;
    }
}
