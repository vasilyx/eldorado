<?php

namespace AppBundle\Entity;

use AppBundle\Interfaces\ArchivableInterface;
use AppBundle\Interfaces\SetArchiveFalseInterface;
use AppBundle\Traits\Archivable\Archivable;
use AppBundle\Traits\Archivable\SetArchiveFalseTrait;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * UserIncome
 */
class UserIncome implements ArchivableInterface, SetArchiveFalseInterface
{
    use Timestampable;
    use Archivable;
    use SetArchiveFalseTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $mainIncomeGrossAnn;

    /**
     * @var integer
     */
    private $mainIncomeNetAnn;

    /**
     * @var integer
     */
    private $otherIncomeGrossAnn;

    /**
     * @var integer
     */
    private $otherIncomeNetAnn;

    /**
     * @var string
     */
    private $otherIncomeSource;

    /**
     * @var integer
     */
    private $spouseIncomeGrossAnn;

    /**
     * @var integer
     */
    private $spouseIncomeNetAnn;

    /**
     * @var integer
     */
    private $totalIncomeGrossAnn;

    /**
     * @var string
     */
    private $mainIncomeSource;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var integer
     */
    private $totalNetIncomeMonthly;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mainIncomeGrossAnn
     *
     * @param integer $mainIncomeGrossAnn
     * @return UserIncome
     */
    public function setMainIncomeGrossAnn($mainIncomeGrossAnn)
    {
        $this->mainIncomeGrossAnn = $mainIncomeGrossAnn;

        return $this;
    }

    /**
     * Get mainIncomeGrossAnn
     *
     * @return integer
     */
    public function getMainIncomeGrossAnn()
    {
        return $this->mainIncomeGrossAnn;
    }

    /**
     * Set mainIncomeNetAnn
     *
     * @param integer $mainIncomeNetAnn
     * @return UserIncome
     */
    public function setMainIncomeNetAnn($mainIncomeNetAnn)
    {
        $this->mainIncomeNetAnn = $mainIncomeNetAnn;

        return $this;
    }

    /**
     * Get mainIncomeNetAnn
     *
     * @return integer
     */
    public function getMainIncomeNetAnn()
    {
        return $this->mainIncomeNetAnn;
    }

    /**
     * Set otherIncomeGrossAnn
     *
     * @param integer $otherIncomeGrossAnn
     * @return UserIncome
     */
    public function setOtherIncomeGrossAnn($otherIncomeGrossAnn)
    {
        $this->otherIncomeGrossAnn = $otherIncomeGrossAnn;

        return $this;
    }

    /**
     * Get otherIncomeGrossAnn
     *
     * @return integer
     */
    public function getOtherIncomeGrossAnn()
    {
        return $this->otherIncomeGrossAnn;
    }

    /**
     * Set otherIncomeNetAnn
     *
     * @param integer $otherIncomeNetAnn
     * @return UserIncome
     */
    public function setOtherIncomeNetAnn($otherIncomeNetAnn)
    {
        $this->otherIncomeNetAnn = $otherIncomeNetAnn;

        return $this;
    }

    /**
     * Get otherIncomeNetAnn
     *
     * @return integer
     */
    public function getOtherIncomeNetAnn()
    {
        return $this->otherIncomeNetAnn;
    }

    /**
     * Set otherIncomeSource
     *
     * @param string $otherIncomeSource
     * @return UserIncome
     */
    public function setOtherIncomeSource($otherIncomeSource)
    {
        $this->otherIncomeSource = $otherIncomeSource;

        return $this;
    }

    /**
     * Get otherIncomeSource
     *
     * @return string
     */
    public function getOtherIncomeSource()
    {
        return $this->otherIncomeSource;
    }

    /**
     * Set spouseIncomeGrossAnn
     *
     * @param integer $spouseIncomeGrossAnn
     * @return UserIncome
     */
    public function setSpouseIncomeGrossAnn($spouseIncomeGrossAnn)
    {
        $this->spouseIncomeGrossAnn = $spouseIncomeGrossAnn;

        return $this;
    }

    /**
     * Get spouseIncomeGrossAnn
     *
     * @return integer
     */
    public function getSpouseIncomeGrossAnn()
    {
        return $this->spouseIncomeGrossAnn;
    }

    /**
     * Set spouseIncomeNetAnn
     *
     * @param integer $spouseIncomeNetAnn
     * @return UserIncome
     */
    public function setSpouseIncomeNetAnn($spouseIncomeNetAnn)
    {
        $this->spouseIncomeNetAnn = $spouseIncomeNetAnn;

        return $this;
    }

    /**
     * Get spouseIncomeNetAnn
     *
     * @return integer
     */
    public function getSpouseIncomeNetAnn()
    {
        return $this->spouseIncomeNetAnn;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return UserIncome
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set totalIncomeGrossAnn
     *
     * @param integer $totalIncomeGrossAnn
     * @return UserIncome
     */
    public function setTotalIncomeGrossAnn($totalIncomeGrossAnn)
    {
        $this->totalIncomeGrossAnn = $totalIncomeGrossAnn;

        return $this;
    }

    /**
     * Get totalIncomeGrossAnn
     *
     * @return integer
     */
    public function getTotalIncomeGrossAnn()
    {
        return $this->totalIncomeGrossAnn;
    }

    /**
     * Set mainIncomeSource
     *
     * @param string $mainIncomeSource
     * @return UserIncome
     */
    public function setMainIncomeSource($mainIncomeSource)
    {
        $this->mainIncomeSource = $mainIncomeSource;

        return $this;
    }

    /**
     * Get mainIncomeSource
     *
     * @return string
     */
    public function getMainIncomeSource()
    {
        return $this->mainIncomeSource;
    }

    /**
     * @return int
     */
    public function getTotalNetIncomeMonthly()
    {
        return $this->totalNetIncomeMonthly;
    }

    /**
     * @param int $totalNetIncomeMonthly
     * @return UserIncome
     */
    public function setTotalNetIncomeMonthly($totalNetIncomeMonthly)
    {
        $this->totalNetIncomeMonthly = $totalNetIncomeMonthly;
        return $this;
    }
}
