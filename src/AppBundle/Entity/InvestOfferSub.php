<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvestOfferSub
 */
class InvestOfferSub
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var \AppBundle\Entity\InvestOffer
     */
    private $investOffer;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return InvestOfferSub
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return InvestOfferSub
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set investOffer
     *
     * @param \AppBundle\Entity\InvestOffer $investOffer
     * @return InvestOfferSub
     */
    public function setInvestOffer(\AppBundle\Entity\InvestOffer $investOffer = null)
    {
        $this->investOffer = $investOffer;

        return $this;
    }

    /**
     * Get investOffer
     *
     * @return \AppBundle\Entity\InvestOffer 
     */
    public function getInvestOffer()
    {
        return $this->investOffer;
    }
}
