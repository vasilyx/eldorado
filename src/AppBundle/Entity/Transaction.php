<?php

namespace AppBundle\Entity;

use AppBundle\Traits\StaticCreatable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * Transaction
 */
class Transaction
{
    /**
     * Contains createdAt property, analog of DateTimeStamp - Date that this transaction will be processed for.  Default is system DateTimeStamp
     */
    use Timestampable;
    use StaticCreatable;

    const TRANSACTION_CCY_GBP = 'GBP';

    const TRANSACTION_OWNER_INVESTOR = 100;
    const TRANSACTION_OWNER_BORROWER = 200;
    const TRANSACTION_OWNER_WORLDPAY = 300;
    const TRANSACTION_OWNER_TRUST_BANK_ACCOUNT_LENDERS = 400;
    const TRANSACTION_OWNER_TRUST_BANK_ACCOUNT_BORROWERS = 500;
    const TRANSACTION_OWNER_TRUST_BANK_ACCOUNT_FIRST_LOSS = 600;
    const TRANSACTION_OWNER_TRUST_BANK_ACCOUNT_BONUS = 700;
    const TRANSACTION_OWNER_FASTER_PAYMENT = 800;
    const TRANSACTION_OWNER_ISA_TRANSFER = 900;
    const TRANSACTION_OWNER_SIPP_TRANSFER = 1000;
    const TRANSACTION_OWNER_BROKER = 1100;
    const TRANSACTION_OWNER_SELLER = 1200;
    const TRANSACTION_OWNER_BUYER = 1300;

    const TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_INVESTOR = 100;
    const TRANSACTION_GLACCOUNT_CLIENT_ACCOUNT_BORROWER = 200;
    const TRANSACTION_GLACCOUNT_BORROWER_ACCOUNT = 300;
    const TRANSACTION_GLACCOUNT_3DPARTY_PAYMENTS = 400;
    const TRANSACTION_GLACCOUNT_LOAN_ACCOUNT_INVESTOR = 500;
    const TRANSACTION_GLACCOUNT_LOAN_ACCOUNT_BORROWER = 600;
    const TRANSACTION_GLACCOUNT_BANK_ACCOUNT_BONUS = 700;
    const TRANSACTION_GLACCOUNT_BONUS_ACCOUNT_BORROWER = 800;
    const TRANSACTION_GLACCOUNT_BANK_ACCOUNT_FIRST_LOSS_FUND = 900;
    const TRANSACTION_GLACCOUNT_BANK_ACCOUNT_BORROWER = 1000;
    const TRANSACTION_GLACCOUNT_BANK_ACCOUNT_INVESTOR = 11000;
    const TRANSACTION_GLACCOUNT_BANK_ACCOUNT_BROKER = 1200;
    const TRANSACTION_GLACCOUNT_INSTALMENT_ACCOUNT_BORROWER = 1300;
    const TRANSACTION_GLACCOUNT_INSTALMENT_ACCOUNT_INVESTOR = 1400;
    const TRANSACTION_GLACCOUNT_INTEREST_EARNED_INVESTOR = 1500;
    const TRANSACTION_GLACCOUNT_INTEREST_EXPENSE_BORROWER = 1600;
    const TRANSACTION_GLACCOUNT_BROKER_FEE = 1700;
    const TRANSACTION_GLACCOUNT_BROKER_SUSPENSE_ACCOUNT = 1800;


    const TRANSACTION_ACCOUNT_CASH_PENDING = 100;
    const TRANSACTION_ACCOUNT_CASH_AVAILABLE = 200;
    const TRANSACTION_ACCOUNT_CASH_ALLOCATED = 250;
    const TRANSACTION_ACCOUNT_CASH_ON_OFFER = 300;
    const TRANSACTION_ACCOUNT_PENDING_PRINCIPLE = 400;
    const TRANSACTION_ACCOUNT_PENDING_INTEREST = 500;
    const TRANSACTION_ACCOUNT_PENDING_BONUS = 600;
    const TRANSACTION_ACCOUNT_PENDING_BROKER_FEE = 700;
    const TRANSACTION_ACCOUNT_PRINCIPLE = 800;
    const TRANSACTION_ACCOUNT_INTEREST_RECEIVABLE = 900;
    const TRANSACTION_ACCOUNT_BONUS = 1000;
    const TRANSACTION_ACCOUNT_BROKER_FEE = 1100;
    const TRANSACTION_ACCOUNT_BORROWER_ADVANCE = 1200;
    const TRANSACTION_ACCOUNT_WITHDRAWAL_PENDING = 1300;
    const TRANSACTION_ACCOUNT_OFFER_ALLOCATED = 1400;

    const TRANSACTION_SUB_ACCOUNT_CASH_PENDING = 100;
    const TRANSACTION_SUB_ACCOUNT_CASH_WITHDRAW = 200;
    const TRANSACTION_SUB_ACCOUNT_CASH_AVAILABLE = 300;
    const TRANSACTION_SUB_ACCOUNT_CASH_OFFER = 400;
    const TRANSACTION_SUB_ACCOUNT_CASH_ALLOC = 500;
    const TRANSACTION_SUB_ACCOUNT_CASH_REINVEST = 600;
    const TRANSACTION_SUB_ACCOUNT_PRINCIPLE = 700;
    const TRANSACTION_SUB_ACCOUNT_INTEREST = 800;
    const TRANSACTION_SUB_ACCOUNT_SETTLING = 900;
    const TRANSACTION_SUB_ACCOUNT_FASTER_PAYMENT = 1000;
    const TRANSACTION_SUB_ACCOUNT_WORLDPAY = 1100;
    const TRANSACTION_SUB_ACCOUNT_IFISA_IN = 1200;
    const TRANSACTION_SUB_ACCOUNT_IFISA_OUT = 1300;
    const TRANSACTION_SUB_ACCOUNT_SIPP_IN = 1400;
    const TRANSACTION_SUB_ACCOUNT_SIPP_OUT = 1500;
    const TRANSACTION_SUB_ACCOUNT_CHARGE = 1600;
    const TRANSACTION_SUB_ACCOUNT_PENDING = 1700;
    const TRANSACTION_SUB_ACCOUNT_EARNED = 1800;
    const TRANSACTION_SUB_ACCOUNT_FORFEIT = 1900;
    const TRANSACTION_SUB_ACCOUNT_PAID = 2000;
    const TRANSACTION_SUB_ACCOUNT_RECOVER = 2100;
    const TRANSACTION_SUB_ACCOUNT_REPAID = 2200;
    const TRANSACTION_SUB_ACCOUNT_INSTALLMENT = 2300;


    public static function getTransactionOwners()
    {
        return [
            self::TRANSACTION_OWNER_INVESTOR => [
                'name' => 'Investor',
                'description' => 'The investor(s) involved in the event that generates this transaction'
            ],
            self::TRANSACTION_OWNER_BORROWER => [
                'name' => 'Borrower',
                'description' => 'The borrower involved in the event that generates this transaction'
            ],
            self::TRANSACTION_OWNER_WORLDPAY => [
                'name' => 'Worldpay',
                'description' => 'Payments Gateway that will deliver the funds'
            ]
        ];
    }

    public static function getAvailableTransactionOwnersCodes()
    {
        return array_keys(self::getTransactionOwners());
    }

    public static function gitTransactionAccounts()
    {
        return [
            self::TRANSACTION_ACCOUNT_CASH_PENDING      => 'Cash pending',
            self::TRANSACTION_ACCOUNT_CASH_AVAILABLE    => 'Cash available',
            self::TRANSACTION_ACCOUNT_CASH_ON_OFFER     => 'Cash on offer'
        ];
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var User $user
     */
    protected $user;

    /**
     * Transaction ID. Mandatory. Length = 36.
     *
     * @var string
     */
    protected $transactionId;

    /**
     * Unique Event ID. Mandatory.
     *
     * @var Event
     */
    protected $event;

    /**
     * Transaction Currency. Mandatory. Length = 3
     *
     * @var string
     */
    protected $transactionCcy;

    /**
     * Transaction Amount. Optional. Length = 20:12,8 (in decimal type terms: precision = 20, scale = 8).
     * If transaction is pending amount is negative.
     *
     * @var string
     */
    protected $transactionAmount;

    /**
     * Transaction Owner is party impacted by the transaction. Optional.
     *
     * @var string
     */
    protected $transactionOwner;

    /**
     * Transaction account showing what type of value this transaction relates to. Optional
     *
     * @var string
     */
    protected $transactionAccount;

    /**
     *  Parent Transaction ID to link all associated transactions for the single event. Optional
     *
     * @var Transaction
     */
    protected $parentTransaction;

    /**
     * Active Status for Transaction. Mandatory.
     *
     * @var boolean
     */
    protected $activeStatus;

    /**
     * Date that this transaction will be processed for - if not provided then default to system DateTimeStamp. Mandatory.
     *
     * @var \DateTime
     */
    protected $reportDateTime;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param string $transactionId
     * @return Transaction
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
        return $this;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param Event $event
     * @return Transaction
     */
    public function setEvent($event)
    {
        $this->transactionId = $this->generateTransactionId($event->getType());
        $this->event = $event;
        return $this;
    }

    /**
     * @return string
     */
    public function getTransactionCcy()
    {
        return $this->transactionCcy;
    }

    /**
     * @param string $transactionCcy
     * @return Transaction
     */
    public function setTransactionCcy($transactionCcy)
    {
        $this->transactionCcy = $transactionCcy;
        return $this;
    }

    /**
     * @return string
     */
    public function getTransactionAmount()
    {
        return $this->transactionAmount;
    }

    /**
     * @param string $transactionAmount
     * @return Transaction
     */
    public function setTransactionAmount($transactionAmount)
    {
        $this->transactionAmount = $transactionAmount;
        return $this;
    }

    /**
     * @return string
     */
    public function getTransactionOwner()
    {
        return $this->transactionOwner;
    }

    /**
     * @param string $transactionOwner
     * @return Transaction
     */
    public function setTransactionOwner($transactionOwner)
    {
        $this->transactionOwner = $transactionOwner;
        return $this;
    }

    /**
     * @return string
     */
    public function getTransactionAccount()
    {
        return $this->transactionAccount;
    }

    /**
     * @param string $transactionAccount
     * @return Transaction
     */
    public function setTransactionAccount($transactionAccount)
    {
        $this->transactionAccount = $transactionAccount;
        return $this;
    }

    /**
     * @return Transaction
     */
    public function getParentTransaction()
    {
        return $this->parentTransaction;
    }

    /**
     * @param Transaction $parentTransaction
     * @return Transaction
     */
    public function setParentTransaction($parentTransaction)
    {
        $this->parentTransaction = $parentTransaction;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isActiveStatus()
    {
        return $this->activeStatus;
    }

    /**
     * @param boolean $activeStatus
     * @return Transaction
     */
    public function setActiveStatus($activeStatus)
    {
        $this->activeStatus = $activeStatus;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getReportDate()
    {
        return $this->reportDateTime;
    }

    /**
     * @param \DateTime $reportDateTime
     * @return Transaction
     */
    public function setReportDate($reportDateTime)
    {
        $this->reportDateTime = $reportDateTime;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Transaction constructor.
     */
    public function __construct()
    {
        $this->setActiveStatus(true);
        $this->setTransactionCcy(Transaction::TRANSACTION_CCY_GBP);
        $this->setReportDate(new \DateTime());

    }

    public function generateTransactionId($eventType)
    {
        return $eventType . md5(rand(1, 100) . time());
    }

    /**
     * Get activeStatus
     *
     * @return boolean
     */
    public function getActiveStatus()
    {
        return $this->activeStatus;
    }

    /**
     * Set reportDateTime
     *
     * @param \DateTime $reportDateTime
     *
     * @return Transaction
     */
    public function setReportDateTime($reportDateTime)
    {
        $this->reportDateTime = $reportDateTime;

        return $this;
    }

    /**
     * Get reportDateTime
     *
     * @return \DateTime
     */
    public function getReportDateTime()
    {
        return $this->reportDateTime;
    }
    /**
     * @var integer
     */
    private $transactionSubAccount;

    /**
     * @var integer
     */
    private $glAccount;


    /**
     * Set transactionSubAccount
     *
     * @param integer $transactionSubAccount
     *
     * @return Transaction
     */
    public function setTransactionSubAccount($transactionSubAccount)
    {
        $this->transactionSubAccount = $transactionSubAccount;

        return $this;
    }

    /**
     * Get transactionSubAccount
     *
     * @return integer
     */
    public function getTransactionSubAccount()
    {
        return $this->transactionSubAccount;
    }

    /**
     * Set glAccount
     *
     * @param integer $glAccount
     *
     * @return Transaction
     */
    public function setGlAccount($glAccount)
    {
        $this->glAccount = $glAccount;

        return $this;
    }

    /**
     * Get glAccount
     *
     * @return integer
     */
    public function getGlAccount()
    {
        return $this->glAccount;
    }
    /**
     * @var integer
     */
    private $investmentType;

    /**
     * @var integer
     */
    private $offerId;


    /**
     * Set investmentType
     *
     * @param integer $investmentType
     *
     * @return Transaction
     */
    public function setInvestmentType($investmentType)
    {
        $this->investmentType = $investmentType;

        return $this;
    }

    /**
     * Get investmentType
     *
     * @return integer
     */
    public function getInvestmentType()
    {
        return $this->investmentType;
    }

    /**
     * Set offerId
     *
     * @param integer $offerId
     *
     * @return Transaction
     */
    public function setOfferId($offerId)
    {
        $this->offerId = $offerId;

        return $this;
    }

    /**
     * Get offerId
     *
     * @return integer
     */
    public function getOfferId()
    {
        return $this->offerId;
    }
    /**
     * @var integer
     */
    private $agreementId;


    /**
     * Set agreementId
     *
     * @param integer $agreementId
     *
     * @return Transaction
     */
    public function setAgreementId($agreementId)
    {
        $this->agreementId = $agreementId;

        return $this;
    }

    /**
     * Get agreementId
     *
     * @return integer
     */
    public function getAgreementId()
    {
        return $this->agreementId;
    }
}
