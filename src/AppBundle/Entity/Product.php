<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 */
class Product
{
    const TYPE_BORROWER = 0;
    const TYPE_INVESTOR = 1;

    /*
     * Borrower products
     */

    /**
     * Borrower Consollify (deClutter) Premium for consumers with excellent credit management history, who earn a good income and paying for credit very affordable.They typically already have investment plans in place
     */
    const BORCONS1 = 10;

    /**
     * Borrower Consollify (deClutter) Near Prime for consumers with good to average credit management history. They earn an above-average income can ordinarily afford the credit, however have missed a handful of payments over the last 3-5 years
     */
    const BORCONS2 = 20;

    /**
     * Borrower Consollify (deClutter) Social for consumers with poor but improving credit management history. These are on lower than average incomes, work to keep up their payments but will be impacted by any significant life event
     */
    const BORCONS3 = 30;

    /**
     * Small and medium-sized businesses that have been trading in excess of five years, are profitable, with positive cash flows. They are looking to borrow to fund expansion activities in production, product innovation or customer market development or penetration
     */
    const BORBUSN1 = 40;

    /**
     * Small and medium-sized businesses that have been trading in excess of two years, are profitable, with positive cash flows. They are looking to borrow to fund growth through improving capacity, generating new markets with a proven product.
     */
    const BORBUSN2 = 50;

    /**
     * Small and medium-sized businesses including charities and social projects that have been trading in excess of two years, are profitable, with good prospects to repay the capital. They are looking to borrow to fund growth through improving capacity,generating new markets with a proven product or launch a new product or social program
     */
    const BORBUSN3 = 60;

    /**
     * 	Small and medium-sized organization including charities and small corporates that want to further develop property they already own or acquire and develop new properties.Typically Loan to Value (LTV) will be below 50%
     */
    const BORPROP1 = 70;

    /**
     * 	Small and medium-sized organization including charities and small corporates that want to further develop property they already own or acquire and develop new properties.Typically Loan to Value (LTV) will be below 65%
     */
    const BORPROP2 = 80;

    /**
     * 	Small and medium-sized organization predominantly charities and small corporates that want to further develop property they already own or acquire and develop new properties. Typically Loan to Value (LTV) will be 65% or higher
     */
    const BORPROP3 = 90;

    /*
     * Investor products
     */

    /**
     * Invest by lending direct to Consollify products with an option to set preferences and restrictions on location and whether it is Premium, Standard or Social
     */
    const INVCONS = 100;

    /**
     * Invest by lending direct to Biz Loan and Prop Loan products choosing from a list of profiled Premium or Standard projects
     */
    const INVBUSN = 200;

    /**
     * Invest by lending direct to Biz Loan and Prop Loan products choosing from a list of profiled Social projects. Clients will also be able to choose Consollify Social
     */
    const INVSOCL = 300;


    /**
     * Returns the array with codes as keys and text names as values
     *
     * @return array
     */
    public static function getProductNamesMap()
    {
        return
        [
            self::BORCONS1      => 'Consollify Premium',
            self::BORCONS2      => 'Consollify Standard',
            self::BORCONS3      => 'Consollify Social',
            self::BORBUSN1      => 'Biz Loan Premier',
            self::BORBUSN2      => 'Biz Loan Standard',
            self::BORBUSN3      => 'Biz Loan Social',
            self::BORPROP1      => 'Prop Loan Premier',
            self::BORPROP2      => 'Prop Loan Standard',
            self::BORPROP3      => 'Prop Loan Social',
            self::INVCONS       => 'Consollify Investments',
            self::INVBUSN       => 'Biz Investments',
            self::INVSOCL       => 'Social Investments'
        ];
    }

    /**
     * Returns the array with codes as keys and short names as values
     *
     * @return array
     */
    public static function getProductShortNamesMap()
    {
        return
            [
                self::BORCONS1      => 'BORCONS1',
                self::BORCONS2      => 'BORCONS2',
                self::BORCONS3      => 'BORCONS3',
                self::BORBUSN1      => 'BORBUSN1',
                self::BORBUSN2      => 'BORBUSN2',
                self::BORBUSN3      => 'BORBUSN3',
                self::BORPROP1      => 'BORPROP1',
                self::BORPROP2      => 'BORPROP2',
                self::BORPROP3      => 'BORPROP3',
                self::INVCONS       => 'INVCONS',
                self::INVBUSN       => 'INVBUSN',
                self::INVSOCL       => 'INVSOCL'
            ];
    }

    public static function getProductTypesMap()
    {
        return
            [
                self::BORCONS1      => self::TYPE_BORROWER,
                self::BORCONS2      => self::TYPE_BORROWER,
                self::BORCONS3      => self::TYPE_BORROWER,
                self::BORBUSN1      => self::TYPE_BORROWER,
                self::BORBUSN2      => self::TYPE_BORROWER,
                self::BORBUSN3      => self::TYPE_BORROWER,
                self::BORPROP1      => self::TYPE_BORROWER,
                self::BORPROP2      => self::TYPE_BORROWER,
                self::BORPROP3      => self::TYPE_BORROWER,
                self::INVCONS       => self::TYPE_INVESTOR,
                self::INVBUSN       => self::TYPE_INVESTOR,
                self::INVSOCL       => self::TYPE_INVESTOR
            ];
    }

    public static function getProductDescriptionsMap()
    {
        return [
            self::BORCONS1 => 'Borrower Consollify (deClutter) Premium for consumers with excellent credit management history, who earn a good income and paying for credit very affordable. They typically already have investment plans in place.',
            self::BORCONS2 => 'Borrower Consollify (deClutter) Near Prime for consumers with good to average credit management history. They earn an above-average income can ordinarily afford the credit, however have missed a handful of payments over the last 3-5 years.',
            self::BORCONS3 => 'Borrower Consollify (deClutter) Social for consumers with poor but improving credit management history. These are on lower than average incomes, work to keep up their payments but will be impacted by any significant life event.',
            self::BORBUSN1 => 'Small and midium-sized businesses that have been trading in excess of five years, are profitable, with positive cashflows.   They are looking to borrow to fund expansion activities in production, product innovation or customer market development or penetration.',
            self::BORBUSN2 => 'Small and midium-sized businesses that have been trading in excess of two years, are profitable, with positive cashflows.   They are looking to borrow to fund growth through improving capacity, generating new markets with a proven product.',
            self::BORBUSN3 => 'Small and midium-sized businesses including charities and social projects that have been trading in excess of two years, are profitable, with good prospects to repay the capital. They are looking to borrow to fund growth through improving capacity, generating new markets with a proven product or launch a new product or social program.',
            self::BORPROP1 => 'Small and midium-sized organization including charities and small corporates that want to further develop property they already own or acquire and develop new properties. Typically Loan to Value (LTV) will be below 50%.',
            self::BORPROP2 => 'Small and midium-sized organization including charities and small corporates that want to further develop property they already own or acquire and develop new properties. Typically Loan to Value (LTV) will be below 65%.',
            self::BORPROP3 => 'Small and midium-sized organization predominantly charities and small corporates that want to further develop property they already own or acquire and develop new properties. Typically Loan to Value (LTV) will be 65% or higher.',
            self::INVCONS  => 'Invest by lending direct to Consollify products with an option to set preferences and restrictions on location and whether it is Premium, Standard or Social.',
            self::INVBUSN  => 'Invest by lending direct to Biz Loan and Prop Loan products choosing from a list of profiled Premium or Standard projects.',
            self::INVSOCL  => 'Invest by lending direct to Biz Loan and Prop Loan products choosing from a list of profiled Social projects. Clients will also be able to choose Consollify Social.'
        ];
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $description;


    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $code;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Product
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Product
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Product
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }
}
