<?php
/**
 * Created by PhpStorm.
 * User: vladislav.shatilenya
 * Date: 05.01.2017
 * Time: 12:17
 */

namespace AppBundle\Entity;


use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

class UnderwritingResult
{
    use Timestampable;

    const TYPE_QUOTE = 1;
    const TYPE_MANAGEMENT = 2;
    const TYPE_CREDIT = 3;

    public static function getUnderwritingResultTypes()
    {
        return [
            self::TYPE_QUOTE,
            self::TYPE_MANAGEMENT,
            self::TYPE_CREDIT
        ];
    }

    /** @var  integer */
    private $id;

    /** @var QuoteApply */
    private $quoteApply;

    /** @var LoanApply */
    private $loanApply;

    /** @var integer */
    private $type;

    /** @var string */
    private $bonusAmount;

    /** @var string */
    private $fixedAmount;

    /** @var string */
    private $managementAmount;

    /** @var string */
    private $investorInterestAmount;

    /** @var string */
    private $loanAmount;

    /** @var string */
    private $totalRepaid;

    /** @var string */
    private $monthly;

    /** @var string */
    private $bonusRate;

    /** @var string */
    private $apr;

    /** @var string */
    private $effectiveRate;

    /** @var string */
    private $investorRate;

    /** @var integer */
    private $score1;

    /** @var integer */
    private $term;

    /**
     * @return integer
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Set term
     *
     * @param integer $term
     * @return UnderwritingResult
     */
    public function setTerm($term)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get quoteApply
     *
     * @return QuoteApply
     */
    public function getQuoteApply()
    {
        return $this->quoteApply;
    }

    /**
     * Set quoteApply
     *
     * @param QuoteApply $quoteApply
     * @return UnderwritingResult
     */
    public function setQuoteApply($quoteApply)
    {
        $this->quoteApply = $quoteApply;

        return $this;
    }

    /**
     * Get loanApply
     *
     * @return LoanApply
     */
    public function getLoanApply()
    {
        return $this->loanApply;
    }

    /**
     * Set LoanApply
     *
     * @param LoanApply $loanApply
     * @return UnderwritingResult
     */
    public function setLoanApply($loanApply)
    {
        $this->loanApply = $loanApply;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param int $type
     * @return UnderwritingResult
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get bonusAmount
     *
     * @return string
     */
    public function getBonusAmount()
    {
        return $this->bonusAmount;
    }

    /**
     * Set bonusAmount
     *
     * @param string $bonusAmount
     * @return UnderwritingResult
     */
    public function setBonusAmount($bonusAmount)
    {
        $this->bonusAmount = $bonusAmount;

        return $this;
    }

    /**
     * Get fixedAmount
     *
     * @return string
     */
    public function getFixedAmount()
    {
        return $this->fixedAmount;
    }

    /**
     * Set fixedAmount
     *
     * @param string $fixedAmount
     * @return UnderwritingResult
     */
    public function setFixedAmount($fixedAmount)
    {
        $this->fixedAmount = $fixedAmount;

        return $this;
    }

    /**
     * Get managementAmount
     *
     * @return string
     */
    public function getManagementAmount()
    {
        return $this->managementAmount;
    }

    /**
     * Set managementAmount
     *
     * @param string $managementAmount
     * @return UnderwritingResult
     */
    public function setManagementAmount($managementAmount)
    {
        $this->managementAmount = $managementAmount;

        return $this;
    }

    /**
     * Get investorInterestAmount
     *
     * @return string
     */
    public function getInvestorInterestAmount()
    {
        return $this->investorInterestAmount;
    }

    /**
     * Set investorInterestAmount
     *
     * @param string $investorInterestAmount
     * @return UnderwritingResult
     */
    public function setInvestorInterestAmount($investorInterestAmount)
    {
        $this->investorInterestAmount = $investorInterestAmount;

        return $this;
    }

    /**
     * Get loanAmount
     *
     * @return string
     */
    public function getLoanAmount()
    {
        return $this->loanAmount;
    }

    /**
     * Set loanAmount
     *
     * @param string $loanAmount
     * @return UnderwritingResult
     */
    public function setLoanAmount($loanAmount)
    {
        $this->loanAmount = $loanAmount;

        return $this;
    }

    /**
     * Get totalRepaid
     *
     * @return string
     */
    public function getTotalRepaid()
    {
        return $this->totalRepaid;
    }

    /**
     * Set totalRepaid
     *
     * @param string $totalRepaid
     * @return UnderwritingResult
     */
    public function setTotalRepaid($totalRepaid)
    {
        $this->totalRepaid = $totalRepaid;

        return $this;
    }

    /**
     * Get monthly
     *
     * @return string
     */
    public function getMonthly()
    {
        return $this->monthly;
    }

    /**
     * Set monthly
     *
     * @param string $monthly
     * @return UnderwritingResult
     */
    public function setMonthly($monthly)
    {
        $this->monthly = $monthly;

        return $this;
    }

    /**
     * Get bonusRate
     *
     * @return string
     */
    public function getBonusRate()
    {
        return $this->bonusRate;
    }

    /**
     * Set bonusRate
     *
     * @param string $bonusRate
     * @return UnderwritingResult
     */
    public function setBonusRate($bonusRate)
    {
        $this->bonusRate = $bonusRate;

        return $this;
    }

    /**
     * Get effectiveRate
     *
     * @return string
     */
    public function getEffectiveRate()
    {
        return $this->effectiveRate;
    }

    /**
     * Set effectiveRate
     *
     * @param string $effectiveRate
     * @return UnderwritingResult
     */
    public function setEffectiveRate($effectiveRate)
    {
        $this->effectiveRate = $effectiveRate;

        return $this;
    }

    /**
     * Get apr
     *
     * @return string
     */
    public function getApr()
    {
        return $this->apr;
    }

    /**
     * Set apr
     *
     * @param string $apr
     * @return UnderwritingResult
     */
    public function setApr($apr)
    {
        $this->apr = $apr;

        return $this;
    }

    /**
     * Get investorRate
     *
     * @return string
     */
    public function getInvestorRate()
    {
        return $this->investorRate;
    }

    /**
     * Set investorRate
     *
     * @param string $investorRate
     * @return UnderwritingResult
     */
    public function setInvestorRate($investorRate)
    {
        $this->investorRate = $investorRate;

        return $this;
    }

    /**
     * Get score1
     *
     * @return int
     */
    public function getScore1()
    {
        return $this->score1;
    }

    /**
     * Set score1
     *
     * @param integer $score1
     * @return UnderwritingResult
     */
    public function setScore1($score1)
    {
        $this->score1 = $score1;

        return $this;
    }
}
