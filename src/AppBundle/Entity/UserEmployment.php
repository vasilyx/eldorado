<?php

namespace AppBundle\Entity;

use AppBundle\Interfaces\ArchivableInterface;
use AppBundle\Interfaces\SetArchiveFalseInterface;
use AppBundle\Traits\Archivable\Archivable;
use AppBundle\Traits\Archivable\SetArchiveFalseTrait;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * UserEmployment
 */
class UserEmployment implements ArchivableInterface, SetArchiveFalseInterface
{
    use Timestampable;
    use Archivable;
    use SetArchiveFalseTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $occMonthStarted;

    /**
     * @var \DateTime
     */
    private $occMonthEnded;

    /**
     * @var string
     */
    private $businessFirmName;

    /**
     * @var string
     */
    private $industry;

    /**
     * @var string
     */
    private $occPostalCode;

    /**
     * @var string
     */
    private $occCountry;

    /**
     * @var string
     */
    private $occStatus;

    /**
     * @var string
     */
    private $jobDescription;

    /**
     * @var string
     */
    private $profQual;

    /**
     * @var boolean
     */
    private $currentStatus;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set occMonthStarted
     *
     * @param \DateTime $occMonthStarted
     * @return UserEmployment
     */
    public function setOccMonthStarted($occMonthStarted)
    {
        $this->occMonthStarted = $occMonthStarted->setTime(0, 0, 0);;

        return $this;
    }

    /**
     * Get occMonthStarted
     *
     * @return \DateTime 
     */
    public function getOccMonthStarted()
    {
        return $this->occMonthStarted;
    }

    /**
     * Set occMonthEnded
     *
     * @param \DateTime $occMonthEnded
     * @return UserEmployment
     */
    public function setOccMonthEnded($occMonthEnded)
    {
        $this->occMonthEnded = $occMonthEnded->setTime(0, 0, 0);

        return $this;
    }

    /**
     * Get occMonthEnded
     *
     * @return \DateTime 
     */
    public function getOccMonthEnded()
    {
        return $this->occMonthEnded;
    }

    /**
     * Set businessFirmName
     *
     * @param string $businessFirmName
     * @return UserEmployment
     */
    public function setBusinessFirmName($businessFirmName)
    {
        $this->businessFirmName = $businessFirmName;

        return $this;
    }

    /**
     * Get businessFirmName
     *
     * @return string 
     */
    public function getBusinessFirmName()
    {
        return $this->businessFirmName;
    }

    /**
     * Set industry
     *
     * @param string $industry
     * @return UserEmployment
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;

        return $this;
    }

    /**
     * Get industry
     *
     * @return string 
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * Set occPostalCode
     *
     * @param string $occPostalCode
     * @return UserEmployment
     */
    public function setOccPostalCode($occPostalCode)
    {
        $this->occPostalCode = $occPostalCode;

        return $this;
    }

    /**
     * Get occPostalCode
     *
     * @return string 
     */
    public function getOccPostalCode()
    {
        return $this->occPostalCode;
    }

    /**
     * Set occCountry
     *
     * @param string $occCountry
     * @return UserEmployment
     */
    public function setOccCountry($occCountry)
    {
        $this->occCountry = $occCountry;

        return $this;
    }

    /**
     * Get occCountry
     *
     * @return string 
     */
    public function getOccCountry()
    {
        return $this->occCountry;
    }

    /**
     * Set occStatus
     *
     * @param string $occStatus
     * @return UserEmployment
     */
    public function setOccStatus($occStatus)
    {
        $this->occStatus = $occStatus;

        return $this;
    }

    /**
     * Get occStatus
     *
     * @return string 
     */
    public function getOccStatus()
    {
        return $this->occStatus;
    }

    /**
     * Set jobDescription
     *
     * @param string $jobDescription
     * @return UserEmployment
     */
    public function setJobDescription($jobDescription)
    {
        $this->jobDescription = $jobDescription;

        return $this;
    }

    /**
     * Get jobDescription
     *
     * @return string 
     */
    public function getJobDescription()
    {
        return $this->jobDescription;
    }

    /**
     * Set profQual
     *
     * @param string $profQual
     * @return UserEmployment
     */
    public function setProfQual($profQual)
    {
        $this->profQual = $profQual;

        return $this;
    }

    /**
     * Get profQual
     *
     * @return string 
     */
    public function getProfQual()
    {
        return $this->profQual;
    }

    /**
     * Set currentStatus
     *
     * @param boolean $currentStatus
     * @return UserEmployment
     */
    public function setCurrentStatus($currentStatus)
    {
        $this->currentStatus = $currentStatus;

        return $this;
    }

    /**
     * Get currentStatus
     *
     * @return boolean 
     */
    public function getCurrentStatus()
    {
        return $this->currentStatus;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return UserEmployment
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
