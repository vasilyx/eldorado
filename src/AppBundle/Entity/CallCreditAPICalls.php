<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 21.12.2016
 * Time: 13:10
 */

namespace AppBundle\Entity;


use AppBundle\Traits\StaticCreatable;

class CallCreditAPICalls
{
    use StaticCreatable;

    const STATUS_NEW        = 1;
    const STATUS_PENDING    = 2;
    const STATUS_SUCCESS    = 3;
    const STATUS_ERROR      = 4;

    const TYPE_CALLVALIDATE   = 1;
    const TYPE_CALLREPORT     = 2;
    const TYPE_AFFORDABILITY  = 3;

    const PURPOSE_QS = 1;
    const PURPOSE_MN = 2;
    const PURPOSE_CA = 3;

    public static function getPurposes()
    {
        return [
            self::PURPOSE_QS => 'QS',
            self::PURPOSE_CA => 'CA',
            self::PURPOSE_MN => 'MN',
        ];
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_NEW => 'new',
            self::STATUS_PENDING => 'pending',
            self::STATUS_SUCCESS => 'success',
            self::STATUS_ERROR => 'error'
        ];
    }

    public static function getTypes()
    {
        return [
            self::TYPE_CALLVALIDATE => 'callvalidate',
            self::TYPE_CALLREPORT => 'callreport',
            self::TYPE_AFFORDABILITY => 'affordability'
        ];
    }
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $requestDate;

    /**
     * @var \DateTime;
     */
    private $responseDate;

    /**
     * @var string
     */
    private $requestXML;

    /**
     * @var string
     */
    private $responseXML;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var string
     */
    private $requestID;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var User
     */
    private $user;

    /**
     * @var QuoteApply
     */
    private $quoteApply;

    /** @var  integer */
    private $purpose;


    /**
     * Get purpose
     *
     * @return int
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * Set purpose
     *
     * @param int $purpose
     * @return CallCreditAPICalls
     */
    public function setPurpose($purpose)
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
    * @return int
    */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return CallCreditApiCalls
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get quoteApply
     *
     * @return QuoteApply
     */
    public function getQuoteApply()
    {
        return $this->quoteApply;
    }

    /**
     * Set quoteApply
     *
     * @param QuoteApply $quoteApply
     * @return CallCreditApiCalls
     */
    public function setQuoteApply($quoteApply)
    {
        $this->quoteApply = $quoteApply;

        return $this;
    }

    /**
     * Get date of request
     *
     * @return \DateTime
     */
    public function getRequestDate()
    {
        return $this->requestDate;
    }

    /**
     * Set date of request
     *
     * @param \DateTime $requestDate
     * @return CallCreditApiCalls
     */
    public function setRequestDate($requestDate)
    {
        $this->requestDate = $requestDate;

        return $this;
    }

    /**
     * Get date of response
     *
     * @return \DateTime
     */
    public function getResponseDate()
    {
        return $this->responseDate;
    }

    /**
     * Set date of response
     *
     * @param \DateTime $responseDate
     * @return CallCreditApiCalls
     */
    public function setResponseDate($responseDate)
    {
        $this->responseDate = $responseDate;

        return $this;
    }

    /**
     * Get requestXML
     *
     * @return string
     */
    public function getRequestXML()
    {
        return $this->requestXML;
    }

    /**
     * Set reuqestXML
     *
     * @param string $requestXML
     * @return CallCreditApiCalls
     */
    public function setRequestXML($requestXML)
    {
        $this->requestXML = $requestXML;

        return $this;
    }

    /**
     * Get responseXML
     *
     * @return string
     */
    public function getResponseXML()
    {
        return $this->responseXML;
    }

    /**
     * Set responseXML
     *
     * @param string $responseXML
     * @return CallCreditApiCalls
     */
    public function setResponseXML($responseXML)
    {
        $this->responseXML = $responseXML;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param int $type
     * @return CallCreditApiCalls
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get requestID
     *
     * @return string
     */
    public function getRequestID()
    {
        return $this->requestID;
    }

    /**
     * Set requestID
     *
     * @param string $requestID
     * @return CallCreditApiCalls
     */
    public function setRequestID($requestID)
    {
        $this->requestID = $requestID;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param mixed $status
     * @return CallCreditApiCalls
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

}
