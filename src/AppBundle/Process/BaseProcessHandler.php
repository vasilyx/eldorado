<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 24.02.17
 * Time: 7:29
 */

namespace AppBundle\Process;
use AppBundle\Entity\BaseProcess;
use AppBundle\Entity\Event;
use AppBundle\Event\SystemTransactionEvent;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Process handler keeps events queue, started process and generates the next event
 *
 * Class BaseProcessHandler
 * @package AppBundle\Process
 */
abstract class BaseProcessHandler
{
    use ContainerAwareTrait;
    /**
     * @var string
     */
    protected $processClass;

    protected $processRange = 0;

    abstract public function getEventsQueue();
    abstract public function nextStep(BaseProcess $process, SystemTransactionEvent $systemTransactionEvent);

    public function start(BaseProcess $process, SystemTransactionEvent $systemTransactionEvent)
    {
        if (!$process instanceof $this->processClass) {
            throw new \Exception(sprintf('Process should be instance of %1$s, %2$s given', $this->processClass, get_class($process)));
        }
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $entityManager->persist($process);
        $entityManager->flush();
    }

    public function setProcessClass($processClass)
    {
        $this->processClass = $processClass;
    }

    public function getProcessClass()
    {
        return $this->processClass;
    }

    public function getProcessRange(BaseProcess $process)
    {
        /**
         * @var Event[] $events
         */
        $events = $process->getEvents()->toArray();
        foreach ($events as $event) {
            $this->processRange = $this->processRange + $this->getEventsQueue()[$event->getType()];
        }

        return $this->processRange;
    }
}