<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 24.02.17
 * Time: 15:16
 */

namespace AppBundle\Process;


use AppBundle\Entity\BaseProcess;

class ProcessHandlersResolver
{
    /**
     * @var BaseProcessHandler[]
     */
    private $processHandlers;

    public function addProcessHandler(BaseProcessHandler $processHandler)
    {
        $this->processHandlers[] = $processHandler;
    }

    public function resolve(BaseProcess $process)
    {
        foreach ($this->processHandlers as $processHandler) {
            if ($processHandler->getProcessClass() === get_class($process)) {
                return $processHandler;
            }
        }
    }
}