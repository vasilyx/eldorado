<?php
/**
 * Created by PhpStorm.
 * User: effective-soft
 * Date: 24.02.17
 * Time: 7:51
 */

namespace AppBundle\Process\Handlers;


use AppBundle\Entity\BaseProcess;
use AppBundle\Entity\InvestOfferProcess;
use AppBundle\Entity\WorldpayHtmlTransaction;
use AppBundle\Event\SystemTransactionEvent;
use AppBundle\Event\TransactionEvents;
use AppBundle\Process\BaseProcessHandler;

class InvestOfferProcessHandler extends BaseProcessHandler
{

    /**
     * Method is responsible for generation of process tree. The result will be used by further logic
     *
     * @return array
     */
    public function getEventsQueue()
    {
        return [
            TransactionEvents::DEPC => 10,
            TransactionEvents::DEPA => 20,
            TransactionEvents::DEPX => -10,
            'OFRF' => 30
        ];
    }

    /**
     * Makes next step in the process
     *
     * @param BaseProcess $process
     * @param SystemTransactionEvent $systemTransactionEvent
     */
    public function nextStep(BaseProcess $process, SystemTransactionEvent $systemTransactionEvent)
    {
        switch ($this->getProcessRange($process)) {
            case 0:
                $this->createDEPC($process, $systemTransactionEvent);
                break;
            case 10:
                if (WorldpayHtmlTransaction::TRANS_STATUS_SUCCESS === $systemTransactionEvent->getArgument('transStatus')) {
                    $this->createDEPA($process, $systemTransactionEvent);
                }
                if (WorldpayHtmlTransaction::TRANS_STATUS_CANCEL === $systemTransactionEvent->getArgument('transStatus')) {
                    $this->createDEPX($process, $systemTransactionEvent);
                }
                break;
        }
    }

    public function start(BaseProcess $process, SystemTransactionEvent $systemTransactionEvent)
    {
        parent::start($process, $systemTransactionEvent);
    }

    protected function createDEPA(BaseProcess $process, SystemTransactionEvent $systemTransactionEvent)
    {
        $this->container->get('event_dispatcher')->dispatch(TransactionEvents::DEPA, $systemTransactionEvent->setProcess($process));

        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $entityManager->flush();
        $this->container->get('event_dispatcher')->dispatch(TransactionEvents::OFRF, $systemTransactionEvent);

        /**
         * Logging
         */
        $this->container->get('monolog.logger.callcredit')->debug(sprintf('Event %1$s is generated.', TransactionEvents::DEPA), [$this->container->getParameter('kernel.environment')]);
        /**
         * End logging
         */
    }

    protected function createDEPX(BaseProcess $process, SystemTransactionEvent $systemTransactionEvent)
    {
        $this->container->get('event_dispatcher')->dispatch(TransactionEvents::DEPX, $systemTransactionEvent->setProcess($process)
        );

        /**
         * Logging
         */
        $this->container->get('monolog.logger.callcredit')->debug(sprintf('Event %1$s is generated.', TransactionEvents::DEPX), [$this->container->getParameter('kernel.environment')]);
        /**
         * End logging
         */
    }

    protected function createDEPC(BaseProcess $process, SystemTransactionEvent $systemTransactionEvent)
    {
        $this->container->get('event_dispatcher')->dispatch(TransactionEvents::DEPC, $systemTransactionEvent->setProcess($process));
    }

    public function getAmount(InvestOfferProcess $process)
    {
        return $process->getInvestOffer()->getInvestmentAmount();
    }
}