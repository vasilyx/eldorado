<?php

namespace App\DataFixtures;

use AdminBundle\Entity\User;
use AdminBundle\Manager\UserManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 01.06.2018
 * Time: 12:52
 */
class AdminsFixtures extends Fixture
{
    //Command: php app/console doctrine:fixtures:load --fixtures=src/DataFixtures --append
    public function load(ObjectManager $manager)
    {
        $adminEmails = ['diego@wh.money', 'thomas@wh.money', 'natasha@wh.money', 'michaela@stordeo.com', 'yingdee@wh.money', 'wayne@wh.money', 'gabriella@wh.money', 'ezra@savvney.com', 'frank@wh.money'];

        $userManager = new UserManager();
        $userManager->setContainer($this->container);

        foreach ($adminEmails as $adminEmail) {
            $user = new User();
            $user
                ->setUsername($adminEmail)
                ->setSalt($userManager->generateSalt())
            ;

            $user
                ->setPassword($userManager->encodePassword($user, 'A11111111'))
                ->eraseCredentials()
            ;

            $user->setRoles(['ROLE_SUPER_ADMIN']);

            $manager->persist($user);
        }

        $manager->flush();
    }
}