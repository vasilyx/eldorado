<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161118161241 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_address ADD city VARCHAR(45) DEFAULT NULL COMMENT \'postcodeanywhere City\', ADD street VARCHAR(45) DEFAULT NULL COMMENT \'postcodeanywhere Street\', ADD building_number VARCHAR(45) DEFAULT NULL COMMENT \'postcodeanywhere BuildingNumber\', ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL, ADD deleted_at DATETIME DEFAULT NULL, DROP address1, DROP address2, DROP address3, CHANGE postal_code postal_code VARCHAR(45) NOT NULL COMMENT \'postcodeanywhere PostalCode\', CHANGE country country VARCHAR(45) DEFAULT NULL COMMENT \'postcodeanywhere CountryName\', CHANGE active active TINYINT(1) DEFAULT NULL COMMENT \'this field is used for putting the address into the archive (no active address)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_address ADD address1 VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD address2 VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD address3 VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, DROP city, DROP street, DROP building_number, DROP created_at, DROP updated_at, DROP deleted_at, CHANGE postal_code postal_code VARCHAR(45) NOT NULL COLLATE utf8_unicode_ci, CHANGE country country VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE active active TINYINT(1) DEFAULT NULL');
    }
}
