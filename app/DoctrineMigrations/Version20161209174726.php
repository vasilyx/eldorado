<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161209174726 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_address CHANGE postal_code postal_code VARCHAR(8) NOT NULL COMMENT \'postcodeanywhere PostalCode\', CHANGE country country VARCHAR(45) NOT NULL COMMENT \'postcodeanywhere CountryName\', CHANGE city city VARCHAR(35) NOT NULL COMMENT \'postcodeanywhere City\', CHANGE building_number building_number VARCHAR(10) DEFAULT NULL COMMENT \'postcodeanywhere BuildingNumber\', CHANGE building_name building_name VARCHAR(50) DEFAULT NULL COMMENT \'postcodeanywhere BuildingName\', CHANGE line1 line1 VARCHAR(255) NOT NULL COMMENT \'postcodeanywhere line1\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_address CHANGE postal_code postal_code VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'postcodeanywhere PostalCode\', CHANGE city city VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'postcodeanywhere City\', CHANGE building_name building_name VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'postcodeanywhere BuildingName\', CHANGE building_number building_number VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'postcodeanywhere BuildingNumber\', CHANGE line1 line1 VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'postcodeanywhere line1\', CHANGE country country VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'postcodeanywhere CountryName\'');
    }
}
