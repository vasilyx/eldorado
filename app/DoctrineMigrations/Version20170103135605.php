<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170103135605 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE world_pay_transaction ADD customer_order_code VARCHAR(255) NOT NULL COMMENT \'The code or ID under which this order is known in your systems\', ADD dtype VARCHAR(255) NOT NULL, ADD callback_url VARCHAR(255) DEFAULT NULL, CHANGE token token VARCHAR(255) DEFAULT NULL COMMENT \'A unique token which the WorldPay.js library added to your checkout form, or obtained via the token API. This token represents the customer\'\'s card details/payment method which was stored on our server. One of token or paymentMethod must be specified\', CHANGE order_description order_description VARCHAR(255) DEFAULT NULL COMMENT \'The description of the order provided by you\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE world_pay_transaction DROP customer_order_code, DROP dtype, DROP callback_url, CHANGE token token VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci COMMENT \'A unique token which the WorldPay.js library added to your checkout form, or obtained via the token API. This token represents the customer\'\'s card details/payment method which was stored on our server. One of token or paymentMethod must be specified\', CHANGE order_description order_description VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci COMMENT \'The description of the order provided by you\'');
    }
}
