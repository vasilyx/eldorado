<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\Product;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180621160518 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE product CHANGE description description LONGTEXT NOT NULL');

        $result = $this->connection->fetchAll('SELECT * FROM product');

        foreach (Product::getProductShortNamesMap() as $key => $shortName) {
            if (in_array($key, array_column($result, 'code'))) {
                $this->addSql('UPDATE product SET description=:description WHERE code=:code', [
                        'description' => Product::getProductNamesMap()[$key],
                        'code' => $key]
                );
            }
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
