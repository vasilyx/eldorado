<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170310184214 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_details DROP FOREIGN KEY FK_2A2B158059F5EDE4');
        $this->addSql('DROP INDEX fk_user_details_2_idx ON user_details');
        $this->addSql('ALTER TABLE user_details DROP default_user_communication_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_details ADD default_user_communication_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_details ADD CONSTRAINT FK_2A2B158059F5EDE4 FOREIGN KEY (default_user_communication_id) REFERENCES user_communication (id)');
        $this->addSql('CREATE INDEX fk_user_details_2_idx ON user_details (default_user_communication_id)');
    }
}
