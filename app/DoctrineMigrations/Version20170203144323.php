<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170203144323 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->connection->beginTransaction();
        $this->addSql("INSERT INTO restriction_group (id, code, name) VALUES 
                      (1, 'age', 'Age'),
                      (2, 'gender', 'Gender'),
                      (3, 'amount', 'Loan amount')
                      ");
        $this->addSql("INSERT INTO restriction 
                      (id, restriction_group_id, code, name, archive, created_at, updated_at) VALUES 
                      (1, 1, '18_35', 'From 18 to 35', 0, NOW(), NOW()), 
                      (2, 1, '35_55', 'From 35 to 55', 0, NOW(), NOW()), 
                      (3, 1, '55_', 'More than 55', 0, NOW(), NOW()),
                      
                      (4, 2, 'male', 'Male', 0, NOW(), NOW()), 
                      (5, 2, 'female', 'Female', 0, NOW(), NOW()), 
                      
                      (6, 3, '_15K', 'Less than 15,000', 0, NOW(), NOW()), 
                      (7, 3, '15K_', 'More than 15,000', 0, NOW(), NOW())
                      ");
        $this->connection->commit();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->connection->beginTransaction();
        $this->addSql("DELETE FROM restriction WHERE 1=1");
        $this->addSql("ALTER TABLE restriction AUTO_INCREMENT = 1;");
        $this->addSql("DELETE FROM restriction_group WHERE 1=1");
        $this->addSql("ALTER TABLE restriction_group AUTO_INCREMENT = 1;");
        $this->connection->commit();
    }
}
