<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161123143442 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_address ADD building_name VARCHAR(255) DEFAULT NULL COMMENT \'postcodeanywhere BuildingName\', ADD sub_building_name VARCHAR(255) DEFAULT NULL COMMENT \'postcodeanywhere subBuildingName\', ADD dependant_locality VARCHAR(255) DEFAULT NULL, ADD line1 VARCHAR(255) DEFAULT NULL COMMENT \'postcodeanywhere line1\', ADD line2 VARCHAR(255) DEFAULT NULL COMMENT \'postcodeanywhere line2\', CHANGE postal_code postal_code VARCHAR(45) DEFAULT NULL COMMENT \'postcodeanywhere PostalCode\', CHANGE country country VARCHAR(45) DEFAULT NULL COMMENT \'postcodeanywhere CountryName\', CHANGE city city VARCHAR(45) DEFAULT NULL COMMENT \'postcodeanywhere City\', CHANGE street street VARCHAR(45) DEFAULT NULL COMMENT \'postcodeanywhere Street\', CHANGE building_number building_number VARCHAR(45) DEFAULT NULL COMMENT \'postcodeanywhere BuildingNumber\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_address DROP building_name, DROP sub_building_name, DROP dependant_locality, DROP line1, DROP line2, CHANGE postal_code postal_code VARCHAR(45) NOT NULL COLLATE utf8_unicode_ci COMMENT \'postcodeanywhere PostalCode\', CHANGE city city VARCHAR(45) NOT NULL COLLATE utf8_unicode_ci COMMENT \'postcodeanywhere City\', CHANGE street street VARCHAR(45) NOT NULL COLLATE utf8_unicode_ci COMMENT \'postcodeanywhere Street\', CHANGE building_number building_number VARCHAR(45) NOT NULL COLLATE utf8_unicode_ci COMMENT \'postcodeanywhere BuildingNumber\', CHANGE country country VARCHAR(45) NOT NULL COLLATE utf8_unicode_ci COMMENT \'postcodeanywhere CountryName\'');
    }
}
