<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161208100205 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transaction CHANGE borrower_instalment borrower_instalment NUMERIC(10, 2) DEFAULT NULL, CHANGE borrower_principle borrower_principle NUMERIC(10, 2) DEFAULT NULL, CHANGE borrower_interest borrower_interest NUMERIC(5, 2) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transaction CHANGE borrower_instalment borrower_instalment DOUBLE PRECISION DEFAULT NULL, CHANGE borrower_principle borrower_principle DOUBLE PRECISION DEFAULT NULL, CHANGE borrower_interest borrower_interest DOUBLE PRECISION DEFAULT NULL');
    }
}
