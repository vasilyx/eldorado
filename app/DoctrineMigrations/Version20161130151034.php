<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161130151034 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE invest_offer ADD rate NUMERIC(10, 0) NOT NULL, CHANGE investment_type investment_type VARCHAR(36) NOT NULL, CHANGE finance_type finance_type VARCHAR(36) NOT NULL, CHANGE investment_amount investment_amount INT NOT NULL, CHANGE investment_frequency investment_frequency VARCHAR(24) NOT NULL, CHANGE term term INT NOT NULL, CHANGE prioritisation_tags prioritisation_tags VARCHAR(288) NOT NULL, CHANGE restriction_tags restriction_tags VARCHAR(288) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE invest_offer DROP rate, CHANGE investment_type investment_type VARCHAR(36) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE finance_type finance_type VARCHAR(36) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE investment_amount investment_amount INT DEFAULT NULL, CHANGE investment_frequency investment_frequency VARCHAR(24) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE term term INT DEFAULT NULL, CHANGE prioritisation_tags prioritisation_tags VARCHAR(288) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE restriction_tags restriction_tags VARCHAR(288) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
