<?php

namespace AppBundle\Migrations;

use AdminBundle\Entity\User;
use AdminBundle\Manager\UserManager;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170209170103 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $user = new User();
        $userManager = new UserManager();
        $userManager->setContainer($this->container);
        $user
            ->setUsername('Kirill.Makarenko@effective-soft.com')
            ->setSalt($userManager->generateSalt())
        ;

        $user
            ->setPassword($userManager->encodePassword($user, 'whadmin2017'))
            ->eraseCredentials()
        ;

        $data = [
            'username'  => $user->getUsername(),
            'password'  => $user->getPassword(),
            'salt'      => $user->getSalt(),
            'roles'     => json_encode(['ROLE_SUPER_ADMIN'])
        ];

        $this->addSql('INSERT INTO whadmin_user (username, password, salt, roles, created_at, updated_at) VALUES (:username, :password, :salt, :roles, NOW(), NOW())', $data);

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
