<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180531124717 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agreement CHANGE monthly_repayment monthly_repayment NUMERIC(16, 8) NOT NULL, CHANGE bonus_amount bonus_amount NUMERIC(16, 8) NOT NULL, CHANGE agreement_platform_fixed_amt agreement_platform_fixed_amt NUMERIC(16, 8) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agreement CHANGE monthly_repayment monthly_repayment NUMERIC(10, 8) NOT NULL, CHANGE bonus_amount bonus_amount NUMERIC(10, 8) NOT NULL, CHANGE agreement_platform_fixed_amt agreement_platform_fixed_amt NUMERIC(10, 8) DEFAULT NULL');
    }
}
