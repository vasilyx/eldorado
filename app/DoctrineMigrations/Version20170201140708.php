<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170201140708 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE restriction (id INT AUTO_INCREMENT NOT NULL, restriction_group_id INT NOT NULL, code VARCHAR(10) NOT NULL, name VARCHAR(60) NOT NULL, UNIQUE INDEX UNIQ_7A999BCE77153098 (code), UNIQUE INDEX UNIQ_7A999BCE5E237E06 (name), INDEX IDX_7A999BCE292F112 (restriction_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE restriction_group (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(10) NOT NULL, name VARCHAR(60) NOT NULL, UNIQUE INDEX UNIQ_F3E2823477153098 (code), UNIQUE INDEX UNIQ_F3E282345E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE restriction ADD CONSTRAINT FK_7A999BCE292F112 FOREIGN KEY (restriction_group_id) REFERENCES restriction_group (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE restriction DROP FOREIGN KEY FK_7A999BCE292F112');
        $this->addSql('DROP TABLE restriction');
        $this->addSql('DROP TABLE restriction_group');
    }
}
