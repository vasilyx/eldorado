<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161201151128 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE invest_offer CHANGE confirm_invest confirm_invest TINYINT(1) DEFAULT NULL, CHANGE risk_confirm risk_confirm TINYINT(1) DEFAULT NULL, CHANGE accept_terms accept_terms TINYINT(1) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE invest_offer CHANGE confirm_invest confirm_invest TINYINT(1) NOT NULL, CHANGE risk_confirm risk_confirm TINYINT(1) NOT NULL, CHANGE accept_terms accept_terms TINYINT(1) NOT NULL');
    }
}
