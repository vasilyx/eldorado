<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180525160146_loan_apply_extra_fields extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE loan_apply ADD amount DOUBLE PRECISION DEFAULT NULL, ADD term INT DEFAULT NULL, ADD apr DOUBLE PRECISION DEFAULT NULL, DROP summ, CHANGE monthly monthly DOUBLE PRECISION DEFAULT NULL, CHANGE status status INT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE loan_apply ADD summ VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, DROP amount, DROP term, DROP apr, CHANGE monthly monthly VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE status status VARCHAR(24) NOT NULL COLLATE utf8_unicode_ci');
    }
}
