<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170104143328 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE loan_apply ADD quote_apply_id INT NOT NULL, DROP request_id, CHANGE user_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE loan_apply ADD CONSTRAINT FK_EA969440A7E6634F FOREIGN KEY (quote_apply_id) REFERENCES quote_apply (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EA969440A7E6634F ON loan_apply (quote_apply_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE loan_apply DROP FOREIGN KEY FK_EA969440A7E6634F');
        $this->addSql('DROP INDEX UNIQ_EA969440A7E6634F ON loan_apply');
        $this->addSql('ALTER TABLE loan_apply ADD request_id VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, DROP quote_apply_id, CHANGE user_id user_id INT DEFAULT NULL');
    }
}
