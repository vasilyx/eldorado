<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180524104553 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agreement ADD creation_date DATETIME DEFAULT NULL, ADD amount INT NOT NULL, ADD term INT NOT NULL, ADD payment_day INT NOT NULL, ADD monthly_repayment NUMERIC(10, 8) NOT NULL, ADD bonus_amount NUMERIC(10, 8) NOT NULL, ADD agreement_platform_fixed_amt NUMERIC(10, 8) NOT NULL, DROP bonus_amt, DROP platform_fixed_amt, DROP platform_mgmt_amt, DROP investor_interest_amount, DROP loan_amount, DROP total_repaid, DROP monthly, DROP rate, DROP iInvestor_rate, DROP status, CHANGE bonus_rate bonus_rate NUMERIC(10, 2) NOT NULL, CHANGE apr apr NUMERIC(10, 2) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agreement ADD bonus_amt VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD platform_fixed_amt VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD platform_mgmt_amt VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD investor_interest_amount VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD loan_amount VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD total_repaid VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD monthly VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD rate VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD iInvestor_rate VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD status INT DEFAULT NULL, DROP creation_date, DROP amount, DROP term, DROP payment_day, DROP monthly_repayment, DROP bonus_amount, DROP agreement_platform_fixed_amt, CHANGE apr apr VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE bonus_rate bonus_rate VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
