<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170116141937 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quote_term ADD total_payment NUMERIC(10, 2) NOT NULL, ADD bonus_rate NUMERIC(8, 4) NOT NULL, ADD effective_rate NUMERIC(8, 4) NOT NULL, ADD broker_rate NUMERIC(8, 4) NOT NULL, ADD investor_rate NUMERIC(8, 4) NOT NULL, CHANGE rate contract_rate NUMERIC(8, 4) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quote_term ADD rate NUMERIC(8, 4) NOT NULL, DROP total_payment, DROP contract_rate, DROP bonus_rate, DROP effective_rate, DROP broker_rate, DROP investor_rate');
    }
}
