<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180607120634 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transaction ADD transaction_sub_account INT DEFAULT NULL, ADD gl_account INT DEFAULT NULL, CHANGE transaction_owner transaction_owner INT DEFAULT NULL, CHANGE transaction_account transaction_account INT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transaction DROP transaction_sub_account, DROP gl_account, CHANGE transaction_owner transaction_owner VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE transaction_account transaction_account VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
