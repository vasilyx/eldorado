<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170125105450 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE user_communication_verification');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_communication_verification (id INT AUTO_INCREMENT NOT NULL, communication_id INT DEFAULT NULL, user_id INT DEFAULT NULL, value VARCHAR(256) DEFAULT NULL COLLATE utf8_unicode_ci, code VARCHAR(256) DEFAULT NULL COLLATE utf8_unicode_ci, datetime_create INT DEFAULT NULL, datetime_verified INT DEFAULT NULL, INDEX fk_user_verification_1_idx (user_id), INDEX fk_user_communication_verification_1_idx (communication_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_communication_verification ADD CONSTRAINT FK_C80481BD1C2D1E0C FOREIGN KEY (communication_id) REFERENCES communication (id)');
        $this->addSql('ALTER TABLE user_communication_verification ADD CONSTRAINT FK_C80481BDA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }
}
