<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180615150645 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE instalment (id INT AUTO_INCREMENT NOT NULL, loan_apply_id INT NOT NULL, status SMALLINT NOT NULL, period SMALLINT NOT NULL, payment_date DATETIME NOT NULL, instalment DOUBLE PRECISION NOT NULL, interest DOUBLE PRECISION NOT NULL, principle DOUBLE PRECISION NOT NULL, balance DOUBLE PRECISION NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_D40DEC1B1A9A3175 (loan_apply_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE instalment ADD CONSTRAINT FK_D40DEC1B1A9A3175 FOREIGN KEY (loan_apply_id) REFERENCES loan_apply (id)');
        $this->addSql('ALTER TABLE agreement ADD balance DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE file DROP FOREIGN KEY FK_8C9F3610A7E6634F');
        $this->addSql('DROP INDEX IDX_8C9F3610A7E6634F ON file');
        $this->addSql('ALTER TABLE file ADD loan_apply_id INT NOT NULL, DROP quote_apply_id');
        $this->addSql('ALTER TABLE file ADD CONSTRAINT FK_8C9F36101A9A3175 FOREIGN KEY (loan_apply_id) REFERENCES loan_apply (id)');
        $this->addSql('CREATE INDEX IDX_8C9F36101A9A3175 ON file (loan_apply_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE instalment');
        $this->addSql('ALTER TABLE agreement DROP balance');
        $this->addSql('ALTER TABLE file DROP FOREIGN KEY FK_8C9F36101A9A3175');
        $this->addSql('DROP INDEX IDX_8C9F36101A9A3175 ON file');
        $this->addSql('ALTER TABLE file ADD quote_apply_id INT DEFAULT NULL, DROP loan_apply_id');
        $this->addSql('ALTER TABLE file ADD CONSTRAINT FK_8C9F3610A7E6634F FOREIGN KEY (quote_apply_id) REFERENCES quote_apply (id)');
        $this->addSql('CREATE INDEX IDX_8C9F3610A7E6634F ON file (quote_apply_id)');
    }
}
