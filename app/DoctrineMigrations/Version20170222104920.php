<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170222104920 extends AbstractMigration
{
    private static $regions = [
        ['code' => 'channel_islands', 'name' => 'Channel Islands'],
        ['code' => 'east_midlands', 'name' => 'East Midlands'],
        ['code' => 'east_england', 'name' => 'East of England'],
        ['code' => 'isle_of_man', 'name' => 'Isle of Man'],
        ['code' => 'london', 'name' => 'London'],
        ['code' => 'north_east', 'name' => 'North East'],
        ['code' => 'north_west', 'name' => 'North West'],
        ['code' => 'northern_ireland', 'name' => 'Northern Ireland'],
        ['code' => 'scotland', 'name' => 'Scotland'],
        ['code' => 'south_east', 'name' => 'South East'],
        ['code' => 'south_west', 'name' => 'South West'],
        ['code' => 'wales', 'name' => 'Wales'],
        ['code' => 'west_midlands', 'name' => 'West Midlands'],
        ['code' => 'yorkshire_and_humber', 'name' => 'Yorkshire and The Humber'],
    ];

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->connection->beginTransaction();
        $this->connection->insert('restriction_group', ['code' => 'region', 'name' => 'Region']);
        $restrictionGroup = $this->connection->fetchAssoc("SELECT id FROM restriction_group WHERE code = 'region'");
        foreach (self::$regions as $restriction) {
            $this->connection->insert(
                'restriction',
                ['restriction_group_id' => $restrictionGroup['id'], 'code' => $restriction['code'], 'name' => $restriction['name'], 'archive' => 0, 'created_at' => (new \DateTime())->format('Y-m-d H:i:s'), 'updated_at' => (new \DateTime())->format('Y-m-d H:i:s')]
            );
        }
        $this->connection->commit();

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->connection->beginTransaction();
        foreach (self::$regions as $restriction) {
            $this->connection->delete('restriction', ['code' => $restriction['code']]);
        }
        $this->connection->delete('restriction_group', ['code' => 'region']);
        $this->connection->commit();
    }
}
