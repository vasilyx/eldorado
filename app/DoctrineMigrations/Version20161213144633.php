<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161213144633 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_creditline ADD credit_limit INT NOT NULL, ADD settle_balance INT NOT NULL, ADD archive TINYINT(1) NOT NULL, ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL, DROP `limit`, DROP refinance_event_id, DROP balance, CHANGE type type VARCHAR(36) NOT NULL, CHANGE name name VARCHAR(36) NOT NULL, CHANGE balance_input balance_input INT NOT NULL, CHANGE apr apr NUMERIC(8, 4) NOT NULL, CHANGE monthly_payment monthly_payment INT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_creditline ADD `limit` VARCHAR(45) NOT NULL COLLATE utf8_unicode_ci, ADD refinance_event_id INT NOT NULL, ADD balance INT NOT NULL, DROP credit_limit, DROP settle_balance, DROP archive, DROP created_at, DROP updated_at, CHANGE type type VARCHAR(45) NOT NULL COLLATE utf8_unicode_ci, CHANGE name name VARCHAR(45) NOT NULL COLLATE utf8_unicode_ci, CHANGE balance_input balance_input VARCHAR(45) NOT NULL COLLATE utf8_unicode_ci, CHANGE apr apr VARCHAR(45) NOT NULL COLLATE utf8_unicode_ci, CHANGE monthly_payment monthly_payment DOUBLE PRECISION NOT NULL');
    }
}
