<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161212162634 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_household ADD monthly_rent INT NOT NULL, ADD monthly_utilities INT NOT NULL, ADD monthly_travel INT NOT NULL, ADD monthly_food INT NOT NULL, ADD monthly_savings INT NOT NULL, ADD monthly_entertainment INT NOT NULL, ADD active_status TINYINT(1) DEFAULT \'1\' NOT NULL, DROP name, DROP birth_date, DROP active, DROP creditline_id, CHANGE user_id user_id INT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_household ADD name VARCHAR(45) NOT NULL COLLATE utf8_unicode_ci, ADD birth_date DATETIME DEFAULT NULL, ADD active TINYINT(1) DEFAULT NULL, ADD creditline_id INT DEFAULT NULL, DROP monthly_rent, DROP monthly_utilities, DROP monthly_travel, DROP monthly_food, DROP monthly_savings, DROP monthly_entertainment, DROP active_status, CHANGE user_id user_id INT DEFAULT NULL');
    }
}
