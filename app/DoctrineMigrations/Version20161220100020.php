<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161220100020 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_household ADD spouse_first_name VARCHAR(36) DEFAULT NULL, ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL, CHANGE monthly_rent monthly_rent NUMERIC(10, 2) NOT NULL, CHANGE monthly_utilities monthly_utilities NUMERIC(10, 2) NOT NULL, CHANGE monthly_travel monthly_travel NUMERIC(10, 2) NOT NULL, CHANGE monthly_food monthly_food NUMERIC(10, 2) NOT NULL, CHANGE monthly_savings monthly_savings NUMERIC(10, 2) NOT NULL, CHANGE monthly_entertainment monthly_entertainment NUMERIC(10, 2) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_household DROP spouse_first_name, DROP created_at, DROP updated_at, CHANGE monthly_rent monthly_rent INT NOT NULL, CHANGE monthly_utilities monthly_utilities INT NOT NULL, CHANGE monthly_travel monthly_travel INT NOT NULL, CHANGE monthly_food monthly_food INT NOT NULL, CHANGE monthly_savings monthly_savings INT NOT NULL, CHANGE monthly_entertainment monthly_entertainment INT NOT NULL');
    }
}
