<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161227145901 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transaction ADD transaction_id VARCHAR(36) NOT NULL, ADD transaction_ccy VARCHAR(3) NOT NULL, ADD transaction_amount NUMERIC(10, 8) DEFAULT NULL, ADD transaction_owner VARCHAR(255) DEFAULT NULL, ADD transaction_account VARCHAR(255) DEFAULT NULL, ADD active_status TINYINT(1) NOT NULL, ADD report_date_time DATETIME NOT NULL, ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL, DROP ccy, DROP borrower_instalment, DROP borrower_principle, DROP borrower_interest, DROP borrower_misc, DROP investor_instalment, DROP investor_principle, DROP investor_interest, DROP investor_misc, DROP platform_fixed_fee, DROP platform_mgmt_fee, DROP platform_misc_fee, DROP trust_first_loss, DROP active, DROP field1, DROP field2, DROP field3, CHANGE contract_id parent_transaction INT DEFAULT NULL');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D1191B17A1 FOREIGN KEY (parent_transaction) REFERENCES transaction (id)');
        $this->addSql('CREATE INDEX IDX_723705D1191B17A1 ON transaction (parent_transaction)');
        $this->addSql('ALTER TABLE transaction RENAME INDEX fk_transactions_1_idx TO IDX_723705D171F7E88B');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D1191B17A1');
        $this->addSql('DROP INDEX IDX_723705D1191B17A1 ON transaction');
        $this->addSql('ALTER TABLE transaction ADD ccy VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD borrower_instalment NUMERIC(10, 2) DEFAULT NULL, ADD borrower_principle NUMERIC(10, 2) DEFAULT NULL, ADD borrower_interest NUMERIC(5, 2) DEFAULT NULL, ADD borrower_misc DOUBLE PRECISION DEFAULT NULL, ADD investor_instalment DOUBLE PRECISION DEFAULT NULL, ADD investor_principle DOUBLE PRECISION DEFAULT NULL, ADD investor_interest DOUBLE PRECISION DEFAULT NULL, ADD investor_misc DOUBLE PRECISION DEFAULT NULL, ADD platform_fixed_fee DOUBLE PRECISION DEFAULT NULL, ADD platform_mgmt_fee DOUBLE PRECISION DEFAULT NULL, ADD platform_misc_fee DOUBLE PRECISION DEFAULT NULL, ADD trust_first_loss VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD active TINYINT(1) DEFAULT NULL, ADD field1 VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD field2 VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD field3 VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, DROP transaction_id, DROP transaction_ccy, DROP transaction_amount, DROP transaction_owner, DROP transaction_account, DROP active_status, DROP report_date_time, DROP created_at, DROP updated_at, CHANGE parent_transaction contract_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE transaction RENAME INDEX idx_723705d171f7e88b TO fk_transactions_1_idx');
    }
}
