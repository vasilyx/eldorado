<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170301131832 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE agreements_tags (agreement_id INT NOT NULL, restriction_id INT NOT NULL, INDEX IDX_187A317D24890B2B (agreement_id), INDEX IDX_187A317DE6160631 (restriction_id), PRIMARY KEY(agreement_id, restriction_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE agreements_tags ADD CONSTRAINT FK_187A317D24890B2B FOREIGN KEY (agreement_id) REFERENCES agreement (id)');
        $this->addSql('ALTER TABLE agreements_tags ADD CONSTRAINT FK_187A317DE6160631 FOREIGN KEY (restriction_id) REFERENCES restriction (id)');
        $this->addSql('DROP TABLE loans_tags');
        $this->addSql('ALTER TABLE user_details DROP FOREIGN KEY FK_2A2B158059F5EDE4');
        $this->addSql('ALTER TABLE user_details ADD CONSTRAINT FK_2A2B158059F5EDE4 FOREIGN KEY (default_user_communication_id) REFERENCES user_communication (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE loans_tags (loan_request_id INT NOT NULL, restriction_id INT NOT NULL, INDEX IDX_154E392F52E591C3 (loan_request_id), INDEX IDX_154E392FE6160631 (restriction_id), PRIMARY KEY(loan_request_id, restriction_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE loans_tags ADD CONSTRAINT FK_154E392F52E591C3 FOREIGN KEY (loan_request_id) REFERENCES loan_request (id)');
        $this->addSql('ALTER TABLE loans_tags ADD CONSTRAINT FK_154E392FE6160631 FOREIGN KEY (restriction_id) REFERENCES restriction (id)');
        $this->addSql('DROP TABLE agreements_tags');
        $this->addSql('ALTER TABLE user_details DROP FOREIGN KEY FK_2A2B158059F5EDE4');
        $this->addSql('ALTER TABLE user_details ADD CONSTRAINT FK_2A2B158059F5EDE4 FOREIGN KEY (default_user_communication_id) REFERENCES user_communication (id) ON UPDATE CASCADE ON DELETE CASCADE');
    }
}
