<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\CallCreditCredentials;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170203150502 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("UPDATE callcredit_credentials set application_name='V57-API-TEST' WHERE type=" . CallCreditCredentials::CALLVALIDATE_TYPE);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("UPDATE callcredit_credentials set application_name='' WHERE type=" . CallCreditCredentials::CALLVALIDATE_TYPE);
    }
}
