<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161229161507 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE call_credit_api_calls (id INT AUTO_INCREMENT NOT NULL, quote_apply_id INT DEFAULT NULL, user_id INT DEFAULT NULL, request_date DATETIME DEFAULT NULL, response_date DATETIME DEFAULT NULL, request_xml LONGTEXT NOT NULL, response_xml LONGTEXT DEFAULT NULL, type INT NOT NULL, request_id VARCHAR(36) NOT NULL, status INT NOT NULL, UNIQUE INDEX UNIQ_3CF5A55FA7E6634F (quote_apply_id), INDEX IDX_3CF5A55FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE call_credit_api_calls ADD CONSTRAINT FK_3CF5A55FA7E6634F FOREIGN KEY (quote_apply_id) REFERENCES quote_apply (id)');
        $this->addSql('ALTER TABLE call_credit_api_calls ADD CONSTRAINT FK_3CF5A55FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE call_credit_api_calls');
    }
}
