<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161216155354 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE world_pay_transaction (id INT AUTO_INCREMENT NOT NULL, token VARCHAR(255) NOT NULL COMMENT \'A unique token which the WorldPay.js library added to your checkout form, or obtained via the token API. This token represents the customer\'\'s card details/payment method which was stored on our server. One of token or paymentMethod must be specified\', amount INT NOT NULL COMMENT \'The amount to be charged in pennies/cents (or whatever the smallest unit is of the currencyCode that you specified, see currencyCodeExponent below)\', currency_code VARCHAR(255) DEFAULT \'GBP\' NOT NULL, name VARCHAR(255) DEFAULT NULL COMMENT \'The name of the cardholder or payee\', order_description VARCHAR(255) NOT NULL COMMENT \'The description of the order provided by you\', customer_order_code VARCHAR(255) NOT NULL COMMENT \'The code or ID under which this order is known in your systems\', payment_status VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE world_pay_transaction');
    }
}
