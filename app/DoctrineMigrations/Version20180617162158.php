<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180617162158 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE instalment ADD installment_status SMALLINT NOT NULL, ADD outstanding NUMERIC(16, 8) DEFAULT NULL, ADD allocation NUMERIC(16, 8) DEFAULT NULL, ADD payment_amount NUMERIC(16, 8) DEFAULT NULL, CHANGE status payment_status SMALLINT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE instalment ADD status SMALLINT NOT NULL, DROP payment_status, DROP installment_status, DROP outstanding, DROP allocation, DROP payment_amount');
    }
}
