<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170103125755 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE declutter ADD need_to_close TINYINT(1) DEFAULT \'0\' NOT NULL, DROP total_repaid, DROP total_interest, DROP total_months_to_clear, DROP active, CHANGE loan_apply_id loan_apply_id INT NOT NULL, CHANGE user_creditline_id user_creditline_id INT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE declutter ADD total_repaid INT DEFAULT NULL, ADD total_interest INT DEFAULT NULL, ADD total_months_to_clear INT DEFAULT NULL, ADD active TINYINT(1) DEFAULT NULL, DROP need_to_close, CHANGE user_creditline_id user_creditline_id INT DEFAULT NULL, CHANGE loan_apply_id loan_apply_id INT DEFAULT NULL');
    }
}
