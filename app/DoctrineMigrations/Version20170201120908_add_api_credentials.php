<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\CallCreditCredentials;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170201120908_add_api_credentials extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $credentials = array(
            array(
                'type' => CallCreditCredentials::CALLREPORT_TYPE,
                'login' => 'Wealth Harbour CR API CTEST',
                'company' => 'Wealth Harbour CR CTEST',
                'password' => '8wJc29gbz',
                'url' => '',
            ),
            array(
                'type' => CallCreditCredentials::CALLVALIDATE_TYPE,
                'login' => 'api.ctest@wealthharbour.com',
                'company' => 'Wealth Harbour CTEST',
                'password' => '8wJc29gbz',
                'url' => '',
            ),
            array(
                'type' => CallCreditCredentials::AFFORDABILITY_TYPE,
                'login' => 'Wealth Harbour AR API CTEST',
                'company' => 'Wealth Harbour AR CTEST',
                'password' => '8wJc29gbz',
                'url' => '',
            )
        );

        foreach ($credentials as $credential) {
            $this->addSql('INSERT INTO callcredit_credentials (type, company, login, password, url, updated_at)
                                  VALUES (:type, :company, :login, :password, :url, NOW())', $credential);
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('TRUNCATE TABLE callcredit_credentials');
    }
}
