<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161228172636 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE event ADD active TINYINT(1) NOT NULL, ADD updated_at DATETIME DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT NULL, CHANGE status product_id INT DEFAULT NULL, CHANGE datetime_create created_at DATETIME DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE event ADD datetime_create DATETIME DEFAULT NULL, DROP active, DROP created_at, DROP updated_at, CHANGE type type INT DEFAULT NULL, CHANGE product_id status INT DEFAULT NULL');
    }
}
