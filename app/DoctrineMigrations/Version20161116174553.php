<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161116174553 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quote_income ADD is_homeowner TINYINT(1) DEFAULT NULL, ADD has_ccj TINYINT(1) DEFAULT NULL, ADD has_current_default TINYINT(1) DEFAULT NULL, ADD rate INT NOT NULL, CHANGE income_gross_ann total_gross_annual INT NOT NULL, CHANGE income_source main_source VARCHAR(36) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quote_income ADD income_gross_ann INT NOT NULL, DROP total_gross_annual, DROP is_homeowner, DROP has_ccj, DROP has_current_default, DROP rate, CHANGE main_source income_source VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
    }
}
