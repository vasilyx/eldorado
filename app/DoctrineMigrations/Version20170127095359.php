<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170127095359 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quote_apply_status_log DROP FOREIGN KEY FK_B37C4D4AA7E6634F');
        $this->addSql('DROP INDEX IDX_B37C4D4AA7E6634F ON quote_apply_status_log');
        $this->addSql('ALTER TABLE quote_apply_status_log ADD quote_apply INT NOT NULL, DROP quote_apply_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quote_apply_status_log ADD quote_apply_id INT DEFAULT NULL, DROP quote_apply');
        $this->addSql('ALTER TABLE quote_apply_status_log ADD CONSTRAINT FK_B37C4D4AA7E6634F FOREIGN KEY (quote_apply_id) REFERENCES quote_apply (id)');
        $this->addSql('CREATE INDEX IDX_B37C4D4AA7E6634F ON quote_apply_status_log (quote_apply_id)');
    }
}
