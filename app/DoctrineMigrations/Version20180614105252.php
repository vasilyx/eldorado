<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180614105252 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE file ADD invest_offer_id INT DEFAULT NULL, ADD agreement_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE file ADD CONSTRAINT FK_8C9F3610D5407B3 FOREIGN KEY (invest_offer_id) REFERENCES invest_offer (id)');
        $this->addSql('ALTER TABLE file ADD CONSTRAINT FK_8C9F361024890B2B FOREIGN KEY (agreement_id) REFERENCES agreement (id)');
        $this->addSql('CREATE INDEX IDX_8C9F3610D5407B3 ON file (invest_offer_id)');
        $this->addSql('CREATE INDEX IDX_8C9F361024890B2B ON file (agreement_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE file DROP FOREIGN KEY FK_8C9F3610D5407B3');
        $this->addSql('ALTER TABLE file DROP FOREIGN KEY FK_8C9F361024890B2B');
        $this->addSql('DROP INDEX IDX_8C9F3610D5407B3 ON file');
        $this->addSql('DROP INDEX IDX_8C9F361024890B2B ON file');
        $this->addSql('ALTER TABLE file DROP invest_offer_id, DROP agreement_id');
    }
}
