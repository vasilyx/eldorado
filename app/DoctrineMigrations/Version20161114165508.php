<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\Product;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161114165508 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $products = array(
            array(
                'name' => 'Borrower',
                'active' => true,
                'description' => 'Borrower',
                'type' => Product::TYPE_BORROWER),
            array(
                'name' => 'Investor',
                'active' => true,
                'description' => 'Investor',
                'type' => Product::TYPE_INVESTOR)
        );
        foreach ($products as $product) {
            $this->addSql('INSERT INTO product (name, active, description, type) VALUES (:name,:active,:description,:type)', $product);
        }

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('TRUNCATE TABLE product');

    }
}
