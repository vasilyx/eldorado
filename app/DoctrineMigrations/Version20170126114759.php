<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170126114759 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('RENAME TABLE loan_apply TO loan_request');
        $this->addSql('ALTER TABLE agreement CHANGE loan_apply_id loan_request_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contract_matching CHANGE loan_apply_id loan_request_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE declutter CHANGE loan_apply_id loan_request_id INT NOT NULL');
        $this->addSql('ALTER TABLE underwriting_result CHANGE loan_apply_id loan_request_id INT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('RENAME TABLE loan_request TO loan_apply');
        $this->addSql('ALTER TABLE agreement CHANGE loan_request_id loan_apply_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contract_matching CHANGE loan_request_id loan_apply_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE declutter CHANGE loan_request_id loan_apply_id INT NOT NULL');
        $this->addSql('ALTER TABLE underwriting_result CHANGE loan_request_id loan_apply_id INT DEFAULT NULL');
    }
}
