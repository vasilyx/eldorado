<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161118171826 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_address CHANGE country country VARCHAR(45) NOT NULL COMMENT \'postcodeanywhere CountryName\', CHANGE moved moved DATETIME NOT NULL, CHANGE city city VARCHAR(45) NOT NULL COMMENT \'postcodeanywhere City\', CHANGE street street VARCHAR(45) NOT NULL COMMENT \'postcodeanywhere Street\', CHANGE building_number building_number VARCHAR(45) NOT NULL COMMENT \'postcodeanywhere BuildingNumber\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_address CHANGE city city VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'postcodeanywhere City\', CHANGE street street VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'postcodeanywhere Street\', CHANGE building_number building_number VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'postcodeanywhere BuildingNumber\', CHANGE country country VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'postcodeanywhere CountryName\', CHANGE moved moved DATETIME DEFAULT NULL');
    }
}
