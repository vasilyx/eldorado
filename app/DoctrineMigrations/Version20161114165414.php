<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161114165414 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_session (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, datetime_create DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8849CBDE5F37A13B (token), INDEX IDX_8849CBDEA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(36) NOT NULL, active TINYINT(1) DEFAULT NULL, description VARCHAR(36) NOT NULL, type VARCHAR(255) NOT NULL, UNIQUE INDEX username_UNIQUE (type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE invest_offer (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, investment_type VARCHAR(36) DEFAULT NULL, finance_type VARCHAR(36) DEFAULT NULL, investment_amount INT DEFAULT NULL, investment_frequency VARCHAR(24) DEFAULT NULL, term INT DEFAULT NULL, prioritisation_tags VARCHAR(288) DEFAULT NULL, restriction_tags VARCHAR(288) DEFAULT NULL, active TINYINT(1) DEFAULT NULL, INDEX fk_invest_offer_1_idx (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE agreement (id INT AUTO_INCREMENT NOT NULL, loan_apply_id INT DEFAULT NULL, bonus_amt VARCHAR(45) DEFAULT NULL, platform_fixed_amt VARCHAR(45) DEFAULT NULL, platform_mgmt_amt VARCHAR(45) DEFAULT NULL, investor_interest_amount VARCHAR(45) DEFAULT NULL, loan_amount VARCHAR(45) DEFAULT NULL, total_repaid VARCHAR(45) DEFAULT NULL, monthly VARCHAR(45) DEFAULT NULL, bonus_rate VARCHAR(45) DEFAULT NULL, rate VARCHAR(45) DEFAULT NULL, apr VARCHAR(45) DEFAULT NULL, iInvestor_rate VARCHAR(45) DEFAULT NULL, status INT DEFAULT NULL, INDEX fk_agreement_1_idx (loan_apply_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_income (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, bank_sort_code INT NOT NULL, bank_account_number INT NOT NULL, main_income_gross_ann INT NOT NULL, main_income_net_ann INT NOT NULL, other_income_gross_ann INT NOT NULL, other_income_net_ann INT NOT NULL, other_income_source VARCHAR(256) DEFAULT NULL, spouse_income_gross_ann INT DEFAULT NULL, spouse_income_net_ann INT DEFAULT NULL, active TINYINT(1) DEFAULT NULL, INDEX fk_user_income_1_idx (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_communication_verification (id INT AUTO_INCREMENT NOT NULL, communication_id INT DEFAULT NULL, user_id INT DEFAULT NULL, value VARCHAR(256) DEFAULT NULL, code VARCHAR(256) DEFAULT NULL, datetime_create INT DEFAULT NULL, datetime_verified INT DEFAULT NULL, INDEX fk_user_verification_1_idx (user_id), INDEX fk_user_communication_verification_1_idx (communication_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE declutter (id INT AUTO_INCREMENT NOT NULL, user_creditline_id INT DEFAULT NULL, loan_apply_id INT DEFAULT NULL, total_repaid INT DEFAULT NULL, total_interest INT DEFAULT NULL, total_months_to_clear INT DEFAULT NULL, active TINYINT(1) DEFAULT NULL, INDEX fk_declutter_1_idx (user_creditline_id), INDEX fk_declutter_2_idx (loan_apply_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quote_income (id INT AUTO_INCREMENT NOT NULL, quote_apply_id INT DEFAULT NULL, income_gross_ann INT NOT NULL, income_source VARCHAR(36) NOT NULL, INDEX fk_quote_income_1_idx (quote_apply_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_marketing (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, device_id INT DEFAULT NULL, field1 TEXT DEFAULT NULL, field2 TEXT DEFAULT NULL, field3 TEXT DEFAULT NULL, field4 TEXT DEFAULT NULL, field5 TEXT DEFAULT NULL, field6 TEXT DEFAULT NULL, field7 TEXT DEFAULT NULL, field8 TEXT DEFAULT NULL, field9 TEXT DEFAULT NULL, field10 TEXT DEFAULT NULL, INDEX fk_users_marketing_1_idx (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_creditline (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, type VARCHAR(45) NOT NULL, name VARCHAR(45) NOT NULL, `limit` VARCHAR(45) NOT NULL, balance_input VARCHAR(45) NOT NULL, effective_balance_date DATETIME NOT NULL, apr VARCHAR(45) NOT NULL, monthly_payment DOUBLE PRECISION NOT NULL, active TINYINT(1) NOT NULL, refinance_event_id INT NOT NULL, balance INT NOT NULL, INDEX fk_user_creditlines_1_idx (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_employment (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, started DATETIME NOT NULL, ended DATETIME NOT NULL, company_name VARCHAR(256) NOT NULL, profession VARCHAR(256) NOT NULL, postal_code VARCHAR(256) NOT NULL, country VARCHAR(256) NOT NULL, qualification VARCHAR(256) NOT NULL, description VARCHAR(256) NOT NULL, active TINYINT(1) DEFAULT NULL, current TINYINT(1) DEFAULT NULL, work_status VARCHAR(256) NOT NULL, INDEX fk_user_employment_1_idx (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_household (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(45) NOT NULL, birth_date DATETIME DEFAULT NULL, active TINYINT(1) DEFAULT NULL, creditline_id INT DEFAULT NULL, INDEX fk_user_household_1_idx (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quote_apply (id INT AUTO_INCREMENT NOT NULL, user_products_id INT DEFAULT NULL, amount INT DEFAULT NULL, term INT DEFAULT NULL, income INT DEFAULT NULL, income_source TEXT DEFAULT NULL, ccj TINYINT(1) DEFAULT NULL, my_credit_score INT DEFAULT NULL, INDEX fk_quote_apply_1_idx (user_products_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_communication (id INT AUTO_INCREMENT NOT NULL, communication_id INT DEFAULT NULL, user_id INT DEFAULT NULL, value VARCHAR(256) DEFAULT NULL, is_verified INT DEFAULT NULL, INDEX fk_user_communications_1_idx (communication_id), INDEX fk_user_communications_2_idx (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(128) NOT NULL, password VARCHAR(128) NOT NULL, active TINYINT(1) DEFAULT NULL, datetime_registration DATETIME DEFAULT NULL, datetime_last_login DATETIME DEFAULT NULL, forgotten_password_token VARCHAR(64) DEFAULT NULL, confirm_email_token VARCHAR(64) DEFAULT NULL, partner_id INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX username_UNIQUE (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction (id INT AUTO_INCREMENT NOT NULL, event_id INT DEFAULT NULL, ccy VARCHAR(45) DEFAULT NULL, borrower_instalment DOUBLE PRECISION DEFAULT NULL, borrower_principle DOUBLE PRECISION DEFAULT NULL, borrower_interest DOUBLE PRECISION DEFAULT NULL, borrower_misc DOUBLE PRECISION DEFAULT NULL, investor_instalment DOUBLE PRECISION DEFAULT NULL, investor_principle DOUBLE PRECISION DEFAULT NULL, investor_interest DOUBLE PRECISION DEFAULT NULL, investor_misc DOUBLE PRECISION DEFAULT NULL, platform_fixed_fee DOUBLE PRECISION DEFAULT NULL, platform_mgmt_fee DOUBLE PRECISION DEFAULT NULL, platform_misc_fee DOUBLE PRECISION DEFAULT NULL, trust_first_loss VARCHAR(45) DEFAULT NULL, contract_id INT DEFAULT NULL, active TINYINT(1) DEFAULT NULL, field1 VARCHAR(45) DEFAULT NULL, field2 VARCHAR(45) DEFAULT NULL, field3 VARCHAR(45) DEFAULT NULL, INDEX fk_transactions_1_idx (event_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_device (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, user_marketing_id INT DEFAULT NULL, INDEX fk_user_devices_1_idx (user_id), INDEX fk_user_devices_2_idx (user_marketing_id), UNIQUE INDEX index4 (user_id, user_marketing_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE agreement_performance (id INT AUTO_INCREMENT NOT NULL, agreement_id INT DEFAULT NULL, performance VARCHAR(36) DEFAULT NULL, credit_risk_indicator DOUBLE PRECISION DEFAULT NULL, payment_risk_indicator DOUBLE PRECISION DEFAULT NULL, INDEX fk_agreement_performance_1_idx (agreement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_asset (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, home INT NOT NULL, shares INT NOT NULL, cash INT NOT NULL, isa INT NOT NULL, sipp INT NOT NULL, investable INT NOT NULL, property INT NOT NULL, INDEX fk_user_assets_1_idx (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_role (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, role_id INT NOT NULL, INDEX IDX_2DE8C6A3A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_address (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, postal_code VARCHAR(45) NOT NULL, address_number INT DEFAULT NULL, address1 VARCHAR(45) DEFAULT NULL, address2 VARCHAR(45) DEFAULT NULL, address3 VARCHAR(45) DEFAULT NULL, country VARCHAR(45) DEFAULT NULL, region VARCHAR(45) DEFAULT NULL, state VARCHAR(45) DEFAULT NULL, moved DATETIME DEFAULT NULL, active TINYINT(1) DEFAULT NULL, current TINYINT(1) DEFAULT NULL, INDEX fk_user_addresses_1_idx (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE loan_apply (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, request_id VARCHAR(45) DEFAULT NULL, summ VARCHAR(45) DEFAULT NULL, monthly VARCHAR(45) DEFAULT NULL, INDEX fk_loan_apply_1_idx (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_product (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, product_id INT DEFAULT NULL, INDEX fk_user_products_2_idx (product_id), INDEX fk_user_products_1 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE invest_offer_sub (id INT AUTO_INCREMENT NOT NULL, invest_offer_id INT DEFAULT NULL, amount DOUBLE PRECISION DEFAULT NULL, active TINYINT(1) DEFAULT NULL, INDEX fk_invest_offer_sub_1_idx (invest_offer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quote_result (id INT AUTO_INCREMENT NOT NULL, quote_apply_id INT DEFAULT NULL, quote_bonus_amt INT NOT NULL, platform_fixed_amt INT NOT NULL, platform_mgmt_amt INT NOT NULL, bonus_rate DOUBLE PRECISION NOT NULL, rate DOUBLE PRECISION NOT NULL, investor_rate DOUBLE PRECISION NOT NULL, apr DOUBLE PRECISION NOT NULL, monthly INT NOT NULL, total_repaid INT NOT NULL, score1 INT NOT NULL, score2 INT NOT NULL, score3 INT NOT NULL, INDEX fk_quote_result_1_idx (quote_apply_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE communication (id INT AUTO_INCREMENT NOT NULL, value VARCHAR(45) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_dependent (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, household_id INT DEFAULT NULL, number INT NOT NULL, expense_total INT NOT NULL, utilities INT NOT NULL, rent INT NOT NULL, travel INT NOT NULL, food INT NOT NULL, saving INT NOT NULL, entertaiment INT NOT NULL, INDEX fk_user_dependents_1_idx (user_id), INDEX fk_user_dependents_2_idx (household_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quote_term (id INT AUTO_INCREMENT NOT NULL, quote_apply_id INT DEFAULT NULL, amount INT NOT NULL, term INT NOT NULL, INDEX fk_quote_term_1_idx (quote_apply_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_details (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, default_user_communication_id INT DEFAULT NULL, first_name VARCHAR(256) DEFAULT NULL, middle_name VARCHAR(256) DEFAULT NULL, last_name VARCHAR(256) DEFAULT NULL, maiden_name VARCHAR(256) DEFAULT NULL, gender INT DEFAULT NULL, birth_date DATETIME DEFAULT NULL, title VARCHAR(45) DEFAULT NULL, screen_name VARCHAR(45) DEFAULT NULL, martial_status INT DEFAULT NULL, UNIQUE INDEX UNIQ_2A2B1580A76ED395 (user_id), INDEX fk_user_details_1_idx (user_id), INDEX fk_user_details_2_idx (default_user_communication_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, admin_id INT DEFAULT NULL, type INT DEFAULT NULL, status INT DEFAULT NULL, datetime_create DATETIME DEFAULT NULL, INDEX fk_events_1_idx (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contract_matching (id INT AUTO_INCREMENT NOT NULL, loan_apply_id INT DEFAULT NULL, invest_offer_id INT DEFAULT NULL, invest_offer_sub_id INT DEFAULT NULL, agreement_id INT DEFAULT NULL, contract_amount DOUBLE PRECISION DEFAULT NULL, active TINYINT(1) DEFAULT NULL, status INT DEFAULT NULL, INDEX fk_contract_matching_1_idx (loan_apply_id), INDEX fk_contract_matching_2_idx (invest_offer_id), INDEX fk_contract_matching_3_idx (invest_offer_sub_id), INDEX fk_contract_matching_4_idx (agreement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_session ADD CONSTRAINT FK_8849CBDEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE invest_offer ADD CONSTRAINT FK_9FBB4B6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE agreement ADD CONSTRAINT FK_2E655A241A9A3175 FOREIGN KEY (loan_apply_id) REFERENCES loan_apply (id)');
        $this->addSql('ALTER TABLE user_income ADD CONSTRAINT FK_5AFA79EDA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_communication_verification ADD CONSTRAINT FK_C80481BD1C2D1E0C FOREIGN KEY (communication_id) REFERENCES communication (id)');
        $this->addSql('ALTER TABLE user_communication_verification ADD CONSTRAINT FK_C80481BDA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE declutter ADD CONSTRAINT FK_9AF3E48A6BF0DEF0 FOREIGN KEY (user_creditline_id) REFERENCES user_creditline (id)');
        $this->addSql('ALTER TABLE declutter ADD CONSTRAINT FK_9AF3E48A1A9A3175 FOREIGN KEY (loan_apply_id) REFERENCES loan_apply (id)');
        $this->addSql('ALTER TABLE quote_income ADD CONSTRAINT FK_510CC93A7E6634F FOREIGN KEY (quote_apply_id) REFERENCES quote_apply (id)');
        $this->addSql('ALTER TABLE user_marketing ADD CONSTRAINT FK_9461EC8CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_creditline ADD CONSTRAINT FK_94634FE4A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_employment ADD CONSTRAINT FK_186F5A74A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_household ADD CONSTRAINT FK_5423A224A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE quote_apply ADD CONSTRAINT FK_B0D0017EC591483C FOREIGN KEY (user_products_id) REFERENCES user_product (id)');
        $this->addSql('ALTER TABLE user_communication ADD CONSTRAINT FK_172118BB1C2D1E0C FOREIGN KEY (communication_id) REFERENCES communication (id)');
        $this->addSql('ALTER TABLE user_communication ADD CONSTRAINT FK_172118BBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D171F7E88B FOREIGN KEY (event_id) REFERENCES event (id)');
        $this->addSql('ALTER TABLE user_device ADD CONSTRAINT FK_6C7DADB3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_device ADD CONSTRAINT FK_6C7DADB34610CA6E FOREIGN KEY (user_marketing_id) REFERENCES user_marketing (id)');
        $this->addSql('ALTER TABLE agreement_performance ADD CONSTRAINT FK_20B1D75D24890B2B FOREIGN KEY (agreement_id) REFERENCES agreement (id)');
        $this->addSql('ALTER TABLE user_asset ADD CONSTRAINT FK_E06DA104A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_address ADD CONSTRAINT FK_5543718BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE loan_apply ADD CONSTRAINT FK_EA969440A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_product ADD CONSTRAINT FK_8B471AA7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_product ADD CONSTRAINT FK_8B471AA74584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE invest_offer_sub ADD CONSTRAINT FK_61B35C7ED5407B3 FOREIGN KEY (invest_offer_id) REFERENCES invest_offer (id)');
        $this->addSql('ALTER TABLE quote_result ADD CONSTRAINT FK_29D26F50A7E6634F FOREIGN KEY (quote_apply_id) REFERENCES quote_apply (id)');
        $this->addSql('ALTER TABLE user_dependent ADD CONSTRAINT FK_BB70FA40A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_dependent ADD CONSTRAINT FK_BB70FA40E79FF843 FOREIGN KEY (household_id) REFERENCES user_household (id)');
        $this->addSql('ALTER TABLE quote_term ADD CONSTRAINT FK_37E8B807A7E6634F FOREIGN KEY (quote_apply_id) REFERENCES quote_apply (id)');
        $this->addSql('ALTER TABLE user_details ADD CONSTRAINT FK_2A2B1580A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_details ADD CONSTRAINT FK_2A2B158059F5EDE4 FOREIGN KEY (default_user_communication_id) REFERENCES user_communication (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE contract_matching ADD CONSTRAINT FK_49A7E9761A9A3175 FOREIGN KEY (loan_apply_id) REFERENCES loan_apply (id)');
        $this->addSql('ALTER TABLE contract_matching ADD CONSTRAINT FK_49A7E976D5407B3 FOREIGN KEY (invest_offer_id) REFERENCES invest_offer (id)');
        $this->addSql('ALTER TABLE contract_matching ADD CONSTRAINT FK_49A7E9767C487065 FOREIGN KEY (invest_offer_sub_id) REFERENCES invest_offer_sub (id)');
        $this->addSql('ALTER TABLE contract_matching ADD CONSTRAINT FK_49A7E97624890B2B FOREIGN KEY (agreement_id) REFERENCES agreement (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_product DROP FOREIGN KEY FK_8B471AA74584665A');
        $this->addSql('ALTER TABLE invest_offer_sub DROP FOREIGN KEY FK_61B35C7ED5407B3');
        $this->addSql('ALTER TABLE contract_matching DROP FOREIGN KEY FK_49A7E976D5407B3');
        $this->addSql('ALTER TABLE agreement_performance DROP FOREIGN KEY FK_20B1D75D24890B2B');
        $this->addSql('ALTER TABLE contract_matching DROP FOREIGN KEY FK_49A7E97624890B2B');
        $this->addSql('ALTER TABLE user_device DROP FOREIGN KEY FK_6C7DADB34610CA6E');
        $this->addSql('ALTER TABLE declutter DROP FOREIGN KEY FK_9AF3E48A6BF0DEF0');
        $this->addSql('ALTER TABLE user_dependent DROP FOREIGN KEY FK_BB70FA40E79FF843');
        $this->addSql('ALTER TABLE quote_income DROP FOREIGN KEY FK_510CC93A7E6634F');
        $this->addSql('ALTER TABLE quote_result DROP FOREIGN KEY FK_29D26F50A7E6634F');
        $this->addSql('ALTER TABLE quote_term DROP FOREIGN KEY FK_37E8B807A7E6634F');
        $this->addSql('ALTER TABLE user_details DROP FOREIGN KEY FK_2A2B158059F5EDE4');
        $this->addSql('ALTER TABLE user_session DROP FOREIGN KEY FK_8849CBDEA76ED395');
        $this->addSql('ALTER TABLE invest_offer DROP FOREIGN KEY FK_9FBB4B6A76ED395');
        $this->addSql('ALTER TABLE user_income DROP FOREIGN KEY FK_5AFA79EDA76ED395');
        $this->addSql('ALTER TABLE user_communication_verification DROP FOREIGN KEY FK_C80481BDA76ED395');
        $this->addSql('ALTER TABLE user_marketing DROP FOREIGN KEY FK_9461EC8CA76ED395');
        $this->addSql('ALTER TABLE user_creditline DROP FOREIGN KEY FK_94634FE4A76ED395');
        $this->addSql('ALTER TABLE user_employment DROP FOREIGN KEY FK_186F5A74A76ED395');
        $this->addSql('ALTER TABLE user_household DROP FOREIGN KEY FK_5423A224A76ED395');
        $this->addSql('ALTER TABLE user_communication DROP FOREIGN KEY FK_172118BBA76ED395');
        $this->addSql('ALTER TABLE user_device DROP FOREIGN KEY FK_6C7DADB3A76ED395');
        $this->addSql('ALTER TABLE user_asset DROP FOREIGN KEY FK_E06DA104A76ED395');
        $this->addSql('ALTER TABLE user_role DROP FOREIGN KEY FK_2DE8C6A3A76ED395');
        $this->addSql('ALTER TABLE user_address DROP FOREIGN KEY FK_5543718BA76ED395');
        $this->addSql('ALTER TABLE loan_apply DROP FOREIGN KEY FK_EA969440A76ED395');
        $this->addSql('ALTER TABLE user_product DROP FOREIGN KEY FK_8B471AA7A76ED395');
        $this->addSql('ALTER TABLE user_dependent DROP FOREIGN KEY FK_BB70FA40A76ED395');
        $this->addSql('ALTER TABLE user_details DROP FOREIGN KEY FK_2A2B1580A76ED395');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA7A76ED395');
        $this->addSql('ALTER TABLE agreement DROP FOREIGN KEY FK_2E655A241A9A3175');
        $this->addSql('ALTER TABLE declutter DROP FOREIGN KEY FK_9AF3E48A1A9A3175');
        $this->addSql('ALTER TABLE contract_matching DROP FOREIGN KEY FK_49A7E9761A9A3175');
        $this->addSql('ALTER TABLE quote_apply DROP FOREIGN KEY FK_B0D0017EC591483C');
        $this->addSql('ALTER TABLE contract_matching DROP FOREIGN KEY FK_49A7E9767C487065');
        $this->addSql('ALTER TABLE user_communication_verification DROP FOREIGN KEY FK_C80481BD1C2D1E0C');
        $this->addSql('ALTER TABLE user_communication DROP FOREIGN KEY FK_172118BB1C2D1E0C');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D171F7E88B');
        $this->addSql('DROP TABLE user_session');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE invest_offer');
        $this->addSql('DROP TABLE agreement');
        $this->addSql('DROP TABLE user_income');
        $this->addSql('DROP TABLE user_communication_verification');
        $this->addSql('DROP TABLE declutter');
        $this->addSql('DROP TABLE quote_income');
        $this->addSql('DROP TABLE user_marketing');
        $this->addSql('DROP TABLE user_creditline');
        $this->addSql('DROP TABLE user_employment');
        $this->addSql('DROP TABLE user_household');
        $this->addSql('DROP TABLE quote_apply');
        $this->addSql('DROP TABLE user_communication');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE transaction');
        $this->addSql('DROP TABLE user_device');
        $this->addSql('DROP TABLE agreement_performance');
        $this->addSql('DROP TABLE user_asset');
        $this->addSql('DROP TABLE user_role');
        $this->addSql('DROP TABLE user_address');
        $this->addSql('DROP TABLE loan_apply');
        $this->addSql('DROP TABLE user_product');
        $this->addSql('DROP TABLE invest_offer_sub');
        $this->addSql('DROP TABLE quote_result');
        $this->addSql('DROP TABLE communication');
        $this->addSql('DROP TABLE user_dependent');
        $this->addSql('DROP TABLE quote_term');
        $this->addSql('DROP TABLE user_details');
        $this->addSql('DROP TABLE event');
        $this->addSql('DROP TABLE contract_matching');
    }
}
