<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161128102256 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_income CHANGE main_income_gross_ann main_income_gross_ann INT DEFAULT NULL, CHANGE main_income_net_ann main_income_net_ann INT DEFAULT NULL, CHANGE other_income_net_ann other_income_net_ann INT DEFAULT NULL, CHANGE other_income_gross_ann other_income_gross_ann INT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_income CHANGE main_income_gross_ann main_income_gross_ann INT NOT NULL, CHANGE main_income_net_ann main_income_net_ann INT NOT NULL, CHANGE other_income_net_ann other_income_net_ann INT NOT NULL, CHANGE other_income_gross_ann other_income_gross_ann INT NOT NULL');
    }
}
