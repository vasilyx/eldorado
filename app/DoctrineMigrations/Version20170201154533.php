<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\Product;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170201154533 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $result = $this->connection->fetchAll('SELECT * FROM product');
        foreach ($result as $row) {
            if ("" === $row['code']) {
                $this->addSql('UPDATE product SET code = :type WHERE id=:id ', [
                    'type' => $row['type'],
                    'id'    => $row['id']
                ]);
            }

        }
        foreach (Product::getProductShortNamesMap() as $key => $shortName) {
            if (!in_array($key, array_column($result, 'code'))) {
                $this->addSql('INSERT INTO product (name, active, description, type, code) VALUES (:name, :active, :description, :type, :code)', [
                        'name' => $shortName,
                        'active' => 1,
                        'description' => Product::getProductNamesMap()[$key],
                        'type' => Product::getProductTypesMap()[$key],
                        'code' => $key]
                );
            }
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
