<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180523110131_admin_decision extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE whadmin_admin_decision (id INT AUTO_INCREMENT NOT NULL, admin_id INT DEFAULT NULL, call_credit_api_call_id INT DEFAULT NULL, decision INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_ACDC015D642B8210 (admin_id), INDEX IDX_ACDC015D15F1F2DD (call_credit_api_call_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE whadmin_admin_decision ADD CONSTRAINT FK_ACDC015D642B8210 FOREIGN KEY (admin_id) REFERENCES whadmin_user (id)');
        $this->addSql('ALTER TABLE whadmin_admin_decision ADD CONSTRAINT FK_ACDC015D15F1F2DD FOREIGN KEY (call_credit_api_call_id) REFERENCES call_credit_api_calls (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE whadmin_admin_decision');
    }
}
