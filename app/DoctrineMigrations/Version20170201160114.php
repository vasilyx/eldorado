<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170201160114 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE invest_offers_restrictions (invest_offer_id INT NOT NULL, restriction_id INT NOT NULL, INDEX IDX_8CAD4484D5407B3 (invest_offer_id), INDEX IDX_8CAD4484E6160631 (restriction_id), PRIMARY KEY(invest_offer_id, restriction_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE invest_offers_priorities (invest_offer_id INT NOT NULL, restriction_id INT NOT NULL, INDEX IDX_C99AA418D5407B3 (invest_offer_id), INDEX IDX_C99AA418E6160631 (restriction_id), PRIMARY KEY(invest_offer_id, restriction_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE invest_offers_restrictions ADD CONSTRAINT FK_8CAD4484D5407B3 FOREIGN KEY (invest_offer_id) REFERENCES invest_offer (id)');
        $this->addSql('ALTER TABLE invest_offers_restrictions ADD CONSTRAINT FK_8CAD4484E6160631 FOREIGN KEY (restriction_id) REFERENCES restriction (id)');
        $this->addSql('ALTER TABLE invest_offers_priorities ADD CONSTRAINT FK_C99AA418D5407B3 FOREIGN KEY (invest_offer_id) REFERENCES invest_offer (id)');
        $this->addSql('ALTER TABLE invest_offers_priorities ADD CONSTRAINT FK_C99AA418E6160631 FOREIGN KEY (restriction_id) REFERENCES restriction (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE invest_offers_restrictions');
        $this->addSql('DROP TABLE invest_offers_priorities');
    }
}
