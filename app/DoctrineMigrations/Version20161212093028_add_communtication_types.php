<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\Communication;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161212093028_add_communtication_types extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $result = $this->connection->fetchAll('SELECT type FROM communication');
        foreach (Communication::getCommunicationTypes() as $key => $value) {
            if (!in_array($key, array_column($result, 'type'))) {
                $this->addSql('INSERT INTO communication (type, value) VALUES (:type, :value)', [
                        'type' => $key,
                        'value' => $value]
                );
            }
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
