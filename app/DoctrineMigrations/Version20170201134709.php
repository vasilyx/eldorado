<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170201134709 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quote_apply ADD user_id INT DEFAULT NULL, ADD product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE quote_apply ADD CONSTRAINT FK_B0D0017EA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE quote_apply ADD CONSTRAINT FK_B0D0017E4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_B0D0017EA76ED395 ON quote_apply (user_id)');
        $this->addSql('CREATE INDEX IDX_B0D0017E4584665A ON quote_apply (product_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quote_apply DROP FOREIGN KEY FK_B0D0017EA76ED395');
        $this->addSql('ALTER TABLE quote_apply DROP FOREIGN KEY FK_B0D0017E4584665A');
        $this->addSql('DROP INDEX IDX_B0D0017EA76ED395 ON quote_apply');
        $this->addSql('DROP INDEX IDX_B0D0017E4584665A ON quote_apply');
        $this->addSql('ALTER TABLE quote_apply DROP user_id, DROP product_id');
    }
}
