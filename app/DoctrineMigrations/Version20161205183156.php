<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161205183156 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_employment ADD occ_month_started DATETIME NOT NULL, ADD occ_month_ended DATETIME DEFAULT NULL, ADD business_firm_name VARCHAR(144) NOT NULL, ADD industry VARCHAR(144) NOT NULL, ADD occ_postal_code VARCHAR(256) NOT NULL, ADD occ_country VARCHAR(36) NOT NULL, ADD job_description VARCHAR(144) NOT NULL, ADD prof_qual VARCHAR(144) NOT NULL, ADD active_status TINYINT(1) NOT NULL, ADD current_status TINYINT(1) NOT NULL, ADD archive TINYINT(1) NOT NULL, DROP started, DROP ended, DROP company_name, DROP profession, DROP postal_code, DROP country, DROP qualification, DROP description, DROP active, DROP current, CHANGE work_status work_status VARCHAR(36) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_employment ADD ended DATETIME NOT NULL, ADD profession VARCHAR(256) NOT NULL COLLATE utf8_unicode_ci, ADD postal_code VARCHAR(256) NOT NULL COLLATE utf8_unicode_ci, ADD country VARCHAR(256) NOT NULL COLLATE utf8_unicode_ci, ADD qualification VARCHAR(256) NOT NULL COLLATE utf8_unicode_ci, ADD description VARCHAR(256) NOT NULL COLLATE utf8_unicode_ci, ADD active TINYINT(1) DEFAULT NULL, ADD current TINYINT(1) DEFAULT NULL, DROP occ_month_ended, DROP business_firm_name, DROP industry, DROP occ_country, DROP job_description, DROP prof_qual, DROP active_status, DROP current_status, DROP archive, CHANGE work_status work_status VARCHAR(256) NOT NULL COLLATE utf8_unicode_ci, CHANGE occ_month_started started DATETIME NOT NULL, CHANGE occ_postal_code company_name VARCHAR(256) NOT NULL COLLATE utf8_unicode_ci');
    }
}
