<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170105141751 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE underwriting_result (id INT AUTO_INCREMENT NOT NULL, quote_apply_id INT DEFAULT NULL, loan_apply_id INT DEFAULT NULL, type INT NOT NULL, bonus_amount NUMERIC(10, 4) NOT NULL, fixed_amount NUMERIC(10, 4) NOT NULL, management_amount NUMERIC(10, 4) NOT NULL, investor_interest_amount NUMERIC(10, 4) NOT NULL, loan_amount NUMERIC(10, 4) NOT NULL, total_repaid NUMERIC(10, 4) NOT NULL, monthly NUMERIC(10, 4) NOT NULL, bonus_rate NUMERIC(10, 4) NOT NULL, rate NUMERIC(10, 4) NOT NULL, apr NUMERIC(10, 4) NOT NULL, investor_rate NUMERIC(10, 4) NOT NULL, score1 INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_35C7F635A7E6634F (quote_apply_id), INDEX IDX_35C7F6351A9A3175 (loan_apply_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE underwriting_result ADD CONSTRAINT FK_35C7F635A7E6634F FOREIGN KEY (quote_apply_id) REFERENCES quote_apply (id)');
        $this->addSql('ALTER TABLE underwriting_result ADD CONSTRAINT FK_35C7F6351A9A3175 FOREIGN KEY (loan_apply_id) REFERENCES loan_apply (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE underwriting_result');
    }
}
