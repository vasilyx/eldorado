<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161201135658 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE invest_offer ADD confirm_invest TINYINT(1) NOT NULL, ADD risk_confirm TINYINT(1) NOT NULL, ADD accept_terms TINYINT(1) NOT NULL, DROP prioritisation_tags, DROP restriction_tags');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE invest_offer ADD prioritisation_tags VARCHAR(288) NOT NULL COLLATE utf8_unicode_ci, ADD restriction_tags VARCHAR(288) NOT NULL COLLATE utf8_unicode_ci, DROP confirm_invest, DROP risk_confirm, DROP accept_terms');
    }
}
