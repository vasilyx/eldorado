<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180523000734 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE loan_apply RENAME INDEX fk_loan_apply_1_idx TO fk_loan_request_1_idx');
        $this->addSql('ALTER TABLE underwriting_result DROP FOREIGN KEY FK_35C7F6351A9A3175');
        $this->addSql('DROP INDEX IDX_35C7F6351A9A3175 ON underwriting_result');
        $this->addSql('ALTER TABLE underwriting_result CHANGE loan_request_id loan_apply_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE underwriting_result ADD CONSTRAINT FK_35C7F6351A9A3175 FOREIGN KEY (loan_apply_id) REFERENCES loan_apply (id)');
        $this->addSql('CREATE INDEX IDX_35C7F6351A9A3175 ON underwriting_result (loan_apply_id)');
        $this->addSql('ALTER TABLE agreement DROP FOREIGN KEY FK_2E655A241A9A3175');
        $this->addSql('DROP INDEX fk_agreement_1_idx ON agreement');
        $this->addSql('ALTER TABLE agreement CHANGE loan_request_id loan_apply_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE agreement ADD CONSTRAINT FK_2E655A241A9A3175 FOREIGN KEY (loan_apply_id) REFERENCES loan_apply (id)');
        $this->addSql('CREATE INDEX fk_agreement_1_idx ON agreement (loan_apply_id)');
        $this->addSql('ALTER TABLE declutter DROP FOREIGN KEY FK_9AF3E48A1A9A3175');
        $this->addSql('DROP INDEX fk_declutter_2_idx ON declutter');
        $this->addSql('ALTER TABLE declutter CHANGE loan_request_id loan_apply_id INT NOT NULL');
        $this->addSql('ALTER TABLE declutter ADD CONSTRAINT FK_9AF3E48A1A9A3175 FOREIGN KEY (loan_apply_id) REFERENCES loan_apply (id)');
        $this->addSql('CREATE INDEX fk_declutter_2_idx ON declutter (loan_apply_id)');
        $this->addSql('ALTER TABLE contract_matching DROP FOREIGN KEY FK_49A7E9761A9A3175');
        $this->addSql('DROP INDEX fk_contract_matching_1_idx ON contract_matching');
        $this->addSql('ALTER TABLE contract_matching CHANGE loan_request_id loan_apply_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contract_matching ADD CONSTRAINT FK_49A7E9761A9A3175 FOREIGN KEY (loan_apply_id) REFERENCES loan_apply (id)');
        $this->addSql('CREATE INDEX fk_contract_matching_1_idx ON contract_matching (loan_apply_id)');
        $this->addSql('ALTER TABLE loan_apply_status_log DROP FOREIGN KEY FK_42495BAC52E591C3');
        $this->addSql('DROP INDEX IDX_42495BAC52E591C3 ON loan_apply_status_log');
        $this->addSql('ALTER TABLE loan_apply_status_log CHANGE loan_request_id loan_apply_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE loan_apply_status_log ADD CONSTRAINT FK_878CFB7E1A9A3175 FOREIGN KEY (loan_apply_id) REFERENCES loan_apply (id)');
        $this->addSql('CREATE INDEX IDX_878CFB7E1A9A3175 ON loan_apply_status_log (loan_apply_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agreement DROP FOREIGN KEY FK_2E655A241A9A3175');
        $this->addSql('DROP INDEX fk_agreement_1_idx ON agreement');
        $this->addSql('ALTER TABLE agreement CHANGE loan_apply_id loan_request_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE agreement ADD CONSTRAINT FK_2E655A241A9A3175 FOREIGN KEY (loan_request_id) REFERENCES loan_apply (id)');
        $this->addSql('CREATE INDEX fk_agreement_1_idx ON agreement (loan_request_id)');
        $this->addSql('ALTER TABLE contract_matching DROP FOREIGN KEY FK_49A7E9761A9A3175');
        $this->addSql('DROP INDEX fk_contract_matching_1_idx ON contract_matching');
        $this->addSql('ALTER TABLE contract_matching CHANGE loan_apply_id loan_request_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contract_matching ADD CONSTRAINT FK_49A7E9761A9A3175 FOREIGN KEY (loan_request_id) REFERENCES loan_apply (id)');
        $this->addSql('CREATE INDEX fk_contract_matching_1_idx ON contract_matching (loan_request_id)');
        $this->addSql('ALTER TABLE declutter DROP FOREIGN KEY FK_9AF3E48A1A9A3175');
        $this->addSql('DROP INDEX fk_declutter_2_idx ON declutter');
        $this->addSql('ALTER TABLE declutter CHANGE loan_apply_id loan_request_id INT NOT NULL');
        $this->addSql('ALTER TABLE declutter ADD CONSTRAINT FK_9AF3E48A1A9A3175 FOREIGN KEY (loan_request_id) REFERENCES loan_apply (id)');
        $this->addSql('CREATE INDEX fk_declutter_2_idx ON declutter (loan_request_id)');
        $this->addSql('ALTER TABLE loan_apply RENAME INDEX fk_loan_request_1_idx TO fk_loan_apply_1_idx');
        $this->addSql('ALTER TABLE loan_apply_status_log DROP FOREIGN KEY FK_878CFB7E1A9A3175');
        $this->addSql('DROP INDEX IDX_878CFB7E1A9A3175 ON loan_apply_status_log');
        $this->addSql('ALTER TABLE loan_apply_status_log CHANGE loan_apply_id loan_request_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE loan_apply_status_log ADD CONSTRAINT FK_42495BAC52E591C3 FOREIGN KEY (loan_request_id) REFERENCES loan_apply (id)');
        $this->addSql('CREATE INDEX IDX_42495BAC52E591C3 ON loan_apply_status_log (loan_request_id)');
        $this->addSql('ALTER TABLE underwriting_result DROP FOREIGN KEY FK_35C7F6351A9A3175');
        $this->addSql('DROP INDEX IDX_35C7F6351A9A3175 ON underwriting_result');
        $this->addSql('ALTER TABLE underwriting_result CHANGE loan_apply_id loan_request_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE underwriting_result ADD CONSTRAINT FK_35C7F6351A9A3175 FOREIGN KEY (loan_request_id) REFERENCES loan_apply (id)');
        $this->addSql('CREATE INDEX IDX_35C7F6351A9A3175 ON underwriting_result (loan_request_id)');
    }
}
