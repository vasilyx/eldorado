<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\Communication;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161124171035 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {;

        foreach (Communication::getCommunicationTypes() as $key => $value) {
            $this->addSql('INSERT INTO communication (type, value) VALUES (:type, :value)', [
                'type' => $key,
                'value' => $value]
            );
        }

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        foreach (Communication::getCommunicationTypes() as $key => $value) {
            $this->addSql('DELETE FROM communication WHERE type = :type', [
                    'type' => $key]
            );
        }
    }
}
