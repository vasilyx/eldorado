<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161122153522 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quote_apply DROP amount, DROP term, DROP income, DROP income_source, DROP ccj, DROP my_credit_score');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quote_apply ADD amount INT DEFAULT NULL, ADD term INT DEFAULT NULL, ADD income INT DEFAULT NULL, ADD income_source TEXT DEFAULT NULL COLLATE utf8_unicode_ci, ADD ccj TINYINT(1) DEFAULT NULL, ADD my_credit_score INT DEFAULT NULL');
    }
}
