<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161226104904 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE world_pay_transaction ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE world_pay_transaction ADD CONSTRAINT FK_9F13C3F7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_9F13C3F7A76ED395 ON world_pay_transaction (user_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE world_pay_transaction DROP FOREIGN KEY FK_9F13C3F7A76ED395');
        $this->addSql('DROP INDEX IDX_9F13C3F7A76ED395 ON world_pay_transaction');
        $this->addSql('ALTER TABLE world_pay_transaction DROP user_id');
    }
}
