import { EldoradoPage } from './app.po';

describe('eldorado App', function() {
  let page: EldoradoPage;

  beforeEach(() => {
    page = new EldoradoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
