import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { StorageService} from '../service/storage.service';
import { Router} from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private storageService: StorageService,
    private router: Router
  ) {}

  canActivate() {
    if (!this.storageService.read('token')) {
      return false;
    }
    return true;
  }
}
