import {environment} from '../environments/environment';

export const DEV_PREFIX: string = environment.url;
export const PATTERN_EMAIL: RegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export const PATTERN_PASSWORD: RegExp = /^(?=.*[A-Z])(?=.*\d)[A-Za-z\d!@#$%^&*()-_]{8,}/;
export const PATTERN_ADMIN_PASSWORD: RegExp = /^(?=.*[A-Z])(?=.*\d)[A-Za-z\d!@#$%^&*()-_]{12,}/;
export const PATTERN_POSTCODE: RegExp = /^([A-PR-UWYZ0-9][A-HK-Y0-9][ABCDEFGHJKSTUW0-9]?[ABEHMNPRVWXY0-9]? ?[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/i;
export const PATTERN_STRING: RegExp = /^[A-Z\s\.\-\']+$/i;
export const DATE_FORMAT_SERVER: String = 'YYYYMMDD';
export const DATE_FORMAT_CLIENT: String = 'DD/MM/YYYY';
export const DATE_FORMAT_MASK: Array<any> = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
export const CONFIRMATION_URL: string = '/#/login';
export const FORGOT_PASSWORD_URL: string = '/#/reset';
export const ADMIN_FORGOT_PASSWORD_URL: string = '/#/whadmin/reset';
export const PAYMENT_REDIRECT_URL: string = '/#/portal/transfer_money_result';
export const PORTAL_PREFIX_PATH: string = '/portal';
export const ADMIN_PREFIX_PATH: string = '/whadmin';
export const API = {
  CHECK: DEV_PREFIX + '/api/auth/check.json',
  LOGIN: DEV_PREFIX + '/api/auth/login.json',
  LOGOUT: DEV_PREFIX + '/api/auth/logout.json',
  FORGOT_PASSWORD: DEV_PREFIX + '/api/auth/forgotten.json',
  RESET_PASSWORD: DEV_PREFIX + '/api/auth/reset_password.json',
  EMAIL_CONFIRMATION: DEV_PREFIX + '/api/auth/email_confirmation.json',
  BORROWER_REGISTRATION: DEV_PREFIX + '/api/user/registration/borrower.json',
  BORROWER_LOAN_AGREEMENT: DEV_PREFIX + '/api/file/{quote_id}/acceptance_agreement.json',
  BORROWER_CONFIRM_LOAN_AGRREMENT: DEV_PREFIX + '/api/user/confirmLoanAgreement.json',
  BORROWER_DECLINE_LOAN_AGRREMENT: DEV_PREFIX + '/api/user/declineLoanAgreement.json',
  INVEST_REGISTRATION: DEV_PREFIX + '/api/user/registration/lender.json',
  ADD_QUOTE_TERM: DEV_PREFIX + '/api/user/add_quote_term.json',
  QUOTE_TERM: DEV_PREFIX + '/api/user/quote_term.json',
  QUOTE_INCOME: DEV_PREFIX + '/api/user/quote_income.json',
  ADDRESS: DEV_PREFIX + '/api/user/address.json',
  GET_CURRENT_ADDRESS: DEV_PREFIX + '/api/user/address_current.json',
  PREFERENCES: DEV_PREFIX + '/api/user/user_preferences.json',
  APPLY_QUOTE: DEV_PREFIX + '/api/user/apply_quote.json',
  GET_DASHBOARD: DEV_PREFIX + '/api/user/dashboard.json',
  GET_QUOTES: DEV_PREFIX + '/api/user/quotes.json',
  ADD_INVESTOR_CONTACTS: DEV_PREFIX + '/api/user/add_investor_contacts.json',
  GET_INVESTOR_CONTACTS: DEV_PREFIX + '/api/user/investor_communications.json',
  USER_INCOME: DEV_PREFIX + '/api/user/user_income.json',
  BANK_DETAILS: DEV_PREFIX + '/api/user/bank_details.json',
  GET_USER_DETAILS: DEV_PREFIX + '/api/user/user_details.json',
  UPDATE_USER_DETAILS: DEV_PREFIX + '/api/user/update_details.json',
  INVEST_OFFER: DEV_PREFIX + '/api/user/invest_offers{id}.json',
  INVEST_OFFERS: DEV_PREFIX + '/api/user/invest_offers.json',
  INVEST_OFFER_RESTRICTIONS: DEV_PREFIX + '/api/user/invest_offers_restrictions.json',
  CHECK_CODE_VERIFICATION: DEV_PREFIX + '/api/user/check_verification_code.json',
  SEND_CODE_VERIFICATION: DEV_PREFIX + '/api/user/receive_verification_code.json',
  EMPLOYMENTS: DEV_PREFIX + '/api/user/employments.json',
  DEPENDENTS: DEV_PREFIX + '/api/user/dependent.json',
  DELETE_DEPENDENTS: DEV_PREFIX + '/api/user/dependents.json',
  HOUSEHOLD: DEV_PREFIX + '/api/user/household.json',
  BASE_LOAN_PROPERTIES: DEV_PREFIX + '/api/compute/base_loan_properties.json',
  CREDIT_LINE: DEV_PREFIX + '/api/user/credit_line.json',
  CREDIT_LINE_DECLUTTER: DEV_PREFIX + '/api/user/declutters.json',
  WORLDPAY_PARAMS: DEV_PREFIX + '/api/worldpay/form_parameters.json',
  WORLDPAY_RESPONSE: DEV_PREFIX + '/api/worldpay/response.json',
  GET_QUOTE_DETAILS: DEV_PREFIX + '/api/user/quote_details_for_draft_loan.json',
  GET_QUOTE_UNDERWRITING_DETAILS: DEV_PREFIX + '/api/user/quote_underwriting_details.json',
  GET_ALL_TERMS: DEV_PREFIX + '/api/compute/all_terms_payment_values.json',
  SUBMIT_LOAN_APPLICATION: DEV_PREFIX + '/api/user/submit_loan_application.json',
  GET_QUOTE_XML_RESPONSE: DEV_PREFIX + '/api/user/get_quote_response.json',
  GET_LOAN_XML_RESPONSE: DEV_PREFIX + '/api/user/get_loan_response.json',
  GET_USER_ACTIVITY_LOGS: DEV_PREFIX + '/api/user/user_activity_logs.json',
  BORROWER_OPPORTUNITIES: DEV_PREFIX + '/api/user/borrower_opportunities.json',
  RETRIEVE_ADDRESS: 'https://services.postcodeanywhere.co.uk/CapturePlus/Interactive/RetrieveFormatted/v2.00/json3ex.ws',
  FIND_ADDRESS: 'https://services.postcodeanywhere.co.uk/CapturePlus/Interactive/Find/v2.00/json3ex.ws',
  LOANS_DETAILS: DEV_PREFIX + '/api/user/loans_details.json',
  PORTFOLIO_SUMMARY: DEV_PREFIX + '/api/user/portfolioSummary.json',
  WITHDRAW_BORROWER: DEV_PREFIX + '/api/user/withdrawal/createBorrower',
  WITHDRAW_INVESTOR: DEV_PREFIX + '/api/user/withdrawal/createInvestor',
  CONTRACT_PDF: DEV_PREFIX + '/api/file/contract/{loan_apply_id}/pdf.json',
  INSTALMENTS: DEV_PREFIX + '/api/user/instalments',
  INSTALMENTS_BY_OFFER: DEV_PREFIX + '/api/user/instalmentsByOffer.json',

  ADMIN_TOKEN: DEV_PREFIX + '/whadmin/token.json',
  ADMIN_CLIENTS: DEV_PREFIX + '/whadmin/clients/search.json',
  ADMIN_BORROWERS: DEV_PREFIX + '/whadmin/clients/borrowers.json',
  ADMIN_INVESTORS: DEV_PREFIX + '/whadmin/clients/investors.json',
  ADMIN_CLIENT_ADDRESSES: DEV_PREFIX + '/whadmin/clients/{id}/addresses.json',
  ADMIN_CLIENT_COMMUNICATION: DEV_PREFIX + '/whadmin/clients/{id}/communications.json',
  ADMIN_CLIENT_DETAILS: DEV_PREFIX + '/whadmin/clients/{id}/details.json',
  ADMIN_CLIENT_ACTIVITY_LOGS: DEV_PREFIX + '/whadmin/clients/{id}/activity.json',
  ADMIN_CLEAR_SECURITY: DEV_PREFIX + '/whadmin/clear_security{id}.json',
  ADMIN_ACTIVITY_LOGS: DEV_PREFIX + '/whadmin/admin/activity.json',
  ADMIN_SET_PASSWORD: DEV_PREFIX + '/whadmin/user/password.json',
  ADMIN_FORGOT_PASSWORD_TOKEN: DEV_PREFIX + '/whadmin/user/forgotten_password_token.json',
  ADMIN_RESET_PASSWORD: DEV_PREFIX + '/whadmin/user/password.json',
  ADMIN_BORROWER_CLIENT: DEV_PREFIX + '/whadmin/borrowers/{id}/addresses.json',
  ADMIN_BORROWER_DETAILS: DEV_PREFIX + '/whadmin/borrowers/{id}/details.json',
  ADMIN_BORROWER_FINANCES: DEV_PREFIX + '/whadmin/borrowers/{id}/finances.json',
  ADMIN_HISTORY: DEV_PREFIX + '/whadmin/application/{id}/occupation_history.json',
  ADMIN_DEBTS: DEV_PREFIX + '/whadmin/application/{id}/debts.json',
  ADMIN_HOUSEHOLD: DEV_PREFIX + '/whadmin/application/{id}/monthly_expenses.json',
  ADMIN_DEPENDANTS: DEV_PREFIX + '/whadmin/application/{id}/dependents.json',
  ADMIN_SOCIAL_CONTACTS: DEV_PREFIX + '/whadmin/application/{id}/socials_contacts.json',
  ADMIN_BORROWER_TERMS: DEV_PREFIX + '/whadmin/borrowers/{id}/terms.json',
  ADMIN_INVESTOR_CLIENT: DEV_PREFIX + '/whadmin/investors/{id}/addresses.json',
  ADMIN_INVESTOR_DETAILS: DEV_PREFIX + '/whadmin/investors/{id}/details.json',
  ADMIN_INVESTOR_FINANCES: DEV_PREFIX + '/whadmin/investors/{id}/finances.json',
  ADMIN_BORROWER_ID_CHECK: DEV_PREFIX + '/whadmin/borrowers/{id}/id_check.json',
  ADMIN_INVESTOR_ID_CHECK: DEV_PREFIX + '/whadmin/investors/{id}/id_check.json',
  ADMIN_INVESTOR_ID_CHECK_HTML: DEV_PREFIX + '/whadmin/get_api_call_response.json',
  ADMIN_BORROWER_ACCEPT_DECLINE_CHECK_HTML: DEV_PREFIX + '/whadmin/borrower/decision.json',
  ADMIN_INVESTOR_ACCEPT_DECLINE_CHECK_HTML: DEV_PREFIX + '/whadmin/investor/decision.json',
  ADMIN_CREDIT_CONFIRM_DECISION: DEV_PREFIX + '/whadmin/borrowers/confirmCreditDecision.json',
  ADMIN_CREDIT_DECLINE_DECISION: DEV_PREFIX + '/whadmin/borrowers/declineCreditDecision.json',
  ADMIN_CREDIT_DECISION_GET_QUOTE: DEV_PREFIX + '/whadmin/borrowers/creditDecision/{quote_id}.json',
  ADMIN_BORROWER_LOAN_APPLY_DETAILS: DEV_PREFIX + '/whadmin/borrowers/{quote_id}/loan_apply_details.json',
  ADMIN_INVESTOR_LOAN_APPLY_DETAILS: DEV_PREFIX + '/whadmin/investors/{quote_id}/quotation_details.json',
  ADMIN_BORROWER_MATCHING_DETAILS: DEV_PREFIX + '/whadmin/borrowers/{quote_id}/matchingDetails.json',
  ADMIN_BORROWER_ACCEPTENCE: DEV_PREFIX + '/whadmin/borrowers/{quote_id}/acceptanceStatus.json',
  ADMIN_BORROWER_QUATATION: DEV_PREFIX + '/whadmin/borrowers/{quote_id}/quotation.json',
  ADMIN_CALCULATE_LOAN: DEV_PREFIX + '/whadmin/borrowers/calculate_loan_properties',
  ADMIN_INVESTOR_OFFER_DETAILS: DEV_PREFIX + '/whadmin/investors/{quote_id}/offer_details_list.json',
  ADMIN_PENDING_CREDIT_LINES: DEV_PREFIX + '/whadmin/pendingCreditLines.json',
  ADMIN_SET_CREDIT_LINES: DEV_PREFIX + '/whadmin/setPayCreditLine.json',
  ADMIN_PORTFOLIO_SUMMARY: DEV_PREFIX + '/whadmin/investor/{id}/portfolioSummary.json',
  ADMIN_CONTRACT_PDF: DEV_PREFIX + '/whadmin/clients/contract/{loan_apply_id}/pdf.json',
  ADMIN_WITHDRAWS: DEV_PREFIX + '/whadmin/withdrawals.json',
  ADMIN_CONFIRM_WITHDRAWS: DEV_PREFIX + '/whadmin/confirmWithdrawal.json',
  ADMIN_INSTALMENTS: DEV_PREFIX + '/whadmin/application/{quote_id}/instalments.json',
  ADMIN_INSTALMENTS_BY_OFFER: DEV_PREFIX + '/whadmin/instalmentsByOffer.json',
  ADMIN_GENERATE_INSTALMENT: DEV_PREFIX + '/whadmin/borrowers/{quote_id}/instalment.json',
  ADMIN_PAY_INSTALMENT: DEV_PREFIX + '/whadmin/confirmInstalment.json',
  ADMIN_TRANSACTIONS: DEV_PREFIX + '/whadmin/transactions.json'
};
export const PCA_KEY: string = 'FN34-CK53-JM31-AG87';
export const INCOME_SOURCES: Array<string> = [
  'Permanent',
  'Temporary',
  'Contractor',
  'Part-time',
  'Social Allowance',
  'Self-Employed',
  'Spousal Maintenance',
  'Pension',
  'Savings',
  'Investments'
];
export const TITLES: Array<string> = [
  'Mr',
  'Mrs',
  'Ms'
];
export const CONTACT_NUMBER_CODE = {
  SKYPE: 100,
  MOBILE: 200,
  HOME: 300,
  WORK: 400,
  LINKEDIN: 500,
  FACEBOOK: 600,
  EMAIL: 700
};

export const PAYMENT_STATUS = {
  10: 'PENDING',
  20: 'PAID',
  30: 'LATE',
  40: 'MISSED',
  50: 'RECOVERED',
  1000: 'PROJECTED'
};

export const INSTALLMENT_STATUS = {
  10: 'OUTSTANDING',
  20: 'PART_CLEARED',
  30: 'CLEARED',
  40: 'OVERPAID'
};

export const COMMUNICATION_CODE = {
  100: {prop: 'skype', label: 'Skype'},
  200: {prop: 'mobile_phone', label: 'Mobile'},
  300: {prop: 'home_phone', label: 'Home'},
  400: {prop: 'work_phone', label: 'Work'},
  500: {prop: 'linkedin', label: 'Linkedin'},
  600: {prop: 'facebook', label: 'Facebook'},
  700: {prop: 'email', label: 'Email'}
};
export const QUOTE_STATUS_CODE = {
  10: 'Draft',
  15: 'Funding Required',
  16: 'Pending Funds',
  20: 'Pending',
  30: 'Refer',
  40: 'Rejected',
  50: 'Approved',
  60: 'Verification',
  70: 'Declined',
  80: 'Active',
  100: 'Closed'
};
export const LOAN_STATUS_CODE = {
  100: 'Draft',
  200: 'Submitted',
  300: 'Call Validate check',
  320: 'Affordability Check',
  330: 'Credit History Check',
  350: 'Credit Decision',
  400: 'Acceptance',
  450: 'Declined',
  500: 'Matching',
  550: 'Fully Matched',
  600: 'Signed',
  700: 'Aborted',
  800: 'Active',
  900: 'Settled'
};
export const PRODUCT_TYPE = {
  0: 'borrow',
  1: 'invest'
};
export const CREDIT_TYPE_CODE = {
  100: 'Credit card',
  200: 'Personal loan',
  300: 'Store card',
  400: 'Mortgage',
  500: 'Car finance',
  600: 'Hire Purchase',
  700: 'Overdraft',
  800: 'Payday',
  900: 'Friends & Family'
};
export const OFFER_RATE_CODE = {
  100: 'SECONDARY MARKET',
  200: 'FIXED TERM PAYOUT'
};
export const OFFER_FREQUENCY_CODE = {
  100: 'Monthly',
  200: 'Quarterly',
  300: 'One-Off'
};
export const OFFER_TYPE_CODE = {
  100: 'ISA',
  200: 'SIPP',
  300: 'GEN',
  400: 'PIONEER'
};
export const REVERSE_OFFER_TYPE_CODE = {
  'ISA': 100,
  'SIPP': 200,
  'GENERAL': 300,
  'PIONEER': 400
};
export const OFFER_TERM_CODE = {
  12: '1 Year',
  18: '1 Year and Half',
  24: '2 Years',
  36: '3 Years',
  48: '4 Years',
  60: '5 Years',
  1000: 'Ongoing'
};
export const OFFER_STATUS_CODE = {
  100: 'Draft',
  200: 'Pending Funds',
  250: 'Pending',
  300: 'Active',
  400: 'Settled',
  500: 'Cancel'
};
export const WORLDPAY_MODE = {
  test: 100,
  prod: 0
};
export const FINANCE_TYPE: string = '100';
export const INVEST_RATES: any = {
  1000: 6.3,
  12: 3.4,
  18: 3.9,
  24: 4.2,
  36: 4.7,
  48: 5.2,
  60: 5.7
};
export const ACTIVITY_LOGS_MESSAGE = {
  1001: 'Cleared security \'Admin only\' type',
  1002: 'Cleared security \'Cleared\' type',
  1020: 'Login',
  1021: 'Logout',
  2000: 'User Registered',
  2001: 'Email Verified',
  2002: 'Mobile Verified',
  2003: 'Login',
  2004: 'Logout',
  2005: 'User preferences updated',
  2006: 'User forgot password',
  2007: 'User reset password',
  2010: 'Email Address Added',
  2011: '[Communication Type] was  added/updated',
  2020: 'Bank details updated/added',
  2021: 'Customer Income updated/added',
  2022: 'Occupation updated/added',
  2023: 'Household updated/added',
  2024: 'DeClutter updated/added',
  2025: 'CreditLine updated/added',
  2026: 'Dependent updated/added',
  2100: 'Borrower Application Initiated',
  2101: 'Borrower Application Submitted',
  2102: 'Loan Request was created',
  2103: 'Loan Request was submitted',
  2201: 'Borrower Quotation request send',
  2202: 'Borrower CallReport request send',
  2203: 'Borrower CallValidate request send',
  2204: 'Borrower Affordability request send',
  2205: 'Lender Quotation request send',
  2206: 'Assign [Product Description] product to the Quote',
  2207: 'Quotation CallCredit request',
  2208: 'Credit History CallCredit request',
  2209: 'Management CallCredit request',
  2300: 'Lender Application Initiated',
  2301: 'User submitted Lender Quote',
  2302: 'New offer was created',
  2303: 'WorldPay payment was done',
  2304: 'WorldPay payment was failed'
};

export const RATE = {
  10: 'POOR',
  20: 'AVERAGE',
  30: 'GOOD',
  40: 'EXCELLENT'
};

export const TRANSACTION_OWNER_STATUS = {
  100: 'INVESTOR',
  200: 'BORROWER',
  300: 'WORLDPAY',
  400: 'TRUST BANK ACCOUNT LENDERS',
  500: 'TRUST BANK ACCOUNT BORROWERS',
  600: 'TRUST BANK ACCOUNT FIRST LOSS',
  700: 'TRUST BANK ACCOUNT BONUS',
  800: 'FASTER PAYMENT',
  900: 'ISA TRANSFER',
  1000: 'SIPP TRANSFER',
  1100: 'BROKER',
  1200: 'SELLER',
  1300: 'BUYER'

};
export const TRANSACTION_GLACCOUNT_STATUS = {
  100: 'CLIENT ACCOUNT INVESTOR',
  200: 'CLIENT ACCOUNT BORROWER',
  300: 'BORROWER ACCOUNT',
  400: '3DPARTY PAYMENTS',
  500: 'LOAN ACCOUNT INVESTOR',
  600: 'LOAN ACCOUNT BORROWER',
  700: 'BANK ACCOUNT BONUS',
  800: 'BONUS ACCOUNT BORROWER',
  900: 'BANK_ACCOUNT_FIRST_LOSS_FUND',
  1000: 'BANK ACCOUNT BORROWER',
  11000: 'BANK ACCOUNT INVESTOR',
  1200: 'BANK ACCOUNT BROKER',
  1300: 'INSTALMENT ACCOUNT BORROWER',
  1400: 'INSTALMENT ACCOUNT INVESTOR',
  1500: 'INTEREST EARNED INVESTOR',
  1600: 'INTEREST EXPENSE BORROWER',
  1700: 'BROKER FEE',
  1800: 'BROKER SUSPENSE ACCOUNT'
};

export const TRANSACTION_ACCOUNT_STATUS = {
  100: 'CASH PENDING',
  200: 'CASH AVAILABLE',
  250: 'CASH ALLOCATED',
  300: 'CASH ON OFFER',
  400: 'PENDING PRINCIPLE',
  500: 'PENDING INTEREST',
  600: 'PENDING BONUS',
  700: 'PENDING BROKER FEE',
  800: 'PRINCIPLE',
  900: 'INTEREST RECEIVABLE',
  1000: 'BONUS',
  1100: 'BROKER FEE',
  1200: 'BORROWER ADVANCE',
  1300: 'WITHDRAWAL PENDING',
};

export const TRANSACTION_SUB_ACCOUNT_STATUS = {
  100: 'CASH PENDING',
  200: 'CASH WITHDRAW',
  300: 'CASH AVAILABLE',
  400: 'CASH OFFER',
  500: 'CASH ALLOC',
  600: 'CASH REINVEST',
  700: 'PRINCIPLE',
  800: 'INTEREST',
  900: 'SETTLING',
  1000: 'FASTER PAYMENT',
  1100: 'WORLDPAY',
  1200: 'IFISA IN',
  1300: 'IFISA OUT',
  1400: 'SIPP IN',
  1500: 'SIPP OUT',
  1600: 'CHARGE',
  1700: 'PENDING',
  1800: 'EARNED',
  1900: 'FORFEIT',
  2000: 'PAID',
  2100: 'RECOVER',
  2200: 'REPAID',
  2300: 'INSTALLMENT'
};

export const ERRORS = {
  'user.username.isBlank': 'We need your username to proceed. Please enter a valid username.',
  'user.registrationKey.invalid': 'Oops! Your email validation key has expired, please contact support@ to request a new verification email to be sent.',
  'user.badCredentials': 'Sorry, something didn\'t quite go right. The combination of email and password do not seem to be correct. Try again, or follow the "Forgot Password" link.',
  'user.password.isBlank': 'You need to enter your password here to proceed. Please enter your password.',
  'user.emailNotConfirmed': 'You need to re-enter your email address to proceed. Please re-enter your email address.',
  'userAddress.moved.smallPeriod': 'We need at least five (5) years UK address history. Please enter prior addresses.',
  'userIncome.mainIncomeSource.isBlank': 'Please enter your Main Source of Income, this cannot be blank.',
  'userIncome.bankSortCode.isBlank': 'Please enter your Bank Sort Code, this cannot be blank.',
  'userIncome.bankAccountNumber.isBlank': 'Please enter your Bank Account Number, this cannot be blank.',
  'investOffer.investmentAmount.isBlank': 'Oops! You need to tell us how much you want to invest. Please enter Investment Amount here.',
  'investOffer.investmentAmount.notInRange': 'Please enter Investment Amount less than £2,000.',
  'investOffer.investmentAmount.wrong': 'Please enter valid amount.',
  'investOffer.investmentAmount.notMultipleBy20': 'Investment Amount should be multiple by 20.',

  'form.firstname.isBlank': 'Please enter your first name, this cannot be blank.',
  'form.lastname.isBlank': 'Please enter your surname, this cannot be blank.',
  'form.password.isBlank': 'Oops! You need to enter a password here.',
  'form.password.short': 'Password is too short',
  'form.password.not_matched': 'Passwords are not the same',
  'form.password.wrong': 'Your password needs to be a little stronger. Make sure it is at least eight (8) characters, including at least 1x CAPITAL LETTER and at least 1x number (from 0 to 9).',
  'form.username.isBlank': 'Oops! You need to enter your username. You are best using your main email address.',
  'form.username.wrongEmail': 'Sorry, something didn\'t quite go right. Make sure you have entered a valid email address (e.g. myname@mywebsite.com).',
  'form.username.notUnique': 'The email address you have entered has already been registered. Go to the log-in page and use "forgotten password" to reset your password and log on.',
  'form.password.notConfirmed': 'Oops! The confirmation password does not match, please re-enter both passwords to be sure.',
  'form.birthdate.wrong': 'No offence intended, you have to be at least 18 years old to register on our platform. If you are over 18, please re-enter you date of birth.',
  'form.birthdate.isBlank': 'Oops! You need to enter a date of birth here.',
  'form.quoteTerm.amount.notInRange': 'Sorry, we currently only accept loan amounts between £1,000 and £25,000. Please re-enter an amount within this acceptable range or contact support.',
  'form.quoteTerm.term.notInRange': 'Sorry, your loan term can only be between 12 months and 60 months.',
  'form.quoteIncome.totalGrossAnnual.notInRange': 'Sorry, you need to have a gross annual income of at least £5,000. If you earn more than £500,000, please contact support, they will guide you through the next steps. £21,000 default; £100 minor step; £2,000 major step.',
  'userAddress.isNull': 'You need to enter your address.',

  'admin.password.wrong': 'Your password needs to be a little stronger. Make sure it is at least twelve (12) characters, including at least 1x CAPITAL LETTER, at least 1x number (from 0 to 9) and a special symbol (!@#$%^&*()-_)',
  'form.userAddress.isBlank': 'This value should not be blank.',
  'form.userAddress.movedMonth.isBlank': 'This value should not be blank.',
  'badCredentials': 'This value should not be blank.',
  'form.userAddress.movedMonthMovedYear.isBlank': 'This value should not be blank.',
  'form.userAddress.mobilePoneNumber.isBlank': 'This value should not be blank.',
  'form.userAddress.homePoneNumber.isBlank': 'This value should not be blank.',
  'form.userAddress.workPoneNumber.isBlank': 'This value should not be blank.',
  'form.userAddress.title.isBlank': 'This value should not be blank.',
  'form.userAddress.screenName.isBlank': 'This value should not be blank.',
  'userCreditLine.type.isBlank': 'This value should not be blank.',
  'userCreditLine.name.isBlank': 'This value should not be blank.',
  'userCreditLine.creditLimit.isBlank': 'This value should not be blank.',
  'userCreditLine.balanceInput.isBlank': 'This value should not be blank.',
  'userCreditLine.apr.isBlank': 'This value should not be blank.',
  'userCreditLine.monthlyPayment.isBlank': 'This value should not be blank.',
  'userCreditLine.name.notString': 'This field is a string',
  'investOffer.investmentOfferName.isBlank': 'This value should not be blank.',
  'form.mobileVerification.code.isBlank': 'This value should not be blank.',
};
