import { Injectable } from '@angular/core';
import { ACTIVITY_LOGS_MESSAGE, COMMUNICATION_CODE } from "../config";

@Injectable()
export class TextBuilderService {

  activityLogMessage(code:number, metadata:any) {
    let message;
    switch (code) {
      case 1001:
        //Cleared security 'Admin only' type
        message = `${ACTIVITY_LOGS_MESSAGE[code]} for <span class="link" data-client-id="${metadata.user.details.id}">${metadata.user.details.first_name} ${metadata.user.details.last_name}<span>`;
        break;
      case 1002:
        //Cleared security 'Cleared' type
        message = `${ACTIVITY_LOGS_MESSAGE[code]} for <span class="link" data-client-id="${metadata.user.details.id}">${metadata.user.details.first_name} ${metadata.user.details.last_name}<span>`;
        break;
      case 2011:
        //[Communication Type] was added/updated
        message = ACTIVITY_LOGS_MESSAGE[code].replace('[Communication Type]', COMMUNICATION_CODE[JSON.parse(metadata.metadata).type].label);
        break;
      case 2206:
        //Assign [Product Description] product to the Quote
        message = ACTIVITY_LOGS_MESSAGE[code].replace('[Product Description]', metadata.metadata.product_description);
        break;
      default:
        message = ACTIVITY_LOGS_MESSAGE[code];
    }

    return message ? message : '-';
  }
}
