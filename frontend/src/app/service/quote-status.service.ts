import { Injectable } from '@angular/core';
import { BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class QuoteStatusService {

  constructor() {}

  private quoteStatus = new BehaviorSubject<string>('');
  quoteStatus$ = this.quoteStatus.asObservable();

  changeQuoteStatus(status) {
    this.quoteStatus.next(status);
  }

}
