import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class DashboardService {

  constructor() {}

  private dashboard = new BehaviorSubject<any>({});
  dashboard$ = this.dashboard.asObservable();

  changeDashboardData(status) {
    this.dashboard.next(status);
  }

}
