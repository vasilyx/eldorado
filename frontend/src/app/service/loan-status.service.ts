import { Injectable } from '@angular/core';
import { BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class LoanStatusService {

  constructor() {}

  private loanStatus = new BehaviorSubject<string>('');
  loanStatus$ = this.loanStatus.asObservable();

  changeLoanStatus(status) {
    this.loanStatus.next(status);
  }

}
