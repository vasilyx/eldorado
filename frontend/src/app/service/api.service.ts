import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {NotificationsService} from 'angular2-notifications-lite';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {SpinnerService} from '../shared/spinner/spinner.service';
import {StorageService} from './storage.service'
import {ADMIN_PREFIX_PATH, API, ERRORS, PCA_KEY} from '../config'

@Injectable()
export class ApiService {

  constructor(private http: Http,
              private storageService: StorageService,
              private spinner: SpinnerService,
              private router: Router,
              private _notifications: NotificationsService) {
  }

  retrievePCAAddress(id: string): Observable<any> {
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    let options = new RequestOptions({headers: headers});
    let url = API.RETRIEVE_ADDRESS +
      '?Key=' + PCA_KEY +
      '&Id=' + id;

    return this.http.get(url, options)
      .map((res: Response) => {
        if (res.statusText === 'OK') {

          return JSON.parse(res['_body']);
        }

        return res;
      })
      .catch((error: any) => Observable.throw(error.json() || 'Server error'));
  }

  findPCAAddress(id: string, text: string): Observable<any> {
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    let options = new RequestOptions({headers: headers});
    let url = API.FIND_ADDRESS +
      '?Key=' + PCA_KEY +
      '&LastId=' + id +
      '&SearchTerm=' + text;

    return this.http.get(url, options)
      .map((res: Response) => {
        if (res.statusText === 'OK') {

          return JSON.parse(res['_body']);
        }

        return res;
      })
      .catch((error: any) => Observable.throw(error.json() || 'Server error'));
  }

  /*option:
   * saveToken(login, registration),
   * deleteToken(logout),
   * hideLoader(other requests),
   * authorizationToken(for Authorization Header)
   * credentials(for Authorization Header)
   * schema(for Authorization Header)
   */
  getMethod(url: string, option: Object = {}): Observable<any> {
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Auth': 'whs' + this.storageService.read('token')
    });
    option['authorizationToken'] ? headers.append('Authorization', this.getAuthorizationToken(option['schema'], option['credentials'])) : null;
    let options = new RequestOptions({headers: headers});

    //show loader by default
    option['hideLoader'] ? this.spinner.stop() : this.spinner.start();

    return this.http.get(url, options)
      .map((res: Response) => {
        this.spinner.stop();
        if (res.statusText === 'OK' && res['_body']) {
          let body = JSON.parse(res['_body']);

          option['saveToken'] ? this.storageService.write('token', body.content ? body.content.token : body.token) : '';
          option['deleteToken'] ? this.storageService.remove('token') : '';

          return body;
        }
      })
      .catch((error: any) => {
        this.errorHandler(error.json());
        return Observable.throw(error.json() || 'Server error')
      });
  }

  /*option:
   * saveToken(login, registration),
   * deleteToken(logout),
   * hideLoader(other requests),
   * authorizationToken(for Authorization Header)
   * credentials(for Authorization Header)
   */
  postMethod(url: string, body?: Object, option: Object = {}): Observable<any> {
    let bodyString = JSON.stringify(body);
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Auth': 'whs' + this.storageService.read('token')
    });
    option['authorizationToken'] ? headers.append('Authorization', this.getAuthorizationToken(option['schema'], option['credentials'])) : null;
    let options = new RequestOptions({headers: headers});

    //show loader by default
    option['hideLoader'] ? this.spinner.stop() : this.spinner.start();

    return this.http.post(url, bodyString, options)
      .map((res: Response) => {
        this.spinner.stop();
        if ((res.statusText === 'OK' || res.statusText === 'Created') && res['_body']) {
          let body = JSON.parse(res['_body']);

          option['saveToken'] ? this.storageService.write('token', body.content ? body.content.token : body.token) : '';
          option['deleteToken'] ? this.storageService.remove('token') : '';

          return body;
        }

        return res;
      })
      .catch((error: any) => {
        this.errorHandler(error.json());
        return Observable.throw(error.json() || 'Server error')
      });
  }

  /*option:
   * saveToken(login, registration),
   * deleteToken(logout),
   * hideLoader(other requests),
   * authorizationToken(for Authorization Header)
   * credentials(for Authorization Header)
   * schema(for Authorization Header)
   */
  patchMethod(url: string, body?: Object, option: Object = {}): Observable<any> {
    let bodyString = JSON.stringify(body);
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Auth': 'whs' + this.storageService.read('token')
    });
    option['authorizationToken'] ? headers.append('Authorization', this.getAuthorizationToken(option['schema'], option['credentials'])) : null;
    let options = new RequestOptions({headers: headers});

    //show loader by default
    option['hideLoader'] ? this.spinner.stop() : this.spinner.start();

    return this.http.patch(url, bodyString, options)
      .map((res: Response) => {
        this.spinner.stop();
        if (res.statusText === 'OK' && res['_body']) {
          let body = JSON.parse(res['_body']);

          option['saveToken'] ? this.storageService.write('token', body.content.token) : '';
          option['deleteToken'] ? this.storageService.remove('token') : '';

          return body;
        }

        return res;
      })
      .catch((error: any) => {
        this.errorHandler(error.json());
        return Observable.throw(error.json() || 'Server error')
      });
  }

  /*option:
   * saveToken(login, registration),
   * deleteToken(logout),
   * hideLoader(other requests),
   * authorizationToken(for Authorization Header)
   * credentials(for Authorization Header)
   * schema(for Authorization Header)
   */
  putMethod(url: string, body?: Object, option: Object = {}): Observable<any> {
    let bodyString = JSON.stringify(body);
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Auth': 'whs' + this.storageService.read('token')
    });
    option['authorizationToken'] ? headers.append('Authorization', this.getAuthorizationToken(option['schema'], option['credentials'])) : null;
    let options = new RequestOptions({headers: headers});

    //show loader by default
    option['hideLoader'] ? this.spinner.stop() : this.spinner.start();

    return this.http.put(url, bodyString, options)
      .map((res: Response) => {
        this.spinner.stop();
        if (res.statusText === 'OK' && res['_body']) {
          let body = JSON.parse(res['_body']);

          option['saveToken'] ? this.storageService.write('token', body.content.token) : '';
          option['deleteToken'] ? this.storageService.remove('token') : '';

          return body;
        }

        return res;
      })
      .catch((error: any) => {
        this.errorHandler(error.json());
        return Observable.throw(error.json() || 'Server error')
      });
  }

  /*option:
   * saveToken(login, registration),
   * deleteToken(logout),
   * hideLoader(other requests),
   * authorizationToken(for Authorization Header)
   * credentials(for Authorization Header)
   * schema(for Authorization Header)
   */
  deleteMethod(url: string, body?: Object, option: Object = {}): Observable<any> {
    let bodyString = JSON.stringify(body);
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Auth': 'whs' + this.storageService.read('token')
    });
    option['authorizationToken'] ? headers.append('Authorization', this.getAuthorizationToken()) : null;
    let options = new RequestOptions({headers: headers, body: bodyString});

    //show loader by default
    option['hideLoader'] ? this.spinner.stop() : this.spinner.start();

    return this.http.delete(url, options)
      .map((res: Response) => {
        this.spinner.stop();
        if (res.statusText === 'OK' && res['_body']) {
          let body = JSON.parse(res['_body']);

          option['saveToken'] ? this.storageService.write('token', body.content.token) : '';
          option['deleteToken'] ? this.storageService.remove('token') : '';

          return body;
        }

        return res;
      })
      .catch((error: any) => {
        this.errorHandler(error.json());
        return Observable.throw(error.json() || 'Server error')
      });
  }

  getAuthorizationToken(schema?: string, credentials?: string) {
    if (schema === 'FORGOTTEN') {
      return schema + ' ' + credentials;
    } else if (credentials) {
      return 'Basic ' + window.btoa(credentials);
    } else {
      return 'BEARER ' + this.storageService.read('token');
    }
  }

  errorHandler(error) {
    this.spinner.stop();
    if (error.content && error.content.code === 401) {
      if (this.router.url.indexOf(ADMIN_PREFIX_PATH) !== -1) {
        this.router.navigate(['/whadmin/login']);
      } else {
        this.router.navigate(['/login']);
      }
    } else if (error.content && error.content.code === 403) {
      if (this.router.url.indexOf(ADMIN_PREFIX_PATH) !== -1) {
        this.router.navigate(['/whadmin/clients']);
      } else {
        this.router.navigate(['/portal/dashboard']);
      }
    }
    let message = [];
    if (error.content instanceof Array || error.content instanceof Object) {
      for (let key in error.content) {
        if (error.content[key] instanceof Array || error.content[key] instanceof Object) {
          let innerContent = error.content[key];
          for (let key in innerContent) {
            let errorMessage = ERRORS[innerContent[key]] ? ERRORS[innerContent[key]] : innerContent[key];
            message.push('\n' + errorMessage);
          }
        } else {
          let errorMessage = ERRORS[error.content[key]] ? ERRORS[error.content[key]] : error.content[key];
          message.push('\n' + errorMessage);
        }
      }
    } else {
      let errorMessage = ERRORS[error.content.code] ? ERRORS[error.content.code] : error.content.message;
      message.push('\n' + errorMessage);
    }
    this._notifications.error(message[0], message[1], {timeOut: 3000, showProgressBar: true})
    //todo other errors
  }
}
