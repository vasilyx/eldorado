import { Injectable } from '@angular/core';

@Injectable()
export class TabSaverService {

  constructor() {}

  private _tab: {} = {};

  tab(tab, value) {
    this._tab[tab] = value;
  }

  getTab(tab):any {
    return this._tab[tab] ? this._tab[tab] : undefined;
  }

}
