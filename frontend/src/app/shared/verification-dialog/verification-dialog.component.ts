import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { API, ERRORS } from "../../config";

@Component({
  selector: 'verification-dialog',
  templateUrl: './verification-dialog.component.html',
  styleUrls: ['./verification-dialog.component.scss']
})
export class VerificationDialogComponent {

  code:string;
  mobile_phone:string;
  errors:any = {};

  constructor(public dialogRef:MdDialogRef<any>,
              public spinner:SpinnerService,
              public apiService:ApiService) {
  }

  ngOnInit() {
    this.sendCodeVerification();
  }

  checkCodeVerification() {
    if (this.codeValidation()) {
      this.apiService.postMethod(API.CHECK_CODE_VERIFICATION, {'code': this.code}).subscribe(
        response => {
          this.dialogRef.close(true);
        }
      )
    }
  }

  sendCodeVerification() {
    if(!/(^\+?44)|(^07)|(^\+?375)/.test(this.mobile_phone)) {
      this.mobile_phone = '+44' + this.mobile_phone;
    }
    this.apiService.postMethod(API.SEND_CODE_VERIFICATION, {'mobile_number': this.mobile_phone}, {hideLoader: true}).subscribe(
      response => {
        //
      }
    )
  }

  codeValidation() {
    let isValid = true;

    if (!this.code) {
      this.errors.code = ERRORS['form.mobileVerification.code.isBlank'];
      isValid = false;
    }

    return isValid;
  }
}
