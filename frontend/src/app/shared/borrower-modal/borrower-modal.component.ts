import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-invest-modal',
  templateUrl: './borrower-modal.component.html',
  styleUrls: ['./borrower-modal.component.scss']
})
export class BorrowerModalComponent {
  constructor(public dialogRef:MdDialogRef<BorrowerModalComponent>) { }
}

