import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class NavigationService {

  step:BehaviorSubject<number> = new BehaviorSubject(1);

  public getStep():number {
    return this.step.value;
  }

  public setStep(v:number) {
    this.step.next(v);
  }
}
