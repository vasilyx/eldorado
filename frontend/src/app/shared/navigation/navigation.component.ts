import { Component, OnInit, Input } from '@angular/core';
import { NavigationService } from '../navigation/navigation.service';

@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  @Input() steps:number;
  step:number;

  constructor(public navService:NavigationService) {
    this.navService = navService;
  }

  ngOnInit() {
    this.navService.step.subscribe(value => {
      this.step = value;
    });
  }

  getStepsCount(steps) {
    return new Array(steps);
  }
}
