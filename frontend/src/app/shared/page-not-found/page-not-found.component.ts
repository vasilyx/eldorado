import { Component, OnInit } from '@angular/core';
import {SpinnerService} from '../spinner/spinner.service';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {

  constructor(private spinner: SpinnerService) { }

  ngOnInit() {
    this.spinner.stop();
  }

}
