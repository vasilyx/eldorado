import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';

import { ApiService } from '../../service/api.service';
import { API, DATE_FORMAT_SERVER } from "../../config";

@Component({
  selector: 'app-clear-security-dialog',
  templateUrl: './clear-security-dialog.component.html',
  styleUrls: ['./clear-security-dialog.component.scss']
})
export class ClearSecurityDialogComponent {

  clientId:number;
  client:any = {};
  model:any = {};
  metadata:any = {};
  countOfCheckedMetadata:number = 0;

  constructor(private dialogRef: MdDialogRef<any>,
              private apiService: ApiService) {}

  ngOnInit() {
    this.getClientInformation();
  }

  getClientInformation() {
    this.apiService.getMethod(API.ADMIN_CLEAR_SECURITY.replace(/(\{id\})/i, `/${this.clientId}`), {'authorizationToken': true}).subscribe(
      response => {
        this.client = response.content;
      },
      err => {
        this.dialogRef.close(false);
      }
    )
  }

  metadataChecked(name:string) {
    if (this.model[name]) {
      this.metadata[name] = this.client[name];
      this.countOfCheckedMetadata++;
    } else {
      delete this.metadata[name];
      this.countOfCheckedMetadata--;
    }
  }

  goToDetails(type:number) {
    let data = {
      user_id: this.clientId,
      type: type,
      metadata: {}
    };
    if (type === 0) {
      data.metadata = this.metadata;
    }
    this.apiService.postMethod(API.ADMIN_CLEAR_SECURITY.replace(/(\{id\})/i, ''), data, {'authorizationToken': true}).subscribe(
      response => {
        this.dialogRef.close(true);
      }
    );
  }

  convertDateForDisplay(date) {
    if (date) {
      return moment(date, DATE_FORMAT_SERVER).format('D MMM YYYY');
    }
  }
}
