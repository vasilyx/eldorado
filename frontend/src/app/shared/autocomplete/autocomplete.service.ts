import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class AutocompleteService {

  public address:BehaviorSubject<any> = new BehaviorSubject('');

  public getAddress() {
    return this.address;
  }

  public setAddress(v) {
    this.address.next(v);
  }
}
