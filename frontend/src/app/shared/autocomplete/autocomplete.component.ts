import { Component, OnInit, Input } from '@angular/core';

import { AutocompleteService } from './autocomplete.service';
import { ApiService } from '../../service/api.service';
import { PATTERN_POSTCODE, PATTERN_STRING } from '../../config';

@Component({
  selector: 'my-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit {

  @Input() searchStr:string;
  @Input() errors: any;
  items:Array<Object>;
  address: any;

  constructor(public autocompleteService:AutocompleteService,
              public apiService:ApiService) { }

  ngOnInit() {
  }

  onChange(value) {
    this.searchStr = value;
    if (value.length > 1) {
      this.findAddress('', value);
    } else {
      this.items = [];
    }
  }

  public clickAddress(selected:any) {
    this.items = [];
    if (selected && selected.Next === 'Retrieve') {
      this.searchStr = selected.Text;
      this.retrieveAddress(selected.Id);
    } else if (selected && selected.Next === 'Find') {
      this.searchStr = selected.Text;
      this.findAddress(selected.Id, selected.Text);
    } else {
      this.searchStr = '';
    }
  }

  findAddress(id, text) {
    this.apiService.findPCAAddress(id, text).subscribe(
      response => {
        if (response.Items.length === 1 && response.Items[0].Error) {
          this.items = [{Text: response.Items[0].Resolution}];
        } else {
          this.items = response.Items;
        }
      },
      err => {
        //
      }
    )
  }

  retrieveAddress(id) {
    this.apiService.retrievePCAAddress(id).subscribe(
      response => {
        let data = response.Items[0];
        if (this.validation(data)) {
          const address: any = {
            postal_code: data.PostalCode,
            city: data.City,
            street: data.Street,
            building_number: data.BuildingNumber,
            building_name: data.BuildingName,
            sub_building_name: data.SubBuilding,
            dependant_locality: data.Neighbourhood,
            line1: data.Line1,
            line2: data.Line2,
            country: data.CountryIso2,
            region: data.Region,
            state: data.State
          };
          this.autocompleteService.setAddress(address);
        }
      },
      err => {
        //
      }
    )
  }

  validation(data) {
    let isValid = true;

    if (data.CountryIso2 !== 'GB') {
      isValid = false;
    }

    if (data.BuildingNumber.length > 10) {
      isValid = false;
    }

    if (data.BuildingName.length > 50) {
      isValid = false;
    }

    if (!data.PostalCode) {
      isValid = false;
    } else if (PATTERN_POSTCODE instanceof RegExp && !PATTERN_POSTCODE.test(data.PostalCode)) {
      isValid = false;
    }

    if (!data.City) {
      isValid = false;
    } else if (PATTERN_STRING instanceof RegExp && !PATTERN_STRING.test(data.City)) {
      isValid = false;
    }

    if (!data.Line1) {
      isValid = false;
    }

    this.items = isValid ? this.items : [{Text: 'error.address.missedFields'}];

    return isValid;
  }
}
