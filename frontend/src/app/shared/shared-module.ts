import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {ActivityLogsListComponent} from '.././shared/activity-logs-list/activity-logs-list.component';
import {SafePipe} from '../pipe/safe.pipe';
import {FilterInputPipe} from '../pipe/filter-input.pipe';
import {CheckDialogComponent} from '../admin/client-details/borrower-details/check-dialog/check-dialog.component';
import {CreditDecisionDialogComponent} from '../admin/client-details/borrower-details/credit-decision-dialog/credit-decision-dialog.component';
import {AuthGuard} from '../guard/auth-guard';
import {LoanAgreementDialogComponent} from '../loan/loan-agreement/loan-agreement-dialog/loan-agreement-dialog.component';
import {TabSaverService} from '../service/tab-saver.service';
import {SimpleNotificationsModule} from 'angular2-notifications-lite';
import {OfferAndDetailsInstalmentDialogComponent} from '../admin/client-details/investor-details/offers-and-details/offer-and-details-instalment-dialog/offer-and-details-instalment-dialog.component';
import {PaymentDialogComponent} from '../admin/client-details/borrower-details/instalments-borrower/payment-dialog/payment-dialog.component';


@NgModule({
  imports: [
    BrowserModule,
    SimpleNotificationsModule.forRoot()
  ],
  declarations: [
    ActivityLogsListComponent,
    SafePipe,
    FilterInputPipe,
    CheckDialogComponent,
    CreditDecisionDialogComponent,
    LoanAgreementDialogComponent,
    OfferAndDetailsInstalmentDialogComponent,
    PaymentDialogComponent
  ],
  exports: [
    BrowserModule,
    ActivityLogsListComponent,
    SafePipe,
    FilterInputPipe,
    CheckDialogComponent,
    CreditDecisionDialogComponent,
    LoanAgreementDialogComponent,
    SimpleNotificationsModule
  ],
  providers: [
    FilterInputPipe,
    AuthGuard,
    TabSaverService
  ],
  entryComponents: [
    CheckDialogComponent,
    CreditDecisionDialogComponent,
    LoanAgreementDialogComponent,
    OfferAndDetailsInstalmentDialogComponent,
    PaymentDialogComponent
  ]
})
export class SharedModule {
}