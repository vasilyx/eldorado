import { Component, OnInit, Input, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { HeaderService } from './header.service';
import { PORTAL_PREFIX_PATH } from '../../config';
import { DashboardService} from '../../service/dashboard.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'my-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit, OnDestroy {

  @Input() activeTab:string;
  @Input() loginPage: boolean = false;

  linkMain:string = `${PORTAL_PREFIX_PATH}/dashboard`;
  linkBorrow:string = '/borrow/registration';
  linkInvest:string = '/invest/registration';
  linkHome:string = '/login';
  clientStatus: string;
  subscription: Subscription;

  constructor(private router:Router,
              private headerService:HeaderService,
              private dashboardService: DashboardService) {
  }

  ngOnInit() {
    if (this.router.url.indexOf(PORTAL_PREFIX_PATH) === -1) {
      this.linkMain = '/login';
    }
    this.subscription = this.dashboardService.dashboard$.subscribe(res => {
      if(Object.keys(res).length) {
        this.clientStatus = res.quotes[0].quote_product == 1 ? 'investor' : 'borrower';
      }
    });
  }

//  toggleSidenav() {
//    this.headerService.toggleSidenav();
//  }

  login() {
    if (this.activeTab) {
      this.router.navigate(['/login']);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
