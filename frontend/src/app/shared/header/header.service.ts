import { Injectable }      from '@angular/core'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class HeaderService {

  sidenavOpenState = new BehaviorSubject<boolean>(true);

  sidenavItem$ = this.sidenavOpenState.asObservable();

  toggleSidenav() {
    this.sidenavOpenState.next(!this.sidenavOpenState.value);
  }
}
