import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-invest-modal',
  templateUrl: './invest-modal.component.html',
  styleUrls: ['./invest-modal.component.scss']
})
export class InvestModalComponent {
  constructor(public dialogRef:MdDialogRef<InvestModalComponent>) { }
}

