import { Component, Input } from '@angular/core';

@Component({
  selector: 'activity-logs-list',
  templateUrl: './activity-logs-list.component.html',
  styleUrls: ['./activity-logs-list.component.scss']
})
export class ActivityLogsListComponent {

  @Input() logs:Array<any>;

  constructor() {
  }
}
