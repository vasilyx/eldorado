import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { AutocompleteService } from '../../shared/autocomplete/autocomplete.service';
import { DATE_FORMAT_SERVER, API, ERRORS } from "../../config";

@Component({
  selector: 'address-dialog',
  templateUrl: './address-dialog.component.html',
  styleUrls: ['./address-dialog.component.scss']
})
export class AddressDialogComponent {

  index:number = -1;
  address:any;
  searchStr:string;
  moved:string;
  month:string;
  months:any = [];
  year:string;
  years:any = [];
  errors:any = {};

  constructor(public dialogRef:MdDialogRef<any>,
              public spinner:SpinnerService,
              public apiService:ApiService,
              public autocompleteService:AutocompleteService) {
  }

  ngOnInit() {
    this.autocompleteService.address.subscribe(address => this.address = address);
    this.months = moment.months();
    for (let i = parseInt(moment().format('YYYY')); i >= 1970; i--) {
      this.years.push(i.toString());
    }
    if (this.moved) {
      this.month = moment(this.moved, DATE_FORMAT_SERVER).format('MMMM');
      this.year = moment(this.moved, DATE_FORMAT_SERVER).format('YYYY');
    }
  }

  updateAddress(index) {

    if (!this.validation()) {
      return false;
    }

    let data:any = {};
    data = (<any>Object).assign({}, this.address);
    data.id = index !== -1 ? index : undefined;
    data.moved = this.convertDateForServer(this.month, this.year);

    this.apiService.postMethod(API.ADDRESS, data).subscribe(
      response => {
        this.dialogRef.close(true);
        this.autocompleteService.setAddress(undefined);
      }
    );
  }

  deleteAddress(id) {
    if (this.index === -1) {
      return false;
    }
    this.apiService.deleteMethod(API.ADDRESS, {id: id}).subscribe(
      response => {
        this.dialogRef.close(true);
        this.autocompleteService.setAddress(undefined);
      }
    );
  }

  validation() {
    let isValid = true;

    if (!this.searchStr && !this.address) {
      this.errors.address = ERRORS['form.userAddress.isBlank'];
      isValid = false;
    }

    if (!this.month && !this.year) {
      this.errors.moved = ERRORS['form.userAddress.movedMonthMovedYear.isBlank'];
      isValid = false;
    } else if (!this.month) {
      this.errors.moved = ERRORS['form.userAddress.movedMonth.isBlank'];
      isValid = false;
    } else if (!this.year) {
      this.errors.moved = ERRORS['form.userAddress.movedYear.isBlank'];
      isValid = false;
    }

    return isValid;
  }

  convertDateForServer(month, year) {
    if (month && year) {
      return moment(month + year, 'MMMMYYYY').format(DATE_FORMAT_SERVER);
    }
  }
}
