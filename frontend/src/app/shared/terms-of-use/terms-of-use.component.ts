import { Component, OnInit } from '@angular/core';
import {SpinnerService} from '../spinner/spinner.service';

@Component({
  selector: 'app-terms-of-use',
  templateUrl: './terms-of-use.component.html',
  styleUrls: ['./terms-of-use.component.scss']
})
export class TermsOfUseComponent implements OnInit {

  constructor(private spinner:SpinnerService) { }

  ngOnInit() {
    this.spinner.stop();
  }

}
