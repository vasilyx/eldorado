import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterInput'
})
export class FilterInputPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    let newValue = value.replace(/\,|£/g,'');
    /*if(newValue.match( /\./ig ) && newValue.match( /\./ig ).length > 1) {
      return newValue.slice(0, newValue.lastIndexOf('.') - 1);
    }*/
    return newValue ? newValue : 0;
  }

}
