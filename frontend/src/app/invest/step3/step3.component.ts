import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { NavigationService } from '../../shared/navigation/navigation.service';
import { ApiService } from '../../service/api.service';
import { StorageService } from '../../service/storage.service';
import { INCOME_SOURCES, PORTAL_PREFIX_PATH, API, ERRORS } from "../../config";
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';
import {InvestModalComponent} from '../../shared/invest-modal/invest-modal.component';

@Component({
  selector: 'invest-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss']
})
export class InvestStep3Component implements OnInit {

  step:number = 2;
  dialogRef:MdDialogRef<any>;
  model:any;
  bankDetails:any = {};
  errors:any = {};
  slider:any;
  options:Array<string> = INCOME_SOURCES;
  prefixPath:string;

  constructor(public spinner:SpinnerService,
              private router:Router,
              public navigationService:NavigationService,
              private apiService:ApiService,
              private storageService:StorageService,
              public dialog:MdDialog,
              public viewContainerRef:ViewContainerRef) {
    this.model = {
      total_gross_annual: 5000,
      main_source: ''
    };
    this.slider = {
      total_gross_annual: {
        min: 5000,
        max: 300000,
        step: 100
      }
    };
  }

  ngOnInit() {
    this.getPrefixPath();
    this.navigationService.setStep(this.step);
    if (this.storageService.read('token')) {
      this.getInvestorIncome();
      this.getBankDetails();
    } else {
      this.spinner.stop();
    }
  }

  onSubmit() {
    if (this.validation()) {
      let data = (<any>Object).assign({}, this.model);
      this.apiService.postMethod(API.QUOTE_INCOME, data).subscribe(
        response => {
          let data = (<any>Object).assign({}, this.bankDetails);
          this.apiService.postMethod(API.BANK_DETAILS, data).subscribe(
            response => {
              this.openConfirmDialog(this.prefixPath);
            }
          );
        }
      );
    }
  }

  back() {
    this.router.navigate([`${this.prefixPath}/invest/contact_information`]);
  }

  getInvestorIncome() {
    this.apiService.getMethod(API.QUOTE_INCOME).subscribe(
      response => {
        if (Object.keys(response.content).length) {
          this.model = response.content;
        }
      }
    );
  }

  getBankDetails() {
    this.apiService.getMethod(API.BANK_DETAILS).subscribe(
      response => {
        this.bankDetails = response.content;
      }
    );
  }


  validation() {
    let isValid = true;

    if (!this.model.main_source) {
      this.errors.mainIncomeSource = ERRORS['userIncome.mainIncomeSource.isBlank'];
      isValid = false;
    }

    if (!this.bankDetails.bank_sort_code) {
      this.errors.bankSortCode = ERRORS['userIncome.bankSortCode.isBlank'];
      isValid = false;
    }

    if (!this.bankDetails.bank_account_number) {
      this.errors.bankAccountNumber = ERRORS['userIncome.bankAccountNumber.isBlank'];
      isValid = false;
    }

    return isValid;
  }

  getPrefixPath() {
    this.prefixPath = this.router.url.indexOf(PORTAL_PREFIX_PATH) !== -1 ? PORTAL_PREFIX_PATH : '';
  }

  openConfirmDialog(prefixPath) {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;
    config.width = '480px';

    this.dialogRef = this.dialog.open(InvestModalComponent, config);

    this.dialogRef.afterClosed().subscribe((data) => {
      this.dialogRef = null;
      if (data) {
        if (prefixPath) {
          this.router.navigate([`${prefixPath}/create_offer`])
        } else {
          this.router.navigate(['/login']);
        }
      }
    });
  }

  getTotalGrossAnnual() {
    this.model.total_gross_annual = 100 * Math.ceil(this.model.total_gross_annual / 100);
    if(this.model.total_gross_annual < this.slider.total_gross_annual.min) {
      this.model.total_gross_annual = this.slider.total_gross_annual.min;
    }
    if(this.model.total_gross_annual > this.slider.total_gross_annual.max) {
      this.model.total_gross_annual = this.slider.total_gross_annual.max;
    }
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
