import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { NavigationService } from '../../shared/navigation/navigation.service';
import { PORTAL_PREFIX_PATH, API, WORLDPAY_MODE, PAYMENT_REDIRECT_URL } from '../../config';
import { ApiService } from "../../service/api.service";

@Component({
  selector: 'invest-step6',
  templateUrl: './step6.component.html',
  styleUrls: ['./step6.component.scss']
})
export class InvestStep6Component implements OnInit {

  step:number = 6;
  offerId:number;
  model:any = {};
  worldpay:any = {};

  constructor(public spinner:SpinnerService,
              public router:Router,
              public route: ActivatedRoute,
              public navigationService:NavigationService,
              public apiService:ApiService) {}

  ngOnInit() {
    this.offerId = this.route.snapshot.params['offerId'];
    this.navigationService.setStep(this.step);
    this.getOffer();
  }

  getOffer() {
    if (this.offerId) {
      this.apiService.getMethod(API.INVEST_OFFER.replace(/(\{id\})/i, `/${this.offerId}`)).subscribe(
        response => {
          this.model.amount = response.content.investment_amount;
          this.model.process_id = response.content.process_id;
        }
      )
    }
  }

  worldPayAction() {
    if (this.model.amount) {
      let url = `?test_mode=${WORLDPAY_MODE['test']}&process_id=${this.model.process_id}&redirect_url=${encodeURIComponent(PAYMENT_REDIRECT_URL)}`;
      this.apiService.getMethod(API.WORLDPAY_PARAMS + url).subscribe(
        response => {
          this.worldpay = response.content;
          setTimeout(() => {
            document.forms['worldpay'].submit();
          }, 0);
        }
      )
    }
  }

  goToDashboard() {
    this.router.navigate([`${PORTAL_PREFIX_PATH}/dashboard`]);
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
