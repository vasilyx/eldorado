import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { NavigationService } from '../../shared/navigation/navigation.service';
import { PORTAL_PREFIX_PATH, API } from '../../config';
import { ApiService } from "../../service/api.service";
import {Subscription} from 'rxjs/Subscription';
import {DashboardService} from '../../service/dashboard.service';

@Component({
  selector: 'invest-step7',
  templateUrl: './step7.component.html',
  styleUrls: ['./step7.component.scss']
})
export class InvestStep7Component implements OnInit {

  model:any = {};
  clientSubscription:Subscription;

  constructor(public spinner:SpinnerService,
              public router:Router,
              public route:ActivatedRoute,
              public navigationService:NavigationService,
              public apiService: ApiService,
              private dashboardService: DashboardService) {
    this.model = {
      payment: {}
    }
  }

  ngOnInit() {
    this.spinner.stop();
    this.getPaymentResponse();
    this.clientSubscription = this.apiService.getMethod(API.GET_DASHBOARD).subscribe(res => {
      this.dashboardService.changeDashboardData(res.content);
    });
  }

  getPaymentResponse() {
    this.route.queryParams.subscribe(params => {
      if (params['cartId']) {
        this.apiService.getMethod(API.WORLDPAY_RESPONSE + `?cart_id=${params['cartId']}`).subscribe(
          response => {
            this.model.payment = response.content;
          }
        )
      }
    });
  }

  goToDashboard() {
    this.router.navigate([`${PORTAL_PREFIX_PATH}/dashboard`]);
  }

  ngOnDestroy() {
    this.spinner.start();
    this.clientSubscription.unsubscribe();
  }
}
