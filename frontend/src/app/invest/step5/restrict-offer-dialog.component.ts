import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';

@Component({
  selector: 'restrict-offer-dialog',
  templateUrl: './restrict-offer-dialog.component.html',
  styleUrls: ['./restrict-offer-dialog.component.scss']
})
export class RestrictOfferDialogComponent {

  restrictionList:Array<any> = [];
  restrictionsIDs:Array<number> = [];
  prioritiesIDs:Array<number> = [];

  constructor(public dialogRef:MdDialogRef<any>) {}

  onSubmit() {
    let data = {
      restrictionsIDs: this.restrictionsIDs,
      prioritiesIDs: this.prioritiesIDs
    };
    this.dialogRef.close(data);
  }

  checkRestriction(group, item) {
    if (item.restrictionChecked) {
      this.restrictionsIDs.push(item.id);
    } else {
      this.restrictionsIDs = this.restrictionsIDs.filter(id => id !== item.id);
    }
    if (group.restrictions.some(item => item.restrictionChecked === true)) {
      this.restrictionList.map(restriction => {
        if (restriction.code === group.code) {
          group.prioritiesDisabled = true;
        }
      });
    } else {
      this.restrictionList.map(restriction => {
        if (restriction.code === group.code) {
          group.prioritiesDisabled = false;
        }
      });
    }
  }

  checkPriority(group, item) {
    if (item.priorityChecked) {
      this.prioritiesIDs.push(item.id);
    } else {
      this.prioritiesIDs = this.prioritiesIDs.filter(id => id !== item.id);
    }
    if (group.restrictions.some(item => item.priorityChecked === true)) {
      this.restrictionList.map(restriction => {
        if (restriction.code === group.code) {
          group.restrictionsDisabled = true;
        }
      });
    } else {
      this.restrictionList.map(restriction => {
        if (restriction.code === group.code) {
          group.restrictionsDisabled = false;
        }
      });
    }
  }
}
