import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MdDialogRef } from '@angular/material';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { API, OFFER_TYPE_CODE, OFFER_FREQUENCY_CODE, OFFER_RATE_CODE, FINANCE_TYPE, INVEST_RATES, ERRORS } from '../../config';
import {DashboardService} from '../../service/dashboard.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'confirm-offer-dialog',
  templateUrl: './confirm-offer-dialog.component.html',
  styleUrls: ['./confirm-offer-dialog.component.scss']
})
export class ConfirmOfferDialogComponent {

  investment:any;
  model:any = {};
  restrictionsIDs:Array<number> = [];
  prioritiesIDs:Array<number> = [];
  errors:any = {};
  OFFER_FREQUENCY_CODE:any = OFFER_FREQUENCY_CODE;
  OFFER_RATE_CODE:any = OFFER_RATE_CODE;
  OFFER_TYPE_CODE:any = OFFER_TYPE_CODE;
  clientSubscription:Subscription;
  invest_rates: any = INVEST_RATES;

  constructor(public dialogRef:MdDialogRef<any>,
              private spinner:SpinnerService,
              private router:Router,
              private apiService:ApiService,
              private dashboardService: DashboardService) {}

  onSubmit() {
    if (this.validation()) {
      let data = (<any>Object).assign(this.model, this.investment);
      data.restrictions = [];
      this.restrictionsIDs.map(id => data.restrictions.push({'id': id}));
      data.priorities = [];
      this.prioritiesIDs.map(id => data.priorities.push({'id': id}));
      data.finance_type = FINANCE_TYPE;
      this.apiService.postMethod(API.INVEST_OFFER.replace(/(\{id\})/i, ''), data).subscribe(
        response => {
          if (response.content) {
            this.clientSubscription = this.apiService.getMethod(API.GET_DASHBOARD).subscribe(res => {
              this.dashboardService.changeDashboardData(res.content);
              this.dialogRef.close(response.content);
            });
          }
        }
      )
    }
  }

  validation() {
    let isValid = true;

    if (!this.model.investment_name) {
      this.errors.investment_name = ERRORS['investOffer.investmentOfferName.isBlank'];
      isValid = false;
    }

    return isValid;
  }

  ngOnDestroy() {
    this.clientSubscription.unsubscribe();
  }
}
