import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { NavigationService } from '../../shared/navigation/navigation.service';
import { ApiService } from '../../service/api.service';
import { API, PORTAL_PREFIX_PATH, INVEST_RATES, ERRORS } from "../../config";
import { ConfirmOfferDialogComponent } from "./confirm-offer-dialog.component";
import { RestrictOfferDialogComponent } from "./restrict-offer-dialog.component";

@Component({
  selector: 'invest-step5',
  templateUrl: './step5.component.html',
  styleUrls: ['./step5.component.scss']
})
export class InvestStep5Component implements OnInit {

  step:number = 5;
  dialogRef:MdDialogRef<any>;
  willGetBackAmount:any = {};
  investRates:any = {};
  investment:any = {};
  restrictionList:Array<any> = [];
  restrictionsIDs:Array<number> = [];
  prioritiesIDs:Array<number> = [];
  errors:any = {};
  skipOfferCreation:boolean = false;

  constructor(public spinner:SpinnerService,
              public router:Router,
              public navigationService:NavigationService,
              public apiService:ApiService,
              public dialog:MdDialog,
              public viewContainerRef:ViewContainerRef) {
  }

  ngOnInit() {
    this.navigationService.setStep(this.step);
    this.investRates = INVEST_RATES;
    this.getRestrictions();
  }

  getRestrictions() {
    this.apiService.getMethod(API.INVEST_OFFER_RESTRICTIONS).subscribe(
      response => {
        if (Object.keys(response.content).length) {
          this.restrictionList = response.content.restriction_groups;
        }
      }
    )
  }

  onSubmitOpenConfirmDialog() {
    if (this.skipOfferCreation) {
      this.router.navigate([`${PORTAL_PREFIX_PATH}/transfer_money`]);
    }
    if (this.validation()) {
      this.openRestrictDialog();
    }
  }

  openConfirmDialog() {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;
    config.width = '1200px';

    this.dialogRef = this.dialog.open(ConfirmOfferDialogComponent, config);

    this.dialogRef.componentInstance.investment = this.investment;
    this.dialogRef.componentInstance.restrictionsIDs = this.restrictionsIDs;
    this.dialogRef.componentInstance.prioritiesIDs = this.prioritiesIDs;

    this.dialogRef.afterClosed().subscribe((data) => {
      this.dialogRef = null;
      if (data) {
        this.router.navigate([`${PORTAL_PREFIX_PATH}/transfer_money/${data.id}`]);
      }
    });
  }

  openRestrictDialog() {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;
    config.width = '560px';
    config.height = '560px';

    this.dialogRef = this.dialog.open(RestrictOfferDialogComponent, config);

    this.dialogRef.componentInstance.restrictionList = this.restrictionList;
    this.dialogRef.componentInstance.restrictionsIDs = this.restrictionsIDs;
    this.dialogRef.componentInstance.prioritiesIDs = this.prioritiesIDs;

    this.dialogRef.afterClosed().subscribe((data) => {
      this.dialogRef = null;
      if (data) {
        this.restrictionsIDs = data.restrictionsIDs;
        this.prioritiesIDs = data.prioritiesIDs;
        this.openConfirmDialog();
      }
    });
  }

  setInvestmentType(investment_type) {
    this.investment.investment_type = investment_type;
  }

  onChangeInvestAmount() {
    //you will get back = L*(1+r)n [where L = investment amount; r = annual interest rates; n = number of years].
    this.willGetBackAmount = {
      12: this.investment.investment_amount * Math.pow((1 + this.investRates[12] / 100),1),
      24: this.investment.investment_amount * Math.pow((1 + this.investRates[24] / 100),2),
      36: this.investment.investment_amount * Math.pow((1 + this.investRates[36] / 100),3),
      48: this.investment.investment_amount * Math.pow((1 + this.investRates[48] / 100),4),
      60: this.investment.investment_amount * Math.pow((1 + this.investRates[60] / 100),5)
    }
  }

  setInvestmentFrequency(investment_frequency) {
    if (this.InvestAmountValidation()) {
      this.investment.investment_frequency = investment_frequency;
    }
  }

  setRate(rate) {
    if (this.investment.rate !== rate) {
      delete this.investment.term;
    }
    this.investment.rate = rate;
  }

  setTerm(term) {
    this.investment.term = term;
  }

  InvestAmountValidation() {
    let isValid = true;

    if (!this.investment.investment_amount) {
      this.errors.investmentAmount = ERRORS['investOffer.investmentAmount.isBlank'];
      isValid = false;
    } else if (!/^\d.+$/.test(this.investment.investment_amount)) {
      this.errors.investmentAmount = ERRORS['investOffer.investmentAmount.wrong'];
      isValid = false;
    } else if (this.investment.investment_amount > 2000) {
      this.errors.investmentAmount = ERRORS['investOffer.investmentAmount.notInRange'];
      isValid = false;
    } else if (this.investment.investment_amount % 20 !== 0) {
      this.errors.investmentAmount = ERRORS['investOffer.investmentAmount.notMultipleBy20'];
      isValid = false;
    }

    return isValid;
  }

  validation() {
    let isValid = true;

    if (!this.investment.investment_type) {
      isValid = false;
    }

    if (!this.investment.investment_amount) {
      this.errors.investmentAmount = ERRORS['investOffer.investmentAmount.isBlank'];
      isValid = false;
    } else if (!/^\d.+$/.test(this.investment.investment_amount)) {
      this.errors.investmentAmount = ERRORS['investOffer.investmentAmount.wrong'];
      isValid = false;
    } else if (this.investment.investment_amount > 2000) {
      this.errors.investmentAmount = ERRORS['investOffer.investmentAmount.notInRange'];
      isValid = false;
    }

    if (!this.investment.investment_frequency) {
      isValid = false;
    }

    if (!this.investment.rate) {
      isValid = false;
    }

    if (!this.investment.term) {
      isValid = false;
    }

    return isValid;
  }

  back() {
    this.router.navigate([`${PORTAL_PREFIX_PATH}/invest/quote_income`]);
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
