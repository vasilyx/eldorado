import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { PATTERN_EMAIL, PATTERN_PASSWORD, PATTERN_STRING, DATE_FORMAT_SERVER, CONFIRMATION_URL, ERRORS, API, DATE_FORMAT_CLIENT, DATE_FORMAT_MASK } from '../../config';
import { ApiService } from '../../service/api.service';
import { NavigationService } from '../../shared/navigation/navigation.service';

@Component({
  selector: 'invest-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})

export class InvestRegistrationComponent implements OnInit {

  step:number = 0;
  model:any = {};
  errors:any = {};
  birthdateObj:any = {};
  mask:Array<any> = DATE_FORMAT_MASK;

  constructor(private spinner:SpinnerService,
              private router:Router,
              private apiService:ApiService,
              private navigationService:NavigationService,
              private datepickerConfig:NgbDatepickerConfig) {
    datepickerConfig.minDate = {year: 1900, month: 1, day: 1};
    datepickerConfig.maxDate = {year: moment().format('YYYY'), month: moment().format('M'), day: moment().format('D')};
  }

  ngOnInit() {
    this.spinner.stop();
    this.navigationService.setStep(this.step);
  }

  onSubmit() {
    if (this.validation()) {
      let data = (<any>Object).assign({}, this.model);
      data.birthdate = this.convertDate(this.model.birthdate);
      data.confirm_url = CONFIRMATION_URL;
      this.apiService.postMethod(API.INVEST_REGISTRATION, data, {saveToken: true}).subscribe(
        response => {
          this.router.navigate(['/invest/contact_information']);
        },
        err => {
          if (err.status === 'error') {
            for (var key in err.content) {
              this.errors[key] = err.content[key].code;
            }
          }
        }
      );
    }
  }

  onChangeDateInput() {
    if (moment(this.model.birthdate, 'DD/MM/YYYY', true).isValid()) {
      this.birthdateObj = {
        year: +moment(this.model.birthdate, 'DD/MM/YYYY').format('YYYY'),
        month: +moment(this.model.birthdate, 'DD/MM/YYYY').format('M'),
        day: +moment(this.model.birthdate, 'DD/MM/YYYY').format('D')
      };
    }
  }

  onChangeDateCalendar() {
    if (this.birthdateObj) {
      this.model.birthdate = `${moment(this.birthdateObj.day, 'D').format('DD')}/${moment(this.birthdateObj.month, 'M').format('MM')}/${this.birthdateObj.year}`;
    }
  }

  validation() {
    let isValid = true;

    if (!this.model.firstname) {
      this.errors.firstname = ERRORS['form.firstname.isBlank'];
      isValid = false;
    } else if (PATTERN_STRING instanceof RegExp && !PATTERN_STRING.test(this.model.firstname)) {
      this.errors.firstname = ERRORS['form.firstname.notString'] || 'form.firstname.notString';
      isValid = false;
    }

    if (!this.model.lastname) {
      this.errors.lastname = ERRORS['form.lastname.isBlank'];
      isValid = false;
    } else if (PATTERN_STRING instanceof RegExp && !PATTERN_STRING.test(this.model.lastname)) {
      this.errors.lastname = ERRORS['form.lastname.notString'] || 'form.lastname.notString';
      isValid = false;
    }

    if (!this.model.birthdate) {
      this.errors.birthdate = ERRORS['form.birthdate.isBlank'];
      isValid = false;
    } else if (!moment(this.model.birthdate, DATE_FORMAT_CLIENT, true).isValid()) {
      this.errors.birthdate = ERRORS['form.birthdate.wrong'];
      isValid = false;
    } else if (moment().diff(moment(this.model.birthdate, DATE_FORMAT_CLIENT), 'years') < 18) {
      this.errors.birthdate = ERRORS['form.birthdate.wrong'];
      isValid = false;
    }

    if (!this.model.username) {
      this.errors.username = ERRORS['form.username.isBlank'];
      isValid = false;
    } else if (PATTERN_EMAIL instanceof RegExp && !PATTERN_EMAIL.test(this.model.username)) {
      this.errors.username = ERRORS['form.username.wrongEmail'];
      isValid = false;
    }

    if (!this.model.password) {
      this.errors.password = ERRORS['form.password.isBlank'];
      isValid = false;
    } else if (this.model.password.length < 8) {
      this.errors.password = ERRORS['form.password.wrong'];
      isValid = false;
    } else if (PATTERN_PASSWORD instanceof RegExp && !PATTERN_PASSWORD.test(this.model.password)) {
      this.errors.password = ERRORS['form.password.wrong'];
      isValid = false;
    }

    if (!this.model.plain_password) {
      this.errors.plainPassword = ERRORS['form.password.isBlank'];
      isValid = false;
    } else if (this.model.plain_password.length < 8) {
      this.errors.plainPassword = ERRORS['form.password.wrong'];
      isValid = false;
    } else if (PATTERN_PASSWORD instanceof RegExp && !PATTERN_PASSWORD.test(this.model.plain_password)) {
      this.errors.plainPassword = ERRORS['form.password.wrong'];
      isValid = false;
    } else if (this.model.password && this.model.password !== this.model.plain_password) {
      this.errors.plainPassword = ERRORS['form.password.notConfirmed'];
      isValid = false;
    }

    return isValid;
  }

  convertDate(date) {
    if (date) {
      return moment(date, DATE_FORMAT_CLIENT).format(DATE_FORMAT_SERVER);
    }
  }

  formatDate(date) {
    return date ? `${date.day}/${date.month}/${date.year}` : '';
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
