import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { NavigationService } from '../../shared/navigation/navigation.service';
import { AutocompleteService } from '../../shared/autocomplete/autocomplete.service';
import { ApiService } from '../../service/api.service';
import { TITLES, CONTACT_NUMBER_CODE, DATE_FORMAT_SERVER, PORTAL_PREFIX_PATH, API, ERRORS } from '../../config';
import { AddressDialogComponent } from "../../shared/address-dialog/address-dialog.component";
import { VerificationDialogComponent } from "../../shared/verification-dialog/verification-dialog.component";

@Component({
  selector: 'invest-step2-dashboard',
  templateUrl: './step2-dashboard.component.html',
  styleUrls: ['./step2-dashboard.component.scss']
})
export class InvestStep2DashboardComponent implements OnInit {

  dialogRef:MdDialogRef<any>;
  step:number = 1;
  address:any;
  searchStr:string;
  moved:string;
  month:string;
  months:any = [];
  year:string;
  years:any = [];
  options:Array<string> = TITLES;
  model:any = {};
  popup:any;
  errors:any = {};
  hasCurrentAddress:boolean = false;
  displayedAddress:string;
  displayedMoved:string;
  prefixPath:string;
  showMobileVerificationButton:boolean;
  comms:boolean = false;

  constructor(private spinner:SpinnerService,
              private router:Router,
              private navigationService:NavigationService,
              private apiService:ApiService,
              private autocompleteService:AutocompleteService,
              private dialog:MdDialog,
              private viewContainerRef:ViewContainerRef) {
    this.popup = {
      errors: {}
    };
  }

  ngOnInit() {
    this.getPrefixPath();
    this.getCurrentAddress();
    this.getInvestorContacts();
    this.getUserDetails();
    this.getPreferences();
    this.generateDates();
    this.navigationService.setStep(this.step);
    this.autocompleteService.address.subscribe(address => this.address = address);
  }

  onSubmit() {
    if (this.validation()) {
      if (this.hasCurrentAddress) {
        this.addInvestorContacts();
      } else {
        let data = (<any>Object).assign({}, this.address);
        data['moved'] = this.convertDateForServer(this.month, this.year);
        this.apiService.postMethod(API.ADDRESS, data).subscribe(
          response => {
            this.addInvestorContacts();
          }
        );
      }
    }
  }

  showVerificationPopup() {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.width = "480px";

    this.dialogRef = this.dialog.open(VerificationDialogComponent, config);

    this.dialogRef.componentInstance.mobile_phone = this.model.mobile_phone;

    this.dialogRef.afterClosed().subscribe((moveOn) => {
      this.dialogRef = null;
      if (moveOn) {
        this.router.navigate([`${this.prefixPath}/invest/quote_income`]);
      }
    });
  }

  getCurrentAddress() {
    this.apiService.getMethod(API.GET_CURRENT_ADDRESS).subscribe(
      response => {
        if (response.id) {
          let currentAddress = response;
          this.displayedAddress = `${currentAddress.street} ${currentAddress.building_number}, ${currentAddress.city}, ${currentAddress.country}`;
          this.displayedMoved = this.convertDateForDisplay(currentAddress.moved);
          this.hasCurrentAddress = true;
        }
      }
    )
  }

  getInvestorContacts() {
    this.apiService.getMethod(API.GET_INVESTOR_CONTACTS).subscribe(
      response => {
        let content = response.content;
        if (content.length) {
          for (let item in content) {
            if (content[item].type === CONTACT_NUMBER_CODE.MOBILE) {
              this.model.mobile_phone = content[item].value;
              this.showMobileVerificationButton = this.prefixPath ? !content[item].verified : undefined;
            } else if (content[item].type === CONTACT_NUMBER_CODE.HOME) {
              this.model.home_phone = content[item].value;
            } else if (content[item].type === CONTACT_NUMBER_CODE.WORK) {
              this.model.work_phone = content[item].value;
            }
          }
        }
      }
    );
  }

  getUserDetails() {
    this.apiService.getMethod(API.GET_USER_DETAILS).subscribe(
      response => {
        let content = response.content;
        if (content) {
          this.model.title = content.title;
          this.model.screen_name = content.screen_name;
        }
      }
    )
  }

  getPreferences() {
    this.apiService.getMethod(API.PREFERENCES).subscribe(
        response => {
          this.comms = response.content.receive_comms;
        }
    )
  }

  addInvestorContacts() {
    let data = {
      communications: {},
      title: this.model.title,
      screen_name: this.model.screen_name
    };
    data.communications[CONTACT_NUMBER_CODE.MOBILE] = this.model.mobile_phone;
    data.communications[CONTACT_NUMBER_CODE.HOME] = this.model.home_phone;
    data.communications[CONTACT_NUMBER_CODE.WORK] = this.model.work_phone;
    this.apiService.postMethod(API.ADD_INVESTOR_CONTACTS, data).subscribe(
      response => {
        if (!this.prefixPath) {
          this.router.navigate(['/invest/quote_income']);
        } else {
          this.checkMobileVerification(response.content);
          if (!this.showMobileVerificationButton) {
            this.router.navigate([`${this.prefixPath}/invest/quote_income`]);
          }
        }
      },
      err => {
        if (err.status === 'error') {
          for (var key in err.content) {
            this.errors[key] = err.content[key].code;
          }
        }
      }
    );
  }

  checkMobileVerification(content) {
    if (content.length) {
      for (let item in content) {
        if (content[item].type === CONTACT_NUMBER_CODE.MOBILE) {
          this.showMobileVerificationButton = this.prefixPath ? !content[item].verified : undefined;
        }
      }
    }
  }

  addAddress() {
    this.autocompleteService.setAddress(undefined);
    this.searchStr = '';
    this.moved = '';
    this.errors = {};

    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;
    config.width = "480px";

    this.dialogRef = this.dialog.open(AddressDialogComponent, config);

    this.dialogRef.componentInstance.searchStr = this.searchStr;
    this.dialogRef.componentInstance.movedmonth = this.moved;

    this.dialogRef.afterClosed().subscribe((needRefetch) => {
      this.searchStr = '';
      this.moved = '';
      this.errors = {};
      this.dialogRef = null;
      if (needRefetch) {
        this.getCurrentAddress();
      }
    });
  }

  validation() {
    let isValid = true;

    if (!this.hasCurrentAddress) {
      if (!this.address) {
        this.errors.address = ERRORS['form.userAddress.isBlank'];
        isValid = false;
      }

      if (!this.month && !this.year) {
        this.errors.moved = ERRORS['form.userAddress.movedMonthMovedYear.isBlank'];
        isValid = false;
      } else if (!this.month) {
        this.errors.moved = ERRORS['form.userAddress.movedMonth.isBlank'];
        isValid = false;
      } else if (!this.year) {
        this.errors.moved = ERRORS['form.userAddress.movedYear.isBlank'];
        isValid = false;
      }
    }

    if (!this.model.mobile_phone) {
      this.errors.mobilePhone = ERRORS['form.userAddress.mobilePoneNumber.isBlank'];
      isValid = false;
    }

    if (!this.model.home_phone) {
      this.errors.homePhone = ERRORS['form.userAddress.homePoneNumber.isBlank'];
      isValid = false;
    }

    if (!this.model.work_phone) {
      this.errors.workPhone = ERRORS['form.userAddress.workPoneNumber.isBlank'];
      isValid = false;
    }

    if (!this.model.title) {
      this.errors.title = ERRORS['form.userAddress.title.isBlank'];
      isValid = false;
    }

    if (!this.model.screen_name) {
      this.errors.screenName = ERRORS['form.userAddress.screenName.isBlank'];
      isValid = false;
    }

    return isValid;
  }

  convertDateForDisplay(date) {
    if (date) {
      return moment(date, DATE_FORMAT_SERVER).format('MMM, YYYY');
    }
  }

  convertDateForServer(month, year) {
    if (month && year) {
      return moment(month + year, 'MMMMYYYY').format(DATE_FORMAT_SERVER);
    }
  }

  generateDates() {
    this.months = moment.months();
    for (let i = parseInt(moment().format('YYYY')); i >= 1970; i--) {
      this.years.push(i.toString());
    }
    if (this.moved) {
      this.month = moment(this.moved, DATE_FORMAT_SERVER).format('MMMM');
      this.year = moment(this.moved, DATE_FORMAT_SERVER).format('YYYY');
    }
  }

  getPrefixPath() {
    this.prefixPath = this.router.url.indexOf(PORTAL_PREFIX_PATH) !== -1 ? PORTAL_PREFIX_PATH : '';
  }

  check(month, year) {
    let newMonth =  parseInt(moment(month, 'MMMM').format('MM'));
    if(year == (new Date()).getFullYear() && newMonth > (new Date()).getMonth()+1 ) {
      this.errors.moved = 'You cannot use a date in the future';
    }
    else {
      this.errors.moved = '';
    }
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
