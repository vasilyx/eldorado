import { Component, OnInit } from '@angular/core';
import { SpinnerService } from '../shared/spinner/spinner.service';

@Component({
  selector: 'invest',
  templateUrl: './invest.component.html',
  styleUrls: ['./invest.component.scss']
})
export class InvestComponent implements OnInit {

  constructor(public spinner:SpinnerService) {}

  ngOnInit() {
    this.spinner.stop();
  }

  ngOnDestroy() {
    this.spinner.start();
  }

}
