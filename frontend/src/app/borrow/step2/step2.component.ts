import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { NavigationService } from '../../shared/navigation/navigation.service';
import { PORTAL_PREFIX_PATH, API, QUOTE_STATUS_CODE } from '../../config';

@Component({
  selector: 'borrow-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss']
})
export class BorrowStep2Component implements OnInit {

  step:number = 1;
  model:any = {};
  props:any = {};
  slider:any = {};
  prefixPath:string;

  constructor(private spinner:SpinnerService,
              private router:Router,
              private apiService:ApiService,
              private navigationService:NavigationService) {
    this.model = {
      amount: 1000,
      term: 12
    };
    this.slider = {
      amount: {
        min: 1000,
        max: 25000,
        step: 100
      },
      term: {
        min: 12,
        max: 60,
        step: 12
      }
    };
  }

  ngOnInit() {
    this.getPrefixPath();
    this.apiService.getMethod(API.GET_DASHBOARD).subscribe(
      response => {
        if(QUOTE_STATUS_CODE[response.content.quotes[0].quote_status] != 'Draft') {
          if(this.prefixPath) {
            this.router.navigate([`${this.prefixPath}/dashboard`]);
          }
          else {
            this.router.navigate([`/login`]);
          }
        }
      }
    );
    this.navigationService.setStep(this.step);
    this.apiService.getMethod(API.QUOTE_TERM).subscribe(
      response => {
        if (Object.keys(response.content).length) {
          this.model = response.content;
          this.getBaseLoanProperties();
        }
      }
    );
  }

  onSubmit() {
    let data = (<any>Object).assign({}, this.model);
    this.apiService.postMethod(API.ADD_QUOTE_TERM, data).subscribe(
      response => {
        this.router.navigate([`${this.prefixPath}/borrow/quote_income`]);
      }
    );
  }

  getBaseLoanProperties() {
    this.model.amount = this.roundTo100(this.model.amount);
    if(this.model.amount < this.slider.amount.min) {
      this.model.amount = this.slider.amount.min;
    }
    if(this.model.amount > this.slider.amount.max) {
      this.model.amount = this.slider.amount.max;
    }
    let url = `${API.BASE_LOAN_PROPERTIES}?loan_amount=${this.model.amount}&loan_term=${this.model.term}`;
    this.apiService.getMethod(url, {hideLoader: true}).subscribe(
      response => {
        if (Object.keys(response.content).length) {
          this.props = response.content;
        }
      }
    );
  }

  roundTo100(value) {
    return 100 * Math.ceil(value / 100);
  }

  getPrefixPath() {
    this.prefixPath = this.router.url.indexOf(PORTAL_PREFIX_PATH) !== -1 ? PORTAL_PREFIX_PATH : '';
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
