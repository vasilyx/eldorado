import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import { SpinnerService } from '../shared/spinner/spinner.service';

@Component({
  selector: 'borrow',
  templateUrl: './borrow.component.html',
  styleUrls: ['./borrow.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BorrowComponent implements OnInit {

  constructor(public spinner:SpinnerService) {}

  ngOnInit() {
    this.spinner.stop();
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
