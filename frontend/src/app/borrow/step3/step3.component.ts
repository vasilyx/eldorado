import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { StorageService } from '../../service/storage.service';
import { NavigationService } from '../../shared/navigation/navigation.service';
import {INCOME_SOURCES, PORTAL_PREFIX_PATH, API, QUOTE_STATUS_CODE, ERRORS} from '../../config'

@Component({
  selector: 'borrow-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss']
})
export class BorrowStep3Component implements OnInit {

  step:number = 2;
  model:any = {};
  slider:any = {};
  errors:any = {};
  options:Array<string> = INCOME_SOURCES;
  prefixPath:string;

  constructor(public spinner:SpinnerService,
              private router:Router,
              private apiService:ApiService,
              private storageService:StorageService,
              public navigationService:NavigationService) {
    this.model = {
      total_gross_annual: 5000,
      main_source: '',
      is_homeowner: false,
      has_c_c_j: false,
      has_current_default: false,
      rate: 30
    };
    this.slider = {
      total_gross_annual: {
        min: 5000,
        max: 300000,
        step: 100
      },
      rate: {
        min: 10,
        max: 40,
        step: 10
      }
    };
  }

  onSubmit() {
    if (this.validation()) {
      this.apiService.postMethod(API.QUOTE_INCOME, this.model).subscribe(
        response => {
          this.router.navigate([`${this.prefixPath}/borrow/contact_information`]);
        }
      );
    }
  }

  back() {
    this.router.navigate([`${this.prefixPath}/borrow/quote_term`]);
  }

  ngOnInit() {
    this.getPrefixPath();
    this.apiService.getMethod(API.GET_DASHBOARD).subscribe(
      response => {
        if(QUOTE_STATUS_CODE[response.content.quotes[0].quote_status] != 'Draft') {
          if(this.prefixPath) {
            this.router.navigate([`${this.prefixPath}/dashboard`]);
          }
          else {
            this.router.navigate([`/login`]);
          }
        }
      }
    );
    this.navigationService.setStep(this.step);
    if (this.storageService.read('token')) {
      this.apiService.getMethod(API.QUOTE_INCOME).subscribe(
        response => {
          if (Object.keys(response.content).length) {
            this.model = response.content;
          }
        }
      );
    } else {
      this.spinner.stop();
    }
  }

  getPrefixPath() {
    this.prefixPath = this.router.url.indexOf(PORTAL_PREFIX_PATH) !== -1 ? PORTAL_PREFIX_PATH : '';
  }

  validation() {
    let isValid = true;

    if (!this.model.main_source) {
      this.errors.mainSource = ERRORS['userIncome.mainIncomeSource.isBlank'];
      isValid = false;
    }

    return isValid;
  }

  getTotalGrossAnnual() {
    this.model.total_gross_annual = 100 * Math.ceil(this.model.total_gross_annual / 100);
    if(this.model.total_gross_annual < this.slider.total_gross_annual.min) {
      this.model.total_gross_annual = this.slider.total_gross_annual.min;
    }
    if(this.model.total_gross_annual > this.slider.total_gross_annual.max) {
      this.model.total_gross_annual = this.slider.total_gross_annual.max;
    }
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
