import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { NavigationService } from '../../shared/navigation/navigation.service';
import { ApiService } from '../../service/api.service';
import { AutocompleteService } from '../../shared/autocomplete/autocomplete.service';
import {DATE_FORMAT_SERVER, PORTAL_PREFIX_PATH, API, ERRORS, QUOTE_STATUS_CODE} from '../../config';
import { AddressDialogComponent } from "../../shared/address-dialog/address-dialog.component";
import { BorrowerModalComponent} from '../../shared/borrower-modal/borrower-modal.component';

@Component({
  selector: 'borrow-step5',
  templateUrl: './step5.component.html',
  styleUrls: ['./step5.component.scss']
})
export class BorrowStep5Component implements OnInit {

  dialogRef:MdDialogRef<any>;
  step:number = 3;
  index:number = -1;
  address:any;
  searchStr:string;
  moved:string;
  month:string;
  months:any = [];
  year:string;
  years:any = [];
  addresses:Array<any> = [];
  shortAddressPeriod:boolean = true;
  errors:any = {};
  prefixPath:string;
  comms:boolean = false;
  terms:boolean = false;

  constructor(private spinner:SpinnerService,
              private router:Router,
              private navigationService:NavigationService,
              private apiService:ApiService,
              private autocompleteService:AutocompleteService,
              private dialog:MdDialog,
              private viewContainerRef:ViewContainerRef) {
  }

  ngOnInit() {
    this.navigationService.setStep(this.step);
    this.getPrefixPath();
    this.apiService.getMethod(API.GET_DASHBOARD).subscribe(
      response => {
        if(QUOTE_STATUS_CODE[response.content.quotes[0].quote_status] != 'Draft') {
          this.router.navigate([`${this.prefixPath}/login`]);
        }
      }
    );
    this.getAddresses();
    this.getPreferences();
    this.generateDates();
    this.autocompleteService.address.subscribe(address => this.address = address);
  }

  openDialog() {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;
    config.width = "480px";

    this.dialogRef = this.dialog.open(AddressDialogComponent, config);

    this.dialogRef.componentInstance.index = this.index;
    this.dialogRef.componentInstance.searchStr = this.searchStr;
    this.dialogRef.componentInstance.moved = this.moved;

    this.dialogRef.afterClosed().subscribe(needRefetch => {
      this.index = -1;
      this.searchStr = '';
      this.moved = '';
      this.errors = {};
      this.dialogRef = null;
      if (needRefetch) {
        this.getAddresses();
      }
    });
  }

  openAddressModal() {
    this.autocompleteService.setAddress(undefined);
    this.index = -1;
    this.searchStr = '';
    this.moved = '';
    this.openDialog();
  }

  addFirstAddress() {
    if (!this.validation()) {
      return false;
    }

    let data = (<any>Object).assign({}, this.address);
    data.moved = this.convertDateForServer(this.month, this.year);
    this.apiService.postMethod(API.ADDRESS, data).subscribe(
      response => {
        this.getAddresses();
      }
    );
  }

  editAddress(address) {
    this.index = address.id;
    this.autocompleteService.setAddress(address);
    this.searchStr = `${address.street} ${address.building_number}, ${address.city}, ${address.country}`;
    this.moved = address.moved;
    this.openDialog();
  }

  onSubmit() {
    if (this.shortPeriodValidation()) {
      this.apiService.getMethod(API.APPLY_QUOTE).subscribe(
        response => {
          this.openConfirmDialog(this.prefixPath);
        }
      );
    }
  }

  getAddresses() {
    this.apiService.getMethod(API.ADDRESS).subscribe(
      response => {
        this.addresses = response;
        this.dateHandler(this.addresses);
      }
    )
  }

  getPreferences() {
    this.apiService.getMethod(API.PREFERENCES).subscribe(
      response => {
        this.comms = response.content.receive_comms;
      }
    )
  }

  convertDateForDisplay(date) {
    if (date) {
      return moment(date, DATE_FORMAT_SERVER).format('MMM, YYYY');
    }
  }

  convertDateForServer(month, year) {
    if (month && year) {
      return moment(month + year, 'MMMMYYYY').format(DATE_FORMAT_SERVER);
    }
  }

  validation() {
    let isValid = true;

    if (!this.searchStr && !this.address) {
      this.errors.address = ERRORS['form.userAddress.isBlank'];
      isValid = false;
    }

    if (!this.month && !this.year) {
      this.errors.moved = ERRORS['form.userAddress.movedMonthMovedYear.isBlank'];
      isValid = false;
    } else if (!this.month) {
      this.errors.moved = ERRORS['form.userAddress.movedMonth.isBlank'];
      isValid = false;
    } else if (!this.year) {
      this.errors.moved = ERRORS['form.userAddress.movedYear.isBlank'];
      isValid = false;
    }

    return isValid;
  }

  shortPeriodValidation() {
    let isValid = true;

    if (this.addresses.length && this.shortAddressPeriod) {
      this.errors.movedPeriod = ERRORS['userAddress.moved.smallPeriod'];
      isValid = false;
    }

    return isValid;
  }

  setPreferences(value) {
    this.apiService.patchMethod(API.PREFERENCES, {receive_comms: value}, {hideLoader: true}).subscribe(
      response => {}
    )
  }

  dateHandler(addresses) {
    let minDate;
    if (addresses.length) {
      minDate = Math.min(...addresses.map(v => v.moved));
      this.shortAddressPeriod = moment().diff(moment(minDate, 'YYYYMMDD'), 'years') < 5;
    }
  }

  generateDates() {
    this.months = moment.months();
    for (let i = parseInt(moment().format('YYYY')); i >= 1970; i--) {
      this.years.push(i.toString());
    }
    if (this.moved) {
      this.month = moment(this.moved, DATE_FORMAT_SERVER).format('MMMM');
      this.year = moment(this.moved, DATE_FORMAT_SERVER).format('YYYY');
    }
  }

  getPrefixPath() {
    this.prefixPath = this.router.url.indexOf(PORTAL_PREFIX_PATH) !== -1 ? PORTAL_PREFIX_PATH : '';
  }

  back() {
    this.router.navigate([`${this.prefixPath}/borrow/quote_income`]);
  }

  openConfirmDialog(prefixPath) {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;
    config.width = '480px';

    this.dialogRef = this.dialog.open(BorrowerModalComponent, config);

    this.dialogRef.afterClosed().subscribe((data) => {
      this.dialogRef = null;
      if (data) {
        if (prefixPath) {
          this.router.navigate([`${prefixPath}/dashboard`])
        } else {
          this.router.navigate(['/login']);
        }
      }
    });
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
