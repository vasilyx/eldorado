import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <spinner-component></spinner-component>
    <simple-notifications></simple-notifications>
    <!-- Routed views go here -->
    <router-outlet></router-outlet>
  `
})

export class AppComponent { }
