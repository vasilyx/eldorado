import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { PATTERN_EMAIL, FORGOT_PASSWORD_URL, API, ERRORS } from '../../config';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  model:any = {};
  errors:any = {};
  successMessage:string;
  forgotPasswordForm: boolean;

  constructor(private spinner:SpinnerService,
              private apiService:ApiService,
              private router:Router,
              private route:ActivatedRoute) {}

  ngOnInit() {
    this.spinner.stop();
    this.route.queryParams.subscribe(params => {
      if (params['t']) {
        let data = {
          t: params['t']
        };
        this.apiService.postMethod(API.EMAIL_CONFIRMATION, data).subscribe(
          response => {}
        )
      }
    });
  }

  login() {
    if (this.validation()) {
      let data = (<any>Object).assign({}, this.model);
      this.apiService.postMethod(API.LOGIN, data, {saveToken: true}).subscribe(
        response => {
          this.router.navigate(['/portal/dashboard']);
        }
      );
    }
  }

  logout() {
    this.apiService.postMethod(API.LOGOUT, {}, {deleteToken: true}).subscribe(
      response => {}
    );
  }

  sendEmail() {
    if (this.validation(true)) {
      let data = {
        username: this.model.username,
        url: FORGOT_PASSWORD_URL
      };
      this.apiService.postMethod(API.FORGOT_PASSWORD, data).subscribe(
        response => {
          this.successMessage = 'Reset password link was sent to your email box. Please check it out.';
        }
      );
    }
  }

  validation(ignorePass?) {
    let isValid = true;

    if (!this.model.username) {
      this.errors.username = ERRORS['user.username.isBlank'];
      isValid = false;
    } else if (PATTERN_EMAIL instanceof RegExp && !PATTERN_EMAIL.test(this.model.username)) {
      this.errors.username = ERRORS['user.emailNotConfirmed'];
      isValid = false;
    }

    if (!ignorePass && !this.model.password) {
      this.errors.password = ERRORS['user.password.isBlank'];
      isValid = false;
    }

    return isValid;
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
