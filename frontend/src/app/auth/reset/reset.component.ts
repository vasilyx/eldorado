import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { PATTERN_PASSWORD, API, ERRORS } from '../../config';

@Component({
  selector: 'reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {

  model:any = {};
  errors:any = {};
  token:string;

  constructor(private spinner:SpinnerService,
              private apiService: ApiService,
              private router:Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.spinner.stop();
    this.route.queryParams.subscribe(params => {
      if (params['t']) {
        this.token = params['t'];
      }
    });
  }

  onSubmit() {
    if (this.validation()) {
      let data = (<any>Object).assign({}, this.model);
      data.t = this.token;
      this.apiService.postMethod(API.RESET_PASSWORD, data).subscribe(
        response => {
          this.router.navigate(['/login']);
        }
      )
    }
  }

  validation() {
    let isValid = true;

    if (!this.model.password) {
      this.errors.password = ERRORS['form.password.isBlank'];
      isValid = false;
    } else if (this.model.password.length < 8) {
      this.errors.password = ERRORS['form.password.short'];
      isValid = false;
    } else if (PATTERN_PASSWORD instanceof RegExp && !PATTERN_PASSWORD.test(this.model.password)) {
      this.errors.password = ERRORS['form.password.wrong'];
      isValid = false;
    }

    if (!this.model.plain_password) {
      this.errors.plainPassword = ERRORS['form.password.isBlank'];
      isValid = false;
    } else if (this.model.plain_password.length < 8) {
      this.errors.plainPassword = ERRORS['form.password.short'];
      isValid = false;
    } else if (PATTERN_PASSWORD instanceof RegExp && !PATTERN_PASSWORD.test(this.model.plain_password)) {
      this.errors.plainPassword = ERRORS['form.password.wrong'];
      isValid = false;
    } else if (this.model.password && this.model.password !== this.model.plain_password) {
      this.errors.plainPassword = ERRORS['form.password.not_matched'];
      isValid = false;
    }

    return isValid;
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
