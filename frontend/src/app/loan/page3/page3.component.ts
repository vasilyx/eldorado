import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { PORTAL_PREFIX_PATH, API } from "../../config";
import {PaddingZeroService} from '../../service/padding-zero.service';

@Component({
  selector: 'loan-page3',
  templateUrl: './page3.component.html',
  styleUrls: ['./page3.component.scss']
})
export class LoanPage3Component implements OnInit {

  model:any = {};
  prefixPath:string = PORTAL_PREFIX_PATH;
  quoteId: any;

  constructor(private spinner:SpinnerService,
              private router:Router,
              private route:ActivatedRoute,
              private apiService:ApiService,
              public paddingZeroService: PaddingZeroService) {}

  ngOnInit() {
    this.getQuoteUnderwritingDetails();
    this.apiService.getMethod(API.GET_DASHBOARD).subscribe(
      response => {
        this.quoteId = response.content.quotes[0].quote_id;
      }
    );
  }

  getQuoteUnderwritingDetails() {
    let id = +this.route.snapshot.params['id'];
    this.apiService.postMethod(API.GET_QUOTE_UNDERWRITING_DETAILS, {id}).subscribe(
        response => {
            this.model = response.content;
        }
    );
  }

  goToLoan() {
    this.router.navigate([`${this.prefixPath}/loan/personal_information`]);
  }

  ngOnDestroy() {
    this.spinner.start();
  }

  back() {
    this.router.navigate([`${this.prefixPath}/dashboard`]);
  }
}
