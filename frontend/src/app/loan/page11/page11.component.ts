import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { PORTAL_PREFIX_PATH, API } from '../../config';

@Component({
  selector: 'loan-page11',
  templateUrl: './page11.component.html',
  styleUrls: ['./page11.component.scss']
})
export class LoanPage11Component implements OnInit {

  constructor(private router:Router,
              private spinner:SpinnerService,
              private apiService:ApiService) {}

  ngOnInit() {
    this.spinner.stop();
  }

  goToDashboard() {
    this.router.navigate([`${PORTAL_PREFIX_PATH}/dashboard`]);
  }

  logout() {
    this.apiService.postMethod(API.LOGOUT).subscribe(
        response => {
          this.router.navigate(['/login']);
        }
    )
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
