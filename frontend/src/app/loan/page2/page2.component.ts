import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { PORTAL_PREFIX_PATH, API } from "../../config";
import { PaddingZeroService} from '../../service/padding-zero.service';

@Component({
  selector: 'loan-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.scss']
})
export class LoanPage2Component implements OnInit {

  public model:any = {};
  public user: any = {};
  private prefixPath:string = PORTAL_PREFIX_PATH;
  public quoteId: any;

  constructor(public spinner:SpinnerService,
              private router:Router,
              private route:ActivatedRoute,
              private apiService:ApiService,
              public paddingZeroService: PaddingZeroService) {}

  ngOnInit() {
    this.getQuoteUnderwritingDetails();
  }

  getQuoteUnderwritingDetails() {
    let id = this.quoteId = this.route.snapshot.params['id'];
    this.apiService.getMethod(API.GET_DASHBOARD).subscribe(
      response => {
        this.quoteId = response.content.quotes[0].quote_id;
      }
    );
    this.apiService.getMethod(API.GET_USER_DETAILS).subscribe(
      response => {
        this.user = response.content;
      }
    );
    this.apiService.postMethod(API.GET_QUOTE_UNDERWRITING_DETAILS, {id}).subscribe(
      response => {
        this.model = response.content;
      }
    );
  }

  goToLoan() {
    this.router.navigate([`${this.prefixPath}/loan/personal_information`]);
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
