import {Component, ElementRef, OnInit, ViewContainerRef} from '@angular/core';
import {API} from '../../config';
import {ApiService} from '../../service/api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MdDialog, MdDialogConfig, MdDialogRef} from '@angular/material';
import {LoanAgreementDialogComponent} from './loan-agreement-dialog/loan-agreement-dialog.component';
import {DashboardService} from '../../service/dashboard.service';

@Component({
  selector: 'app-loan-agreement',
  templateUrl: './loan-agreement.component.html',
  styleUrls: ['./loan-agreement.component.scss']
})
export class LoanAgreementComponent implements OnInit {
  dialogRef: MdDialogRef<any>;
  apiCallId: number;
  id = this.route.snapshot.params['id'];
  payment_day: number = 1;
  options = [{
    text: '1st',
    num: 1
  }, {
    text: '10th',
    num: 10
  }, {
    text: '15th',
    num: 15
  }, {
    text: '21th',
    num: 21
  }, {
    text: '26th',
    num: 26
  }
  ];
  htmlBlock: any;

  constructor(private apiService: ApiService,
              private elementRef: ElementRef,
              private viewContainerRef: ViewContainerRef,
              private dialog: MdDialog,
              private route: ActivatedRoute,
              private router: Router,
              private dashboardService: DashboardService) {
  }

  ngOnInit() {
    this.apiService.getMethod(API.BORROWER_LOAN_AGREEMENT.replace(/(\{quote_id\})/i, `${this.id}`)).map(res => res.content).subscribe((res) => {
      const checkHtml = this.elementRef.nativeElement.querySelector('.html');
      this.htmlBlock = res;
      if (this.htmlBlock) {
        checkHtml.insertAdjacentHTML('beforeend', this.htmlBlock);
      }
      else {
        checkHtml.insertAdjacentHTML('beforeend', 'Document is not ready');
      }
    });
  }

  openConfirmDialog(decision) {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;
    config.width = '210px';

    this.dialogRef = this.dialog.open(LoanAgreementDialogComponent, config);

    this.dialogRef.afterClosed().subscribe((data) => {
      this.dialogRef = null;
      if (data && data.confirm) {
        if (decision) {
          this.apiService.postMethod(API.BORROWER_CONFIRM_LOAN_AGRREMENT,
            {
              quote_id: this.id,
              payment_day: this.payment_day
            }).subscribe((res) => {
            this.apiService.getMethod(API.GET_DASHBOARD).subscribe(res => {
              this.dashboardService.changeDashboardData(res.content);
              this.router.navigate(['/portal/dashboard']);
            });
          });
        }
        else {
          this.apiService.postMethod(API.BORROWER_DECLINE_LOAN_AGRREMENT,
            {
              quote_id: this.id
            }).subscribe((res) => {
            this.apiService.getMethod(API.GET_DASHBOARD).subscribe(res => {
              this.dashboardService.changeDashboardData(res.content);
              this.router.navigate(['/portal/dashboard']);
            });
          });
        }
      }
    });
  }
}

