import { Component, OnInit } from '@angular/core';
import {LoanAgreementComponent} from '../loan-agreement.component';
import {MdDialogRef} from '@angular/material';

@Component({
  selector: 'app-loan-agreement-dialog',
  templateUrl: './loan-agreement-dialog.component.html',
  styleUrls: ['./loan-agreement-dialog.component.scss']
})
export class LoanAgreementDialogComponent implements OnInit {

  constructor(public dialogRef:MdDialogRef<LoanAgreementComponent>) { }

  ngOnInit() {
  }

}
