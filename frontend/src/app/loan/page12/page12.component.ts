import { Component, OnInit } from '@angular/core';

import { SpinnerService } from '../../shared/spinner/spinner.service';

@Component({
  selector: 'loan-page12',
  templateUrl: './page12.component.html',
  styleUrls: ['./page12.component.scss']
})
export class LoanPage12Component implements OnInit {

  constructor(public spinner:SpinnerService) {}

  ngOnInit() {
    this.spinner.stop();
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
