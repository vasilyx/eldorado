import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { PORTAL_PREFIX_PATH, API } from '../../config';

@Component({
  selector: 'view-xml',
  templateUrl: './view-xml.component.html',
  styleUrls: ['./view-xml.component.scss']
})
export class ViewXMLComponent implements OnInit {

  xmlStringList:Array<string> = [];
  tabTitles:Array<string> = [];

  constructor(private route:ActivatedRoute,
              private router:Router,
              private spinner:SpinnerService,
              private apiService:ApiService) {
  }

  ngOnInit() {
    this.spinner.stop();
    this.getXMLResponse();
  }

  getXMLResponse() {
    let id = +this.route.snapshot.params['id'],
      type = this.route.snapshot.params['type'];
    if (type === 'quote') {
      this.apiService.postMethod(API.GET_QUOTE_XML_RESPONSE, {id}).subscribe(
        response => {
          this.xmlStringList = [this.formatXml(response.content)];
          this.tabTitles = ['Call Credit API'];
        }
      );
    } else if (type === 'loan') {
      this.apiService.postMethod(API.GET_LOAN_XML_RESPONSE, {id}).subscribe(
        response => {
          for (let content of response.content) {
            this.xmlStringList.push(this.formatXml(content));
            this.tabTitles = ['CallValidate XML', 'Affordability XML', 'CallReport XML'];
          }
        }
      );
    }
  }

  formatXml(xml) {
    let reg = /(>)\s*(<)(\/*)/g;
    let wsexp = / *(.*) +\n/g;
    let contexp = /(<.+>)(.+\n)/g;
    xml = xml.replace(reg, '$1\n$2$3').replace(wsexp, '$1\n').replace(contexp, '$1\n$2');
    let pad = 0;
    let formatted = '';
    let lines = xml.split('\n');
    let indent = 0;
    let lastType = 'other';
    // 4 types of tags - single, closing, opening, other (text, doctype, comment) - 4*4 = 16 transitions
    let transitions = {
      'single->single': 0,
      'single->closing': -1,
      'single->opening': 0,
      'single->other': 0,
      'closing->single': 0,
      'closing->closing': -1,
      'closing->opening': 0,
      'closing->other': 0,
      'opening->single': 1,
      'opening->closing': 0,
      'opening->opening': 1,
      'opening->other': 1,
      'other->single': 0,
      'other->closing': -1,
      'other->opening': 0,
      'other->other': 0
    };

    for (let i = 0; i < lines.length; i++) {
      let ln = lines[i];
      let single = Boolean(ln.match(/<.+\/>/)); // is this line a single tag? ex. <br />
      let closing = Boolean(ln.match(/<\/.+>/)); // is this a closing tag? ex. </a>
      let opening = Boolean(ln.match(/<[^!].*>/)); // is this even a tag (that's not <!something>)
      let type = single ? 'single' : closing ? 'closing' : opening ? 'opening' : 'other';
      let fromTo = lastType + '->' + type;
      lastType = type;
      let padding = '';

      indent += transitions[fromTo];
      for (let j = 0; j < indent; j++) {
        padding += '    ';
      }

      formatted += padding + ln + '\n';
    }

    return formatted;
  };

  goToDashboard() {
    this.router.navigate([`${PORTAL_PREFIX_PATH}/dashboard`]);
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
