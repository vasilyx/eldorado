import {Component, OnInit} from '@angular/core';

import {SpinnerService} from '../../shared/spinner/spinner.service';
import {DashboardService} from '../../service/dashboard.service';
import {QUOTE_STATUS_CODE, API, LOAN_STATUS_CODE} from '../../config';
import {ApiService} from '../../service/api.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  items:any = {};
  item: any = [];
  tab: string = '';
  clientStatus: string;
  subscription: Subscription;
  user: any = {};

  constructor(private spinner: SpinnerService,
              private dashboardService: DashboardService,
              private apiService: ApiService) {
  }

  ngOnInit() {
    this.apiService.getMethod(API.GET_USER_DETAILS).subscribe(
      response => {
        this.user = response.content;
      }
    );
    this.subscription = this.dashboardService.dashboard$.subscribe(res => {
      if(Object.keys(res).length) {
        this.spinner.stop();
        this.items = res;
        this.clientStatus = this.items.quotes[0].quote_product == 1 ? 'investor' : 'borrower';
        this.item = this.items.quotes.filter((item) => {
          if(this.clientStatus == 'investor') {
            if(QUOTE_STATUS_CODE[item.quote_status] !== 'Active' &&
              QUOTE_STATUS_CODE[item.quote_status] !== 'Closed') {
              return true;
            }
          }
          else {
            if(LOAN_STATUS_CODE[item.loan_status] !== 'Active' &&
              LOAN_STATUS_CODE[item.loan_status] !== 'Fully Matched' &&
              LOAN_STATUS_CODE[item.loan_status] !== 'Signed' &&
              LOAN_STATUS_CODE[item.loan_status] !== 'Settled') {
              return true;
            }
          }
        });
        if(this.item.length == 0) {
          if(this.clientStatus == 'borrower') {
            this.changeTab('loans');
          }
          if(this.clientStatus == 'investor') {
            this.changeTab('investments');
          }
        } else {
          this.changeTab('dashboard');
        }
        /*if(this.clientStatus == 'borrower' && QUOTE_STATUS_CODE[this.items.quotes[0].loan_status] === 'Active'
      && LOAN_STATUS_CODE[this.items.quotes[0]] == 'Active') {
          this.changeTab('loans');
        }
        if(this.clientStatus == 'investor' && LOAN_STATUS_CODE[this.items.quotes[0]] == 'Active') {
          this.changeTab('investments');
        }*/
      }
    });
  }

  changeTab(who) {
    if(this.tab != who) {
      this.tab = who;
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.spinner.start();
  }

}
