import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {SpinnerService} from '../../../shared/spinner/spinner.service';
import {ApiService} from '../../../service/api.service';
import {DashboardService} from '../../../service/dashboard.service';
import {
  API,
  LOAN_STATUS_CODE,
  OFFER_RATE_CODE,
  OFFER_STATUS_CODE,
  OFFER_TERM_CODE,
  OFFER_TYPE_CODE,
  PORTAL_PREFIX_PATH,
  PRODUCT_TYPE,
  QUOTE_STATUS_CODE
} from '../../../config';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'dashboard-tab',
  templateUrl: './dashboard-tab.component.html',
  styleUrls: ['./dashboard-tab.component.scss']
})
export class DashboardTabComponent implements OnInit {
  items:any = {};
  prefixPath:string = PORTAL_PREFIX_PATH;
  loanStatus = LOAN_STATUS_CODE;
  clientStatus: string;
  subscription: Subscription;

  constructor(private spinner:SpinnerService,
              private apiService:ApiService,
              private router:Router,
              private dashboardService: DashboardService) {
    this.items = {
      quotes: [],
      loans: [],
      offers: []
    }
  }

  ngOnInit() {
    this.getDashboard();
  }

  getDashboard() {
    this.subscription = this.dashboardService.dashboard$.subscribe(res => {
      if(Object.keys(res).length) {
        this.spinner.stop();
        this.items = res;
        this.clientStatus = this.items.quotes[0].quote_product == 1 ? 'investor' : 'borrower';
        this.items.quotes = this.items.quotes.filter((item) => {
          if(this.clientStatus == 'investor') {
            if(QUOTE_STATUS_CODE[item.quote_status] !== 'Active' &&
              QUOTE_STATUS_CODE[item.quote_status] !== 'Closed') {
              return true;
            }
          }
          else {
            if(LOAN_STATUS_CODE[item.loan_status] !== 'Active' &&
              LOAN_STATUS_CODE[item.loan_status] !== 'Fully Matched' &&
              LOAN_STATUS_CODE[item.loan_status] !== 'Signed' &&
              LOAN_STATUS_CODE[item.loan_status] !== 'Settled') {
              return true;
            }
          }
        });
        for (let quote of this.items.quotes) {
          quote.displayedStatus = QUOTE_STATUS_CODE[quote.quote_status];
          quote.displayedDate = moment(quote.quote_datetime_update).format('DD MMM YYYY');
        }
        for (let loan of this.items.loans) {
          loan.displayedStatus = LOAN_STATUS_CODE[loan.loan_status];
          loan.displayedDate = moment(loan.loan_datetime_update).format('DD MMM YYYY');
        }
        for (let offer of this.items.offers) {
          offer.displayedRate = OFFER_RATE_CODE[offer.offer_rate];
          offer.displayedTerm = OFFER_TERM_CODE[offer.offer_term];
          offer.displayedType = OFFER_TYPE_CODE[offer.offer_type];
          offer.displayedStatus = OFFER_STATUS_CODE[offer.offer_status];
        }
      }
    });
  }

  selectQuote(item) {
    if (PRODUCT_TYPE[item.quote_product] === 'borrow') {
      if (QUOTE_STATUS_CODE[item.quote_status] === 'Draft') {
        this.router.navigate([`${this.prefixPath}/borrow/quote_term`]);
      } else if (QUOTE_STATUS_CODE[item.quote_status] === 'Approved') {
        this.router.navigate([`${this.prefixPath}/quote_details/approved/${item.quote_id}`]);
      } else if (QUOTE_STATUS_CODE[item.quote_status] === 'Rejected') {
        this.router.navigate([`${this.prefixPath}/quote_details/rejected/${item.quote_id}`]);
      }
    } else if (PRODUCT_TYPE[item.quote_product] === 'invest') {
      if (QUOTE_STATUS_CODE[item.quote_status] === 'Draft') {
        this.router.navigate([`${this.prefixPath}/invest/contact_information`]);
      }
    }
  }

  selectLoan() {
    this.router.navigate([`${this.prefixPath}/loan/status`]);
  }

  selectOffer(item) {
    this.router.navigate([`${this.prefixPath}/transfer_money/${item.offer_id}`]);
  }

  deleteOffer(item) {
    if (item.offer_id) {
      this.apiService.deleteMethod(API.INVEST_OFFER.replace(/(\{id\})/i, `/${item.offer_id}`)).subscribe(
        response => {
          this.getDashboard();
        }
      )
    }
  }

  viewXML(item) {
    let id, type;
    if (item.quote_id) {
      type = 'quote';
      id = item.quote_id;
    } else if (item.loan_id) {
      type = 'loan';
      id = item.loan_id;
    } else {
      return false;
    }
    this.router.navigate([`${this.prefixPath}/view_xml/${type}/${id}`]);
  }

  navigateToLoanAgreement(item) {
    this.router.navigate([`${this.prefixPath}/loan/loan_agreement/${item.quote_id}`]);
  }

  ngOnDestroy() {
    this.spinner.start();
    this.subscription.unsubscribe();
  }
}
