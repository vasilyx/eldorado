import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {SpinnerService} from '../../../shared/spinner/spinner.service';
import {API, INSTALLMENT_STATUS, LOAN_STATUS_CODE, PAYMENT_STATUS, QUOTE_STATUS_CODE} from '../../../config';
import {Headers, Http, RequestOptions, ResponseContentType} from '@angular/http';
import {StorageService} from '../../../service/storage.service';
import {ApiService} from '../../../service/api.service';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';
import {LoansDialogComponent} from './loans-dialog/loans-dialog.component';
import {DashboardService} from '../../../service/dashboard.service';

@Component({
  selector: 'app-loans-tab',
  templateUrl: './loans-tab.component.html',
  styleUrls: ['./loans-tab.component.scss']
})
export class LoansTabComponent implements OnInit {

  dialogRef:MdDialogRef<any>;
  data: any = [];
  loanStatus = LOAN_STATUS_CODE;
  clientStatus: string;
  instalments: Array<any> = [];
  paymentStatus = PAYMENT_STATUS;
  instalmentStatus = INSTALLMENT_STATUS;

  constructor(private spinner: SpinnerService,
              private apiService: ApiService,
              private http: Http,
              private storageService: StorageService,
              public dialog:MdDialog,
              public viewContainerRef:ViewContainerRef,
              private dashboardService: DashboardService){}

  ngOnInit() {
    this.spinner.stop();
    this.apiService.getMethod(API.LOANS_DETAILS).subscribe(res => {
      this.data = res.content;
    });
    this.dashboardService.dashboard$.subscribe(res => {
      if(Object.keys(res).length) {
        this.clientStatus = res.quotes[0].quote_product == 1 ? 'investor' : 'borrower';
        if(this.clientStatus == 'borrower') {
          this.apiService.getMethod(API.INSTALMENTS).subscribe(res => {
            this.instalments = res.content;
          });
        }
      }
    });
  }

  pdf(loan_apply_id, offer_id) {
    const saveBlob = (function () {
      let a = document.createElement("a");
      document.body.appendChild(a);
      return function (blob, fileName) {
        let url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = fileName;
        a.click();
        window.URL.revokeObjectURL(url);
      };
    }());
    const headers = new Headers({
      'Auth': 'whs' + this.storageService.read('token'),
      'Content-Type': 'application/pdf'
    });
    let options = new RequestOptions({headers: headers});
    let url = API.CONTRACT_PDF.replace(/(\{loan_apply_id\})/i, loan_apply_id);
    if(this.clientStatus == 'investor') {
      url += `?invest_offer_id=${offer_id}`;
    }
    options.responseType = ResponseContentType.Blob;
    this.http.get(url,options).subscribe(
      (response) => {
        if(response.status === 200) {
          const blob = new Blob([response.blob()], {type: 'application/pdf'});
          saveBlob(blob, 'Agreement.pdf');
        }
      });
  }

  view(data) {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;
    config.width = '1200px';
    config.data = data;

    this.dialogRef = this.dialog.open(LoansDialogComponent, config);
    this.dialogRef.afterClosed().subscribe((data) => {
      this.dialogRef = null;
      if (data) {

      }
    });
  }

  convertDateForDisplay(date) {
    if (date) {
      return moment(date).format('D MMM YYYY');
    }
  }
}
