import {Component, Inject} from '@angular/core';
import {MdDialogRef, MD_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-loans-dialog',
  templateUrl: './loans-dialog.component.html',
  styleUrls: ['./loans-dialog.component.scss']
})
export class LoansDialogComponent {

  constructor(@Inject(MD_DIALOG_DATA) public data,
              public dialogRef:MdDialogRef<LoansDialogComponent>) { }

  convertDateForDisplay(date) {
    if (date) {
      return moment(date).format('D MMM YYYY');
    }
  }
}
