import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {CurrencyPipe} from '@angular/common';
import {API, REVERSE_OFFER_TYPE_CODE} from '../../../../config';
import {ApiService} from '../../../../service/api.service';

@Component({
  selector: 'app-investments-dialog',
  templateUrl: './investments-dialog.component.html',
  styleUrls: ['./investments-dialog.component.scss'],
  providers: [CurrencyPipe]
})
export class InvestmentsDialogComponent implements OnInit {

  type: string = '';
  step: number = 1;
  amountData: {} = {};
  withdraw_amount: number = 0;
  error: string;

  constructor(@Inject(MD_DIALOG_DATA) public data,
              public dialogRef: MdDialogRef<InvestmentsDialogComponent>,
              private currencyPipe: CurrencyPipe,
              private apiService: ApiService) {
  }

  ngOnInit() {

  }

  amount() {
    if (this.type) {
      this.step = 2;
      switch (this.type) {
        case 'GEN':
          this.amountData = Object.assign({}, this.data.GEN, {name: 'GENERAL'});
          break;
        case 'SIPP':
          this.amountData = Object.assign({}, this.data.GEN, {name: 'SIPP'});
          break;
        case 'ISA':
          this.amountData = Object.assign({}, this.data.GEN, {name: 'ISA'});
          break;
        default :
      }
    }
  }

  verify() {
    this.error = '';
    if (this.withdraw_amount > this.amountData['cash_amount']) {
      this.error = `Max limit ${this.currencyPipe.transform(this.amountData['cash_amount'], 'GBP', true, '.0-2')}`;
    }
    else {
      this.apiService.postMethod(API.WITHDRAW_INVESTOR, {
        amount: this.withdraw_amount,
        investment_type: REVERSE_OFFER_TYPE_CODE[this.amountData['name']]
      }).subscribe(res => {
        this.step = 4;
      });
    }
  }

  confirm() {

  }

}
