import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {SpinnerService} from '../../../shared/spinner/spinner.service';
import {API} from '../../../config';
import {ApiService} from '../../../service/api.service';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';
import {InvestmentsDialogComponent} from './investments-dialog/investments-dialog.component';


@Component({
  selector: 'app-investments-tab',
  templateUrl: './investments-tab.component.html',
  styleUrls: ['./investments-tab.component.scss']
})
export class InvestmentsTabComponent implements OnInit {

  data: any = {};
  opportunities: Array<any> = [];
  dialogRef:MdDialogRef<any>;

  constructor(private spinner: SpinnerService,
              private apiService: ApiService,
              public dialog:MdDialog,
              public viewContainerRef:ViewContainerRef) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.apiService.getMethod(API.PORTFOLIO_SUMMARY).subscribe(res => {
      this.data = res.content;
    });
    this.apiService.getMethod(API.BORROWER_OPPORTUNITIES).subscribe(res => {
      this.opportunities = res.content;
    });
  }

  withdraw(data) {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;
    config.width = '500px';
    config.data = data;

    this.dialogRef = this.dialog.open(InvestmentsDialogComponent, config);
    this.dialogRef.afterClosed().subscribe(() => {
      this.dialogRef = null;
      this.getData();
    });
  }
}
