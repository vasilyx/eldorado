import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { Router, NavigationEnd} from '@angular/router';
import 'rxjs/add/operator/filter';

import { SpinnerService } from '../shared/spinner/spinner.service';
import { HeaderService } from '../shared/header/header.service';
import { ApiService } from '../service/api.service';
import { API } from '../config';
import { DashboardService} from '../service/dashboard.service';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.scss']
})
export class LoanComponent implements OnInit {

  sidenavOpenState:boolean;
  modeSlider:string;
  @ViewChild('sidenav') sidenav;
  subscription:Subscription;
  clientSubscription:Subscription;
  disableRouterEventListener:any;
  activityLogs:string = 'activity_logs';
  clientStatus: string;

  constructor(private router:Router,
              private spinner:SpinnerService,
              private headerService:HeaderService,
              private apiService:ApiService,
              private dashboardService: DashboardService) {
    this.disableRouterEventListener = this.router.events.subscribe((event:NavigationEnd) => {
      this.sidenavOpenState = window.innerWidth > 768;
      this.modeSlider = (window.innerWidth > 768) ? "side" : "over";
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.sidenavOpenState = event.currentTarget.innerWidth > 768;
    this.modeSlider = (event.currentTarget.innerWidth > 768) ? "side" : "over";
  }

  @HostListener('window:load', ['$event'])
  onload(event) {
    this.sidenavOpenState = event.currentTarget.innerWidth > 768;
    this.modeSlider = (event.currentTarget.innerWidth > 768) ? "side" : "over";
  }


  ngOnInit() {
    this.spinner.start();
    //todo https://github.com/angular/material2/issues/1370
    this.sidenav._onTransitionEnd = function () {
      this._openPromise = null;
      this._closePromise = null;
    };
    this.subscription = this.headerService.sidenavItem$.subscribe(item => {
      this.sidenavOpenState = item;
    });
    this.clientSubscription = this.apiService.getMethod(API.GET_DASHBOARD).subscribe(res => {
      this.dashboardService.changeDashboardData(res.content);
      this.clientStatus = res.content.quotes[0].quote_product == 1 ? 'investor' : 'borrower';
    });
  }

  logout() {
    this.apiService.postMethod(API.LOGOUT).subscribe(
      response => {
        this.router.navigate(['/login']);
      }
    )
  }

  ngOnDestroy() {
    this.spinner.start();
    this.subscription.unsubscribe();
    this.clientSubscription.unsubscribe();
    this.disableRouterEventListener.unsubscribe();
    this.dashboardService.changeDashboardData({});
  }
}
