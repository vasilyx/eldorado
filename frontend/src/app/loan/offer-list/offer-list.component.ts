import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {
  API,
  LOAN_STATUS_CODE,
  OFFER_RATE_CODE,
  OFFER_STATUS_CODE,
  OFFER_TERM_CODE,
  OFFER_TYPE_CODE,
  PORTAL_PREFIX_PATH
} from '../../config';
import {ApiService} from '../../service/api.service';
import {Router} from '@angular/router';
import {Headers, Http, RequestOptions, ResponseContentType} from '@angular/http';
import {StorageService} from '../../service/storage.service';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';
import {OfferInstalmentDialogComponent} from './offer-instalment-dialog/offer-instalment-dialog.component';

@Component({
  selector: 'app-offer-list',
  templateUrl: './offer-list.component.html',
  styleUrls: ['./offer-list.component.scss']
})
export class OfferListComponent implements OnInit {
  offers: Array<any>;
  indexArray: Array<boolean>;
  prefixPath: string = PORTAL_PREFIX_PATH;
  status = LOAN_STATUS_CODE;
  dialogRef:MdDialogRef<any>;

  constructor(private apiService: ApiService,
              private router: Router,
              private http: Http,
              private storageService: StorageService,
              public dialog:MdDialog,
              public viewContainerRef:ViewContainerRef) {
  }

  ngOnInit() {
    this.getDashboard();
  }

  getDashboard() {
    this.apiService.getMethod(API.INVEST_OFFERS).map(res => res.content).subscribe(res => {
      this.offers = res;
      this.offers.forEach((item)=> {
        if(item.details.agreements && item.details.agreements.length) {
          let offersAmount = 0;
          item.details.agreements.forEach((agreement)=> {
            offersAmount += parseFloat(agreement.sum_amount);
          });
          item.details['matching'] = offersAmount / item.details['amount'];
        }
      });
      this.indexArray = new Array(this.offers.length);
      for (let offer of this.offers) {
        offer.displayedRate = OFFER_RATE_CODE[offer.offer.rate];
        offer.displayedTerm = OFFER_TERM_CODE[offer.offer.term];
        offer.displayedType = OFFER_TYPE_CODE[offer.offer.investment_type];
        offer.displayedStatus = OFFER_STATUS_CODE[offer.offer.status];
      }
    })
  }

  selectOffer(item) {
    this.router.navigate([`${this.prefixPath}/transfer_money/${item.id}`]);
  }

  deleteOffer(item) {
    if (item.id) {
      this.apiService.deleteMethod(API.INVEST_OFFER.replace(/(\{id\})/i, `/${item.id}`)).subscribe(
        response => {
          this.getDashboard();
        }
      )
    }
  }

  pdf(loan_apply_id, offer_id) {
    const saveBlob = (function () {
      let a = document.createElement("a");
      document.body.appendChild(a);
      return function (blob, fileName) {
        let url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = fileName;
        a.click();
        window.URL.revokeObjectURL(url);
      };
    }());
    const headers = new Headers({
      'Auth': 'whs' + this.storageService.read('token'),
      'Content-Type': 'application/pdf'
    });
    let options = new RequestOptions({headers: headers});
    let url = API.CONTRACT_PDF.replace(/(\{loan_apply_id\})/i, loan_apply_id) + `?invest_offer_id=${offer_id}`;
    options.responseType = ResponseContentType.Blob;
    this.http.get(url,options).subscribe(
      (response) => {
        if(response.status === 200) {
          const blob = new Blob([response.blob()], {type: 'application/pdf'});
          saveBlob(blob, 'Agreement.pdf');
        }
      });
  }

  getInstalment(agreement_id, offer_id) {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;
    config.width = '1000px';
    config.data = {
      agreement_id: agreement_id,
      offer_id: offer_id
    };

    this.dialogRef = this.dialog.open(OfferInstalmentDialogComponent, config);
    this.dialogRef.afterClosed().subscribe(() => {
      this.dialogRef = null;
    });
  }

}
