import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {API, INSTALLMENT_STATUS, PAYMENT_STATUS} from '../../../config';
import {ApiService} from '../../../service/api.service';

@Component({
  selector: 'app-offer-instalment-dialog',
  templateUrl: './offer-instalment-dialog.component.html',
  styleUrls: ['./offer-instalment-dialog.component.scss']
})
export class OfferInstalmentDialogComponent implements OnInit {


  instalmentsData: Array<any> = [];
  paymentStatus = PAYMENT_STATUS;
  instalmentStatus = INSTALLMENT_STATUS;

  constructor(@Inject(MD_DIALOG_DATA) public data,
              public dialogRef: MdDialogRef<OfferInstalmentDialogComponent>,
              private apiService: ApiService) {
  }

  ngOnInit() {
    this.apiService.postMethod(API.INSTALMENTS_BY_OFFER,{
      agreement_id: this.data.agreement_id,
      offer_id: this.data.offer_id
    }).subscribe(res => {
      this.instalmentsData = res.content;
    })
  }
  convertDateForDisplay(date) {
    if (date) {
      return moment(date).format('D MMM YYYY');
    }
  }
}
