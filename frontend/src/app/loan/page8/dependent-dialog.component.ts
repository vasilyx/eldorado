import { Component, ViewChild } from '@angular/core';
import { MdDialogRef } from '@angular/material';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { DATE_FORMAT_CLIENT, DATE_FORMAT_SERVER, PATTERN_STRING, ERRORS, API } from '../../config';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'dependent-dialog',
  templateUrl: './dependent-dialog.component.html',
  styleUrls: ['./dependent-dialog.component.scss']
})
export class DependentDialogComponent {

  @ViewChild('d') datePicker;
  dependent:any = {};
  index:number;
  birthdateObj:any;
  errors:any = {};
  mask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];

  constructor(public dialogRef:MdDialogRef<any>,
              public spinner:SpinnerService,
              public apiService:ApiService,
              public datepickerConfig:NgbDatepickerConfig) {
    datepickerConfig.minDate = {year: 1900, month: 1, day: 1};
    datepickerConfig.maxDate = {year: moment().format('YYYY'), month: moment().format('M'), day: moment().format('D')};
  }

  addUpdateDependent() {
    if (this.validation()) {
      let data = (<any>Object).assign({}, this.dependent);
      data.birthdate = this.convertDateForServer(this.dependent.birthdate);
      this.apiService.postMethod(API.DEPENDENTS, data).subscribe(
        response => {
          this.dialogRef.close(true);
        },
        err => {
          if (err.status === 'error') {
            for (var key in err.content) {
              this.errors[key] = ERRORS[err.content[key].code] || err.content[key].code;
            }
          }
        }
      )
    }
  }

  onChangeDateInput() {
    if (moment(this.dependent.birthdate, 'DD/MM/YYYY', true).isValid()) {
      this.birthdateObj = {
        year: +moment(this.dependent.birthdate, 'DD/MM/YYYY').format('YYYY'),
        month: +moment(this.dependent.birthdate, 'DD/MM/YYYY').format('M'),
        day: +moment(this.dependent.birthdate, 'DD/MM/YYYY').format('D')
      };
    }
  }

  onChangeDateCalendar() {
    if (this.birthdateObj) {
      this.dependent.birthdate = `${moment(this.birthdateObj.day, 'D').format('DD')}/${moment(this.birthdateObj.month, 'M').format('MM')}/${this.birthdateObj.year}`;
    }
  }


  deleteDependent(index) {
    this.apiService.deleteMethod(API.DEPENDENTS, {id: index}).subscribe(
      response => {
        this.dialogRef.close(true);
      }
    )
  }

  validation() {
    let isValid = true;

    if (!this.dependent.first_name) {
      this.errors.first_name = 'form.first_name.isBlank';
      isValid = false;
    } else if (PATTERN_STRING instanceof RegExp && !PATTERN_STRING.test(this.dependent.first_name)) {
      this.errors.first_name = ERRORS['form.first_name.notString'] || 'form.first_name.notString';
      isValid = false;
    }

    if (!this.dependent.birthdate) {
      this.errors.birthdate = 'form.birthdate.isBlank';
      isValid = false;
    } else if (!moment(this.dependent.birthdate, DATE_FORMAT_CLIENT, true).isValid())  {
      this.errors.birthdate = 'form.birthdate.wrong';
      isValid = false;
    } else if (moment(this.dependent.birthdate, DATE_FORMAT_CLIENT).unix() > moment().unix()) {
      this.errors.birthdate = 'form.birthdate.infuture';
      isValid = false;
    }

    return isValid;
  }

  convertDateForServer(date) {
    if (date) {
      return moment(date, DATE_FORMAT_CLIENT).format(DATE_FORMAT_SERVER);
    }
  }

  formatDate(date) {
    return date ? `${date.day}/${date.month}/${date.year}` : '';
  }
}
