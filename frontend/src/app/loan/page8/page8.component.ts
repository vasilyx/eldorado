import { Component, OnInit, ViewContainerRef } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import {DATE_FORMAT_SERVER, PORTAL_PREFIX_PATH, ERRORS, API, DATE_FORMAT_CLIENT} from '../../config';
import { DependentDialogComponent } from './dependent-dialog.component';
import { FilterInputPipe} from '../../pipe/filter-input.pipe';
import { PaddingZeroService} from '../../service/padding-zero.service';

@Component({
  selector: 'loan-page8',
  templateUrl: './page8.component.html',
  styleUrls: ['./page8.component.scss']
})
export class LoanPage8Component implements OnInit {

  dialogRef:MdDialogRef<any>;
  model:any = {};
  investorIncome:any = {};
  quote:any = {};
  dependents:any = [];
  dependent:any = {};
  birthdateObj:any = {};
  errors:any = {};
  prefixPath:string = PORTAL_PREFIX_PATH;
  index:number = -1;
  noDependencies:boolean = false;
  monthlySumm:number = 0;
  quoteId: number;

  constructor(private spinner:SpinnerService,
              private router:Router,
              private apiService:ApiService,
              public dialog:MdDialog,
              private filterInputPipe: FilterInputPipe,
              public viewContainerRef:ViewContainerRef,
              private route:ActivatedRoute,
              public paddingZeroService: PaddingZeroService) {
  }

  ngOnInit() {
    this.apiService.getMethod(API.GET_DASHBOARD).subscribe(
      response => {
        this.quoteId = response.content.quotes[0].quote_id;
      }
    );
    this.getHouseholdDetails();
    this.getPreferences();
    this.getDependents();
    this.getQuoteDetails();
    this.getInvestorIncome();
  }

  onSubmit() {
    if (this.validation()) {
      let data = (<any>Object).assign({}, this.model);
      if (this.noDependencies && this.dependents.length) {
        this.apiService.deleteMethod(API.DELETE_DEPENDENTS, data).subscribe(
          response => {
            this.apiService.postMethod(API.HOUSEHOLD, data).subscribe(
              response => {
                this.router.navigate([`${this.prefixPath}/loan/credit_lines`]);
              }
            )
          }
        )
      } else {
        this.apiService.postMethod(API.HOUSEHOLD, data).subscribe(
          response => {
            this.router.navigate([`${this.prefixPath}/loan/credit_lines`]);
          }
        )
      }
    }
  }

  getDependents() {
    this.apiService.getMethod(API.DEPENDENTS).subscribe(
      response => {
        this.dependents = response.content;
      }
    )
  }

  getPreferences() {
    this.apiService.getMethod(API.PREFERENCES).subscribe(
      response => {
        this.noDependencies = response.content.has_no_dependencies;
      }
    )
  }

  setPreferences(value) {
    this.apiService.patchMethod(API.PREFERENCES, {has_no_dependencies: value}, {hideLoader: true}).subscribe(
      response => {}
    )
  }

  getHouseholdDetails() {
    this.apiService.getMethod(API.HOUSEHOLD).subscribe(
      response => {
        if (response.content) {
          this.model = response.content;
          this.calculateExpenses();
        }
      }
    );
  }

  getQuoteDetails() {
    this.apiService.getMethod(API.GET_QUOTE_DETAILS).subscribe(
      response => {
        this.quote = response.content ? response.content : {};
      }
    );
  }

  getInvestorIncome() {
    this.apiService.getMethod(API.USER_INCOME).subscribe(
      response => {
        if (Object.keys(response.content).length) {
          this.investorIncome = response.content;
        }
      }
    );
  }

  showDependentPopup() {
    this.index = -1;
    this.dependent = {};
    this.birthdateObj = null;
    this.openDialog();
  }

  editDependent(dependent) {
    this.index = dependent.id;
    this.dependent = dependent;
    this.dependent.birthdate = moment(dependent.birthdate, DATE_FORMAT_SERVER).format(DATE_FORMAT_CLIENT);
    this.birthdateObj = {
      day: moment(dependent.birthdate, DATE_FORMAT_SERVER).format('D'),
      month: moment(dependent.birthdate, DATE_FORMAT_SERVER).format('M'),
      year: moment(dependent.birthdate, DATE_FORMAT_SERVER).format('YYYY')
    };
    this.openDialog();
  }

  openDialog() {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.width = "480px";

    this.dialogRef = this.dialog.open(DependentDialogComponent, config);

    this.dialogRef.componentInstance.index = this.index;
    this.dialogRef.componentInstance.dependent = this.dependent;
    this.dialogRef.componentInstance.birthdateObj = this.birthdateObj;

    this.dialogRef.afterClosed().subscribe((needRefetch) => {
      this.dialogRef = null;
      if (needRefetch) {
        this.noDependencies = false;
        this.getDependents();
      }
    });
  }

  calculateExpenses() {
    this.monthlySumm = 0;
    var fieldArr = ['monthly_rent', 'monthly_utilities', 'monthly_travel', 'monthly_food', 'monthly_entertainment', 'monthly_savings'];
    fieldArr.forEach((val) => {
      if (!isNaN(+this.model[val])) {
        this.monthlySumm += +this.model[val];
      }
    });
  }

  validation() {

    let isValid = true;

    if (!this.model.monthly_rent && this.model.monthly_rent != 0) {
      this.errors.monthly_rent = ERRORS['form.monthly_rent.isBlank'];
      isValid = false;
    }

    if (!this.model.monthly_utilities) {
      this.errors.monthly_utilities = ERRORS['form.monthly_utilities.isBlank'];
      isValid = false;
    }

    if (!this.model.monthly_food) {
      this.errors.monthly_food = ERRORS['form.monthly_food.isBlank'];
      isValid = false;
    }

    if (!this.model.monthly_savings) {
      this.errors.monthly_savings = ERRORS['form.monthly_savings.isBlank'];
      isValid = false;
    }

    if (!this.model.monthly_entertainment) {
      this.errors.monthly_entertainment = ERRORS['form.monthly_entertainment.isBlank'];
      isValid = false;
    }

    return isValid;
  }

  convertDateForDisplay(date) {
    if (date) {
      return moment(date, DATE_FORMAT_SERVER).format('D MMM YYYY');
    }
  }

  calculateAge(date) {
    if (date) {
      return moment().diff(moment(date, 'YYYYMMDD'), 'years');
    }
  }

  back() {
    this.router.navigate([`${this.prefixPath}/loan/employment_information`]);
  }

  filter(event) {
    return this.filterInputPipe.transform(event);
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
