import { Component, OnInit, ViewContainerRef } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { TITLES, CONTACT_NUMBER_CODE, PORTAL_PREFIX_PATH, API, ERRORS } from '../../config';
import { VerificationDialogComponent } from "../../shared/verification-dialog/verification-dialog.component";
import { PaddingZeroService} from '../../service/padding-zero.service';

@Component({
  selector: 'loan-page5',
  templateUrl: './page5.component.html',
  styleUrls: ['./page5.component.scss']
})
export class LoanPage5Component implements OnInit {

  dialogRef:MdDialogRef<any>;
  model:any = {};
  quote:any = {};
  errors:any = {};
  email:string;
  options:Array<string> = TITLES;
  showMobileVerificationButton:boolean;
  prefixPath:string = PORTAL_PREFIX_PATH;
  quoteId: number;

  constructor(private spinner:SpinnerService,
              private router:Router,
              private apiService:ApiService,
              private dialog:MdDialog,
              private viewContainerRef:ViewContainerRef,
              private route:ActivatedRoute,
              public paddingZeroService: PaddingZeroService) {}

  ngOnInit() {
    this.apiService.getMethod(API.GET_DASHBOARD).subscribe(
      response => {
        this.quoteId = response.content.quotes[0].quote_id;
      }
    );
    this.getInvestorContacts();
    this.getUserDetails();
    this.getQuoteDetails();
  }

  onSubmit() {
    if (this.validation()) {
      this.addInvestorContacts();
    }
  }

  getInvestorContacts() {
    this.apiService.getMethod(API.GET_INVESTOR_CONTACTS).subscribe(
      response => {
        let content = response.content;
        if (content.length) {
          for (let item in content) {
            if (content[item].type === CONTACT_NUMBER_CODE.MOBILE) {
              this.model.mobile_phone = content[item].value;
              this.showMobileVerificationButton = !content[item].verified;
            } else if (content[item].type === CONTACT_NUMBER_CODE.HOME) {
              this.model.home_phone = content[item].value;
            } else if (content[item].type === CONTACT_NUMBER_CODE.WORK) {
              this.model.work_phone = content[item].value;
            } else if (content[item].type === CONTACT_NUMBER_CODE.SKYPE) {
              this.model.skype = content[item].value;
            } else if (content[item].type === CONTACT_NUMBER_CODE.LINKEDIN) {
              this.model.linkedin = content[item].value;
            } else if (content[item].type === CONTACT_NUMBER_CODE.FACEBOOK) {
              this.model.facebook = content[item].value;
            }
          }
        }
      }
    );
  }

  getUserDetails() {
    this.apiService.getMethod(API.GET_USER_DETAILS).subscribe(
      response => {
        let content = response.content;
        if (content) {
          this.model.title = content.title;
          this.model.screen_name = content.screen_name;
          this.email = content.user.username;
        }
      }
    )
  }

  getQuoteDetails() {
    this.apiService.getMethod(API.GET_QUOTE_DETAILS).subscribe(
      response => {
        this.quote = response.content ? response.content : {};
      }
    );
  }

  addInvestorContacts() {
    let data = {
      communications: {},
      title: this.model.title,
      screen_name: this.model.screen_name
    };
    data.communications[CONTACT_NUMBER_CODE.SKYPE] = this.model.skype;
    data.communications[CONTACT_NUMBER_CODE.MOBILE] = this.model.mobile_phone;
    data.communications[CONTACT_NUMBER_CODE.HOME] = this.model.home_phone;
    data.communications[CONTACT_NUMBER_CODE.WORK] = this.model.work_phone;
    data.communications[CONTACT_NUMBER_CODE.LINKEDIN] = this.model.linkedin;
    data.communications[CONTACT_NUMBER_CODE.FACEBOOK] = this.model.facebook;
    this.apiService.postMethod(API.ADD_INVESTOR_CONTACTS, data).subscribe(
      response => {
        this.checkMobileVerification(response.content);
        if (!this.showMobileVerificationButton) {
          this.router.navigate([`${this.prefixPath}/loan/employment_information`]);
        }
      },
      err => {
        if (err.status === 'error') {
          for (var key in err.content) {
            this.errors[key] = err.content[key].code;
          }
        }
      }
    );
  }

  checkMobileVerification(content) {
    if (content.length) {
      for (let item in content) {
        if (content[item].type === CONTACT_NUMBER_CODE.MOBILE) {
          this.showMobileVerificationButton = this.prefixPath ? !content[item].verified : undefined;
        }
      }
    }
  }

  showVerificationPopup() {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;

    this.dialogRef = this.dialog.open(VerificationDialogComponent, config);

    this.dialogRef.componentInstance.mobile_phone = this.model.mobile_phone;

    this.dialogRef.afterClosed().subscribe((moveOn) => {
      this.dialogRef = null;
      if (moveOn) {
        this.router.navigate([`${this.prefixPath}/loan/employment_information`]);
      }
    });
  }

  validation() {
    let isValid = true;

    if (!this.model.mobile_phone) {
      this.errors.mobilePhone = ERRORS['form.mobilePhone.required'];
      isValid = false;
    }

    if (!this.model.home_phone) {
      this.errors.homePhone = ERRORS['form.homePhone.required'];
      isValid = false;
    }

    if (!this.model.work_phone) {
      this.errors.workPhone = ERRORS['form.workPhone.required'];
      isValid = false;
    }

    if (!this.model.title) {
      this.errors.title = ERRORS['form.title.required'];
      isValid = false;
    }

    if (!this.model.screen_name) {
      this.errors.screenName = ERRORS['form.screenName.required'];
      isValid = false;
    }

    return isValid;
  }

  back() {
    this.router.navigate([`${this.prefixPath}/loan/personal_information`]);
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
