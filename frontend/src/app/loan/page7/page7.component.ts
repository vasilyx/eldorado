import { Component, OnInit, ViewContainerRef } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { DATE_FORMAT_SERVER, PORTAL_PREFIX_PATH, ERRORS, API } from '../../config';
import { OccupationDialogComponent } from './occupation-dialog.component';
import { FilterInputPipe} from '../../pipe/filter-input.pipe';
import { PaddingZeroService} from '../../service/padding-zero.service';

@Component({
  selector: 'loan-page7',
  templateUrl: './page7.component.html',
  styleUrls: ['./page7.component.scss']
})
export class LoanPage7Component implements OnInit {

  dialogRef:MdDialogRef<any>;
  model:any = {};
  bankDetails:any = {};
  quote:any = {};
  employments:any = [];
  employment:any = {};
  errors:any = {};
  shortEmploymentPeriod:boolean = true;
  prefixPath:string = PORTAL_PREFIX_PATH;
  index:number = -1;
  quoteId: number;

  constructor(private spinner:SpinnerService,
              private router:Router,
              private apiService:ApiService,
              private dialog:MdDialog,
              private viewContainerRef:ViewContainerRef,
              private filterInputPipe: FilterInputPipe,
              private route:ActivatedRoute,
              public paddingZeroService: PaddingZeroService) {
  }

  ngOnInit() {
    this.apiService.getMethod(API.GET_DASHBOARD).subscribe(
      response => {
        this.quoteId = response.content.quotes[0].quote_id;
      }
    );
    this.getEmployments();
    this.getBankDetails();
    this.getInvestorIncome();
    this.getQuoteDetails();
  }

  onSubmit() {
    if (this.validation()) {
      let data = (<any>Object).assign({}, this.model);
      this.apiService.postMethod(API.USER_INCOME, data).subscribe(
        response => {
          let data = (<any>Object).assign({}, this.bankDetails);
          this.apiService.postMethod(API.BANK_DETAILS, data).subscribe(
            response => {
              this.router.navigate([`${this.prefixPath}/loan/dependent_information`]);
            }
          );
        }
      )
    }
  }

  getEmployments() {
    this.apiService.getMethod(API.EMPLOYMENTS).subscribe(
      response => {
        this.employments = response.content;
        this.dateHandler();
      }
    );
  }

  getBankDetails() {
    this.apiService.getMethod(API.BANK_DETAILS).subscribe(
      response => {
        this.bankDetails = response.content;
      }
    );
  }

  getInvestorIncome() {
    this.apiService.getMethod(API.USER_INCOME).subscribe(
      response => {
        if (Object.keys(response.content).length) {
          this.model = response.content;
        }
      }
    );
  }

  getQuoteDetails() {
    this.apiService.getMethod(API.GET_QUOTE_DETAILS).subscribe(
      response => {
        this.quote = response.content ? response.content : {};
      }
    );
  }

  editEmployment(employment) {
    this.index = employment.id;
    this.employment = employment;
    this.openDialog();
  }

  showOccupationPopup() {
    this.index = -1;
    this.employment = {};
    this.openDialog();
  }

  openDialog() {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.width = "480px";

    this.dialogRef = this.dialog.open(OccupationDialogComponent, config);

    this.dialogRef.componentInstance.index = this.index;
    this.dialogRef.componentInstance.employment = this.employment;

    this.dialogRef.afterClosed().subscribe((needRefetch) => {
      this.dialogRef = null;
      if (needRefetch) {
        this.getEmployments();
      }
    });
  }

  dateHandler() {
    let minDate,
        hasCurrentOccupation;
    if (this.employments.length) {
      minDate = Math.min(...this.employments.map(v => v.occ_month_started));
      hasCurrentOccupation = this.employments.some(v => v.current_status);
      this.shortEmploymentPeriod = moment().diff(moment(minDate, 'YYYYMMDD'), 'years') < 5 || !hasCurrentOccupation;
    } else {
      this.shortEmploymentPeriod = true;
    }
  }

  validation() {

    let isValid = true;

    if (!this.model.main_income_gross_ann) {
      this.errors.main_income_gross_ann = 'form.main_income_gross_ann.isBlank';
      isValid = false;
    }

    /*if (!this.model.other_income_gross_ann) {
      this.errors.other_income_gross_ann = 'form.other_income_gross_ann.isBlank';
      isValid = false;
    }*/

    if (!this.model.total_net_income_monthly) {
      this.errors.total_net_income_monthly = 'form.total_net_income_monthly.isBlank';
      isValid = false;
    }

    if (!this.bankDetails.bank_account_name) {
      this.errors.bank_account_name = 'form.bank_account_name.isBlank';
      isValid = false;
    }

    if (!this.bankDetails.bank_sort_code) {
      this.errors.bank_sort_code = 'form.bank_sort_code.isBlank';
      isValid = false;
    }

    if (!this.bankDetails.bank_account_number) {
      this.errors.bank_account_number = 'form.bank_account_number.isBlank';
      isValid = false;
    }

    return isValid;
  }

  convertDateForDisplay(date) {
    if (date) {
      return moment(date, DATE_FORMAT_SERVER).format('MMM, YYYY');
    } else {
      return '-';
    }
  }

  back() {
    this.router.navigate([`${this.prefixPath}/loan/contact_information`]);
  }

  filter(event) {
    return this.filterInputPipe.transform(event);
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
