import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { DATE_FORMAT_SERVER, ERRORS, API, PATTERN_STRING } from '../../config';

@Component({
  selector: 'occupation-dialog',
  templateUrl: './occupation-dialog.component.html',
  styleUrls: ['./occupation-dialog.component.scss']
})
export class OccupationDialogComponent {

  employment:any = {};
  index:number;
  month_start:string;
  month_end:string;
  months:any = [];
  year_start:string;
  year_end:string;
  years:any = [];
  errors:any = {};
  mask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];

  constructor(public dialogRef:MdDialogRef<any>,
              public spinner:SpinnerService,
              public apiService:ApiService) {
  }

  ngOnInit() {
    this.employment.current_status = !!this.employment.current_status;
    this.months = moment.months();
    for (let i = parseInt(moment().format('YYYY')); i >= 1970; i--) {
      this.years.push(i.toString());
    }
    if (this.employment.occ_month_started) {
      this.month_start = moment(this.employment.occ_month_started, DATE_FORMAT_SERVER).format('MMMM');
      this.year_start = moment(this.employment.occ_month_started, DATE_FORMAT_SERVER).format('YYYY');
    }
    if (this.employment.occ_month_ended) {
      this.month_end = moment(this.employment.occ_month_ended, DATE_FORMAT_SERVER).format('MMMM');
      this.year_end = moment(this.employment.occ_month_ended, DATE_FORMAT_SERVER).format('YYYY');
    }
  }

  addUpdateEmployment() {
    if (this.validation()) {
      let data = (<any>Object).assign({}, this.employment);
      data.occ_month_started = this.convertDateForServer(this.month_start, this.year_start);
      data.occ_month_ended = this.convertDateForServer(this.month_end, this.year_end);
      data.current_status = !!data.current_status;
      this.apiService.postMethod(API.EMPLOYMENTS, data).subscribe(
        response => {
          this.dialogRef.close(true);
        },
        err => {
          if (err.status === 'error') {
            for (var key in err.content) {
              this.errors[key] = ERRORS[err.content[key].code] || err.content[key].code;
            }
          }
        }
      )
    }
  }

  deleteEmployment(index) {
    this.apiService.deleteMethod(API.EMPLOYMENTS, {id: index}).subscribe(
      response => {
        this.dialogRef.close(true);
      }
    )
  }

  validation() {
    let isValid = true;

    if (!this.employment.business_firm_name) {
      this.errors.business_firm_name = 'form.business_firm_name.isBlank';
      isValid = false;
    }

    if (!this.employment.industry) {
      this.errors.industry = 'form.industry.isBlank';
      isValid = false;
    }

    if (!this.employment.occ_postal_code) {
      this.errors.occ_postal_code = 'form.occ_postal_code.isBlank';
      isValid = false;
    }

    if (!this.employment.occ_country) {
      this.errors.occ_country = 'form.occ_country.isBlank';
      isValid = false;
    } else if (PATTERN_STRING instanceof RegExp && !PATTERN_STRING.test(this.employment.occ_country)) {
      this.errors.occ_country = ERRORS['form.occ_country.notString'] || 'form.occ_country.notString';
      isValid = false;
    }

    if (!this.employment.prof_qual) {
      this.errors.prof_qual = 'form.prof_qual.isBlank';
      isValid = false;
    }

    if (!this.employment.occ_status) {
      this.errors.occ_status = 'form.occ_status.isBlank';
      isValid = false;
    } else if (PATTERN_STRING instanceof RegExp && !PATTERN_STRING.test(this.employment.occ_status)) {
      this.errors.occ_status = ERRORS['form.occ_status.notString'] || 'form.occ_status.notString';
      isValid = false;
    }

    if (!this.employment.job_description) {
      this.errors.job_description = 'form.job_description.isBlank';
      isValid = false;
    }

    if (!this.month_start && !this.year_start) {
      this.errors.occ_month_started = 'form.month&year.required';
      isValid = false;
    } else if (!this.month_start) {
      this.errors.occ_month_started = 'form.month.required';
      isValid = false;
    } else if (!this.year_start) {
      this.errors.occ_month_started = 'form.year.required';
      isValid = false;
    }

    if (this.employment.current_status) {
      return isValid;
    } else if (!this.month_end && !this.year_end) {
      this.errors.occ_month_ended = 'form.month&year.required';
      isValid = false;
    } else if (!this.month_end) {
      this.errors.occ_month_ended = 'form.month.required';
      isValid = false;
    } else if (!this.year_end) {
      this.errors.occ_month_ended = 'form.year.required';
      isValid = false;
    }

    return isValid;
  }

  convertDateForServer(month, year) {
    if (month && year) {
      return moment(month + year, 'MMMMYYYY').format(DATE_FORMAT_SERVER);
    }
  }

}
