import { Component, OnInit, ViewContainerRef } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { NavigationService } from '../../shared/navigation/navigation.service';
import { ApiService } from '../../service/api.service';
import { AutocompleteService } from '../../shared/autocomplete/autocomplete.service';
import { DATE_FORMAT_SERVER, PORTAL_PREFIX_PATH, API, PATTERN_STRING, ERRORS } from "../../config";
import { AddressDialogComponent } from "../../shared/address-dialog/address-dialog.component";
import { PaddingZeroService} from '../../service/padding-zero.service';

@Component({
  selector: 'loan-page4',
  templateUrl: './page4.component.html',
  styleUrls: ['./page4.component.scss']
})
export class LoanPage4Component implements OnInit {

  dialogRef:MdDialogRef<any>;
  index:number = -1;
  quote:any = {};
  address:any;
  searchStr:string;
  moved:string;
  addresses:Array<any> = [];
  shortAddressPeriod:boolean = true;
  model:any = {};
  private_information:any = {};
  errors:any = {};
  prefixPath:string = PORTAL_PREFIX_PATH;
  quoteId: number;

  constructor(private spinner:SpinnerService,
              private router:Router,
              private navigationService:NavigationService,
              private apiService:ApiService,
              private autocompleteService:AutocompleteService,
              private dialog:MdDialog,
              private viewContainerRef:ViewContainerRef,
              private route:ActivatedRoute,
              public paddingZeroService: PaddingZeroService) {
  }

  ngOnInit() {
    this.apiService.getMethod(API.GET_DASHBOARD).subscribe(
      response => {
        this.quoteId = response.content.quotes[0].quote_id;
      }
    );
    this.getAddresses();
    this.getUserDetails();
    this.getQuoteDetails();
    this.autocompleteService.address.subscribe(address => this.address = address);
  }

  openDialog() {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;

    this.dialogRef = this.dialog.open(AddressDialogComponent, config);

    this.dialogRef.componentInstance.index = this.index;
    this.dialogRef.componentInstance.searchStr = this.searchStr;
    this.dialogRef.componentInstance.moved = this.moved;

    this.dialogRef.afterClosed().subscribe(needRefetch => {
      this.index = -1;
      this.searchStr = '';
      this.moved = '';
      this.errors = {};
      this.dialogRef = null;
      if (needRefetch) {
        this.getAddresses();
      } else {
        this.spinner.stop();
      }
    });
  }

  editAddress(address) {
    this.index = address.id;
    this.autocompleteService.setAddress(address);
    this.searchStr = `${address.street} ${address.building_number}, ${address.city}, ${address.country}`;
    this.moved = address.moved;
    this.openDialog();
  }

  addAddress() {
    this.autocompleteService.setAddress(undefined);
    this.index = -1;
    this.searchStr = '';
    this.moved = '';
    this.openDialog();
  }

  getAddresses() {
    this.apiService.getMethod(API.ADDRESS).subscribe(
      response => {
        this.addresses = response;
        this.dateHandler(this.addresses);
      }
    )
  }

  getUserDetails() {
    this.apiService.getMethod(API.GET_USER_DETAILS).subscribe(
      response => {
        let content = response.content;
        if (content) {
          this.model.middle_name = content.middle_name;
          this.model.maiden_name = content.maiden_name;
          this.model.gender = content.gender;
          this.private_information.first_name = content.first_name;
          this.private_information.last_name = content.last_name;
          this.private_information.birthdate = moment(content.birthdate).format('D MMM YYYY');
        }
      }
    )
  }

  getQuoteDetails() {
    this.apiService.getMethod(API.GET_QUOTE_DETAILS).subscribe(
      response => {
        this.quote = response.content ? response.content : {};
      }
    );
  }

  onSubmit() {
    if (this.validation()) {
      let data = (<any>Object).assign({}, this.model);
      this.apiService.postMethod(API.UPDATE_USER_DETAILS, data).subscribe(
          response => {
            this.router.navigate([`${this.prefixPath}/loan/contact_information`]);
          }
      );
    }
  }

  convertDateForDisplay(date) {
    if (date) {
      return moment(date, DATE_FORMAT_SERVER).format('MMM, YYYY');
    }
  }

  convertDateForServer(date) {
    if (date) {
      return moment(date, 'DD/MM/YYYY').format(DATE_FORMAT_SERVER);
    }
  }

  validation() {
    let isValid = true;

    if (this.model.middle_name && PATTERN_STRING instanceof RegExp && !PATTERN_STRING.test(this.model.middle_name)) {
      this.errors.middleName = ERRORS['form.middleName.notString'] || 'form.middleName.notString';
      isValid = false;
    }

    if (this.model.maiden_name && PATTERN_STRING instanceof RegExp && !PATTERN_STRING.test(this.model.maiden_name)) {
      this.errors.maidenName = ERRORS['form.maidenName.notString'] || 'form.maidenName.notString';
      isValid = false;
    }

    return isValid;
  }

  dateHandler(addresses) {
    let minDate;
    if (addresses.length) {
      minDate = Math.min(...addresses.map(v => v.moved));
      this.shortAddressPeriod = moment().diff(moment(minDate, 'YYYYMMDD'), 'years') < 5;
    } else {
      this.shortAddressPeriod = true;
    }
  }

  back() {
    this.router.navigate([`${this.prefixPath}/dashboard`]);
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
