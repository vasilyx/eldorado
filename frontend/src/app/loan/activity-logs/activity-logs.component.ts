import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { PORTAL_PREFIX_PATH, API } from '../../config';
import { TextBuilderService } from "../../service/text_builder.service";

@Component({
  selector: 'activity-logs',
  templateUrl: './activity-logs.component.html',
  styleUrls: ['./activity-logs.component.scss']
})
export class ActivityLogsComponent implements OnInit {

  logs:Array<any> = [];
  count:number;
  page:number = 1;
  limit:number = 10;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private spinner: SpinnerService,
              private textBuilderService:TextBuilderService,
              private apiService: ApiService) {
  }

  ngOnInit() {
    this.getActivityLogs();
  }

  getActivityLogs() {
    this.apiService.getMethod(`${API.GET_USER_ACTIVITY_LOGS}?offset=${this.page}&limit=${this.limit}`).subscribe(
      response => {
        this.count = response.content.count;
        this.logs = response.content.logs;
        for (let item of this.logs) {
          item.displayedDate = moment(item.created_at).format('DD MMM YYYY kk:mm');
          item.displayedAction = this.textBuilderService.activityLogMessage(item.code, item);
        }
      }
    );
  }

  goToDashboard() {
    this.router.navigate([`${PORTAL_PREFIX_PATH}/dashboard`]);
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
