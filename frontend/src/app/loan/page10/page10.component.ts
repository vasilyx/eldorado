import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MdDialog, MdDialogConfig, MdDialogRef} from '@angular/material';

import {SpinnerService} from '../../shared/spinner/spinner.service';
import {ApiService} from '../../service/api.service';
import {API, CREDIT_TYPE_CODE, PORTAL_PREFIX_PATH} from '../../config';
import {ConfirmationModalDialog} from '../../shared/confirmation-dialog/confirmation-dialog.component';
import {DashboardService} from '../../service/dashboard.service';

@Component({
  selector: 'loan-page10',
  templateUrl: './page10.component.html',
  styleUrls: ['./page10.component.scss']
})
export class LoanPage10Component implements OnInit {

  dialogRef:MdDialogRef<any>;
  creditLines:any = [];
  allTerms:any = [];
  model:any = {};
  slider:any = {};
  errors:any = {};
  changedDeclutters:boolean = false;
  terms: boolean;

  constructor(private router:Router,
              private spinner:SpinnerService,
              private apiService:ApiService,
              private dialog: MdDialog,
              private dashboardService: DashboardService) {
    this.slider = {
      term: {
        min: 0,
        max: 48,
        step: 1,
        value: 0
      },
      monthlyPayment: {
        min: -48,
        max: 0,
        step: 1,
        value: -48
      }
    };
  }

  ngOnInit() {
    this.getCreditLines();
  }

  onSubmit() {
    const data = {
      loan_amount: this.model.deClutterLoanAmount,
      loan_term: this.model.term,
      loan_monthly: this.model.monthlyPayment,
      loan_apr: this.model.interestRate.toFixed(2)
    };
    if (this.changedDeclutters) {
      this.apiService.postMethod(API.CREDIT_LINE_DECLUTTER, this.createSubmitData()).subscribe(
        response => {
          this.apiService.postMethod(API.SUBMIT_LOAN_APPLICATION, data).subscribe(
            response => {
              this.apiService.getMethod(API.GET_DASHBOARD).subscribe(res => {
                this.dashboardService.changeDashboardData(res.content);
                this.router.navigate([`${PORTAL_PREFIX_PATH}/loan/status`]);
              });

            }
          );
        }
      );
    } else {
      this.apiService.postMethod(API.SUBMIT_LOAN_APPLICATION, data).subscribe(
        response => {
          this.apiService.getMethod(API.GET_DASHBOARD).subscribe(res => {
            this.dashboardService.changeDashboardData(res.content);
            this.router.navigate([`${PORTAL_PREFIX_PATH}/loan/status`]);
          });
        }
      );
    }
  }

  getCreditLines() {
    this.apiService.getMethod(API.CREDIT_LINE).subscribe(
      response => {
        this.creditLines = response.content;
        this.getDeClutters();
      }
    );
  }

  getDeClutters() {
    this.apiService.getMethod(API.CREDIT_LINE_DECLUTTER).subscribe(
      response => {
        if (response.content && response.content.length) {
          for (let item of response.content) {
            let creditLineId = item.user_creditline.id;
            for (let creditLine of this.creditLines) {
              if (creditLine.id === creditLineId) {
                creditLine.deClutter = true;
                creditLine.close = item.need_to_close;
              }
            }
          }
        }
        this.calculateTotalRow(this.creditLines);
        this.calculateDeClutterRow(this.creditLines);
        this.getQuoteDetails();
      }
    );
  }

  createSubmitData() {
    let data = [];
    this.creditLines.map(item => {
      if (item.deClutter) {
        data.push({
          user_creditline: {
            id: item.id
          },
          need_to_close: item.close ? '1' : '0'
        });
      }
    });

    return data;
  }

  getCreditType(code) {
    return CREDIT_TYPE_CODE[code];
  }

  changeDeClutterCheckbox(item) {
    item.close = item.deClutter;
    this.calculateDeClutterBalanceInput(this.creditLines);
    if (this.model.deClutterBalanceInput <= 30000) {
      this.calculateDeClutterRow(this.creditLines);
      this.changedDeclutters = true;
      this.getQuoteDetails();
    } else {
      this.showConfirmationDialog(item);
    }
  }

  changeCloseCheckbox() {
    this.changedDeclutters = true;
  }

  calculateTotalRow(items) {
    let totalBalanceInput = 0;
    let totalInterestRate = 0;
    let totalMonthlyPayment = 0;
    let totalTotalInterest = 0;
    let totalTotalToRepay = 0;
    items.map(item => {
      totalBalanceInput += item.balance_input;
      totalMonthlyPayment += item.monthly_payment;
      totalTotalInterest += item.total_interest;
      totalTotalToRepay += item.total_repaid;
    });
    items.map(item => {
      totalInterestRate += item.balance_input / totalBalanceInput * item.apr;
    });
    this.model.totalBalanceInput = totalBalanceInput ? totalBalanceInput : 0.00;
    this.model.totalInterestRate = totalInterestRate ? totalInterestRate.toFixed(2) : 0.00;
    this.model.totalMonthlyPayment = totalMonthlyPayment ? totalMonthlyPayment : 0.00;
    this.model.totalTotalInterest = totalTotalInterest ? totalTotalInterest : 0.00;
    this.model.totalTotalToRepay = totalTotalToRepay ? totalTotalToRepay : 0.00;
    this.model.totalMonthsToClear = totalTotalToRepay && totalMonthlyPayment ? Math.ceil(totalTotalToRepay / totalMonthlyPayment) : 0;
  }

  calculateDeClutterBalanceInput(items) {
    let deClutterBalanceInput = 0;
    items.map(item => {
      if (item.deClutter) {
        deClutterBalanceInput += item.balance_input;
      }
    });
    this.model.deClutterBalanceInput = deClutterBalanceInput ? deClutterBalanceInput : 0.00;
  }

  calculateDeClutterRow(items) {
    let deClutterBalanceInput = 0;
    let deClutterInterestRate = 0;
    let deClutterMonthlyPayment = 0;
    let deClutterTotalInterest = 0;
    let deClutterTotalToRepay = 0;
    items.map(item => {
      if (item.deClutter) {
        deClutterBalanceInput += item.balance_input;
        deClutterMonthlyPayment += item.monthly_payment;
        deClutterTotalInterest += item.total_interest;
        deClutterTotalToRepay += item.total_repaid;
      }
    });
    items.map(item => {
      if (item.deClutter) {
        deClutterInterestRate += item.balance_input / deClutterBalanceInput * item.apr;
      }
    });
    this.model.deClutterBalanceInput = deClutterBalanceInput ? deClutterBalanceInput : 0.00;
    this.model.deClutterInterestRate = deClutterInterestRate ? deClutterInterestRate.toFixed(2) : 0.00;
    this.model.deClutterMonthlyPayment = deClutterMonthlyPayment ? deClutterMonthlyPayment : 0.00;
    this.model.deClutterTotalInterest = deClutterTotalInterest ? deClutterTotalInterest : 0.00;
    this.model.deClutterTotalToRepay = deClutterTotalToRepay ? deClutterTotalToRepay : 0.00;
    this.model.deClutterMonthsToClear = deClutterTotalToRepay && deClutterMonthlyPayment ? Math.ceil(deClutterTotalToRepay / deClutterMonthlyPayment) : 0;
  }

  getQuoteDetails() {
    if (this.model.deClutterBalanceInput >= 1000) {
      if (this.model.showCalculateSection) {
        this.getAllTerms();
      } else {
        this.apiService.getMethod(API.GET_QUOTE_DETAILS).subscribe(
          response => {
            let content = response.content;
            if (content) {
              this.model.term = content.term;
              this.model.interestRate = content.contract_rate;
              this.model.amount = content.amount;
            }
            this.getAllTerms();
          }
        );
      }
    } else {
      this.model.showCalculateSection = false;
    }
  }

  getAllTerms() {
    this.model.deClutterLoanAmount = this.model.deClutterBalanceInput <= 30000 ? (Math.ceil(this.model.deClutterBalanceInput / 100) * 100).toFixed(2) : 30000;
    let url = `${API.GET_ALL_TERMS}?loan_amount=${this.model.deClutterLoanAmount}&interest_rate=${this.model.interestRate}`;
    this.apiService.getMethod(url).subscribe(
      response => {
        this.allTerms = response.content;
        let index = this.allTerms.findIndex(item => item.loan_term === this.model.term);
        this.slider.term.value = index;
        this.slider.monthlyPayment.value = -index;
        this.model.monthlyPayment = this.allTerms[index].contract_monthly_payment;
        this.model.totalInterest = this.allTerms[index].effective_total_interest;
        this.calculateSavedSection();
      }
    );
  }

  calculateSavedSection() {
    this.model.interestSaved = (this.model.deClutterTotalInterest - this.model.totalInterest).toFixed(2);
    this.model.monthsSaved = this.model.deClutterMonthsToClear - this.model.term;
    this.model.showCalculateSection = true;
  }

  changeMonthlyPaymentValue() {
    this.slider.term.value = -this.slider.monthlyPayment.value;
    this.model.monthlyPayment = this.allTerms[-this.slider.monthlyPayment.value].contract_monthly_payment;
    this.model.term = this.allTerms[this.slider.term.value].loan_term;
    this.model.totalInterest = this.allTerms[this.slider.term.value].effective_total_interest;
    this.calculateSavedSection();
  }

  changeTermValue() {
    this.slider.monthlyPayment.value = -this.slider.term.value;
    this.model.monthlyPayment = this.allTerms[-this.slider.monthlyPayment.value].contract_monthly_payment;
    this.model.term = this.allTerms[this.slider.term.value].loan_term;
    this.model.totalInterest = this.allTerms[this.slider.term.value].effective_total_interest;
    this.calculateSavedSection();
  }

  showConfirmationDialog(item) {
    let config = new MdDialogConfig();
    config.width = '470px';
    this.dialogRef = this.dialog.open(ConfirmationModalDialog, config);

    this.dialogRef.afterClosed().subscribe(result => {
      this.dialogRef = null;
      item.deClutter = false;
      item.close = false;
      this.calculateDeClutterBalanceInput(this.creditLines);
    });
  }

  back() {
    this.router.navigate([`${PORTAL_PREFIX_PATH}/loan/credit_lines`]);
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
