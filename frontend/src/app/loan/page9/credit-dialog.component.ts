import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { ERRORS, PATTERN_STRING, API, CREDIT_TYPE_CODE } from '../../config';

@Component({
  selector: 'credit-dialog',
  templateUrl: './credit-dialog.component.html',
  styleUrls: ['./credit-dialog.component.scss']
})
export class CreditDialogComponent {

  credit_line:any = {};
  index:number = -1;
  options:any;
  errors:any = {};

  constructor(public dialogRef:MdDialogRef<any>,
              public spinner:SpinnerService,
              public apiService:ApiService) {
  }

  ngOnInit() {
    this.options = (<any>Object).keys(CREDIT_TYPE_CODE).map(key => {
      return {code: key, value: CREDIT_TYPE_CODE[key]};
    });
    this.credit_line = (<any>Object).assign({}, this.credit_line);
  }

  addUpdateCreditLine() {
    if (this.validation()) {
      let data = (<any>Object).assign({}, this.credit_line);
      this.apiService.postMethod(API.CREDIT_LINE, data).subscribe(
        response => {
          this.dialogRef.close(true);
        },
        err => {
          if (err.status === 'error') {
            for (var key in err.content) {
              this.errors[key] = ERRORS[err.content[key].code] || err.content[key].code;
            }
          }
        }
      )
    }
  }

  deleteCreditLine(index) {
    this.apiService.deleteMethod(API.CREDIT_LINE, {id: index}).subscribe(
      response => {
        this.dialogRef.close(true);
      }
    )
  }

  validation() {
    let isValid = true;

    if (!this.credit_line.type) {
      this.errors.type = ERRORS['userCreditLine.type.isBlank'];
      isValid = false;
    }

    if (!this.credit_line.name) {
      this.errors.name = ERRORS['userCreditLine.name.isBlank'];
      isValid = false;
    } else if (PATTERN_STRING instanceof RegExp && !PATTERN_STRING.test(this.credit_line.name)) {
      this.errors.name = ERRORS['userCreditLine.name.notString'];
      isValid = false;
    }

    if (!this.credit_line.credit_limit) {
      this.errors.creditLimit = ERRORS['userCreditLine.creditLimit.isBlank'];
      isValid = false;
    }

    if (!this.credit_line.balance_input) {
      this.errors.balanceInput = ERRORS['userCreditLine.balanceInput.isBlank'];
      isValid = false;
    }

    if (!this.credit_line.apr) {
      this.errors.apr = ERRORS['userCreditLine.apr.isBlank'];
      isValid = false;
    }

    if (!this.credit_line.monthly_payment) {
      this.errors.monthlyPayment = ERRORS['userCreditLine.monthlyPayment.isBlank'];
      isValid = false;
    }

    return isValid;
  }
}
