import { Component, OnInit, ViewContainerRef } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import {
  CREDIT_TYPE_CODE,
  PORTAL_PREFIX_PATH,
  API,
  OFFER_TYPE_CODE,
  QUOTE_STATUS_CODE,
  OFFER_TERM_CODE, LOAN_STATUS_CODE, OFFER_RATE_CODE, OFFER_STATUS_CODE
} from '../../config';
import { CreditDialogComponent } from './credit-dialog.component';
import {PaddingZeroService} from '../../service/padding-zero.service';

@Component({
  selector: 'loan-page9',
  templateUrl: './page9.component.html',
  styleUrls: ['./page9.component.scss']
})
export class LoanPage9Component implements OnInit {

  dialogRef:MdDialogRef<any>;
  credit_lines:any = [];
  credit_line:any = {};
  quote:any = {};
  errors:any = {};
  prefixPath:string = PORTAL_PREFIX_PATH;
  index:number = -1;
  totalOwe:number = 0;
  totalMonthly: number = 0;
  quoteId: any;

  constructor(private spinner:SpinnerService,
              private router:Router,
              private apiService:ApiService,
              public dialog:MdDialog,
              public viewContainerRef:ViewContainerRef,
              private route:ActivatedRoute,
              public paddingZeroService: PaddingZeroService) {
  }

  ngOnInit() {
    this.apiService.getMethod(API.GET_DASHBOARD).subscribe(
      response => {
        this.quoteId = response.content.quotes[0].quote_id;
      }
    );
    this.getCreditLines();
    this.getQuoteDetails();
  }

  onSubmit() {
    this.router.navigate([`${this.prefixPath}/loan/credit_lines_declutter`]);
  }

  getCreditLines() {
    this.apiService.getMethod(API.CREDIT_LINE).subscribe(
      response => {
        this.credit_lines = response.content;
        this.calculateTotal();
      }
    );
  }

  calculateTotal() {
    this.totalOwe = 0;
    this.totalMonthly = 0;
    this.credit_lines.forEach(
      (credit) => {
        this.totalOwe += credit.balance_input;
        this.totalMonthly += credit.monthly_payment;
      }
    );
  }

  getQuoteDetails() {
    this.apiService.getMethod(API.GET_QUOTE_DETAILS).subscribe(
      response => {
        this.quote = response.content ? response.content : {};
      }
    );
  }

  getCreditType(code) {
    return CREDIT_TYPE_CODE[code];
  }

  showCreditLinePopup() {
    this.index = -1;
    this.credit_line = {};
    this.openDialog();
  }

  editCreditLine(credit_line) {
    this.index = credit_line.id;
    this.credit_line = credit_line;
    this.openDialog();
  }

  openDialog() {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.width = '480px';

    this.dialogRef = this.dialog.open(CreditDialogComponent, config);

    this.dialogRef.componentInstance.index = this.index;
    this.dialogRef.componentInstance.credit_line = this.credit_line;

    this.dialogRef.afterClosed().subscribe((needRefetch) => {
      this.dialogRef = null;
      if (needRefetch) {
        this.getCreditLines();
      }
    });
  }

  back() {
    this.router.navigate([`${this.prefixPath}/loan/dependent_information`]);
  }

  ngOnDestroy() {
    this.spinner.start();
  }

}
