import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SpinnerService } from '../../../shared/spinner/spinner.service';
import { ApiService } from '../../../service/api.service';
import { PATTERN_EMAIL, PATTERN_ADMIN_PASSWORD, ADMIN_FORGOT_PASSWORD_URL, API, ERRORS } from '../../../config';
import { StorageService } from "../../../service/storage.service";

@Component({
  selector: 'admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent implements OnInit {

  model:any = {};
  errors:any = {};
  successMessage:string;
  newPasswordsFields:boolean = false;
  forgotPasswordForm: boolean;

  constructor(private spinner:SpinnerService,
              private apiService:ApiService,
              private router:Router,
              private storageService:StorageService) {
  }

  ngOnInit() {
    this.spinner.stop();
  }

  login() {
    if (this.newPasswordsFields) {
      this.setNewPassword();
    } else {
      if (this.loginValidation()) {
        this.apiService.getMethod(API.ADMIN_TOKEN, {
          saveToken: true,
          authorizationToken: true,
          credentials: `${this.model.username}:${this.model.password}`
        }).subscribe(
          response => {
            if(response.status == 'success') {
              this.router.navigate(['/whadmin/clients']);
            }
          },
          err => {
            if (err.content.code === 'admin.token.passwordIsExpired') {
              this.openNewPasswordFields();
            }
          }
        );
      }
    }
  }

  setNewPassword() {
    if (this.setNewPasswordValidation()) {
      let data = {
        plain_password: this.model.plain_password,
        repeat_password: this.model.repeat_password
      };
      this.apiService.putMethod(API.ADMIN_SET_PASSWORD, data, {
        saveToken: true,
        authorizationToken: true,
        credentials: `${this.model.username}:${this.model.password}`
      }).subscribe(
        response => {
          this.newPasswordsFields = false;
          this.model.password = this.model.plain_password;
          this.login();
        },
        err => {
          if (err.status === 'error') {
            for (let key in err.content) {
              this.errors[key] = ERRORS[err.content[key].code] || err.content[key].message;
            }
          }
        }
      );
    }
  }

  openNewPasswordFields() {
    this.newPasswordsFields = true;
  }

  sendEmail() {
    if (this.sendEmailValidation()) {
      let data = {
        url: ADMIN_FORGOT_PASSWORD_URL
      };
      this.apiService.putMethod(API.ADMIN_FORGOT_PASSWORD_TOKEN, data, {
        authorizationToken: true,
        credentials: `${this.model.username}:`
      }).subscribe(
        response => {
          this.successMessage = 'Reset password link was sent to your email box. Please check it out.';
        }
      );
    }
  }

  loginValidation() {
    let isValid = true;

    if (!this.model.username) {
      this.errors.username = ERRORS['user.username.isBlank'];
      isValid = false;
    } else if (PATTERN_EMAIL instanceof RegExp && !PATTERN_EMAIL.test(this.model.username)) {
      this.errors.username = ERRORS['user.emailNotConfirmed'];
      isValid = false;
    }

    if (!this.model.password) {
      this.errors.password = ERRORS['user.password.isBlank'];
      isValid = false;
    }

    return isValid;
  }

  sendEmailValidation() {
    let isValid = true;

    if (!this.model.username) {
      this.errors.username = ERRORS['user.username.isBlank'];
      isValid = false;
    } else if (PATTERN_EMAIL instanceof RegExp && !PATTERN_EMAIL.test(this.model.username)) {
      this.errors.username = ERRORS['user.emailNotConfirmed'];
      isValid = false;
    }

    return isValid;
  }

  setNewPasswordValidation() {
    let isValid = true;

    if (!this.model.plain_password) {
      this.errors.plainPassword = ERRORS['form.password.isBlank'];
      isValid = false;
    } else if (this.model.plain_password.length < 12) {
      this.errors.plainPassword = ERRORS['admin.password.wrong'];
      isValid = false;
    } else if (PATTERN_ADMIN_PASSWORD instanceof RegExp && !PATTERN_ADMIN_PASSWORD.test(this.model.plain_password)) {
      this.errors.plainPassword = ERRORS['admin.password.wrong'];
      isValid = false;
    }

    if (!this.model.repeat_password) {
      this.errors.repeatPassword = ERRORS['form.password.isBlank'];
      isValid = false;
    } else if (this.model.repeat_password.length < 12) {
      this.errors.repeatPassword = ERRORS['admin.password.wrong'];
      isValid = false;
    } else if (PATTERN_ADMIN_PASSWORD instanceof RegExp && !PATTERN_ADMIN_PASSWORD.test(this.model.repeat_password)) {
      this.errors.repeatPassword = ERRORS['admin.password.wrong'];
      isValid = false;
    } else if (this.model.plain_password && this.model.plain_password !== this.model.repeat_password) {
      this.errors.repeatPassword = ERRORS['form.password.notConfirmed'];
      isValid = false;
    }

    return isValid;
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
