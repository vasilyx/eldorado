import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SpinnerService } from '../../../shared/spinner/spinner.service';
import { ApiService } from '../../../service/api.service';
import { PATTERN_ADMIN_PASSWORD, API, ERRORS } from '../../../config';

@Component({
  selector: 'admin-reset',
  templateUrl: './admin-reset.component.html',
  styleUrls: ['./admin-reset.component.scss']
})
export class AdminResetComponent implements OnInit {

  model:any = {};
  errors:any = {};
  token:string;

  constructor(private spinner:SpinnerService,
              private apiService: ApiService,
              private router:Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.spinner.stop();
    this.route.queryParams.subscribe(params => {
      if (params['t']) {
        this.token = params['t'];
      }
    });
  }

  onSubmit() {
    if (this.validation()) {
      let data = (<any>Object).assign({}, this.model);
      this.apiService.putMethod(API.ADMIN_RESET_PASSWORD, data, {
        authorizationToken: true,
        schema: 'FORGOTTEN',
        credentials: this.token
      }).subscribe(
        response => {
          this.router.navigate(['/whadmin/login']);
        }
      )
    }
  }

  validation() {
    let isValid = true;

    if (!this.model.plain_password) {
      this.errors.plainPassword = ERRORS['form.password.isBlank'];
      isValid = false;
    } else if (this.model.plain_password.length < 12) {
      this.errors.plainPassword = ERRORS['admin.password.wrong'];
      isValid = false;
    } else if (PATTERN_ADMIN_PASSWORD instanceof RegExp && !PATTERN_ADMIN_PASSWORD.test(this.model.plain_password)) {
      this.errors.plainPassword = ERRORS['admin.password.wrong'];
      isValid = false;
    }

    if (!this.model.repeat_password) {
      this.errors.repeatPassword = ERRORS['form.password.isBlank'];
      isValid = false;
    } else if (this.model.repeat_password.length < 12) {
      this.errors.repeatPassword = ERRORS['admin.password.wrong'];
      isValid = false;
    } else if (PATTERN_ADMIN_PASSWORD instanceof RegExp && !PATTERN_ADMIN_PASSWORD.test(this.model.repeat_password)) {
      this.errors.repeatPassword = ERRORS['admin.password.wrong'];
      isValid = false;
    } else if (this.model.plain_password && this.model.plain_password !== this.model.repeat_password) {
      this.errors.repeatPassword = ERRORS['form.password.notConfirmed'];
      isValid = false;
    }

    return isValid;
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
