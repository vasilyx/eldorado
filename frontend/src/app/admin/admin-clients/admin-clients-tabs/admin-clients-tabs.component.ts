import {Component, Input, OnInit, ViewContainerRef} from '@angular/core';
import {Router} from '@angular/router';
import {SpinnerService} from '../../../shared/spinner/spinner.service';
import {MdDialog, MdDialogConfig, MdDialogRef} from '@angular/material';
import {ClearSecurityDialogComponent} from '../../../shared/clear-security-dialog/clear-security-dialog.component';
import {API, QUOTE_STATUS_CODE, LOAN_STATUS_CODE} from '../../../config';
import {Subject} from 'rxjs/Subject';
import {ApiService} from '../../../service/api.service';

@Component({
  selector: 'app-admin-clients-tabs',
  templateUrl: './admin-clients-tabs.component.html',
  styleUrls: ['./admin-clients-tabs.component.scss']
})
export class AdminClientsTabsComponent implements OnInit {
  @Input() set client(name: string) {
    this.clientListName = name;
    this.sortingObj = {
      name: '',
      reversed: false
    };
    this.page = 1;
    this.getClients(this.clientListName);
  };

  clientListName: string;
  dialogRef: MdDialogRef<any>;
  clients: Array<any> = [];
  searchStr: string = '';
  count: number;
  page: number = 1;
  limit: number = 10;
  sortingObj: any;
  searchStrChanged: Subject<string> = new Subject<string>();
  quoteStatus = QUOTE_STATUS_CODE;
  loanStatus = LOAN_STATUS_CODE;

  constructor(private spinner: SpinnerService,
              private apiService: ApiService,
              private router: Router,
              private dialog: MdDialog,
              private viewContainerRef: ViewContainerRef) {
    this.searchStrChanged
      .debounceTime(1000)
      .distinctUntilChanged()
      .subscribe(newValue => {
        let oldValue = this.searchStr;
        this.searchStr = newValue;
        if (newValue !== oldValue) {
          this.page = 1;
          this.getClients(this.clientListName);
        }
      });
    this.sortingObj = {
      name: '',
      reversed: false
    }
  }

  ngOnInit() {

  }

  filterChanged(text: string) {
    this.searchStrChanged.next(text);
  }

  pageChanged() {
    this.getClients(this.clientListName);
  }

  getClients(client) {
    this.clients = [];
    let url = client == 'borrow' ? API.ADMIN_BORROWERS : API.ADMIN_INVESTORS;
    let tail = `?page=${this.page}&limit=${this.limit}`;
    this.searchStr.length > 1 ? tail += `&search_string=${this.searchStr}` : tail;
    this.sortingObj.name ? tail += `&order_field=${this.sortingObj.name}&order_direction=${this.sortingObj.reversed ? 'desc' : 'asc'}` : tail;
    this.apiService.getMethod(url + tail, {'authorizationToken': true}).subscribe(
      response => {
        this.clients = response.content.clients;
        this.count = response.content.count;
      }
    )
  }

  openDialog(client: any) {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.width = '480px';

    this.dialogRef = this.dialog.open(ClearSecurityDialogComponent, config);

    this.dialogRef.componentInstance.clientId = client.id;

    this.dialogRef.afterClosed().subscribe((needRedirect) => {
      this.dialogRef = null;
      if (needRedirect) {
        this.router.navigate([`/whadmin/client/${client.id}`]);
      }
    });
  }

  convertBirthDate(date) {
    if (date) {
      return moment(date).format('DD/MM/YYYY');
    }
  }

  sortTable(name: string) {
    if (this.sortingObj.name !== name) {
      this.sortingObj.name = name;
    } else {
      this.sortingObj.reversed = !this.sortingObj.reversed;
    }
    this.getClients(this.clientListName);
  }

  clientData(event, quote_id) {
    event.stopPropagation();
    if (this.clientListName == 'borrow') {
      this.router.navigate([`/whadmin/client-details/borrower/${quote_id}`]);
      /*window.open(window.location.protocol + '//' + window.location.host + `/#/whadmin/client-details/borrower/${quote_id}`, '_blank');*/
    }
    else {
      this.router.navigate([`/whadmin/client-details/investor/${quote_id}`]);
      /*window.open(window.location.protocol + '//' +window.location.host + `/#/whadmin/client-details/investor/${quote_id}`, '_blank');*/
    }
  }

  ngOnDestroy() {

  }
}
