import {Component, OnInit} from '@angular/core';


import 'rxjs/add/operator/debounceTime';

import {SpinnerService} from '../../shared/spinner/spinner.service';
import {TabSaverService} from '../../service/tab-saver.service';

@Component({
  selector: 'admin-clients',
  templateUrl: './admin-clients.component.html',
  styleUrls: ['./admin-clients.component.scss']
})
export class AdminClientsComponent implements OnInit {
  client: string = this.tabSaverService.getTab('adminClientTab') || 'borrow';

  constructor(private spinner: SpinnerService,
              private tabSaverService: TabSaverService) {
  }

  ngOnInit() {

  }

  changeClients(who) {
    if (this.client != who) {
      this.client = who;
      this.tabSaverService.tab('adminClientTab', who);
    }
  }

  ngOnDestroy() {
    this.spinner.start();
  }
}
