import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {MaterialModule} from '@angular/material';
import {TextMaskModule} from 'angular2-text-mask/';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import {AdminComponent} from './admin.component';
import {AdminLoginComponent} from './admin-auth/admin-login/admin-login.component';
import {AdminResetComponent} from './admin-auth/admin-reset/admin-reset.component';
import {AdminClientsComponent} from './admin-clients/admin-clients.component';
import {AdminClientInfoComponent} from './admin-client-info/admin-client-info.component';
import {AdminSettingsComponent} from './admin-settings/admin-settings.component';
import {AdminRoutingModule} from './admin-routing.module';
import {AdminHeaderComponent} from './admin-header/admin-header.component';
import {SharedModule} from '../shared/shared-module';

import {ClientPersonalInfoComponent} from './admin-client-info/shared/client-personal-info/client-personal-info.component';
import {ClientAddressInfoComponent} from './admin-client-info/shared/client-address-info/client-address-info.component';
import {ClientCommunicationInfoComponent} from './admin-client-info/shared/client-communication-info/client-communication-info.component';
import {AdminClientsTabsComponent} from './admin-clients/admin-clients-tabs/admin-clients-tabs.component';
import {InvestorDetailsComponent} from './client-details/investor-details/investor-details.component';
import {BorrowerDetailsComponent} from './client-details/borrower-details/borrower-details.component';
import {ClientDetailsComponent} from './client-details/client-details.component';
import {ClientOverviewBorrowerComponent} from './client-details/borrower-details/client-overview/client-overview.component';
import {ClientOverviewInvestorComponent} from './client-details/investor-details/client-overview/client-overview.component';
import {IdCheckComponent} from './client-details/borrower-details/id-check/id-check.component';
import {AffordabilityComponent} from './client-details/borrower-details/affordability/affordability.component';
import {CreditHistoryComponent} from './client-details/borrower-details/credit-history/credit-history.component';
import {QuotationComponent} from './client-details/quotation/quotation.component';
import {CreditDecisionComponent} from './client-details/borrower-details/credit-decision/credit-decision.component';
import {LoanStatusService} from '../service/loan-status.service';
import {AcceptanceComponent} from './client-details/borrower-details/acceptance/acceptance.component';
import {MatchingComponent} from './client-details/borrower-details/matching/matching.component';
import {OffersAndDetailsComponent} from './client-details/investor-details/offers-and-details/offers-and-details.component';
import {IdCheckInvestorComponent} from './client-details/investor-details/id-check-investor/id-check-investor.component';
import {QuoteStatusService} from '../service/quote-status.service';
import {AdminFinancesComponent} from './admin-finances/admin-finances.component';
import {AdminFinancesTabComponent} from './admin-finances/admin-finances-tab/admin-finances-tab.component';
import {AdminWithdrawTabComponent} from './admin-finances/admin-withdraw-tab/admin-withdraw-tab.component';
import {InstalmentsBorrowerComponent} from './client-details/borrower-details/instalments-borrower/instalments-borrower.component';
import {AdminTransactionsTabComponent} from './admin-finances/admin-transactions-tab/admin-transactions-tab.component';


@NgModule({
  imports: [
    AdminRoutingModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    NgbModule.forRoot(),
    TextMaskModule,
    SharedModule
  ],
  declarations: [
    AdminComponent,
    AdminLoginComponent,
    AdminResetComponent,
    AdminClientsComponent,
    AdminSettingsComponent,
    AdminClientsTabsComponent,
    AdminHeaderComponent,
    AdminClientInfoComponent,
    ClientPersonalInfoComponent,
    ClientAddressInfoComponent,
    ClientCommunicationInfoComponent,
    InvestorDetailsComponent,
    ClientDetailsComponent,
    BorrowerDetailsComponent,
    ClientOverviewBorrowerComponent,
    ClientOverviewInvestorComponent,
    IdCheckComponent,
    AffordabilityComponent,
    CreditHistoryComponent,
    QuotationComponent,
    CreditDecisionComponent,
    AcceptanceComponent,
    MatchingComponent,
    InstalmentsBorrowerComponent,
    OffersAndDetailsComponent,
    IdCheckInvestorComponent,
    AdminFinancesComponent,
    AdminFinancesTabComponent,
    AdminWithdrawTabComponent,
    AdminTransactionsTabComponent],
  providers: [
    LoanStatusService,
    QuoteStatusService
  ],
  entryComponents: [

  ]
})
export class AdminModule {
}