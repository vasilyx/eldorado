import { Component, Input } from '@angular/core';

@Component({
  selector: 'client-communication-info',
  templateUrl: './client-communication-info.component.html',
  styleUrls: ['./client-communication-info.component.scss']
})
export class ClientCommunicationInfoComponent {

  @Input() communications:any = [];

  constructor() {}
}
