import { Component, Input } from '@angular/core';

import { DATE_FORMAT_SERVER } from "../../../../config";

@Component({
  selector: 'client-personal-info',
  templateUrl: './client-personal-info.component.html',
  styleUrls: ['./client-personal-info.component.scss']
})
export class ClientPersonalInfoComponent {

  @Input() personal_information:any = {};

  constructor() {}

  displayAgesOfDependents(dependents) {
    if (dependents.length) {
      let ages = [];
      for (let item in dependents) {
        ages.push(moment().diff(moment(dependents[item].birthdate, DATE_FORMAT_SERVER), 'years'));
      }
      return ages.join(', ');
    }
    return '-';
  }

  convertDateForDisplay(date) {
    if (date) {
      return moment(date, DATE_FORMAT_SERVER).format('D MMM YYYY');
    } else {
      return '-';
    }
  }
}
