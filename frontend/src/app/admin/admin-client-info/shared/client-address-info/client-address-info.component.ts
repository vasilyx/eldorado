import { Component, Input } from '@angular/core';

import { DATE_FORMAT_SERVER } from "../../../../config";

@Component({
  selector: 'client-address-info',
  templateUrl: './client-address-info.component.html',
  styleUrls: ['./client-address-info.component.scss']
})
export class ClientAddressInfoComponent {

  @Input() addresses:any = [];

  constructor() {}

  convertDateForDisplay(date) {
    if (date) {
      return moment(date, DATE_FORMAT_SERVER).format('MMM, YYYY');
    }
  }
}
