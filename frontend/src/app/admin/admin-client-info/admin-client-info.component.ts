import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ApiService } from "../../service/api.service";
import { TextBuilderService } from "../../service/text_builder.service";
import { API, COMMUNICATION_CODE } from '../../config';

@Component({
  selector: 'app-admin-client-info',
  templateUrl: './admin-client-info.component.html',
  styleUrls: ['./admin-client-info.component.scss']
})
export class AdminClientInfoComponent implements OnInit {

  clientId:string;

  personal_information:any = {};

  addresses:any = [];

  communications:any = [];

  logs:Array<any> = [];
  count:number;
  page:number = 1;
  limit:number = 10;

  constructor(private apiService:ApiService,
              private textBuilderService:TextBuilderService,
              private route:ActivatedRoute) {}

  ngOnInit() {
    this.clientId = this.route.snapshot.params['id'];
    this.getPersonalInfo();
    this.getAddresses();
    this.getCommunications();
    this.getActivityLogs();
  }

  getPersonalInfo() {
    this.apiService.getMethod(API.ADMIN_CLIENT_DETAILS.replace(/(\{id\})/i, this.clientId), {'authorizationToken': true}).subscribe(
      response => {
        this.personal_information = response.content;
      }
    )
  }

  getAddresses() {
    this.apiService.getMethod(API.ADMIN_CLIENT_ADDRESSES.replace(/(\{id\})/i, this.clientId), {'authorizationToken': true}).subscribe(
      response => {
        this.addresses = response.content;
      }
    )
  }

  getCommunications() {
    this.apiService.getMethod(API.ADMIN_CLIENT_COMMUNICATION.replace(/(\{id\})/i, this.clientId), {'authorizationToken': true}).subscribe(
      response => {
        let content = response.content;
        if (content.length) {
          for (let item in content) {
            this.communications.push({
              label: COMMUNICATION_CODE[content[item].type].label,
              value: content[item].value
            });
          }
        }
      }
    )
  }

  getActivityLogs() {
    this.apiService.getMethod(`${API.ADMIN_CLIENT_ACTIVITY_LOGS}?page=${this.page}&limit=${this.limit}`.replace(/(\{id\})/i, this.clientId), {'authorizationToken': true}).subscribe(
      response => {
        this.count = response.content.count;
        this.logs = response.content.logs;
        for (let item of this.logs) {
          item.displayedDate = moment(item.created_at).format('DD MMM YYYY kk:mm');
          item.displayedAction = this.textBuilderService.activityLogMessage(item.code, item);
        }
      }
    );
  }
}
