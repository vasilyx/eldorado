import {Component, Input, OnInit} from '@angular/core';
import {SpinnerService} from '../../../shared/spinner/spinner.service';
import {Router} from '@angular/router';
import {API} from '../../../config';
import {ApiService} from '../../../service/api.service';

@Component({
  selector: 'app-admin-finances-tab',
  templateUrl: './admin-finances-tab.component.html',
  styleUrls: ['./admin-finances-tab.component.scss']
})
export class AdminFinancesTabComponent implements OnInit {
  @Input() client;

  credit_lines: Array<any> = [];

  constructor(private spinner: SpinnerService,
              private apiService: ApiService,
              private router: Router,
) {}

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.apiService.getMethod(API.ADMIN_PENDING_CREDIT_LINES, {'authorizationToken': true}).subscribe(
      response => {
        this.credit_lines = response.content;
        this.spinner.stop();
      }
    )
  }

  pay(id) {
    this.spinner.start();
    this.apiService.postMethod(API.ADMIN_SET_CREDIT_LINES, {id},  {'authorizationToken': true}).subscribe(
      response => {
        this.getData();
      }
    )
  }

  ngOnDestroy() {

  }
}
