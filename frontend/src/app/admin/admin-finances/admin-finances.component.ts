import { Component, OnInit } from '@angular/core';
import {SpinnerService} from '../../shared/spinner/spinner.service';

@Component({
  selector: 'app-admin-finances',
  templateUrl: './admin-finances.component.html',
  styleUrls: ['./admin-finances.component.scss']
})
export class AdminFinancesComponent implements OnInit {

  type: string = 'finances';
  constructor(private spinner: SpinnerService) {
  }

  ngOnInit() {

  }

  changeClients(who) {
    if(this.type != who) {
      this.type = who;
    }
  }

  ngOnDestroy() {
    this.spinner.start();
  }

}
