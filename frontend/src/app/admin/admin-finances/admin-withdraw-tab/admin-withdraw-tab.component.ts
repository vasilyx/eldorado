import {Component, Input, OnInit, OnDestroy} from '@angular/core';
import {ApiService} from '../../../service/api.service';
import {API} from '../../../config';
import {SpinnerService} from '../../../shared/spinner/spinner.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-withdraw-tab',
  templateUrl: './admin-withdraw-tab.component.html',
  styleUrls: ['./admin-withdraw-tab.component.scss']
})
export class AdminWithdrawTabComponent implements OnInit {
  @Input() client;

  withdraws: Array<any> = [];

  constructor(private spinner: SpinnerService,
              private apiService: ApiService,
              private router: Router,
  ) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.apiService.getMethod(API.ADMIN_WITHDRAWS, {'authorizationToken': true}).subscribe(res => {
      this.withdraws = res.content;
    });
  }

  confirmWithdraw(id) {
    this.spinner.start();
    this.apiService.postMethod(API.ADMIN_CONFIRM_WITHDRAWS, {id},  {'authorizationToken': true}).subscribe(
      response => {
        this.getData();
      }
    )
  }
  convertDateForDisplay(date) {
    if (date) {
      return moment(date).format('D MMM YYYY');
    }
  }

  ngOnDestroy() {

  }
}