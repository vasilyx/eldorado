import {Component, Input, OnInit, OnDestroy} from '@angular/core';
import {ApiService} from '../../../service/api.service';
import {API, TRANSACTION_OWNER_STATUS,TRANSACTION_GLACCOUNT_STATUS, TRANSACTION_ACCOUNT_STATUS,
  TRANSACTION_SUB_ACCOUNT_STATUS} from '../../../config';
import {SpinnerService} from '../../../shared/spinner/spinner.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-transactions-tab',
  templateUrl: './admin-transactions-tab.component.html',
  styleUrls: ['./admin-transactions-tab.component.scss']
})
export class AdminTransactionsTabComponent implements OnInit {
  @Input() client;

  count: number;
  page: number = 1;
  limit: number = 10;
  transactions: Array<any> = [];
  transcationOwnerStatus = TRANSACTION_OWNER_STATUS;
  transcationGlAccountStatus = TRANSACTION_GLACCOUNT_STATUS;
  transcationAccountStatus = TRANSACTION_ACCOUNT_STATUS;
  transcationSubAccountStatus = TRANSACTION_SUB_ACCOUNT_STATUS;

  constructor(private spinner: SpinnerService,
              private apiService: ApiService,
              private router: Router,
  ) {
  }

  ngOnInit() {
    this.getTransactions()
  }

  pageChanged() {
    this.getTransactions();
  }

  getTransactions() {
    this.transactions = [];
    let tail = `?page=${this.page}&limit=${this.limit}`;
    this.apiService.getMethod(API.ADMIN_TRANSACTIONS + tail, {'authorizationToken': true}).subscribe(
      response => {
        console.log(response);
        this.transactions = response.content.rows;
        this.count = response.content.count;
      }
    )
  }

  ngOnDestroy() {

  }
}