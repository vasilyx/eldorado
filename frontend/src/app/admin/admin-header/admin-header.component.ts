import { Component, Input, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from "../../service/api.service";
import { API } from "../../config";

@Component({
  selector: 'admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss']
})
export class AdminHeaderComponent {

  @Input() activeTab:string;
  mobileMenu:boolean = false;

  constructor(private apiService:ApiService,
              private router:Router) {
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.mobileMenu = event.currentTarget.innerWidth < 1200;
  }

  @HostListener('window:load', ['$event'])
  onload(event) {
    this.mobileMenu = event.currentTarget.innerWidth < 1200;
  }

  logout() {
    this.apiService.deleteMethod(API.ADMIN_TOKEN, {}, {
      authorizationToken: true,
      deleteToken: true,
      hideLoader: true
    }).subscribe(
      response => {
        this.router.navigate(['/whadmin/login']);
      }
    );
  }
}
