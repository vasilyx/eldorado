import { Component, OnInit, ElementRef, Renderer, ViewContainerRef } from '@angular/core';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';
import { Router } from '@angular/router';

import { SpinnerService } from '../../shared/spinner/spinner.service';
import { ApiService } from '../../service/api.service';
import { API } from '../../config';
import { TextBuilderService } from "../../service/text_builder.service";
import { ClearSecurityDialogComponent } from '../../shared/clear-security-dialog/clear-security-dialog.component'

@Component({
  selector: 'admin-settings',
  templateUrl: './admin-settings.component.html',
  styleUrls: ['./admin-settings.component.scss']
})
export class AdminSettingsComponent implements OnInit {

  logs:Array<any> = [];
  count:number;
  page:number = 1;
  limit:number = 10;
  subscription:any;
  dialogRef:MdDialogRef<any>;

  constructor(private spinner:SpinnerService,
              private apiService:ApiService,
              private textBuilderService:TextBuilderService,
              private router:Router,
              private dialog:MdDialog,
              private viewContainerRef:ViewContainerRef,
              private renderer:Renderer) {}

  ngOnInit() {
    this.getActivityLogs();
  }

  getActivityLogs() {
    this.apiService.getMethod(`${API.ADMIN_ACTIVITY_LOGS}?page=${this.page}&limit=${this.limit}`, {'authorizationToken': true}).subscribe(
      response => {
        this.count = response.content.count;
        this.logs = response.content.logs;
        for (let item of this.logs) {
          item.displayedDate = moment(item.created_at).format('DD MMM YYYY kk:mm');
          item.displayedAction = this.textBuilderService.activityLogMessage(item.code, item);
        }
        //workaround for register click event for the link
        setTimeout(() => {
          let listOfLinks = document.getElementsByClassName('link');
          for (let i = 0; i < listOfLinks.length; i++) {
            this.subscription = this.renderer.listen(listOfLinks[i], 'click', () => {
              this.openClientDetails((<HTMLElement>listOfLinks[i]).dataset);
            });
          }
        }, 0);
      }
    );
  }

  openClientDetails(data) {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.width = '480px';

    this.dialogRef = this.dialog.open(ClearSecurityDialogComponent, config);

    this.dialogRef.componentInstance.clientId = data.clientId;

    this.dialogRef.afterClosed().subscribe((needRedirect) => {
      this.dialogRef = null;
      if (needRedirect) {
        this.router.navigate([`/whadmin/client/${data.clientId}`]);
      }
    });
  }

  ngOnDestroy() {
    this.spinner.start();
    if(this.subscription) {
      this.subscription();
    }
  }
}
