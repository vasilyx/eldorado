import {Component, EventEmitter, Input, OnInit, Output, OnDestroy} from '@angular/core';
import {API} from '../../../../config';
import { ApiService} from '../../../../service/api.service';
import {Headers, RequestOptions, ResponseContentType, Http} from '@angular/http';
import {StorageService} from '../../../../service/storage.service';
import { PaddingZeroService} from '../../../../service/padding-zero.service';
import { LoanStatusService} from '../../../../service/loan-status.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-matching',
  templateUrl: './matching.component.html',
  styleUrls: ['./matching.component.scss']
})
export class MatchingComponent implements OnInit, OnDestroy {
  @Input() id;
  @Output() tab: EventEmitter<any> = new EventEmitter();
  matching: number;
  investors: Array<any> = [];
  loanStatus: any;
  loanSubscription: Subscription;

  constructor(
    private apiService: ApiService,
    private http: Http,
    private storageService: StorageService,
    public paddingZeroService: PaddingZeroService,
    private loanStatusService: LoanStatusService
  ) { }

  ngOnInit() {
    this.apiService.getMethod(API.ADMIN_BORROWER_MATCHING_DETAILS.replace(/(\{quote_id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content).subscribe(
      res => {
        this.matching = res.matching;
        this.investors = res.investors;
      }
    );

    this.loanSubscription = this.loanStatusService.loanStatus$.subscribe(res => {
      this.loanStatus = res;
    });
  }

  changeTab(tab) {
    this.tab.emit(tab);
  }

  pdf() {
    const saveBlob = (function () {
      let a = document.createElement('a');
      document.body.appendChild(a);
      return function (blob, fileName) {
        let url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = fileName;
        a.click();
        window.URL.revokeObjectURL(url);
      };
    }());
    const headers = new Headers({
      'Auth': 'whs' + this.storageService.read('token'),
      'Authorization': 'Bearer ' + this.storageService.read('token'),
      'Content-Type': 'application/pdf'
    });
    let options = new RequestOptions({headers: headers});
    options.responseType = ResponseContentType.Blob;
    let url = API.ADMIN_CONTRACT_PDF.replace(/(\{loan_apply_id\})/i, this.investors[0].request_id);
    this.http.get(url, options).subscribe(
      (response) => {
        if (response.status === 200) {
          const blob = new Blob([response.blob()], {type: 'application/pdf'});
          saveBlob(blob, 'Agreement.pdf');
        }
      });
  }

  ngOnDestroy() {
    this.loanSubscription.unsubscribe();
  }
}

