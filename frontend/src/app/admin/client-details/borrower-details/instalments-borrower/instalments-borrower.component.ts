import {Component, EventEmitter, Input, OnInit, Output, ViewContainerRef, OnDestroy} from '@angular/core';
import {API, PAYMENT_STATUS, INSTALLMENT_STATUS} from '../../../../config';
import {ApiService} from '../../../../service/api.service';
import {PaymentDialogComponent} from './payment-dialog/payment-dialog.component';
import {MdDialog, MdDialogConfig, MdDialogRef} from '@angular/material';
import {LoanStatusService} from '../../../../service/loan-status.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-instalments-borrower',
  templateUrl: './instalments-borrower.component.html',
  styleUrls: ['./instalments-borrower.component.scss']
})
export class InstalmentsBorrowerComponent implements OnInit, OnDestroy {
  @Input() id;
  @Output() tab: EventEmitter<any> = new EventEmitter();

  instalmentsData: Array<any> = [];
  dialogRef: MdDialogRef<any>;
  paymentStatus = PAYMENT_STATUS;
  instalmentStatus = INSTALLMENT_STATUS;
  loanStatus: any;
  loanSubscription: Subscription;

  constructor(private apiService: ApiService,
              public dialog: MdDialog,
              public viewContainerRef: ViewContainerRef,
              private loanStatusService: LoanStatusService) { }

  ngOnInit() {
    this.getInstalments();
  }

  getInstalments() {
    this.apiService.getMethod(API.ADMIN_INSTALMENTS.replace(/(\{quote_id\})/i, this.id), {'authorizationToken': true}).subscribe(res => {
      this.instalmentsData = res.content;
    });

    this.loanSubscription = this.loanStatusService.loanStatus$.subscribe(res => {
      this.loanStatus = res;
    });
  }

  convertDateForDisplay(date) {
    if (date) {
      return moment(date).format('D MMM YYYY');
    }
  }

  changeTab(tab) {
    this.tab.emit(tab);
  }

  pay(id ) {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;
    config.width = '200px';
    this.dialogRef = this.dialog.open(PaymentDialogComponent, config);
    this.dialogRef.afterClosed().subscribe((data) => {
      this.dialogRef = null;
      if(data) {
        this.apiService.postMethod(API.ADMIN_PAY_INSTALMENT, {
          id: id,
          amount: data
        }, {'authorizationToken': true}).subscribe(res => {
          this.getInstalments();
        })
      }
    });
  }

  generateInstalment() {
    this.apiService.postMethod(API.ADMIN_GENERATE_INSTALMENT.replace(/(\{quote_id\})/i, this.id),{},{'authorizationToken': true}).subscribe(res => {
      this.getInstalments();
    })
  }

  ngOnDestroy() {
    this.loanSubscription.unsubscribe();
  }
}