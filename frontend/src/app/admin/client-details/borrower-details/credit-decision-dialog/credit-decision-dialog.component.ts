import { Component, OnInit } from '@angular/core';
import {CheckDialogComponent} from '../check-dialog/check-dialog.component';
import {MdDialogRef} from '@angular/material';

@Component({
  selector: 'app-credit-decision-dialog',
  templateUrl: './credit-decision-dialog.component.html',
  styleUrls: ['./credit-decision-dialog.component.scss']
})
export class CreditDecisionDialogComponent implements OnInit {

  constructor(public dialogRef:MdDialogRef<CheckDialogComponent>) { }

  ngOnInit() {
  }

}
