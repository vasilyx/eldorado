import { Component, OnInit, OnDestroy } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { ApiService} from '../../../service/api.service';
import {API} from '../../../config';
import { LoanStatusService} from '../../../service/loan-status.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-borrower-details',
  templateUrl: './borrower-details.component.html',
  styleUrls: ['./borrower-details.component.scss']
})
export class BorrowerDetailsComponent implements OnInit, OnDestroy {

  public tab: string;
  public id: number;
  public loanStatus;
  loanSubscription: Subscription;

  constructor(private route: ActivatedRoute,
              private apiService: ApiService,
              private loanStatusService: LoanStatusService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.apiService.getMethod(API.ADMIN_BORROWER_LOAN_APPLY_DETAILS.replace(/(\{quote_id\})/i, '' + this.id), {'authorizationToken': true} ).map(res => res.content).subscribe(res => {
      this.loanStatus = res.status;
      this.loanStatusService.changeLoanStatus(res.status);
    });

    this.loanSubscription = this.loanStatusService.loanStatus$.subscribe(res => {
      this.loanStatus = res;
      this.switchFunction(this.loanStatus);
    })
  }

  switchFunction(loanStatus) {
    switch (loanStatus) {
      case 100:
        this.tab = 'quotation';
        break;
      case 200:
        this.tab = 'overview';
        break;
      case 300:
        this.tab = 'check';
        break;
      case 320:
        this.tab = 'affordability';
        break;
      case 330:
        this.tab = 'history';
        break;
      case 350:
      case 450:
        this.tab = 'decision';
        break;
      case 400:
        this.tab = 'acceptance';
        break;
      case 500:
      case 550:
        this.tab = 'matching';
        break;
      case 600:
      case 700:
      case 800:
        this.tab = 'installments';
        break;
      default:
        this.tab = '';
    }
  }

  changeTab(tab) {
    if(this.tab !== tab) {
      this.tab = tab;
    }
  }

  ngOnDestroy() {
    this.loanSubscription.unsubscribe();
  }
}
