import {Component} from '@angular/core';
import {MdDialogRef} from '@angular/material';

@Component({
  selector: 'app-check-dialog',
  templateUrl: './check-dialog.component.html',
  styleUrls: ['./check-dialog.component.scss']
})
export class CheckDialogComponent {

  public comment: string = '';
  constructor(public dialogRef:MdDialogRef<CheckDialogComponent>) { }
}
