import {Component, EventEmitter, Input, OnInit, Output, OnDestroy} from '@angular/core';
import {API, CREDIT_TYPE_CODE, DATE_FORMAT_SERVER, RATE, COMMUNICATION_CODE} from '../../../../config';
import {ApiService} from '../../../../service/api.service';
import {LoanStatusService} from '../../../../service/loan-status.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-client-overview-borrower',
  templateUrl: './client-overview.component.html',
  styleUrls: ['./client-overview.component.scss']
})
export class ClientOverviewBorrowerComponent implements OnInit, OnDestroy {
  @Input() id;
  @Input() client;
  @Output() tab: EventEmitter<any> = new EventEmitter();

  addresses: Observable<any[]>;
  details: any = {};
  finances: any = {};
  rate: string;
  terms: any = {};
  idCheck: any = {};
  loanStatus: any;
  histories: Observable<any[]>;
  debts: Observable<any[]>;
  household: any = {};
  dependants: any = {};
  social_contacts: Observable<any[]>;
  totaldebts: number = 0;
  totalbalance: number = 0;
  creditType = CREDIT_TYPE_CODE;
  contacts = COMMUNICATION_CODE;
  loanSubscription: Subscription;

  constructor(private apiService: ApiService,
              private loanStatusService: LoanStatusService) {
  }

  ngOnInit() {
    this.addresses = this.apiService.getMethod(API.ADMIN_BORROWER_CLIENT.replace(/(\{id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content);

    this.apiService.getMethod(API.ADMIN_BORROWER_FINANCES.replace(/(\{id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content).subscribe(res => {
      this.finances = res;
      this.rate = RATE[this.finances.rate];
    });

    this.apiService.getMethod(API.ADMIN_BORROWER_DETAILS.replace(/(\{id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content).subscribe(res => {
      this.details = res;
    });

    this.apiService.getMethod(API.ADMIN_BORROWER_TERMS.replace(/(\{id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content).subscribe(res => {
      this.terms = res;
    });

    this.apiService.getMethod(API.ADMIN_BORROWER_ID_CHECK.replace(/(\{id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content).subscribe(res => {
      this.idCheck = res;
    });

    this.histories = this.apiService.getMethod(API.ADMIN_HISTORY.replace(/(\{id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content);

    this.debts = this.apiService.getMethod(API.ADMIN_DEBTS.replace(/(\{id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content);

    this.debts.subscribe(res => {
      res.forEach((item, i) => {
        this.totaldebts += item.credit_line.monthly_payment;
        this.totalbalance += item.credit_line.credit_balance_now;
      });
    });

    this.household = this.apiService.getMethod(API.ADMIN_HOUSEHOLD.replace(/(\{id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content).subscribe(res => {
      this.household = res[0];
    });

    this.apiService.getMethod(API.ADMIN_DEPENDANTS.replace(/(\{id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content).subscribe(res => {
      this.dependants = res;
    });

    this.social_contacts = this.apiService.getMethod(API.ADMIN_SOCIAL_CONTACTS.replace(/(\{id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content);

    this.loanSubscription = this.loanStatusService.loanStatus$.subscribe(res => {
      this.loanStatus = res;
    });
  }

  convertDateForDisplay(date) {
    if (date) {
      return moment(date, DATE_FORMAT_SERVER).format('MMM, YYYY');
    }
  }

  convertBirthDate(date) {
    if (date) {
      return moment(date).format('DD/MM/YYYY');
    }
  }

  changeTab(tab) {
    this.tab.emit(tab);
  }

  sum(...args) {
    let sum = 0;
    [...args].forEach((item)=>
    {
      if(typeof parseFloat(item) ==  'number') {
        sum += parseFloat(item)
      }
    });
    return sum
  }

  ngOnDestroy() {
    this.loanSubscription.unsubscribe();
  }
}
