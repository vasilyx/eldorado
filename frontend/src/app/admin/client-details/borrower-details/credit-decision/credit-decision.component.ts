import {Component, EventEmitter, Input, OnInit, Output, ViewContainerRef, OnDestroy} from '@angular/core';
import {API} from '../../../../config';
import {ApiService} from '../../../../service/api.service';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';
import {CreditDecisionDialogComponent} from '../credit-decision-dialog/credit-decision-dialog.component';
import {LoanStatusService} from '../../../../service/loan-status.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-credit-decision',
  templateUrl: './credit-decision.component.html',
  styleUrls: ['./credit-decision.component.scss']
})
export class CreditDecisionComponent implements OnInit, OnDestroy {
  @Input() id;
  @Output() tab: EventEmitter<any> = new EventEmitter();
  content: any = {};
  decision: any = {};
  dialogRef:MdDialogRef<any>;
  loanStatus: any;
  checkGenerate: Array<any> = [];
  loanSubscription: Subscription;

  constructor(private apiService: ApiService,
              private viewContainerRef:ViewContainerRef,
              private dialog:MdDialog,
              private loanStatusService: LoanStatusService) { }

  ngOnInit() {
    this.apiService.getMethod(API.ADMIN_BORROWER_QUATATION.replace(/(\{quote_id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content).subscribe(res => {
      this.content = res;
      this.checkGenerate = Object.keys(this.content.generated);
      this.decision = Object.assign({}, this.content.generated);
    });
    this.loanSubscription = this.loanStatusService.loanStatus$.subscribe(res => {
      this.loanStatus = res;
    })
  }

  accept(comment) {
    const data = {
      quote_id: this.id,
      amount: this.decision.loan_amount,
      term: this.decision.term,
      apr: this.decision.apr,
      monthly_repayment: this.decision.monthly,
      bonus_amount: this.decision.bonus_amount,
      bonus_rate: this.decision.bonus_rate,
      comment: comment
    };
    this.apiService.postMethod(API.ADMIN_CREDIT_CONFIRM_DECISION, data, {'authorizationToken': true}).subscribe(
      response => {
        this.loanStatusService.changeLoanStatus(response.content.loan_apply_status);
      }
    )
  }

  decline() {
    this.apiService.postMethod(API.ADMIN_CREDIT_DECLINE_DECISION, {quote_id: this.id}, {'authorizationToken': true}).subscribe(
      response => {
        this.loanStatusService.changeLoanStatus(response.content.loan_apply_status);
      }
    )
  }

  changeTab(tab) {
    this.tab.emit(tab);
  }

  openConfirmDialog(decision, comment = '') {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;
    config.width = '210px';

    this.dialogRef = this.dialog.open(CreditDecisionDialogComponent, config);

    this.dialogRef.afterClosed().subscribe((data) => {
      this.dialogRef = null;
      if(data && data.confirm) {
        if(decision) {
          this.accept(comment);
        }
        else {
          this.decline();
        }
      }
    });
  }

  changeData() {
    const data = {
      loan_term: parseFloat(this.decision.term),
      loan_amount: parseFloat(this.decision.loan_amount),
      loan_apr: parseFloat(this.decision.apr),
      loan_bonus_rate: parseFloat(this.decision.bonus_rate)
    };
    const url = Object.keys(data).map(function(key) {
      return key + '=' + data[key];
    }).join('&');
    this.apiService.getMethod(API.ADMIN_CALCULATE_LOAN + `?${url}`, {'authorizationToken': true}).subscribe(
      response => {
        this.decision.monthly = response.content.monthly_payment;
        this.decision.bonus_amount = response.content.bonus_amount;
      }
    )

  }

  ngOnDestroy() {
    this.loanSubscription.unsubscribe();
  }

}
