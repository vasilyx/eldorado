import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {API, DATE_FORMAT_SERVER} from '../../../../config';
import { ApiService} from '../../../../service/api.service';

@Component({
  selector: 'app-acceptance',
  templateUrl: './acceptance.component.html',
  styleUrls: ['./acceptance.component.scss']
})
export class AcceptanceComponent implements OnInit {
  @Input() id;
  @Output() tab: EventEmitter<any> = new EventEmitter();
  acceptance: any = {};

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getMethod(API.ADMIN_BORROWER_ACCEPTENCE.replace(/(\{quote_id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content).subscribe(
      res => {
        this.acceptance = res;
      }
    )
  }

  convertDateForDisplay(date) {
    if (date) {
      return moment(date).format('DD MMMM YYYY, HH:mm:ss');
    }
  }

  changeTab(tab) {
    this.tab.emit(tab);
  }
}
