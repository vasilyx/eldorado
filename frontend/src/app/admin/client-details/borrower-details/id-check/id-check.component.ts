import {Component, Input, OnInit, ElementRef, ViewContainerRef, Output, EventEmitter} from '@angular/core';
import {API} from '../../../../config';
import {ApiService} from '../../../../service/api.service';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';
import {CheckDialogComponent} from '../check-dialog/check-dialog.component';
import { LoanStatusService} from '../../../../service/loan-status.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-id-check',
  templateUrl: './id-check.component.html',
  styleUrls: ['./id-check.component.scss']
})
export class IdCheckComponent implements OnInit {
  @Input() id;
  @Input() client;
  @Output() tab: EventEmitter<any> = new EventEmitter();
  type: number = 1;
  apiCallId: number;
  dialogRef:MdDialogRef<any>;
  loanStatus: any;
  htmlBlock: any;
  loanSubscription: Subscription;

  constructor(private apiService: ApiService,
              private elementRef:ElementRef,
              private viewContainerRef:ViewContainerRef,
              private dialog:MdDialog,
              private loanStatusService: LoanStatusService) { }

  ngOnInit() {
    const url = `${API.ADMIN_INVESTOR_ID_CHECK_HTML}?id=${this.id}&type=${this.type}`;
    this.apiService.getMethod(url, {'authorizationToken': true}).map(res => res.content).subscribe((res) => {
      const checkHtml = this.elementRef.nativeElement.querySelector('.html');
      this.apiCallId = res.id;
      this.htmlBlock = res.html;
      if(this.htmlBlock) {
        checkHtml.insertAdjacentHTML('beforeend', this.htmlBlock);
      }
      else {
        checkHtml.insertAdjacentHTML('beforeend', 'Document is not ready');
      }
    });
    this.loanSubscription = this.loanStatusService.loanStatus$.subscribe(res => {
      this.loanStatus = res;
    })
  }

  openConfirmDialog(decision) {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;
    config.width = '480px';

    this.dialogRef = this.dialog.open(CheckDialogComponent, config);

    this.dialogRef.afterClosed().subscribe((data) => {
      this.dialogRef = null;
      if(data && data.confirm) {
        this.apiService.postMethod(API.ADMIN_BORROWER_ACCEPT_DECLINE_CHECK_HTML,
          {api_call_id: this.apiCallId,
            decision: decision,
            comment: data.comment
          }, {'authorizationToken': true}).subscribe((res) => {
          this.loanStatusService.changeLoanStatus(res.content.loan_apply_status);
        });
      }
    });
  }

  changeTab(tab) {
    this.tab.emit(tab);
    this.loanSubscription.unsubscribe();
  }
}
