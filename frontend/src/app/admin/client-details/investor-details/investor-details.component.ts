import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {API} from '../../../config';
import {ApiService} from '../../../service/api.service';
import {QuoteStatusService} from '../../../service/quote-status.service';
import {LoanStatusService} from '../../../service/loan-status.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-investor-details',
  templateUrl: './investor-details.component.html',
  styleUrls: ['./investor-details.component.scss']
})
export class InvestorDetailsComponent implements OnInit, OnDestroy {

  public tab: string;
  public id;
  public quoteStatus;
  quoteSubscription: Subscription;

  constructor(private route: ActivatedRoute,
              private apiService: ApiService,
              private quoteStatusService: QuoteStatusService) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.apiService.getMethod(API.ADMIN_INVESTOR_LOAN_APPLY_DETAILS.replace(/(\{quote_id\})/i, '' + this.id), {'authorizationToken': true}).map(res => res.content).subscribe(res => {
      this.quoteStatus = res.status;
      this.quoteStatusService.changeQuoteStatus(res.status);
    });
    this.quoteSubscription = this.quoteStatusService.quoteStatus$.subscribe(res => {
      this.quoteStatus = res;
      this.switchFunction(this.quoteStatus);
    })
  }

  switchFunction(quoteStatus) {
    switch (quoteStatus) {
      case 10:
      case 15:
      case 16:
        this.tab = 'overview';
        break;
      case 20:
      case 30:
      case 40:
      case 50:
      case 60:
      case 70:
        this.tab = 'check';
        break;
      case 80:
        this.tab = 'offers-details';
        break;
      default:
        this.tab = '';
    }
  }

  changeTab(tab) {
    if (this.tab !== tab) {
      this.tab = tab;
    }
  }

  ngOnDestroy() {
    this.quoteSubscription.unsubscribe();
  }
}
