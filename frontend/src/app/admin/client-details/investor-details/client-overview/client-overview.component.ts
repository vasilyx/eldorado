import {Component, EventEmitter, Input, OnInit, Output, OnDestroy} from '@angular/core';
import {API, DATE_FORMAT_SERVER} from '../../../../config';
import {ApiService} from '../../../../service/api.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {QuoteStatusService} from '../../../../service/quote-status.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-client-overview-investor',
  templateUrl: './client-overview.component.html',
  styleUrls: ['./client-overview.component.scss']
})
export class ClientOverviewInvestorComponent implements OnInit, OnDestroy {
  @Input() id;
  @Input() client;
  @Output() tab: EventEmitter<any> = new EventEmitter();

  addresses: Observable<any[]>;
  details: any = {};
  finances: any = [];
  idCheck: any = {};
  quoteStatus: any;
  data: any = {ISA: {}, GEN: {}, SIPP: {}};
  quoteSubscription: Subscription;

  constructor(private apiService: ApiService,
              private quoteStatusService: QuoteStatusService) {
  }

  ngOnInit() {
    this.addresses = this.apiService.getMethod(API.ADMIN_INVESTOR_CLIENT.replace(/(\{id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content);
    this.apiService.getMethod(API.ADMIN_INVESTOR_FINANCES.replace(/(\{id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content).subscribe(res => {
      this.finances = res;
    });
    this.apiService.getMethod(API.ADMIN_INVESTOR_DETAILS.replace(/(\{id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content).subscribe(res => {
      this.details = res;
    });
    this.apiService.getMethod(API.ADMIN_INVESTOR_ID_CHECK.replace(/(\{id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content).subscribe(res => {
      this.idCheck = res;
    });
    this.apiService.getMethod(API.ADMIN_PORTFOLIO_SUMMARY.replace(/(\{id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content).subscribe(res => {
      this.data = res;
    });
    this.quoteSubscription = this.quoteStatusService.quoteStatus$.subscribe(res => {
      this.quoteStatus = res;
    });
  }

  convertDateForDisplay(date) {
    if (date) {
      return moment(date, DATE_FORMAT_SERVER).format('MMM, YYYY');
    }
  }

  convertBirthDate(date) {
    if (date) {
      return moment(date).format('DD/MM/YYYY');
    }
  }

  changeTab(tab) {
    this.tab.emit(tab);
  }

  ngOnDestroy() {
    this.quoteSubscription.unsubscribe();
  }
}
