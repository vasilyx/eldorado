import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {API, INSTALLMENT_STATUS, PAYMENT_STATUS} from '../../../../../config';
import {ApiService} from '../../../../../service/api.service';

@Component({
  selector: 'app-offer-and-details-instalment-dialog',
  templateUrl: './offer-and-details-instalment-dialog.component.html',
  styleUrls: ['./offer-and-details-instalment-dialog.component.scss']
})
export class OfferAndDetailsInstalmentDialogComponent implements OnInit {


  instalmentsData: Array<any> = [];
  paymentStatus = PAYMENT_STATUS;
  instalmentStatus = INSTALLMENT_STATUS;

  constructor(@Inject(MD_DIALOG_DATA) public data,
              public dialogRef: MdDialogRef<OfferAndDetailsInstalmentDialogComponent>,
              private apiService: ApiService) {
  }

  ngOnInit() {
    this.apiService.postMethod(API.ADMIN_INSTALMENTS_BY_OFFER,{
      agreement_id: this.data.agreement_id,
      offer_id: this.data.offer_id
    },{'authorizationToken': true}).subscribe(res => {
      this.instalmentsData = res.content;
    })
  }
  convertDateForDisplay(date) {
    if (date) {
      return moment(date).format('D MMM YYYY');
    }
  }
}
