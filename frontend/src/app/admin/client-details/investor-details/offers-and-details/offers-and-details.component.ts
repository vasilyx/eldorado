import {Component, Input, OnInit, ViewContainerRef} from '@angular/core';
import {API, INSTALLMENT_STATUS, LOAN_STATUS_CODE, PAYMENT_STATUS} from '../../../../config';
import {ApiService} from '../../../../service/api.service';
import {Headers, Http, RequestOptions, ResponseContentType} from '@angular/http';
import {StorageService} from '../../../../service/storage.service';
import {MdDialog, MdDialogConfig, MdDialogRef} from '@angular/material';
import {OfferAndDetailsInstalmentDialogComponent} from './offer-and-details-instalment-dialog/offer-and-details-instalment-dialog.component';

@Component({
  selector: 'app-offers-and-details',
  templateUrl: './offers-and-details.component.html',
  styleUrls: ['./offers-and-details.component.scss']
})
export class OffersAndDetailsComponent implements OnInit {
  @Input() id;
  offers: Array<any>;
  indexArray: Array<boolean>;
  status = LOAN_STATUS_CODE;
  dialogRef: MdDialogRef<any>;
  paymentStatus = PAYMENT_STATUS;
  instalmentStatus = INSTALLMENT_STATUS;

  constructor(private apiService: ApiService,
              private http: Http,
              private storageService: StorageService,
              public dialog: MdDialog,
              public viewContainerRef: ViewContainerRef) {
  }

  ngOnInit() {
    this.apiService.getMethod(API.ADMIN_INVESTOR_OFFER_DETAILS.replace(/(\{quote_id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content).subscribe((res) => {
      this.offers = res;
      this.offers.forEach((item)=> {
        if(item.agreements && item.agreements.length) {
          let offersAmount = 0;
          item.agreements.forEach((agreement)=> {
            offersAmount += parseFloat(agreement.sum_amount);
          });
          item['matching'] = offersAmount / item['amount'];
        }
      });
      this.indexArray = new Array(this.offers.length);
    });
  }

  pdf(loan_apply_id, offer_id) {
    const saveBlob = (function () {
      let a = document.createElement('a');
      document.body.appendChild(a);
      return function (blob, fileName) {
        let url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = fileName;
        a.click();
        window.URL.revokeObjectURL(url);
      };
    }());
    const headers = new Headers({
      'Auth': 'whs' + this.storageService.read('token'),
      'Authorization': 'Bearer ' + this.storageService.read('token'),
      'Content-Type': 'application/pdf'
    });
    let options = new RequestOptions({headers: headers});
    options.responseType = ResponseContentType.Blob;
    let url = API.ADMIN_CONTRACT_PDF.replace(/(\{loan_apply_id\})/i, loan_apply_id) + `?invest_offer_id=${offer_id}`;
    this.http.get(url, options).subscribe(
      (response) => {
        if (response.status === 200) {
          const blob = new Blob([response.blob()], {type: 'application/pdf'});
          saveBlob(blob, 'Agreement.pdf');
        }
      });
  }

  getInstalment(agreement_id, offer_id) {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;
    config.width = '1000px';
    config.height = '560px';
    config.data = {
      agreement_id: agreement_id,
      offer_id: offer_id
    };

    this.dialogRef = this.dialog.open(OfferAndDetailsInstalmentDialogComponent, config);
    this.dialogRef.afterClosed().subscribe(() => {
      this.dialogRef = null;
    });
  }
}
