import {Component, Input, OnInit, ElementRef, ViewContainerRef, Output, EventEmitter, OnDestroy} from '@angular/core';
import {API} from '../../../../config';
import {ApiService} from '../../../../service/api.service';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';
import {CheckDialogComponent} from '../../borrower-details/check-dialog/check-dialog.component';
import { QuoteStatusService} from '../../../../service/quote-status.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-id-check-investor',
  templateUrl: './id-check-investor.component.html',
  styleUrls: ['./id-check-investor.component.scss']
})
export class IdCheckInvestorComponent implements OnInit, OnDestroy {
  @Input() id;
  @Input() client;
  @Output() tab: EventEmitter<any> = new EventEmitter();
  type: number = 1;
  apiCallId: number;
  dialogRef:MdDialogRef<any>;
  quoteStatus: any;
  htmlBlock: any;
  quoteSubscription: Subscription;

  constructor(private apiService: ApiService,
              private elementRef:ElementRef,
              private viewContainerRef:ViewContainerRef,
              private dialog:MdDialog,
              private quoteStatusService: QuoteStatusService) { }

  ngOnInit() {
    const url = `${API.ADMIN_INVESTOR_ID_CHECK_HTML}?id=${this.id}&type=${this.type}`;
    this.apiService.getMethod(url, {'authorizationToken': true}).map(res => res.content).subscribe((res) => {
      const checkHtml = this.elementRef.nativeElement.querySelector('.html');
      this.apiCallId = res.id;
      this.htmlBlock = res.html;
      if(this.htmlBlock) {
        checkHtml.insertAdjacentHTML('beforeend', this.htmlBlock);
      }
      else {
        checkHtml.insertAdjacentHTML('beforeend', 'Document is not ready');
      }
    });
    this.quoteSubscription = this.quoteStatusService.quoteStatus$.subscribe(res => {
      this.quoteStatus = res;
    });
  }

  openConfirmDialog(decision) {
    let config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = false;
    config.width = '480px';

    this.dialogRef = this.dialog.open(CheckDialogComponent, config);

    this.dialogRef.afterClosed().subscribe((data) => {
      this.dialogRef = null;
      if(data && data.confirm) {
        this.apiService.postMethod(API.ADMIN_INVESTOR_ACCEPT_DECLINE_CHECK_HTML,
          {api_call_id: this.apiCallId,
            decision: decision,
            comment: data.comment
          }, {'authorizationToken': true}).subscribe((res) => {
          this.quoteStatusService.changeQuoteStatus(res.content.quote_status);
        });
      }
    });
  }

  changeTab(tab) {
    this.tab.emit(tab);
  }

  ngOnDestroy() {
    this.quoteSubscription.unsubscribe();
  }
}
