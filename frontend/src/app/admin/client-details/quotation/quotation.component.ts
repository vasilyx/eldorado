import {Component, Input, OnInit} from '@angular/core';
import {API} from '../../../config';
import {ApiService} from '../../../service/api.service';

@Component({
  selector: 'app-quotation',
  templateUrl: './quotation.component.html',
  styleUrls: ['./quotation.component.scss']
})
export class QuotationComponent implements OnInit {
  @Input() id;
  content: any = {};

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getMethod(API.ADMIN_BORROWER_QUATATION.replace(/(\{quote_id\})/i, `${this.id}`), {'authorizationToken': true}).map(res => res.content).subscribe(res => {
      this.content = res;
    });
  }
}
