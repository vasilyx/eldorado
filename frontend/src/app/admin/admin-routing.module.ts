import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AdminComponent} from './admin.component';
import {AdminClientInfoComponent} from './admin-client-info/admin-client-info.component';
import {AdminLoginComponent} from './admin-auth/admin-login/admin-login.component';
import {AdminResetComponent} from './admin-auth/admin-reset/admin-reset.component';
import {AdminClientsComponent} from './admin-clients/admin-clients.component';
import {AdminSettingsComponent} from './admin-settings/admin-settings.component';
import {ClientDetailsComponent} from './client-details/client-details.component'
import {BorrowerDetailsComponent} from './client-details/borrower-details/borrower-details.component';
import {InvestorDetailsComponent} from './client-details/investor-details/investor-details.component';
import {AdminFinancesComponent} from './admin-finances/admin-finances.component';
import {AuthGuard} from '../guard/auth-guard';

const routes: Routes = [
  {
    path: 'whadmin',
    component: AdminComponent,
    children: [
      {path: '', redirectTo: 'login', pathMatch: 'full'},
      {path: 'login', component: AdminLoginComponent},
      {path: 'reset', component: AdminResetComponent},
      {path: 'clients', component: AdminClientsComponent, canActivate: [AuthGuard]},
      {path: 'settings', component: AdminSettingsComponent, canActivate: [AuthGuard]},
      {path: 'finances', component: AdminFinancesComponent, canActivate: [AuthGuard]},
      {path: 'client/:id', component: AdminClientInfoComponent, canActivate: [AuthGuard]},
      {
        path: 'client-details',
        component: ClientDetailsComponent,
        children: [
          {path: 'borrower/:id', component: BorrowerDetailsComponent, canActivate: [AuthGuard]},
          {path: 'investor/:id', component: InvestorDetailsComponent, canActivate: [AuthGuard]},
        ]}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
