import { Routes, RouterModule } from '@angular/router';

import { BorrowComponent } from './borrow/borrow.component';
import { BorrowRegistrationComponent } from './borrow/registration/registration.component';
import { BorrowStep2Component } from './borrow/step2/step2.component';
import { BorrowStep3Component } from './borrow/step3/step3.component';
import { BorrowStep5Component } from './borrow/step5/step5.component';

import { InvestComponent } from './invest/invest.component';
import { InvestRegistrationComponent } from './invest/registration/registration.component';
import { InvestStep2Component } from './invest/step2/step2.component';
import { InvestStep3Component } from './invest/step3/step3.component';
import { InvestStep5Component } from './invest/step5/step5.component';
import { InvestStep6Component } from './invest/step6/step6.component';
import { InvestStep7Component } from './invest/step7/step7.component';

import { LoanComponent } from './loan/loan.component';
import { DashboardComponent } from './loan/dashboard/dashboard.component';
import { LoanPage2Component } from './loan/page2/page2.component';
import { LoanPage3Component } from './loan/page3/page3.component';
import { LoanPage4Component } from './loan/page4/page4.component';
import { LoanPage5Component } from './loan/page5/page5.component';
import { LoanPage7Component } from './loan/page7/page7.component';
import { LoanPage8Component } from './loan/page8/page8.component';
import { LoanPage9Component } from './loan/page9/page9.component';
import { LoanPage10Component } from './loan/page10/page10.component';
import { LoanPage11Component } from './loan/page11/page11.component';
import { LoanPage12Component } from './loan/page12/page12.component';

import { ViewXMLComponent } from './loan/view-xml/view-xml.component';
import { LoginComponent } from './auth/login/login.component';
import { ResetComponent } from './auth/reset/reset.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { ActivityLogsComponent } from './loan/activity-logs/activity-logs.component';
import {InvestStep3DashboardComponent} from './invest/step3-dashboard/step3-dashboard.component';
import {BorrowStep5DashboardComponent} from './borrow/step5-dashboard/step5-dashboard.component';
import {TermsOfUseComponent} from './shared/terms-of-use/terms-of-use.component';
import {LoanAgreementComponent} from './loan/loan-agreement/loan-agreement.component';
import {OfferListComponent} from './loan/offer-list/offer-list.component';

export const routes:Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'borrow', component: BorrowComponent},
  {
    path: 'borrow',
    component: BorrowComponent,
    children: [
      {path: 'registration', component: BorrowRegistrationComponent},
      {path: 'quote_term', component: BorrowStep2Component},
      {path: 'quote_income', component: BorrowStep3Component},
      {path: 'contact_information', component: BorrowStep5Component}
    ]
  },
  {path: 'invest', component: InvestComponent},
  {
    path: 'invest',
    component: InvestComponent,
    children: [
      {path: 'registration', component: InvestRegistrationComponent},
      {path: 'contact_information', component: InvestStep2Component},
      {path: 'quote_income', component: InvestStep3Component}
    ]
  },
  {
    path: 'portal',
    component: LoanComponent,
    children: [
      {path: 'dashboard', component: DashboardComponent},
      {path: 'create_offer', component: InvestStep5Component},
      {path: 'transfer_money/:offerId', component: InvestStep6Component},
      {path: 'transfer_money_result', component: InvestStep7Component},
      {path: 'invest/contact_information', component: InvestStep2Component},
      {path: 'invest/quote_income', component: InvestStep3DashboardComponent},
      {path: 'borrow/quote_term', component: BorrowStep2Component},
      {path: 'borrow/quote_income', component: BorrowStep3Component},
      {path: 'borrow/contact_information', component: BorrowStep5DashboardComponent},
      {path: 'quote_details/approved/:id', component: LoanPage2Component},
      {path: 'quote_details/rejected/:id', component: LoanPage3Component},
      {path: 'loan/loan_agreement/:id', component: LoanAgreementComponent},
      {path: 'loan/personal_information', component: LoanPage4Component},
      {path: 'loan/contact_information', component: LoanPage5Component},
      {path: 'loan/employment_information', component: LoanPage7Component},
      {path: 'loan/dependent_information', component: LoanPage8Component},
      {path: 'loan/credit_lines', component: LoanPage9Component},
      {path: 'loan/credit_lines_declutter', component: LoanPage10Component},
      {path: 'loan/status', component: LoanPage11Component},
      {path: '12', component: LoanPage12Component},
      {path: 'view_xml/:type/:id', component: ViewXMLComponent},
      {path: 'activity_logs', component: ActivityLogsComponent},
      {path: 'offer-list', component: OfferListComponent}
    ]
  },
  {path: 'login', component: LoginComponent},
  {path: 'reset', component: ResetComponent},
  {path: 'terms-of-use', component: TermsOfUseComponent},
  {path: '**', component: PageNotFoundComponent}
];

export const routing = RouterModule.forRoot(routes, {useHash: true});
