import 'hammerjs';
import {NgModule} from '@angular/core';
import {HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {routing} from './routes';
import {GestureConfig, MaterialModule} from '@angular/material';
import {TextMaskModule} from 'angular2-text-mask/';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AdminModule} from './admin/admin.module';

import {AppComponent} from './app.component';
import {BorrowComponent} from './borrow/borrow.component';
import {BorrowRegistrationComponent} from './borrow/registration/registration.component';
import {BorrowStep2Component} from './borrow/step2/step2.component';
import {BorrowStep3Component} from './borrow/step3/step3.component';

import {BorrowStep5Component} from './borrow/step5/step5.component';
import {InvestComponent} from './invest/invest.component';
import {InvestRegistrationComponent} from './invest/registration/registration.component';
import {InvestStep2Component} from './invest/step2/step2.component';
import {InvestStep3Component} from './invest/step3/step3.component';

import {InvestStep5Component} from './invest/step5/step5.component';
import {ConfirmOfferDialogComponent} from './invest/step5/confirm-offer-dialog.component';
import {RestrictOfferDialogComponent} from './invest/step5/restrict-offer-dialog.component';
import {InvestStep6Component} from './invest/step6/step6.component';
import {InvestStep7Component} from './invest/step7/step7.component';
import {LoanComponent} from './loan/loan.component';
import {DashboardComponent} from './loan/dashboard/dashboard.component';
import {LoanPage2Component} from './loan/page2/page2.component';
import {LoanPage3Component} from './loan/page3/page3.component';
import {LoanPage4Component} from './loan/page4/page4.component';
import {LoanPage5Component} from './loan/page5/page5.component';

import {LoanPage7Component} from './loan/page7/page7.component';
import {OccupationDialogComponent} from './loan/page7/occupation-dialog.component';
import {LoanPage8Component} from './loan/page8/page8.component';
import {DependentDialogComponent} from './loan/page8/dependent-dialog.component';
import {LoanPage9Component} from './loan/page9/page9.component';
import {CreditDialogComponent} from './loan/page9/credit-dialog.component';
import {LoanPage10Component} from './loan/page10/page10.component';
import {LoanPage11Component} from './loan/page11/page11.component';
import {LoanPage12Component} from './loan/page12/page12.component';
import {LoginComponent} from './auth/login/login.component';
import {ResetComponent} from './auth/reset/reset.component';
import {HeaderComponent} from './shared/header/header.component';
import {NavigationComponent} from './shared/navigation/navigation.component';
import {PageNotFoundComponent} from './shared/page-not-found/page-not-found.component';
import {SpinnerComponent} from './shared/spinner/spinner.component';
import {AutocompleteComponent} from './shared/autocomplete/autocomplete.component';
import {HeaderService} from './shared/header/header.service';
import {AddressDialogComponent} from './shared/address-dialog/address-dialog.component';
import {VerificationDialogComponent} from './shared/verification-dialog/verification-dialog.component';
import {ViewXMLComponent} from './loan/view-xml/view-xml.component';
import {ActivityLogsComponent} from './loan/activity-logs/activity-logs.component';
import {ConfirmationModalDialog} from './shared/confirmation-dialog/confirmation-dialog.component';

import {NavigationService} from './shared/navigation/navigation.service';
import {SpinnerService} from './shared/spinner/spinner.service';
import {ApiService} from './service/api.service';
import {StorageService} from './service/storage.service';
import {TextBuilderService} from './service/text_builder.service';
import {AutocompleteService} from './shared/autocomplete/autocomplete.service';

import {ClearSecurityDialogComponent} from './shared/clear-security-dialog/clear-security-dialog.component';

import {SharedModule} from './shared/shared-module';
import {TermsOfUseComponent} from './shared/terms-of-use/terms-of-use.component';
import {InvestModalComponent} from './shared/invest-modal/invest-modal.component';
import {InvestStep2DashboardComponent} from './invest/step2-dashboard/step2-dashboard.component';
import {InvestStep3DashboardComponent} from './invest/step3-dashboard/step3-dashboard.component';
import {BorrowerModalComponent} from './shared/borrower-modal/borrower-modal.component';
import {BorrowStep5DashboardComponent} from './borrow/step5-dashboard/step5-dashboard.component';
import {LoanAgreementComponent} from './loan/loan-agreement/loan-agreement.component';
import {PaddingZeroService} from './service/padding-zero.service';
import {OfferListComponent} from './loan/offer-list/offer-list.component';
import {DashboardService} from './service/dashboard.service';
import {DashboardTabComponent} from './loan/dashboard/dashboard-tab/dashboard-tab.component';
import {InvestmentsTabComponent} from './loan/dashboard/investments-tab/investments-tab.component';
import {LoansTabComponent} from './loan/dashboard/loans-tab/loans-tab.component';
import {LoansDialogComponent} from './loan/dashboard/loans-tab/loans-dialog/loans-dialog.component';
import {InvestmentsDialogComponent} from './loan/dashboard/investments-tab/investments-dialog/investments-dialog.component';
import {OfferInstalmentDialogComponent} from './loan/offer-list/offer-instalment-dialog/offer-instalment-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    BorrowComponent,
    BorrowRegistrationComponent,
    BorrowStep2Component,
    BorrowStep3Component,

    BorrowStep5Component,
    InvestComponent,
    InvestRegistrationComponent,
    InvestStep2Component,
    InvestStep3Component,

    InvestStep5Component,
    ConfirmOfferDialogComponent,
    RestrictOfferDialogComponent,
    InvestStep6Component,
    InvestStep7Component,
    LoanComponent,
    DashboardComponent,
    LoanPage2Component,
    LoanPage3Component,
    LoanPage4Component,
    LoanPage5Component,
    ViewXMLComponent,
    ActivityLogsComponent,

    LoanPage7Component,
    OccupationDialogComponent,
    LoanPage8Component,
    DependentDialogComponent,
    LoanPage9Component,
    CreditDialogComponent,
    LoanPage10Component,
    LoanPage11Component,
    LoanPage12Component,
    LoginComponent,
    ResetComponent,
    HeaderComponent,
    PageNotFoundComponent,
    NavigationComponent,
    SpinnerComponent,
    AutocompleteComponent,
    AddressDialogComponent,
    VerificationDialogComponent,
    ConfirmationModalDialog,
    ClearSecurityDialogComponent,
    TermsOfUseComponent,
    InvestModalComponent,
    BorrowerModalComponent,
    InvestStep2DashboardComponent,
    InvestStep3DashboardComponent,
    BorrowStep5DashboardComponent,
    LoanAgreementComponent,
    OfferListComponent,
    DashboardTabComponent,
    InvestmentsTabComponent,
    LoansTabComponent,
    LoansDialogComponent,
    InvestmentsDialogComponent,
    OfferInstalmentDialogComponent],
  entryComponents: [
    AddressDialogComponent,
    VerificationDialogComponent,
    OccupationDialogComponent,
    DependentDialogComponent,
    ConfirmOfferDialogComponent,
    RestrictOfferDialogComponent,
    CreditDialogComponent,
    ConfirmationModalDialog,
    ClearSecurityDialogComponent,
    InvestModalComponent,
    BorrowerModalComponent,
    LoansDialogComponent,
    InvestmentsDialogComponent,
    OfferInstalmentDialogComponent
  ],
  imports: [
    FormsModule,
    HttpModule,
    MaterialModule,
    NgbModule.forRoot(),
    TextMaskModule,
    SharedModule,
    routing,
    AdminModule
  ],
  providers: [
    SpinnerService,
    ApiService,
    StorageService,
    TextBuilderService,
    NavigationService,
    AutocompleteService,
    HeaderService,
    PaddingZeroService,
    DashboardService,
    {provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

